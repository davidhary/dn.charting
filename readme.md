# Charting Libraries

Encapsulates the Visual Studio chart control.

* [Getting started](#Getting-Started)* Project README files:
  * [cc.isr.Json.AppSettings.MSTest](/src/app.settings/app.settings.MSTest/readme.md) 
* [Attributions](Attributions.md)
* [Change Log](./CHANGELOG.md)
* [Cloning](Cloning.md)
* [Code of Conduct](code_of_conduct.md)
* [Contributing](contributing.md)
* [Legal Notices](#legal-notices)
* [License](LICENSE)
* [Open Source](Open-Source.md)
* [Repository Owner](#Repository-Owner)
* [Security](security.md)

<a name="Getting-Started"></a>
## Getting Started

Clone the repository along with its requisite repositories to their respective relative path.

### Line Chart Control VB.Net

1. Add the line chart control to a control (MeterChartControl) or a form in place of MS Chart.

2. Add handler for updating the tool tip
```
Public Class MeterChartControl
	
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        AddHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
    End Sub
	
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If Me._Chart IsNot Nothing Then
                    RemoveHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class	

```

3. Initialize
```
    Public Sub InitializeKnownState()
        Me.Chart.AddTitle("Frequency Distribution")
        Me.Chart.InitializeCustomPalette()
        Me.Chart.InitializeChartArea(False)
    End Sub
```

4. Display
```
    Public Function Simulate() As Series
        Dim l As New List(Of Double)
        Dim rnd As New Random
        For i As Integer = 1 To 20000
            l.Add(rnd.NextNormal)
        Next
        Return Me.Display(l.ToArray)
    End Function

    ''' <summary> Name of the histogram series. </summary>
    Public Const HistogramSeriesName As String = "histogram"
	
    Public Function Display(ByVal values As IEnumerable(Of Double)) As Series
        Dim lowerLimit As Double = -3
        Dim upperLimit As Double = 3
        Dim count As Integer = 51
        Dim result As IEnumerable(Of Windows.Point) = values.HistogramDirect(lowerLimit, upperLimit, count)
        Dim ser As Series = Me.Chart.GraphLineSeries(MeterChartControl.HistogramSeriesName, SeriesChartType.FastLine, result)
        Me.Chart.ChartArea.AxisX.Minimum = -3
        Me.Chart.ChartArea.AxisX.Maximum = 3
        Return ser
    End Function
```

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="legal-notices"></a>
## Legal Notices

Integrated Scientific Resources, Inc., and any contributors grant you a license to the documentation and other content in this repository under the [Creative Commons Attribution 4.0 International Public License] and grant you a license to any code in the repository under the [MIT License].

Integrated Scientific Resources, Inc., and/or other Integrated Scientific Resources, Inc., products and services referenced in the documentation may be either trademarks or registered trademarks of Integrated Scientific Resources, Inc., in the United States and/or other countries. The licenses for this project do not grant you rights to use any Integrated Scientific Resources, Inc., names, logos, or trademarks.

Integrated Scientific Resources, Inc., and any contributors reserve all other rights, whether under their respective copyrights, patents, or trademarks, whether by implication, estoppel or otherwise.

[Creative Commons Attribution 4.0 International Public License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license
[MIT License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license-code
 
[ATE Coder]: https://www.IntegratedScientificResources.com
[dn.core]: https://www.bitbucket.org/davidhary/dn.core

