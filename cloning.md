# Cloning

* [Source Code](#Source-Code)
* [Repositories](#Repositories)
* [Packages](#Packages)
* [Facilitated By](#Facilitated-By)

<a name="Getting-Started"></a>
## Getting Started

Clone the repository along with its requisite repositories to their respective relative path.

### Line Chart Control VB.Net

1. Add the line chart control to a control (MeterChartControl) or a form in place of MS Chart.

2. Add handler for updating the tool tip
```
Public Class MeterChartControl
	
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        AddHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
    End Sub
	
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If Me._Chart IsNot Nothing Then
                    RemoveHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class	

```

3. Initialize
```
    Public Sub InitializeKnownState()
        Me.Chart.AddTitle("Frequency Distribution")
        Me.Chart.InitializeCustomPalette()
        Me.Chart.InitializeChartArea(False)
    End Sub
```

4. Display
```
    Public Function Simulate() As Series
        Dim l As New List(Of Double)
        Dim rnd As New Random
        For i As Integer = 1 To 20000
            l.Add(rnd.NextNormal)
        Next
        Return Me.Display(l.ToArray)
    End Function

    ''' <summary> Name of the histogram series. </summary>
    Public Const HistogramSeriesName As String = "histogram"
	
    Public Function Display(ByVal values As IEnumerable(Of Double)) As Series
        Dim lowerLimit As Double = -3
        Dim upperLimit As Double = 3
        Dim count As Integer = 51
        Dim result As IEnumerable(Of Windows.Point) = values.HistogramDirect(lowerLimit, upperLimit, count)
        Dim ser As Series = Me.Chart.GraphLineSeries(MeterChartControl.HistogramSeriesName, SeriesChartType.FastLine, result)
        Me.Chart.ChartArea.AxisX.Minimum = -3
        Me.Chart.ChartArea.AxisX.Maximum = 3
        Return ser
    End Function
```

### Repositories
The repositories listed in [external repositories] are required:
* [Charting] - Charting Libraries
* [Std] - .NET standard libraries

```
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.charting.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\std
%vslib%\visual\charting
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

## Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

## Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Charting]: https://www.bitbucket.org/davidhary/dn.charting
[Std]: https://www.bitbucket.org/davidhary/dn.std

[Visuals Charting Libraries]: https://bitbucket.org/davidhary/vs.Visuals.Charting  
[Smooth Zoom & Round Numbers in MS Chart]: https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart
[Speedup Chart Clear Data Points]: http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points
[Graphing time series]: http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series
[Data Visualization]: https://www.codeproject.com/Articles/5300595/Winforms-datavisualization-NET-5

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

