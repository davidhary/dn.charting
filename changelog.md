# Changelog
All notable changes to these libraries will be documented in this file. 
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.1.8956] - 2024-07-09 Preview 202304
* Reference updated core projects.

## [3.1.8949] - 2024-07-02 Preview 202304
* Update to .Net 8.
* Add a local editor config to point to the exclusion.dix dictionary.
* Split off Chart 3D to its own solution.
* Apply or exclude code analysis rules.
* Generate assembly attributes.

## [3.1.8548] - 2023-05-28 Preview 202304
* Update project files.
* Update packages.

## [3.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [3.0.8189] - 2022-06-03
* Use Tuples to implement GetHashCode().

## [3.0.8125] - 2022-03-31
* pass tests in project reference mode.

## [3.0.8106] - 2022-03-12
* rebuild packages using the ISR .NET standard framework.

## [3.1.8099] - 2022-03-05
* Rebuild packages verifying that dependencies referenced in the 
consuming project such that only top package needs to be installed  explicitly.

## [3.0.8082] - 2022-02-15
* Build NuGet packages.

## [3.0.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.

## [3.0.7915] - 2021-09-02
* Add methods charting an existing series.

## [3.0.7829] - 2021-06-07
* Uses core framework. 
* Added .NET Core 5 projects

## [3.0.7612] - 2020-11-02
* Converted to C#

## [3.0.7604] - 2020-10-26
Breaking change. 
Remove Chart initialization from the Line Chart Control constructor to prevent 
exception because the form constructor adds a default area.

## [2.1.6667] - 2018-04-03
2018 release.

## [2.0.6325 ] - 2017-04-24
Uses VS 2017 rule set. Applies camel casing to all
elements public or private. Simplifies initializing of objects and
collections.

## [2.0.6083] - 2016-08-27
Changes line chart to a control.

## [1.0.6055] - 2016-07-
Adds line chart class.

## [1.0.5167] - 2014-02-23
Created.

&copy;  2014 Integrated Scientific Resources, Inc. All rights reserved.

[3.1.8556]: https://www.bitbucket.org/davidhary/dn.charting
