# Open Source

Encapsulates the Visual Studio chart control.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open source
Open source used by this software is described and licensed at the
following sites:  
[Visuals Charting Libraries]
[Smooth Zoom & Round Numbers in MS Chart]
[Speedup Chart Clear Data Points]
[Graphing time series]
[Data Visualization]

<a name="Closed-software"></a>
## Closed software
None

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Charting]: https://www.bitbucket.org/davidhary/dn.charting
[Std]: https://www.bitbucket.org/davidhary/dn.std

[Visuals Charting Libraries]: https://bitbucket.org/davidhary/vs.Visuals.Charting  
[Smooth Zoom & Round Numbers in MS Chart]: https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart
[Speedup Chart Clear Data Points]: http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points
[Graphing time series]: http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series
[Data Visualization]: https://www.codeproject.com/Articles/5300595/Winforms-datavisualization-NET-5

