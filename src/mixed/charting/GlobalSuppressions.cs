// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr.Visuals.Charting" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr.Visuals.Charting.ChartingExtensions" )]
[assembly: SuppressMessage( "Style", "IDE0290:Use primary constructor", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.ChartScaleData.#ctor(System.Windows.Forms.DataVisualization.Charting.Chart)" )]
[assembly: SuppressMessage( "Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.ChartScaleData.UpdateAxisBaseDataX" )]
[assembly: SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.LineChartControl.OnGetTooltipText(System.Object,System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.LineChartControl.GraphLineSeries(System.Windows.Forms.DataVisualization.Charting.Series,System.Windows.Forms.DataVisualization.Charting.SeriesChartType,System.Double[],System.Double[],System.Boolean,System.Drawing.Color,System.Boolean)~System.Windows.Forms.DataVisualization.Charting.Series" )]
[assembly: SuppressMessage( "Style", "IDE0071:Simplify interpolation", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.LineChartControl.GraphLineSeries(System.Windows.Forms.DataVisualization.Charting.Series,System.Windows.Forms.DataVisualization.Charting.SeriesChartType,System.Double[],System.Double[],System.Boolean,System.Drawing.Color,System.Boolean)~System.Windows.Forms.DataVisualization.Charting.Series" )]
[assembly: SuppressMessage( "Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.ChartingExtensions.Methods.DrawZoomRect(System.Windows.Forms.DataVisualization.Charting.Chart,System.Drawing.Rectangle,System.Boolean)" )]
[assembly: SuppressMessage( "Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.ChartingExtensions.Methods.DrawZoomRect(System.Windows.Forms.DataVisualization.Charting.Chart,System.Drawing.Pen,System.Drawing.Rectangle,System.Boolean)" )]
[assembly: SuppressMessage( "Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.Visuals.Charting.ChartScaleData.UpdateAxisBaseDataY" )]
