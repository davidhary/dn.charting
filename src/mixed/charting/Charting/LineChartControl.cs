using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace cc.isr.Visuals.Charting;

/// <summary> a line chart. </summary>
/// <remarks>
/// (c) 2016 Darryl Bryk. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-07-29. Source:
/// http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series. </para>
/// </remarks>
public class LineChartControl : Chart
{
    #region " construction "

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public LineChartControl() : base()
    {
    }

    #region " idisposable support "

    /// <summary> Releases unmanaged and, optionally, managed resources. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both unmanaged and managed
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.ChartArea?.Dispose();

                if ( this.ContextMenuStrip is object )
                {
                    this.ContextMenuStrip.Items?.Clear();
                    this.ZoomOutToolStripMenuItem?.Dispose();
                    this.NiceRoundNumbersToolStripMenuItem?.Dispose();
                    this.ZoomWithGDI32ToolStripMenuItem?.Dispose();
                    this.DefaultFormatToolStripMenuItem?.Dispose();
                    this.SmartFormatToolStripMenuItem?.Dispose();
                    this.WholeNumberFormatToolStripMenuItem?.Dispose();
                    this._separatorToolStripItem?.Dispose();
                    this._separatorToolStripItem1?.Dispose();
                }
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #endregion

    #region " initialize known state "

    /// <summary> Initializes the known state. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="zoomable"> True if zoomable. </param>
    public void InitializeKnownState( bool zoomable )
    {
        this.InitializeKnownState( ChartAreaName, zoomable );
    }

    /// <summary> Initializes the known state. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="areaName"> Name of the area. </param>
    /// <param name="zoomable"> True if zoomable. </param>
    public void InitializeKnownState( string areaName, bool zoomable )
    {
        this.ChartAreas.Clear();
        this.Clear();
        this.InitializeChartArea( areaName, zoomable );
        this.InitializeCustomPalette();
    }

    #endregion

    #region " area "

    /// <summary> Name of the chart area. </summary>
    public const string ChartAreaName = "Area";

    /// <summary> Gets or sets the chart area. </summary>
    /// <value> The chart area. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ChartArea? ChartArea { get; private set; }

    /// <summary> Initializes the chart area. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="areaName"> Name of the area. </param>
    /// <param name="zoomable"> True if zoomable. </param>
    public void InitializeChartArea( string areaName, bool zoomable )
    {
        this.ChartArea = this.ChartAreas.IsUniqueName( areaName ) ? this.ChartAreas.Add( ChartAreaName ) : this.ChartAreas[ChartAreaName];

        // Enable range selection and zooming
        this.ChartArea.CursorX.IsUserEnabled = zoomable;
        this.ChartArea.CursorX.IsUserSelectionEnabled = zoomable;
        this.ChartArea.AxisX.ScaleView.Zoomable = zoomable;
        this.ChartArea.AxisX.ScrollBar.IsPositionedInside = true;
        this.ChartArea.AxisX.ScaleView.SmallScrollSize = 1d;
        this.ChartArea.AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
        this.ChartArea.AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount;
        this.ChartArea.AxisX.LabelStyle.Format = "F0";
        this.ChartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
        this.ChartArea.AxisX.MajorGrid.LineColor = Color.FromArgb( 200, 200, 200 );
        this.ChartArea.AxisY.MajorGrid.LineColor = Color.FromArgb( 200, 200, 200 );
        this.ChartArea.AxisX.LabelStyle.Font = new Font( this.ChartArea.AxisX.LabelStyle.Font.Name, 9f, FontStyle.Regular );
        this.ChartArea.AxisY.LabelStyle.Font = new Font( this.ChartArea.AxisY.LabelStyle.Font.Name, 9f, FontStyle.Regular );
        this.ChartArea.AxisX.TitleFont = new Font( this.ChartArea.AxisX.TitleFont.Name, 9f, FontStyle.Regular );
        this.ChartArea.AxisY.TitleFont = new Font( this.ChartArea.AxisY.TitleFont.Name, 9f, FontStyle.Regular );
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void Clear()
    {
        this.Series.Clear();
        this.Titles.Clear();
        this.Legends.Clear();
    }

    /// <summary> Last chart area. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <returns> a ChartArea. </returns>
    private ChartArea LastChartArea()
    {
        return this.ChartAreas[^1];
    }

    #endregion

    #region " axis: abscissa "

    /// <summary> Axis x coordinate title. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value"> The title. </param>
    public void AbscissaTitle( string value )
    {
        if ( this.ChartArea?.AxisX is not null )
            this.ChartArea.AxisX.Title = value;
    }

    #endregion

    #region " axis: ordinate "

    /// <summary> Axis y coordinate title. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value"> The title. </param>
    public void OrdinateTitle( string value )
    {
        if ( this.ChartArea?.AxisY is null ) return;

        if ( string.IsNullOrEmpty( this.ChartArea.AxisY.Title ) )
        {
            this.ChartArea.AxisY.Title = value;
        }
        else if ( !this.ChartArea.AxisY.Title.Contains( value ) )
        {
            this.ChartArea.AxisY.Title = $"{this.ChartArea.AxisY.Title}, {value}"; // Append
        }
    }

    #endregion

    #region " legend "

    /// <summary> Name of the chart legend. </summary>
    public const string MainLegendName = "Legend";

    /// <summary> Gets or sets the main legend. </summary>
    /// <value> The main legend. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Legend? MainLegend { get; private set; }

    /// <summary> Adds main legend. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void AddMainLegend()
    {
        this.MainLegend = this.Legends.Add( MainLegendName );
        this.MainLegend.Docking = Docking.Bottom;
        this.MainLegend.Font = new Font( this.Legends[0].Font.FontFamily, 8f ); // Font size
        this.MainLegend.IsTextAutoFit = true;
    }

    #endregion

    #region " line annotation "

    /// <summary> Adds t=0 line annotation to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void GraphOriginLine()
    {
        this.GraphOriginLine( "t=0" );
    }

    /// <summary> Adds t=0 line annotation to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="caption"> The caption. </param>
    public void GraphOriginLine( string caption )
    {
        this.GraphLineAnnotations( caption, 0d, Color.Black, true, ChartDashStyle.Dash );
    }

    /// <summary> Adds line annotation to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="name">         The name. </param>
    /// <param name="anchorX">      The x coordinate. </param>
    /// <param name="lineColor">    The lineColor. </param>
    /// <param name="labelEnabled"> true to label. </param>
    public void GraphLineAnnotations( string name, double anchorX, Color lineColor, bool labelEnabled )
    {
        this.GraphLineAnnotations( name, anchorX, lineColor, labelEnabled, ChartDashStyle.Solid );
    }

    /// <summary> Adds line annotation to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="name">      The name. </param>
    /// <param name="anchorX">   The x coordinate. </param>
    /// <param name="lineColor"> The lineColor. </param>
    /// <param name="lineStyle"> the line style. </param>
    public void GraphLineAnnotations( string name, double anchorX, Color lineColor, ChartDashStyle lineStyle )
    {
        this.GraphLineAnnotations( name, anchorX, lineColor, false, lineStyle );
    }

    /// <summary> Adds line annotation to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="name">         The name. </param>
    /// <param name="anchorX">      The x coordinate. </param>
    /// <param name="lineColor">    The lineColor. </param>
    /// <param name="labelEnabled"> true to label. </param>
    /// <param name="lineStyle">    the line style. </param>
    public void GraphLineAnnotations( string name, double anchorX, Color lineColor, bool labelEnabled, ChartDashStyle lineStyle )
    {
        foreach ( Annotation ann in this.Annotations )
        {
            if ( (ann.Name ?? "") == (name ?? "") ) // Don't duplicate
            {
                return;
            }
        }

        if ( this.ChartArea is null ) return;

        VerticalLineAnnotation annot = new()
        {
            AxisX = this.ChartArea.AxisX,
            AxisY = this.ChartArea.AxisY,
            IsInfinitive = true,
            ClipToChartArea = this.ChartArea.Name,
            LineWidth = 1,
            LineColor = lineColor,
            LineDashStyle = lineStyle,
            ToolTip = name
        };
        annot.Name = annot.ToolTip;
        annot.AnchorX = anchorX;
        this.Annotations.Add( annot );
        if ( labelEnabled )
        {
            RectangleAnnotation a = new()
            {
                Text = name,
                AxisX = annot.AxisX,
                AxisY = annot.AxisY,
                AnchorX = anchorX,
                LineColor = Color.Transparent,
                BackColor = Color.Transparent,
                ForeColor = lineColor
            };
            double textWidth = TextRenderer.MeasureText( name, a.Font ).Width + 1.0f;
            double textHeight = TextRenderer.MeasureText( name, a.Font ).Height + 1.0f;
            a.Width = textWidth / this.Width * 100d;
            a.Height = textHeight / this.Height * 100d;
            a.X -= Math.Floor( 0.5d * a.Width );
            this.ChartArea.RecalculateAxesScale(); // Needed so AxisY.Maximum != NaN
            a.Y = this.ChartArea.AxisY.Maximum;
            this.Annotations.Add( a );

            // Handle resize - annotation sized as % of chart size
            Resize += ( sender, e ) =>
            {
                a.Width = textWidth / this.Width * 100d;
                a.Height = textHeight / this.Height * 100d;
            };
        }
    }

    #endregion

    #region " series "

    /// <summary> Number of series in the chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <returns> An Integer. </returns>
    public int SeriesCount()
    {
        return this.Series.Count;
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="seriesName"> The series name. </param>
    public void Clear( string seriesName )
    {
        if ( !this.Series.IsUniqueName( seriesName ) )
            this.Series[seriesName].Points.Clear();
    }

    /// <summary> Next series name. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <returns>   A <see cref="string" />. </returns>
    public string NextSeriesName()
    {
        return $"Series#{this.SeriesCount() + 1}";
    }

    /// <summary> Adds the series. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="seriesName"> The series name. </param>
    /// <returns> The Series. </returns>
    public Series AddSeries( string seriesName )
    {
        Series ser = new( this.NextSeriesName() );
        if ( this.Series.IsUniqueName( seriesName ) )
        {
            this.Series.Add( ser );
        }
        else
        {
            ser = this.Series[seriesName];
        }

        return ser;
    }

    /// <summary> Adds the series. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="seriesName">  The series name. </param>
    /// <param name="chartArea">   The chart area. </param>
    /// <param name="seriesColor"> The series color. </param>
    /// <returns> The Series. </returns>
    public Series AddSeries( string seriesName, string chartArea, Color seriesColor )
    {
        Series ser = new( this.NextSeriesName() );
        if ( this.Series.IsUniqueName( seriesName ) )
        {
            this.Series.Add( ser );
        }
        else
        {
            ser = this.Series[seriesName];
        }

        ser.ChartArea = chartArea;
        ser.Color = seriesColor;
        return ser;
    }

    /// <summary> Adds the series. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <returns> The Series. </returns>
    public Series AddSeries()
    {
        return this.AddSeries( this.NextSeriesName(), this.LastChartArea().Name, this.NextSeriesColor() );
    }

    /// <summary>   Select series and binds it to the last chart area if not already bound. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="seriesName">   The series name. </param>
    /// <returns>   The Series. </returns>
    public Series SelectSeries( string seriesName )
    {
        if ( string.IsNullOrWhiteSpace( seriesName ) ) throw new ArgumentNullException( nameof( seriesName ) );
        Series series;
        if ( this.Series.IsUniqueName( seriesName ) )
        {
            series = new Series( seriesName );
            this.Series.Add( series );
        }
        else
        {
            series = this.Series[seriesName];
        }

        // Bind to last chart area
        if ( string.IsNullOrWhiteSpace( series.ChartArea ) && (this.ChartAreas.Count > 1) )
            series.ChartArea = this.LastChartArea().Name;

        return series;
    }

    #endregion

    #region " series: bind "

    /// <summary> Bind line series. Call <see cref="Chart.DataBind()"/> to update the chart. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName"> The series name. </param>
    /// <param name="chartType">  Type of the chart. </param>
    /// <param name="points">     The points. </param>
    /// <returns> The Series. </returns>
    public Series BindLineSeries( string seriesName, SeriesChartType chartType, IList<Std.Cartesian.CartesianPoint<double>> points )
    {
        return this.BindLineSeries( this.SelectSeries( seriesName ), chartType, points );
    }

    /// <summary>   Bind line series. Call <see cref="Chart.DataBind()"/> to update the chart. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="series">       The series; must be an existing <see cref="Series"/> that is already bound to a chart area. </param>
    /// <param name="chartType">    Type of the chart. </param>
    /// <param name="points">       The points. </param>
    /// <returns>   The Series. </returns>
    public Series BindLineSeries( Series series, SeriesChartType chartType, IList<Std.Cartesian.CartesianPoint<double>> points )
    {
        if ( series is not object ) throw new ArgumentNullException( nameof( series ) );
        if ( points is not object ) throw new ArgumentNullException( nameof( points ) );
        if ( !this.Series.Contains( series ) ) this.Series.Add( series );
        if ( string.IsNullOrEmpty( series.ChartArea ) ) throw new InvalidOperationException( $" {nameof( series )} is not bound to a chart area" );
        series.Points.Clear();
        series.ChartType = chartType;
        this.DataSource = points;
        series.XValueMember = nameof( cc.isr.Std.Cartesian.CartesianPoint<double>.X );
        series.YValueMembers = nameof( cc.isr.Std.Cartesian.CartesianPoint<double>.Y );
        return series;
    }

    #endregion

    #region " series: draw "

    /// <summary> Adds line series to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName"> The series name. </param>
    /// <param name="chartType">  Type of the chart. </param>
    /// <param name="points">     The points. </param>
    /// <returns> The Series. </returns>
    public Series GraphLineSeries( string seriesName, SeriesChartType chartType, IList<Point> points )
    {
        return this.GraphLineSeries( this.SelectSeries( seriesName ), chartType, points );
    }

    /// <summary>   Adds line series to chart. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="series">       The series; must be an existing <see cref="Series"/> that is
    ///                             already bound to a chart area. </param>
    /// <param name="chartType">    Type of the chart. </param>
    /// <param name="points">       The points. </param>
    /// <returns>   The Series. </returns>
    public Series GraphLineSeries( Series series, SeriesChartType chartType, IList<Point> points )
    {
        if ( series is not object ) throw new ArgumentNullException( nameof( series ) );
        if ( points is not object || !points.Any() ) throw new ArgumentNullException( nameof( points ) );
        if ( !this.Series.Contains( series ) ) this.Series.Add( series );
        series.Points.Clear();
        series.ChartType = chartType;
        this.DataSource = points;
        series.XValueMember = nameof( System.Drawing.Point.X );
        series.YValueMembers = nameof( System.Drawing.Point.Y );
        return series;
    }

    /// <summary> Adds line series to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName">     The series name. </param>
    /// <param name="chartType">      Type of the chart. </param>
    /// <param name="abscissaValues"> The x coordinate values. </param>
    /// <param name="ordinateValues"> The y coordinate values. </param>
    /// <returns> The Series. </returns>
    public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues )
    {
        return abscissaValues is null
            ? throw new ArgumentNullException( nameof( abscissaValues ) )
            : ordinateValues is null
            ? throw new ArgumentNullException( nameof( ordinateValues ) )
            : this.GraphLineSeries( seriesName, chartType, abscissaValues, ordinateValues, false, Color.Empty, true );
    }

    /// <summary> Adds line series to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName">      The series name. </param>
    /// <param name="chartType">       Type of the chart. </param>
    /// <param name="abscissaValues">  The x coordinate values. </param>
    /// <param name="ordinateValues">  The y coordinate values. </param>
    /// <param name="title">           The title. </param>
    /// <param name="abscissaTitle">   The abscissa title. </param>
    /// <param name="ordinateTitle">   The ordinate title. </param>
    /// <param name="addMinMaxLabels"> true to add minimum and maximum labels. </param>
    /// <returns> The Series. </returns>
    public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues, string title, string abscissaTitle, string ordinateTitle, bool addMinMaxLabels )
    {
        if ( abscissaValues is not object ) throw new ArgumentNullException( nameof( abscissaValues ) );
        if ( ordinateValues is not object ) throw new ArgumentNullException( nameof( ordinateValues ) );
        this.AddTitle( title, Docking.Top );
        this.OrdinateTitle( ordinateTitle );
        this.AbscissaTitle( abscissaTitle );
        return this.GraphLineSeries( seriesName, chartType, abscissaValues, ordinateValues, addMinMaxLabels, Color.Empty, true );
    }

    /// <summary> Adds line series to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName">        The series name. </param>
    /// <param name="chartType">         Type of the chart. </param>
    /// <param name="abscissaValues">    The x coordinate values. </param>
    /// <param name="ordinateValues">    The y coordinate values. </param>
    /// <param name="addMinMaxLabels">   true to add minimum and maximum labels. </param>
    /// <param name="seriesColor">       The series color. </param>
    /// <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    /// <returns> The Series. </returns>
    public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues, bool addMinMaxLabels, Color seriesColor, bool isVisibleInLegend )
    {
        return this.GraphLineSeries( this.SelectSeries( seriesName ), chartType, abscissaValues, ordinateValues, addMinMaxLabels, seriesColor, isVisibleInLegend );
    }

    /// <summary>   Adds line series to chart. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="series">               The series; must be an existing <see cref="Series"/> that
    ///                                     is already bound to a chart area. </param>
    /// <param name="chartType">            Type of the chart. </param>
    /// <param name="abscissaValues">       The x coordinate values. </param>
    /// <param name="ordinateValues">       The y coordinate values. </param>
    /// <param name="addMinMaxLabels">      true to add minimum and maximum labels. </param>
    /// <param name="seriesColor">          The series color. </param>
    /// <param name="isVisibleInLegend">    true if this object is visible in legend. </param>
    /// <returns>   The Series. </returns>
    public Series GraphLineSeries( Series series, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues, bool addMinMaxLabels, Color seriesColor, bool isVisibleInLegend )
    {
        if ( series is not object ) throw new ArgumentNullException( nameof( series ) );
        if ( abscissaValues is not object ) throw new ArgumentNullException( nameof( abscissaValues ) );
        if ( ordinateValues is not object ) throw new ArgumentNullException( nameof( ordinateValues ) );
        if ( !this.Series.Contains( series ) ) this.Series.Add( series );
        series.ChartType = chartType;
        series.Points.DataBindXY( abscissaValues, ordinateValues );

        series.IsVisibleInLegend = isVisibleInLegend;
        series.Color = seriesColor;
        if ( addMinMaxLabels ) // Add min, max labels
        {
            this.ApplyPaletteColors(); // Force color assign so labels match
            SmartLabels( series, series.Color, series.Color, FontStyle.Regular, 7 );
            series.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.TopLeft;
            var p = series.Points.FindMaxByValue();
            p.Label = $"Max = {p.YValues[0].ToString( "F1", System.Globalization.CultureInfo.InvariantCulture )}";
            p = series.Points.FindMinByValue();
            p.Label = "Min = " + p.YValues[0].ToString( "F1", System.Globalization.CultureInfo.InvariantCulture );
        }
        return series;
    }

    /// <summary> Graph range series. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName">           The series name. </param>
    /// <param name="abscissaValues">       The x coordinate values. </param>
    /// <param name="ordinateValues">       The y coordinate values. </param>
    /// <param name="ordinateHeightValues"> The ordinate height values. </param>
    /// <param name="seriesColor">          The series color. </param>
    /// <returns> The Series. </returns>
    public Series GraphRangeSeries( string seriesName, double[] abscissaValues, double[] ordinateValues, double[] ordinateHeightValues, Color seriesColor )
    {
        return this.GraphRangeSeries( this.SelectSeries( seriesName ), abscissaValues, ordinateValues, ordinateHeightValues, seriesColor );
    }

    /// <summary>   Graph range series. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="series">               The series; must be an existing <see cref="Series"/> that
    ///                                     is already bound to a chart area. </param>
    /// <param name="abscissaValues">       The x coordinate values. </param>
    /// <param name="ordinateValues">       The y coordinate values. </param>
    /// <param name="ordinateHeightValues"> The ordinate height values. </param>
    /// <param name="seriesColor">          The series color. </param>
    /// <returns>   The Series. </returns>
    public Series GraphRangeSeries( Series series, double[] abscissaValues, double[] ordinateValues, double[] ordinateHeightValues, Color seriesColor )
    {
        if ( series is not object ) throw new ArgumentNullException( nameof( series ) );
        if ( abscissaValues is not object ) throw new ArgumentNullException( nameof( abscissaValues ) );
        if ( ordinateValues is not object ) throw new ArgumentNullException( nameof( ordinateValues ) );
        if ( ordinateHeightValues is not object ) throw new ArgumentNullException( nameof( ordinateHeightValues ) );
        if ( !this.Series.Contains( series ) ) this.Series.Add( series );
        series.ChartType = SeriesChartType.Range;
        series.IsVisibleInLegend = false;
        series.Color = seriesColor;
        series.Points.DataBindXY( abscissaValues, ordinateValues, ordinateHeightValues );
        return series;
    }

    /// <summary> Graphs series points style to chart. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="seriesName">        Name of the series. </param>
    /// <param name="abscissa">          The abscissa values. </param>
    /// <param name="ordinate">          The ordinate values. </param>
    /// <param name="marker">            The marker. </param>
    /// <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    /// <returns> The Series. </returns>
    public Series GraphPoints( string seriesName, double[] abscissa, double[] ordinate, ChartMarker marker, bool isVisibleInLegend )
    {
        return this.GraphPoints( this.SelectSeries( seriesName ), abscissa, ordinate, marker, isVisibleInLegend );
    }

    /// <summary>   Graphs series points style to chart. </summary>
    /// <remarks>   David, 2021-09-02. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="series">               The series. </param>
    /// <param name="abscissa">             The abscissa values. </param>
    /// <param name="ordinate">             The ordinate values. </param>
    /// <param name="marker">               The marker. </param>
    /// <param name="isVisibleInLegend">    true if this object is visible in legend. </param>
    /// <returns>   The Series. </returns>
    public Series GraphPoints( Series series, double[] abscissa, double[] ordinate, ChartMarker marker, bool isVisibleInLegend )
    {
        if ( series is not object ) throw new ArgumentNullException( nameof( series ) );
        if ( abscissa is not object ) throw new ArgumentNullException( nameof( abscissa ) );
        if ( ordinate is not object ) throw new ArgumentNullException( nameof( ordinate ) );
        if ( !this.Series.Contains( series ) ) this.Series.Add( series );
        series.ChartType = SeriesChartType.Point;
        series.IsVisibleInLegend = isVisibleInLegend;
        // ser.MarkerBorderWidth = LineWeight;
        // ser.BorderWidth = LineWeight;
        series.Points.DataBindXY( abscissa, ordinate );
        series.MarkerStyle = marker.Style;
        series.MarkerSize = marker.Size;
        series.MarkerColor = marker.Color;
        return series;
    }

    #endregion

    #region " series smart labels  "

    /// <summary> Setup Smart labels. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="chartingSeries"> The charting series. </param>
    /// <param name="labelColor">     The label color. </param>
    /// <param name="lineColor">      The lineColor. </param>
    /// <param name="fontStyle">      The font style. </param>
    /// <param name="fontSize">       size of the font. </param>
    private static void SmartLabels( Series chartingSeries, Color labelColor, Color lineColor, FontStyle fontStyle, int fontSize )
    {
        if ( chartingSeries is not object ) throw new ArgumentNullException( nameof( chartingSeries ) );
        chartingSeries.LabelForeColor = labelColor;
        chartingSeries.Font = new Font( chartingSeries.Font.Name, fontSize, fontStyle );
        chartingSeries.SmartLabelStyle.Enabled = true;
        chartingSeries.SmartLabelStyle.CalloutLineColor = lineColor;
        chartingSeries.SmartLabelStyle.CalloutStyle = LabelCalloutStyle.None;
        chartingSeries.SmartLabelStyle.IsMarkerOverlappingAllowed = false;
        chartingSeries.SmartLabelStyle.MinMovingDistance = 1d;
        chartingSeries.SmartLabelStyle.IsOverlappedHidden = false;
        chartingSeries.SmartLabelStyle.AllowOutsidePlotArea = LabelOutsidePlotAreaStyle.No;
        chartingSeries.SmartLabelStyle.CalloutLineAnchorCapStyle = LineAnchorCapStyle.None;
    }

    #endregion

    #region " settings "

    /// <summary> Gets or sets a list of colors of the use twenties. </summary>
    /// <value> a list of colors of the use twenties. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseTwentyColors { get; set; }

    /// <summary> The series colors 10. </summary>
    /// <remarks> plot colors (HTTPS://GitHub.com/vega/vega/wiki/Scales#scale-range-literals)</remarks>
    private readonly string[] _seriesColors10 = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"];

    /// <summary> The series colors 20. </summary>
    private readonly string[] _seriesColors20 = ["#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"];

    /// <summary> Next series color. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <returns> a Color. </returns>
    public Color NextSeriesColor()
    {
        string[] colors = this.UseTwentyColors ? this._seriesColors20 : this._seriesColors10;
        return ColorTranslator.FromHtml( colors[this.SeriesCount() % colors.Length] );
    }

    /// <summary> Initializes the chart Palette. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void InitializeCustomPalette()
    {
        this.Palette = ChartColorPalette.None;
        this.PaletteCustomColors = [Color.Blue, Color.DarkRed, Color.OliveDrab, Color.DarkOrange, Color.Purple, Color.Turquoise, Color.Green, Color.Crimson, Color.RoyalBlue, Color.Sienna, Color.Teal, Color.YellowGreen, Color.SandyBrown, Color.DeepSkyBlue, Color.Brown, Color.Cyan];
    }

    #endregion

    #region " title "

    /// <summary>
    /// Add graph title. Repeated calls append new titles if unique or if new position.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value"> The title. </param>
    public void AddTitle( string value )
    {
        // Defaults to top, black
        this.AddTitle( value, Docking.Top, Color.Black );
    }

    /// <summary>
    /// Add graph title. Repeated calls append new titles if unique or if new position.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value">    The title. </param>
    /// <param name="position"> The position. </param>
    public void AddTitle( string value, Docking position )
    {
        this.AddTitle( value, position, Color.Black );
    }

    /// <summary>
    /// Add graph title. Repeated calls append new titles if unique or if new position.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value">      The title. </param>
    /// <param name="position">   The position. </param>
    /// <param name="titleColor"> The title color. </param>
    public void AddTitle( string value, Docking position, Color titleColor )
    {
        foreach ( Title t in this.Titles )
        {
            if ( t.Docking == position )
            {
                if ( !t.Text.Contains( value ) ) // Append
                {
                    t.Text = $"{t.Text}{Environment.NewLine}{value}";
                }

                return;
            }
        }

        // If here, no match, add new title
        Title newtitle = new( value, position );
        this.Titles.Add( newtitle );
        newtitle.ForeColor = titleColor;
        // newtitle.Font = new Font(newtitle.Font.Name, 8, FontStyle.Bold);
    }

    #endregion

    #region " tool tip handler "

    /// <summary> Event handler. Called by chart for tool tip events. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Tool tip event information. </param>
    public void OnGetTooltipText( object? sender, ToolTipEventArgs e )
    {
        if ( e is not object ) return;
        if ( this.ChartArea is not object ) return;
        HitTestResult h = e.HitTestResult;
        if ( this.ChartArea.AxisX.ScaleView.Zoomable )
        {
            switch ( h.ChartElementType )
            {
                case ChartElementType.Axis:
                case ChartElementType.AxisLabels:
                    {
                        e.Text = "Click-drag in graph area to zoom";
                        break;
                    }

                case ChartElementType.ScrollBarZoomReset:
                    {
                        e.Text = "Zoom undo";
                        break;
                    }
            }
        }

        switch ( h.ChartElementType )
        {
            case ChartElementType.DataPoint:
                {
                    e.Text = $"{h.Series.Name}\n{h.Series.Points[h.PointIndex]}";
                    break;
                }
        }
    }

    #endregion

    #region " print "

    /// <summary> Prints a page. </summary>
    /// <remarks>
    /// The Chart control uses a PrintManager object to manage printing, which is encapsulated in the
    /// Chart.Printing property. The PrintManager has several methods that are used for very basic
    /// printing operations, such as PageSetup, PrintPreview and Print. These are pretty self
    /// explanatory, although it should be said that the Print method only prints a picture of the
    /// graph on your paper and nothing else. Although I played around with it a lot, I could not get
    /// the Print method to print very well, if such things like borders, etc. were anything except
    /// very simple. I found the best way to print reports based on the graph was to set up a
    /// PrintPage routine to handle printing, and add a Delegate to handle the specific graphs
    /// PrintPage event. In the PrintPage routine, you can design your report as you like.
    /// </remarks>
    /// <param name="eventArgs"> Print page event information. </param>
    public void PrintPage( PrintPageEventArgs eventArgs )
    {
        if ( eventArgs.Graphics is not object ) return;

        // Create a memory stream to save the chart image    
        using System.IO.MemoryStream stream = new();

        // Save the chart image to the stream    
        this.SaveImage( stream, System.Drawing.Imaging.ImageFormat.Bmp );

        // Draw the bitmap from the stream on the printer graphics
        eventArgs.Graphics.DrawImage( new Bitmap( stream ), eventArgs.MarginBounds );
    }

    #endregion

    #region " axes labels "

    /// <summary>
    /// The array Round Mantissa defines bins for the value of a, from which nice round values are
    /// provided for major and minor tick intervals. For example, the first bin is for a = 1.0, and
    /// from that, we get the interval values of 0.20 and 0.05. The second bin is for a in (1.0,1.2],
    /// and from that we again get the interval values of 0.20 and 0.05. The algorithm is easily
    /// customized by changing the bin and interval values in these arrays.
    /// </summary>
    /// <value> The round mantissa. </value>
    private readonly double[] _roundMantissa = [1.0d, 1.2d, 1.4d, 1.6d, 1.8d, 2.0d, 2.5d, 3.0d, 4.0d, 5.0d, 6.0d, 8.0d, 10.0d];

    /// <summary> Gets or sets the round interval major. </summary>
    /// <value> The round interval major. </value>
    private readonly double[] _roundIntervalMajor = [0.2d, 0.2d, 0.2d, 0.2d, 0.2d, 0.5d, 0.5d, 0.5d, 0.5d, 1.0d, 1.0d, 2.0d, 2.0d];

    /// <summary> The round interval minor. </summary>
    /// <value> The round interval minor. </value>
    private readonly double[] _roundIntervalMinor = [0.05d, 0.05d, 0.05d, 0.05d, 0.05d, 0.1d, 0.1d, 0.1d, 0.1d, 0.2d, 0.2d, 0.5d, 0.5d];

    /// <summary>
    /// Gets nice round numbers for the axes. For the horizontal axis, minValue is always 0.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="minValue">      [in,out] The minimum value. </param>
    /// <param name="maxValue">      [in,out] The maximum value. </param>
    /// <param name="majorInterval"> [in,out] The major interval. </param>
    /// <param name="minorInterval"> [in,out] The minor interval. </param>
    public void GetNiceRoundNumbers( ref double minValue, ref double maxValue, ref double majorInterval, ref double minorInterval )
    {
        double min = Math.Min( minValue, maxValue );
        double max = Math.Max( minValue, maxValue );
        double delta = max - min; // The full range
                                  // Special handling for zero full range
        if ( delta == 0d )
        {
            // When min == max == 0, choose arbitrary range of 0 - 1
            if ( min == 0d )
            {
                minValue = 0d;
                maxValue = 1d;
                majorInterval = 0.2d;
                minorInterval = 0.5d;
                return;
            }
            // min == max, but not zero, so set one to zero
            if ( min < 0d )
            {
                max = 0d; // min-max are -|min| to 0
            }
            else
            {
                min = 0d;
            } // min-max are 0 to +|max|

            delta = max - min;
        }

        int n = ChartingExtensions.Methods.Base10Exponent( delta );
        double tenToN = Math.Pow( 10d, n );
        double a = delta / tenToN;
        // At this point delta = a x Exp10(n), where
        // 1.0 <= a < 10.0 and n = integer exponent value
        // Now, based on a select a nice round interval and maximum value
        for ( int i = 0, loopTo = this._roundMantissa.Length - 1; i <= loopTo; i++ )
        {
            if ( a <= this._roundMantissa[i] )
            {
                majorInterval = this._roundIntervalMajor[i] * tenToN;
                minorInterval = this._roundIntervalMinor[i] * tenToN;
                break;
            }
        }

        minValue = majorInterval * Math.Floor( min / majorInterval );
        maxValue = majorInterval * Math.Ceiling( max / majorInterval );
    }

    #endregion

    #region " line chart "

    /// <summary>   Sets up the line chart. </summary>
    /// <remarks>   David, 2021-09-01. </remarks>
    /// <param name="seriesName">       The series name. </param>
    /// <param name="abscissaMin">      The abscissa minimum. </param>
    /// <param name="ordinateMin">      The ordinate minimum. </param>
    /// <param name="abscissaMax">      The abscissa maximum. </param>
    /// <param name="ordinateMax">      The ordinate maximum. </param>
    /// <param name="abscissaTitle">    The abscissa title. </param>
    /// <param name="ordinateTitle">    The ordinate title. </param>
    /// <returns>   The Series. </returns>
    public Series SetupLineChartChart( string seriesName, double abscissaMin, double ordinateMin, double abscissaMax, double ordinateMax, string abscissaTitle, string ordinateTitle )
    {
        this.Tag = new ChartScaleData( this );
        this.Series.Clear();
        Series s = new( seriesName )
        {
            IsVisibleInLegend = true,
            IsXValueIndexed = false,
            ChartType = SeriesChartType.FastLine,
            BorderWidth = 2,
            MarkerStyle = MarkerStyle.None,
            MarkerBorderWidth = 3,
            MarkerStep = 1,
            MarkerSize = 10
        };
        this.Series.Add( s );
        _ = s.Points.AddXY( abscissaMin, ordinateMin );

        double xInt, xMinInt, yInt, yMinInt;
        yMinInt = 0d;
        yInt = yMinInt;
        xMinInt = yInt;
        xInt = xMinInt;
        if ( this.UseNiceRoundNumbers )
        {
            this.GetNiceRoundNumbers( ref abscissaMin, ref abscissaMax, ref xInt, ref xMinInt );
            this.GetNiceRoundNumbers( ref ordinateMin, ref ordinateMax, ref yInt, ref yMinInt );
        }

        ChartArea area = this.ChartAreas.First();
        area.AxisX.Title = abscissaTitle;
        area.AxisX.Minimum = abscissaMin;
        area.AxisX.Maximum = abscissaMax;
        area.AxisX.Interval = xInt;
        area.AxisX.MinorTickMark.Interval = xMinInt;
        area.AxisX.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
        area.AxisX.MinorTickMark.Enabled = true;
        area.AxisX.MinorTickMark.Size = area.AxisX.MajorTickMark.Size / 2f;

        area.AxisY.Title = ordinateTitle;
        area.AxisY.Minimum = ordinateMin;
        area.AxisY.Maximum = ordinateMax;
        area.AxisY.Interval = yInt;
        area.AxisY.MinorTickMark.Interval = yMinInt;
        area.AxisY.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
        area.AxisY.MinorTickMark.Enabled = true;
        area.AxisY.MinorTickMark.Size = area.AxisY.MajorTickMark.Size / 2f;
        (this.Tag as ChartScaleData)?.UpdateAxisBaseData();
        ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );

        return s;
    }

    #endregion

    #region " zoom "

    /// <summary> Gets or sets or set the flag for Using GDI32 for zooming. </summary>
    /// <value> The use GDI 32. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseGdi32 { get; set; } = true;

    /// <summary>
    /// Gets or sets the flag for using nice round numbers based on the <see cref="_roundMantissa"/>
    /// values.
    /// </summary>
    /// <value> The use nice round numbers. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseNiceRoundNumbers { get; set; } = true;

    /// <summary> Gets or sets the axis label format. </summary>
    /// <value> The axis label format. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public AxisLabelFormat AxisLabelFormat { get; set; } = AxisLabelFormat.Smart;

    /// <summary> Sets up the chart zoom example. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void SetupChartZoomExample()
    {
        this.Tag = new ChartScaleData( this );
        this.Series.Clear();
        int iMax = 5 * 360;
        double xMax, yMax, xMin, yMin;
        yMin = 0d;
        xMin = yMin;
        yMax = xMin;
        xMax = yMax;
        int jMin = -3;
        int jMax = 3;
        double piDiv180 = Math.PI / 180d;
        for ( int j = jMin, loopTo = jMax; j <= loopTo; j++ )
        {
            Series s = new( $"exp {j}" )
            {
                IsVisibleInLegend = true,
                IsXValueIndexed = false,
                ChartType = SeriesChartType.FastLine,
                BorderWidth = 2,
                MarkerStyle = ( MarkerStyle ) (j - jMin + 1), // 0 = MarkerStyle.None
                MarkerBorderWidth = 3,
                MarkerStep = 1,
                MarkerSize = 10
            };
            this.Series.Add( s );
            for ( int i = 0, loopTo1 = iMax - 1; i <= loopTo1; i++ )
            {
                double mag = Math.Exp( 0.5d * (j * i) / iMax );
                double y = mag * Math.Sin( i * piDiv180 );
                if ( i == 0 && j == 0 )
                {
                    xMax = i;
                    xMin = xMax;
                    yMax = y;
                    yMin = yMax;
                }
                else
                {
                    if ( y < yMin )
                        yMin = y;
                    if ( y > yMax )
                        yMax = y;
                    if ( i < xMin )
                        xMin = i;
                    if ( i > xMax )
                        xMax = i;
                }

                _ = s.Points.AddXY( i, y );
            }
        }

        double xInt, xMinInt, yInt, yMinInt;
        yMinInt = 0d;
        yInt = yMinInt;
        xMinInt = yInt;
        xInt = xMinInt;
        if ( this.UseNiceRoundNumbers )
        {
            this.GetNiceRoundNumbers( ref xMin, ref xMax, ref xInt, ref xMinInt );
            this.GetNiceRoundNumbers( ref yMin, ref yMax, ref yInt, ref yMinInt );
        }

        ChartArea area = this.ChartAreas.First();
        area.AxisX.Minimum = xMin;
        area.AxisX.Maximum = xMax;
        area.AxisX.Interval = xInt;
        area.AxisX.MinorTickMark.Interval = xMinInt;
        area.AxisX.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
        area.AxisX.MinorTickMark.Enabled = true;
        area.AxisX.MinorTickMark.Size = area.AxisX.MajorTickMark.Size / 2f;
        area.AxisY.Minimum = yMin;
        area.AxisY.Maximum = yMax;
        area.AxisY.Interval = yInt;
        area.AxisY.MinorTickMark.Interval = yMinInt;
        area.AxisY.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
        area.AxisY.MinorTickMark.Enabled = true;
        area.AxisY.MinorTickMark.Size = area.AxisY.MajorTickMark.Size / 2f;
        (this.Tag as ChartScaleData)!.UpdateAxisBaseData();
        ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
    }

    /// <summary>
    /// Toggles handling the dashed zoom rectangle when the mouse is dragged over a chart with the
    /// CTRL key pressed. The MouseDown, MouseMove and MouseUp events handle the creation and drawing
    /// of the Zoom Rectangle.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="addHandlerToggle"> True to add handler toggle. </param>
    public void ToggleDashedZoomRectangleMouseHandlers( bool addHandlerToggle )
    {
        if ( addHandlerToggle )
        {
            MouseDown += this.HandleDashedZoomRectangleMouseDown;
            MouseMove += this.HandleDashedZoomRectangleMouseMove;
            MouseUp += this.HandleDashedZoomRectangleMouseUp;
        }
        else
        {
            MouseDown -= this.HandleDashedZoomRectangleMouseDown;
            MouseMove -= this.HandleDashedZoomRectangleMouseMove;
            MouseUp -= this.HandleDashedZoomRectangleMouseUp;
        }
    }

    /// <summary> The zoom rectangle. </summary>
    private Rectangle _zoomRect;

    /// <summary> Indicate that we are dragging. </summary>
    private bool _zoomingNow;

    /// <summary> Handles the dashed zoom rectangle mouse down. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Mouse event information. </param>
    private void HandleDashedZoomRectangleMouseDown( object? sender, MouseEventArgs e )
    {
        if ( LicenseManager.UsageMode == LicenseUsageMode.Designtime )
            return;
        if ( sender is not Chart chart )
            return;
        _ = this.Focus();

        // Test for CTRL+Left Single Click to start displaying selection box
        if ( e.Button == MouseButtons.Left && e.Clicks == 1 && (ModifierKeys & Keys.Control) != 0 )
        {
            this._zoomingNow = true;
            this._zoomRect.Location = e.Location;
            this._zoomRect.Height = 0;
            this._zoomRect.Width = this._zoomRect.Height;
            // Draw the new selection rectangle
            ChartingExtensions.Methods.DrawZoomRect( chart, this._zoomRect, this.UseGdi32 );
        }

        _ = this.Focus();
    }

    /// <summary> Handles the dashed zoom rectangle mouse move. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Mouse event information. </param>
    private void HandleDashedZoomRectangleMouseMove( object? sender, MouseEventArgs e )
    {
        if ( sender is not Chart chart )
            return;
        if ( this._zoomingNow )
        {
            // Redraw the old selection rectangle, which erases it
            ChartingExtensions.Methods.DrawZoomRect( chart, this._zoomRect, this.UseGdi32 );
            this._zoomRect.Width = e.X - this._zoomRect.Left;
            this._zoomRect.Height = e.Y - this._zoomRect.Top;
            // Draw the new selection rectangle
            ChartingExtensions.Methods.DrawZoomRect( chart, this._zoomRect, this.UseGdi32 );
        }
    }

    /// <summary> Handles the dashed zoom rectangle mouse up. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Mouse event information. </param>
    private void HandleDashedZoomRectangleMouseUp( object? sender, MouseEventArgs e )
    {
        if ( sender is Chart chart && this._zoomingNow && e.Button == MouseButtons.Left )
        {
            // Redraw the selection rectangle, which erases it
            ChartingExtensions.Methods.DrawZoomRect( chart, this._zoomRect, this.UseGdi32 );
            if ( this._zoomRect.Width != 0 && this._zoomRect.Height != 0 )
            {
                // Just in case the selection was dragged from lower right to upper left
                this._zoomRect = new Rectangle( Math.Min( this._zoomRect.Left, this._zoomRect.Right ), Math.Min( this._zoomRect.Top, this._zoomRect.Bottom ), Math.Abs( this._zoomRect.Width ), Math.Abs( this._zoomRect.Height ) );
                // no Shift so Zoom in.
                if ( chart.Tag is ChartScaleData chartScaleData )
                    this.ZoomInToZoomRect( chartScaleData, chart.ChartAreas.First() );
            }

            this._zoomingNow = false;
        }
    }

    /// <summary>
    /// This method zooms in to the area contained within the portion of zoomRect that overlaps the
    /// chart innerPlotRectangle. zoomRect is positioned in the client rectangle of the chart in
    /// which the zoom was initiated with a Left MouseDown event.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="chartScaleData"> Information describing the chart scale. </param>
    /// <param name="area">           The area. </param>
    private void ZoomInToZoomRect( ChartScaleData chartScaleData, ChartArea area )
    {
        if ( this._zoomRect.Width == 0 || this._zoomRect.Height == 0 )
        {
            return;
        }

        Rectangle r = this._zoomRect;

        // Get overlap of zoomRect and the innerPlotRectangle
        Rectangle ipr = chartScaleData.InnerPlotRectangle;
        if ( !r.IntersectsWith( ipr ) )
        {
            return;
        }

        r.Intersect( ipr );
        if ( !chartScaleData.IsZoomed )
        {
            chartScaleData.IsZoomed = true;
            chartScaleData.UpdateAxisBaseData();
        }

        this.SetZoomAxisScale( area, area.AxisX, r.Left, r.Right );
        this.SetZoomAxisScale( area, area.AxisY, r.Bottom, r.Top );
    }

    /// <summary>
    /// Sets the axis parameters to an non-zoomed state showing all data, and nice round numbers and
    /// intervals for the axis labels Note: at this stage the axis is unchanged, and this has not
    /// been tested with reversed axes.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="area">  The area. </param>
    /// <param name="axis">  . </param>
    /// <param name="pxLow"> low pixel position in chart client rectangle coordinates. </param>
    /// <param name="pxHi">  Hi pixel position in chart client rectangle coordinates. </param>
    private void SetZoomAxisScale( ChartArea area, Axis axis, int pxLow, int pxHi )
    {
        double minValue = Math.Max( axis.Minimum, axis.PixelPositionToValue( pxLow ) );
        double maxValue = Math.Min( axis.Maximum, axis.PixelPositionToValue( pxHi ) );
        double axisInterval = 0d;
        double axisIntMinor = 0d;
        if ( this.UseNiceRoundNumbers )
        {
            this.GetNiceRoundNumbers( ref minValue, ref maxValue, ref axisInterval, ref axisIntMinor );
        }
        else
        {
            axisInterval = (maxValue - minValue) / 5.0d;
        }

        axis.Minimum = minValue;
        axis.Maximum = maxValue;
        axis.Interval = axisInterval;
        axis.MinorTickMark.Interval = axisIntMinor;
        ChartingExtensions.Methods.SetAxisFormats( area, this.AxisLabelFormat );
    }

    #endregion

    #region " context menu "

    private ToolStripMenuItem? _zoomOutToolStripMenuItem;

    private ToolStripMenuItem? ZoomOutToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._zoomOutToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._zoomOutToolStripMenuItem is not null )
            {
                this._zoomOutToolStripMenuItem.Click -= this.ZoomOutToolStripMenuItem_Click;
            }

            this._zoomOutToolStripMenuItem = value;
            if ( this._zoomOutToolStripMenuItem is not null )
            {
                this._zoomOutToolStripMenuItem.Click += this.ZoomOutToolStripMenuItem_Click;
            }
        }
    }

    private ToolStripSeparator? _separatorToolStripItem;

    private ToolStripMenuItem? _niceRoundNumbersToolStripMenuItem;

    private ToolStripMenuItem? NiceRoundNumbersToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._niceRoundNumbersToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._niceRoundNumbersToolStripMenuItem is not null )
            {
                this._niceRoundNumbersToolStripMenuItem.Click -= this.NiceRoundNumbersToolStripMenuItem_Click;
            }

            this._niceRoundNumbersToolStripMenuItem = value;
            if ( this._niceRoundNumbersToolStripMenuItem is not null )
            {
                this._niceRoundNumbersToolStripMenuItem.Click += this.NiceRoundNumbersToolStripMenuItem_Click;
            }
        }
    }

    private ToolStripMenuItem? _zoomWithGDI32ToolStripMenuItem;

    private ToolStripMenuItem? ZoomWithGDI32ToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._zoomWithGDI32ToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._zoomWithGDI32ToolStripMenuItem is not null )
            {
                this._zoomWithGDI32ToolStripMenuItem.Click -= this.ZoomWithGDI32ToolStripMenuItem_Click;
            }

            this._zoomWithGDI32ToolStripMenuItem = value;
            if ( this._zoomWithGDI32ToolStripMenuItem is not null )
            {
                this._zoomWithGDI32ToolStripMenuItem.Click += this.ZoomWithGDI32ToolStripMenuItem_Click;
            }
        }
    }

    private ToolStripSeparator? _separatorToolStripItem1;

    private ToolStripMenuItem? _defaultFormatToolStripMenuItem;

    private ToolStripMenuItem? DefaultFormatToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._defaultFormatToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._defaultFormatToolStripMenuItem is not null )
            {
                this._defaultFormatToolStripMenuItem.Click -= this.DefaultFormatToolStripMenuItem_Click;
            }

            this._defaultFormatToolStripMenuItem = value;
            if ( this._defaultFormatToolStripMenuItem is not null )
            {
                this._defaultFormatToolStripMenuItem.Click += this.DefaultFormatToolStripMenuItem_Click;
            }
        }
    }

    private ToolStripMenuItem? _smartFormatToolStripMenuItem;

    private ToolStripMenuItem? SmartFormatToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._smartFormatToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._smartFormatToolStripMenuItem is not null )
            {
                this._smartFormatToolStripMenuItem.Click -= this.SmartFormatToolStripMenuItem_Click;
            }

            this._smartFormatToolStripMenuItem = value;
            if ( this._smartFormatToolStripMenuItem is not null )
            {
                this._smartFormatToolStripMenuItem.Click += this.SmartFormatToolStripMenuItem_Click;
            }
        }
    }

    private ToolStripMenuItem? _wholeNumberFormatToolStripMenuItem;

    private ToolStripMenuItem? WholeNumberFormatToolStripMenuItem
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._wholeNumberFormatToolStripMenuItem;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._wholeNumberFormatToolStripMenuItem is not null )
            {
                this._wholeNumberFormatToolStripMenuItem.Click -= this.WholeNumberFormatToolStripMenuItem_Click;
            }

            this._wholeNumberFormatToolStripMenuItem = value;
            if ( this._wholeNumberFormatToolStripMenuItem is not null )
            {
                this._wholeNumberFormatToolStripMenuItem.Click += this.WholeNumberFormatToolStripMenuItem_Click;
            }
        }
    }

    /// <summary> Populate context menu. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void PopulateContextMenu()
    {
        this.ZoomOutToolStripMenuItem = new ToolStripMenuItem() { Text = "Zoom Out", CheckOnClick = true };
        this.NiceRoundNumbersToolStripMenuItem = new ToolStripMenuItem() { Text = "Nice Round Numbers", CheckOnClick = true };
        this.ZoomWithGDI32ToolStripMenuItem = new ToolStripMenuItem() { Text = "Zoom /w GDI", CheckOnClick = true };
        this.DefaultFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Default Format", CheckOnClick = true };
        this.SmartFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Smart Format", CheckOnClick = true };
        this.WholeNumberFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Whole Number Format", CheckOnClick = true };
        this._separatorToolStripItem = new ToolStripSeparator();
        this._separatorToolStripItem1 = new ToolStripSeparator();
        this.ContextMenuStrip = new ContextMenuStrip();
        this.ContextMenuStrip.Items.AddRange( [this.ZoomOutToolStripMenuItem, this._separatorToolStripItem, this.NiceRoundNumbersToolStripMenuItem, this.ZoomWithGDI32ToolStripMenuItem, this._separatorToolStripItem1, this.DefaultFormatToolStripMenuItem, this.SmartFormatToolStripMenuItem, this.WholeNumberFormatToolStripMenuItem] );
    }

    /// <summary> Toggle context menu. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="enabled"> True to enable, false to disable. </param>
    public void ToggleContextMenu( bool enabled )
    {
        if ( this.ContextMenuStrip is null ) return;
        if ( enabled )
        {
            this.ContextMenuStrip.Opening += this.HandleContextMenuStripOpening;
        }
        else
        {
            this.ContextMenuStrip.Opening -= this.HandleContextMenuStripOpening;
        }
    }

    /// <summary> Handles the context menu strip opening. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void HandleContextMenuStripOpening( object? sender, CancelEventArgs e )
    {
        if ( this._zoomingNow )
        {
            e.Cancel = true;
            return;
        }
        // controls are not null on context strip opening.
        this.ZoomOutToolStripMenuItem!.Visible = (this.Tag as ChartScaleData)!.IsZoomed;
        this._separatorToolStripItem!.Visible = this.ZoomOutToolStripMenuItem.Visible;
        this.ZoomWithGDI32ToolStripMenuItem!.Checked = this.UseGdi32;
        this.NiceRoundNumbersToolStripMenuItem!.Checked = this.UseNiceRoundNumbers;
        this.DefaultFormatToolStripMenuItem!.Checked = this.AxisLabelFormat == AxisLabelFormat.None;
        this.SmartFormatToolStripMenuItem!.Checked = this.AxisLabelFormat == AxisLabelFormat.Smart;
        this.WholeNumberFormatToolStripMenuItem!.Checked = this.AxisLabelFormat == AxisLabelFormat.WholeNumber;
    }

    /// <summary> Zoom out tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ZoomOutToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        (this.Tag as ChartScaleData)!.ResetAxisScale();
        (this.Tag as ChartScaleData)!.IsZoomed = false;
    }

    /// <summary> Nice round numbers tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void NiceRoundNumbersToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        this.UseNiceRoundNumbers = this.NiceRoundNumbersToolStripMenuItem?.Checked ?? true;
        this.SetupChartZoomExample();
    }

    /// <summary> Zoom with GDI 32 tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ZoomWithGDI32ToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        this.UseGdi32 = this.ZoomWithGDI32ToolStripMenuItem?.Checked ?? true;
    }

    /// <summary> Whole number format tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void WholeNumberFormatToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        this.DefaultFormatToolStripMenuItem!.Checked = false;
        this.SmartFormatToolStripMenuItem!.Checked = false;
        this.WholeNumberFormatToolStripMenuItem!.Checked = true;
        this.AxisLabelFormat = AxisLabelFormat.WholeNumber;
        ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
    }

    /// <summary> Smart format tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void SmartFormatToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        this.DefaultFormatToolStripMenuItem!.Checked = false;
        this.SmartFormatToolStripMenuItem!.Checked = true;
        this.WholeNumberFormatToolStripMenuItem!.Checked = false;
        this.AxisLabelFormat = AxisLabelFormat.Smart;
        ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
    }

    /// <summary> Default format tool strip menu item click. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void DefaultFormatToolStripMenuItem_Click( object? sender, EventArgs e )
    {
        this.DefaultFormatToolStripMenuItem!.Checked = true;
        this.SmartFormatToolStripMenuItem!.Checked = false;
        this.WholeNumberFormatToolStripMenuItem!.Checked = false;
        this.AxisLabelFormat = AxisLabelFormat.None;
        ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
    }

    #endregion
}
/// <summary> a chart marker. </summary>
/// <remarks> David, 2020-10-20. </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-10-20. </remarks>
/// <param name="style"> The style. </param>
/// <param name="size">  The size. </param>
/// <param name="color"> The color. </param>
public struct ChartMarker( MarkerStyle style, int size, Color color )
{
    /// <summary> Gets or sets the size in points. </summary>
    /// <value> The size. </value>
    public int Size { get; set; } = size;

    /// <summary> Gets or sets the style. </summary>
    /// <value> The style. </value>
    public MarkerStyle Style { get; set; } = style;

    /// <summary> Gets or sets the color. </summary>
    /// <value> The color. </value>
    public Color Color { get; set; } = color;
}
/// <summary> Values that represent axis label formats. </summary>
/// <remarks> David, 2020-10-20. </remarks>
public enum AxisLabelFormat
{
    /// <summary> An enum constant representing the none option. </summary>
    [Description( "None, String Empty" )]
    None,

    /// <summary> An enum constant representing the whole number option. </summary>
    [Description( "Whole number (F0)" )]
    WholeNumber,

    /// <summary> An enum constant representing the smart option. </summary>
    [Description( "Smart format" )]
    Smart
}
