using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace cc.isr.Visuals.Charting;
/// <summary>
/// Unsafe native GDI32 methods. P/Invoke method declarations which are harmless for any code to
/// call.
/// </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-06-18 </para>
/// </remarks>
public sealed class UnsafeNativeMethods
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    private UnsafeNativeMethods()
    {
    }

    /// <summary> Values that represent drawing modes. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public enum DrawingMode
    {
        /// <summary> An enum constant representing the none option. </summary>
        None,

        /// <summary> An enum constant representing the 2 notxorpen option. </summary>
        R2_NOTXORPEN = 10
    }

    /// <summary> Rectangles. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="hDC">    The device-context. </param>
    /// <param name="left">   The left. </param>
    /// <param name="top">    The top. </param>
    /// <param name="right">  The right. </param>
    /// <param name="bottom"> The bottom. </param>
    /// <returns> A <see cref="bool" /> </returns>
    [DllImport("gdi32.dll")]
    internal static extern bool Rectangle(IntPtr hDC, int left, int top, int right, int bottom);

    /// <summary> Sets rop 2. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="hDC">        The device-context. </param>
    /// <param name="fnDrawMode"> The draw mode. </param>
    /// <returns> An Integer. </returns>
    [DllImport("gdi32.dll")]
    internal static extern int SetROP2(IntPtr hDC, int fnDrawMode);

    /// <summary> Move to exception. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="hDC"> Handle to the device-context. </param>
    /// <param name="x">   The x coordinate. </param>
    /// <param name="y">   The y coordinate. </param>
    /// <param name="p">   [in,out] A Point to process. </param>
    /// <returns> A <see cref="bool" /> </returns>
    [DllImport("gdi32.dll")]
    internal static extern bool MoveToEx(IntPtr hDC, int x, int y, ref Point p);

    /// <summary> Line to. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="hDC"> Handle to the device-context. </param>
    /// <param name="x">   The x coordinate. </param>
    /// <param name="y">   The y coordinate. </param>
    /// <returns> A <see cref="bool" /> </returns>
    [DllImport("gdi32.dll")]
    internal static extern bool LineTo(IntPtr hDC, int x, int y);

    /// <summary> Creates a pen. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="fnPenStyle"> The pen style. </param>
    /// <param name="nWidth">     The width. </param>
    /// <param name="crColor">    The pen color. </param>
    /// <returns> The new pen. </returns>
    [DllImport("gdi32.dll")]
    internal static extern IntPtr CreatePen(int fnPenStyle, int nWidth, int crColor);

    /// <summary> Select object. </summary>
    /// <remarks>
    /// Selects an object into the specified device context (DC). The new object replaces the
    /// previous object of the same type.
    /// </remarks>
    /// <param name="hDC">  The device-context. </param>
    /// <param name="hObj"> The object. </param>
    /// <returns> An IntPtr. </returns>
    [DllImport("gdi32.dll")]
    internal static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObj);

    /// <summary> Deletes the object described by hObject. </summary>
    /// <remarks>
    /// Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
    /// associated with the object. After the object has been deleted, the specified handle is no
    /// longer valid.
    /// </remarks>
    /// <param name="hObj"> The object. </param>
    /// <returns> A <see cref="bool" /> </returns>
    [DllImport("gdi32.dll")]
    internal static extern bool DeleteObject(IntPtr hObj);

    /// <summary> The null point. </summary>
    private static Point nullPoint = new Point(0, 0);

    /// <summary> ARGB from .NET to a GDI32 RGB. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="rgb"> The RGB. </param>
    /// <returns> An Integer. </returns>
    private static int ArgbToRGB(int rgb)
    {
        return rgb >> 16 & 0xFF | rgb & 0xFF00 | rgb << 16 & 0xFF0000;
    }

    /// <summary> Draw exclusive-or rectangle. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="graphics">  The graphics. </param>
    /// <param name="pen">       The pen. </param>
    /// <param name="rectangle"> The rectangle. </param>
    internal static void DrawXorRectangle(Graphics graphics, Pen pen, Rectangle rectangle)
    {
        if (graphics is null) throw new ArgumentNullException(nameof(graphics));
        if (pen is null) throw new ArgumentNullException(nameof(pen));
        var hDC = graphics.GetHdc();
        var hPen = CreatePen((int)pen.DashStyle, (int)pen.Width, ArgbToRGB(pen.Color.ToArgb()));
        SelectObject(hDC, hPen);
        SetROP2(hDC, (int)DrawingMode.R2_NOTXORPEN);
        Rectangle(hDC, rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
        DeleteObject(hPen);
        graphics.ReleaseHdc(hDC);
    }

    /// <summary> Draw exclusive-or line. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="graphics"> The graphics. </param>
    /// <param name="pen">      The pen. </param>
    /// <param name="x1">       The first x value. </param>
    /// <param name="y1">       The first y value. </param>
    /// <param name="x2">       The second x value. </param>
    /// <param name="y2">       The second y value. </param>
    internal static void DrawXorLine(Graphics graphics, Pen pen, int x1, int y1, int x2, int y2)
    {
        if (graphics is null) throw new ArgumentNullException(nameof(graphics));
        if (pen is null) throw new ArgumentNullException(nameof(pen));
        var hDC = graphics.GetHdc();
        var hPen = CreatePen((int)pen.DashStyle, (int)pen.Width, ArgbToRGB(pen.Color.ToArgb()));
        SelectObject(hDC, hPen);
        SetROP2(hDC, (int)DrawingMode.R2_NOTXORPEN);
        MoveToEx(hDC, x1, y1, ref nullPoint);
        LineTo(hDC, x2, y2);
        DeleteObject(hPen);
        graphics.ReleaseHdc(hDC);
    }
}
