using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace cc.isr.Visuals.Charting.ChartingExtensions;

/// <summary> Includes extensions for charts. </summary>
/// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-02-22, 1.0.5168.x. </para></remarks>
public static class Methods
{
    #region " chart points "

    /// <summary> Remove all values from a collection. </summary>
    /// <remarks>
    /// Same as clear. Is preferred in some collections such as the collection of points in a chart
    /// where clear is slow.
    /// </remarks>
    /// <param name="list"> The collection. </param>
    public static void RemoveAll<T>( this IList<T> list )
    {
        while ( list?.Count > 0 )
            list.RemoveAt( list.Count - 1 );
    }

    /// <summary> Fast clear. </summary>
    /// <remarks>
    /// From Code Project Speedup MS Chart Clear Data Points by Code Artist, 30 Apr 2012,
    /// http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points.
    /// </remarks>
    /// <param name="points"> The points. </param>
    public static void FastClear( this DataPointCollection points )
    {
        if ( points is object && points.Count > 0 )
        {
            points.SuspendUpdates();
            points.RemoveAll();
            points.ResumeUpdates();
            // This ensures the chart axis updates correctly on next plot. 
            // Without adding this line, previous axis settings is used  
            // when plotting new data points right after calling Fast Clear.
            points.Clear();
        }
    }

    #endregion

    #region " number fomrat "

    /// <summary>
    /// Base 10 exponent returns the integer exponent (N) that would yield a number of the form A x
    /// 10N, where Absolute value of A greater or equal to 1 and less than 10.0.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <param name="value"> Number of. </param>
    /// <returns> An Integer. </returns>
    public static int Base10Exponent( this double value )
    {
        return value == 0d ? -int.MaxValue : Convert.ToInt32( Math.Floor( Math.Log10( Math.Abs( value ) ) ) );
    }

    /// <summary>
    /// Returns a consistent format string with minimum necessary precision for a range with
    /// intervals. The format string should distinguish between two values spaced by
    /// <paramref name="interval"/> and still can go as high as the <paramref name="maximum"/>.
    /// </summary>
    /// <remarks>
    /// From https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart.
    /// </remarks>
    /// <param name="interval">    The interval. </param>
    /// <param name="minimum">     The minimum value. </param>
    /// <param name="maximum">     The maximum value. </param>
    /// <param name="extraDigits"> The extra digits. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string RangeFormatString( this double interval, double minimum, double maximum, int extraDigits )
    {
        if ( interval == 0d )
            interval = 0.1d * (maximum - minimum);

        // get the precision to which must show decimal
        int minE = interval.Base10Exponent();

        // get the maximum absolute value
        double maxAbsVal = Math.Max( Math.Abs( minimum ), Math.Abs( maximum ) );

        // get the maximum value precision
        int maxE = maxAbsVal.Base10Exponent();

        // (maxE - minE + 1) is the number of significant digits needed to distinguish two numbers spaced by "interval"
        if ( maxE is < (-4) or > 3 )
        {
            // "E#" format displays 1 digit to the left of the decimal place, and #
            // digits to the right of the decimal place, so # = maxE - minE.
            return $"E{extraDigits + maxE - minE}";
        }
        else
        {
            // In fixed format, since all digits to the left of the decimal place are
            // displayed by default, for "F#" format, # = -minE or zero, whichever is greater.
            return $"F{extraDigits + Math.Max( 0, -minE )}";
        }
    }

    #endregion

    #region " area "

    /// <summary> Sets the axis formats. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="area">                 The area. </param>
    /// <param name="axisLabelFormatStyle"> The axis label format style. </param>
    public static void SetAxisFormats( this ChartArea area, AxisLabelFormat axisLabelFormatStyle )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( area, nameof( area ) );
#else
        if ( area is null ) throw new ArgumentNullException( nameof( area ) );
#endif
        area.AxisX.SetAxisFormat( axisLabelFormatStyle );
        area.AxisY.SetAxisFormat( axisLabelFormatStyle );
    }

    /// <summary> Sets the axis format. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="axis">                 The axis. </param>
    /// <param name="axisLabelFormatStyle"> The axis label format style. </param>
    public static void SetAxisFormat( this Axis axis, AxisLabelFormat axisLabelFormatStyle )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( axis, nameof( axis ) );
#else
        if ( axis is null ) throw new ArgumentNullException( nameof( axis ) );
#endif
        axis.LabelStyle.Format = axisLabelFormatStyle == AxisLabelFormat.WholeNumber
            ? "F0"
            : axisLabelFormatStyle == AxisLabelFormat.Smart ? axis.Interval.RangeFormatString( axis.Minimum, axis.Maximum, 0 ) : string.Empty;
    }

    /// <summary>
    /// Draws an XOR selection rectangle. Zoom Rectangle is positioned in the client rectangle of the
    /// chart in which the zoom was initiated with a Left MouseDown event.
    /// </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="chart">         The chart. </param>
    /// <param name="zoomRectangle"> The zoom rectangle. </param>
    /// <param name="usingGdi">      True to using GDI. </param>
    public static void DrawZoomRect( this Chart chart, Rectangle zoomRectangle, bool usingGdi )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( chart, nameof( chart ) );
#else
        if ( chart is null ) throw new ArgumentNullException( nameof( chart ) );
#endif
        if ( usingGdi )
        {
            using var pen = new Pen( Color.Black, 1.0f ) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };
            // This is so much smoother than ControlPaint.DrawReversibleFrame
            UnsafeNativeMethods.DrawXorRectangle( chart.CreateGraphics(), pen, zoomRectangle );
        }
        else
        {
            var screenRect = chart.RectangleToScreen( zoomRectangle );
            ControlPaint.DrawReversibleFrame( screenRect, chart.BackColor, FrameStyle.Dashed );
        }
    }

    /// <summary>
    /// Draws an XOR selection rectangle. Zoom Rectangle is positioned in the client rectangle of the
    /// chart in which the zoom was initiated with a Left MouseDown event.
    /// </summary>
    /// <remarks> David, 2020-10-25. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="chart">         The chart. </param>
    /// <param name="pen">           The pen. </param>
    /// <param name="zoomRectangle"> The zoom rectangle. </param>
    /// <param name="usingGdi">      True to using GDI. </param>
    public static void DrawZoomRect( this Chart chart, Pen pen, Rectangle zoomRectangle, bool usingGdi )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( chart, nameof( chart ) );
#else
        if ( chart is null ) throw new ArgumentNullException( nameof( chart ) );
#endif
        if ( usingGdi )
        {
            // This is so much smoother than ControlPaint.DrawReversibleFrame
            UnsafeNativeMethods.DrawXorRectangle( chart.CreateGraphics(), pen, zoomRectangle );
        }
        else
        {
            var screenRect = chart.RectangleToScreen( zoomRectangle );
            ControlPaint.DrawReversibleFrame( screenRect, chart.BackColor, FrameStyle.Dashed );
        }
    }

    #endregion
}
