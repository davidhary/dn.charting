//
//  Purpose:	Classes related to the Data Points:
//				DataPointCollection - data points collection class
//				DataPoint - data point properties and methods
//				DataPointCustomProperties - data point & series properties
//				DataPointComparer - used for sorting data points in series
//


using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Text;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " customproperties enumeration "
/// <summary>
/// Enumeration of common properties names.
/// </summary>
internal enum CommonCustomProperties
{
    PointName,
    Label,
    AxisLabel,
    LabelFormat,
    IsValueShownAsLabel,
    Color,
    BorderColor,
    BorderDashStyle,
    BorderWidth,
    BackImage,
    BackImageWrapMode,
    BackImageAlignment,
    BackImageTransparentColor,
    BackGradientStyle,
    BackSecondaryColor,
    BackHatchStyle,
    Font,
    LabelForeColor,
    LabelAngle,
    MarkerStyle,
    MarkerSize,
    MarkerImage,
    MarkerImageTransparentColor,
    MarkerColor,
    MarkerBorderColor,
    MarkerBorderWidth,
    MapAreaAttributes,
    PostBackValue,
    MapAreaType,
    LegendMapAreaType,
    LabelMapAreaType,
    Url,
    ToolTip,
    Tag,
    LegendUrl,
    LegendToolTip,
    LegendText,
    LegendMapAreaAttributes,
    LegendPostBackValue,
    IsVisibleInLegend,
    LabelUrl,
    LabelToolTip,
    LabelMapAreaAttributes,
    LabelPostBackValue,
    LabelBorderColor,
    LabelBorderDashStyle,
    LabelBorderWidth,
    LabelBackColor,
};

#endregion
/// <summary>
/// Data points comparer class
/// </summary>
[
SRDescription( "DescriptionAttributeDataPointComparer_DataPointComparer" )
]
public class DataPointComparer : IComparer<DataPoint>
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles

    // Sorting order
    private readonly PointSortOrder _sortingOrder = PointSortOrder.Ascending;

    // Sorting value index
    private readonly int _sortingValueIndex = 1;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructtion "

    /// <summary>
    /// Private default constructor.
    /// </summary>
    private DataPointComparer()
    {
    }

    /// <summary>
    /// Data points comparer class constructor.
    /// </summary>
    /// <param name="series">Data series.</param>
    /// <param name="sortOrder">Sorting order.</param>
    /// <param name="sortBy">Value used for sorting ("X", "Y or Y1", "Y2", ...).</param>
    public DataPointComparer( Series series, PointSortOrder sortOrder, string sortBy )
    {
        // Check if sorting value is valid
        sortBy = sortBy.ToUpper( System.Globalization.CultureInfo.InvariantCulture );
        if ( string.Compare( sortBy, "X", StringComparison.Ordinal ) == 0 )
        {
            this._sortingValueIndex = -1;
        }
        else
        {
            this._sortingValueIndex = string.Compare( sortBy, "Y", StringComparison.Ordinal ) == 0
                ? 0
                : string.Compare( sortBy, "AXISLABEL", StringComparison.Ordinal ) == 0
                                ? -2
                                : sortBy.Length == 2 &&
                                                                sortBy.StartsWith( "Y", StringComparison.Ordinal ) &&
                                                                char.IsDigit( sortBy[1] )
                                                ? int.Parse( sortBy[1..], System.Globalization.CultureInfo.InvariantCulture ) - 1
                                                : throw (new ArgumentException( SR.ExceptionDataPointConverterInvalidSorting, nameof( sortBy ) ));
        }

        // Check if data series support as many Y values as required
        if ( this._sortingValueIndex > 0 && this._sortingValueIndex >= series.YValuesPerPoint )
        {
            throw new ArgumentException( SR.ExceptionDataPointConverterUnavailableSorting( sortBy, series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ), nameof( sortBy ) );
        }

        this._sortingOrder = sortOrder;
    }

    #endregion

    #region " comparing method "

    /// <summary>
    /// Compares two data points.
    /// </summary>
    /// <param name="x">First data point.</param>
    /// <param name="y">Second data point.</param>
    /// <returns>If the two values are equal, it returns zero.  If point 1 is greater than point 2, 
    /// it returns a positive integer; otherwise, it returns a negative integer.
    /// </returns>
    public int Compare( DataPoint x, DataPoint y )
    {
        int result = -1;

        // Compare X value
        result = this._sortingValueIndex == -1
            ? x.XValue.CompareTo( y.XValue )
            : this._sortingValueIndex == -2
                ? string.Compare( x.AxisLabel, y.AxisLabel, StringComparison.CurrentCulture )
                : x.YValues[this._sortingValueIndex].CompareTo( y.YValues[this._sortingValueIndex] );

        // Invert result depending on the sorting order
        if ( this._sortingOrder == PointSortOrder.Descending )
        {
            result = -result;
        }

        return result;
    }

    #endregion
}
/// <summary>
/// A collection of data points.
/// </summary>
[
    SRDescription( "DescriptionAttributeDataPointCollection_DataPointCollection" ),
]
public class DataPointCollection : ChartElementCollection<DataPoint>
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Reference to the sereies of data points
    internal Series series;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructors and initialization "

    /// <summary>
    /// Data Point Collection object constructor.
    /// </summary>
    /// <param name="series">Series object, which the Data Point Collection belongs to.</param>
    internal DataPointCollection( Series series ) : base( series ) => this.series = series;

    /// <summary>
    /// Initialize data point series and name.
    /// </summary>
    /// <param name="dataPoint">Reference to the data point object to initialize.</param>
    internal void DataPointInit( ref DataPoint dataPoint )
    {
        DataPointInit( this.series, ref dataPoint );
    }

    /// <summary>
    /// Initialize data point series and name.
    /// </summary>
    /// <param name="series">Series the data point belongs to.</param>
    /// <param name="dataPoint">Reference to the data point object to initialize.</param>
    internal static void DataPointInit( Series series, ref DataPoint dataPoint )
    {
        dataPoint.series = series;

        if ( dataPoint.AxisLabel.Length > 0 && series is not null )
        {
            series.noLabelsInPoints = false;
        }

        // Set flag that tooltips flags should be recalculated
        if ( dataPoint.ToolTip.Length > 0 &&
            dataPoint.LegendToolTip.Length > 0 &&
            dataPoint.LabelToolTip.Length > 0 &&
            series != null && series.Chart != null && series.Chart.selection is not null )
        {
            series.Chart.selection.enabledChecked = false;
        }
    }

    #endregion

    #region " data point binding, adding and inserting methods "

    /// <summary>
    /// Adds the new DataPoint to a collection and sets its Y values.
    /// </summary>
    /// <param name="y">The y.</param>
    /// <returns></returns>
    public DataPoint Add( params double[] y )
    {
        DataPoint point = new( 0, y );
        this.Add( point );
        return point;
    }

    /// <summary>
    /// Parse the input parameter with other point attribute binding rule
    /// in format: PointProperty=Field[{Format}] [,PointProperty=Field[{Format}]]. 
    /// For example: "Tooltip=Price{C1},Url=WebSiteName".
    /// </summary>
    /// <param name="otherFields">Other fields parameter.</param>
    /// <param name="otherAttributeNames">Returns array of attribute names.</param>
    /// <param name="otherFieldNames">Returns array of field names.</param>
    /// <param name="otherValueFormat">Returns array of format strings.</param>
    internal static void ParsePointFieldsParameter(
    string otherFields,
    ref string[] otherAttributeNames,
    ref string[] otherFieldNames,
    ref string[] otherValueFormat )
    {
        if ( otherFields != null && otherFields.Length > 0 )
        {
            // Split string by comma
            otherAttributeNames = otherFields.Replace( ",,", "\n" ).Split( ',' );
            otherFieldNames = new string[otherAttributeNames.Length];
            otherValueFormat = new string[otherAttributeNames.Length];

            // Loop through all strings
            for ( int index = 0; index < otherAttributeNames.Length; index++ )
            {
                // Split string by equal sign
                int equalSignIndex = otherAttributeNames[index].IndexOf( '=' );
                if ( equalSignIndex > 0 )
                {
                    otherFieldNames[index] = otherAttributeNames[index][(equalSignIndex + 1)..];
                    otherAttributeNames[index] = otherAttributeNames[index][..equalSignIndex];
                }
                else
                {
                    throw new ArgumentException( SR.ExceptionParameterFormatInvalid, nameof( otherFields ) );
                }

                // Check if format string was specified
                int bracketIndex = otherFieldNames[index].IndexOf( '{' );
                if ( bracketIndex > 0 && otherFieldNames[index][^1] == '}' )
                {
                    otherValueFormat[index] = otherFieldNames[index][(bracketIndex + 1)..];
                    otherValueFormat[index] = otherValueFormat[index].Trim( '{', '}' );
                    otherFieldNames[index] = otherFieldNames[index][..bracketIndex];
                }

                // Trim and replace new line character
                otherAttributeNames[index] = otherAttributeNames[index].Trim().Replace( "\n", "," );
                otherFieldNames[index] = otherFieldNames[index].Trim().Replace( "\n", "," );
                if ( otherValueFormat[index] is not null )
                    otherValueFormat[index] = otherValueFormat[index].Trim().Replace( "\n", "," );
            }
        }
    }

    /// <summary>
    /// Data bind X, Y and other values (like Tooltip, LabelStyle,...) of the data points to the data source.
    /// Data source can be the Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="xField">Name of the field for X values.</param>
    /// <param name="yFields">Comma separated names of the fields for Y values.</param>
    /// <param name="otherFields">Other point properties binding rule in format: PointProperty=Field[{Format}] [,PointProperty=Field[{Format}]]. For example: "Tooltip=Price{C1},Url=WebSiteName".</param>
    public void DataBind( IEnumerable dataSource, string xField, string yFields, string otherFields )
    {
        // Check arguments
        if ( dataSource == null ) throw new ArgumentNullException( nameof( dataSource ), SR.ExceptionDataPointInsertionNoDataSource );
        if ( dataSource is string )
            throw new ArgumentException( SR.ExceptionDataBindSeriesToString, nameof( dataSource ) );
        if ( yFields == null ) throw new ArgumentNullException( nameof( yFields ) );

        // Convert comma separated Y values field names string to array of names
        string[] yFieldNames = yFields.Replace( ",,", "\n" ).Split( ',' );
        for ( int index = 0; index < yFieldNames.Length; index++ )
        {
            yFieldNames[index] = yFieldNames[index].Replace( "\n", "," );
        }

        if ( yFieldNames.GetLength( 0 ) > this.series.YValuesPerPoint )
            throw new ArgumentOutOfRangeException( nameof( yFields ), SR.ExceptionDataPointYValuesCountMismatch( this.series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );

        // Convert other fields/properties names to two arrays of names
        string[] otherAttributeNames = null;
        string[] otherFieldNames = null;
        string[] otherValueFormat = null;
        ParsePointFieldsParameter(
            otherFields,
            ref otherAttributeNames,
            ref otherFieldNames,
            ref otherValueFormat );

        // Remove all existing data points
        this.Clear();

        // Get and reset enumerator
        IEnumerator enumerator = GetDataSourceEnumerator( dataSource );
        if ( enumerator.GetType() != typeof( DbEnumerator ) )
        {
            try
            {
                enumerator.Reset();
            }
            // Some enumerators may not support Resetting 
            catch ( InvalidOperationException )
            {
            }
            catch ( NotImplementedException )
            {
            }
            catch ( NotSupportedException )
            {
            }
        }

        // Add data points
        bool valueExsist = true;
        object[] yValuesObj = new object[yFieldNames.Length];
        object xValueObj = null;
        bool autoDetectType = true;

        this.SuspendUpdates();
        try
        {
            do
            {
                // Move to the next objects in the enumerations
                if ( valueExsist )
                {
                    valueExsist = enumerator.MoveNext();
                }

                // Auto detect valu(s) type
                if ( autoDetectType )
                {
                    autoDetectType = false;
                    AutoDetectValuesType( this.series, enumerator, xField, enumerator, yFieldNames[0] );
                }

                // Create and initialize data point
                if ( valueExsist )
                {
                    DataPoint newDataPoint = new( this.series );
                    bool emptyValues = false;

                    // Set X to the value provided
                    if ( xField.Length > 0 )
                    {
                        xValueObj = ConvertEnumerationItem( enumerator.Current, xField );
                        if ( IsEmptyValue( xValueObj ) )
                        {
                            emptyValues = true;
                            xValueObj = 0.0;
                        }
                    }

                    // Set Y values
                    if ( yFieldNames.Length == 0 )
                    {
                        yValuesObj[0] = ConvertEnumerationItem( enumerator.Current, null );
                        if ( IsEmptyValue( yValuesObj[0] ) )
                        {
                            emptyValues = true;
                            yValuesObj[0] = 0.0;
                        }
                    }
                    else
                    {
                        for ( int i = 0; i < yFieldNames.Length; i++ )
                        {
                            yValuesObj[i] = ConvertEnumerationItem( enumerator.Current, yFieldNames[i] );
                            if ( IsEmptyValue( yValuesObj[i] ) )
                            {
                                emptyValues = true;
                                yValuesObj[i] = 0.0;
                            }
                        }
                    }

                    // Set other values
                    if ( otherAttributeNames != null &&
                        otherAttributeNames.Length > 0 )
                    {
                        for ( int i = 0; i < otherFieldNames.Length; i++ )
                        {
                            // Get object by field name
                            object obj = ConvertEnumerationItem( enumerator.Current, otherFieldNames[i] );
                            if ( !IsEmptyValue( obj ) )
                            {
                                newDataPoint.SetPointCustomProperty(
                                    obj,
                                    otherAttributeNames[i],
                                    otherValueFormat[i] );
                            }
                        }
                    }

                    // IsEmpty value was detected
                    if ( emptyValues )
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        newDataPoint.IsEmpty = true;
                        this.Add( newDataPoint );
                    }
                    else
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        this.Add( newDataPoint );
                    }
                }

            } while ( valueExsist );

        }
        finally
        {
            this.ResumeUpdates();
        }
    }

    /// <summary>
    /// Data bind Y values of the data points to the data source.
    /// Data source can be the Array, Collection, Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="yValue">One or more enumerable objects with Y values.</param>
    public void DataBindY( params IEnumerable[] yValue )
    {
        this.DataBindXY( null, yValue );
    }

    /// <summary>
    /// Data bind X and Y values of the data points to the data source.
    /// Data source can be the Array, Collection, Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="xValue">Enumerable objects with X values.</param>
    /// <param name="yValues">One or more enumerable objects with Y values.</param>
    public void DataBindXY( IEnumerable xValue, params IEnumerable[] yValues )
    {
        // Y value must be provided
        if ( yValues == null ||
            (yValues.Length == 1 && yValues[0] == null) ) throw new ArgumentNullException( nameof( yValues ) );
        if ( yValues.GetLength( 0 ) == 0 )
            throw new ArgumentException( SR.ExceptionDataPointBindingYValueNotSpecified, nameof( yValues ) );

        // Double check that a string object is not provided for data binding
        for ( int i = 0; i < yValues.Length; i++ )
        {
            if ( yValues[i] is string )
            {
                throw new ArgumentException( SR.ExceptionDataBindYValuesToString, nameof( yValues ) );
            }
        }

        // Check if number of Y values do not out of range
        if ( yValues.GetLength( 0 ) > this.series.YValuesPerPoint )
        {
            throw new ArgumentOutOfRangeException( nameof( yValues ), SR.ExceptionDataPointYValuesBindingCountMismatch( this.series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );
        }

        // Remove all existing data points
        this.Clear();

        // Reset X, Y enumerators
        IEnumerator xEnumerator = null;
        IEnumerator[] yEnumerator = new IEnumerator[yValues.GetLength( 0 )];
        if ( xValue is not null )
        {
            // Double check that a string object is not provided for data binding
            if ( xValue is string )
            {
                throw new ArgumentException( SR.ExceptionDataBindXValuesToString, nameof( xValue ) );
            }

            // Get and reset Y values enumerators
            xEnumerator = GetDataSourceEnumerator( xValue );
            if ( xEnumerator.GetType() != typeof( DbEnumerator ) )
            {
                xEnumerator.Reset();
            }
        }
        for ( int i = 0; i < yValues.Length; i++ )
        {
            // Get and reset Y values enumerators
            yEnumerator[i] = GetDataSourceEnumerator( yValues[i] );
            if ( yEnumerator[i].GetType() != typeof( DbEnumerator ) )
            {
                yEnumerator[i].Reset();
            }
        }

        // Add data points
        bool xValueExsist = false;
        bool yValueExsist = true;
        object[] yValuesObj = new object[this.series.YValuesPerPoint];
        object xValueObj = null;
        bool autoDetectType = true;

        this.SuspendUpdates();
        try
        {
            do
            {
                // Move to the next objects in the enumerations
                yValueExsist = true;
                for ( int i = 0; i < yValues.Length; i++ )
                {
                    if ( yValueExsist )
                    {
                        yValueExsist = yEnumerator[i].MoveNext();
                    }
                }
                if ( xValue is not null )
                {
                    xValueExsist = xEnumerator.MoveNext();
                    if ( yValueExsist && !xValueExsist )
                    {
                        throw new ArgumentOutOfRangeException( nameof( xValue ), SR.ExceptionDataPointInsertionXValuesQtyIsLessYValues );
                    }
                }

                // Auto detect value(s) type
                if ( autoDetectType )
                {
                    autoDetectType = false;
                    AutoDetectValuesType( this.series, xEnumerator, null, yEnumerator[0], null );
                }

                // Create and initialize data point
                if ( xValueExsist || yValueExsist )
                {
                    DataPoint newDataPoint = new( this.series );
                    bool emptyValues = false;

                    // Set X to the value provided
                    if ( xValueExsist )
                    {
                        xValueObj = ConvertEnumerationItem( xEnumerator.Current, null );
                        if ( xValueObj is DBNull or null )
                        {
                            emptyValues = true;
                            xValueObj = 0.0;
                        }
                    }

                    // Set Y values
                    for ( int i = 0; i < yValues.Length; i++ )
                    {
                        yValuesObj[i] = ConvertEnumerationItem( yEnumerator[i].Current, null );
                        if ( yValuesObj[i] is DBNull or null )
                        {
                            emptyValues = true;
                            yValuesObj[i] = 0.0;
                        }
                    }

                    // IsEmpty value was detected
                    if ( emptyValues )
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        newDataPoint.IsEmpty = true;
                        this.Add( newDataPoint );
                    }
                    else
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        this.Add( newDataPoint );
                    }

                }

            } while ( xValueExsist || yValueExsist );

        }
        finally
        {
            this.ResumeUpdates();
        }
    }

    /// <summary>
    /// Data bind Y values of the data points to the data source.
    /// Data source can be the Array, Collection, Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="yValue">Enumerable objects with Y values.</param>
    /// <param name="yFields">Name of the fields for Y values.</param>
    public void DataBindY( IEnumerable yValue, string yFields )
    {
        this.DataBindXY( null, null, yValue, yFields );
    }

    /// <summary>
    /// Data bind X and Y values of the data points to the data source.
    /// Data source can be the Array, Collection, Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="xValue">Enumerable object with X values.</param>
    /// <param name="xField">Name of the field for X values.</param>
    /// <param name="yValue">Enumerable objects with Y values.</param>
    /// <param name="yFields">Comma separated names of the fields for Y values.</param>
    public void DataBindXY( IEnumerable xValue, string xField, IEnumerable yValue, string yFields )
    {
        // Check arguments
        if ( xValue is string )
            throw new ArgumentException( SR.ExceptionDataBindXValuesToString, nameof( xValue ) );
        if ( yValue == null ) throw new ArgumentNullException( nameof( yValue ), SR.ExceptionDataPointInsertionYValueNotSpecified );
        if ( yValue is string )
            throw new ArgumentException( SR.ExceptionDataBindYValuesToString, nameof( yValue ) );
        if ( yFields == null )
            throw new ArgumentOutOfRangeException( nameof( yFields ), SR.ExceptionDataPointYValuesCountMismatch( this.series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );

        // Convert comma separated field names string to array of names
        string[] yFieldNames = yFields.Replace( ",,", "\n" ).Split( ',' ); ;
        for ( int index = 0; index < yFieldNames.Length; index++ )
        {
            yFieldNames[index] = yFieldNames[index].Replace( "\n", "," );
        }
        if ( yFieldNames.GetLength( 0 ) > this.series.YValuesPerPoint )
            throw new ArgumentOutOfRangeException( nameof( yFields ), SR.ExceptionDataPointYValuesCountMismatch( this.series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );

        // Remove all existing data points
        this.Clear();

        // Reset X, Y enumerators
        IEnumerator xEnumerator = null;
        IEnumerator yEnumerator = GetDataSourceEnumerator( yValue );

        if ( yEnumerator.GetType() != typeof( DbEnumerator ) )
        {
            yEnumerator.Reset();
        }

        if ( xValue is not null )
        {
            if ( xValue != yValue )
            {
                xEnumerator = GetDataSourceEnumerator( xValue );
                if ( xEnumerator.GetType() != typeof( DbEnumerator ) )
                {
                    xEnumerator.Reset();
                }
            }
            else
            {
                xEnumerator = yEnumerator;
            }
        }

        // Add data points
        bool xValueExsist = false;
        bool yValueExsist = true;
        object[] yValuesObj = new object[yFieldNames.Length];
        object xValueObj = null;
        bool autoDetectType = true;

        this.SuspendUpdates();
        try
        {
            do
            {
                // Move to the next objects in the enumerations
                if ( yValueExsist )
                {
                    yValueExsist = yEnumerator.MoveNext();
                }
                if ( xValue is not null )
                {
                    if ( xValue != yValue )
                    {
                        xValueExsist = xEnumerator.MoveNext();
                        if ( yValueExsist && !xValueExsist )
                        {
                            throw new ArgumentOutOfRangeException( nameof( xValue ), SR.ExceptionDataPointInsertionXValuesQtyIsLessYValues );
                        }
                    }
                    else
                    {
                        xValueExsist = yValueExsist;
                    }
                }

                // Auto detect valu(s) type
                if ( autoDetectType )
                {
                    autoDetectType = false;
                    AutoDetectValuesType( this.series, xEnumerator, xField, yEnumerator, yFieldNames[0] );
                }

                // Create and initialize data point
                if ( xValueExsist || yValueExsist )
                {
                    DataPoint newDataPoint = new( this.series );
                    bool emptyValues = false;

                    // Set X to the value provided or use sequence numbers starting with 1
                    if ( xValueExsist )
                    {
                        xValueObj = ConvertEnumerationItem( xEnumerator.Current, xField );
                        if ( IsEmptyValue( xValueObj ) )
                        {
                            emptyValues = true;
                            xValueObj = 0.0;
                        }

                    }

                    if ( yFieldNames.Length == 0 )
                    {
                        yValuesObj[0] = ConvertEnumerationItem( yEnumerator.Current, null );
                        if ( IsEmptyValue( yValuesObj[0] ) )
                        {
                            emptyValues = true;
                            yValuesObj[0] = 0.0;
                        }
                    }
                    else
                    {
                        for ( int i = 0; i < yFieldNames.Length; i++ )
                        {
                            yValuesObj[i] = ConvertEnumerationItem( yEnumerator.Current, yFieldNames[i] );
                            if ( IsEmptyValue( yValuesObj[i] ) )
                            {
                                emptyValues = true;
                                yValuesObj[i] = 0.0;
                            }
                        }
                    }

                    // IsEmpty value was detected
                    if ( emptyValues )
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        newDataPoint.IsEmpty = true;
                        this.Add( newDataPoint );
                    }
                    else
                    {
                        if ( xValueObj is not null )
                        {
                            newDataPoint.SetValueXY( xValueObj, yValuesObj );
                        }
                        else
                        {
                            newDataPoint.SetValueXY( 0, yValuesObj );
                        }
                        this.DataPointInit( ref newDataPoint );
                        this.Add( newDataPoint );
                    }
                }

            } while ( xValueExsist || yValueExsist );

        }
        finally
        {
            this.ResumeUpdates();
        }
    }

    /// <summary>
    /// Returns true if objet represents an empty value.
    /// </summary>
    /// <param name="val">Value to test.</param>
    /// <returns>True if empty.</returns>
    internal static bool IsEmptyValue( object val )
    {
        if ( val is DBNull or null )
        {
            return true;
        }
        if ( val is double v && double.IsNaN( v ) )
        {
            return true;
        }
        return val is float v1 && float.IsNaN( v1 );
    }

    /// <summary>
    /// Adds one data point with one Y value.
    /// </summary>
    /// <param name="yValue">Y value of the data point.</param>
    /// <returns>Index of newly added data point.</returns>
    public int AddY( double yValue )
    {
        // Create new point object
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueY( yValue );
        this.DataPointInit( ref newDataPoint );
        this.Add( newDataPoint );
        return this.Count - 1;
    }

    /// <summary>
    /// Adds one data point with one or more Y values.
    /// </summary>
    /// <param name="yValue">List of Y values of the data point.</param>
    /// <returns>Index of newly added data point.</returns>
    public int AddY( params object[] yValue )
    {
        //Check arguments
        if ( yValue == null ||
            (yValue.Length == 1 && yValue[0] == null) ) throw new ArgumentNullException( nameof( yValue ) );

        // Auto detect DateTime values type
        if ( this.series.YValueType == ChartValueType.Auto &&
            yValue.Length > 0 &&
            yValue[0] is not null )
        {
            if ( yValue[0] is DateTime )
            {
                this.series.YValueType = ChartValueType.DateTime;
                this.series.autoYValueType = true;
            }
            else if ( yValue[0] is DateTimeOffset )
            {
                this.series.YValueType = ChartValueType.DateTimeOffset;
                this.series.autoYValueType = true;
            }
        }

        // Create new point object
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueY( yValue );
        this.DataPointInit( ref newDataPoint );
        this.Add( newDataPoint );
        return this.Count - 1;
    }

    /// <summary>
    /// Adds one data point with X value and one Y value.
    /// </summary>
    /// <param name="yValue">Y value of the data point.</param>
    /// <param name="xValue">X value of the data point.</param>
    /// <returns>Index of newly added data poit.</returns>
    public int AddXY( double xValue, double yValue )
    {
        // Create new point object
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueXY( xValue, yValue );
        this.DataPointInit( ref newDataPoint );
        this.Add( newDataPoint );
        return this.Count - 1;
    }

    /// <summary>
    /// Adds one data point with X value and one or more Y values.
    /// </summary>
    /// <param name="yValue">List of Y values of the data point.</param>
    /// <param name="xValue">X value of the data point.</param>
    /// <returns>Index of newly added data poit.</returns>
    public int AddXY( object xValue, params object[] yValue )
    {
        // Auto detect DateTime and String values type
        if ( this.series.XValueType == ChartValueType.Auto )
        {
            if ( xValue is DateTime )
            {
                this.series.XValueType = ChartValueType.DateTime;
            }
            if ( xValue is DateTimeOffset )
            {
                this.series.XValueType = ChartValueType.DateTimeOffset;
            }
            if ( xValue is string )
            {
                this.series.XValueType = ChartValueType.String;
            }

            this.series.autoXValueType = true;
        }

        if ( this.series.YValueType == ChartValueType.Auto &&
            yValue.Length > 0 &&
            yValue[0] is not null )
        {
            if ( yValue[0] is DateTime )
            {
                this.series.YValueType = ChartValueType.DateTime;
                this.series.autoYValueType = true;
            }
            else if ( yValue[0] is DateTimeOffset )
            {
                this.series.YValueType = ChartValueType.DateTimeOffset;
                this.series.autoYValueType = true;
            }
        }

        // Create new point object
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueXY( xValue, yValue );
        this.DataPointInit( ref newDataPoint );
        this.Add( newDataPoint );
        return this.Count - 1;
    }

    /// <summary>
    /// Insert one data point with X value and one or more Y values.
    /// </summary>
    /// <param name="index">Index after which to insert the data point.</param>
    /// <param name="xValue">X value of the data point.</param>
    /// <param name="yValue">List of Y values of the data point.</param>
    public void InsertXY( int index, object xValue, params object[] yValue )
    {
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueXY( xValue, yValue );
        this.DataPointInit( ref newDataPoint );
        this.Insert( index, newDataPoint );
    }

    /// <summary>
    /// Insert one data point with one or more Y values.
    /// </summary>
    /// <param name="index">Index after which to insert the data point.</param>
    /// <param name="yValue">List of Y values of the data point.</param>
    public void InsertY( int index, params object[] yValue )
    {
        DataPoint newDataPoint = new( this.series );
        newDataPoint.SetValueY( yValue );
        this.DataPointInit( ref newDataPoint );
        this.Insert( index, newDataPoint );
    }

    /// <summary>
    /// Get data source enumerator object helper function.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <returns>Returns data source enumerator.</returns>
    internal static IEnumerator GetDataSourceEnumerator( IEnumerable dataSource )
    {
        if ( dataSource is DataView dataView )
        {
            return dataView.GetEnumerator();
        }
        if ( dataSource is DataSet dataSet )
        {
            if ( dataSet.Tables.Count > 0 )
            {
                return dataSet.Tables[0].Rows.GetEnumerator();
            }
        }

        return dataSource.GetEnumerator();
    }

    /// <summary>
    /// Convert enumeration item object from DataRow and DataRowView 
    /// to the actual value of specified column in row
    /// </summary>
    /// <param name="item">Enumeration item.</param>
    /// <param name="fieldName">Converted item.</param>
    /// <returns></returns>
    internal static object ConvertEnumerationItem( object item, string fieldName )
    {
        object result = item;

        // If original object is DataRow
        if ( item is DataRow dataRow )
        {
            if ( fieldName != null && fieldName.Length > 0 )
            {
                // Check if specified column exist
                bool failed = true;
                if ( dataRow.Table.Columns.Contains( fieldName ) )
                {
                    result = dataRow[fieldName];
                    failed = false;
                }
                else
                {
                    // Try to treat field name as column index number
                    failed = !int.TryParse( fieldName, NumberStyles.Any, CultureInfo.InvariantCulture, out int columnIndex );

                    if ( !failed && columnIndex < dataRow.Table.Columns.Count && columnIndex >= 0 )
                    {
                        result = dataRow[columnIndex];
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( fieldName ) );
                }
            }
            else
            {
                // Get first column value if name not specified
                result = dataRow[0];
            }
        }

        // If original object is DataRowView

        if ( item is DataRowView dataRowView )
        {
            if ( fieldName != null && fieldName.Length > 0 )
            {
                // Check if specified column exist
                bool failed = true;
                if ( dataRowView.DataView.Table.Columns.Contains( fieldName ) )
                {
                    result = dataRowView[fieldName];
                    failed = false;
                }
                else
                {
                    // Try to treat field name as column index number
                    failed = !int.TryParse( fieldName, NumberStyles.Any, CultureInfo.InvariantCulture, out int columnIndex );
                    if ( !failed && columnIndex < dataRowView.DataView.Table.Columns.Count && columnIndex >= 0 )
                    {
                        result = dataRowView[columnIndex];
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( fieldName ) );
                }
            }
            else
            {
                // Get first column value if name not specified
                result = dataRowView[0];
            }
        }

        // If original object is DbDataRecord
        if ( item is DbDataRecord dbDataRecord )
        {
            if ( fieldName != null && fieldName.Length > 0 )
            {
                // Check if specified column exist
                bool failed = true;
                if ( !char.IsNumber( fieldName, 0 ) )
                {
                    try
                    {
                        result = dbDataRecord[fieldName];
                        failed = false;
                    }
                    catch ( IndexOutOfRangeException )
                    {
                        failed = true;
                    }
                }

                if ( failed )
                {
                    // Try to treat field name as column index number
                    try
                    {
                        bool parseSucceed = int.TryParse( fieldName, NumberStyles.Any, CultureInfo.InvariantCulture, out int columnIndex );

                        if ( parseSucceed )
                        {
                            result = dbDataRecord[columnIndex];
                            failed = false;
                        }
                        else
                        {
                            failed = true;
                        }
                    }
                    catch ( IndexOutOfRangeException )
                    {
                        failed = true;
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( fieldName ) );
                }

            }
            else
            {
                // Get first column value if name not specified
                result = dbDataRecord[0];
            }
        }
        else
        {
            if ( fieldName != null && fieldName.Length > 0 )
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties( item ).Find( fieldName, true );
                if ( descriptor is not null )
                {
                    result = descriptor.GetValue( item );
                    return result ?? null;

                }
            }
        }

        return result;
    }
    /// <summary>
    /// Auto detects the X and Y(s) values type
    /// </summary>
    /// <param name="series">Series the values type is detected for.</param>
    /// <param name="xEnumerator">X values enumerator.</param>
    /// <param name="xField">X value field.</param>
    /// <param name="yEnumerator">Y values enumerator.</param>
    /// <param name="yField">Y value field.</param>
    internal static void AutoDetectValuesType(
        Series series,
        IEnumerator xEnumerator,
        string xField,
        IEnumerator yEnumerator,
        string yField )
    {
        if ( series.XValueType == ChartValueType.Auto )
        {
            series.XValueType = GetValueType( xEnumerator, xField );
            if ( series.XValueType != ChartValueType.Auto )
            {
                series.autoXValueType = true;
            }
        }
        if ( series.YValueType == ChartValueType.Auto )
        {
            series.YValueType = GetValueType( yEnumerator, yField );
            if ( series.YValueType != ChartValueType.Auto )
            {
                series.autoYValueType = true;
            }
        }
    }

    /// <summary>
    /// Return value type.
    /// </summary>
    /// <param name="enumerator">Values enumerator.</param>
    /// <param name="field">Value field.</param>
    private static ChartValueType GetValueType( IEnumerator enumerator, string field )
    {
        ChartValueType type = ChartValueType.Auto;
        Type columnDataType = null;

        // Check parameters
        if ( enumerator == null )
        {
            return type;
        }

        // Check if current enumeration element is available
        try
        {
            if ( enumerator.Current == null )
            {
                return type;
            }
        }
        catch ( InvalidOperationException )
        {
            return type;
        }


        // If original object is DataRow
        if ( enumerator.Current is DataRow row )
        {
            if ( field != null && field.Length > 0 )
            {
                // Check if specified column exist
                bool failed = true;
                if ( row.Table.Columns.Contains( field ) )
                {
                    columnDataType = row.Table.Columns[field].DataType;
                    failed = false;
                }

                // Try to treat field as column number
                if ( failed )
                {
                    bool parseSucceed = int.TryParse( field, NumberStyles.Any, CultureInfo.InvariantCulture, out int columnIndex );

                    if ( parseSucceed )
                    {
                        columnDataType = row.Table.Columns[columnIndex].DataType;
                        failed = false;
                    }
                    else
                    {
                        failed = true;
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( field ) );
                }

            }
            else if ( row.Table.Columns.Count > 0 )
            {
                columnDataType = row.Table.Columns[0].DataType;
            }
        }

        // If original object is DataRowView
        else if ( enumerator.Current is DataRowView view )
        {
            if ( field != null && field.Length > 0 )
            {
                // Check if specified column exist
                bool failed = true;
                if ( view.DataView.Table.Columns.Contains( field ) )
                {
                    columnDataType = view.DataView.Table.Columns[field].DataType;
                    failed = false;
                }

                // Try to treat field as column number
                if ( failed )
                {
                    bool parseSucceed = int.TryParse( field, NumberStyles.Any, CultureInfo.InvariantCulture, out int columnIndex );
                    if ( parseSucceed )
                    {
                        columnDataType = view.DataView.Table.Columns[columnIndex].DataType;
                        failed = false;
                    }
                    else
                    {
                        failed = true;
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( field ) );
                }

            }
            else if ( view.DataView.Table.Columns.Count > 0 )
            {
                columnDataType = view.DataView.Table.Columns[0].DataType;
            }
        }

        // If original object is DbDataRecord
        else if ( enumerator.Current is DbDataRecord record )
        {
            if ( field != null && field.Length > 0 )
            {
                bool failed = true;
                int columnIndex = 0;
                if ( !char.IsNumber( field, 0 ) )
                {
                    columnIndex = record.GetOrdinal( field );
                    columnDataType = record.GetFieldType( columnIndex );
                    failed = false;
                }

                // Try to treat field as column number
                if ( failed )
                {
                    failed = !int.TryParse( field, NumberStyles.Any, CultureInfo.InvariantCulture, out columnIndex );

                    if ( !failed )
                    {
                        columnDataType = record.GetFieldType( columnIndex );
                    }
                }

                if ( failed )
                {
                    throw new ArgumentException( SR.ExceptionColumnNameNotFound( field ) );
                }

            }
            else if ( record.FieldCount > 0 )
            {
                columnDataType = record.GetFieldType( 0 );
            }
        }
        // Try detecting simple data types
        else
        {
            if ( field != null && field.Length > 0 )
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties( enumerator.Current ).Find( field, true );
                if ( descriptor is not null )
                {
                    columnDataType = descriptor.PropertyType;
                }
            }
            if ( columnDataType == null )
            {
                columnDataType = enumerator.Current.GetType();
            }
        }

        // Use data type
        if ( columnDataType is not null )
        {
            if ( columnDataType == typeof( DateTime ) )
                type = ChartValueType.DateTime;
            else if ( columnDataType == typeof( DateTimeOffset ) )
                type = ChartValueType.DateTimeOffset;
            else if ( columnDataType == typeof( TimeSpan ) )
                type = ChartValueType.Time;
            else if ( columnDataType == typeof( double ) )
                type = ChartValueType.Double;
            else if ( columnDataType == typeof( int ) )
                type = ChartValueType.Int32;
            else if ( columnDataType == typeof( long ) )
                type = ChartValueType.Int64;
            else if ( columnDataType == typeof( float ) )
                type = ChartValueType.Single;
            else if ( columnDataType == typeof( string ) )
                type = ChartValueType.String;
            else if ( columnDataType == typeof( uint ) )
                type = ChartValueType.UInt32;
            else if ( columnDataType == typeof( ulong ) )
                type = ChartValueType.UInt64;
        }

        return type;
    }

    #endregion

    #region " datapoint finding functions "

    /// <summary>
    /// Find all the points that equal to the specified value starting from the specified index.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <param name="startIndex">Index of the point to start looking from.</param>
    /// <returns>Enumerator of datapoints.</returns>
    public IEnumerable<DataPoint> FindAllByValue( double valueToFind, string useValue, int startIndex )
    {
        // Loop through all points from specified index
        for ( int i = startIndex; i < this.Count; i++ )
        {
            DataPoint point = this[i];
            if ( point.GetValueByName( useValue ) == valueToFind )
            {
                yield return point;
            }
        }
    }

    /// <summary>
    /// Find all the points that equal to the specified value.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <returns>Enumerator of datapoints.</returns>
    public IEnumerable<DataPoint> FindAllByValue( double valueToFind, string useValue )
    {
        // Loop through all points from specified index
        for ( int i = 0; i < this.Count; i++ )
        {
            DataPoint point = this[i];
            if ( point.GetValueByName( useValue ) == valueToFind )
            {
                yield return point;
            }
        }
    }

    /// <summary>
    /// Find all the points that equal to the specified value.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <returns>Enumerator of datapoints.</returns>
    public IEnumerable<DataPoint> FindAllByValue( double valueToFind )
    {
        return this.FindAllByValue( valueToFind, "Y" );
    }

    /// <summary>
    /// Find the first point that equals to the specified value starting from the specified index.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <param name="startIndex">Index of the point to start looking from.</param>
    /// <returns>Datapoint which matches the value.  Null if there is no match.</returns>
    public DataPoint FindByValue( double valueToFind, string useValue, int startIndex )
    {
        //Check arguments
        if ( useValue == null ) throw new ArgumentNullException( nameof( useValue ) );
        if ( startIndex < 0 || startIndex >= this.Count )
            throw new ArgumentOutOfRangeException( nameof( startIndex ) );

        // Loop through all points from specified index
        for ( int i = startIndex; i < this.Count; i++ )
        {
            DataPoint point = this[i];
            if ( point.GetValueByName( useValue ) == valueToFind )
            {
                return point;
            }
        }

        // Nothing was found
        return null;
    }

    /// <summary>
    /// Find the first point that equals to the specified value.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <returns>Datapoint which matches the value.  Null if there is no match.</returns>
    public DataPoint FindByValue( double valueToFind, string useValue )
    {
        return this.FindByValue( valueToFind, useValue, 0 );
    }

    /// <summary>
    /// Find the first point that equals to the specified value.
    /// </summary>
    /// <param name="valueToFind">Point value to find.</param>
    /// <returns>Datapoint which matches the value.  Null if there is no match.</returns>
    public DataPoint FindByValue( double valueToFind )
    {
        return this.FindByValue( valueToFind, "Y" );
    }

    /// <summary>
    /// Find point with the maximum value starting from specified index.
    /// </summary>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <param name="startIndex">Index of the point to start looking from.</param>
    /// <returns>Datapoint with the maximum value.</returns>
    public DataPoint FindMaxByValue( string useValue, int startIndex )
    {
        //Check arguments
        if ( useValue == null ) throw new ArgumentNullException( nameof( useValue ) );
        if ( startIndex < 0 || startIndex >= this.Count )
            throw new ArgumentOutOfRangeException( nameof( startIndex ) );

        bool isYValue = useValue.StartsWith( "Y", StringComparison.OrdinalIgnoreCase );
        double maxValue = double.MinValue;
        DataPoint maxPoint = null;

        for ( int i = startIndex; i < this.Count; i++ )
        {
            DataPoint point = this[i];

            // Skip empty points when searching for the Y values
            if ( point.IsEmpty && isYValue )
                continue;

            double pointValue = point.GetValueByName( useValue );

            if ( maxValue < pointValue )
            {
                maxValue = pointValue;
                maxPoint = point;
            }
        }

        return maxPoint;
    }

    /// <summary>
    /// Find point with the maximum value.
    /// </summary>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <returns>Datapoint with the maximum value.</returns>
    public DataPoint FindMaxByValue( string useValue )
    {
        return this.FindMaxByValue( useValue, 0 );
    }

    /// <summary>
    /// Find data point with the maximum value.
    /// </summary>
    /// <returns>Datapoint with the maximum value.</returns>
    public DataPoint FindMaxByValue()
    {
        return this.FindMaxByValue( "Y" );
    }

    /// <summary>
    /// Find point with the Min value starting from specified index.
    /// </summary>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <param name="startIndex">Index of the point to start looking from.</param>
    /// <returns>Datapoint with the Min value.</returns>
    public DataPoint FindMinByValue( string useValue, int startIndex )
    {
        if ( useValue == null ) throw new ArgumentNullException( nameof( useValue ) );
        if ( startIndex < 0 || startIndex >= this.Count )
            throw new ArgumentOutOfRangeException( nameof( startIndex ) );

        bool isYValue = useValue.StartsWith( "Y", StringComparison.OrdinalIgnoreCase );
        double minValue = double.MaxValue;
        DataPoint minPoint = null;

        for ( int i = startIndex; i < this.Count; i++ )
        {
            DataPoint point = this[i];

            // Skip empty points when searching for the Y values
            if ( point.IsEmpty && isYValue )
                continue;

            double pointValue = point.GetValueByName( useValue );

            if ( minValue > pointValue )
            {
                minValue = pointValue;
                minPoint = point;
            }
        }

        return minPoint;
    }

    /// <summary>
    /// Find point with the Min value.
    /// </summary>
    /// <param name="useValue">Which point value to use (X, Y1, Y2,...).</param>
    /// <returns>Datapoint with the Min value.</returns>
    public DataPoint FindMinByValue( string useValue )
    {
        return this.FindMinByValue( useValue, 0 );
    }

    /// <summary>
    /// Find point with the Min value
    /// </summary>
    /// <returns>Datapoint with the Min value.</returns>
    public DataPoint FindMinByValue()
    {
        return this.FindMinByValue( "Y" );
    }

    #endregion

    #region " collection<t> overrides "

    /// <summary>
    /// Initializes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    internal override void Initialize( DataPoint item )
    {
        this.DataPointInit( ref item );
        base.Initialize( item );
    }

    /// <summary>
    /// Removes all elements from the <see cref="Collections.ObjectModel.Collection{T}"/>.
    /// </summary>
    protected override void ClearItems()
    {
        // Refresh Minimum and Maximum from data
        // after recalc and set data			
        if ( this.Common != null && this.Common.ChartPicture is not null )
        {
            this.Common.ChartPicture.ResetMinMaxFromData();
        }

        base.ClearItems();
    }

    #endregion
}
/// <summary>
/// Stores values and properties of a DataPoint of a Series.
/// </summary>
[
SRDescription( "DescriptionAttributeDataPoint_DataPoint" ),
DefaultProperty( "YValues" ),
TypeConverter( typeof( DataPointConverter ) )
]
public class DataPoint : DataPointCustomProperties
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Point X value
    private double _xValue;

    // Point Y values
    private double[] _yValue = new double[1];

    // Pre calculated (during painting) relative position of data point
    internal PointF positionRel = PointF.Empty;

    // VSTS:199794 - Accessibility needs the last rendered label content to be exposed.
    // The current label content evaluation is scattered over different chart types and cannot be isolated without risk of regression.
    // This variable will cache the label content taken just before drawing.
    internal string _lastLabelText = string.Empty;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructtion "

    /// <summary>
    /// DataPoint object constructor.
    /// </summary>
    public DataPoint() : base( null, true ) => this._yValue = new double[1];

    /// <summary>
    /// DataPoint object constructor.
    /// </summary>
    /// <param name="series">series object, which the DataPoint belongs to.</param>
    public DataPoint( Series series ) : base( series, true )
    {
        // Create Y value(s) array
        this._yValue = new double[series.YValuesPerPoint];
        this._xValue = 0;
    }

    /// <summary>
    /// DataPoint object constructor.
    /// </summary>
    /// <param name="xValue">X value.</param>
    /// <param name="yValue">Y value.</param>
    public DataPoint( double xValue, double yValue )
        : base( null, true )
    {
        // Set Y value
        this._yValue = new double[1];
        this._yValue[0] = yValue;

        // Set X value
        this._xValue = xValue;
    }

    /// <summary>
    /// DataPoint object constructor.
    /// </summary>
    /// <param name="xValue">X value.</param>
    /// <param name="yValues">Array of Y values.</param>
    public DataPoint( double xValue, double[] yValues )
        : base( null, true )
    {
        // Set Y value
        this._yValue = yValues;

        // Set X value
        this._xValue = xValue;
    }

    /// <summary>
    /// DataPoint object constructor.
    /// </summary>
    /// <remarks>
    /// This method is only used during the Windows Forms serialization of the chart.
    /// </remarks>
    /// <param name="xValue">X value.</param>
    /// <param name="yValues">String of comma separated Y values.</param>
    [EditorBrowsable( EditorBrowsableState.Never )]
    public DataPoint( double xValue, string yValues )
        : base( null, true )
    {
        string[] values = yValues.Split( ',' );

        // Create Y value(s) array
        this._yValue = new double[values.Length];

        for ( int index = 0; index < values.Length; index++ )
        {
            this._yValue[index] = CommonElements.ParseDouble( values[index], true );
        }

        // Set X value
        this._xValue = xValue;
    }

    #endregion

    #region " data point methods "

    /// <summary>
    /// Sets the specified data point attribute to the specified value.
    /// </summary>
    /// <param name="obj">Attribute value.</param>
    /// <param name="propertyName">Attribute name.</param>
    /// <param name="format">Value format.</param>
    internal void SetPointCustomProperty(
        object obj,
        string propertyName,
        string format )
    {
        // Convert value to string
        if ( obj is not string stringValue )
        {
            double doubleObj = double.NaN;
            ChartValueType valueType = ChartValueType.Auto;
            if ( obj is DateTime time )
            {
                doubleObj = time.ToOADate();
                valueType = ChartValueType.Date;
            }
            else
            {
                doubleObj = this.ConvertValue( obj );
            }

            // Try converting to string
            if ( !double.IsNaN( doubleObj ) )
            {
                try
                {
                    stringValue = ValueConverter.FormatValue(
                        this.Chart,
                        this,
                        this.Tag,
                        doubleObj,
                        format,
                        valueType,
                        ChartElementType.DataPoint );
                }
                catch ( FormatException )
                {
                    // Use basic string converter
                    stringValue = obj.ToString();
                }
            }
            else
            {
                // Use basic string converter
                stringValue = obj.ToString();
            }
        }

        // Assign data point attribute by name
        if ( stringValue.Length > 0 )
        {
            if ( string.Compare( propertyName, "AxisLabel", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.AxisLabel = stringValue;
            }
            else if ( string.Compare( propertyName, "Tooltip", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.ToolTip = stringValue;
            }
            else if ( string.Compare( propertyName, "Label", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.Label = stringValue;
            }
            else if ( string.Compare( propertyName, "LegendTooltip", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.LegendToolTip = stringValue;
            }
            else if ( string.Compare( propertyName, "LegendText", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.LegendText = stringValue;
            }
            else if ( string.Compare( propertyName, "LabelToolTip", StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                this.LabelToolTip = stringValue;
            }
            else
            {
                this[propertyName] = stringValue;
            }
        }
    }

    /// <summary>
    /// Converts object to double.
    /// </summary>
    /// <param name="value">Object to convert.</param>
    /// <returns>Double value.</returns>
    private double ConvertValue( object value )
    {
        if ( value == null )
        {
            return 0;
        }

        if ( value is double )
        {
            return ( double ) value;
        }
        else if ( value is float )
        {
            return ( double ) ( float ) value;
        }
        else if ( value is decimal )
        {
            return ( double ) ( decimal ) value;
        }
        else if ( value is int )
        {
            return ( int ) value;
        }
        else if ( value is uint )
        {
            return ( uint ) value;
        }
        else if ( value is long )
        {
            return ( long ) value;
        }
        else if ( value is ulong )
        {
            return ( ulong ) value;
        }
        else if ( value is byte )
        {
            return ( byte ) value;
        }
        else if ( value is sbyte )
        {
            return ( sbyte ) value;
        }
        else if ( value is bool )
        {
            return (( bool ) value) ? 1.0 : 0.0;
        }
        else
        {
            string stringValue = "";
            stringValue = value.ToString();
            return CommonElements.ParseDouble( stringValue );
        }
    }

    /// <summary>
    /// Set X value and one or more Y values of the data point.
    /// </summary>
    /// <param name="xValue">X value of the data point.</param>
    /// <param name="yValue">List of Y values of the data point.</param>
    public void SetValueXY( object xValue, params object[] yValue )
    {
        // Check arguments
        if ( xValue == null ) throw new ArgumentNullException( nameof( xValue ) );

        // Set Y value first
        this.SetValueY( yValue );

        // Check if parameters type matches with series type
        Type paramType = xValue.GetType();
        base.series?.CheckSupportedTypes( paramType );

        // Save value in the array
        if ( paramType == typeof( string ) )
        {
            this.AxisLabel = ( string ) xValue;
        }
        else
        {
            this._xValue = paramType == typeof( DateTime ) ? (( DateTime ) xValue).ToOADate() : this.ConvertValue( xValue );
        }

        // Get Date or Time if required
        if ( base.series != null && xValue is DateTime time1 )
        {
            if ( base.series.XValueType == ChartValueType.Date )
            {
                DateTime time = new(
                    time1.Year,
                    time1.Month,
                    time1.Day,
                    0,
                    0,
                    0,
                    0 );
                this._xValue = time.ToOADate();
            }
            else if ( base.series.XValueType == ChartValueType.Time )
            {
                DateTime time = new(
                    1899,
                    12,
                    30,
                    time1.Hour,
                    time1.Minute,
                    time1.Second,
                    time1.Millisecond );
                this._xValue = time.ToOADate();
            }
        }

        // Check if one of Y values are not avilable
        bool empty = false;
        foreach ( double d in this._yValue )
        {
            if ( double.IsNaN( d ) )
            {
                empty = true;
                break;
            }
        }

        // Set point empty flag and values to zero
        if ( empty )
        {
            this.IsEmpty = true;
            for ( int valueIndex = 0; valueIndex < this._yValue.Length; valueIndex++ )
            {
                this._yValue[valueIndex] = 0.0;
            }
        }
    }

    /// <summary>
    /// Set one or more Y values of the data point.
    /// </summary>
    /// <param name="yValue">List of Y values of the data point.</param>
    public void SetValueY( params object[] yValue )
    {
        // Check arguments
        if ( yValue == null ) throw new ArgumentNullException( nameof( yValue ) );

        // Check number of parameters. Should be more than 0 and 
        if ( yValue.Length == 0 || (base.series != null && yValue.Length > base.series.YValuesPerPoint) )
            throw new ArgumentOutOfRangeException( nameof( yValue ), SR.ExceptionDataPointYValuesSettingCountMismatch( base.series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );

        // Check if there is a Null Y value
        for ( int i = 0; i < yValue.Length; i++ )
        {
            if ( yValue[i] is null or DBNull )
            {
                yValue[i] = 0.0;
                if ( i == 0 )
                {
                    this.IsEmpty = true;
                }
            }
        }

        // Check if parameters type matches with series type
        Type paramType = yValue[0].GetType();
        base.series?.CheckSupportedTypes( paramType );

        // Make sure the Y values array is big enough
        if ( this._yValue.Length < yValue.Length )
        {
            this._yValue = new double[yValue.Length];
        }

        // Save value in the array
        if ( paramType == typeof( string ) )
        {
            try
            {
                for ( int i = 0; i < yValue.Length; i++ )
                {
                    this._yValue[i] = CommonElements.ParseDouble( ( string ) yValue[i] );
                }
            }
            catch
            {
                // Get reference to the chart object
                if ( this.Common != null && this.Common.ChartPicture != null && this.Common.ChartPicture.SuppressExceptions )
                {
                    this.IsEmpty = true;
                    for ( int i = 0; i < yValue.Length; i++ )
                    {
                        yValue[i] = 0.0;
                    }
                }
                else
                {
                    throw new ArgumentException( SR.ExceptionDataPointYValueStringFormat );
                }
            }

        }
        else if ( paramType == typeof( DateTime ) )
        {
            for ( int i = 0; i < yValue.Length; i++ )
            {
                this._yValue[i] = yValue[i] == null || (yValue[i] is double v && v == 0.0)
                    ? DateTime.Now.ToOADate()
                    : (( DateTime ) yValue[i]).ToOADate();
            }
        }
        else
        {
            for ( int i = 0; i < yValue.Length; i++ )
            {
                this._yValue[i] = this.ConvertValue( yValue[i] );
            }
        }

        // Get Date or Time if required
        if ( base.series is not null )
        {
            for ( int i = 0; i < yValue.Length; i++ )
            {
                if ( yValue[i] == null || (yValue[i] is double v && v == 0.0) )
                {
                    if ( base.series.YValueType == ChartValueType.Date )
                    {
                        this._yValue[i] = Math.Floor( this._yValue[i] );
                    }
                    else if ( base.series.YValueType == ChartValueType.Time )
                    {
                        this._yValue[i] = this._xValue - Math.Floor( this._yValue[i] );
                    }
                }
                else
                {
                    if ( base.series.YValueType == ChartValueType.Date )
                    {
                        DateTime yDate;
                        if ( yValue[i] is DateTime time )
                            yDate = time;
                        else if ( yValue[i] is double v1 )
                            yDate = DateTime.FromOADate( v1 );
                        else
                            yDate = Convert.ToDateTime( yValue[i], CultureInfo.InvariantCulture ); //This will throw an exception in case when the yValue type is not compatible with the DateTime

                        DateTime date = new(
                            yDate.Year,
                            yDate.Month,
                            yDate.Day,
                            0,
                            0,
                            0,
                            0 );

                        this._yValue[i] = date.ToOADate();
                    }
                    else if ( base.series.YValueType == ChartValueType.Time )
                    {
                        DateTime yTime;
                        if ( yValue[i] is DateTime time1 )
                            yTime = time1;
                        if ( yValue[i] is double v1 )
                            yTime = DateTime.FromOADate( v1 );
                        else
                            yTime = Convert.ToDateTime( yValue[i], CultureInfo.InvariantCulture ); //This will throw an exception in case when the yValue type is not compatible with the DateTime

                        DateTime time = new(
                            1899,
                            12,
                            30,
                            yTime.Hour,
                            yTime.Minute,
                            yTime.Second,
                            yTime.Millisecond );

                        this._yValue[i] = time.ToOADate();
                    }
                }
            }
        }

    }

    /// <summary>
    /// Creates an exact copy of this DataPoint object.
    /// </summary>
    /// <returns>An exact copy of this DataPoint object.</returns>
    public DataPoint Clone()
    {
        // Create new data point
        DataPoint clonePoint = new()
        {
            // Reset series pointer
            series = null,
            pointCustomProperties = this.pointCustomProperties,

            // Copy values
            _xValue = this.XValue,
            _yValue = new double[this._yValue.Length]
        };
        this._yValue.CopyTo( clonePoint._yValue, 0 );
        clonePoint.tempColorIsSet = this.tempColorIsSet;
        clonePoint.isEmptyPoint = this.isEmptyPoint;

        // Copy properties
        foreach ( object key in this.properties.Keys )
        {
            clonePoint.properties.Add( key, this.properties[key] );
        }

        return clonePoint;
    }

    /// <summary>
    /// Resize Y values array.
    /// </summary>
    /// <param name="newSize">New number of Y values in array.</param>
    internal void ResizeYValueArray( int newSize )
    {
        // Create new array
        double[] newArray = new double[newSize];

        // Copy elements
        if ( this._yValue is not null )
        {
            for ( int i = 0; i < ((this._yValue.Length < newSize) ? this._yValue.Length : newSize); i++ )
            {
                newArray[i] = this._yValue[i];
            }
        }

        this._yValue = newArray;
    }

    /// <summary>
    /// Helper function, which returns point value by it's name.
    /// </summary>
    /// <param name="valueName">Point value names. X, Y, Y2,...</param>
    /// <returns>Point value.</returns>
    public double GetValueByName( string valueName )
    {
        // Check arguments
        if ( valueName == null ) throw new ArgumentNullException( nameof( valueName ) );

        valueName = valueName.ToUpper( System.Globalization.CultureInfo.InvariantCulture );
        if ( string.Compare( valueName, "X", StringComparison.Ordinal ) == 0 )
        {
            return this.XValue;
        }
        else if ( valueName.StartsWith( "Y", StringComparison.Ordinal ) )

        {
            if ( valueName.Length == 1 )
            {
                return this.YValues[0];
            }
            else
            {
                int yIndex = 0;
                try
                {
                    yIndex = int.Parse( valueName[1..], System.Globalization.CultureInfo.InvariantCulture ) - 1;
                }
                catch ( Exception )
                {
                    throw new ArgumentException( SR.ExceptionDataPointValueNameInvalid, nameof( valueName ) );
                }

                if ( yIndex < 0 )
                {
                    throw new ArgumentException( SR.ExceptionDataPointValueNameYIndexIsNotPositive, nameof( valueName ) );
                }

                return yIndex >= this.YValues.Length
                    ? throw (new ArgumentException( SR.ExceptionDataPointValueNameYIndexOutOfRange, nameof( valueName ) ))
                    : this.YValues[yIndex];
            }
        }
        else
        {
            throw new ArgumentException( SR.ExceptionDataPointValueNameInvalid, nameof( valueName ) );
        }
    }

    /// <summary>
    /// Replaces predefined keyword inside the string with their values.
    /// </summary>
    /// <param name="strOriginal">Original string with keywords.</param>
    /// <returns>Modified string.</returns>
    internal override string ReplaceKeywords( string strOriginal )
    {
        // Nothing to process
        if ( strOriginal == null || strOriginal.Length == 0 )
            return strOriginal;

        // Replace all "\n" strings with '\n' character
        string result = strOriginal;
        result = result.Replace( "\\n", "\n" );

        // #LABEL - point label
        result = result.Replace( KeywordName.Label, this.Label );

        // #LEGENDTEXT - series name
        result = result.Replace( KeywordName.LegendText, this.LegendText );

        // #AXISLABEL - series name
        result = result.Replace( KeywordName.AxisLabel, this.AxisLabel );

        // #CUSTOMPROPERTY - one of the custom properties by name
        result = DataPoint.ReplaceCustomPropertyKeyword( result, this );

        if ( this.series is not null )
        {
            // #INDEX - point index
            result = result.Replace( KeywordName.Index, this.series.Points.IndexOf( this ).ToString( System.Globalization.CultureInfo.InvariantCulture ) );

            // Replace series keywords
            result = this.series.ReplaceKeywords( result );

            // #PERCENT - percentage of Y value from total
            result = this.series.ReplaceOneKeyword(
                this.Chart,
                this,
                this.Tag,
                ChartElementType.DataPoint,
                result,
                KeywordName.Percent,
                this.YValues[0] / this.series.GetTotalYValue(),
                ChartValueType.Double,
                "P" );

            // #VAL[X] - point value X, Y, Y2, ...
            result = this.series.XValueType == ChartValueType.String
                ? result.Replace( KeywordName.ValX, this.AxisLabel )
                : this.series.ReplaceOneKeyword(
                    this.Chart,
                    this,
                    this.Tag,
                    ChartElementType.DataPoint,
                    result,
                    KeywordName.ValX,
                    this.XValue,
                    this.series.XValueType,
                    "" );

            // remove keywords #VAL? for unexisted Y value indices
            for ( int index = this.YValues.Length; index <= 7; index++ )
            {
                result = this.RemoveOneKeyword( result, KeywordName.ValY + index + 1, SR.FormatErrorString );
            }

            for ( int index = 1; index <= this.YValues.Length; index++ )
            {
                result = this.series.ReplaceOneKeyword(
                    this.Chart,
                    this,
                    this.Tag,
                    ChartElementType.DataPoint,
                    result,
                    KeywordName.ValY + index,
                    this.YValues[index - 1],
                    this.series.YValueType,
                    "" );
            }

            result = this.series.ReplaceOneKeyword(
                this.Chart,
                this,
                this.Tag,
                ChartElementType.DataPoint,
                result,
                KeywordName.ValY,
                this.YValues[0],
                this.series.YValueType,
                "" );

            result = this.series.ReplaceOneKeyword(
                this.Chart,
                this,
                this.Tag,
                ChartElementType.DataPoint,
                result,
                KeywordName.Val,
                this.YValues[0],
                this.series.YValueType,
                "" );
        }

        return result;
    }

    /// <summary>
    /// Removes one keyword from format string.
    /// </summary>
    /// <param name="strOriginal">Original format string</param>
    /// <param name="keyword">The keyword</param>
    /// <param name="strToReplace">String to replace the keyword.</param>
    /// <returns>Modified format string</returns>
    private string RemoveOneKeyword( string strOriginal, string keyword, string strToReplace )
    {
        string result = strOriginal;
        int keyIndex = -1;
        while ( (keyIndex = result.IndexOf( keyword, StringComparison.Ordinal )) != -1 )
        {
            // Get optional format
            int keyEndIndex = keyIndex + keyword.Length;
            if ( result.Length > keyEndIndex && result[keyEndIndex] == '{' )
            {
                int formatEnd = result.IndexOf( '}', keyEndIndex );
                if ( formatEnd == -1 )
                {
                    throw new InvalidOperationException( SR.ExceptionDataSeriesKeywordFormatInvalid( result ) );
                }

                keyEndIndex = formatEnd + 1;
            }
            // Remove keyword string (with optional format)
            result = result.Remove( keyIndex, keyEndIndex - keyIndex );
            if ( !string.IsNullOrEmpty( strToReplace ) )
            {
                result = result.Insert( keyIndex, strToReplace );
            }
        }
        return result;
    }

    /// <summary>
    /// Replaces all "#CUSTOMPROPERTY(XXX)" (where XXX is the custom attribute name) 
    /// keywords in the string provided. 
    /// </summary>
    /// <param name="originalString">String where the keyword need to be replaced.</param>
    /// <param name="properties">DataPoint or Series properties class.</param>
    /// <returns>Converted string.</returns>
    internal static string ReplaceCustomPropertyKeyword( string originalString, DataPointCustomProperties properties )
    {
        string result = originalString;
        int keyStartIndex = -1;
        while ( (keyStartIndex = result.IndexOf( KeywordName.CustomProperty, StringComparison.Ordinal )) >= 0 )
        {
            string attributeValue = string.Empty;
            string attributeName = string.Empty;

            // Forward to the end of the keyword
            int keyEndIndex = keyStartIndex + KeywordName.CustomProperty.Length;

            // An opening bracket '(' must follow
            if ( result.Length > keyEndIndex && result[keyEndIndex] == '(' )
            {
                ++keyEndIndex;
                int attributeNameStartIndex = keyEndIndex;

                // Search for the closing bracket
                int closingBracketIndex = result.IndexOf( ')', keyEndIndex );
                if ( closingBracketIndex >= keyEndIndex )
                {
                    keyEndIndex = closingBracketIndex + 1;
                    attributeName = result.Substring( attributeNameStartIndex, keyEndIndex - attributeNameStartIndex - 1 );

                    // Get attribute value
                    if ( properties.IsCustomPropertySet( attributeName ) )
                    {
                        attributeValue = properties.GetCustomProperty( attributeName );
                    }
                    else
                    {
                        // In case of the DataPoint check if the attribute is set in the parent series
                        if ( properties is DataPoint dataPoint && dataPoint.series is not null )
                        {
                            if ( dataPoint.series.IsCustomPropertySet( attributeName ) )
                            {
                                attributeValue = dataPoint.series.GetCustomProperty( attributeName );
                            }
                        }
                    }
                }
            }

            // Remove keyword string with attribute name
            result = result.Remove( keyStartIndex, keyEndIndex - keyStartIndex );

            // Insert value of the custom attribute
            result = result.Insert( keyStartIndex, attributeValue );
        }

        return result;
    }

    /// <summary>
    /// Returns a <see cref="string"/> that represents the current <see cref="object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="string"/> that represents the current <see cref="object"/>.
    /// </returns>
    internal override string ToStringInternal()
    {
        StringBuilder sb = new();
        _ = sb.AppendFormat( CultureInfo.CurrentCulture, "{{X={0}, ", this.XValue );
        if ( this.YValues.Length == 1 )
        {
            _ = sb.AppendFormat( CultureInfo.CurrentCulture, "Y={0}", this.YValues[0] );
        }
        else
        {
            _ = sb.Append( "Y={" );
            for ( int i = 0; i < this.YValues.Length; i++ )
                _ = i == 0
                    ? sb.AppendFormat( CultureInfo.CurrentCulture, "{0}", this.YValues[i] )
                    : sb.AppendFormat( CultureInfo.CurrentCulture, ", {0}", this.YValues[i] );
            _ = sb.Append( "}" );
        }
        _ = sb.Append( "}" );
        return sb.ToString();
    }
    #endregion

    #region " datapoint properties "

    /// <summary>
    /// X value of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeDataPoint_XValue" ),
    TypeConverter( typeof( DataPointValueConverter ) ),
    DefaultValue( typeof( double ), "0.0" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
]
    public double XValue
    {
        get => this._xValue;
        set
        {
            this._xValue = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// List of Y values of the data point.
    /// </summary>
    [
        SRCategory( "CategoryAttributeData" ),
        SRDescription( "DescriptionAttributeDataPoint_YValues" ),
        Bindable( true ),
        DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
        TypeConverter( typeof( DoubleArrayConverter ) ),
        Editor( typeof( UITypeEditor ), typeof( UITypeEditor ) ),
        RefreshProperties( RefreshProperties.All ),
        SerializationVisibility( SerializationVisibility.Attribute )
    ]
    public double[] YValues
    {
        get => this._yValue;
        set
        {
            if ( value == null )
            {
                // Clear array data
                for ( int i = 0; i < this._yValue.Length; i++ )
                {
                    this._yValue[i] = 0;
                }
            }
            else
            {
                this._yValue = value;
            }
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// A flag which indicates whether the data point is empty.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),

    Bindable( true ),
    SRDescription( "DescriptionAttributeDataPoint_Empty" ),
    DefaultValue( false )
    ]
    public bool IsEmpty
    {
        get => base.isEmptyPoint;
        set
        {
            base.isEmptyPoint = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Name of the data point. This field is reserved for internal use only.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeDataPoint_Name" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public override string Name
    {
        get => "DataPoint";
        set
        {
            //Dont call the base method - the names don't need to be unique
        }
    }

    #endregion
}
/// <summary>
/// Stores properties of one Data Point and Data series.
/// </summary>
[
SRDescription( "DescriptionAttributeDataPointCustomProperties_DataPointCustomProperties" ),
DefaultProperty( "LabelStyle" ),
TypeConverter( typeof( DataPointCustomPropertiesConverter ) )
]
public class DataPointCustomProperties : ChartNamedElement
{
    #region " fields and enumerations "

#pragma warning disable IDE1006 // Naming Styles
    // True indicates data point properties. Otherwise - series.
    internal bool pointCustomProperties = true;

    // Reference to the data point series
    internal Series series;

    // Storage for the custom properties names/values
    internal Hashtable properties = [];

    // Flag indicating that temp. color was set
    internal bool tempColorIsSet;

    // Design time custom properties data
    internal CustomProperties customProperties;

    // IsEmpty point indicator
    internal bool isEmptyPoint;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructtion "

    /// <summary>
    /// DataPointCustomProperties constructor.
    /// </summary>
    public DataPointCustomProperties()
    {
        // Initialize the data series
        this.series = null;
        this.customProperties = new CustomProperties( this );
    }

    /// <summary>
    /// DataPointCustomProperties constructor.
    /// </summary>
    /// <param name="series">The series which the data point belongs to.</param>
    /// <param name="pointProperties">Indicates whether this is a data point custom properties.</param>
    public DataPointCustomProperties( Series series, bool pointProperties ) : base( series, string.Empty )
    {
        // Initialize the data series
        this.series = series;
        this.pointCustomProperties = pointProperties;
        this.customProperties = new CustomProperties( this );
    }

    #endregion

    #region " custom properties methods "

    /// <summary>
    /// Checks if custom property with specified name was set.
    /// </summary>
    /// <param name="name">Name of the custom property to check.</param>
    /// <returns>True if custom property was set.</returns>
    public virtual bool IsCustomPropertySet( string name )
    {
        return this.properties.ContainsKey( name );
    }

    /// <summary>
    /// Checks if the custom property with specified name was set.
    /// </summary>
    /// <param name="property">The CommonCustomProperties object to check for.</param>
    /// <returns>True if attribute was set.</returns>
    internal bool IsCustomPropertySet( CommonCustomProperties property )
    {
        return this.properties.ContainsKey( ( int ) property );
    }

    /// <summary>
    /// Delete the data point custom property with the specified name.
    /// </summary>
    /// <param name="name">Name of the property to delete.</param>
    public virtual void DeleteCustomProperty( string name )
    {
        if ( name == null )
        {
            throw new ArgumentNullException( SR.ExceptionAttributeNameIsEmpty );
        }

        // Check if trying to delete the common attribute
#pragma warning disable CA2263
        string[] attributesNames = CommonCustomProperties.GetNames( typeof( CommonCustomProperties ) );
#pragma warning restore CA2263
        foreach ( string commonName in attributesNames )
        {
            if ( name == commonName )
            {
#pragma warning disable CA2263
                this.DeleteCustomProperty( ( CommonCustomProperties ) Enum.Parse( typeof( CommonCustomProperties ), commonName ) );
#pragma warning restore CA2263
            }
        }

        // Remove attribute
        this.properties.Remove( name );
    }

    /// <summary>
    /// Delete Data Point attribute with specified name.
    /// </summary>
    /// <param name="property">ID of the attribute to delete.</param>
    internal void DeleteCustomProperty( CommonCustomProperties property )
    {
        // Check if trying to delete the common attribute from the series
        if ( !this.pointCustomProperties )
        {
            throw new ArgumentException( SR.ExceptionAttributeUnableToDelete );
        }

        // Remove attribute
        this.properties.Remove( ( int ) property );
    }

    /// <summary>
    /// Gets the data point custom property with the specified name.
    /// </summary>
    /// <param name="name">Name of the property to get.</param>
    /// <returns>Returns the data point custom property with the specified name.  If the requested one is not set, 
    /// the default custom property of the data series will be returned.</returns>
    public virtual string GetCustomProperty( string name )
    {
        if ( !this.IsCustomPropertySet( name ) && this.pointCustomProperties )
        {
            // Check if we are in serialization mode
            bool serializing = false;

            if ( this.Chart != null && this.Chart.serializing )
            {
                serializing = true;
            }

            if ( !serializing )
            {
                if ( this.isEmptyPoint )
                {
                    // Return empty point properties from series
                    return ( string ) this.series.EmptyPointStyle.properties[name];
                }

                // Return properties from series
                return ( string ) this.series.properties[name];
            }
            else
            {
                // Return default properties
                return Series.defaultCustomProperties[name];
            }
        }

        return ( string ) this.properties[name];
    }

    /// <summary>
    /// Checks if data is currently serialized.
    /// </summary>
    /// <returns>True if serialized.</returns>
    internal bool IsSerializing()
    {
        // Check if series object is provided
        if ( this.series == null )
        {
            return true;
        }

        // Check if we are in serialization mode
        return this.Chart != null && this.Chart.serializing;
    }

    /// <summary>
    /// Returns an attribute object of the Data Point. If required attribute is not set
    /// in the Data Point the default attribute of the Data series is returned.
    /// </summary>
    /// <param name="attrib">Attribute name ID.</param>
    /// <returns>Attribute value.</returns>
    internal object GetAttributeObject( CommonCustomProperties attrib )
    {
        // Get series properties
        if ( !this.pointCustomProperties || this.series == null )
        {
            return this.properties[( int ) attrib];
        }

        // Get data point properties
        if ( this.properties.Count == 0 || !this.IsCustomPropertySet( attrib ) )
        {
            // Check if we are in serialization mode
            bool serializing = false;
            if ( this.Chart is not null )
            {
                serializing = this.Chart.serializing;
            }

            if ( !serializing )
            {
                if ( this.isEmptyPoint )
                {
                    // Return empty point properties from series
                    return this.series.EmptyPointStyle.properties[( int ) attrib];
                }

                // Return properties from series
                return this.series.properties[( int ) attrib];
            }
            else
            {
                // Return default properties
                return Series.defaultCustomProperties.properties[( int ) attrib];
            }
        }
        return this.properties[( int ) attrib];
    }

    /// <summary>
    /// Sets a custom property of the data point. 
    /// </summary>
    /// <param name="name">Property name.</param>
    /// <param name="propertyValue">Property value.</param>
    public virtual void SetCustomProperty( string name, string propertyValue )
    {
        this.properties[name] = propertyValue;
    }

    /// <summary>
    /// Sets an attribute of the Data Point as an object. 
    /// </summary>
    /// <param name="attrib">Attribute name ID.</param>
    /// <param name="attributeValue">Attribute new value.</param>
    internal void SetAttributeObject( CommonCustomProperties attrib, object attributeValue )
    {
        this.properties[( int ) attrib] = attributeValue;
    }

    /// <summary>
    /// Set the default properties of the data point.
    /// <param name="clearAll">Indicates that previous properties must be cleared.</param>
    /// </summary>
    public virtual void SetDefault( bool clearAll )
    {
        // If setting defaults for the data series - clear all properties and initialize common one
        if ( !this.pointCustomProperties )
        {
            if ( clearAll )
            {
                this.properties.Clear();
            }

            // !!! IMPORTANT !!!
            // After changing the default value of the common attribute you must also
            // change the DefaultAttribute of the property representing this attribute.
            if ( !this.IsCustomPropertySet( CommonCustomProperties.ToolTip ) )
                this.SetAttributeObject( CommonCustomProperties.ToolTip, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LegendToolTip ) )
                this.SetAttributeObject( CommonCustomProperties.LegendToolTip, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.Color ) )
                this.SetAttributeObject( CommonCustomProperties.Color, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.IsValueShownAsLabel ) )
                this.SetAttributeObject( CommonCustomProperties.IsValueShownAsLabel, false );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerStyle ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerStyle, MarkerStyle.None );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerSize ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerSize, 5 );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerImage ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerImage, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.Label ) )
                this.SetAttributeObject( CommonCustomProperties.Label, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BorderWidth ) )
                this.SetAttributeObject( CommonCustomProperties.BorderWidth, 1 );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BorderDashStyle ) )
                this.SetAttributeObject( CommonCustomProperties.BorderDashStyle, ChartDashStyle.Solid );


            if ( !this.IsCustomPropertySet( CommonCustomProperties.AxisLabel ) )
                this.SetAttributeObject( CommonCustomProperties.AxisLabel, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelFormat ) )
                this.SetAttributeObject( CommonCustomProperties.LabelFormat, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BorderColor ) )
                this.SetAttributeObject( CommonCustomProperties.BorderColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackImage ) )
                this.SetAttributeObject( CommonCustomProperties.BackImage, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackImageWrapMode ) )
                this.SetAttributeObject( CommonCustomProperties.BackImageWrapMode, ChartImageWrapMode.Tile );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackImageAlignment ) )
                this.SetAttributeObject( CommonCustomProperties.BackImageAlignment, ChartImageAlignmentStyle.TopLeft );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackImageTransparentColor ) )
                this.SetAttributeObject( CommonCustomProperties.BackImageTransparentColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackGradientStyle ) )
                this.SetAttributeObject( CommonCustomProperties.BackGradientStyle, GradientStyle.None );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackSecondaryColor ) )
                this.SetAttributeObject( CommonCustomProperties.BackSecondaryColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.BackHatchStyle ) )
                this.SetAttributeObject( CommonCustomProperties.BackHatchStyle, ChartHatchStyle.None );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.Font ) )
                this.SetAttributeObject( CommonCustomProperties.Font, null );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerImageTransparentColor ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerImageTransparentColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerColor ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerBorderColor ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerBorderColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MarkerBorderWidth ) )
                this.SetAttributeObject( CommonCustomProperties.MarkerBorderWidth, 1 );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.MapAreaAttributes ) )
                this.SetAttributeObject( CommonCustomProperties.MapAreaAttributes, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.PostBackValue ) )
                this.SetAttributeObject( CommonCustomProperties.PostBackValue, "" );

            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelForeColor ) )
                this.SetAttributeObject( CommonCustomProperties.LabelForeColor, Color.Black );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelAngle ) )
                this.SetAttributeObject( CommonCustomProperties.LabelAngle, 0 );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelToolTip ) )
                this.SetAttributeObject( CommonCustomProperties.LabelToolTip, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelUrl ) )
                this.SetAttributeObject( CommonCustomProperties.LabelUrl, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelPostBackValue ) )
                this.SetAttributeObject( CommonCustomProperties.LabelPostBackValue, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelMapAreaAttributes ) )
                this.SetAttributeObject( CommonCustomProperties.LabelMapAreaAttributes, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelBackColor ) )
                this.SetAttributeObject( CommonCustomProperties.LabelBackColor, Color.Empty );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelBorderWidth ) )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderWidth, 1 );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelBorderDashStyle ) )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderDashStyle, ChartDashStyle.Solid );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LabelBorderColor ) )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderColor, Color.Empty );

            if ( !this.IsCustomPropertySet( CommonCustomProperties.Url ) )
                this.SetAttributeObject( CommonCustomProperties.Url, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LegendUrl ) )
                this.SetAttributeObject( CommonCustomProperties.LegendUrl, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LegendPostBackValue ) )
                this.SetAttributeObject( CommonCustomProperties.LegendPostBackValue, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LegendText ) )
                this.SetAttributeObject( CommonCustomProperties.LegendText, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.LegendMapAreaAttributes ) )
                this.SetAttributeObject( CommonCustomProperties.LegendMapAreaAttributes, "" );
            if ( !this.IsCustomPropertySet( CommonCustomProperties.IsVisibleInLegend ) )
                this.SetAttributeObject( CommonCustomProperties.IsVisibleInLegend, true );
        }

        // If setting defaults for the data point - clear all properties
        else
        {
            this.properties.Clear();
        }
    }

    #endregion

    #region " datapointcustomproperties properties "

    /// <summary>
    /// Indexer of the custom properties. Returns the DataPointCustomProperties object by index.
    /// </summary>
    /// <param name="index">Index of the custom property.</param>
    public string this[int index]
    {
        get
        {
            int currentIndex = 0;
            foreach ( object key in this.properties.Keys )
            {
                if ( currentIndex == index )
                {
                    if ( key is string keyStr )
                    {
                        return keyStr;
                    }
                    else if ( key is int )
                    {
                        return Enum.GetName( typeof( CommonCustomProperties ), key );
                    }
                    return key.ToString();
                }
                ++currentIndex;
            }
            // we can't throw IndexOutOfRangeException here, it is reserved
            // by the CLR.
            throw new InvalidOperationException();
        }
    }

    /// <summary>
    /// Indexer of the custom properties. Returns the DataPointCustomProperties object by name.
    /// </summary>
    /// <param name="name">Name of the custom property.</param>
    public string this[string name]
    {
        get
        {
            // If attribute is not set in data point - try getting it from the series
            if ( !this.IsCustomPropertySet( name ) && this.pointCustomProperties )
            {
                return this.isEmptyPoint ? ( string ) this.series.EmptyPointStyle.properties[name] : ( string ) this.series.properties[name];
            }
            return ( string ) this.properties[name];
        }
        set
        {
            this.properties[name] = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// The text of the data point label.
    /// </summary>
    [
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    SRCategory( "CategoryAttributeLabel" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLabel" ),
    ]
    public virtual string Label
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.Label ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.Label );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.Label )
                        : this.series.label;
                }
            }
            else
            {
                return this.series.label;
            }
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.Label, value );
            else
                this.series.label = value;

            this.Invalidate( true );
        }
    }

    /// <summary>
    /// The text of X axis label for the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeAxisLabel" ),
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    ]
    public virtual string AxisLabel
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.AxisLabel ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.AxisLabel );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.AxisLabel )
                        : this.series.axisLabel;
                }
            }
            else
            {
                return this.series.axisLabel;
            }
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.AxisLabel, value );
            else
                this.series.axisLabel = value;

            // Set flag that there are non-empty axis labels in series or points
            if ( value.Length > 0 && this.series is not null )
            {
                this.series.noLabelsInPoints = false;
            }

            this.Invalidate( false );
        }
    }

    /// <summary>
    /// Format string of the data point label.
    /// </summary>
    [

    SRCategory( "CategoryAttributeLabel" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLabelFormat" )
    ]
    public string LabelFormat
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelFormat ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.LabelFormat );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelFormat )
                        : this.series.labelFormat;
                }
            }
            else
            {
                return this.series.labelFormat;
            }
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelFormat, value );
            else
                this.series.labelFormat = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// A flag which indicates whether to show the data point's value on the label.
    /// </summary>
    [

    SRCategory( "CategoryAttributeLabel" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeShowLabelAsValue" )
    ]
    public bool IsValueShownAsLabel
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.IsValueShownAsLabel ) )
                {
                    return ( bool ) this.GetAttributeObject( CommonCustomProperties.IsValueShownAsLabel );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return false;
                    }
                    return this.isEmptyPoint
                        ? ( bool ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.IsValueShownAsLabel )
                        : this.series.showLabelAsValue;
                }
            }
            else
            {
                return this.series.showLabelAsValue;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.IsValueShownAsLabel, value );
            else
                this.series.showLabelAsValue = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// Color of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeColor4" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color Color
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.Color ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.Color );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.Color )
                        : this.series.color;
                }
            }
            else
            {
                return this.series.color;
            }
        }
        set
        {
            // Remove the temp color flag
            this.tempColorIsSet = false;

            if ( value == Color.Empty && this.pointCustomProperties )
            {
                this.DeleteCustomProperty( CommonCustomProperties.Color );
            }
            else
            {
                if ( this.pointCustomProperties )
                    this.SetAttributeObject( CommonCustomProperties.Color, value );
                else
                    this.series.color = value;
                this.Invalidate( true );
            }
        }
    }

    /// <summary>
    /// Border color of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BorderColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BorderColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.BorderColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BorderColor )
                        : this.series.borderColor;
                }
            }
            else
            {
                return this.series.borderColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BorderColor, value );
            else
                this.series.borderColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Border style of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeBorderDashStyle" )
    ]
    public ChartDashStyle BorderDashStyle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BorderDashStyle ) )
                {
                    return ( ChartDashStyle ) this.GetAttributeObject( CommonCustomProperties.BorderDashStyle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return ChartDashStyle.Solid;
                    }
                    return this.isEmptyPoint
                        ? ( ChartDashStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BorderDashStyle )
                        : this.series.borderDashStyle;
                }
            }
            else
            {
                return this.series.borderDashStyle;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BorderDashStyle, value );
            else
                this.series.borderDashStyle = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Border width of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeBorderWidth" ),
    ]
    public int BorderWidth
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BorderWidth ) )
                {
                    return ( int ) this.GetAttributeObject( CommonCustomProperties.BorderWidth );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return 1;
                    }
                    return this.isEmptyPoint
                        ? ( int ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BorderWidth )
                        : this.series.borderWidth;
                }
            }
            else
            {
                return this.series.borderWidth;
            }
        }
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionBorderWidthIsNotPositive );
            }
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BorderWidth, value );
            else
                this.series.borderWidth = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Background image of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeBackImage" ),
    Editor( typeof( ImageValueEditor ), typeof( UITypeEditor ) ),
    ]
    public string BackImage
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackImage ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.BackImage );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackImage )
                        : this.series.backImage;
                }
            }
            else
            {
                return this.series.backImage;
            }
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackImage, value );
            else
                this.series.backImage = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the drawing mode of the background image.
    /// </summary>
    /// <value>
    /// A <see cref="ChartImageWrapMode"/> value that defines the drawing mode of the image. 
    /// </value>
    [
SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
SRDescription( "DescriptionAttributeImageWrapMode" )
]
    public ChartImageWrapMode BackImageWrapMode
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackImageWrapMode ) )
                {
                    return ( ChartImageWrapMode ) this.GetAttributeObject( CommonCustomProperties.BackImageWrapMode );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return ChartImageWrapMode.Tile;
                    }
                    return this.isEmptyPoint
                        ? ( ChartImageWrapMode ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackImageWrapMode )
                        : this.series.backImageWrapMode;
                }
            }
            else
            {
                return this.series.backImageWrapMode;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackImageWrapMode, value );
            else
                this.series.backImageWrapMode = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets a color which will be replaced with a transparent color while drawing the background image.
    /// </summary>
    /// <value>
    /// A <see cref="Color"/> value which will be replaced with a transparent color while drawing the image.
    /// </value>
    [
SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
NotifyParentProperty( true ),
SRDescription( "DescriptionAttributeImageTransparentColor" ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public Color BackImageTransparentColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackImageTransparentColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.BackImageTransparentColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackImageTransparentColor )
                        : this.series.backImageTransparentColor;
                }
            }
            else
            {
                return this.series.backImageTransparentColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackImageTransparentColor, value );
            else
                this.series.backImageTransparentColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the alignment of the background image which is used by ClampUnscale drawing mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackImageAlign" )
    ]
    public ChartImageAlignmentStyle BackImageAlignment
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackImageAlignment ) )
                {
                    return ( ChartImageAlignmentStyle ) this.GetAttributeObject( CommonCustomProperties.BackImageAlignment );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return ChartImageAlignmentStyle.TopLeft;
                    }
                    return this.isEmptyPoint
                        ? ( ChartImageAlignmentStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackImageAlignment )
                        : this.series.backImageAlignment;
                }
            }
            else
            {
                return this.series.backImageAlignment;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackImageAlignment, value );
            else
                this.series.backImageAlignment = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the background gradient style.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
SRDescription( "DescriptionAttributeBackGradientStyle" ),
Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
]
    public GradientStyle BackGradientStyle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackGradientStyle ) )
                {
                    return ( GradientStyle ) this.GetAttributeObject( CommonCustomProperties.BackGradientStyle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return GradientStyle.None;
                    }
                    return this.isEmptyPoint
                        ? ( GradientStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackGradientStyle )
                        : this.series.backGradientStyle;
                }
            }
            else
            {
                return this.series.backGradientStyle;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackGradientStyle, value );
            else
                this.series.backGradientStyle = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the secondary background color.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
SRDescription( "DescriptionAttributeBackSecondaryColor" ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public Color BackSecondaryColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackSecondaryColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.BackSecondaryColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackSecondaryColor )
                        : this.series.backSecondaryColor;
                }
            }
            else
            {
                return this.series.backSecondaryColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackSecondaryColor, value );
            else
                this.series.backSecondaryColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the background hatch style.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
SRDescription( "DescriptionAttributeBackHatchStyle" ),
Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
]
    public ChartHatchStyle BackHatchStyle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.BackHatchStyle ) )
                {
                    return ( ChartHatchStyle ) this.GetAttributeObject( CommonCustomProperties.BackHatchStyle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return ChartHatchStyle.None;
                    }
                    return this.isEmptyPoint
                        ? ( ChartHatchStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.BackHatchStyle )
                        : this.series.backHatchStyle;
                }
            }
            else
            {
                return this.series.backHatchStyle;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.BackHatchStyle, value );
            else
                this.series.backHatchStyle = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the font of the data point.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeFont" )
    ]
    public Font Font
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.Font ) )
                {
                    if ( this.GetAttributeObject( CommonCustomProperties.Font ) is Font font )
                        return font;
                }

                if ( this.IsSerializing() )
                {
                    return this.series.FontCache.DefaultFont;
                }

                return this.isEmptyPoint ? this.series.EmptyPointStyle.Font : this.series.font;
            }
            else
            {
                return this.series.font;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.Font, value );
            else
                this.series.font = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// Gets or sets the label color.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeFontColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color LabelForeColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelForeColor ) )
                {
                    Color color = ( Color ) this.GetAttributeObject( CommonCustomProperties.LabelForeColor );
                    return color;
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Black;
                    }
                    if ( this.isEmptyPoint )
                    {
                        return ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelForeColor );
                    }
                    return SystemInformation.HighContrast ? Drawing.SystemColors.WindowText : this.series.fontColor;
                }
            }
            else
            {
                return this.series.fontColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelForeColor, value );
            else
                this.series.fontColor = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// Gets or sets the angle of the label.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( SR.Keys.DescriptionAttributeLabel_FontAngle )
    ]
    public int LabelAngle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelAngle ) )
                {
                    return ( int ) this.GetAttributeObject( CommonCustomProperties.LabelAngle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return 0;
                    }
                    return this.isEmptyPoint
                        ? ( int ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelAngle )
                        : this.series.fontAngle;
                }
            }
            else
            {
                return this.series.fontAngle;
            }
        }
        set
        {
            if ( value is < (-90) or > 90 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionAngleRangeInvalid );
            }
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelAngle, value );
            else
                this.series.fontAngle = value;
            this.Invalidate( false );
        }
    }

    /// <summary>
    /// Gets or sets the marker style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerStyle4" ),
    Editor( typeof( MarkerStyleEditor ), typeof( UITypeEditor ) ),
    RefreshProperties( RefreshProperties.All )
    ]
    public MarkerStyle MarkerStyle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerStyle ) )
                {
                    return ( MarkerStyle ) this.GetAttributeObject( CommonCustomProperties.MarkerStyle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return MarkerStyle.None;
                    }
                    return this.isEmptyPoint
                        ? ( MarkerStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerStyle )
                        : this.series.markerStyle;
                }
            }
            else
            {
                return this.series.markerStyle;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerStyle, value );
            else
                this.series.markerStyle = value;

            if ( this is Series thisSeries )
            {
                thisSeries.tempMarkerStyleIsSet = false;
            }
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the size of the marker.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerSize" ),
    RefreshProperties( RefreshProperties.All )
    ]
    public int MarkerSize
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerSize ) )
                {
                    return ( int ) this.GetAttributeObject( CommonCustomProperties.MarkerSize );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return 5;
                    }
                    return this.isEmptyPoint
                        ? ( int ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerSize )
                        : this.series.markerSize;
                }
            }
            else
            {
                return this.series.markerSize;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerSize, value );
            else
                this.series.markerSize = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the marker image.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerImage" ),
    Editor( typeof( ImageValueEditor ), typeof( UITypeEditor ) ),
    RefreshProperties( RefreshProperties.All )
    ]
    public string MarkerImage
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerImage ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.MarkerImage );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerImage )
                        : this.series.markerImage;
                }
            }
            else
            {
                return this.series.markerImage;
            }
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerImage, value );
            else
                this.series.markerImage = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the color which will be replaced with a transparent color while drawing the marker image.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeImageTransparentColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    RefreshProperties( RefreshProperties.All )
    ]
    public Color MarkerImageTransparentColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerImageTransparentColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.MarkerImageTransparentColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerImageTransparentColor )
                        : this.series.markerImageTransparentColor;
                }
            }
            else
            {
                return this.series.markerImageTransparentColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerImageTransparentColor, value );
            else
                this.series.markerImageTransparentColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the marker color.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerColor3" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    RefreshProperties( RefreshProperties.All )
    ]
    public Color MarkerColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.MarkerColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerColor )
                        : this.series.markerColor;
                }
            }
            else
            {
                return this.series.markerColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerColor, value );
            else
                this.series.markerColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Gets or sets the border color of the marker.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    RefreshProperties( RefreshProperties.All )
    ]
    public Color MarkerBorderColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerBorderColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.MarkerBorderColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    return this.isEmptyPoint
                        ? ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerBorderColor )
                        : this.series.markerBorderColor;
                }
            }
            else
            {
                return this.series.markerBorderColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerBorderColor, value );
            else
                this.series.markerBorderColor = value;
            this.Invalidate( true );
        }
    }



    /// <summary>
    /// Gets or sets the border width of the marker.
    /// </summary>
    [

    SRCategory( "CategoryAttributeMarker" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeMarkerBorderWidth" )
    ]
    public int MarkerBorderWidth
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.MarkerBorderWidth ) )
                {
                    return ( int ) this.GetAttributeObject( CommonCustomProperties.MarkerBorderWidth );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return 1;
                    }
                    return this.isEmptyPoint
                        ? ( int ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.MarkerBorderWidth )
                        : this.series.markerBorderWidth;
                }
            }
            else
            {
                return this.series.markerBorderWidth;
            }
        }
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionBorderWidthIsNotPositive );
            }
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.MarkerBorderWidth, value );
            else
                this.series.markerBorderWidth = value;
            this.Invalidate( true );
        }
    }



    /// <summary>
    /// Gets or sets the extended custom properties of the data point.
    /// Extended custom properties can be specified in the following format: 
    /// AttrName1=Value1, AttrName2=Value2, ...  
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( false ),
    SRDescription( "DescriptionAttributeCustomAttributesExtended" ),
    DefaultValue( null ),
    RefreshProperties( RefreshProperties.All ),
    NotifyParentProperty( true ),
    DesignOnly( true ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DisplayName( "CustomProperties" )
    ]
    public CustomProperties CustomPropertiesExtended
    {
        set => this.customProperties = value;
        get => this.customProperties;
    }

    /// <summary>
    /// Gets or sets the custom properties of the data point.
    /// Custom properties can be specified in the following format: 
    /// AttrName1=Value1, AttrName2=Value2, ...  
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeCustomAttributesExtended" ),
    DefaultValue( "" )
    ]
    public string CustomProperties
    {
        get
        {
            // Save all custom properties in a string
            string result = "";
#pragma warning disable CA2263
            string[] attributesNames = CommonCustomProperties.GetNames( typeof( CommonCustomProperties ) );
#pragma warning restore CA2263
            for ( int i = this.properties.Count - 1; i >= 0; i-- )
            {
                if ( this[i] is not null )
                {
                    string attributeName = this[i];

                    // Check if attribute is custom
                    bool customAttribute = true;
                    foreach ( string name in attributesNames )
                    {
                        if ( string.Compare( attributeName, name, StringComparison.OrdinalIgnoreCase ) == 0 )
                        {
                            customAttribute = false;
                            break;
                        }
                    }

                    // Add custom attribute to the string
                    if ( customAttribute && this.properties[attributeName] is not null )
                    {
                        if ( result.Length > 0 )
                        {
                            result += ", ";
                        }
                        string attributeValue = this.properties[attributeName].ToString().Replace( ",", "\\," );
                        attributeValue = attributeValue.Replace( "=", "\\=" );

                        result += attributeName + "=" + attributeValue;
                    }
                }
            }

            return result;
        }
        set
        {
            // Replace NULL with empty string
            value ??= string.Empty;

            // Copy all common properties to the new collection
            Hashtable newAttributes = [];
#pragma warning disable CA2263
            Array enumValues = Enum.GetValues( typeof( CommonCustomProperties ) );
#pragma warning restore CA2263
            foreach ( object val in enumValues )
            {
                if ( this.IsCustomPropertySet( ( CommonCustomProperties ) val ) )
                {
                    newAttributes[( int ) val] = this.properties[( int ) val];
                }
            }

            if ( value.Length > 0 )
            {
                // Replace commas in value string
                value = value.Replace( "\\,", "\\x45" );
                value = value.Replace( "\\=", "\\x46" );

                // Add new custom properties
                string[] nameValueStrings = value.Split( ',' );
                foreach ( string nameValue in nameValueStrings )
                {
                    string[] values = nameValue.Split( '=' );

                    // Check format
                    if ( values.Length != 2 )
                    {
                        throw new FormatException( SR.ExceptionAttributeInvalidFormat );
                    }

                    // Check for empty name or value
                    values[0] = values[0].Trim();
                    values[1] = values[1].Trim();
                    if ( values[0].Length == 0 )
                    {
                        throw new FormatException( SR.ExceptionAttributeInvalidFormat );
                    }

                    // Check if value already defined
                    foreach ( object existingAttributeName in newAttributes.Keys )
                    {
                        if ( existingAttributeName is string existingAttributeNameStr )
                        {
                            if ( string.Compare( existingAttributeNameStr, values[0], StringComparison.OrdinalIgnoreCase ) == 0 )
                            {
                                throw new FormatException( SR.ExceptionAttributeNameIsNotUnique( values[0] ) );
                            }
                        }
                    }

                    string newValue = values[1].Replace( "\\x45", "," );
                    newAttributes[values[0]] = newValue.Replace( "\\x46", "=" );

                }
            }
            this.properties = newAttributes;
            this.Invalidate( true );
        }
    }

    #endregion

    #region " imapareaattributesutes properties implementation "

    /// <summary>
    /// Tooltip.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMapArea" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeToolTip" ),
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    ]
    public string ToolTip
    {
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.ToolTip, value );
            else
                this.series.toolTip = value;

            if ( this.Chart != null && this.Chart.selection is not null )
            {
                this.Chart.selection.enabledChecked = false;
            }
        }
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.ToolTip ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.ToolTip );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.ToolTip )
                        : this.series.toolTip;
                }
            }
            else
            {
                return this.series.toolTip;
            }
        }
    }

    /// <summary>
    /// Replaces predefined keyword inside the string with their values.
    /// </summary>
    /// <param name="strOriginal">Original string with keywords.</param>
    /// <returns>Modified string.</returns>
    internal virtual string ReplaceKeywords( string strOriginal )
    {
        return strOriginal;
    }

    #endregion

    #region " legend properties "

    /// <summary>
    /// Indicates whether the item is shown in the legend.
    /// </summary>
    [
SRCategory( "CategoryAttributeLegend" ),
Bindable( true ),
SRDescription( "DescriptionAttributeShowInLegend" )
]
    public bool IsVisibleInLegend
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.IsVisibleInLegend ) )
                {
                    return ( bool ) this.GetAttributeObject( CommonCustomProperties.IsVisibleInLegend );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return true;
                    }
                    return this.isEmptyPoint
                        ? ( bool ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.IsVisibleInLegend )
                        : this.series.showInLegend;
                }
            }
            else
            {
                return this.series.showInLegend;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.IsVisibleInLegend, value );
            else
                this.series.showInLegend = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Text of the item in the legend
    /// </summary>
    [
    SRCategory( "CategoryAttributeLegend" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLegendText" ),
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    ]
    public string LegendText
    {
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LegendText, value );
            else
                this.series.legendText = value;
            this.Invalidate( true );
        }
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LegendText ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.LegendText );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LegendText )
                        : this.series.legendText;
                }
            }
            else
            {
                return this.series.legendText;
            }
        }
    }

    /// <summary>
    /// Tooltip of the item in the legend
    /// </summary>
    [
    SRCategory( "CategoryAttributeLegend" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLegendToolTip" ),
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    ]
    public string LegendToolTip
    {
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LegendToolTip, value );
            else
                this.series.legendToolTip = value;

            if ( this.Chart != null && this.Chart.selection is not null )
            {
                this.Chart.selection.enabledChecked = false;
            }
        }
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LegendToolTip ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.LegendToolTip );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LegendToolTip )
                        : this.series.legendToolTip;
                }
            }
            else
            {
                return this.series.legendToolTip;
            }
        }
    }



    /// <summary>
    /// Background color of the data point label.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLabelBackColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    DefaultValue( typeof( Color ), "" ),
    ]
    public Color LabelBackColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelBackColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.LabelBackColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    if ( this.isEmptyPoint )
                    {
                        return ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelBackColor );
                    }
                    return SystemInformation.HighContrast ? Drawing.SystemColors.Window : this.series.labelBackColor;
                }
            }
            else
            {
                return this.series.labelBackColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelBackColor, value );
            else
                this.series.labelBackColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Border color of the data point label.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    DefaultValue( typeof( Color ), "" ),
    ]
    public Color LabelBorderColor
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelBorderColor ) )
                {
                    return ( Color ) this.GetAttributeObject( CommonCustomProperties.LabelBorderColor );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return Color.Empty;
                    }
                    if ( this.isEmptyPoint )
                    {
                        return ( Color ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelBorderColor );
                    }
                    return SystemInformation.HighContrast ? Drawing.SystemColors.ActiveBorder : this.series.labelBorderColor;
                }
            }
            else
            {
                return this.series.labelBorderColor;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderColor, value );
            else
                this.series.labelBorderColor = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Border style of the label.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabelAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLabelBorderDashStyle" )
    ]
    public ChartDashStyle LabelBorderDashStyle
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelBorderDashStyle ) )
                {
                    return ( ChartDashStyle ) this.GetAttributeObject( CommonCustomProperties.LabelBorderDashStyle );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return ChartDashStyle.Solid;
                    }
                    return this.isEmptyPoint
                        ? ( ChartDashStyle ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelBorderDashStyle )
                        : this.series.labelBorderDashStyle;
                }
            }
            else
            {
                return this.series.labelBorderDashStyle;
            }
        }
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderDashStyle, value );
            else
                this.series.labelBorderDashStyle = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Border width of the label.
    /// </summary>
    [
SRCategory( "CategoryAttributeLabelAppearance" ),
Bindable( true ),
SRDescription( "DescriptionAttributeBorderWidth" )
]
    public int LabelBorderWidth
    {
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelBorderWidth ) )
                {
                    return ( int ) this.GetAttributeObject( CommonCustomProperties.LabelBorderWidth );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return 1;
                    }
                    return this.isEmptyPoint
                        ? ( int ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelBorderWidth )
                        : this.series.labelBorderWidth;
                }
            }
            else
            {
                return this.series.labelBorderWidth;
            }
        }
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionLabelBorderIsNotPositive );
            }
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelBorderWidth, value );
            else
                this.series.labelBorderWidth = value;
            this.Invalidate( true );
        }
    }

    /// <summary>
    /// Tooltip of the data point label.
    /// </summary>
    [
    SRCategory( "CategoryAttributeLabel" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeLabelToolTip" ),
    Editor( typeof( KeywordsStringEditor ), typeof( UITypeEditor ) ),
    ]
    public string LabelToolTip
    {
        set
        {
            if ( this.pointCustomProperties )
                this.SetAttributeObject( CommonCustomProperties.LabelToolTip, value );
            else
                this.series.labelToolTip = value;

            if ( this.Chart != null && this.Chart.selection is not null )
            {
                this.Chart.selection.enabledChecked = false;
            }
        }
        get
        {
            if ( this.pointCustomProperties )
            {
                if ( this.properties.Count != 0 && this.IsCustomPropertySet( CommonCustomProperties.LabelToolTip ) )
                {
                    return ( string ) this.GetAttributeObject( CommonCustomProperties.LabelToolTip );
                }
                else
                {
                    if ( this.IsSerializing() )
                    {
                        return "";
                    }
                    return this.isEmptyPoint
                        ? ( string ) this.series.EmptyPointStyle.GetAttributeObject( CommonCustomProperties.LabelToolTip )
                        : this.series.labelToolTip;
                }
            }
            else
            {
                return this.series.labelToolTip;
            }
        }
    }

    #endregion

    #region " serialization control "



    private bool CheckIfSerializationRequired( CommonCustomProperties attribute )
    {
        if ( this is DataPoint )
        {
            return this.IsCustomPropertySet( attribute );
        }
        else
        {
            object attr1 = this.GetAttributeObject( attribute );
            object attr2 = Series.defaultCustomProperties.GetAttributeObject( attribute );
            return attr1 != null && attr2 != null && !attr1.Equals( attr2 );
        }
    }

    private void ResetProperty( CommonCustomProperties attribute )
    {
        if ( this is DataPoint )
        {
            this.DeleteCustomProperty( attribute );
        }
        else
        {
            this.SetAttributeObject( attribute, Series.defaultCustomProperties.GetAttributeObject( attribute ) );
        }
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabel()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.Label )
            : !string.IsNullOrEmpty( this.series.label );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeAxisLabel()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.AxisLabel )
            : !string.IsNullOrEmpty( this.series.axisLabel );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelFormat()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelFormat )
            : !string.IsNullOrEmpty( this.series.labelFormat );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeIsValueShownAsLabel()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.IsValueShownAsLabel )
            : this.series.showLabelAsValue;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.Color )
            : this.series.color != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBorderColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BorderColor )
            : this.series.borderColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBorderDashStyle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BorderDashStyle )
            : this.series.borderDashStyle != ChartDashStyle.Solid;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBorderWidth()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BorderWidth )
            : this.series.borderWidth != 1;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerBorderWidth()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerBorderWidth )
            : this.series.markerBorderWidth != 1;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackImage()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackImage )
            : !string.IsNullOrEmpty( this.series.backImage );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackImageWrapMode()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackImageWrapMode )
            : this.series.backImageWrapMode != ChartImageWrapMode.Tile;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackImageTransparentColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackImageTransparentColor )
            : this.series.backImageTransparentColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackImageAlignment()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackImageAlignment )
            : this.series.backImageAlignment != ChartImageAlignmentStyle.TopLeft;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackGradientStyle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackGradientStyle )
            : this.series.backGradientStyle != GradientStyle.None;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackSecondaryColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackSecondaryColor )
            : this.series.backSecondaryColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeBackHatchStyle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.BackHatchStyle )
            : this.series.backHatchStyle != ChartHatchStyle.None;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeFont()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.Font )
            : this.series.font != this.series.FontCache.DefaultFont;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>
    internal bool ShouldSerializeLabelForeColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelForeColor )
            : this.series.fontColor != Color.Black;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelAngle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelAngle )
            : this.series.fontAngle != 0f;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerStyle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerStyle )
            : this.series.markerStyle != MarkerStyle.None;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerSize()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerSize )
            : this.series.markerSize != 5;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerImage()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerImage )
            : !string.IsNullOrEmpty( this.series.markerImage );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerImageTransparentColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerImageTransparentColor )
            : this.series.markerImageTransparentColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerColor )
            : this.series.markerColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeMarkerBorderColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.MarkerBorderColor )
            : this.series.markerBorderColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeToolTip()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.ToolTip )
            : !string.IsNullOrEmpty( this.series.toolTip );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>
    internal bool ShouldSerializeIsVisibleInLegend()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.IsVisibleInLegend )
            : !this.series.showInLegend;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLegendText()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LegendText )
            : !string.IsNullOrEmpty( this.series.legendText );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLegendToolTip()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LegendToolTip )
            : !string.IsNullOrEmpty( this.series.legendToolTip );
    }



    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelToolTip()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelToolTip )
            : !string.IsNullOrEmpty( this.series.labelToolTip );
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelBackColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelBackColor )
            : this.series.labelBackColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelBorderColor()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelBorderColor )
            : this.series.labelBorderColor != Color.Empty;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelBorderDashStyle()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelBorderDashStyle )
            : this.series.labelBorderDashStyle != ChartDashStyle.Solid;
    }

    /// <summary>
    /// Returns true if property should be serialized.
    /// </summary>

    internal bool ShouldSerializeLabelBorderWidth()
    {
        return this.pointCustomProperties
            ? this.CheckIfSerializationRequired( CommonCustomProperties.LabelBorderWidth )
            : this.series.labelBorderWidth != 1;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabel()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.Label );
        else
            this.series.label = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetAxisLabel()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.AxisLabel );
        else
            this.series.axisLabel = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelFormat()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelFormat );
        else
            this.series.labelFormat = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    public void ResetIsValueShownAsLabel()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.IsValueShownAsLabel );
        else
            this.series.IsValueShownAsLabel = false;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.Color );
        else
            this.series.color = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBorderColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BorderColor );
        else
            this.series.borderColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBorderDashStyle()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BorderDashStyle );
        else
            this.series.borderDashStyle = ChartDashStyle.Solid;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBorderWidth()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BorderWidth );
        else
            this.series.borderWidth = 1;
    }



    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerBorderWidth()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerBorderWidth );
        else
            this.series.markerBorderWidth = 1;
    }



    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBackImage()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BackImage );
        else
            this.series.backImage = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBackImageWrapMode()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BackImageWrapMode );
        else
            this.series.backImageWrapMode = ChartImageWrapMode.Tile;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBackImageTransparentColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BackImageTransparentColor );
        else
            this.series.backImageTransparentColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBackSecondaryColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BackSecondaryColor );
        else
            this.series.backSecondaryColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetBackHatchStyle()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.BackHatchStyle );
        else
            this.series.backHatchStyle = ChartHatchStyle.None;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetFont()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.Font );
        else
        {
            this.series.font = this.series.FontCache.DefaultFont;
        }
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelAngle()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelAngle );
        else
            this.series.fontAngle = 0;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerStyle()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerStyle );
        else
            this.series.markerStyle = MarkerStyle.None;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerSize()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerSize );
        else
            this.series.markerSize = 5;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerImage()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerImage );
        else
            this.series.markerImage = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerImageTransparentColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerImageTransparentColor );
        else
            this.series.markerImageTransparentColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerColor );
        else
            this.series.markerColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetMarkerBorderColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.MarkerBorderColor );
        else
            this.series.markerBorderColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetToolTip()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.ToolTip );
        else
            this.series.toolTip = "";

        if ( this.Chart != null && this.Chart.selection is not null )
        {
            this.Chart.selection.enabledChecked = false;
        }
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>
    public void ResetIsVisibleInLegend()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.IsVisibleInLegend );
        else
            this.series.showInLegend = true;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLegendText()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LegendText );
        else
            this.series.legendText = "";
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLegendToolTip()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LegendToolTip );
        else
            this.series.legendToolTip = "";

        if ( this.Chart != null && this.Chart.selection is not null )
        {
            this.Chart.selection.enabledChecked = false;
        }
    }



    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelBackColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelBackColor );
        else
            this.series.labelBackColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelBorderColor()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelBorderColor );
        else
            this.series.labelBorderColor = Color.Empty;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelBorderDashStyle()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelBorderDashStyle );
        else
            this.series.labelBorderDashStyle = ChartDashStyle.Solid;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelBorderWidth()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelBorderWidth );
        else
            this.series.labelBorderWidth = 1;
    }

    /// <summary>
    /// Resets property to its default value.
    /// </summary>

    internal void ResetLabelToolTip()
    {
        if ( this.pointCustomProperties )
            this.ResetProperty( CommonCustomProperties.LabelToolTip );
        else
            this.series.labelToolTip = "";

        if ( this.Chart != null && this.Chart.selection is not null )
        {
            this.Chart.selection.enabledChecked = false;
        }
    }

    #endregion

    #region " invalidating method "

    /// <summary>
    /// Invalidate chart area.
    /// </summary>
    /// <param name="invalidateLegend">Invalidate legend area only.</param>
    internal void Invalidate( bool invalidateLegend )
    {
        if ( this.series is not null )
        {
            this.series.Invalidate( true, invalidateLegend );
        }
        else
        {
            if ( this is Series thisSeries )
            {
                thisSeries.Invalidate( true, invalidateLegend );
            }
        }
    }

    #endregion
}
/// <summary>
/// Class stores additional information about the data point in 3D space.
/// </summary>
internal class DataPoint3D
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    /// <summary>
    /// Reference to the 2D data point object
    /// </summary>
    internal DataPoint dataPoint;

    /// <summary>
    /// Data point index.
    /// </summary>
    internal int index;

    /// <summary>
    /// Point X position in relative coordinates.
    /// </summary>
    internal double xPosition;

    /// <summary>
    /// Point Y position in relative coordinates.
    /// </summary>
    internal double yPosition;

    /// <summary>
    /// Point X center position in relative coordinates. Used for side-by-side charts.
    /// </summary>
    internal double xCenterVal;

    /// <summary>
    /// Point Z position in relative coordinates.
    /// </summary>
    internal float zPosition;

    /// <summary>
    /// Point width.
    /// </summary>
    internal double width;

    /// <summary>
    /// Point height.
    /// </summary>
    internal double height;

    /// <summary>
    /// Point depth.
    /// </summary>
    internal float depth;

    /// <summary>
    /// Indicates that point belongs to indexed series.
    /// </summary>
    internal bool indexedSeries;
#pragma warning restore IDE1006 // Naming Styles


    #endregion
}
/// <summary>
/// Design-time representation of the CustomProperties.
/// This class is used instead of the string "CustomProperties"
/// property at design time and supports expandable list
/// of custom properties.
/// </summary>
[TypeConverter( typeof( CustomPropertiesTypeConverter ) )]
[EditorBrowsable( EditorBrowsableState.Never )]
public class CustomProperties
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Reference to the properties class
    internal DataPointCustomProperties m_DataPointCustomProperties;
#pragma warning restore IDE1006 // Naming Styles

    #endregion // Fields

    #region " constructor "

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="properties">Attributes object.</param>
    internal CustomProperties( DataPointCustomProperties properties ) => this.m_DataPointCustomProperties = properties;

    #endregion // Constructor

    #region " properties "

    internal virtual DataPointCustomProperties DataPointCustomProperties
    {
        get => this.m_DataPointCustomProperties;
        set => this.m_DataPointCustomProperties = value;

    }

    #endregion //Properties

    #region " methods "

    /// <summary>
    /// Gets a comma separated string of user defined custom properties.
    /// </summary>
    /// <returns>Comma separated string of user defined custom properties.</returns>
    internal virtual string GetUserDefinedCustomProperties()
    {
        return this.GetUserDefinedCustomProperties( true );
    }

    /// <summary>
    /// Gets a comma separated string of user defined or non-user defined custom properties.
    /// </summary>
    /// <param name="userDefined">True if user defined properties must be returned.</param>
    /// <returns>Comma separated string of user defined custom properties.</returns>
    internal virtual string GetUserDefinedCustomProperties( bool userDefined )
    {
        // Get comma separated string of custom properties
        string customAttribute = this.DataPointCustomProperties.CustomProperties;
        string userDefinedCustomAttribute = string.Empty;

        // Get custom attribute registry
        CustomPropertyRegistry registry = ( CustomPropertyRegistry ) this.DataPointCustomProperties.Common.container.GetService( typeof( CustomPropertyRegistry ) );

        // Replace commas in value string
        customAttribute = customAttribute.Replace( "\\,", "\\x45" );
        customAttribute = customAttribute.Replace( "\\=", "\\x46" );

        // Split custom properties by commas into individual properties
        if ( customAttribute.Length > 0 )
        {
            string[] nameValueStrings = customAttribute.Split( ',' );
            foreach ( string nameValue in nameValueStrings )
            {
                string[] values = nameValue.Split( '=' );

                // Check format
                if ( values.Length != 2 )
                {
                    throw new FormatException( SR.ExceptionAttributeInvalidFormat );
                }

                // Check for empty name or value
                values[0] = values[0].Trim();
                values[1] = values[1].Trim();
                if ( values[0].Length == 0 )
                {
                    throw new FormatException( SR.ExceptionAttributeInvalidFormat );
                }

                // Check if attribute is registered or user defined
                bool userDefinedAttribute = true;
                foreach ( CustomPropertyInfo info in registry.registeredCustomProperties )
                {
                    if ( string.Compare( info.Name, values[0], StringComparison.OrdinalIgnoreCase ) == 0 )
                    {
                        userDefinedAttribute = false;
                    }
                }

                // Copy attribute into the output string
                if ( userDefinedAttribute == userDefined )
                {
                    if ( userDefinedCustomAttribute.Length > 0 )
                    {
                        userDefinedCustomAttribute += ", ";
                    }

                    string val = values[1].Replace( "\\x45", "," );
                    val = val.Replace( "\\x46", "=" );
                    userDefinedCustomAttribute += values[0] + "=" + val;
                }
            }
        }

        return userDefinedCustomAttribute;
    }

    /// <summary>
    /// Sets user defined custom properties without cleaning registered properties.
    /// </summary>
    /// <param name="val">New user defined properties.</param>
    internal virtual void SetUserDefinedAttributes( string val )
    {
        // Get non-user defined custom properties
        string properties = this.GetUserDefinedCustomProperties( false );

        // Check if new string is empty
        if ( val.Length > 0 )
        {
            // Add comma at the end
            if ( properties.Length > 0 )
            {
                properties += ", ";
            }

            // Add new user defined properties
            properties += val;
        }

        // Set new custom attribute string
        this.DataPointCustomProperties.CustomProperties = properties;
    }

    #endregion // Methods
}


