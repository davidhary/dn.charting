
using System.ComponentModel;

namespace System.Windows.Forms.DataVisualization.Charting;

[AttributeUsage( AttributeTargets.All )]
internal sealed class SRCategoryAttribute( string category ) : CategoryAttribute( category )
{
    protected override string GetLocalizedString( string value )
    {
        return SR.Keys.GetString( value );
    }
}
