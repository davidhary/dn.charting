
using System.ComponentModel;

namespace System.Windows.Forms.DataVisualization.Charting;

[AttributeUsage( AttributeTargets.All )]
internal sealed class SRDescriptionAttribute( string description ) : DescriptionAttribute( description )
{
    // Fields
    private bool _replaced;

    // Properties
    public override string Description
    {
        get
        {
            if ( !this._replaced )
            {
                this._replaced = true;
                base.DescriptionValue = SR.Keys.GetString( base.Description );
            }
            return base.Description;
        }
    }
}
