//
//  Purpose:	This class contains all necessary methods and 
//				properties for drawing and selection of the stacked 
//				bar	and hundred percent stacked bar charts. 
//				Every data point in the Stacked bar chart is 
//				represented with one rectangle. If there is 
//				more then one series with this chart type from 
//				same chart area, bars with same X values are 
//				Stacked.
//

using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting.Utilities;

namespace System.Windows.Forms.DataVisualization.Charting.ChartTypes;
/// <summary>
/// HundredPercentStackedBarChart class extends StackedBarChart class
/// by providing its own algorithm for calculating series data point
/// Y values. It makes sure that total Y value of all data points in a
/// single cluster from all series adds up to 100%.
/// </summary>
internal class HundredPercentStackedBarChart : StackedBarChart
{
    #region " constructor "

    /// <summary>
    /// Default constructor.
    /// </summary>
    public HundredPercentStackedBarChart() => this.HundredPercentStacked = true;

    #endregion

    #region " fields "

    // Total Y values from all series at specified index organized by stacked groups
    // Hash table will contain arrays of doubles stored by group name key.
    private Hashtable _stackedGroupsTotalPerPoint;


    #endregion

    #region " icharttype interface implementation "

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => ChartTypeNames.OneHundredPercentStackedBar;

    /// <summary>
    /// Indicates that it's a hundred percent chart.
    /// Axis scale from 0 to 100 percent should be used.
    /// </summary>
    public override bool HundredPercent => true;

    /// <summary>
    /// Indicates that it's a hundred percent chart.
    /// Axis scale from 0 to 100 percent should be used.
    /// </summary>
    public override bool HundredPercentSupportNegative => true;

    #endregion

    #region " painting and selection methods "

    /// <summary>
    /// Paint HundredPercentStackedBarChart Chart.
    /// </summary>
    /// <param name="graph">The Chart Graphics object.</param>
    /// <param name="common">The Common elements object.</param>
    /// <param name="area">Chart area for this chart.</param>
    /// <param name="seriesToDraw">Chart series to draw.</param>
    public override void Paint( ChartGraphics graph, CommonElements common, ChartArea area, Series seriesToDraw )
    {
        // Reset pre-calculated totals

        this._stackedGroupsTotalPerPoint = null;

        // Call base class painting
        base.Paint( graph, common, area, seriesToDraw );
    }

    #endregion

    #region " y values related methods "

    /// <summary>   Helper function, which returns the Y value of the point. </summary>
    /// <remarks>   2023-05-27. </remarks>
    /// <param name="common">       Chart common elements. </param>
    /// <param name="area">         Chart area the series belongs to. </param>
    /// <param name="series">       Series of the point. </param>
    /// <param name="point">        Point object. </param>
    /// <param name="pointIndex">   Index of the point. </param>
    /// <param name="yValueIndex">  Index of the Y value to get. </param>
    /// <returns>   Y value of the point. </returns>
    public override double GetYValue( CommonElements common, ChartArea area, Series series, DataPoint point, int pointIndex, int yValueIndex )
    {
        // Array of Y totals for individual series index in the current stacked group
        double[] currentGroupTotalPerPoint = null;


        string currentStackedGroupName = HundredPercentStackedColumnChart.GetSeriesStackGroupName( series );
        if ( this._stackedGroupsTotalPerPoint == null )
        {
            // Create new hash table
            this._stackedGroupsTotalPerPoint = [];

            // Iterate through all stacked groups
            foreach ( string groupName in this.StackGroupNames )
            {
                // Get series that belong to the same group
                Series[] seriesArray = HundredPercentStackedColumnChart.GetSeriesByStackedGroupName(
                    common, groupName, series.ChartTypeName, series.ChartArea );

                // Check if series are aligned
                common.DataManipulator.CheckXValuesAlignment( seriesArray );

                // Allocate memory for the array of totals
                double[] totals = new double[series.Points.Count];

                // Calculate the total of Y value per point 
                for ( int index = 0; index < series.Points.Count; index++ )
                {
                    totals[index] = 0;
                    foreach ( Series ser in seriesArray )
                    {
                        totals[index] += Math.Abs( ser.Points[index].YValues[0] );
                    }
                }

                // Add totals array into the hash table
                this._stackedGroupsTotalPerPoint.Add( groupName, totals );
            }
        }

        // Find array of total Y values based on the current stacked group name
        currentGroupTotalPerPoint = ( double[] ) this._stackedGroupsTotalPerPoint[currentStackedGroupName];


        if ( !area.Area3DStyle.Enable3D )
        {
            if ( point.YValues[0] == 0 || point.IsEmpty )
            {
                return 0;
            }
        }

        // Calculate stacked column Y value for 2D chart
        if ( !area.Area3DStyle.Enable3D || yValueIndex == -2 )
        {
            return currentGroupTotalPerPoint[pointIndex] == 0.0 ? 0.0 : point.YValues[0] / currentGroupTotalPerPoint[pointIndex] * 100.0;
        }

        // Get point Height if pointIndex == -1
        double yValue = double.NaN;
        if ( yValueIndex == -1 )
        {
            Axis vAxis = area.GetAxis( AxisName.Y, series.YAxisType, series.YSubAxisName );
            double barZeroValue = vAxis.Crossing;
            yValue = this.GetYValue( common, area, series, point, pointIndex, 0 );
            if ( yValue >= 0 )
            {
                if ( !double.IsNaN( this.PrevPosY ) )
                {
                    barZeroValue = this.PrevPosY;
                }
            }
            else
            {
                if ( !double.IsNaN( this.PrevNegY ) )
                {
                    barZeroValue = this.PrevNegY;
                }
            }

            return yValue - barZeroValue;
        }


        // Loop through all series to find point value
        this.PrevPosY = double.NaN;
        this.PrevNegY = double.NaN;
        foreach ( Series ser in common.DataManager.Series )
        {
            // Check series of the current chart type & area
            if ( string.Compare( series.ChartArea, ser.ChartArea, StringComparison.Ordinal ) == 0 &&
                string.Compare( series.ChartTypeName, ser.ChartTypeName, StringComparison.OrdinalIgnoreCase ) == 0 &&
                ser.IsVisible() )
            {
                // Series must belong to the same stacked group
                if ( currentStackedGroupName != HundredPercentStackedColumnChart.GetSeriesStackGroupName( ser ) )
                {
                    continue;
                }


                if ( double.IsNaN( yValue ) )
                {
                    yValue = currentGroupTotalPerPoint[pointIndex] == 0.0
                        ? 0.0
                        : ser.Points[pointIndex].YValues[0] / currentGroupTotalPerPoint[pointIndex] * 100.0;
                }
                else
                {
                    yValue = currentGroupTotalPerPoint[pointIndex] == 0.0
                        ? 0.0
                        : ser.Points[pointIndex].YValues[0] / currentGroupTotalPerPoint[pointIndex] * 100.0;
                    if ( yValue >= 0.0 && !double.IsNaN( this.PrevPosY ) )
                    {
                        yValue += this.PrevPosY;
                    }
                    if ( yValue < 0.0 && !double.IsNaN( this.PrevNegY ) )
                    {
                        yValue += this.PrevNegY;
                    }
                }

                // Exit loop when current series was found
                if ( string.Compare( series.Name, ser.Name, StringComparison.Ordinal ) == 0 )
                {
                    break;
                }

                // Save previous value
                if ( yValue >= 0.0 )
                {
                    this.PrevPosY = yValue;
                }
                else
                {
                    this.PrevNegY = yValue;
                }
            }
        }

        return (yValue > 100.0) ? 100.0 : yValue;
    }

    #endregion
}
/// <summary>
/// StackedBarChart class contains all the code necessary to draw 
/// and hit test Stacked Bar chart. 
/// </summary>
internal class StackedBarChart : IChartType
{
    #region " fields "

    /// <summary>
    /// Previous stacked positive Y values.
    /// </summary>
    protected double PrevPosY { get; set; } = double.NaN;

    /// <summary>
    /// Previous stacked negative Y values.
    /// </summary>
    protected double PrevNegY { get; set; } = double.NaN;

    /// <summary>
    /// Indicates if chart is 100% stacked
    /// </summary>
    protected bool HundredPercentStacked { get; set; }



    /// <summary>
    /// True if stack group name is applicable
    /// </summary>
    internal bool StackGroupNameUsed { get; set; }

    /// <summary>
    /// List of all stack group names
    /// </summary> 
    internal ArrayList StackGroupNames { get; set; }

    /// <summary>
    /// Name of the current stack group.
    /// </summary>
    internal string CurrentStackGroup { get; set; } = string.Empty;

    #endregion

    #region " icharttype interface implementation "

    /// <summary>
    /// Chart type name
    /// </summary>
    public virtual string Name => ChartTypeNames.StackedBar;

    /// <summary>
    /// Gets chart type image.
    /// </summary>
    /// <param name="registry">Chart types registry object.</param>
    /// <returns>Chart type image.</returns>
    public virtual Image GetImage( ChartTypeRegistry registry )
    {
        return ( Image ) registry.ResourceManager.GetObject( this.Name + "ChartType", Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary>
    /// True if chart type is stacked
    /// </summary>
    public bool Stacked => true;

    /// <summary>
    /// True if stacked chart type supports groups
    /// </summary>
    public virtual bool SupportStackedGroups => true;

    /// <summary>
    /// True if stacked chart type should draw separately positive and 
    /// negative data points ( Bar and column Stacked types ).
    /// </summary>
    public bool StackSign => true;

    /// <summary>
    /// True if chart type supports axes
    /// </summary>
    public bool RequireAxes => true;

    /// <summary>
    /// Chart type with two y values used for scale ( bubble chart type )
    /// </summary>
    public bool SecondYScale => false;

    /// <summary>
    /// True if chart type requires circular chart area.
    /// </summary>
    public bool CircularChartArea => false;

    /// <summary>
    /// True if chart type supports logarithmic axes
    /// </summary>
    public bool SupportLogarithmicAxes => true;

    /// <summary>
    /// True if chart type requires to switch the value (Y) axes position
    /// </summary>
    public bool SwitchValueAxes => true;

    /// <summary>
    /// True if chart series can be placed side-by-side.
    /// </summary>
    public bool SideBySideSeries => false;

    /// <summary>
    /// If the crossing value is auto Crossing value should be 
    /// automatically set to zero for some chart 
    /// types (Bar, column, area etc.)
    /// </summary>
    public bool ZeroCrossing => true;

    /// <summary>
    /// True if each data point of a chart must be represented in the legend
    /// </summary>
    public bool DataPointsInLegend => false;

    /// <summary>
    /// Indicates that extra Y values are connected to the scale of the Y axis
    /// </summary>
    public virtual bool ExtraYValuesConnectedToYAxis => false;

    /// <summary>
    /// Indicates that it's a hundred percent chart.
    /// Axis scale from 0 to 100 percent should be used.
    /// </summary>
    public virtual bool HundredPercent => false;

    /// <summary>
    /// Indicates that it's a hundred percent chart.
    /// Axis scale from 0 to 100 percent should be used.
    /// </summary>
    public virtual bool HundredPercentSupportNegative => false;

    /// <summary>
    /// True if palette colors should be applied for each data point.
    /// Otherwise the color is applied to the series.
    /// </summary>
    public bool ApplyPaletteColorsToPoints => false;

    /// <summary>
    /// How to draw series/points in legend:
    /// Filled rectangle, Line or Marker
    /// </summary>
    /// <param name="series">Legend item series.</param>
    /// <returns>Legend item style.</returns>
    public LegendImageStyle GetLegendImageStyle( Series series )
    {
        return LegendImageStyle.Rectangle;
    }

    /// <summary>
    /// Number of supported Y value(s) per point 
    /// </summary>
    public int YValuesPerPoint => 1;

    #endregion

    #region " painting and selection methods "

    /// <summary>
    /// Paint Stacked Bar Chart.
    /// </summary>
    /// <param name="graph">The Chart Graphics object</param>
    /// <param name="common">The Common elements object</param>
    /// <param name="area">Chart area for this chart</param>
    /// <param name="seriesToDraw">Chart series to draw.</param>
    public virtual void Paint( ChartGraphics graph, CommonElements common, ChartArea area, Series seriesToDraw )
    {
        // Reset stacked group names flag
        this.StackGroupNameUsed = true;


        // Set Clip Region in rounded to a pixel coordinates
        RectangleF areaPosition = graph.GetAbsoluteRectangle( area.PlotAreaPosition.ToRectangleF() );
        float right = ( float ) Math.Ceiling( areaPosition.Right );
        float bottom = ( float ) Math.Ceiling( areaPosition.Bottom );
        areaPosition.X = ( float ) Math.Floor( areaPosition.X );
        areaPosition.Width = right - areaPosition.X;
        areaPosition.Y = ( float ) Math.Floor( areaPosition.Y );
        areaPosition.Height = bottom - areaPosition.Y;
        graph.SetClipAbs( areaPosition );

        // Draw shadow
        this.ProcessChartType( false, graph, common, area, true, false, seriesToDraw );

        // Draw stacked bars
        this.ProcessChartType( false, graph, common, area, false, false, seriesToDraw );

        // Draw labels
        this.ProcessChartType( false, graph, common, area, false, true, seriesToDraw );

        // Reset Clip Region
        graph.ResetClip();
    }

    /// <summary>
    /// This method recalculates size of the stacked bars. This method is used 
    /// from Paint or Select method.
    /// </summary>
    /// <param name="selection">If True selection mode is active, otherwise paint mode is active.</param>
    /// <param name="graph">The Chart Graphics object.</param>
    /// <param name="common">The Common elements object.</param>
    /// <param name="area">Chart area for this chart.</param>
    /// <param name="shadow">True if shadow mode is active.</param>
    /// <param name="labels">Labels drawing mode.</param>
    /// <param name="seriesToDraw">Chart series to draw.</param>
    private void ProcessChartType(
        bool selection,
        ChartGraphics graph,
        CommonElements common,
        ChartArea area,
        bool shadow,
        bool labels,
        Series seriesToDraw )
    {
        // ============================
        // If stacked series is attached to different X and Y axis
        // they can not be processed. To solve this issue series 
        // will be organized in groups based on the axes.
        // ============================

        // Loop through all series and check if different axes are used
        bool differentAxesAreUsed = false;
        AxisType xAxisType = AxisType.Primary;
        AxisType yAxisType = AxisType.Primary;
        string xSubAxisName = string.Empty;
        string ySubAxisName = string.Empty;
        for ( int seriesIndex = 0; seriesIndex < common.DataManager.Series.Count; seriesIndex++ )
        {
            // Process non empty series of the area with stacked column chart type
            Series ser = common.DataManager.Series[seriesIndex];
            if ( string.Compare( ser.ChartTypeName, this.Name, StringComparison.OrdinalIgnoreCase ) != 0
                || ser.ChartArea != area.Name || !ser.IsVisible() )
            {
                continue;
            }

            if ( seriesIndex == 0 )
            {
                xAxisType = ser.XAxisType;
                yAxisType = ser.YAxisType;
                xSubAxisName = ser.XSubAxisName;
                ySubAxisName = ser.YSubAxisName;
            }
            else if ( xAxisType != ser.XAxisType ||
                yAxisType != ser.YAxisType ||
                xSubAxisName != ser.XSubAxisName ||
                ySubAxisName != ser.YSubAxisName )
            {
                differentAxesAreUsed = true;
                break;
            }
        }

        // Set stacked groups based on the axes used
        if ( differentAxesAreUsed )
        {
            for ( int seriesIndex = 0; seriesIndex < common.DataManager.Series.Count; seriesIndex++ )
            {
                // Process non empty series of the area with stacked column chart type
                Series ser = common.DataManager.Series[seriesIndex];
                if ( string.Compare( ser.ChartTypeName, this.Name, StringComparison.OrdinalIgnoreCase ) != 0
                    || ser.ChartArea != area.Name || !ser.IsVisible() )
                {
                    continue;
                }

                // Set new group name
                string stackGroupName = StackedColumnChart.GetSeriesStackGroupName( ser );
                stackGroupName = "_X_" + ser.XAxisType.ToString() + ser.XSubAxisName + "_Y_" + ser.YAxisType.ToString() + ser.YSubAxisName + "__";
                ser[CustomPropertyName.StackedGroupName] = stackGroupName;
            }
        }

        // ============================
        // Check how many stack groups are available.
        // ============================

        // Loop through all series and get unique stack group names.
        this.StackGroupNames = [];
        foreach ( Series ser in common.DataManager.Series )
        {
            // Process non empty series of the area with stacked column chart type
            if ( string.Compare( ser.ChartTypeName, this.Name, StringComparison.OrdinalIgnoreCase ) != 0
                || ser.ChartArea != area.Name || !ser.IsVisible() )
            {
                continue;
            }

            // Get stack group name from the series
            string stackGroupName = StackedColumnChart.GetSeriesStackGroupName( ser );

            // Add group name if it do not already exists
            if ( !this.StackGroupNames.Contains( stackGroupName ) )
            {
                _ = this.StackGroupNames.Add( stackGroupName );
            }
        }


        // Process 3D chart type
        if ( area.Area3DStyle.Enable3D )
        {
            if ( !shadow )
            {
                this.ProcessChartType3D(
                    selection,
                    graph,
                    common,
                    area,
                    labels,
                    seriesToDraw );
            }

            return;
        }

        // All data series from chart area which have Stacked Bar chart type
        string[] seriesList = [.. area.GetSeriesFromChartType( this.Name )];

        // Get maximum number of data points for all series
        int maxNumOfPoints = common.DataManager.GetNumberOfPoints( seriesList );

        // Zero X values mode.
        bool indexedSeries = ChartHelper.IndexedSeries( common, seriesList );

        // ============================
        // Loop through all data points
        // ============================
        for ( int pointIndex = 0; pointIndex < maxNumOfPoints; pointIndex++ )
        {
            // ============================
            // Loop through all stack groups
            // ============================
            for ( int groupIndex = 0; groupIndex < this.StackGroupNames.Count; groupIndex++ )
            {
                // Remember current stack group name
                this.CurrentStackGroup = ( string ) this.StackGroupNames[groupIndex];

                int seriesIndex = 0;     // Data series index
                double PreviousPosY = 0;    // Previous positive Y value
                double PreviousNegY = 0;    // Previous negative Y value

                // ============================
                // Loop through all series
                // ============================
                foreach ( Series ser in common.DataManager.Series )
                {
                    // Process non empty series of the area with stacked bar chart type
                    if ( string.Compare( ser.ChartTypeName, this.Name, StringComparison.OrdinalIgnoreCase ) != 0
                        || ser.ChartArea != area.Name || !ser.IsVisible() )
                    {
                        continue;
                    }

                    // Series point index is out of range
                    if ( pointIndex >= ser.Points.Count )
                    {
                        continue;
                    }


                    // Check if series belongs to the current group name
                    string seriesStackGroupName = StackedColumnChart.GetSeriesStackGroupName( ser );
                    if ( seriesStackGroupName != this.CurrentStackGroup )
                    {
                        continue;
                    }



                    // Get data point
                    DataPoint point = ser.Points[pointIndex];

                    // Reset pre-calculated point position
                    point.positionRel = new PointF( float.NaN, float.NaN );

                    // Set active horizontal/vertical axis
                    Axis vAxis = area.GetAxis( AxisName.X, ser.XAxisType, ser.XSubAxisName );
                    Axis hAxis = area.GetAxis( AxisName.Y, ser.YAxisType, ser.YSubAxisName );

                    // Interval between bars
                    double interval = 1;
                    if ( !indexedSeries )
                    {
                        if ( ser.Points.Count == 1 &&
                            (ser.XValueType == ChartValueType.Date ||
                             ser.XValueType == ChartValueType.DateTime ||
                             ser.XValueType == ChartValueType.Time ||
                             ser.XValueType == ChartValueType.DateTimeOffset) )
                        {
                            // Check if interval is the same
                            List<string> typeSeries = area.GetSeriesFromChartType( this.Name );
                            _ = area.GetPointsInterval( typeSeries, vAxis.IsLogarithmic, vAxis.logarithmBase, true, out bool sameInterval );

                            // Special case when there is only one data point and date scale is used.
                            interval = !double.IsNaN( vAxis.majorGrid.GetInterval() ) && vAxis.majorGrid.GetIntervalType() != DateTimeIntervalType.NotSet
                                ? ChartHelper.GetIntervalSize( vAxis.minimum, vAxis.majorGrid.GetInterval(), vAxis.majorGrid.GetIntervalType() )
                                : ChartHelper.GetIntervalSize( vAxis.minimum, vAxis.Interval, vAxis.IntervalType );
                        }
                        else
                        {
                            interval = area.GetPointsInterval( vAxis.IsLogarithmic, vAxis.logarithmBase );
                        }
                    }

                    // Calculates the width of bars.
                    double width = ser.GetPointWidth( graph, vAxis, interval, 0.8 );


                    // Adjust width by number of stacked groups
                    width /= this.StackGroupNames.Count;


                    // Call Back Paint event
                    if ( !selection )
                    {
                        common.Chart.CallOnPrePaint( new ChartPaintEventArgs( ser, graph, common, area.PlotAreaPosition ) );
                    }

                    // Change Y value if Bar is out of plot area
                    double yValue = this.GetYValue( common, area, ser, point, pointIndex, 0 );
                    if ( seriesIndex != 0 )
                    {
                        yValue = yValue >= 0 ? yValue + PreviousPosY : yValue + PreviousNegY;
                    }

                    // Check if scrolling/zooming frames cutting mode is enabled
                    bool ajaxScrollingEnabled = false;

                    // Save original Y Value
                    double originalYValue = yValue;

                    // Axis is logarithmic
                    if ( hAxis.IsLogarithmic )
                    {
                        yValue = Math.Log( yValue, hAxis.logarithmBase );
                    }

                    // Recalculates Height position and zero position of bars
                    double height = hAxis.GetLinearPosition( yValue );

                    // Set x position
                    double xValue = point.XValue;
                    if ( indexedSeries )
                    {
                        // The formula for position is based on a distance 
                        //from the grid line or nPoints position.
                        xValue = ( double ) pointIndex + 1;
                    }
                    double xPosition = vAxis.GetPosition( xValue );

                    // Adjust X position of each stack group
                    if ( this.StackGroupNames.Count > 1 )
                    {
                        xPosition = xPosition - (width * this.StackGroupNames.Count / 2.0) + (width / 2.0) + (groupIndex * width);
                    }


                    xValue = vAxis.GetLogValue( xValue );


                    // Set Start position for a bar
                    double barZeroValue;
                    if ( seriesIndex == 0 )
                    {
                        if ( ajaxScrollingEnabled && labels )
                        {
                            // If AJAX scrolling is used always use 0.0 as a starting point
                            barZeroValue = 0.0;
                        }
                        else
                        {
                            // Set Start position for a Column
                            barZeroValue = hAxis.Crossing;
                        }
                    }
                    else
                    {
                        barZeroValue = this.GetYValue( common, area, ser, point, pointIndex, 0 ) >= 0 ? PreviousPosY : PreviousNegY;
                    }
                    double zero = hAxis.GetPosition( barZeroValue );

                    // Calculate bar position
                    RectangleF rectSize = RectangleF.Empty;
                    try
                    {
                        // Set the bar rectangle
                        rectSize.Y = ( float ) (xPosition - (width / 2));
                        rectSize.Height = ( float ) width;

                        // The left side of rectangle has always 
                        // smaller value than a right value
                        if ( zero < height )
                        {
                            rectSize.X = ( float ) zero;
                            rectSize.Width = ( float ) height - rectSize.X;
                        }
                        else
                        {
                            rectSize.X = ( float ) height;
                            rectSize.Width = ( float ) zero - rectSize.X;
                        }
                    }
                    catch ( OverflowException )
                    {
                        continue;
                    }

                    // Remember pre-calculated point position
                    point.positionRel = new PointF( rectSize.Right, ( float ) xPosition );


                    // if data point is not empty
                    if ( point.IsEmpty )
                    {
                        continue;
                    }

                    // Axis is logarithmic
                    if ( hAxis.IsLogarithmic )
                    {
                        barZeroValue = Math.Log( barZeroValue, hAxis.logarithmBase );
                    }

                    // Check if column is completely out of the data scaleView
                    bool skipPoint = false;
                    if ( xValue < vAxis.ViewMinimum ||
                        xValue > vAxis.ViewMaximum ||
                        (yValue < hAxis.ViewMinimum && barZeroValue < hAxis.ViewMinimum) ||
                        (yValue > hAxis.ViewMaximum && barZeroValue > hAxis.ViewMaximum) )
                    {
                        skipPoint = true;
                    }

                    // ***************************************************
                    // Painting mode
                    // ***************************************************
                    if ( !skipPoint )
                    {
                        if ( common.ProcessModePaint )
                        {
                            // Check if column is partially in the data scaleView
                            bool clipRegionSet = false;
                            if ( rectSize.Y < area.PlotAreaPosition.Y ||
                                rectSize.Bottom > area.PlotAreaPosition.Bottom ||
                                rectSize.X < area.PlotAreaPosition.X ||
                                rectSize.Right > area.PlotAreaPosition.Right )
                            {
                                // Set clipping region for line drawing 
                                graph.SetClip( area.PlotAreaPosition.ToRectangleF() );
                                clipRegionSet = true;
                            }

                            // Set shadow
                            int shadowOffset = 0;
                            if ( shadow )
                            {
                                shadowOffset = ser.ShadowOffset;
                            }

                            if ( !labels )
                            {
                                // Start Svg Selection mode
                                graph.StartHotRegion( point );

                                // Draw the bar rectangle
                                graph.FillRectangleRel( rectSize,
                                    (!shadow) ? point.Color : Color.Transparent,
                                    point.BackHatchStyle,
                                    point.BackImage,
                                    point.BackImageWrapMode,
                                    point.BackImageTransparentColor,
                                    point.BackImageAlignment,
                                    point.BackGradientStyle,
                                    (!shadow) ? point.BackSecondaryColor : Color.Transparent,
                                    point.BorderColor,
                                    point.BorderWidth,
                                    point.BorderDashStyle,
                                    ser.ShadowColor,
                                    shadowOffset,
                                    PenAlignment.Inset,
                                    shadow ? BarDrawingStyle.Default : ChartGraphics.GetBarDrawingStyle( point ),
                                    false );

                                // End Svg Selection mode
                                graph.EndHotRegion();
                            }

                            // Draw labels 
                            else
                            {
                                // Calculate label rectangle 
                                RectangleF labelRect = new( rectSize.Location, rectSize.Size );
                                if ( clipRegionSet && !ajaxScrollingEnabled )
                                {
                                    labelRect.Intersect( area.PlotAreaPosition.ToRectangleF() );
                                }

                                // Draw Labels
                                this.DrawLabels( common, graph, area, point, pointIndex, ser, labelRect );
                            }

                            // Reset Clip Region
                            if ( clipRegionSet )
                            {
                                graph.ResetClip();
                            }
                        }

                        // ***************************************************
                        // Hot Regions Mode
                        // ***************************************************
                        if ( common.ProcessModeRegions && !shadow && !labels )
                        {
                            common.HotRegionsList.AddHotRegion( rectSize, point, ser.Name, pointIndex );

                            // Process labels and markers regions only if it was not done while painting
                            if ( labels && !common.ProcessModePaint )
                            {
                                this.DrawLabels( common, graph, area, point, pointIndex, ser, rectSize );
                            }
                        }

                        // Call Paint event
                        if ( !selection )
                        {
                            common.Chart.CallOnPostPaint( new ChartPaintEventArgs( ser, graph, common, area.PlotAreaPosition ) );
                        }
                    }

                    // Axis is logarithmic
                    if ( hAxis.IsLogarithmic )
                    {
                        yValue = Math.Pow( hAxis.logarithmBase, yValue );
                    }

                    // Data series index
                    seriesIndex++;
                    if ( this.GetYValue( common, area, ser, point, pointIndex, 0 ) >= 0 )
                    {
                        PreviousPosY = originalYValue;
                    }
                    else
                    {
                        PreviousNegY = originalYValue;
                    }
                }

            }

        }



        // ============================
        // Remove stacked groups created for series attached to different axis
        // ============================

        if ( differentAxesAreUsed )
        {
            for ( int seriesIndex = 0; seriesIndex < common.DataManager.Series.Count; seriesIndex++ )
            {
                // Process non empty series of the area with stacked column chart type
                Series ser = common.DataManager.Series[seriesIndex];
                if ( string.Compare( ser.ChartTypeName, this.Name, StringComparison.OrdinalIgnoreCase ) != 0
                    || ser.ChartArea != area.Name || !ser.IsVisible() )
                {
                    continue;
                }

                // Set new group name
                string stackGroupName = StackedColumnChart.GetSeriesStackGroupName( ser );
                int index = stackGroupName.IndexOf( "__", StringComparison.Ordinal );
                if ( index >= 0 )
                {
                    stackGroupName = stackGroupName[(index + 2)..];
                }
                if ( stackGroupName.Length > 0 )
                {
                    ser[CustomPropertyName.StackedGroupName] = stackGroupName;
                }
                else
                {
                    ser.DeleteCustomProperty( CustomPropertyName.StackedGroupName );
                }
            }
        }



    }

    /// <summary>
    /// Draw Stacked Column labels.
    /// </summary>
    /// <param name="common">Chart common elements.</param>
    /// <param name="graph">Chart Graphics.</param>
    /// <param name="area">Chart area the series belongs to.</param>
    /// <param name="point">Data point.</param>
    /// <param name="pointIndex">Data point index.</param>
    /// <param name="series">Data series.</param>
    /// <param name="rectangle">Column rectangle.</param>
    public void DrawLabels(
        CommonElements common,
        ChartGraphics graph,
        ChartArea area,
        DataPoint point,
        int pointIndex,
        Series series,
        RectangleF rectangle )
    {
        // Label text format
        using StringFormat format = new();
        format.Alignment = StringAlignment.Center;
        format.LineAlignment = StringAlignment.Center;

        // Disable the clip region
        Region oldClipRegion = graph.Clip;
        graph.Clip = new Region();

        if ( point.IsValueShownAsLabel || point.Label.Length > 0 )
        {
            // Round Y values for 100% stacked bar
            double pointLabelValue = this.GetYValue( common, area, series, point, pointIndex, 0 );
            if ( this.HundredPercentStacked && point.LabelFormat.Length == 0 )
            {
                pointLabelValue = Math.Round( pointLabelValue, 2 );
            }

            // Get label text
            string text = point.Label.Length == 0
                ? ValueConverter.FormatValue(
                    series.Chart,
                    point,
                    point.Tag,
                    pointLabelValue,
                    point.LabelFormat,
                    series.YValueType,
                    ChartElementType.DataPoint )
                : point.ReplaceKeywords( point.Label );

            // Calculate position
            PointF labelPosition = PointF.Empty;
            labelPosition.X = rectangle.X + (rectangle.Width / 2f);
            labelPosition.Y = rectangle.Y + (rectangle.Height / 2f);

            // Get text angle
            int textAngle = point.LabelAngle;

            // Check if text contains white space only
            if ( text.Trim().Length != 0 )
            {
                // ============================
                // Measure string
                // ============================
                SizeF sizeFont = graph.GetRelativeSize(
                    graph.MeasureString(
                    text,
                    point.Font,
                    new SizeF( 1000f, 1000f ),
                    StringFormat.GenericTypographic ) );

                // ============================
                // Check labels style custom properties 
                // ============================
                BarValueLabelDrawingStyle drawingStyle = BarValueLabelDrawingStyle.Center;
                string valueLabelAttrib = "";
                if ( point.IsCustomPropertySet( CustomPropertyName.BarLabelStyle ) )
                {
                    valueLabelAttrib = point[CustomPropertyName.BarLabelStyle];
                }
                else if ( series.IsCustomPropertySet( CustomPropertyName.BarLabelStyle ) )
                {
                    valueLabelAttrib = series[CustomPropertyName.BarLabelStyle];
                }

                if ( valueLabelAttrib != null && valueLabelAttrib.Length > 0 )
                {
                    if ( string.Compare( valueLabelAttrib, "Left", StringComparison.OrdinalIgnoreCase ) == 0 )
                        drawingStyle = BarValueLabelDrawingStyle.Left;
                    else if ( string.Compare( valueLabelAttrib, "Right", StringComparison.OrdinalIgnoreCase ) == 0 )
                        drawingStyle = BarValueLabelDrawingStyle.Right;
                    else if ( string.Compare( valueLabelAttrib, "Center", StringComparison.OrdinalIgnoreCase ) == 0 )
                        drawingStyle = BarValueLabelDrawingStyle.Center;
                    else if ( string.Compare( valueLabelAttrib, "Outside", StringComparison.OrdinalIgnoreCase ) == 0 )
                        drawingStyle = BarValueLabelDrawingStyle.Outside;
                }

                // ============================
                // Adjust label position based on the label drawing style
                // ============================
                if ( drawingStyle == BarValueLabelDrawingStyle.Left )
                {
                    labelPosition.X = rectangle.X + (sizeFont.Width / 2f);
                }
                else if ( drawingStyle == BarValueLabelDrawingStyle.Right )
                {
                    labelPosition.X = rectangle.Right - (sizeFont.Width / 2f);
                }
                else if ( drawingStyle == BarValueLabelDrawingStyle.Outside )
                {
                    labelPosition.X = rectangle.Right + (sizeFont.Width / 2f);
                }


                // Check if Smart Labels are enabled
                if ( series.SmartLabelStyle.Enabled )
                {
                    // Force some SmartLabelStyle settings for column chart
                    bool oldMarkerOverlapping = series.SmartLabelStyle.IsMarkerOverlappingAllowed;
                    LabelAlignmentStyles oldMovingDirection = series.SmartLabelStyle.MovingDirection;
                    series.SmartLabelStyle.IsMarkerOverlappingAllowed = true;
                    if ( series.SmartLabelStyle.MovingDirection == (LabelAlignmentStyles.Top | LabelAlignmentStyles.Bottom | LabelAlignmentStyles.Right | LabelAlignmentStyles.Left | LabelAlignmentStyles.TopLeft | LabelAlignmentStyles.TopRight | LabelAlignmentStyles.BottomLeft | LabelAlignmentStyles.BottomRight) )
                    {
                        series.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.Left | LabelAlignmentStyles.Right;
                    }

                    // Adjust label position using SmartLabelStyle algorithm
                    labelPosition = area.smartLabels.AdjustSmartLabelPosition(
                        common,
                        graph,
                        area,
                        series.SmartLabelStyle,
                        labelPosition,
                        sizeFont,
                        format,
                        labelPosition,
                        new SizeF( 0f, 0f ),
                        LabelAlignmentStyles.Center );

                    // Restore forced values
                    series.SmartLabelStyle.IsMarkerOverlappingAllowed = oldMarkerOverlapping;
                    series.SmartLabelStyle.MovingDirection = oldMovingDirection;

                    // Smart labels always use 0 degrees text angle
                    textAngle = 0;
                }



                // Draw label
                if ( !labelPosition.IsEmpty )
                {
                    // Get label background position
                    RectangleF labelBackPosition = RectangleF.Empty;
                    SizeF sizeLabel = new( sizeFont.Width, sizeFont.Height );
                    sizeLabel.Height += sizeFont.Height / 8;
                    sizeLabel.Width += sizeLabel.Width / text.Length;
                    labelBackPosition = new RectangleF(
                        labelPosition.X - (sizeLabel.Width / 2),
                        labelPosition.Y - (sizeLabel.Height / 2) - (sizeFont.Height / 10),
                        sizeLabel.Width,
                        sizeLabel.Height );



                    // Adjust label background position that can be changed by the 
                    // Smart Labels algorithm
                    // NOTE: Fixes issue #4688
                    labelBackPosition = area.smartLabels.GetLabelPosition(
                        graph,
                        labelPosition,
                        sizeLabel,
                        format,
                        true );



                    // Draw label text
                    using Brush brush = new SolidBrush( point.LabelForeColor );
                    graph.DrawPointLabelStringRel(
                        common,
                        text,
                        point.Font,
                        brush,
                        labelPosition,
                        format,
                        textAngle,
                        labelBackPosition,
                        point.LabelBackColor,
                        point.LabelBorderColor,
                        point.LabelBorderWidth,
                        point.LabelBorderDashStyle,
                        series,
                        point,
                        pointIndex );
                }

            }
        }

        // Restore old clip region
        graph.Clip = oldClipRegion;
    }

    #endregion

    #region " y values related methods "

    /// <summary>
    /// Helper function, which returns the Y value of the point.
    /// </summary>
    /// <param name="common">Chart common elements.</param>
    /// <param name="area">Chart area the series belongs to.</param>
    /// <param name="series">Series of the point.</param>
    /// <param name="point">Point object.</param>
    /// <param name="pointIndex">Index of the point.</param>
    /// <param name="yValueIndex">Index of the Y value to get. Set to -1 to get the height.</param>
    /// <returns>Y value of the point.</returns>
    public virtual double GetYValue( CommonElements common, ChartArea area, Series series, DataPoint point, int pointIndex, int yValueIndex )
    {
        double yValue = double.NaN;

        // Calculate stacked column Y value for 2D chart
        if ( !area.Area3DStyle.Enable3D || yValueIndex == -2 )
        {
            return point.YValues[0];
        }

        // Get point Height if pointIndex == -1
        if ( yValueIndex == -1 )
        {
            Axis vAxis = area.GetAxis( AxisName.Y, series.YAxisType, series.YSubAxisName );
            double barZeroValue = vAxis.Crossing;
            yValue = this.GetYValue( common, area, series, point, pointIndex, 0 );
            if ( yValue >= 0 )
            {
                if ( !double.IsNaN( this.PrevPosY ) )
                {
                    barZeroValue = this.PrevPosY;
                }
            }
            else
            {
                if ( !double.IsNaN( this.PrevNegY ) )
                {
                    barZeroValue = this.PrevNegY;
                }
            }

            return yValue - barZeroValue;
        }

        // Loop through all series
        this.PrevPosY = double.NaN;
        this.PrevNegY = double.NaN;
        foreach ( Series ser in common.DataManager.Series )
        {
            // Check series of the current chart type & area
            if ( string.Compare( series.ChartArea, ser.ChartArea, StringComparison.Ordinal ) == 0 &&
                string.Compare( series.ChartTypeName, ser.ChartTypeName, StringComparison.OrdinalIgnoreCase ) == 0 &&
                ser.IsVisible() )
            {
                // Check if series belongs to the current group name
                string seriesStackGroupName = StackedColumnChart.GetSeriesStackGroupName( ser );
                if ( this.StackGroupNameUsed &&
                    seriesStackGroupName != this.CurrentStackGroup )
                {
                    continue;
                }



                if ( double.IsNaN( yValue ) )
                {
                    yValue = ser.Points[pointIndex].YValues[0];
                }
                else
                {
                    yValue = ser.Points[pointIndex].YValues[0];
                    if ( yValue >= 0.0 && !double.IsNaN( this.PrevPosY ) )
                    {
                        yValue += this.PrevPosY;
                    }
                    if ( yValue < 0.0 && !double.IsNaN( this.PrevNegY ) )
                    {
                        yValue += this.PrevNegY;
                    }
                }

                // Exit loop when current series was found
                if ( string.Compare( series.Name, ser.Name, StringComparison.Ordinal ) == 0 )
                {
                    break;
                }

                // Save previous value
                if ( yValue >= 0.0 )
                {
                    this.PrevPosY = yValue;
                }
                if ( yValue < 0.0 )
                {
                    this.PrevNegY = yValue;
                }
            }
        }

        return yValue;
    }

    #endregion

    #region " 3d painting and selection methods "

    /// <summary>
    /// This method recalculates size of the stacked bars in 3D space. This method is used 
    /// from Paint or Select method.
    /// </summary>
    /// <param name="selection">If True selection mode is active, otherwise paint mode is active.</param>
    /// <param name="graph">The Chart Graphics object.</param>
    /// <param name="common">The Common elements object.</param>
    /// <param name="area">Chart area for this chart.</param>
    /// <param name="drawLabels">True if labels must be drawn.</param>
    /// <param name="seriesToDraw">Chart series to draw.</param>
    private void ProcessChartType3D(
        bool selection,
        ChartGraphics graph,
        CommonElements common,
        ChartArea area,
        bool drawLabels,
        Series seriesToDraw )
    {
        // Get list of series to draw
        List<string> typeSeries = null;


        // Get all series names that belong the same cluster
        typeSeries = area.GetClusterSeriesNames( seriesToDraw.Name );


        // ============================
        // Get order of data points drawing
        // ============================
        ArrayList dataPointDrawingOrder = area.GetDataPointDrawingOrder(
            typeSeries,
            this,
            selection,
            COPCoordinates.X | COPCoordinates.Y,
            new BarPointsDrawingOrderComparer( area, selection, COPCoordinates.X | COPCoordinates.Y ),
            0,
            false );


        // ============================
        // Loop through all data points and draw them
        // ============================
        if ( !drawLabels )
        {
            foreach ( object obj in dataPointDrawingOrder )
            {
                // Get point & series
                DataPoint3D pointEx = ( DataPoint3D ) obj;
                DataPoint point = pointEx.dataPoint;
                Series ser = point.series;


                // Set current stack group name
                this.CurrentStackGroup = StackedColumnChart.GetSeriesStackGroupName( ser );


                // Reset pre-calculated point position
                point.positionRel = new PointF( float.NaN, float.NaN );

                // Set active horizontal/vertical axis
                Axis vAxis = area.GetAxis( AxisName.X, ser.XAxisType, ser.XSubAxisName );
                Axis hAxis = area.GetAxis( AxisName.Y, ser.YAxisType, ser.YSubAxisName );

                // Get point bar drawing style
                BarDrawingStyle barDrawingStyle = ChartGraphics.GetBarDrawingStyle( point );

                // All cut points are darkened except of the first and last series
                float rightDarkening = 0.5f;
                float leftDarkening = 0.5f;

                // NOTE: Following code was replaced with the code below to fix issue #5391
                //					if((string)typeSeries[typeSeries.Count - 1] == ser.Name)
                //					{
                //						leftDarkening = 0f;
                //					}
                //					if((string)typeSeries[0] == ser.Name)
                //					{
                //						rightDarkening = 0f;
                //					}
                bool firstVisibleSeries = true;
                bool lastVisibleSeries = false;
                for ( int seriesIndex = 0; seriesIndex < typeSeries.Count; seriesIndex++ )
                {
                    // Get series object
                    Series currentSeries = common.DataManager.Series[seriesIndex];

                    // Check if it is a first series with non-zero Y value
                    if ( firstVisibleSeries )
                    {
                        // Make series has non zero value
                        if ( pointEx.index <= currentSeries.Points.Count &&
                            currentSeries.Points[pointEx.index - 1].YValues[0] != 0.0 )
                        {
                            firstVisibleSeries = false;
                            if ( currentSeries.Name == ser.Name )
                            {
                                rightDarkening = 0f;
                            }
                        }
                    }

                    // Check if it is a last series with non-zero Y value
                    if ( currentSeries.Name == ser.Name )
                    {
                        lastVisibleSeries = true;
                    }
                    else if ( pointEx.index <= currentSeries.Points.Count &&
                        currentSeries.Points[pointEx.index - 1].YValues[0] != 0.0 )
                    {
                        lastVisibleSeries = false;
                    }
                }

                // Remove darkening from the last series in the group
                if ( lastVisibleSeries )
                {
                    leftDarkening = 0f;
                }


                // If stacked groups are used remove darkening from the
                // first/last series in the group
                if ( area.StackGroupNames != null &&
                    area.StackGroupNames.Count > 1 &&
                    area.Area3DStyle.IsClustered )
                {
                    // Get series group name
                    string groupName = StackedColumnChart.GetSeriesStackGroupName( ser );

                    // Iterate through all series in the group
                    bool firstSeries = true;
                    bool lastSeries = false;
                    foreach ( string seriesName in typeSeries )
                    {
                        Series currentSeries = common.DataManager.Series[seriesName];
                        if ( StackedColumnChart.GetSeriesStackGroupName( currentSeries ) == groupName )
                        {
                            // check if first series
                            if ( firstSeries )
                            {
                                // Make series has non zero value
                                if ( pointEx.index < currentSeries.Points.Count &&
                                    currentSeries.Points[pointEx.index - 1].YValues[0] != 0.0 )
                                {
                                    firstSeries = false;
                                    if ( seriesName == ser.Name )
                                    {
                                        rightDarkening = 0f;
                                    }
                                }
                            }

                            // check if last series
                            if ( seriesName == ser.Name )
                            {
                                lastSeries = true;
                            }
                            else if ( pointEx.index < currentSeries.Points.Count &&
                               currentSeries.Points[pointEx.index - 1].YValues[0] != 0.0 )
                            {
                                lastSeries = false;
                            }
                        }
                    }

                    // Remove darkening from the last series in the group
                    if ( lastSeries )
                    {
                        leftDarkening = 0f;
                    }
                }



                // Change Y value if Bar is out of plot area
                double yValue = this.GetYValue( common, area, ser, pointEx.dataPoint, pointEx.index - 1, 0 );

                // Set Start position for a bar
                double barZeroValue = yValue - this.GetYValue( common, area, ser, pointEx.dataPoint, pointEx.index - 1, -1 );

                // Convert values if logarithmic axis is used
                yValue = hAxis.GetLogValue( yValue );
                barZeroValue = hAxis.GetLogValue( barZeroValue );

                if ( barZeroValue > hAxis.ViewMaximum )
                {
                    leftDarkening = 0.5f;
                    barZeroValue = hAxis.ViewMaximum;
                }
                else if ( barZeroValue < hAxis.ViewMinimum )
                {
                    rightDarkening = 0.5f;
                    barZeroValue = hAxis.ViewMinimum;
                }
                if ( yValue > hAxis.ViewMaximum )
                {
                    leftDarkening = 0.5f;
                    yValue = hAxis.ViewMaximum;
                }
                else if ( yValue < hAxis.ViewMinimum )
                {
                    rightDarkening = 0.5f;
                    yValue = hAxis.ViewMinimum;
                }

                // Recalculates Height position and zero position of bars
                double height = hAxis.GetLinearPosition( yValue );
                double zero = hAxis.GetLinearPosition( barZeroValue );

                // Set x position
                double xValue = pointEx.indexedSeries ? pointEx.index : point.XValue;
                xValue = vAxis.GetLogValue( xValue );


                // Calculate bar position
                RectangleF rectSize = RectangleF.Empty;
                try
                {
                    // Set the bar rectangle
                    rectSize.Y = ( float ) (pointEx.xPosition - (pointEx.width / 2));
                    rectSize.Height = ( float ) pointEx.width;

                    // The left side of rectangle has always 
                    // smaller value than a right value
                    if ( zero < height )
                    {
                        (rightDarkening, leftDarkening) = (leftDarkening, rightDarkening);
                        rectSize.X = ( float ) zero;
                        rectSize.Width = ( float ) height - rectSize.X;
                    }
                    else
                    {
                        rectSize.X = ( float ) height;
                        rectSize.Width = ( float ) zero - rectSize.X;
                    }
                }
                catch ( OverflowException )
                {
                    continue;
                }

                // Remember pre-calculated point position
                point.positionRel = new PointF( rectSize.Right, ( float ) pointEx.xPosition );

                // if data point is not empty
                if ( point.IsEmpty )
                {
                    continue;
                }

                GraphicsPath rectPath = null;

                // Check if column is completely out of the data scaleView
                if ( xValue < vAxis.ViewMinimum ||
                    xValue > vAxis.ViewMaximum ||
                    (yValue < hAxis.ViewMinimum && barZeroValue < hAxis.ViewMinimum) ||
                    (yValue > hAxis.ViewMaximum && barZeroValue > hAxis.ViewMaximum) )
                {
                    continue;
                }

                // Check if column is partially in the data scaleView
                bool clipRegionSet = false;
                if ( rectSize.Bottom <= area.PlotAreaPosition.Y || rectSize.Y >= area.PlotAreaPosition.Bottom )
                {
                    continue;
                }
                if ( rectSize.Y < area.PlotAreaPosition.Y )
                {
                    rectSize.Height -= area.PlotAreaPosition.Y - rectSize.Y;
                    rectSize.Y = area.PlotAreaPosition.Y;
                }
                if ( rectSize.Bottom > area.PlotAreaPosition.Bottom )
                {
                    rectSize.Height -= rectSize.Bottom - area.PlotAreaPosition.Bottom;
                }
                if ( rectSize.Height < 0 )
                {
                    rectSize.Height = 0;
                }
                if ( rectSize.Height == 0f || rectSize.Width == 0f )
                {
                    continue;
                }


                // Detect if we need to get graphical path of drawn object
                DrawingOperationTypes drawingOperationType = DrawingOperationTypes.DrawElement;

                if ( common.ProcessModeRegions )
                {
                    drawingOperationType |= DrawingOperationTypes.CalcElementPath;
                }

                // Start Svg Selection mode
                graph.StartHotRegion( point );

                // Draw the Bar rectangle
                rectPath = graph.Fill3DRectangle(
                    rectSize,
                    pointEx.zPosition,
                    pointEx.depth,
                    area.matrix3D,
                    area.Area3DStyle.LightStyle,
                    point.Color,
                    rightDarkening,
                    leftDarkening,
                    point.BorderColor,
                    point.BorderWidth,
                    point.BorderDashStyle,
                    barDrawingStyle,
                    false,
                    drawingOperationType );

                // End Svg Selection mode
                graph.EndHotRegion();

                // Reset Clip Region
                if ( clipRegionSet )
                {
                    graph.ResetClip();
                }

                if ( common.ProcessModeRegions && !drawLabels )
                {
                    common.HotRegionsList.AddHotRegion(
                        rectPath,
                        false,
                        graph,
                        point,
                        ser.Name,
                        pointEx.index - 1
                        );
                }
                rectPath?.Dispose();
            }
        }

        // ============================
        // Loop through all data points and draw labels
        // ============================
        if ( drawLabels )
        {
            foreach ( object obj in dataPointDrawingOrder )
            {
                // Get point & series
                DataPoint3D pointEx = ( DataPoint3D ) obj;
                DataPoint point = pointEx.dataPoint;
                Series ser = point.series;

                // Set active horizontal/vertical axis
                Axis vAxis = area.GetAxis( AxisName.X, ser.XAxisType, ser.XSubAxisName );
                Axis hAxis = area.GetAxis( AxisName.Y, ser.YAxisType, ser.YSubAxisName );

                // Change Y value if Bar is out of plot area
                double yValue = this.GetYValue( common, area, ser, pointEx.dataPoint, pointEx.index - 1, 0 );

                // Axis is logarithmic
                if ( hAxis.IsLogarithmic )
                {
                    yValue = Math.Log( yValue, hAxis.logarithmBase );
                }

                // Recalculates Height position and zero position of bars
                double height = pointEx.yPosition; ;

                // Set x position
                double xValue = pointEx.indexedSeries ? pointEx.index : point.XValue;

                // Set Start position for a bar
                double barZeroValue = yValue - this.GetYValue( common, area, ser, pointEx.dataPoint, pointEx.index - 1, -1 );
                double zero = pointEx.height;

                // Calculate bar position
                RectangleF rectSize = RectangleF.Empty;
                try
                {
                    // Set the bar rectangle
                    rectSize.Y = ( float ) (pointEx.xPosition - (pointEx.width / 2));
                    rectSize.Height = ( float ) pointEx.width;

                    // The left side of rectangle has always 
                    // smaller value than a right value
                    if ( zero < height )
                    {
                        rectSize.X = ( float ) zero;
                        rectSize.Width = ( float ) height - rectSize.X;
                    }
                    else
                    {
                        rectSize.X = ( float ) height;
                        rectSize.Width = ( float ) zero - rectSize.X;
                    }
                }
                catch ( OverflowException )
                {
                    continue;
                }

                // if data point is not empty
                if ( point.IsEmpty )
                {
                    continue;
                }

                // Axis is logarithmic
                if ( hAxis.IsLogarithmic )
                {
                    barZeroValue = Math.Log( barZeroValue, hAxis.logarithmBase );
                }

                // Check if column is completely out of the data scaleView
                if ( xValue < vAxis.ViewMinimum ||
                    xValue > vAxis.ViewMaximum ||
                    (yValue < hAxis.ViewMinimum && barZeroValue < hAxis.ViewMinimum) ||
                    (yValue > hAxis.ViewMaximum && barZeroValue > hAxis.ViewMaximum) )
                {
                    continue;
                }

                // Draw 3D labels
                this.DrawLabels3D( area, graph, common, rectSize, pointEx, ser, barZeroValue, height, pointEx.width, pointEx.index - 1 );
            }
        }
    }

    /// <summary>
    /// Draws labels in 3D.
    /// </summary>
    /// <param name="area">Chart area for this chart.</param>
    /// <param name="graph">The Chart Graphics object.</param>
    /// <param name="common">The Common elements object.</param>
    /// <param name="rectSize">Bar rectangle.</param>
    /// <param name="pointEx">Data point.</param>
    /// <param name="ser">Data series.</param>
    /// <param name="barStartPosition">The zero position or the bottom of bars.</param>
    /// <param name="barSize">The Height of bars.</param>
    /// <param name="width">The width of bars.</param>
    /// <param name="pointIndex">Point index.</param>
    private void DrawLabels3D(
        ChartArea area,
        ChartGraphics graph,
        CommonElements common,
        RectangleF rectSize,
        DataPoint3D pointEx,
        Series ser,
        double barStartPosition,
        double barSize,
        double width,
        int pointIndex )
    {
        DataPoint point = pointEx.dataPoint;

        // ============================
        // Draw data point value label
        // ============================
        if ( ser.IsValueShownAsLabel || point.IsValueShownAsLabel || point.Label.Length > 0 )
        {
            // Label rectangle
            RectangleF rectLabel = RectangleF.Empty;

            // Label text format
            using StringFormat format = new();

            // ============================
            // Get label text 
            // ============================
            string text;
            if ( point.Label.Length == 0 )
            {
                // Round Y values for 100% stacked bar
                double pointLabelValue = this.GetYValue( common, area, ser, point, pointIndex, -2 );
                if ( this.HundredPercentStacked && point.LabelFormat.Length == 0 )
                {
                    pointLabelValue = Math.Round( pointLabelValue, 2 );
                }

                text = ValueConverter.FormatValue(
                    ser.Chart,
                    point,
                    point.Tag,
                    pointLabelValue,
                    point.LabelFormat,
                    ser.YValueType,
                    ChartElementType.DataPoint );
            }
            else
            {
                text = point.ReplaceKeywords( point.Label );
            }


            // ============================
            // Check labels style custom properties 
            // ============================
            BarValueLabelDrawingStyle drawingStyle = BarValueLabelDrawingStyle.Center;
            string valueLabelAttrib = "";
            if ( point.IsCustomPropertySet( CustomPropertyName.BarLabelStyle ) )
            {
                valueLabelAttrib = point[CustomPropertyName.BarLabelStyle];
            }
            else if ( ser.IsCustomPropertySet( CustomPropertyName.BarLabelStyle ) )
            {
                valueLabelAttrib = ser[CustomPropertyName.BarLabelStyle];
            }

            if ( valueLabelAttrib != null && valueLabelAttrib.Length > 0 )
            {
                if ( string.Compare( valueLabelAttrib, "Left", StringComparison.OrdinalIgnoreCase ) == 0 )
                    drawingStyle = BarValueLabelDrawingStyle.Left;
                else if ( string.Compare( valueLabelAttrib, "Right", StringComparison.OrdinalIgnoreCase ) == 0 )
                    drawingStyle = BarValueLabelDrawingStyle.Right;
                else if ( string.Compare( valueLabelAttrib, "Center", StringComparison.OrdinalIgnoreCase ) == 0 )
                    drawingStyle = BarValueLabelDrawingStyle.Center;
                else if ( string.Compare( valueLabelAttrib, "Outside", StringComparison.OrdinalIgnoreCase ) == 0 )
                    drawingStyle = BarValueLabelDrawingStyle.Outside;
            }

            // ============================
            // Make sure label fits. Otherwise change it style
            // ============================
            bool labelFit = false;
            while ( !labelFit )
            {
                // Label text format
                format.Alignment = StringAlignment.Near;
                format.LineAlignment = StringAlignment.Center;

                // LabelStyle rectangle
                if ( barStartPosition < barSize )
                {
                    rectLabel.X = rectSize.Right;
                    rectLabel.Width = area.PlotAreaPosition.Right - rectSize.Right;
                }
                else
                {
                    rectLabel.X = area.PlotAreaPosition.X;
                    rectLabel.Width = rectSize.X - area.PlotAreaPosition.X;
                }

                // Adjust label rectangle
                rectLabel.Y = rectSize.Y - (( float ) width / 2F);
                rectLabel.Height = rectSize.Height + ( float ) width;

                // Adjust label position depending on the drawing style
                if ( drawingStyle == BarValueLabelDrawingStyle.Left )
                {
                    rectLabel = rectSize;
                    format.Alignment = StringAlignment.Near;
                }
                else if ( drawingStyle == BarValueLabelDrawingStyle.Center )
                {
                    rectLabel = rectSize;
                    format.Alignment = StringAlignment.Center;
                }
                else if ( drawingStyle == BarValueLabelDrawingStyle.Right )
                {
                    rectLabel = rectSize;
                    format.Alignment = StringAlignment.Far;
                }

                // Reversed string alignment
                if ( barStartPosition >= barSize )
                {
                    if ( format.Alignment == StringAlignment.Far )
                        format.Alignment = StringAlignment.Near;
                    else if ( format.Alignment == StringAlignment.Near )
                        format.Alignment = StringAlignment.Far;
                }

                // Stacked bar chart can not change the BarValueLabelDrawingStyle trying to
                // fit data point labels because it will cause label overlapping.
                // NOTE: Code below is commented. Fixes issue #4687 - AG
                labelFit = true;

                //					// Make sure value label fits rectangle. 
                //					SizeF valueTextSize = graph.MeasureStringRel(text, point.Font);
                //					if(!labelSwitched && valueTextSize.Width > rectLabel.Width)
                //					{
                //						// Switch label style only once
                //						labelSwitched = true;
                //
                //						// If text do not fit - try to switch between Outside/Inside drawing styles
                //						if(drawingStyle == BarValueLabelDrawingStyle.Outside)
                //						{
                //							drawingStyle = BarValueLabelDrawingStyle.Right;
                //						}
                //						else
                //						{
                //							drawingStyle = BarValueLabelDrawingStyle.Outside;
                //						}
                //					}
                //					else
                //					{
                //						labelFit = true;
                //					}
            }

            // ============================
            // Find text rotation center point
            // ============================

            // Measure string size
            SizeF size = graph.MeasureStringRel( text, point.Font, new SizeF( rectLabel.Width, rectLabel.Height ), format );

            PointF rotationCenter = PointF.Empty;
            rotationCenter.X = format.Alignment == StringAlignment.Near
                ? rectLabel.X + (size.Width / 2)
                : format.Alignment == StringAlignment.Far ? rectLabel.Right - (size.Width / 2) : (rectLabel.Left + rectLabel.Right) / 2;

            rotationCenter.Y = format.LineAlignment == StringAlignment.Near
                ? rectLabel.Top + (size.Height / 2)
                : format.LineAlignment == StringAlignment.Far ? rectLabel.Bottom - (size.Height / 2) : (rectLabel.Bottom + rectLabel.Top) / 2;

            // Reset string alignment to center point
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;


            // ============================
            // Adjust label rotation angle
            // ============================
            int angle = point.LabelAngle;

            // Get projection coordinates
            Point3D[] rotationCenterProjection = [
                                                                     new Point3D(rotationCenter.X, rotationCenter.Y, pointEx.zPosition + pointEx.depth),
                                                                     new Point3D(rotationCenter.X - 20f, rotationCenter.Y, pointEx.zPosition + pointEx.depth) ];
            // Transform coordinates of text rotation point
            area.matrix3D.TransformPoints( rotationCenterProjection );

            // Adjust rotation point
            rotationCenter = rotationCenterProjection[0].PointF;

            // Adjust angle of the horizontal text
            if ( angle is 0 or 180 )
            {
                // Convert coordinates to absolute
                rotationCenterProjection[0].PointF = graph.GetAbsolutePoint( rotationCenterProjection[0].PointF );
                rotationCenterProjection[1].PointF = graph.GetAbsolutePoint( rotationCenterProjection[1].PointF );

                // Calculate axis angle
                float angleXAxis = ( float ) Math.Atan(
                    (rotationCenterProjection[1].Y - rotationCenterProjection[0].Y) /
                    (rotationCenterProjection[1].X - rotationCenterProjection[0].X) );
                angleXAxis = ( float ) Math.Round( angleXAxis * 180f / ( float ) Math.PI );
                angle += ( int ) angleXAxis;
            }

            SizeF sizeFont = SizeF.Empty;


            // Check if Smart Labels are enabled
            if ( ser.SmartLabelStyle.Enabled )
            {
                sizeFont = graph.GetRelativeSize(
                    graph.MeasureString(
                    text,
                    point.Font,
                    new SizeF( 1000f, 1000f ),
                    StringFormat.GenericTypographic ) );

                // Force some SmartLabelStyle settings for column chart
                bool oldMarkerOverlapping = ser.SmartLabelStyle.IsMarkerOverlappingAllowed;
                LabelAlignmentStyles oldMovingDirection = ser.SmartLabelStyle.MovingDirection;
                ser.SmartLabelStyle.IsMarkerOverlappingAllowed = true;
                if ( ser.SmartLabelStyle.MovingDirection == (LabelAlignmentStyles.Top | LabelAlignmentStyles.Bottom | LabelAlignmentStyles.Right | LabelAlignmentStyles.Left | LabelAlignmentStyles.TopLeft | LabelAlignmentStyles.TopRight | LabelAlignmentStyles.BottomLeft | LabelAlignmentStyles.BottomRight) )
                {
                    ser.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.Left | LabelAlignmentStyles.Right;
                }

                // Adjust label position using SmartLabelStyle algorithm
                rotationCenter = area.smartLabels.AdjustSmartLabelPosition(
                    common,
                    graph,
                    area,
                    ser.SmartLabelStyle,
                    rotationCenter,
                    sizeFont,
                    format,
                    rotationCenter,
                    new SizeF( 0f, 0f ),
                    LabelAlignmentStyles.Center );

                // Restore forced values
                ser.SmartLabelStyle.IsMarkerOverlappingAllowed = oldMarkerOverlapping;
                ser.SmartLabelStyle.MovingDirection = oldMovingDirection;

                // Smart labels always use 0 degrees text angle
                angle = 0;
            }





            // ============================
            // Draw label
            // ============================
            if ( !rotationCenter.IsEmpty )
            {
                // Measure string
                if ( sizeFont.IsEmpty )
                {
                    sizeFont = graph.GetRelativeSize(
                        graph.MeasureString(
                        text,
                        point.Font,
                        new SizeF( 1000f, 1000f ),
                        new StringFormat( StringFormat.GenericTypographic ) ) );
                }

                // Get label background position
                RectangleF labelBackPosition = RectangleF.Empty;
                SizeF sizeLabel = new( sizeFont.Width, sizeFont.Height );
                sizeLabel.Height += sizeFont.Height / 8;
                sizeLabel.Width += sizeLabel.Width / text.Length;
                labelBackPosition = new RectangleF(
                    rotationCenter.X - (sizeLabel.Width / 2),
                    rotationCenter.Y - (sizeLabel.Height / 2) - (sizeFont.Height / 10),
                    sizeLabel.Width,
                    sizeLabel.Height );

                // Draw label text
                using Brush brush = new SolidBrush( point.LabelForeColor );
                graph.DrawPointLabelStringRel(
                    common,
                    text,
                    point.Font,
                    brush,
                    rotationCenter,
                    format,
                    angle,
                    labelBackPosition,
                    point.LabelBackColor,
                    point.LabelBorderColor,
                    point.LabelBorderWidth,
                    point.LabelBorderDashStyle,
                    ser,
                    point,
                    pointIndex );
            }
        }
    }

    #endregion

    #region " smartlabelstyle methods "

    /// <summary>
    /// Adds markers position to the list. Used to check SmartLabelStyle overlapping.
    /// </summary>
    /// <param name="common">Common chart elements.</param>
    /// <param name="area">Chart area.</param>
    /// <param name="series">Series values to be used.</param>
    /// <param name="list">List to add to.</param>
    public void AddSmartLabelMarkerPositions( CommonElements common, ChartArea area, Series series, ArrayList list )
    {
        // NOTE: Stacked Bar chart type do not support SmartLabelStyle feature
    }

    #endregion

    #region " idisposable interface implementation "
    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose( bool disposing )
    {
        //Nothing to dispose at the base class. 
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }
    #endregion
}

