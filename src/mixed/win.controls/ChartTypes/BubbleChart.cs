//
//  Purpose:	Bubble chart type is similar to the Point chart 
//              where each data point is presented with a marker 
//              positioned using X and Y values. The difference 
//              of the Bubble chart is that an additional Y value 
//              is used to control the size of the marker.
//


using System.Drawing;
using System.Globalization;
using System.Windows.Forms.DataVisualization.Charting.Utilities;

namespace System.Windows.Forms.DataVisualization.Charting.ChartTypes;
/// <summary>
/// BubbleChart class extends PointChart class to add support for
/// additional Y value which controls the size of the markers used.
/// </summary>
internal class BubbleChart : PointChart
{
    #region " fields and constructor "

#pragma warning disable IDE1006 // Naming Styles

    // Indicates that bubble size scale is calculated
    private bool _scaleDetected;

    // Minimum/Maximum bubble size
    private double _maxPossibleBubbleSize = 15F;
    private double _minPossibleBubbleSize = 3F;
    private float _maxBubleSize;
    private float _minBubleSize;

    // Current min/max size of the bubble size
    private double _minAll = double.MaxValue;
    private double _maxAll = double.MinValue;


    // Bubble size difference value
    private double _valueDiff;
    private double _valueScale = 1;

#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Class public constructor
    /// </summary>
    public BubbleChart() : base( true )
    {
    }

    #endregion

    #region " icharttype interface implementation "

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => ChartTypeNames.Bubble;

    /// <summary>
    /// Number of supported Y value(s) per point 
    /// </summary>
    public override int YValuesPerPoint => 2;

    /// <summary>
    /// Chart type with two y values used for scale ( bubble chart type )
    /// </summary>
    public override bool SecondYScale => true;

    /// <summary>
    /// Gets chart type image.
    /// </summary>
    /// <param name="registry">Chart types registry object.</param>
    /// <returns>Chart type image.</returns>
    public override Image GetImage( ChartTypeRegistry registry )
    {
        return ( Image ) registry.ResourceManager.GetObject( this.Name + "ChartType" );
    }

    #endregion

    #region " bubble chart methods "

    /// <summary>
    /// This method recalculates size of the bars. This method is used 
    /// from Paint or Select method.
    /// </summary>
    /// <param name="selection">If True selection mode is active, otherwise paint mode is active</param>
    /// <param name="graph">The Chart Graphics object</param>
    /// <param name="common">The Common elements object</param>
    /// <param name="area">Chart area for this chart</param>
    /// <param name="seriesToDraw">Chart series to draw.</param>
    protected override void ProcessChartType(
        bool selection,
        ChartGraphics graph,
        CommonElements common,
        ChartArea area,
        Series seriesToDraw )
    {
        this._scaleDetected = false;
        base.ProcessChartType( selection, graph, common, area, seriesToDraw );
    }

    /// <summary>
    /// Gets marker border size.
    /// </summary>
    /// <param name="point">Data point.</param>
    /// <returns>Marker border size.</returns>
    protected override int GetMarkerBorderSize( DataPointCustomProperties point )
    {
        return point.series != null ? point.series.BorderWidth : 1;
    }

    /// <summary>
    /// Returns marker size.
    /// </summary>
    /// <param name="graph">The Chart Graphics object.</param>
    /// <param name="common">The Common elements object.</param>
    /// <param name="area">Chart area for this chart.</param>
    /// <param name="point">Data point.</param>
    /// <param name="markerSize">Marker size.</param>
    /// <param name="markerImage">Marker image.</param>
    /// <returns>Marker width and height.</returns>
    protected override SizeF GetMarkerSize(
        ChartGraphics graph,
        CommonElements common,
        ChartArea area,
        DataPoint point,
        int markerSize,
        string markerImage )
    {
        // Check required Y values number
        if ( point.YValues.Length < this.YValuesPerPoint )
        {
            throw new InvalidOperationException( SR.ExceptionChartTypeRequiresYValues( this.Name, this.YValuesPerPoint.ToString( CultureInfo.InvariantCulture ) ) );
        }

        // Marker size
        SizeF size = new( markerSize, markerSize );
        if ( graph != null && graph.Graphics is not null )
        {
            // Marker size is in pixels and we do the mapping for higher DPIs
            size.Width = markerSize * graph.Graphics.DpiX / 96;
            size.Height = markerSize * graph.Graphics.DpiY / 96;
        }

        // Check number of Y values for non empty points
        if ( point.series.YValuesPerPoint > 1 && !point.IsEmpty )
        {
            // Scale Y values
            size.Width = this.ScaleBubbleSize( graph, common, area, point.YValues[1] );
            size.Height = this.ScaleBubbleSize( graph, common, area, point.YValues[1] );
        }

        return size;
    }

    /// <summary>
    /// Scales the value used to determine the size of the Bubble.
    /// </summary>
    /// <param name="graph">The Chart Graphics object</param>
    /// <param name="common">The Common elements object</param>
    /// <param name="area">Chart area for this chart</param>
    /// <param name="value">Value to scale.</param>
    /// <returns>Scaled values.</returns>
    private float ScaleBubbleSize( ChartGraphics graph, CommonElements common, ChartArea area, double value )
    {
        // Check if scaling numbers are detected
        if ( !this._scaleDetected )
        {
            // Try to find bubble size scale in the custom series properties
            this._minAll = double.MaxValue;
            this._maxAll = double.MinValue;
            foreach ( Series ser in common.DataManager.Series )
            {
                if ( string.Compare( ser.ChartTypeName, this.Name, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 &&
                    ser.ChartArea == area.Name &&
                    ser.IsVisible() )
                {
                    // Check if custom properties are set to specify scale
                    if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleScaleMin ) )
                    {
                        this._minAll = Math.Min( this._minAll, CommonElements.ParseDouble( ser[CustomPropertyName.BubbleScaleMin] ) );
                    }
                    if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleScaleMax ) )
                    {
                        this._maxAll = Math.Max( this._maxAll, CommonElements.ParseDouble( ser[CustomPropertyName.BubbleScaleMax] ) );
                    }

                    // Check if attribute for max. size is set
                    if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleMaxSize ) )
                    {
                        this._maxPossibleBubbleSize = CommonElements.ParseDouble( ser[CustomPropertyName.BubbleMaxSize] );
                        if ( this._maxPossibleBubbleSize is < 0 or > 100 )
                        {
                            throw new ArgumentException( SR.ExceptionCustomAttributeIsNotInRange0to100( "BubbleMaxSize" ) );
                        }
                    }

                    // Check if attribute for min. size is set
                    if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleMinSize ) )
                    {
                        this._minPossibleBubbleSize = CommonElements.ParseDouble( ser[CustomPropertyName.BubbleMinSize] );
                        if ( this._minPossibleBubbleSize is < 0 or > 100 )
                        {
                            throw new ArgumentException( SR.ExceptionCustomAttributeIsNotInRange0to100( "BubbleMinSize" ) );
                        }
                    }


                    // Check if custom properties set to use second Y value (bubble size) as label text
                    this.labelYValueIndex = 0;
                    if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleUseSizeForLabel ) )
                    {
                        if ( string.Compare( ser[CustomPropertyName.BubbleUseSizeForLabel], "true", StringComparison.OrdinalIgnoreCase ) == 0 )
                        {
                            this.labelYValueIndex = 1;
                            break;
                        }
                    }
                }
            }

            // Scale values are not specified - auto detect
            if ( this._minAll == double.MaxValue || this._maxAll == double.MinValue )
            {
                double minSer = double.MaxValue;
                double maxSer = double.MinValue;
                foreach ( Series ser in common.DataManager.Series )
                {
                    if ( ser.ChartTypeName == this.Name && ser.ChartArea == area.Name && ser.IsVisible() )
                    {
                        foreach ( DataPoint point in ser.Points )
                        {
                            if ( !point.IsEmpty )
                            {
                                // Check required Y values number
                                if ( point.YValues.Length < this.YValuesPerPoint )
                                {
                                    throw new InvalidOperationException( SR.ExceptionChartTypeRequiresYValues( this.Name, this.YValuesPerPoint.ToString( CultureInfo.InvariantCulture ) ) );
                                }

                                minSer = Math.Min( minSer, point.YValues[1] );
                                maxSer = Math.Max( maxSer, point.YValues[1] );
                            }
                        }
                    }
                }
                if ( this._minAll == double.MaxValue )
                {
                    this._minAll = minSer;
                }
                if ( this._maxAll == double.MinValue )
                {
                    this._maxAll = maxSer;
                }
            }

            // Calculate maximum bubble size
            SizeF areaSize = graph.GetAbsoluteSize( area.PlotAreaPosition.Size );
            this._maxBubleSize = ( float ) (Math.Min( areaSize.Width, areaSize.Height ) / (100.0 / this._maxPossibleBubbleSize));
            this._minBubleSize = ( float ) (Math.Min( areaSize.Width, areaSize.Height ) / (100.0 / this._minPossibleBubbleSize));

            // Calculate scaling variables depending on the Min/Max values
            if ( this._maxAll == this._minAll )
            {
                this._valueScale = 1;
                this._valueDiff = this._minAll - ((this._maxBubleSize - this._minBubleSize) / 2f);
            }
            else
            {
                this._valueScale = (this._maxBubleSize - this._minBubleSize) / (this._maxAll - this._minAll);
                this._valueDiff = this._minAll;
            }

            this._scaleDetected = true;
        }

        // Check if value do not exceed Min&Max
        if ( value > this._maxAll )
        {
            return 0F;
        }
        if ( value < this._minAll )
        {
            return 0F;
        }

        // Return scaled value
        return ( float ) ((value - this._valueDiff) * this._valueScale) + this._minBubleSize;
    }

    /// <summary>
    /// Scales the value used to determine the size of the Bubble.
    /// </summary>
    /// <param name="common">The Common elements object</param>
    /// <param name="area">Chart area for this chart</param>
    /// <param name="value">Value to scale.</param>
    /// <param name="yValue">True if Y value is calculated, false if X.</param>
    /// <returns>Scaled values.</returns>
    internal static double AxisScaleBubbleSize( CommonElements common, ChartArea area, double value, bool yValue )
    {
        // Try to find bubble size scale in the custom series properties
        double minAll = double.MaxValue;
        double maxAll = double.MinValue;
        double maxPossibleBubbleSize = 15F;
        double minPossibleBubbleSize = 3F;
        float maxBubleSize;
        float minBubleSize;
        double valueScale;
        double valueDiff;
        foreach ( Series ser in common.DataManager.Series )
        {
            if ( string.Compare( ser.ChartTypeName, ChartTypeNames.Bubble, StringComparison.OrdinalIgnoreCase ) == 0 &&
                ser.ChartArea == area.Name &&
                ser.IsVisible() )
            {
                // Check if custom properties are set to specify scale
                if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleScaleMin ) )
                {
                    minAll = Math.Min( minAll, CommonElements.ParseDouble( ser[CustomPropertyName.BubbleScaleMin] ) );
                }
                if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleScaleMax ) )
                {
                    maxAll = Math.Max( maxAll, CommonElements.ParseDouble( ser[CustomPropertyName.BubbleScaleMax] ) );
                }

                // Check if attribute for max. size is set
                if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleMaxSize ) )
                {
                    maxPossibleBubbleSize = CommonElements.ParseDouble( ser[CustomPropertyName.BubbleMaxSize] );
                    if ( maxPossibleBubbleSize is < 0 or > 100 )
                    {
                        throw new ArgumentException( SR.ExceptionCustomAttributeIsNotInRange0to100( "BubbleMaxSize" ) );
                    }
                }

                // Check if custom properties set to use second Y value (bubble size) as label text
                if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleUseSizeForLabel ) )
                {
                    if ( string.Compare( ser[CustomPropertyName.BubbleUseSizeForLabel], "true", StringComparison.OrdinalIgnoreCase ) == 0 )
                    {
                        break;
                    }
                }
            }
        }

        // Scale values are not specified - auto detect
        double minimum = double.MaxValue;
        double maximum = double.MinValue;
        double minSer = double.MaxValue;
        double maxSer = double.MinValue;
        foreach ( Series ser in common.DataManager.Series )
        {
            if ( string.Compare( ser.ChartTypeName, ChartTypeNames.Bubble, StringComparison.OrdinalIgnoreCase ) == 0
                && ser.ChartArea == area.Name
                && ser.IsVisible() )
            {
                foreach ( DataPoint point in ser.Points )
                {
                    if ( !point.IsEmpty )
                    {
                        minSer = Math.Min( minSer, point.YValues[1] );
                        maxSer = Math.Max( maxSer, point.YValues[1] );

                        if ( yValue )
                        {
                            minimum = Math.Min( minimum, point.YValues[0] );
                            maximum = Math.Max( maximum, point.YValues[0] );
                        }
                        else
                        {
                            minimum = Math.Min( minimum, point.XValue );
                            maximum = Math.Max( maximum, point.XValue );
                        }
                    }
                }
            }
        }
        if ( minAll == double.MaxValue )
        {
            minAll = minSer;
        }
        if ( maxAll == double.MinValue )
        {
            maxAll = maxSer;
        }

        // Calculate maximum bubble size
        maxBubleSize = ( float ) ((maximum - minimum) / (100.0 / maxPossibleBubbleSize));
        minBubleSize = ( float ) ((maximum - minimum) / (100.0 / minPossibleBubbleSize));

        // Calculate scaling variables depending on the Min/Max values
        if ( maxAll == minAll )
        {
            valueScale = 1;
            valueDiff = minAll - ((maxBubleSize - minBubleSize) / 2f);
        }
        else
        {
            valueScale = (maxBubleSize - minBubleSize) / (maxAll - minAll);
            valueDiff = minAll;
        }


        // Check if value do not exceed Min&Max
        if ( value > maxAll )
        {
            return 0F;
        }
        if ( value < minAll )
        {
            return 0F;
        }

        // Return scaled value
        return ( float ) ((value - valueDiff) * valueScale) + minBubleSize;
    }

    /// <summary>
    /// Get value from custom attribute BubbleMaxSize 
    /// </summary>
    /// <param name="area">Chart Area</param>
    /// <returns>Bubble Max size</returns>
    internal static double GetBubbleMaxSize( ChartArea area )
    {
        double maxPossibleBubbleSize = 15;
        // Try to find bubble size scale in the custom series properties
        foreach ( Series ser in area.Common.DataManager.Series )
        {
            if ( string.Compare( ser.ChartTypeName, ChartTypeNames.Bubble, StringComparison.OrdinalIgnoreCase ) == 0 &&
                ser.ChartArea == area.Name &&
                ser.IsVisible() )
            {
                // Check if attribute for max. size is set
                if ( ser.IsCustomPropertySet( CustomPropertyName.BubbleMaxSize ) )
                {
                    maxPossibleBubbleSize = CommonElements.ParseDouble( ser[CustomPropertyName.BubbleMaxSize] );
                    if ( maxPossibleBubbleSize is < 0 or > 100 )
                    {
                        throw new ArgumentException( SR.ExceptionCustomAttributeIsNotInRange0to100( "BubbleMaxSize" ) );
                    }
                }
            }
        }

        return maxPossibleBubbleSize / 100;
    }

    #endregion
}
