//
//  Purpose:	Classes that implement different 3D border styles.
//

#pragma warning disable IDE1006 // Naming Styles

using System.Drawing;
using System.Drawing.Drawing2D;

namespace System.Windows.Forms.DataVisualization.Charting.Borders3D;
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle1Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle1Border() => this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle1";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }
    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle2Border : FrameThin2Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle2Border() => this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle2";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle3Border : FrameThin3Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle3Border() => this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle3";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }
    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle4Border : FrameThin4Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle4Border() => this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle4";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle5Border : FrameThin5Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle5Border()
    {
        this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        this.drawScrews = true;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle5";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle6Border : FrameThin6Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle6Border() => this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle6";

    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeLeftTop = new SizeF( this.sizeLeftTop.Width, this.defaultRadiusSize * 2f );
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public override RectangleF GetTitlePositionInBorder()
    {
        return new RectangleF(
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 0.25f,
            this.defaultRadiusSize * 1.6f );
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle7Border : FrameTitle1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle7Border()
    {
        this.sizeRightBottom = new SizeF( 0, this.sizeRightBottom.Height );
        float[] corners = [15f, 1f, 1f, 1f, 1f, 15f, 15f, 15f];
        this.innerCorners = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle7";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            this.sizeRightBottom = new SizeF( 0, this.sizeRightBottom.Height );
            float largeRadius = 15f * this.resolution / 96.0f;
            float smallRadius = 1 * this.resolution / 96.0f;
            float[] corners = [largeRadius, smallRadius, smallRadius, smallRadius, smallRadius, largeRadius, largeRadius, largeRadius];
            this.innerCorners = corners;
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameTitle8Border : FrameTitle1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameTitle8Border()
    {
        this.sizeLeftTop = new SizeF( 0, this.sizeLeftTop.Height );
        this.sizeRightBottom = new SizeF( 0, this.sizeRightBottom.Height );
        float[] corners = [1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f];
        this.innerCorners = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameTitle8";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;

            this.sizeLeftTop = new SizeF( 0, this.sizeLeftTop.Height );
            this.sizeRightBottom = new SizeF( 0, this.sizeRightBottom.Height );
            float radius = 1 * this.resolution / 96.0f;
            float[] corners = [radius, radius, radius, radius, radius, radius, radius, radius];
            this.innerCorners = corners;
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin2Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin2Border()
    {
        float[] corners = [15f, 15f, 15f, 1f, 1f, 1f, 1f, 15f];
        this.cornerRadius = corners;
        this.innerCorners = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin2";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;

            float largeRadius = 15f * this.resolution / 96.0f;
            float smallRadius = 1 * this.resolution / 96.0f;
            float[] corners = [largeRadius, largeRadius, largeRadius, smallRadius, smallRadius, smallRadius, smallRadius, largeRadius];
            this.cornerRadius = corners;
            this.innerCorners = corners;
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin3Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin3Border()
    {
        float[] corners = [1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f];
        this.cornerRadius = corners;
        this.innerCorners = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin3";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            float radius = this.resolution / 96.0f;
            float[] corners = [radius, radius, radius, radius, radius, radius, radius, radius];
            this.cornerRadius = corners;
            this.innerCorners = corners;
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin4Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin4Border()
    {
        float[] corners = [1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f];
        this.cornerRadius = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin4";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            float radius = 1f * this.resolution / 96.0f;
            this.cornerRadius = [radius, radius, radius, radius, radius, radius, radius, radius];
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin5Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin5Border() => this.drawScrews = true;

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin5";

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin6Border : FrameThin1Border
{
    #region " border properties and methods "

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin6Border()
    {
        float[] corners = [1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f];
        this.innerCorners = corners;
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin6";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            float radius = this.resolution / 96.0f;
            float[] corners = [radius, radius, radius, radius, radius, radius, radius, radius];
            this.innerCorners = corners;
        }
    }

    #endregion
}
/// <summary>
/// Implements frame border.
/// </summary>
internal class FrameThin1Border : RaisedBorder
{
    #region " border properties and methods "

    /// <summary>
    /// Inner corners radius array
    /// </summary>
    internal float[] innerCorners = [15f, 15f, 15f, 15f, 15f, 15f, 15f, 15f];

    /// <summary>
    /// Default constructor
    /// </summary>
    public FrameThin1Border()
    {
        this.sizeLeftTop = new SizeF( this.defaultRadiusSize * .8f, this.defaultRadiusSize * .8f );
        this.sizeRightBottom = new SizeF( this.defaultRadiusSize * .8f, this.defaultRadiusSize * .8f );
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "FrameThin1";


    public override float Resolution
    {
        set
        {
            base.Resolution = value;
            float radius = 15.0f * this.resolution / 96.0f;
            this.innerCorners = [radius, radius, radius, radius, radius, radius, radius, radius];
            this.sizeLeftTop = new SizeF( this.defaultRadiusSize * .8f, this.defaultRadiusSize * .8f );
            this.sizeRightBottom = new SizeF( this.defaultRadiusSize * .8f, this.defaultRadiusSize * .8f );

        }
    }

    /// <summary>
    /// Draws 3D border.
    /// </summary>
    /// <param name="graph">Graphics to draw the border on.</param>
    /// <param name="borderSkin">Border skin object.</param>
    /// <param name="rect">Rectangle of the border.</param>
    /// <param name="backColor">Color of rectangle</param>
    /// <param name="backHatchStyle">Hatch style</param>
    /// <param name="backImage">Back Image</param>
    /// <param name="backImageWrapMode">Image mode</param>
    /// <param name="backImageTransparentColor">Image transparent color.</param>
    /// <param name="backImageAlign">Image alignment</param>
    /// <param name="backGradientStyle">Gradient type</param>
    /// <param name="backSecondaryColor">Gradient End Color</param>
    /// <param name="borderColor">Border Color</param>
    /// <param name="borderWidth">Border Width</param>
    /// <param name="borderDashStyle">Border Style</param>
    public override void DrawBorder(
    ChartGraphics graph,
    BorderSkin borderSkin,
    RectangleF rect,
    Color backColor,
    ChartHatchStyle backHatchStyle,
    string backImage,
    ChartImageWrapMode backImageWrapMode,
    Color backImageTransparentColor,
    ChartImageAlignmentStyle backImageAlign,
    GradientStyle backGradientStyle,
    Color backSecondaryColor,
    Color borderColor,
    int borderWidth,
    ChartDashStyle borderDashStyle )
    {
        this.drawBottomShadow = true;
        this.sunken = false;
        this.outsideShadowRate = .9f;
        this.drawOutsideTopLeftShadow = false;
        bool oldScrewsFlag = this.drawScrews;
        this.drawScrews = false;
        base.DrawBorder(
            graph,
            borderSkin,
            rect,
            borderSkin.BackColor,
            borderSkin.BackHatchStyle,
            borderSkin.BackImage,
            borderSkin.BackImageWrapMode,
            borderSkin.BackImageTransparentColor,
            borderSkin.BackImageAlignment,
            borderSkin.BackGradientStyle,
            borderSkin.BackSecondaryColor,
            borderSkin.BorderColor,
            borderSkin.BorderWidth,
            borderSkin.BorderDashStyle );

        this.drawScrews = oldScrewsFlag;
        rect.X += this.sizeLeftTop.Width;
        rect.Y += this.sizeLeftTop.Height;
        rect.Width -= this.sizeRightBottom.Width + this.sizeLeftTop.Width;
        rect.Height -= this.sizeRightBottom.Height + this.sizeLeftTop.Height;
        if ( rect.Width > 0 && rect.Height > 0 )
        {
            float[] oldCorners = new float[8];
            oldCorners = ( float[] ) this.cornerRadius.Clone();
            this.cornerRadius = this.innerCorners;
            this.drawBottomShadow = false;
            this.sunken = true;
            this.drawOutsideTopLeftShadow = true;
            this.outsideShadowRate = 1.4f;
            Color oldPageColor = borderSkin.PageColor;
            borderSkin.PageColor = Color.Transparent;
            base.DrawBorder(
                graph,
                borderSkin,
                rect,
                backColor,
                backHatchStyle,
                backImage,
                backImageWrapMode,
                backImageTransparentColor,
                backImageAlign,
                backGradientStyle,
                backSecondaryColor,
                borderColor,
                borderWidth,
                borderDashStyle );
            borderSkin.PageColor = oldPageColor;
            this.cornerRadius = oldCorners;
        }
    }

    #endregion
}
/// <summary>
/// Implements raised border.
/// </summary>
internal class RaisedBorder : SunkenBorder
{
    #region " border properties and methods "

    /// <summary>
    /// Public constructor
    /// </summary>
    public RaisedBorder() => this.sunken = false;

    /// <summary>
    /// Chart type name
    /// </summary>
    public override string Name => "Raised";

    #endregion
}
/// <summary>
/// Implements embed 3D border.
/// </summary>
internal class SunkenBorder : IBorderType
{
    #region " border properties and methods "

    /// <summary>
    /// Radius for rounded rectangle
    /// </summary>
    internal float defaultRadiusSize = 15f;

    /// <summary>
    /// Outside shadow rate
    /// </summary>
    internal float outsideShadowRate = .9f;

    /// <summary>
    /// Indicates that sunken shadows should be drawn
    /// </summary>
    internal bool sunken = true;

    /// <summary>
    /// Indicates that bottom shadow should be drawn
    /// </summary>
    internal bool drawBottomShadow = true;

    /// <summary>
    /// Indicates that top left outside dark shadow must be drawn
    /// </summary>
    internal bool drawOutsideTopLeftShadow;

    /// <summary>
    /// Array of corner radius
    /// </summary>
    internal float[] cornerRadius = [15f, 15f, 15f, 15f, 15f, 15f, 15f, 15f];

    /// <summary>
    /// Border top/left size 
    /// </summary>
    internal SizeF sizeLeftTop = SizeF.Empty;

    /// <summary>
    /// Border right/bottom size
    /// </summary>
    internal SizeF sizeRightBottom = SizeF.Empty;

    /// <summary>
    /// Indicates that screws should be drawn in the corners of the frame
    /// </summary>
    internal bool drawScrews;


    internal float resolution = 96f;

    /// <summary>
    /// Public constructor
    /// </summary>
    public SunkenBorder()
    {
    }

    /// <summary>
    /// Chart type name
    /// </summary>
    public virtual string Name => "Sunken";


    public virtual float Resolution
    {
        set
        {
            this.resolution = value;
            this.defaultRadiusSize = 15 * this.resolution / 96;
            //X = defaultRadiusSize;
            //Y = defaultRadiusSize;
            this.cornerRadius = [this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize, this.defaultRadiusSize];
        }
    }

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    public virtual RectangleF GetTitlePositionInBorder()
    {
        return RectangleF.Empty;
    }

    /// <summary>
    /// Adjust areas rectangle coordinate to fit the 3D border
    /// </summary>
    /// <param name="graph">Graphics to draw the border on.</param>
    /// <param name="areasRect">Position to adjust.</param>
    public virtual void AdjustAreasPosition( ChartGraphics graph, ref RectangleF areasRect )
    {
        SizeF relSizeLeftTop = new( this.sizeLeftTop );
        SizeF relSizeRightBottom = new( this.sizeRightBottom );
        relSizeLeftTop.Width += this.defaultRadiusSize * 0.7f;
        relSizeLeftTop.Height += this.defaultRadiusSize * 0.85f;
        relSizeRightBottom.Width += this.defaultRadiusSize * 0.7f;
        relSizeRightBottom.Height += this.defaultRadiusSize * 0.7f;
        relSizeLeftTop = graph.GetRelativeSize( relSizeLeftTop );
        relSizeRightBottom = graph.GetRelativeSize( relSizeRightBottom );

        if ( relSizeLeftTop.Width > 30f )
            relSizeLeftTop.Width = 0;
        if ( relSizeLeftTop.Height > 30f )
            relSizeLeftTop.Height = 0;
        if ( relSizeRightBottom.Width > 30f )
            relSizeRightBottom.Width = 0;
        if ( relSizeRightBottom.Height > 30f )
            relSizeRightBottom.Height = 0;


        areasRect.X += relSizeLeftTop.Width;
        areasRect.Width -= ( float ) Math.Min( areasRect.Width, relSizeLeftTop.Width + relSizeRightBottom.Width );
        areasRect.Y += relSizeLeftTop.Height;
        areasRect.Height -= ( float ) Math.Min( areasRect.Height, relSizeLeftTop.Height + relSizeRightBottom.Height );

        if ( areasRect.Right > 100f )
        {
            if ( areasRect.Width > 100f - areasRect.Right )
                areasRect.Width -= 100f - areasRect.Right;
            else
                areasRect.X -= 100f - areasRect.Right;
        }
        if ( areasRect.Bottom > 100f )
        {
            if ( areasRect.Height > 100f - areasRect.Bottom )
                areasRect.Height -= 100f - areasRect.Bottom;
            else
                areasRect.Y -= 100f - areasRect.Bottom;

        }
    }

    /// <summary>
    /// Draws 3D border
    /// </summary>
    /// <param name="graph">Graphics to draw the border on.</param>
    /// <param name="borderSkin">Border skin object.</param>
    /// <param name="rect">Rectangle of the border.</param>
    /// <param name="backColor">Color of rectangle</param>
    /// <param name="backHatchStyle">Hatch style</param>
    /// <param name="backImage">Back Image</param>
    /// <param name="backImageWrapMode">Image mode</param>
    /// <param name="backImageTransparentColor">Image transparent color.</param>
    /// <param name="backImageAlign">Image alignment</param>
    /// <param name="backGradientStyle">Gradient type</param>
    /// <param name="backSecondaryColor">Gradient End Color</param>
    /// <param name="borderColor">Border Color</param>
    /// <param name="borderWidth">Border Width</param>
    /// <param name="borderDashStyle">Border Style</param>
    public virtual void DrawBorder(
    ChartGraphics graph,
    BorderSkin borderSkin,
    RectangleF rect,
    Color backColor,
    ChartHatchStyle backHatchStyle,
    string backImage,
    ChartImageWrapMode backImageWrapMode,
    Color backImageTransparentColor,
    ChartImageAlignmentStyle backImageAlign,
    GradientStyle backGradientStyle,
    Color backSecondaryColor,
    Color borderColor,
    int borderWidth,
    ChartDashStyle borderDashStyle )
    {
        RectangleF absolute = graph.Round( rect );
        RectangleF shadowRect = absolute;

        // Calculate shadow colors (0.2 - 0.6)
        float colorDarkeningIndex = 0.3f + (0.4f * (borderSkin.PageColor.R + borderSkin.PageColor.G + borderSkin.PageColor.B) / 765f);
        Color shadowColor = Color.FromArgb(
            ( int ) (backColor.R * colorDarkeningIndex),
            ( int ) (backColor.G * colorDarkeningIndex),
            ( int ) (backColor.B * colorDarkeningIndex) );

        colorDarkeningIndex += 0.2f;
        Color shadowLightColor = Color.FromArgb(
            ( int ) (borderSkin.PageColor.R * colorDarkeningIndex),
            ( int ) (borderSkin.PageColor.G * colorDarkeningIndex),
            ( int ) (borderSkin.PageColor.B * colorDarkeningIndex) );
        if ( borderSkin.PageColor == Color.Transparent )
        {
            shadowLightColor = Color.FromArgb( 60, 0, 0, 0 );
        }

        // Calculate rounded rect radius
        float radius = this.defaultRadiusSize;
        radius = ( float ) Math.Max( radius, 2f * this.resolution / 96.0f );
        radius = ( float ) Math.Min( radius, rect.Width / 2f );
        radius = ( float ) Math.Min( radius, rect.Height / 2f );
        radius = ( float ) Math.Ceiling( radius );

        // Fill page background color
        using ( Brush brush = new SolidBrush( borderSkin.PageColor ) )
        {
            graph.FillRectangle( brush, rect );
        }

        if ( this.drawOutsideTopLeftShadow )
        {
            // Top/Left outside shadow
            shadowRect = absolute;
            shadowRect.X -= radius * 0.3f;
            shadowRect.Y -= radius * 0.3f;
            shadowRect.Width -= radius * .3f;
            shadowRect.Height -= radius * .3f;
            graph.DrawRoundedRectShadowAbs( shadowRect, this.cornerRadius, radius, Color.FromArgb( 128, Color.Black ), borderSkin.PageColor, this.outsideShadowRate );
        }

        // Bottom/Right outside shadow
        shadowRect = absolute;
        shadowRect.X += radius * 0.3f;
        shadowRect.Y += radius * 0.3f;
        shadowRect.Width -= radius * .3f;
        shadowRect.Height -= radius * .3f;
        graph.DrawRoundedRectShadowAbs( shadowRect, this.cornerRadius, radius, shadowLightColor, borderSkin.PageColor, this.outsideShadowRate );

        // Background
        shadowRect = absolute;
        shadowRect.Width -= radius * .3f;
        shadowRect.Height -= radius * .3f;
        GraphicsPath path = graph.CreateRoundedRectPath( shadowRect, this.cornerRadius );
        graph.DrawPathAbs(
            path,
            backColor,
            backHatchStyle,
            backImage,
            backImageWrapMode,
            backImageTransparentColor,
            backImageAlign,
            backGradientStyle,
            backSecondaryColor,
            borderColor,
            borderWidth,
            borderDashStyle,
            PenAlignment.Inset );

        // Dispose Graphic path
        path?.Dispose();

        // Draw screws imitation in the corners of the farame
        if ( this.drawScrews )
        {
            // Left/Top screw
            RectangleF screwRect = RectangleF.Empty;
            float offset = radius * 0.4f;
            screwRect.X = shadowRect.X + offset;
            screwRect.Y = shadowRect.Y + offset;
            screwRect.Width = radius * 0.55f;
            screwRect.Height = screwRect.Width;
            this.DrawScrew( graph, screwRect );

            // Right/Top screw
            screwRect.X = shadowRect.Right - offset - screwRect.Width;
            this.DrawScrew( graph, screwRect );

            // Right/Bottom screw
            screwRect.X = shadowRect.Right - offset - screwRect.Width;
            screwRect.Y = shadowRect.Bottom - offset - screwRect.Height;
            this.DrawScrew( graph, screwRect );

            // Left/Bottom screw
            screwRect.X = shadowRect.X + offset;
            screwRect.Y = shadowRect.Bottom - offset - screwRect.Height;
            this.DrawScrew( graph, screwRect );
        }

        // Bottom/Right inner shadow
        Region innerShadowRegion = null;
        if ( this.drawBottomShadow )
        {
            shadowRect = absolute;
            shadowRect.Width -= radius * .3f;
            shadowRect.Height -= radius * .3f;
            innerShadowRegion = new Region(
                graph.CreateRoundedRectPath(
                new RectangleF(
                shadowRect.X - radius,
                shadowRect.Y - radius,
                shadowRect.Width + (0.5f * radius),
                shadowRect.Height + (0.5f * radius) ),
                this.cornerRadius ) );
            innerShadowRegion.Complement( graph.CreateRoundedRectPath( shadowRect, this.cornerRadius ) );
            graph.Clip = innerShadowRegion;

            shadowRect.X -= 0.5f * radius;
            shadowRect.Width += 0.5f * radius;
            shadowRect.Y -= 0.5f * radius;
            shadowRect.Height += 0.5f * radius;

            graph.DrawRoundedRectShadowAbs(
                shadowRect,
                this.cornerRadius,
                radius,
                Color.Transparent,
                Color.FromArgb( 175, this.sunken ? Color.White : shadowColor ),
                1.0f );
            graph.Clip = new Region();
        }

        // Top/Left inner shadow					
        shadowRect = absolute;
        shadowRect.Width -= radius * .3f;
        shadowRect.Height -= radius * .3f;
        innerShadowRegion = new Region(
            graph.CreateRoundedRectPath(
            new RectangleF(
            shadowRect.X + (radius * .5f),
            shadowRect.Y + (radius * .5f),
            shadowRect.Width - (.2f * radius),
            shadowRect.Height - (.2f * radius) ),
            this.cornerRadius ) );

        RectangleF shadowWithOffset = shadowRect;
        shadowWithOffset.Width += radius;
        shadowWithOffset.Height += radius;
        innerShadowRegion.Complement( graph.CreateRoundedRectPath( shadowWithOffset, this.cornerRadius ) );

        innerShadowRegion.Intersect( graph.CreateRoundedRectPath( shadowRect, this.cornerRadius ) );
        graph.Clip = innerShadowRegion;
        graph.DrawRoundedRectShadowAbs(
            shadowWithOffset,
            this.cornerRadius,
            radius,
            Color.Transparent,
            Color.FromArgb( 175, this.sunken ? shadowColor : Color.White ),
            1.0f );
        graph.Clip = new Region();

    }

    /// <summary>
    /// Helper function, which draws a screw on the frame
    /// </summary>
    /// <param name="graph">Chart graphics to use.</param>
    /// <param name="rect">Screw position.</param>
    private void DrawScrew( ChartGraphics graph, RectangleF rect )
    {
        // Draw screw
        Pen screwPen = new( Color.FromArgb( 128, 255, 255, 255 ), 1 );
        graph.DrawEllipse( screwPen, rect.X, rect.Y, rect.Width, rect.Height );
        graph.DrawLine( screwPen, rect.X + (2 * this.resolution / 96.0f), rect.Y + rect.Height - (2 * this.resolution / 96.0f), rect.Right - (2 * this.resolution / 96.0f), rect.Y + (2 * this.resolution / 96.0f) );
        screwPen = new Pen( Color.FromArgb( 128, Color.Black ), 1 );
        graph.DrawEllipse( screwPen, rect.X + (1 * this.resolution / 96.0f), rect.Y + (1 * this.resolution / 96.0f), rect.Width, rect.Height );
        graph.DrawLine( screwPen, rect.X + (3 * this.resolution / 96.0f), rect.Y + rect.Height - (1 * this.resolution / 96.0f), rect.Right - (1 * this.resolution / 96.0f), rect.Y + (3 * this.resolution / 96.0f) );
    }

    #endregion
}

#pragma warning restore IDE1006 // Naming Styles
