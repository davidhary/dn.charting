//
//  Purpose:	3D borders related classes:
//				  BorderTypeRegistry	- known borders registry.
//				  IBorderType			- border class interface.
//				  BorderSkin	        - border visual properties.
//

#pragma warning disable IDE1006 // Naming Styles

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " border style enumeration "
/// <summary>
/// Styles of the border skin.
/// </summary>
public enum BorderSkinStyle
{
    /// <summary>
    /// Border not used.
    /// </summary>
    None,
    /// <summary>
    /// Emboss border.
    /// </summary>
    Emboss,
    /// <summary>
    /// Raised border.
    /// </summary>
    Raised,
    /// <summary>
    /// Sunken border.
    /// </summary>
    Sunken,
    /// <summary>
    /// Thin border with rounded corners.
    /// </summary>
    FrameThin1,
    /// <summary>
    /// Thin border with rounded top corners.
    /// </summary>
    FrameThin2,
    /// <summary>
    /// Thin border with square corners.
    /// </summary>
    FrameThin3,
    /// <summary>
    /// Thin border with square outside corners and rounded inside corners.
    /// </summary>
    FrameThin4,
    /// <summary>
    /// Thin border with rounded corners and screws.
    /// </summary>
    FrameThin5,
    /// <summary>
    /// Thin border with square inside corners and rounded outside corners.
    /// </summary>
    FrameThin6,
    /// <summary>
    /// Border with rounded corners. Supports title text.
    /// </summary>
    FrameTitle1,
    /// <summary>
    /// Border with rounded top corners. Supports title text.
    /// </summary>
    FrameTitle2,
    /// <summary>
    /// Border with square corners. Supports title text.
    /// </summary>
    FrameTitle3,
    /// <summary>
    /// Border with rounded inside corners and square outside corners. Supports title text.
    /// </summary>
    FrameTitle4,
    /// <summary>
    /// Border with rounded corners and screws. Supports title text.
    /// </summary>
    FrameTitle5,
    /// <summary>
    /// Border with rounded outside corners and square inside corners. Supports title text.
    /// </summary>
    FrameTitle6,
    /// <summary>
    /// Border with rounded corners. No border on the right side. Supports title text.
    /// </summary>
    FrameTitle7,
    /// <summary>
    /// Border with rounded corners on top and bottom sides only. Supports title text.
    /// </summary>
    FrameTitle8
}

#endregion
/// <summary>
/// Drawing properties of the 3D border skin.
/// </summary>
[
    DefaultProperty( "SkinStyle" ),
    SRDescription( "DescriptionAttributeBorderSkin_BorderSkin" ),
]
public class BorderSkin : ChartElement
{
    #region " fields "

    // Private data members, which store properties values
    private Color _pageColor = Color.White;
    private BorderSkinStyle _skinStyle = BorderSkinStyle.None;
    private GradientStyle _backGradientStyle = GradientStyle.None;
    private Color _backSecondaryColor = Color.Empty;
    private Color _backColor = Color.Gray;
    private string _backImage = "";
    private ChartImageWrapMode _backImageWrapMode = ChartImageWrapMode.Tile;
    private Color _backImageTransparentColor = Color.Empty;
    private ChartImageAlignmentStyle _backImageAlignment = ChartImageAlignmentStyle.TopLeft;
    private Color _borderColor = Color.Black;
    private int _borderWidth = 1;
    private ChartDashStyle _borderDashStyle = ChartDashStyle.NotSet;
    private ChartHatchStyle _backHatchStyle = ChartHatchStyle.None;

    #endregion

    #region " constructtion "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public BorderSkin() : base()
    {
    }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="parent">The parent chart element.</param>
    internal BorderSkin( IChartElement parent ) : base( parent )
    {
    }

    #endregion

    #region " border skin properties "

    /// <summary>
    /// Gets or sets the page color of a border skin.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBorderSkin_PageColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color PageColor
    {
        get => this._pageColor;
        set
        {
            this._pageColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the style of a border skin.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( BorderSkinStyle.None ),
    SRDescription( "DescriptionAttributeBorderSkin_SkinStyle" ),
    ParenthesizePropertyName( true )
    ]
    public BorderSkinStyle SkinStyle
    {
        get => this._skinStyle;
        set
        {
            this._skinStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background color of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( typeof( Color ), "Gray" ),
    SRDescription( "DescriptionAttributeFrameBackColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BackColor
    {
        get => this._backColor;
        set
        {
            this._backColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the border color of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BorderColor
    {
        get => this._borderColor;
        set
        {
            this._borderColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background hatch style of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( ChartHatchStyle.None ),
    SRDescription( "DescriptionAttributeFrameBackHatchStyle" ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
    ]
    public ChartHatchStyle BackHatchStyle
    {
        get => this._backHatchStyle;
        set
        {
            this._backHatchStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background image of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeBackImage" ),
    Editor( typeof( ImageValueEditor ), typeof( UITypeEditor ) ),
    ]
    public string BackImage
    {
        get => this._backImage;
        set
        {
            this._backImage = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the drawing mode for the background image of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( ChartImageWrapMode.Tile ),
    SRDescription( "DescriptionAttributeImageWrapMode" ),
    ]
    public ChartImageWrapMode BackImageWrapMode
    {
        get => this._backImageWrapMode;
        set
        {
            this._backImageWrapMode = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a color which will be replaced with a transparent color 
    /// while drawing the background image of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeImageTransparentColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BackImageTransparentColor
    {
        get => this._backImageTransparentColor;
        set
        {
            this._backImageTransparentColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background image alignment of a skin frame.
    /// </summary>
    /// <remarks>
    /// Used by ClampUnscale drawing mode.
    /// </remarks>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( ChartImageAlignmentStyle.TopLeft ),
    SRDescription( "DescriptionAttributeBackImageAlign" ),
    ]
    public ChartImageAlignmentStyle BackImageAlignment
    {
        get => this._backImageAlignment;
        set
        {
            this._backImageAlignment = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background gradient style of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( GradientStyle.None ),
    SRDescription( "DescriptionAttributeBackGradientStyle" ),
    Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
    ]
    public GradientStyle BackGradientStyle
    {
        get => this._backGradientStyle;
        set
        {
            this._backGradientStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the secondary background color of a skin frame.
    /// </summary>
    /// <remarks>
    /// This color is used with <see cref="BackColor"/> when <see cref="BackHatchStyle"/> or
    /// <see cref="BackGradientStyle"/> are used.
    /// </remarks>
    [

SRCategory( "CategoryAttributeAppearance" ),
Bindable( true ),
NotifyParentProperty( true ),
DefaultValue( typeof( Color ), "" ),
SRDescription( "DescriptionAttributeBorderSkin_FrameBackSecondaryColor" ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public Color BackSecondaryColor
    {
        get => this._backSecondaryColor;
        set
        {
            this._backSecondaryColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the width of the border line of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeBorderSkin_FrameBorderWidth" ),
    ]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionBorderWidthIsNotPositive );
            }
            this._borderWidth = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the style of the border line of a skin frame.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    NotifyParentProperty( true ),
    DefaultValue( ChartDashStyle.NotSet ),
    SRDescription( "DescriptionAttributeBorderSkin_FrameBorderDashStyle" ),
    ]
    public ChartDashStyle BorderDashStyle
    {
        get => this._borderDashStyle;
        set
        {
            this._borderDashStyle = value;
            this.Invalidate();
        }
    }

    #endregion
}

