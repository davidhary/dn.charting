//  Purpose:	3D borders related classes:
//				  BorderTypeRegistry	- known borders registry.
//				  IBorderType			- border class interface.
//				  BorderSkin	        - border visual properties.

using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Resources;

namespace System.Windows.Forms.DataVisualization.Charting.Borders3D;
/// <summary>
/// Keep track of all registered 3D borders.
/// </summary>
internal class BorderTypeRegistry : IServiceProvider
{
    #region " fields "

    // Border types image resource manager
    private ResourceManager _resourceManager;

    // Storage for all registered border types
    internal Hashtable RegisteredBorderTypes { get; set; } = new( StringComparer.OrdinalIgnoreCase );

    private readonly Hashtable _createdBorderTypes = new( StringComparer.OrdinalIgnoreCase );

    #endregion

    #region " constructors and services "

    /// <summary>
    /// Border types registry public constructor
    /// </summary>
    public BorderTypeRegistry()
    {
    }

    /// <summary>
    /// Returns border type registry service object
    /// </summary>
    /// <param name="serviceType">Service type to get.</param>
    /// <returns>Border registry service.</returns>
    [EditorBrowsable( EditorBrowsableState.Never )]
    object IServiceProvider.GetService( Type serviceType )
    {
        return serviceType == typeof( BorderTypeRegistry )
            ? this
            : throw (new ArgumentException( SR.ExceptionBorderTypeRegistryUnsupportedType( serviceType.ToString() ) ));
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Adds 3D border type into the registry.
    /// </summary>
    /// <param name="name">Border type name.</param>
    /// <param name="borderType">Border class type.</param>
    public void Register( string name, Type borderType )
    {
        // First check if border type with specified name already registered
        if ( this.RegisteredBorderTypes.Contains( name ) )
        {
            // If same type provided - ignore
            if ( this.RegisteredBorderTypes[name].GetType() == borderType )
            {
                return;
            }

            // Error - throw exception
            throw new ArgumentException( SR.ExceptionBorderTypeNameIsNotUnique( name ) );
        }

        // Make sure that specified class support IBorderType interface
        bool found = false;
        Type[] interfaces = borderType.GetInterfaces();
        foreach ( Type type in interfaces )
        {
            if ( type == typeof( IBorderType ) )
            {
                found = true;
                break;
            }
        }
        if ( !found )
        {
            throw new ArgumentException( SR.ExceptionBorderTypeHasNoInterface );
        }

        // Add border type to the hash table
        this.RegisteredBorderTypes[name] = borderType;
    }

    /// <summary>
    /// Returns border type object by name.
    /// </summary>
    /// <param name="name">Border type name.</param>
    /// <returns>Border type object derived from IBorderType.</returns>
    public IBorderType GetBorderType( string name )
    {
        // First check if border type with specified name registered
        if ( !this.RegisteredBorderTypes.Contains( name ) )
        {
            throw new ArgumentException( SR.ExceptionBorderTypeUnknown( name ) );
        }

        // Check if the border type object is already created
        if ( !this._createdBorderTypes.Contains( name ) )
        {
            // Create border type object
            this._createdBorderTypes[name] =
                (( Type ) this.RegisteredBorderTypes[name]).Assembly.
                CreateInstance( (( Type ) this.RegisteredBorderTypes[name]).ToString() );
        }

        return ( IBorderType ) this._createdBorderTypes[name];
    }

    /// <summary>
    /// Border images resource manager.
    /// </summary>
    public ResourceManager ResourceManager
    {
        get
        {
            // Create border images resource manager
            this._resourceManager ??= new ResourceManager( "System.Web.UI.DataVisualization.Charting", Assembly.GetExecutingAssembly() );
            return this._resourceManager;
        }
    }

    #endregion
}
/// <summary>
/// Interface which defines the set of standard methods and
/// properties for each border type.
/// </summary>
internal interface IBorderType
{
    #region " properties and method "

    #endregion

    #region " properties and method "

    /// <summary>
    /// Border type name.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Sets/Gets the resolution to draw with;
    /// </summary>
    float Resolution
    {
        set;
    }
    /// <summary>
    /// Draws 3D border.
    /// </summary>
    /// <param name="graph">Graphics to draw the border on.</param>
    /// <param name="borderSkin">Border skin object.</param>
    /// <param name="rect">Rectangle of the border.</param>
    /// <param name="backColor">Color of rectangle.</param>
    /// <param name="backHatchStyle">Hatch style.</param>
    /// <param name="backImage">Back Image.</param>
    /// <param name="backImageWrapMode">Image mode.</param>
    /// <param name="backImageTransparentColor">Image transparent color.</param>
    /// <param name="backImageAlign">Image alignment.</param>
    /// <param name="backGradientStyle">Gradient type.</param>
    /// <param name="backSecondaryColor">Gradient End Color.</param>
    /// <param name="borderColor">Border Color.</param>
    /// <param name="borderWidth">Border Width.</param>
    /// <param name="borderDashStyle">Border Style.</param>
    void DrawBorder(
    ChartGraphics graph,
    BorderSkin borderSkin,
    RectangleF rect,
    Color backColor,
    ChartHatchStyle backHatchStyle,
    string backImage,
    ChartImageWrapMode backImageWrapMode,
    Color backImageTransparentColor,
    ChartImageAlignmentStyle backImageAlign,
    GradientStyle backGradientStyle,
    Color backSecondaryColor,
    Color borderColor,
    int borderWidth,
    ChartDashStyle borderDashStyle );

    /// <summary>
    /// Adjust areas rectangle coordinate to fit the 3D border.
    /// </summary>
    /// <param name="graph">Graphics to draw the border on.</param>
    /// <param name="areasRect">Position to adjust.</param>
    void AdjustAreasPosition( ChartGraphics graph, ref RectangleF areasRect );

    /// <summary>
    /// Returns the position of the rectangular area in the border where
    /// title should be displayed. Returns empty rect if title can't be shown in the border.
    /// </summary>
    /// <returns>Title position in border.</returns>
    RectangleF GetTitlePositionInBorder();

    #endregion
}

