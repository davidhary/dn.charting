using System.Drawing;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// <b>AnnotationCollection</b> is a collection that stores chart annotation objects.
/// <seealso cref="Chart.Annotations"/>
/// </summary>
/// <remarks>
/// All chart annotations are stored in this collection.  It is exposed as 
/// a <see cref="Chart.Annotations"/> property of the chart. It is also used to 
/// store annotations inside the <see cref="AnnotationGroup"/> class.
/// <para>
/// This class includes methods for adding, inserting, iterating and removing annotations.
/// </para>
/// </remarks>
[
    SRDescription( "DescriptionAttributeAnnotations3" ),
]
public class AnnotationCollection : ChartNamedElementCollection<Annotation>
{
    #region " fields "

    /// <summary>
    /// Group this collection belongs too
    /// </summary>
    internal AnnotationGroup AnnotationGroup { get; set; }

    /// <summary>   Annotation object that was last clicked on. </summary>
    /// <value> The last clicked annotation. </value>
    internal Annotation LastClickedAnnotation { get; set; }

    /// <summary>   Start point of annotation moving or resizing. </summary>
    private PointF _movingResizingStartPoint = PointF.Empty;

    /// <summary>   Current resizing mode. </summary>
    private ResizingMode _resizingMode = ResizingMode.None;

    /// <summary>   Annotation object which is currently placed on the chart. </summary>
    /// <value> The placing annotation. </value>
    internal Annotation PlacingAnnotation { get; set; }

    #endregion

    #region " construction and initialization "

    /// <summary>
    /// Initializes a new instance of the <see cref="AnnotationCollection"/> class.
    /// </summary>
    /// <param name="parent">The parent chart element.</param>
    internal AnnotationCollection( IChartElement parent ) : base( parent )
    {
    }

    #endregion

    #region " items inserting and removing notification methods "

    /// <summary>
    /// Initializes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    internal override void Initialize( Annotation item )
    {
        if ( item is not null )
        {
            if ( item is TextAnnotation textAnnotation && string.IsNullOrEmpty( textAnnotation.Text ) && this.Chart != null && this.Chart.IsDesignMode() )
            {
                textAnnotation.Text = item.Name;
            }

            //If the collection belongs to annotation group we need to pass a ref to this group to all the child annotations
            if ( this.AnnotationGroup is not null )
            {
                item.AnnotationGroup = this.AnnotationGroup;
            }

            item.ResetCurrentRelativePosition();
        }
        base.Initialize( item );
    }

    /// <summary>
    /// De-Initializes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    internal override void Deinitialize( Annotation item )
    {
        if ( item is not null )
        {
            item.AnnotationGroup = null;
            item.ResetCurrentRelativePosition();
        }
        base.Deinitialize( item );
    }

    /// <summary>
    /// Finds an annotation in the collection by name.
    /// </summary>
    /// <param name="name">
    /// Name of the annotation to find.
    /// </param>
    /// <returns>
    /// <see cref="Annotation"/> object, or null (or nothing) if it does not exist.
    /// </returns>
    public override Annotation FindByName( string name )
    {
        foreach ( Annotation annotation in this )
        {
            // Compare annotation name 
            if ( annotation.Name == name )
            {
                return annotation;
            }

            // Check if annotation is a group
            if ( annotation is AnnotationGroup annotationGroup )
            {
                Annotation result = annotationGroup.Annotations.FindByName( name );
                if ( result is not null )
                {
                    return result;
                }
            }
        }

        return null;
    }

    #endregion

    #region " painting "

    /// <summary>
    /// Paints all annotation objects in the collection.
    /// </summary>
    /// <param name="chartGraph">Chart graphics used for painting.</param>
    /// <param name="drawAnnotationOnly">Indicates that only annotation objects are redrawn.</param>
    internal void Paint( ChartGraphics chartGraph, bool drawAnnotationOnly )
    {
        ChartPicture chartPicture = this.Chart.chartPicture;

        // Restore previous background using double buffered bitmap
        if ( !chartPicture.isSelectionMode &&
            this.Count > 0 /* &&
				!this.Chart.chartPicture.isPrinting */)
        {
            chartPicture.backgroundRestored = true;
            Rectangle chartPosition = new( 0, 0, chartPicture.Width, chartPicture.Height );
            if ( chartPicture.nonTopLevelChartBuffer == null || !drawAnnotationOnly )
            {
                // Dispose previous bitmap
                if ( chartPicture.nonTopLevelChartBuffer is not null )
                {
                    chartPicture.nonTopLevelChartBuffer.Dispose();
                    chartPicture.nonTopLevelChartBuffer = null;
                }

                // Copy chart area plotting rectangle from the chart's double buffer image into area double buffer image
                if ( this.Chart.paintBufferBitmap != null &&
                    this.Chart.paintBufferBitmap.Size.Width >= chartPosition.Size.Width &&
                    this.Chart.paintBufferBitmap.Size.Height >= chartPosition.Size.Height )
                {
                    chartPicture.nonTopLevelChartBuffer = this.Chart.paintBufferBitmap.Clone(
                        chartPosition, this.Chart.paintBufferBitmap.PixelFormat );
                }
            }
            else if ( drawAnnotationOnly && chartPicture.nonTopLevelChartBuffer is not null )
            {
                // Restore previous background
                this.Chart.paintBufferBitmapGraphics.DrawImageUnscaled(
                    chartPicture.nonTopLevelChartBuffer,
                    chartPosition );
            }
        }

        // Draw all annotation objects
        foreach ( Annotation annotation in this )
        {
            // Reset calculated relative position
            annotation.ResetCurrentRelativePosition();

            if ( annotation.IsVisible() )
            {
                bool resetClip = false;

                // Check if anchor point associated with plot area is inside the scaleView
                if ( annotation.IsAnchorVisible() )
                {
                    // Set annotation object clipping
                    if ( annotation.ClipToChartArea.Length > 0 &&
                        annotation.ClipToChartArea != Constants.NotSetValue &&
                        this.Chart is not null )
                    {
                        int areaIndex = this.Chart.ChartAreas.IndexOf( annotation.ClipToChartArea );
                        if ( areaIndex >= 0 )
                        {
                            // Get chart area object
                            ChartArea chartArea = this.Chart.ChartAreas[areaIndex];
                            chartGraph.SetClip( chartArea.PlotAreaPosition.ToRectangleF() );
                            resetClip = true;
                        }
                    }

                    // Start Svg Selection mode
                    string url = string.Empty;
                    chartGraph.StartHotRegion(
                        annotation.ReplaceKeywords( url ),
                        annotation.ReplaceKeywords( annotation.ToolTip ) );

                    // Draw annotation object
                    annotation.Paint( this.Chart, chartGraph );


                    // End Svg Selection mode
                    chartGraph.EndHotRegion();

                    // Reset clipping region
                    if ( resetClip )
                    {
                        chartGraph.ResetClip();
                    }
                }
            }
        }
    }

    #endregion

    #region " mouse events handlers "

    /// <summary>
    /// Mouse was double clicked.
    /// </summary>
    internal void OnDoubleClick()
    {
        if ( this.LastClickedAnnotation != null &&
            this.LastClickedAnnotation.AllowTextEditing )
        {
            TextAnnotation textAnnotation = this.LastClickedAnnotation as TextAnnotation;

            if ( textAnnotation == null )
            {
                if ( this.LastClickedAnnotation is AnnotationGroup group )
                {
                    // Try to edit text annotation in the group
                    foreach ( Annotation annotation in group.Annotations )
                    {
                        if ( annotation is TextAnnotation groupAnnotation &&
                            groupAnnotation.AllowTextEditing )
                        {
                            // Get annotation position in relative coordinates
                            PointF firstPoint = PointF.Empty;
                            PointF anchorPoint = PointF.Empty;
                            SizeF size = SizeF.Empty;
                            groupAnnotation.GetRelativePosition( out firstPoint, out size, out anchorPoint );
                            RectangleF textPosition = new( firstPoint, size );

                            // Check if last clicked coordinate is inside this text annotation
                            if ( groupAnnotation.GetGraphics() != null &&
                                textPosition.Contains( groupAnnotation.GetGraphics().GetRelativePoint( this._movingResizingStartPoint ) ) )
                            {
                                textAnnotation = groupAnnotation;
                                this.LastClickedAnnotation = textAnnotation;
                                break;
                            }
                        }
                    }
                }
            }

            // Start annotation text editing
            textAnnotation?.BeginTextEditing();
        }
    }

    /// <summary>
    /// Checks if specified point is contained by any of the selection handles.
    /// </summary>
    /// <param name="point">Point which is tested in pixel coordinates.</param>
    /// <param name="resizingMode">Handle containing the point or None.</param>
    /// <returns>Annotation that contains the point or Null.</returns>
    internal Annotation HitTestSelectionHandles( PointF point, ref ResizingMode resizingMode )
    {
        Annotation annotationResult = null;

        if ( this.Common != null &&
            this.Common.graph is not null )
        {
            PointF pointRel = this.Common.graph.GetRelativePoint( point );
            foreach ( Annotation annotation in this )
            {
                // Reset selected path point
                annotation.CurrentPathPointIndex = -1;

                // Check if annotation is selected
                if ( annotation.IsSelected )
                {
                    if ( annotation.SelectionRectangles() is not null )
                    {
                        for ( int index = 0; index < annotation.SelectionRectangles().Length; index++ )
                        {
                            if ( !annotation.SelectionRectangles()[index].IsEmpty &&
                                annotation.SelectionRectangles()[index].Contains( pointRel ) )
                            {
                                annotationResult = annotation;
                                if ( index > ( int ) ResizingMode.AnchorHandle )
                                {
                                    resizingMode = ResizingMode.MovingPathPoints;
                                    annotation.CurrentPathPointIndex = index - 9;
                                }
                                else
                                {
                                    resizingMode = ( ResizingMode ) index;
                                }
                            }
                        }
                    }
                }
            }
        }
        return annotationResult;
    }

    /// <summary>
    /// Mouse button pressed in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    /// <param name="isHandled">Returns true if event is handled and no further processing required.</param>
    internal void OnMouseDown( MouseEventArgs e, ref bool isHandled )
    {
        // Reset last clicked annotation object and stop text editing
        if ( this.LastClickedAnnotation is not null )
        {
            if ( this.LastClickedAnnotation is TextAnnotation textAnnotation )
            {
                // Stop annotation text editing
                textAnnotation.StopTextEditing();
            }
            this.LastClickedAnnotation = null;
        }

        // Check if in annotation placement mode
        if ( this.PlacingAnnotation is not null )
        {
            // Process mouse down
            this.PlacingAnnotation.PlacementMouseDown( new PointF( e.X, e.Y ), e.Button );

            // Set handled flag
            isHandled = true;
            return;
        }

        // Process only left mouse buttons
        if ( e.Button == MouseButtons.Left )
        {
            bool updateRequired = false;
            this._resizingMode = ResizingMode.None;

            // Check if mouse button was pressed in any selection handles areas
            Annotation annotation = this.HitTestSelectionHandles( new PointF( e.X, e.Y ), ref this._resizingMode );

            // Check if mouse button was pressed over one of the annotation objects
            if ( annotation == null && this.Count > 0 )
            {
                HitTestResult result = this.Chart.HitTest( e.X, e.Y, ChartElementType.Annotation );
                if ( result != null && result.ChartElementType == ChartElementType.Annotation )
                {
                    annotation = ( Annotation ) result.Object;
                }
            }

            // Unselect all annotations if mouse clicked outside any annotations
            if ( annotation == null || !annotation.IsSelected )
            {
                if ( (Control.ModifierKeys & Keys.Control) != Keys.Control &&
                    (Control.ModifierKeys & Keys.Shift) != Keys.Shift )
                {
                    foreach ( Annotation chartAnnotation in this.Chart.Annotations )
                    {
                        if ( chartAnnotation != annotation && chartAnnotation.IsSelected )
                        {
                            chartAnnotation.IsSelected = false;
                            updateRequired = true;

                            // Call selection changed notification
                            this.Chart?.OnAnnotationSelectionChanged( chartAnnotation );
                        }
                    }
                }
            }

            // Process mouse action in the annotation object
            if ( annotation is not null )
            {
                // Mouse down event handled
                isHandled = true;

                // Select/Unselect annotation 
                Annotation selectableAnnotation = annotation;
                if ( annotation.AnnotationGroup is not null )
                {
                    // Select annotation group when click on any child annotations
                    selectableAnnotation = annotation.AnnotationGroup;
                }
                if ( !selectableAnnotation.IsSelected && selectableAnnotation.AllowSelecting )
                {
                    selectableAnnotation.IsSelected = true;
                    updateRequired = true;

                    // Call selection changed notification
                    this.Chart?.OnAnnotationSelectionChanged( selectableAnnotation );
                }
                else if ( (Control.ModifierKeys & Keys.Control) == Keys.Control ||
                    (Control.ModifierKeys & Keys.Shift) == Keys.Shift )
                {
                    selectableAnnotation.IsSelected = false;
                    updateRequired = true;

                    // Call selection changed notification
                    this.Chart?.OnAnnotationSelectionChanged( selectableAnnotation );
                }

                // Remember last clicked and selected annotation
                this.LastClickedAnnotation = annotation;

                // Remember mouse position
                this._movingResizingStartPoint = new PointF( e.X, e.Y );

                // Start moving, repositioning or resizing of annotation
                if ( annotation.IsSelected )
                {
                    // Check if one of selection handles was clicked on
                    this._resizingMode = annotation.GetSelectionHandle( this._movingResizingStartPoint );
                    if ( !annotation.AllowResizing &&
                        this._resizingMode >= ResizingMode.TopLeftHandle &&
                        this._resizingMode <= ResizingMode.LeftHandle )
                    {
                        this._resizingMode = ResizingMode.None;
                    }
                    if ( !annotation.AllowAnchorMoving &&
                        this._resizingMode == ResizingMode.AnchorHandle )
                    {
                        this._resizingMode = ResizingMode.None;
                    }
                    if ( this._resizingMode == ResizingMode.None && annotation.AllowMoving )
                    {
                        // Annotation moving mode
                        this._resizingMode = ResizingMode.Moving;
                    }
                }
                else
                {
                    if ( this._resizingMode == ResizingMode.None && annotation.AllowMoving )
                    {
                        // Do not allow moving child annotations inside the group. 
                        // Only the whole group can be selected, resized or repositioned.
                        if ( annotation.AnnotationGroup is not null )
                        {
                            // Move the group instead
                            this.LastClickedAnnotation = annotation.AnnotationGroup;
                        }

                        // Annotation moving mode
                        this._resizingMode = ResizingMode.Moving;
                    }
                }
            }

            // Update chart
            if ( updateRequired )
            {
                // Invalidate and update the chart
                this.Chart.Invalidate( true );
                this.Chart.UpdateAnnotations();
            }
        }
    }

    /// <summary>
    /// Mouse button released in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void OnMouseUp( MouseEventArgs e )
    {
        // Check if in annotation placement mode
        if ( this.PlacingAnnotation is not null )
        {
            if ( !this.PlacingAnnotation.PlacementMouseUp( new PointF( e.X, e.Y ), e.Button ) )
            {
                return;
            }
        }

        if ( e.Button == MouseButtons.Left )
        {
            // Reset moving sizing start point 
            this._movingResizingStartPoint = PointF.Empty;
            this._resizingMode = ResizingMode.None;
        }

        // Loop through all annotation objects
        for ( int index = 0; index < this.Count; index++ )
        {
            Annotation annotation = this[index];

            // NOTE: Automatic deleting feature was disabled. -AG.
            /*
				// Delete all annotation objects moved outside clipping region
				if( annotation.outsideClipRegion )
				{
					this.List.RemoveAt(index);
					--index;
				}
			*/

            // Reset start position/location fields
            annotation.StartMovePositionRel = RectangleF.Empty;
            annotation.StartMoveAnchorLocationRel = PointF.Empty;
            if ( annotation.StartMovePathRel is not null )
            {
                annotation.StartMovePathRel.Dispose();
                annotation.StartMovePathRel = null;
            }

            // Fire position changed event
            if ( annotation.PositionChanged )
            {
                annotation.PositionChanged = false;
                this.Chart?.OnAnnotationPositionChanged( annotation );
            }
        }
    }

    /// <summary>
    /// Mouse moved in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void OnMouseMove( MouseEventArgs e )
    {
        // Check if in annotation placement mode
        if ( this.PlacingAnnotation is not null )
        {
            Forms.Cursor newCursor = this.Chart.Cursor;
            newCursor = this.PlacingAnnotation.IsValidPlacementPosition( e.X, e.Y ) ? Cursors.Cross : this.Chart.defaultCursor;

            // Set current chart cursor
            if ( newCursor != this.Chart.Cursor )
            {
                Forms.Cursor tmpCursor = this.Chart.defaultCursor;
                this.Chart.Cursor = newCursor;
                this.Chart.defaultCursor = tmpCursor;
            }

            this.PlacingAnnotation.PlacementMouseMove( new PointF( e.X, e.Y ) );

            return;
        }

        // Check if currently resizing/moving annotation
        if ( !this._movingResizingStartPoint.IsEmpty &&
            this._resizingMode != ResizingMode.None )
        {
            // Calculate how far the mouse was moved
            SizeF moveDistance = new(
                this._movingResizingStartPoint.X - e.X,
                this._movingResizingStartPoint.Y - e.Y );

            // Update location of all selected annotation objects
            foreach ( Annotation annotationItem in this )
            {
                if ( annotationItem.IsSelected &&
                    ((this._resizingMode == ResizingMode.MovingPathPoints && annotationItem.AllowPathEditing) ||
                    (this._resizingMode == ResizingMode.Moving && annotationItem.AllowMoving) ||
                    (this._resizingMode == ResizingMode.AnchorHandle && annotationItem.AllowAnchorMoving) ||
                    (this._resizingMode >= ResizingMode.TopLeftHandle && this._resizingMode <= ResizingMode.LeftHandle && annotationItem.AllowResizing)) )
                {
                    annotationItem.AdjustLocationSize( moveDistance, this._resizingMode, true, true );
                }
            }

            // Move last clicked non-selected annotation
            if ( this.LastClickedAnnotation != null &&
                !this.LastClickedAnnotation.IsSelected )
            {
                if ( this._resizingMode == ResizingMode.Moving &&
                    this.LastClickedAnnotation.AllowMoving )
                {
                    this.LastClickedAnnotation.AdjustLocationSize( moveDistance, this._resizingMode, true, true );
                }
            }

            // Invalidate and update the chart
            this.Chart.Invalidate( true );
            this.Chart.UpdateAnnotations();
        }
        else if ( this.Count > 0 )
        {
            // Check if currently placing annotation from the UserInterface
            bool process = true;

            if ( process )
            {
                // Check if mouse pointer is over the annotation selection handle
                ResizingMode currentResizingMode = ResizingMode.None;
                Annotation annotation =
                    this.HitTestSelectionHandles( new PointF( e.X, e.Y ), ref currentResizingMode );

                // Check if mouse pointer over the annotation object movable area
                if ( annotation == null )
                {
                    HitTestResult result = this.Chart.HitTest( e.X, e.Y, ChartElementType.Annotation );
                    if ( result != null && result.ChartElementType == ChartElementType.Annotation )
                    {
                        annotation = ( Annotation ) result.Object;
                        if ( annotation is not null )
                        {
                            // Check if annotation is in the collection
                            if ( this.Contains( annotation ) )
                            {
                                currentResizingMode = ResizingMode.Moving;
                                if ( !annotation.AllowMoving )
                                {
                                    // Movement is not allowed
                                    annotation = null;
                                    currentResizingMode = ResizingMode.None;
                                }
                            }
                        }
                    }
                }
                // Set mouse cursor			
                this.SetResizingCursor( annotation, currentResizingMode );
            }
        }
    }

    /// <summary>
    /// Sets mouse cursor shape.
    /// </summary>
    /// <param name="annotation">Annotation object.</param>
    /// <param name="currentResizingMode">Resizing mode.</param>
    private void SetResizingCursor( Annotation annotation, ResizingMode currentResizingMode )
    {
        // Change current cursor
        if ( this.Chart is not null )
        {
            Forms.Cursor newCursor = this.Chart.Cursor;
            if ( annotation is not null )
            {
                if ( currentResizingMode == ResizingMode.MovingPathPoints &&
                    annotation.AllowPathEditing )
                {
                    newCursor = Cursors.Cross;
                }

                if ( currentResizingMode == ResizingMode.Moving &&
                    annotation.AllowMoving )
                {
                    newCursor = Cursors.SizeAll;
                }

                if ( currentResizingMode == ResizingMode.AnchorHandle &&
                    annotation.AllowAnchorMoving )
                {
                    newCursor = Cursors.Cross;
                }

                if ( currentResizingMode != ResizingMode.Moving &&
                    annotation.AllowResizing )
                {
                    if ( annotation.SelectionPointsStyle == SelectionPointsStyle.TwoPoints )
                    {
                        if ( currentResizingMode is ResizingMode.TopLeftHandle or
                            ResizingMode.BottomRightHandle )
                        {
                            newCursor = Cursors.Cross;
                        }
                    }
                    else
                    {
                        if ( currentResizingMode is ResizingMode.TopLeftHandle or
                            ResizingMode.BottomRightHandle )
                        {
                            newCursor = Cursors.SizeNWSE;
                        }
                        else if ( currentResizingMode is ResizingMode.TopRightHandle or
                            ResizingMode.BottomLeftHandle )
                        {
                            newCursor = Cursors.SizeNESW;
                        }
                        else if ( currentResizingMode is ResizingMode.TopHandle or
                            ResizingMode.BottomHandle )
                        {
                            newCursor = Cursors.SizeNS;
                        }
                        else if ( currentResizingMode is ResizingMode.LeftHandle or
                            ResizingMode.RightHandle )
                        {
                            newCursor = Cursors.SizeWE;
                        }
                    }
                }
            }
            else
            {
                newCursor = this.Chart.defaultCursor;
            }

            // Set current chart cursor
            if ( newCursor != this.Chart.Cursor )
            {
                Forms.Cursor tmpCursor = this.Chart.defaultCursor;
                this.Chart.Cursor = newCursor;
                this.Chart.defaultCursor = tmpCursor;
            }
        }
    }

    #endregion

    #region " event handlers "
    internal void ChartAreaNameReferenceChanged( object sender, NameReferenceChangedEventArgs e )
    {
        // If all the chart areas are removed and then a new one is inserted - Annotations don't get bound to it by default
        if ( e.OldElement == null )
            return;

        foreach ( Annotation annotation in this )
        {
            if ( annotation.ClipToChartArea == e.OldName )
                annotation.ClipToChartArea = e.NewName;

            if ( annotation is AnnotationGroup group )
            {
                group.Annotations.ChartAreaNameReferenceChanged( sender, e );
            }
        }
    }
    #endregion
}
