//
//  Purpose:	Line annotation class.
//


using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// <b>LineAnnotation</b> is a class that represents a line annotation.
/// </summary>
[
    SRDescription( "DescriptionAttributeLineAnnotation_LineAnnotation" ),
]
public class LineAnnotation : Annotation
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Indicates that an infinitive line should be drawn through 2 specified points.
    private bool _isInfinitive;

    // Line start/end caps
    private LineAnchorCapStyle _startCap = LineAnchorCapStyle.None;
    private LineAnchorCapStyle _endCap = LineAnchorCapStyle.None;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public LineAnnotation()
        : base() => this.AnchorAlignment = ContentAlignment.TopLeft;

    #endregion

    #region " properties "

    #region " line visual attributes "

    /// <summary>
    /// Gets or sets a flag that indicates if an infinitive line should be drawn.
    /// </summary>
    /// <value>
    /// <b>True</b> if a line should be drawn infinitively through 2 points provided, <b>false</b> otherwise.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeDrawInfinitive" ),
    ]
    public virtual bool IsInfinitive
    {
        get => this._isInfinitive;
        set
        {
            this._isInfinitive = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a cap style used at the start of an annotation line.
    /// <seealso cref="EndCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value, used for a cap style used at the start of an annotation line.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( LineAnchorCapStyle.None ),
    SRDescription( "DescriptionAttributeStartCap3" ),
    ]
    public virtual LineAnchorCapStyle StartCap
    {
        get => this._startCap;
        set
        {
            this._startCap = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a cap style used at the end of an annotation line.
    /// <seealso cref="StartCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value, used for a cap style used at the end of an annotation line.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( LineAnchorCapStyle.None ),
    SRDescription( "DescriptionAttributeStartCap3" ),
    ]
    public virtual LineAnchorCapStyle EndCap
    {
        get => this._endCap;
        set
        {
            this._endCap = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " non applicable annotation appearance attributes (set as non-browsable) "

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    /// <value>
    /// A <see cref="ContentAlignment"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( ContentAlignment ), "MiddleCenter" ),
    ]
    public override ContentAlignment Alignment
    {
        get => base.Alignment;
        set => base.Alignment = value;
    }

    /// <summary>
    /// Gets or sets an annotation's text style.
    /// <seealso cref="Font"/>
    /// 	<seealso cref="ForeColor"/>
    /// </summary>
    /// <value>
    /// A <see cref="TextStyle"/> value used to draw an annotation's text.
    /// </value>
    [Browsable( false )]
    [EditorBrowsable( EditorBrowsableState.Never )]
    public override TextStyle TextStyle
    {
        get => base.TextStyle;
        set => base.TextStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="Font"/>
    /// </summary>
    /// <value>
    /// A <see cref="Color"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeForeColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color ForeColor
    {
        get => base.ForeColor;
        set => base.ForeColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="ForeColor"/>
    /// </summary>
    /// <value>
    /// A <see cref="Font"/> object.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8pt" ),
    ]
    public override Font Font
    {
        get => base.Font;
        set => base.Font = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( typeof( Color ), "" ),
NotifyParentProperty( true ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    /// <value>
    /// A <see cref="ChartHatchStyle"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( ChartHatchStyle.None ),
    NotifyParentProperty( true ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
    ]
    public override ChartHatchStyle BackHatchStyle
    {
        get => base.BackHatchStyle;
        set => base.BackHatchStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( GradientStyle.None ),
NotifyParentProperty( true ),
Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
]
    public override GradientStyle BackGradientStyle
    {
        get => base.BackGradientStyle;
        set => base.BackGradientStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( typeof( Color ), "" ),
NotifyParentProperty( true ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public override Color BackSecondaryColor
    {
        get => base.BackSecondaryColor;
        set => base.BackSecondaryColor = value;
    }

    #endregion

    #region " position "

    /// <summary>
    /// Gets or sets a flag that specifies whether the size of an annotation is always 
    /// defined in relative chart coordinates.
    /// <seealso cref="Annotation.Width"/>
    /// <seealso cref="Annotation.Height"/>
    /// </summary>
    /// <value>
    /// <b>True</b> if an annotation's <see cref="Annotation.Width"/> and <see cref="Annotation.Height"/> are always 
    /// in chart relative coordinates, <b>false</b> otherwise.
    /// </value>
    /// <remarks>
    /// An annotation's width and height may be set in relative chart or axes coordinates. 
    /// By default, relative chart coordinates are used.
    /// <para>
    /// To use axes coordinates for size set the <b>IsSizeAlwaysRelative</b> property to 
    /// <b>false</b> and either anchor the annotation to a data point or set the 
    /// <see cref="Annotation.AxisX"/> or <see cref="Annotation.AxisY"/> properties.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributePosition" ),
    DefaultValue( true ),
    SRDescription( "DescriptionAttributeSizeAlwaysRelative3" ),
    ]
    public override bool IsSizeAlwaysRelative
    {
        get => base.IsSizeAlwaysRelative;
        set => base.IsSizeAlwaysRelative = value;
    }

    #endregion // Position

    #region " anchor "

    /// <summary>
    /// Gets or sets an annotation position's alignment to the anchor point.
    /// <seealso cref="Annotation.AnchorX"/>
    /// <seealso cref="Annotation.AnchorY"/>
    /// <seealso cref="Annotation.AnchorDataPoint"/>
    /// <seealso cref="Annotation.AnchorOffsetX"/>
    /// <seealso cref="Annotation.AnchorOffsetY"/>
    /// </summary>
    /// <value>
    /// A <see cref="ContentAlignment"/> value that represents the annotation's alignment to 
    /// the anchor point.
    /// </value>
    /// <remarks>
    /// The annotation must be anchored using either <see cref="Annotation.AnchorDataPoint"/>, or the <see cref="Annotation.AnchorX"/> 
    /// and <see cref="Annotation.AnchorY"/> properties. Its <see cref="Annotation.X"/> and <see cref="Annotation.Y"/> 
    /// properties must be set to <b>double.NaN</b>.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAnchor" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( typeof( ContentAlignment ), "TopLeft" ),
    SRDescription( "DescriptionAttributeAnchorAlignment" ),
    ]
    public override ContentAlignment AnchorAlignment
    {
        get => base.AnchorAlignment;
        set => base.AnchorAlignment = value;
    }

    #endregion // Anchoring

    #region " other "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeAnnotationType" ),
    ]
    public override string AnnotationType => "Line";

    /// <summary>
    /// Gets or sets an annotation's selection points style.
    /// </summary>
    /// <value>
    /// A <see cref="SelectionPointsStyle"/> value that represents the annotation
    /// selection style.
    /// </value>
    /// <remarks>
    /// This property is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( SelectionPointsStyle.Rectangle ),
    ParenthesizePropertyName( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeSelectionPointsStyle" ),
    ]
    internal override SelectionPointsStyle SelectionPointsStyle => SelectionPointsStyle.TwoPoints;

    #endregion

    #endregion

    #region " methods "

    /// <summary>
    /// Adjusts the two coordinates of the line.
    /// </summary>
    /// <param name="point1">First line coordinate.</param>
    /// <param name="point2">Second line coordinate.</param>
    /// <param name="selectionRect">Selection rectangle.</param>
    internal virtual void AdjustLineCoordinates( ref PointF point1, ref PointF point2, ref RectangleF selectionRect )
    {
        // Adjust line points to draw infinitive line
        if ( this.IsInfinitive )
        {
            if ( Math.Round( point1.X, 3 ) == Math.Round( point2.X, 3 ) )
            {
                point1.Y = (point1.Y < point2.Y) ? 0f : 100f;
                point2.Y = (point1.Y < point2.Y) ? 100f : 0f;
            }
            else if ( Math.Round( point1.Y, 3 ) == Math.Round( point2.Y, 3 ) )
            {
                point1.X = (point1.X < point2.X) ? 0f : 100f;
                point2.X = (point1.X < point2.X) ? 100f : 0f;
            }
            else
            {
                // Calculate intersection point of the line with two boundaries Y = 0 and Y = 100
                PointF intersectionPoint1 = PointF.Empty;
                intersectionPoint1.Y = 0f;
                intersectionPoint1.X = ((0f - point1.Y) *
                    (point2.X - point1.X) /
                    (point2.Y - point1.Y)) +
                    point1.X;
                PointF intersectionPoint2 = PointF.Empty;
                intersectionPoint2.Y = 100f;
                intersectionPoint2.X = ((100f - point1.Y) *
                    (point2.X - point1.X) /
                    (point2.Y - point1.Y)) +
                    point1.X;

                // Select point closest to the intersection
                point1 = (point1.Y < point2.Y) ? intersectionPoint1 : intersectionPoint2;
                point2 = (point1.Y < point2.Y) ? intersectionPoint2 : intersectionPoint1;
            }
        }
    }

    /// <summary>
    /// Paints an annotation object on the specified graphics.
    /// </summary>
    /// <param name="graphics">
    /// A <see cref="ChartGraphics"/> object, used to paint an annotation object.
    /// </param>
    /// <param name="chart">
    /// Reference to the <see cref="Chart"/> owner control.
    /// </param>
    internal override void Paint( Chart chart, ChartGraphics graphics )
    {
        // Get annotation position in relative coordinates
        PointF firstPoint = PointF.Empty;
        PointF anchorPoint = PointF.Empty;
        SizeF size = SizeF.Empty;
        this.GetRelativePosition( out firstPoint, out size, out anchorPoint );
        PointF secondPoint = new( firstPoint.X + size.Width, firstPoint.Y + size.Height );

        // Create selection rectangle
        RectangleF selectionRect = new( firstPoint, new SizeF( secondPoint.X - firstPoint.X, secondPoint.Y - firstPoint.Y ) );

        // Adjust coordinates
        this.AdjustLineCoordinates( ref firstPoint, ref secondPoint, ref selectionRect );

        // Check if text position is valid
        if ( float.IsNaN( firstPoint.X ) ||
            float.IsNaN( firstPoint.Y ) ||
            float.IsNaN( secondPoint.X ) ||
            float.IsNaN( secondPoint.Y ) )
        {
            return;
        }

        // Set line caps
        bool capChanged = false;
        LineCap oldStartCap = LineCap.Flat;
        LineCap oldEndCap = LineCap.Flat;
        if ( this._startCap != LineAnchorCapStyle.None ||
            this._endCap != LineAnchorCapStyle.None )
        {
            capChanged = true;
            oldStartCap = graphics.Pen.StartCap;
            oldEndCap = graphics.Pen.EndCap;

            // Apply anchor cap settings
            if ( this._startCap == LineAnchorCapStyle.Arrow )
            {
                // Adjust arrow size for small line width
                if ( this.LineWidth < 4 )
                {
                    int adjustment = 3 - this.LineWidth;
                    graphics.Pen.StartCap = LineCap.Custom;
                    graphics.Pen.CustomStartCap = new AdjustableArrowCap(
                        this.LineWidth + adjustment,
                        this.LineWidth + adjustment,
                        true );
                }
                else
                {
                    graphics.Pen.StartCap = LineCap.ArrowAnchor;
                }
            }
            else if ( this._startCap == LineAnchorCapStyle.Diamond )
            {
                graphics.Pen.StartCap = LineCap.DiamondAnchor;
            }
            else if ( this._startCap == LineAnchorCapStyle.Round )
            {
                graphics.Pen.StartCap = LineCap.RoundAnchor;
            }
            else if ( this._startCap == LineAnchorCapStyle.Square )
            {
                graphics.Pen.StartCap = LineCap.SquareAnchor;
            }
            if ( this._endCap == LineAnchorCapStyle.Arrow )
            {
                // Adjust arrow size for small line width
                if ( this.LineWidth < 4 )
                {
                    int adjustment = 3 - this.LineWidth;
                    graphics.Pen.EndCap = LineCap.Custom;
                    graphics.Pen.CustomEndCap = new AdjustableArrowCap(
                        this.LineWidth + adjustment,
                        this.LineWidth + adjustment,
                        true );
                }
                else
                {
                    graphics.Pen.EndCap = LineCap.ArrowAnchor;
                }
            }
            else if ( this._endCap == LineAnchorCapStyle.Diamond )
            {
                graphics.Pen.EndCap = LineCap.DiamondAnchor;
            }
            else if ( this._endCap == LineAnchorCapStyle.Round )
            {
                graphics.Pen.EndCap = LineCap.RoundAnchor;
            }
            else if ( this._endCap == LineAnchorCapStyle.Square )
            {
                graphics.Pen.EndCap = LineCap.SquareAnchor;
            }
        }

        if ( this.Common.ProcessModePaint )
        {
            // Draw line
            graphics.DrawLineRel(
                this.LineColor,
                this.LineWidth,
                this.LineDashStyle,
                firstPoint,
                secondPoint,
                this.ShadowColor,
                this.ShadowOffset );
        }

        if ( this.Common.ProcessModeRegions )
        {
            // Create line graphics path
            using GraphicsPath path = new();
            path.AddLine(
                graphics.GetAbsolutePoint( firstPoint ),
                graphics.GetAbsolutePoint( secondPoint ) );
            using ( Pen pen = ( Pen ) graphics.Pen.Clone() )
            {
                // Increase pen size by 2 pixels
                pen.DashStyle = DashStyle.Solid;
                pen.Width += 2;
                try
                {
                    path.Widen( pen );
                }
                catch ( OutOfMemoryException )
                {
                    // GraphicsPath.Widen incorrectly throws OutOfMemoryException
                    // catching here and reacting by not widening
                }
                catch ( ArgumentException )
                {
                }
            }

            // Add hot region
            this.Common.HotRegionsList.AddHotRegion(
                graphics,
                path,
                false,
                this.ReplaceKeywords( this.ToolTip ),
                string.Empty,
                string.Empty,
                string.Empty,
                this,
                ChartElementType.Annotation );
        }


        // Restore line caps
        if ( capChanged )
        {
            graphics.Pen.StartCap = oldStartCap;
            graphics.Pen.EndCap = oldEndCap;
        }

        // Paint selection handles
        this.PaintSelectionHandles( graphics, selectionRect, null );
    }

    #endregion
}
/// <summary>
/// <b>VerticalLineAnnotation</b> is a class that represents a vertical line annotation.
/// </summary>
[
    SRDescription( "DescriptionAttributeVerticalLineAnnotation_VerticalLineAnnotation" ),
]
public class VerticalLineAnnotation : LineAnnotation
{
    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public VerticalLineAnnotation()
        : base()
    {
    }

    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeAnnotationType" ),
    ]
    public override string AnnotationType => "VerticalLine";

    #endregion

    #region " methods "

    /// <summary>
    /// Adjusts the two coordinates of the line.
    /// </summary>
    /// <param name="point1">First line coordinate.</param>
    /// <param name="point2">Second line coordinate.</param>
    /// <param name="selectionRect">Selection rectangle.</param>
    internal override void AdjustLineCoordinates( ref PointF point1, ref PointF point2, ref RectangleF selectionRect )
    {
        // Make line vertical
        point2.X = point1.X;
        selectionRect.Width = 0f;

        // Call base class
        base.AdjustLineCoordinates( ref point1, ref point2, ref selectionRect );
    }

    #region " content size "

    /// <summary>
    /// Gets text annotation content size based on the text and font.
    /// </summary>
    /// <returns>Annotation content position.</returns>
    internal override RectangleF GetContentPosition()
    {
        return new RectangleF( float.NaN, float.NaN, 0f, float.NaN );
    }

    #endregion // Content Size

    #endregion
}
/// <summary>
/// <b>HorizontalLineAnnotation</b> is a class that represents a horizontal line annotation.
/// </summary>
[
    SRDescription( "DescriptionAttributeHorizontalLineAnnotation_HorizontalLineAnnotation" ),
]
public class HorizontalLineAnnotation : LineAnnotation
{
    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public HorizontalLineAnnotation()
        : base()
    {
    }

    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeAnnotationType" ),
    ]
    public override string AnnotationType => "HorizontalLine";

    #endregion

    #region " methods "

    /// <summary>
    /// Adjusts the two coordinates of the line.
    /// </summary>
    /// <param name="point1">First line coordinate.</param>
    /// <param name="point2">Second line coordinate.</param>
    /// <param name="selectionRect">Selection rectangle.</param>
    internal override void AdjustLineCoordinates( ref PointF point1, ref PointF point2, ref RectangleF selectionRect )
    {
        // Make line horizontal
        point2.Y = point1.Y;
        selectionRect.Height = 0f;

        // Call base class
        base.AdjustLineCoordinates( ref point1, ref point2, ref selectionRect );
    }

    #region " content size "

    /// <summary>
    /// Gets text annotation content size based on the text and font.
    /// </summary>
    /// <returns>Annotation content position.</returns>
    internal override RectangleF GetContentPosition()
    {
        return new RectangleF( float.NaN, float.NaN, float.NaN, 0f );
    }

    #endregion // Content Size

    #endregion
}
