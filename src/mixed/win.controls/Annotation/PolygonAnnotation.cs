//
//  Purpose:	Polyline and polygon annotation classes.
//


using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// <b>PolylineAnnotation</b> is a class that represents a polyline annotation.
/// </summary>
[
    SRDescription( "DescriptionAttributePolylineAnnotation_PolylineAnnotation" ),
]
public class PolylineAnnotation : Annotation
{
    #region " fields "

    /// <summary>   Indicate that filled polygon must be drawn. </summary>
    /// <value> True if this object is polygon, false if not. </value>
    protected bool IsPolygon { get; set; }

    #endregion

    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public PolylineAnnotation()
        : base()
    {
        this._pathPoints = new AnnotationPathPointCollection( this );

        this._graphicsPath = this._defaultGraphicsPath;
    }

    #endregion

    #region " properties "

    #region " polyline visual attributes "

    private LineAnchorCapStyle _startCap = LineAnchorCapStyle.None;

    /// <summary>
    /// Gets or sets a cap style used at the start of an annotation line.
    /// <seealso cref="EndCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value used for a cap style used at the start of an annotation line.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( LineAnchorCapStyle.None ),
    SRDescription( "DescriptionAttributeStartCap3" ),
    ]
    public virtual LineAnchorCapStyle StartCap
    {
        get => this._startCap;
        set
        {
            this._startCap = value;
            this.Invalidate();
        }
    }

    private LineAnchorCapStyle _endCap = LineAnchorCapStyle.None;

    /// <summary>
    /// Gets or sets a cap style used at the end of an annotation line.
    /// <seealso cref="StartCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value used for a cap style used at the end of an annotation line.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( LineAnchorCapStyle.None ),
    SRDescription( "DescriptionAttributeStartCap3" ),
    ]
    public virtual LineAnchorCapStyle EndCap
    {
        get => this._endCap;
        set
        {
            this._endCap = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " non applicable annotation appearance attributes (set as non-browsable) "

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    /// <value>
    /// A <see cref="ContentAlignment"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( ContentAlignment ), "MiddleCenter" ),
    ]
    public override ContentAlignment Alignment
    {
        get => base.Alignment;
        set => base.Alignment = value;
    }

    /// <summary>
    /// Gets or sets an annotation's text style.
    /// <seealso cref="Font"/>
    /// 	<seealso cref="ForeColor"/>
    /// </summary>
    /// <value>
    /// A <see cref="TextStyle"/> value used to draw an annotation's text.
    /// </value>
    [Browsable( false )]
    [EditorBrowsable( EditorBrowsableState.Never )]
    public override TextStyle TextStyle
    {
        get => base.TextStyle;
        set => base.TextStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="Font"/>
    /// </summary>
    /// <value>
    /// A <see cref="Color"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeForeColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color ForeColor
    {
        get => base.ForeColor;
        set => base.ForeColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="ForeColor"/>
    /// </summary>
    /// <value>
    /// A <see cref="Font"/> object.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8pt" ),
    ]
    public override Font Font
    {
        get => base.Font;
        set => base.Font = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( typeof( Color ), "" ),
NotifyParentProperty( true ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( ChartHatchStyle.None ),
NotifyParentProperty( true ),
Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
]
    public override ChartHatchStyle BackHatchStyle
    {
        get => base.BackHatchStyle;
        set => base.BackHatchStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( GradientStyle.None ),
NotifyParentProperty( true ),
Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
]
    public override GradientStyle BackGradientStyle
    {
        get => base.BackGradientStyle;
        set => base.BackGradientStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( false ),
DefaultValue( typeof( Color ), "" ),
NotifyParentProperty( true ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public override Color BackSecondaryColor
    {
        get => base.BackSecondaryColor;
        set => base.BackSecondaryColor = value;
    }

    #endregion

    #region " other "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeAnnotationType" ),
    ]
    public override string AnnotationType => "Polyline";

    /// <summary>
    /// Gets or sets an annotation selection points style.
    /// </summary>
    /// <value>
    /// A <see cref="SelectionPointsStyle"/> value that represents the annotation 
    /// selection style.
    /// </value>
    /// <remarks>
    /// This property is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( SelectionPointsStyle.Rectangle ),
    ParenthesizePropertyName( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeSelectionPointsStyle" ),
    ]
    internal override SelectionPointsStyle SelectionPointsStyle => SelectionPointsStyle.Rectangle;

    // Indicates that annotation will be placed using free-draw style
    private bool _isFreeDrawPlacement;

    /// <summary>
    /// Gets or sets a flag that determines whether an annotation should be placed using the free-draw mode.
    /// </summary>
    /// <value>
    /// <b>True</b> if an annotation should be placed using free-draw mode, 
    /// <b>false</b> otherwise.  Defaults to <b>false</b>.
    /// </value>
    /// <remarks>
    /// Two different placement modes are supported when the Annotation.BeginPlacement 
    /// method is called. Set this property to <b>true</b> to switch from the default 
    /// mode to free-draw mode, which allows the caller to free-draw while moving the mouse cursor.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeFreeDrawPlacement" ),
    ]
    public virtual bool IsFreeDrawPlacement
    {
        get => this._isFreeDrawPlacement;
        set => this._isFreeDrawPlacement = value;
    }

    // Indicates that path was changed
    private bool _pathChanged;

    // Path with polygon points.
    private GraphicsPath _defaultGraphicsPath = new();
    private GraphicsPath _graphicsPath;

    /// <summary>
    /// Gets or sets the path points of a polyline at run-time.
    /// </summary>
    /// <value>
    /// A <see cref="GraphicsPath"/> object with the polyline shape.
    /// </value>
    /// <remarks>
    /// A polyline must use coordinates relative to an annotation object, where (0,0) is 
    /// the top-left coordinates and (100,100) is the bottom-right coordinates of the annotation.  
    /// <para>
    /// This property is not accessible at design time (at design-time, use the 
    /// <see cref="GraphicsPathPoints"/> property instead).
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributePosition" ),
    DefaultValue( null ),
    SRDescription( "DescriptionAttributePath" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    ]
    public virtual GraphicsPath GraphicsPath
    {
        get => this._graphicsPath;
        set
        {
            this._graphicsPath = value;
            this._pathChanged = true;
        }
    }

    // Collection of path points exposed at design-time
    private AnnotationPathPointCollection _pathPoints;

    /// <summary>
    /// Gets or sets the path points of the polyline at design-time.
    /// </summary>
    /// <value>
    /// An <see cref="AnnotationPathPointCollection"/> object with the polyline shape.
    /// </value>
    /// <remarks>
    /// A polyline must use coordinates relative to an annotation object, where (0,0) is 
    /// the top-left coordinates and (100,100) is the bottom-right coordinates of the annotation.
    /// <para>
    /// This property is not accessible at runtime (at runtime, use the <see cref="GraphicsPath"/> 
    /// property instead).
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributePosition" ),
    SRDescription( "DescriptionAttributePathPoints" ),
    EditorBrowsable( EditorBrowsableState.Never ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public AnnotationPathPointCollection GraphicsPathPoints
    {
        get
        {
            if ( this._pathChanged ||
                this._graphicsPath.PointCount != this._pathPoints.Count )
            {
                // Recreate collection from graphics path
                this._pathPoints.Annotation = null;
                this._pathPoints.Clear();
                if ( this._graphicsPath.PointCount > 0 )
                {
                    PointF[] points = this._graphicsPath.PathPoints;
                    byte[] types = this._graphicsPath.PathTypes;
                    for ( int index = 0; index < points.Length; index++ )
                    {
                        this._pathPoints.Add( new AnnotationPathPoint( points[index].X, points[index].Y, types[index] ) );
                    }
                }
                this._pathPoints.Annotation = this;
            }
            return this._pathPoints;
        }
    }

    #endregion

    #endregion

    #region " methods "

    #region " painting "

    /// <summary>
    /// Paints an annotation object on the specified graphics.
    /// </summary>
    /// <param name="graphics">
    /// A <see cref="ChartGraphics"/> object, used to paint an annotation object.
    /// </param>
    /// <param name="chart">
    /// Reference to the <see cref="Chart"/> owner control.
    /// </param>
    internal override void Paint( Chart chart, ChartGraphics graphics )
    {
        // Check for empty path
        if ( this._graphicsPath.PointCount == 0 )
        {
            return;
        }

        // Get annotation position in relative coordinates
        PointF firstPoint = PointF.Empty;
        PointF anchorPoint = PointF.Empty;
        SizeF size = SizeF.Empty;
        this.GetRelativePosition( out firstPoint, out size, out anchorPoint );
        PointF secondPoint = new( firstPoint.X + size.Width, firstPoint.Y + size.Height );

        // Create selection rectangle
        RectangleF selectionRect = new( firstPoint, new SizeF( secondPoint.X - firstPoint.X, secondPoint.Y - firstPoint.Y ) );

        // Get position
        RectangleF rectanglePosition = new( selectionRect.Location, selectionRect.Size );
        if ( rectanglePosition.Width < 0 )
        {
            rectanglePosition.X = rectanglePosition.Right;
            rectanglePosition.Width = -rectanglePosition.Width;
        }
        if ( rectanglePosition.Height < 0 )
        {
            rectanglePosition.Y = rectanglePosition.Bottom;
            rectanglePosition.Height = -rectanglePosition.Height;
        }

        // Check if position is valid
        if ( float.IsNaN( rectanglePosition.X ) ||
            float.IsNaN( rectanglePosition.Y ) ||
            float.IsNaN( rectanglePosition.Right ) ||
            float.IsNaN( rectanglePosition.Bottom ) )
        {
            return;
        }

        // Get annotation absolute position
        RectangleF rectanglePositionAbs = graphics.GetAbsoluteRectangle( rectanglePosition );

        // Calculate scaling
        float groupScaleX = rectanglePositionAbs.Width / 100.0f;
        float groupScaleY = rectanglePositionAbs.Height / 100.0f;

        // Convert path to pixel coordinates
        PointF[] pathPoints = this._graphicsPath.PathPoints;
        byte[] pathTypes = this._graphicsPath.PathTypes;
        for ( int pointIndex = 0; pointIndex < pathPoints.Length; pointIndex++ )
        {
            pathPoints[pointIndex].X = rectanglePositionAbs.X + (pathPoints[pointIndex].X * groupScaleX);
            pathPoints[pointIndex].Y = rectanglePositionAbs.Y + (pathPoints[pointIndex].Y * groupScaleY);
        }

        using GraphicsPath pathAbs = new( pathPoints, pathTypes );

        // Set line caps
        bool capChanged = false;
        LineCap oldStartCap = LineCap.Flat;
        LineCap oldEndCap = LineCap.Flat;
        if ( !this.IsPolygon )
        {
            if ( this._startCap != LineAnchorCapStyle.None ||
                this._endCap != LineAnchorCapStyle.None )
            {
                capChanged = true;
                oldStartCap = graphics.Pen.StartCap;
                oldEndCap = graphics.Pen.EndCap;

                // Apply anchor cap settings
                if ( this._startCap == LineAnchorCapStyle.Arrow )
                {
                    // Adjust arrow size for small line width
                    if ( this.LineWidth < 4 )
                    {
                        int adjustment = 3 - this.LineWidth;
                        graphics.Pen.StartCap = LineCap.Custom;
                        graphics.Pen.CustomStartCap = new AdjustableArrowCap(
                            this.LineWidth + adjustment,
                            this.LineWidth + adjustment,
                            true );
                    }
                    else
                    {
                        graphics.Pen.StartCap = LineCap.ArrowAnchor;
                    }
                }
                else if ( this._startCap == LineAnchorCapStyle.Diamond )
                {
                    graphics.Pen.StartCap = LineCap.DiamondAnchor;
                }
                else if ( this._startCap == LineAnchorCapStyle.Round )
                {
                    graphics.Pen.StartCap = LineCap.RoundAnchor;
                }
                else if ( this._startCap == LineAnchorCapStyle.Square )
                {
                    graphics.Pen.StartCap = LineCap.SquareAnchor;
                }
                if ( this._endCap == LineAnchorCapStyle.Arrow )
                {
                    // Adjust arrow size for small line width
                    if ( this.LineWidth < 4 )
                    {
                        int adjustment = 3 - this.LineWidth;
                        graphics.Pen.EndCap = LineCap.Custom;
                        graphics.Pen.CustomEndCap = new AdjustableArrowCap(
                            this.LineWidth + adjustment,
                            this.LineWidth + adjustment,
                            true );
                    }
                    else
                    {
                        graphics.Pen.EndCap = LineCap.ArrowAnchor;
                    }
                }
                else if ( this._endCap == LineAnchorCapStyle.Diamond )
                {
                    graphics.Pen.EndCap = LineCap.DiamondAnchor;
                }
                else if ( this._endCap == LineAnchorCapStyle.Round )
                {
                    graphics.Pen.EndCap = LineCap.RoundAnchor;
                }
                else if ( this._endCap == LineAnchorCapStyle.Square )
                {
                    graphics.Pen.EndCap = LineCap.SquareAnchor;
                }
            }
        }

        // Painting mode
        if ( this.Common.ProcessModePaint )
        {
            if ( this.IsPolygon )
            {
                // Draw polygon
                pathAbs.CloseAllFigures();
                graphics.DrawPathAbs(
                    pathAbs,
                    this.BackColor,
                    this.BackHatchStyle,
                    string.Empty,
                    ChartImageWrapMode.Scaled,
                    Color.Empty,
                    ChartImageAlignmentStyle.Center,
                    this.BackGradientStyle,
                    this.BackSecondaryColor,
                    this.LineColor,
                    this.LineWidth,
                    this.LineDashStyle,
                    PenAlignment.Center,
                    this.ShadowOffset,
                    this.ShadowColor );
            }
            else
            {
                // Draw polyline
                graphics.DrawPathAbs(
                    pathAbs,
                    Color.Transparent,
                    ChartHatchStyle.None,
                    string.Empty,
                    ChartImageWrapMode.Scaled,
                    Color.Empty,
                    ChartImageAlignmentStyle.Center,
                    GradientStyle.None,
                    Color.Empty,
                    this.LineColor,
                    this.LineWidth,
                    this.LineDashStyle,
                    PenAlignment.Center,
                    this.ShadowOffset,
                    this.ShadowColor );
            }
        }

        if ( this.Common.ProcessModeRegions )
        {
            // Create line graphics path
            GraphicsPath selectionPath = null;
            GraphicsPath newPath = null;

            if ( this.IsPolygon )
            {
                selectionPath = pathAbs;
            }
            else
            {
                newPath = new GraphicsPath();
                selectionPath = newPath;
                selectionPath.AddPath( pathAbs, false );
                using Pen pen = ( Pen ) graphics.Pen.Clone();
                // Increase pen size by 2 pixels
                pen.DashStyle = DashStyle.Solid;
                pen.Width += 2;
                try
                {
                    selectionPath.Widen( pen );
                }
                catch ( OutOfMemoryException )
                {
                    // GraphicsPath.Widen incorrectly throws OutOfMemoryException
                    // catching here and reacting by not widening
                }
                catch ( ArgumentException )
                {
                }
            }

            // Add hot region
            this.Common.HotRegionsList.AddHotRegion(
                graphics,
                selectionPath,
                false,
                this.ReplaceKeywords( this.ToolTip ),
                string.Empty,
                string.Empty,
                string.Empty,
                this,
                ChartElementType.Annotation );

            // Clean up
            newPath?.Dispose();
        }

        // Restore line caps
        if ( capChanged )
        {
            graphics.Pen.StartCap = oldStartCap;
            graphics.Pen.EndCap = oldEndCap;
        }

        // Paint selection handles
        this.PaintSelectionHandles( graphics, rectanglePosition, pathAbs );
    }

    #endregion // Painting

    #region " position changing "
    /// <summary>
    /// Changes annotation position, so it exactly matches the boundary of the 
    /// polyline path.
    /// </summary>
    private void ResizeToPathBoundary()
    {
        if ( this._graphicsPath.PointCount > 0 )
        {
            // Get current annotation position in relative coordinates
            PointF firstPoint = PointF.Empty;
            PointF anchorPoint = PointF.Empty;
            SizeF size = SizeF.Empty;
            this.GetRelativePosition( out firstPoint, out size, out anchorPoint );

            // Get path boundary and convert it to relative coordinates
            RectangleF pathBoundary = this._graphicsPath.GetBounds();
            pathBoundary.X *= size.Width / 100f;
            pathBoundary.Y *= size.Height / 100f;
            pathBoundary.X += firstPoint.X;
            pathBoundary.Y += firstPoint.Y;
            pathBoundary.Width *= size.Width / 100f;
            pathBoundary.Height *= size.Height / 100f;

            // Scale all current points
            using ( Matrix matrix = new() )
            {
                matrix.Scale( size.Width / pathBoundary.Width, size.Height / pathBoundary.Height );
                matrix.Translate( -pathBoundary.X, -pathBoundary.Y );
                this._graphicsPath.Transform( matrix );
            }

            // Set new position for annotation
            this.SetPositionRelative( pathBoundary, anchorPoint );
        }
    }
    /// <summary>
    /// Adjust annotation location and\or size as a result of user action.
    /// </summary>
    /// <param name="movingDistance">Distance to resize/move the annotation.</param>
    /// <param name="resizeMode">Resizing mode.</param>
    /// <param name="pixelCoord">Distance is in pixels, otherwise relative.</param>
    /// <param name="userInput">Indicates if position changing was a result of the user input.</param>
    internal override void AdjustLocationSize( SizeF movingDistance, ResizingMode resizeMode, bool pixelCoord, bool userInput )
    {
        // Call base class when not resizing the path points
        if ( resizeMode != ResizingMode.MovingPathPoints )
        {
            base.AdjustLocationSize( movingDistance, resizeMode, pixelCoord, userInput );
            return;
        }

        // Get annotation position in relative coordinates
        PointF firstPoint = PointF.Empty;
        PointF anchorPoint = PointF.Empty;
        SizeF size = SizeF.Empty;
        this.GetRelativePosition( out firstPoint, out size, out anchorPoint );

        // Remember path before moving operation
        if ( userInput && this.StartMovePathRel == null )
        {
            this.StartMovePathRel = ( GraphicsPath ) this._graphicsPath.Clone();
            this.StartMovePositionRel = new RectangleF( firstPoint, size );
            this.StartMoveAnchorLocationRel = new PointF( anchorPoint.X, anchorPoint.Y );

        }

        // Convert moving distance to coordinates relative to the annotation
        if ( pixelCoord )
        {
            movingDistance = this.GetGraphics().GetRelativeSize( movingDistance );
        }
        movingDistance.Width /= this.StartMovePositionRel.Width / 100.0f;
        movingDistance.Height /= this.StartMovePositionRel.Height / 100.0f;

        // Get path points and adjust position of one of them
        if ( this._graphicsPath.PointCount > 0 )
        {
            GraphicsPath pathToMove = userInput ? this.StartMovePathRel : this._graphicsPath;
            PointF[] pathPoints = pathToMove.PathPoints;
            byte[] pathTypes = pathToMove.PathTypes;

            for ( int pointIndex = 0; pointIndex < pathPoints.Length; pointIndex++ )
            {
                // Adjust position
                if ( this.CurrentPathPointIndex == pointIndex ||
                    this.CurrentPathPointIndex < 0 ||
                    this.CurrentPathPointIndex >= pathPoints.Length )
                {
                    pathPoints[pointIndex].X -= movingDistance.Width;
                    pathPoints[pointIndex].Y -= movingDistance.Height;
                }
            }


            // Adjust annotation position to the boundary of the path
            if ( userInput && this.AllowResizing )
            {
                // Get path bounds in relative coordinates
                this._defaultGraphicsPath.Dispose();
                this._defaultGraphicsPath = new GraphicsPath( pathPoints, pathTypes );
                this._graphicsPath = this._defaultGraphicsPath;

                RectangleF pathBounds = this._graphicsPath.GetBounds();
                pathBounds.X *= this.StartMovePositionRel.Width / 100f;
                pathBounds.Y *= this.StartMovePositionRel.Height / 100f;
                pathBounds.X += this.StartMovePositionRel.X;
                pathBounds.Y += this.StartMovePositionRel.Y;
                pathBounds.Width *= this.StartMovePositionRel.Width / 100f;
                pathBounds.Height *= this.StartMovePositionRel.Height / 100f;

                // Set new annotation position
                this.SetPositionRelative( pathBounds, anchorPoint );

                // Adjust path point position
                for ( int pointIndex = 0; pointIndex < pathPoints.Length; pointIndex++ )
                {
                    pathPoints[pointIndex].X = this.StartMovePositionRel.X + (pathPoints[pointIndex].X * (this.StartMovePositionRel.Width / 100f));
                    pathPoints[pointIndex].Y = this.StartMovePositionRel.Y + (pathPoints[pointIndex].Y * (this.StartMovePositionRel.Height / 100f));

                    pathPoints[pointIndex].X = (pathPoints[pointIndex].X - pathBounds.X) / (pathBounds.Width / 100f);
                    pathPoints[pointIndex].Y = (pathPoints[pointIndex].Y - pathBounds.Y) / (pathBounds.Height / 100f);
                }
            }


            // Position changed
            this.PositionChanged = true;

            // Recreate path with new points
            this._defaultGraphicsPath.Dispose();
            this._defaultGraphicsPath = new GraphicsPath( pathPoints, pathTypes );
            this._graphicsPath = this._defaultGraphicsPath;
            this._pathChanged = true;

            // Invalidate annotation
            this.Invalidate();
        }
    }

    #endregion // Position Changing

    #region " placement methods "

    /// <summary>
    /// Ends user placement of an annotation.
    /// </summary>
    /// <remarks>
    /// Ends an annotation placement operation previously started by a 
    /// <see cref="Annotation.BeginPlacement"/> method call.
    /// <para>
    /// Calling this method is not required, since placement will automatically
    /// end when an end user enters all required points. However, it is useful when an annotation 
    /// placement operation needs to be aborted for some reason.
    /// </para>
    /// </remarks>
    public override void EndPlacement()
    {
        // Call base method
        base.EndPlacement();

        // Position was changed
        this.Chart?.OnAnnotationPositionChanged( this );

        // Reset last placement position
        this.LastPlacementPosition = PointF.Empty;

        // Resize annotation to the boundary of the polygon
        this.ResizeToPathBoundary();

        // Position changed
        this.PositionChanged = true;
    }

    /// <summary>
    /// Handles mouse down event during annotation placement.
    /// </summary>
    /// <param name="point">Mouse cursor position in pixels.</param>
    /// <param name="buttons">Mouse button down.</param>
    internal override void PlacementMouseDown( PointF point, MouseButtons buttons )
    {
        // Call base class method if path editing is not allowed 
        if ( !this.AllowPathEditing )
        {
            base.PlacementMouseDown( point, buttons );
            return;
        }

        if ( buttons == MouseButtons.Right )
        {
            // Stop placement
            this.EndPlacement();
        }
        if ( buttons == MouseButtons.Left &&
            this.IsValidPlacementPosition( point.X, point.Y ) )
        {
            // Convert coordinate to relative
            PointF newPoint = this.GetGraphics().GetRelativePoint( point );

            if ( this.LastPlacementPosition.IsEmpty )
            {
                // Set annotation coordinates to full chart
                this.X = 0f;
                this.Y = 0f;
                this.Width = 100f;
                this.Height = 100f;

                // Remember position where mouse was clicked
                this.LastPlacementPosition = newPoint;
            }
            else
            {
                if ( this.LastPlacementPosition.X == newPoint.X &&
                    this.LastPlacementPosition.Y == newPoint.Y )
                {
                    // Stop placement
                    this.EndPlacement();
                }
            }

            // Add a line from prev. position to current into the path
            using ( GraphicsPath tmpPath = new() )
            {
                PointF firstPoint = this.LastPlacementPosition;
                if ( this._graphicsPath.PointCount > 1 )
                {
                    firstPoint = this._graphicsPath.GetLastPoint();
                }
                tmpPath.AddLine( firstPoint, newPoint );
                this._graphicsPath.AddPath( tmpPath, true );
            }

            // Remember last position
            this.LastPlacementPosition = newPoint;

            // Invalidate and update the chart
            if ( this.Chart is not null )
            {
                this.Invalidate();
                this.Chart.UpdateAnnotations();
            }
        }
    }

    /// <summary>
    /// Handles mouse up event during annotation placement.
    /// </summary>
    /// <param name="point">Mouse cursor position in pixels.</param>
    /// <param name="buttons">Mouse button Up.</param>
    /// <returns>Return true when placing finished.</returns>
    internal override bool PlacementMouseUp( PointF point, MouseButtons buttons )
    {
        // Call base class method if path editing is not allowed 
        if ( !this.AllowPathEditing )
        {
            return base.PlacementMouseUp( point, buttons );
        }

        if ( buttons == MouseButtons.Left &&
            this._isFreeDrawPlacement )
        {
            // Stop placement
            this.EndPlacement();

        }

        return false;
    }

    /// <summary>
    /// Handles mouse move event during annotation placement.
    /// </summary>
    /// <param name="point">Mouse cursor position in pixels.</param>
    internal override void PlacementMouseMove( PointF point )
    {
        // Call base class method if path editing is not allowed 
        if ( !this.AllowPathEditing )
        {
            base.PlacementMouseMove( point );
            return;
        }

        // Check if annotation was moved
        if ( this.GetGraphics() != null &&
            this._graphicsPath.PointCount > 0 &&
            !this.LastPlacementPosition.IsEmpty )
        {
            // Convert coordinate to relative
            PointF newPoint = this.GetGraphics().GetRelativePoint( point );
            if ( this._isFreeDrawPlacement )
            {
                // Add new point
                using GraphicsPath tmpPath = new();
                PointF firstPoint = this.LastPlacementPosition;
                if ( this._graphicsPath.PointCount > 1 )
                {
                    firstPoint = this._graphicsPath.GetLastPoint();
                }
                tmpPath.AddLine( firstPoint, newPoint );
                this._graphicsPath.AddPath( tmpPath, true );
            }
            else
            {
                // Adjust last point position
                PointF[] pathPoints = this._graphicsPath.PathPoints;
                byte[] pathTypes = this._graphicsPath.PathTypes;
                pathPoints[^1] = newPoint;

                this._defaultGraphicsPath.Dispose();
                this._defaultGraphicsPath = new GraphicsPath( pathPoints, pathTypes );
                this._graphicsPath = this._defaultGraphicsPath;

            }

            // Position changed
            this.PositionChanged = true;

            // Invalidate and update the chart
            if ( this.Chart is not null )
            {
                this.Invalidate();
                this.Chart.UpdateAnnotations();
            }
        }
    }

    #endregion // Placement Methods

    #endregion

    #region " idisposable override  "
    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            if ( this._defaultGraphicsPath is not null )
            {
                this._defaultGraphicsPath.Dispose();
                this._defaultGraphicsPath = null;
            }
            if ( this._pathPoints is not null )
            {
                this._pathPoints.Dispose();
                this._pathPoints = null;
            }

        }
        base.Dispose( disposing );
    }
    #endregion
}
/// <summary>
/// <b>PolygonAnnotation</b> is a class that represents a polygon annotation.
/// </summary>
[
    SRDescription( "DescriptionAttributePolygonAnnotation_PolygonAnnotation" ),
]
public class PolygonAnnotation : PolylineAnnotation
{
    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public PolygonAnnotation()
        : base() => this.IsPolygon = true;

    #endregion

    #region " properties "

    #region " non applicable annotation appearance attributes (set as non-browsable) "

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="EndCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( LineAnchorCapStyle.None ),
    ]
    public override LineAnchorCapStyle StartCap
    {
        get => base.StartCap;
        set => base.StartCap = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// <seealso cref="StartCap"/>
    /// </summary>
    /// <value>
    /// A <see cref="LineAnchorCapStyle"/> value.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( LineAnchorCapStyle.None ),
    ]
    public override LineAnchorCapStyle EndCap
    {
        get => base.EndCap;
        set => base.EndCap = value;
    }

    #endregion

    #region " applicable annotation appearance attributes (set as browsable) "

    /// <summary>
    /// Gets or sets the background color of an annotation.
    /// <seealso cref="BackSecondaryColor"/>
    /// <seealso cref="BackHatchStyle"/>
    /// <seealso cref="BackGradientStyle"/>
    /// </summary>
    /// <value>
    /// A <see cref="Color"/> value used for the background of an annotation.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( true ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeBackColor" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = value;
    }

    /// <summary>
    /// Gets or sets the background hatch style of an annotation.
    /// <seealso cref="BackSecondaryColor"/>
    /// <seealso cref="BackColor"/>
    /// <seealso cref="BackGradientStyle"/>
    /// </summary>
    /// <value>
    /// A <see cref="ChartHatchStyle"/> value used for the background of an annotation.
    /// </value>
    /// <remarks>
    /// Two colors are used to draw the hatching, <see cref="BackColor"/> and <see cref="BackSecondaryColor"/>.
    /// </remarks>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( true ),
DefaultValue( ChartHatchStyle.None ),
NotifyParentProperty( true ),
SRDescription( "DescriptionAttributeBackHatchStyle" ),
Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
]
    public override ChartHatchStyle BackHatchStyle
    {
        get => base.BackHatchStyle;
        set => base.BackHatchStyle = value;
    }

    /// <summary>
    /// Gets or sets the background gradient style of an annotation.
    /// <seealso cref="BackSecondaryColor"/>
    /// <seealso cref="BackColor"/>
    /// <seealso cref="BackHatchStyle"/>
    /// </summary>
    /// <value>
    /// A <see cref="GradientStyle"/> value used for the background of an annotation.
    /// </value>
    /// <remarks>
    /// Two colors are used to draw the gradient, <see cref="BackColor"/> and <see cref="BackSecondaryColor"/>.
    /// </remarks>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( true ),
DefaultValue( GradientStyle.None ),
NotifyParentProperty( true ),
SRDescription( "DescriptionAttributeBackGradientStyle" ),
Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
]
    public override GradientStyle BackGradientStyle
    {
        get => base.BackGradientStyle;
        set => base.BackGradientStyle = value;
    }

    /// <summary>
    /// Gets or sets the secondary background color of an annotation.
    /// <seealso cref="BackColor"/>
    /// <seealso cref="BackHatchStyle"/>
    /// <seealso cref="BackGradientStyle"/>
    /// </summary>
    /// <value>
    /// A <see cref="Color"/> value used for the secondary color of an annotation background with 
    /// hatching or gradient fill.
    /// </value>
    /// <remarks>
    /// This color is used with <see cref="BackColor"/> when <see cref="BackHatchStyle"/> or
    /// <see cref="BackGradientStyle"/> are used.
    /// </remarks>
    [
SRCategory( "CategoryAttributeAppearance" ),
Browsable( true ),
DefaultValue( typeof( Color ), "" ),
NotifyParentProperty( true ),
SRDescription( "DescriptionAttributeBackSecondaryColor" ),
TypeConverter( typeof( ColorConverter ) ),
Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
]
    public override Color BackSecondaryColor
    {
        get => base.BackSecondaryColor;
        set => base.BackSecondaryColor = value;
    }

    #endregion

    #region " other "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeAnnotationType" ),
    ]
    public override string AnnotationType => "Polygon";

    /// <summary>
    /// Gets or sets an annotation's selection points style.
    /// </summary>
    /// <value>
    /// A <see cref="SelectionPointsStyle"/> value that represents an annotation's 
    /// selection style.
    /// </value>
    /// <remarks>
    /// This property is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( SelectionPointsStyle.Rectangle ),
    ParenthesizePropertyName( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeSelectionPointsStyle" ),
    ]
    internal override SelectionPointsStyle SelectionPointsStyle => SelectionPointsStyle.Rectangle;

    #endregion

    #endregion
}
/// <summary><b>AnnotationPathPointCollection</b> is a collection of polyline 
/// annotation path points, and is only available via the <b>GraphicsPathPoints</b> 
/// property at design-time.
/// <seealso cref="PolylineAnnotation.GraphicsPathPoints"/></summary>
/// <remarks>
/// This collection is used at design-time only, and uses serialization to expose the 
/// shape of the polyline and polygon via their GraphicsPathPoints collection property.
/// At run-time, use Path property to set the path of a polyline or polygon
/// </remarks>
/// <remarks>
/// Default public constructor.
/// </remarks>
[
SRDescription( "DescriptionAttributeAnnotationPathPointCollection_annotationPathPointCollection" ),
]
public class AnnotationPathPointCollection( PolylineAnnotation annotation ) : ChartElementCollection<AnnotationPathPoint>( annotation )
{
    #region " fields "

    internal PolylineAnnotation Annotation { get; set; } = annotation;

#pragma warning disable IDE1006 // Naming Styles

    private GraphicsPath _graphicsPath;

#pragma warning restore IDE1006 // Naming Styles
    #endregion // Fields
    #region " constructtion "

    #endregion // Constructors

    #region " methods "

    /// <summary>
    /// Forces the invalidation of the chart element
    /// </summary>
    public override void Invalidate()
    {
        if ( this.Annotation is not null )
        {
            //Dispose previously instantiated graphics path
            if ( this._graphicsPath is not null )
            {
                this._graphicsPath.Dispose();
                this._graphicsPath = null;
            }

            // Recreate polyline annotation path
            if ( this.Count > 0 )
            {
                PointF[] points = new PointF[this.Count];
                byte[] types = new byte[this.Count];
                for ( int index = 0; index < this.Count; index++ )
                {
                    points[index] = new PointF( this[index].X, this[index].Y );
                    types[index] = this[index].PointType;
                }
                this._graphicsPath = new GraphicsPath( points, types );
            }
            else
            {
                this._graphicsPath = new GraphicsPath();
            }

            // Invalidate annotation
            this.Annotation.GraphicsPath = this._graphicsPath;
            this.Annotation.Invalidate();
        }
        base.Invalidate();
    }

    #endregion // Methods

    #region " idisposable members "
    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            // Free up managed resources
            if ( this._graphicsPath is not null )
            {
                this._graphicsPath.Dispose();
                this._graphicsPath = null;
            }
        }
        base.Dispose( disposing );
    }

    #endregion
}
/// <summary>
/// The <b>AnnotationPathPoint</b> class represents a path point of a polyline or polygon, 
/// and is stored in their <b>GraphicsPathPoints</b> property, which is only available at design-time.
/// </summary>
/// <remarks>
/// At run-time, use <b>Path</b> property to set the path of a polyline or polygon.
/// </remarks>
[
    SRDescription( "DescriptionAttributeAnnotationPathPoint_annotationPathPoint" ),
]
public class AnnotationPathPoint : ChartElement
{
    #region " fields "

    // Point X value

    // Point type

    #endregion // Fields

    #region " constructtion "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public AnnotationPathPoint()
    {
    }

    /// <summary>
    /// Constructor that takes X and Y parameters.
    /// </summary>
    /// <param name="x">Point's X value.</param>
    /// <param name="y">Point's Y value.</param>
    public AnnotationPathPoint( float x, float y )
    {
        this.X = x;
        this.Y = y;
    }

    /// <summary>
    /// Constructor that takes X, Y and point type parameters.
    /// </summary>
    /// <param name="x">Point's X value.</param>
    /// <param name="y">Point's Y value.</param>
    /// <param name="type">Point type.</param>
    public AnnotationPathPoint( float x, float y, byte type )
    {
        this.X = x;
        this.Y = y;
        this.PointType = type;
    }

    #endregion // Constructors

    #region " properties "

    /// <summary>
    /// Gets or sets an annotation path point's X coordinate.
    /// </summary>
    /// <value>
    /// A float value for the point's X coordinate.
    /// </value>
    [
    SRCategory( "CategoryAttributePosition" ),
    DefaultValue( 0f ),
    Browsable( true ),
    SRDescription( "DescriptionAttributeAnnotationPathPoint_X" ),
    ]
    public float X { get; set; } = 0f;

    /// <summary>
    /// Gets or sets an annotation path point's Y coordinate.
    /// </summary>
    /// <value>
    /// A float value for the point's Y coordinate.
    /// </value>
    [
    SRCategory( "CategoryAttributePosition" ),
    DefaultValue( 0f ),
    Browsable( true ),
    SRDescription( "DescriptionAttributeAnnotationPathPoint_Y" ),
    ]
    public float Y { get; set; } = 0f;

    /// <summary>
    /// Gets or sets an annotation path point's type.
    /// </summary>
    /// <value>
    /// A byte value.
    /// </value>
    /// <remarks>
    /// See the <see cref="PathPointType"/> enumeration for more details.
    /// </remarks>
    [
    SRCategory( "CategoryAttributePosition" ),
    DefaultValue( typeof( byte ), "1" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SRDescription( "DescriptionAttributeAnnotationPathPoint_Name" ),
    ]
    public byte PointType { get; set; } = 1;

    /// <summary>
    /// Gets or sets an annotation path point's name.
    /// </summary>
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( "PathPoint" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SRDescription( "DescriptionAttributeAnnotationPathPoint_Name" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    ]
    public string Name => "PathPoint";

    #endregion // Properties

}
