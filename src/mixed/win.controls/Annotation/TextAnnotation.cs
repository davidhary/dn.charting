//
//  Purpose:	Text annotation class.
//

#pragma warning disable IDE1006 // Naming Styles

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Security;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;
using Point = Point;
using Size = Size;
/// <summary>
/// <b>TextAnnotation</b> is a class that represents a text annotation.
/// </summary>
/// <remarks>
/// Note that other annotations do display inner text (e.g. rectangle, 
/// ellipse annotations.).
/// </remarks>
[
    SRDescription( "DescriptionAttributeTextAnnotation_TextAnnotation" ),
]
public class TextAnnotation : Annotation
{
    // Current content size
    internal SizeF contentSize = SizeF.Empty;

    // Indicates that annotation is an ellipse
    internal bool isEllipse;

    #region " construction and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public TextAnnotation()
        : base()
    {
    }

    #endregion

    #region " properties "

    #region " text visual attributes "

    // Annotation text
    private string _text = "";

    /// <summary>
    /// Annotation's text.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeText" ),
    ]
    public virtual string Text
    {
        get => this._text;
        set
        {
            this._text = value;
            this.Invalidate();

            // Reset content size to empty
            this.contentSize = SizeF.Empty;
        }
    }

    private bool _isMultiline;

    /// <summary>
    /// Indicates whether the annotation text is multi-line.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeMultiline" ),
    ]
    public virtual bool IsMultiline
    {
        get => this._isMultiline;
        set
        {
            this._isMultiline = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the font of an annotation's text.
    /// <seealso cref="Annotation.ForeColor"/>
    /// </summary>
    /// <value>
    /// A <see cref="Font"/> object used for an annotation's text.
    /// </value>
    [
SRCategory( "CategoryAttributeAppearance" ),
DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8pt" ),
SRDescription( "DescriptionAttributeTextFont4" ),
]
    public override Font Font
    {
        get => base.Font;
        set
        {
            base.Font = value;

            // Reset content size to empty
            this.contentSize = SizeF.Empty;
        }
    }

    #endregion

    #region " non applicable annotation appearance attributes (set as non-browsable) "

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "Black" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color LineColor
    {
        get => base.LineColor;
        set => base.LineColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeLineWidth" ),
    ]
    public override int LineWidth
    {
        get => base.LineWidth;
        set => base.LineWidth = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( ChartDashStyle.Solid ),
    ]
    public override ChartDashStyle LineDashStyle
    {
        get => base.LineDashStyle;
        set => base.LineDashStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( ChartHatchStyle.None ),
    NotifyParentProperty( true ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
    ]
    public override ChartHatchStyle BackHatchStyle
    {
        get => base.BackHatchStyle;
        set => base.BackHatchStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( GradientStyle.None ),
    NotifyParentProperty( true ),
    Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
    ]
    public override GradientStyle BackGradientStyle
    {
        get => base.BackGradientStyle;
        set => base.BackGradientStyle = value;
    }

    /// <summary>
    /// Not applicable to this annotation type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color BackSecondaryColor
    {
        get => base.BackSecondaryColor;
        set => base.BackSecondaryColor = value;
    }

    #endregion

    #region " other "

    /// <summary>
    /// Gets or sets an annotation's type name.
    /// </summary>
    /// <remarks>
    /// This property is used to get the name of each annotation type 
    /// (e.g. Line, Rectangle, Ellipse). 
    /// <para>
    /// This property is for internal use and is hidden at design and run time.
    /// </para>
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeTextAnnotation_annotationType" ),
    ]
    public override string AnnotationType => "Text";

    /// <summary>
    /// Annotation selection points style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( SelectionPointsStyle.Rectangle ),
    ParenthesizePropertyName( true ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    SRDescription( "DescriptionAttributeSelectionPointsStyle" ),
    ]
    internal override SelectionPointsStyle SelectionPointsStyle => SelectionPointsStyle.Rectangle;

    #endregion

    #endregion

    #region " methods "

    #region " painting "

    /// <summary>
    /// Paints an annotation object on the specified graphics.
    /// </summary>
    /// <param name="graphics">
    /// A <see cref="ChartGraphics"/> object, used to paint an annotation object.
    /// </param>
    /// <param name="chart">
    /// Reference to the <see cref="Chart"/> owner control.
    /// </param>
    internal override void Paint( Chart chart, ChartGraphics graphics )
    {
        // Get annotation position in relative coordinates
        PointF firstPoint = PointF.Empty;
        PointF anchorPoint = PointF.Empty;
        SizeF size = SizeF.Empty;
        this.GetRelativePosition( out firstPoint, out size, out anchorPoint );
        PointF secondPoint = new( firstPoint.X + size.Width, firstPoint.Y + size.Height );

        // Create selection rectangle
        RectangleF selectionRect = new( firstPoint, new SizeF( secondPoint.X - firstPoint.X, secondPoint.Y - firstPoint.Y ) );

        // Get text position
        RectangleF textPosition = new( selectionRect.Location, selectionRect.Size );
        if ( textPosition.Width < 0 )
        {
            textPosition.X = textPosition.Right;
            textPosition.Width = -textPosition.Width;
        }
        if ( textPosition.Height < 0 )
        {
            textPosition.Y = textPosition.Bottom;
            textPosition.Height = -textPosition.Height;
        }

        // Check if text position is valid
        if ( textPosition.IsEmpty ||
            float.IsNaN( textPosition.X ) ||
            float.IsNaN( textPosition.Y ) ||
            float.IsNaN( textPosition.Right ) ||
            float.IsNaN( textPosition.Bottom ) )
        {
            return;
        }

        if ( this.Common.ProcessModePaint )
        {
            _ = this.DrawText( graphics, textPosition, false, false );
        }

        if ( this.Common.ProcessModeRegions )
        {
            // Add hot region
            if ( this.isEllipse )
            {
                using GraphicsPath ellipsePath = new();
                ellipsePath.AddEllipse( textPosition );
                this.Common.HotRegionsList.AddHotRegion(
                    graphics,
                    ellipsePath,
                    true,
                    this.ReplaceKeywords( this.ToolTip ),
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    this,
                    ChartElementType.Annotation );
            }
            else
            {
                this.Common.HotRegionsList.AddHotRegion(
                    textPosition,
                    this.ReplaceKeywords( this.ToolTip ),
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    this,
                    ChartElementType.Annotation,
                    string.Empty );
            }
        }

        // Paint selection handles
        this.PaintSelectionHandles( graphics, selectionRect, null );
    }

    /// <summary>
    /// Draws text in specified rectangle.
    /// </summary>
    /// <param name="graphics">Chart graphics.</param>
    /// <param name="textPosition">Text position.</param>
    /// <param name="noSpacingForCenteredText">True if text allowed to be outside of position when centered.</param>
    /// <param name="getTextPosition">True if position text must be returned by the method.</param>
    /// <returns>Text actual position if required.</returns>
    internal RectangleF DrawText( ChartGraphics graphics, RectangleF textPosition, bool noSpacingForCenteredText, bool getTextPosition )
    {
        RectangleF textActualPosition = RectangleF.Empty;

        // ============================***
        // Adjust text position using text spacing
        // ============================***
        RectangleF textSpacing = this.GetTextSpacing( out bool annotationRelative );
        float spacingScaleX = 1f;
        float spacingScaleY = 1f;
        if ( annotationRelative )
        {
            if ( textPosition.Width > 25f )
            {
                spacingScaleX = textPosition.Width / 50f;
                spacingScaleX = Math.Max( 1f, spacingScaleX );
            }
            if ( textPosition.Height > 25f )
            {
                spacingScaleY = textPosition.Height / 50f;
                spacingScaleY = Math.Max( 1f, spacingScaleY );
            }
        }

        RectangleF textPositionWithSpacing = new( textPosition.Location, textPosition.Size );
        textPositionWithSpacing.Width -= (textSpacing.Width + textSpacing.X) * spacingScaleX;
        textPositionWithSpacing.X += textSpacing.X * spacingScaleX;
        textPositionWithSpacing.Height -= (textSpacing.Height + textSpacing.Y) * spacingScaleY;
        textPositionWithSpacing.Y += textSpacing.Y * spacingScaleY;

        // ============================***
        // Replace new line characters
        // ============================***
        string titleText = this.ReplaceKeywords( this.Text.Replace( "\\n", "\n" ) );

        // ============================***
        // Check if centered text require spacing.
        // Use only half of the spacing required.
        // Apply only for 1 line of text.
        // ============================***
        if ( noSpacingForCenteredText && !titleText.Contains( '\n'.ToString() ) )
        {
            if ( this.Alignment is ContentAlignment.MiddleCenter or
                ContentAlignment.MiddleLeft or
                ContentAlignment.MiddleRight )
            {
                textPositionWithSpacing.Y = textPosition.Y;
                textPositionWithSpacing.Height = textPosition.Height;
                textPositionWithSpacing.Height -= (textSpacing.Height / 2f) + (textSpacing.Y / 2f);
                textPositionWithSpacing.Y += textSpacing.Y / 2f;
            }
            if ( this.Alignment is ContentAlignment.BottomCenter or
                ContentAlignment.MiddleCenter or
                ContentAlignment.TopCenter )
            {
                textPositionWithSpacing.X = textPosition.X;
                textPositionWithSpacing.Width = textPosition.Width;
                textPositionWithSpacing.Width -= (textSpacing.Width / 2f) + (textSpacing.X / 2f);
                textPositionWithSpacing.X += textSpacing.X / 2f;
            }
        }

        // Draw text
        using ( Brush textBrush = new SolidBrush( this.ForeColor ) )
        {
            using StringFormat format = new( StringFormat.GenericTypographic );
            // ============================***
            // Set text format
            // ============================***
            format.FormatFlags ^= StringFormatFlags.LineLimit;
            format.Trimming = StringTrimming.EllipsisCharacter;
            if ( this.Alignment is ContentAlignment.BottomRight or
                ContentAlignment.MiddleRight or
                ContentAlignment.TopRight )
            {
                format.Alignment = StringAlignment.Far;
            }
            if ( this.Alignment is ContentAlignment.BottomCenter or
                ContentAlignment.MiddleCenter or
                ContentAlignment.TopCenter )
            {
                format.Alignment = StringAlignment.Center;
            }
            if ( this.Alignment is ContentAlignment.BottomCenter or
                ContentAlignment.BottomLeft or
                ContentAlignment.BottomRight )
            {
                format.LineAlignment = StringAlignment.Far;
            }
            if ( this.Alignment is ContentAlignment.MiddleCenter or
                ContentAlignment.MiddleLeft or
                ContentAlignment.MiddleRight )
            {
                format.LineAlignment = StringAlignment.Center;
            }

            // ============================***
            // Set shadow color and offset
            // ============================***
            Color textShadowColor = ChartGraphics.GetGradientColor( this.ForeColor, Color.Black, 0.8 );
            int textShadowOffset = 1;
            TextStyle textStyle = this.TextStyle;
            if ( textStyle == TextStyle.Shadow &&
                this.ShadowOffset != 0 )
            {
                // Draw shadowed text
                textShadowColor = this.ShadowColor;
                textShadowOffset = this.ShadowOffset;
            }

            if ( textStyle == TextStyle.Shadow )
            {
                textShadowColor = (textShadowColor.A != 255) ? textShadowColor : Color.FromArgb( textShadowColor.A / 2, textShadowColor );
            }

            // ============================***
            // Get text actual position
            // ============================***
            if ( getTextPosition )
            {
                // Measure text size
                SizeF textSize = graphics.MeasureStringRel(
                    this.ReplaceKeywords( this._text.Replace( "\\n", "\n" ) ),
                    this.Font,
                    textPositionWithSpacing.Size,
                    format );

                // Get text position
                textActualPosition = new RectangleF( textPositionWithSpacing.Location, textSize );
                if ( this.Alignment is ContentAlignment.BottomRight or
                    ContentAlignment.MiddleRight or
                    ContentAlignment.TopRight )
                {
                    textActualPosition.X += textPositionWithSpacing.Width - textSize.Width;
                }
                if ( this.Alignment is ContentAlignment.BottomCenter or
                    ContentAlignment.MiddleCenter or
                    ContentAlignment.TopCenter )
                {
                    textActualPosition.X += (textPositionWithSpacing.Width - textSize.Width) / 2f;
                }
                if ( this.Alignment is ContentAlignment.BottomCenter or
                    ContentAlignment.BottomLeft or
                    ContentAlignment.BottomRight )
                {
                    textActualPosition.Y += textPositionWithSpacing.Height - textSize.Height;
                }
                if ( this.Alignment is ContentAlignment.MiddleCenter or
                    ContentAlignment.MiddleLeft or
                    ContentAlignment.MiddleRight )
                {
                    textActualPosition.Y += (textPositionWithSpacing.Height - textSize.Height) / 2f;
                }

                // Do not allow text to go outside annotation position
                textActualPosition.Intersect( textPositionWithSpacing );
            }

            RectangleF absPosition = graphics.GetAbsoluteRectangle( textPositionWithSpacing );
            Title.DrawStringWithStyle(
                    graphics,
                    titleText,
                    this.TextStyle,
                    this.Font,
                    absPosition,
                    this.ForeColor,
                    textShadowColor,
                    textShadowOffset,
                    format,
                    TextOrientation.Auto
              );
        }

        return textActualPosition;
    }

    #endregion // Painting

    #region " text editing "

    // Control used to edit text
    private TextBox _editTextBox;

    /// <summary>
    /// Stops editing of the annotation text.
    /// <seealso cref="BeginTextEditing"/>
    /// </summary>
    /// <remarks>
    /// Call this method to cancel text editing, which was started via a call to 
    /// the <see cref="BeginTextEditing"/> method, or after the end-user double-clicks 
    /// on the annotation.
    /// </remarks>
    public void StopTextEditing()
    {
        // Check if text is currently edited
        if ( this._editTextBox is not null )
        {
            // Set annotation text
            this.Text = this._editTextBox.Text;

            // Remove and dispose the text box
            try
            {
                this._editTextBox.KeyDown -= new KeyEventHandler( this.OnTextBoxKeyDown );
                this._editTextBox.LostFocus -= new EventHandler( this.OnTextBoxLostFocus );
            }
            catch ( SecurityException )
            {
                // Ignore security issues
            }

            if ( this.Chart.Controls.Contains( this._editTextBox ) )
            {
                TextBox tempControl = null;
                try
                {
                    // NOTE: Workaround .Net bug. Issue with application closing if
                    // active control is removed.
                    Form parentForm = this.Chart.FindForm();
                    if ( parentForm is not null )
                    {
                        tempControl = new TextBox
                        {
                            Visible = false
                        };

                        // Add temp. control as active
                        parentForm.Controls.Add( tempControl );
                        parentForm.ActiveControl = tempControl;
                    }
                }
                catch ( SecurityException )
                {
                    // Ignore security issues
                }

                // Remove text editor
                this.Chart.Controls.Remove( this._editTextBox );

                // Dispose temp. text box
                tempControl?.Dispose();
            }

            // Dispose edit box
            this._editTextBox.Dispose();
            this._editTextBox = null;

            // Raise notification event
            this.Chart?.OnAnnotationTextChanged( this );

            // Update chart
            if ( this.Chart is not null )
            {
                this.Chart.Invalidate();
                this.Chart.Update();
            }

        }
    }

    /// <summary>
    /// Handles event when focus is lost by the text editing control.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void OnTextBoxLostFocus( object sender, EventArgs e )
    {
        this.StopTextEditing();
    }

    /// <summary>
    /// Handles event when key is pressed in the text editing control.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void OnTextBoxKeyDown( object sender, KeyEventArgs e )
    {
        if ( e.KeyCode == Keys.Escape )
        {
            // Reset text and stop editing
            this._editTextBox.Text = this.Text;
            this.StopTextEditing();
        }
        else if ( e.KeyCode == Keys.Enter &&
!this.IsMultiline )
        {
            // Stop editing
            this.StopTextEditing();
        }
    }

    /// <summary>
    /// Begins editing the annotation's text by an end user.
    /// <seealso cref="StopTextEditing"/>
    /// </summary>
    /// <remarks>
    /// After calling this method, the annotation displays an editing box which allows 
    /// for editing of the annotation's text.
    /// <para>
    /// Call the <see cref="StopTextEditing"/> method to cancel this mode programmatically.  
    /// Note that editing ends when the end-user hits the <c>Enter</c> key if multi-line 
    /// is false, or when the end-user clicks outside of the editing box if multi-line is true.
    /// </para>
    /// </remarks>
    public void BeginTextEditing()
    {


        if ( this.Chart != null && this.AllowTextEditing )
        {
            // Dispose previous text box
            if ( this._editTextBox is not null )
            {
                if ( this.Chart.Controls.Contains( this._editTextBox ) )
                {
                    this.Chart.Controls.Remove( this._editTextBox );
                }
                this._editTextBox.Dispose();
                this._editTextBox = null;
            }

            // Create a text box inside the chart
            this._editTextBox = new TextBox
            {
                Text = this.Text,
                Multiline = this.IsMultiline,
                Font = this.Font,
                BorderStyle = BorderStyle.FixedSingle,
                BackColor = Color.FromArgb( 255, this.BackColor.IsEmpty ? Color.White : this.BackColor ),
                ForeColor = Color.FromArgb( 255, this.ForeColor )
            };

            // Calculate text position in relative coordinates
            PointF firstPoint = PointF.Empty;
            PointF anchorPoint = PointF.Empty;
            SizeF size = SizeF.Empty;
            this.GetRelativePosition( out firstPoint, out size, out anchorPoint );
            PointF secondPoint = new( firstPoint.X + size.Width, firstPoint.Y + size.Height );
            RectangleF textPosition = new( firstPoint, new SizeF( secondPoint.X - firstPoint.X, secondPoint.Y - firstPoint.Y ) );
            if ( textPosition.Width < 0 )
            {
                textPosition.X = textPosition.Right;
                textPosition.Width = -textPosition.Width;
            }
            if ( textPosition.Height < 0 )
            {
                textPosition.Y = textPosition.Bottom;
                textPosition.Height = -textPosition.Height;
            }

            // Set text control position in pixels
            if ( this.GetGraphics() is not null )
            {
                // Convert point to relative coordinates
                textPosition = this.GetGraphics().GetAbsoluteRectangle( textPosition );
            }

            // Adjust Location and Size
            if ( this.IsMultiline )
            {
                textPosition.X -= 1;
                textPosition.Y -= 1;
                textPosition.Width += 2;
                textPosition.Height += 2;
            }
            else
            {
                textPosition.Y += (textPosition.Height / 2f) - (this._editTextBox.Size.Height / 2f);
            }
            this._editTextBox.Location = Point.Round( textPosition.Location );
            this._editTextBox.Size = Size.Round( textPosition.Size );

            // Add control to the chart
            this.Chart.Controls.Add( this._editTextBox );
            try
            {
                this._editTextBox.SelectAll();
                _ = this._editTextBox.Focus();
            }
            catch ( SecurityException )
            {
                // Ignore security issues
            }

            try
            {
                // Set text box event handlers
                this._editTextBox.KeyDown += new KeyEventHandler( this.OnTextBoxKeyDown );
                this._editTextBox.LostFocus += new EventHandler( this.OnTextBoxLostFocus );
            }
            catch ( SecurityException )
            {
                // Ignore security issues
            }
        }
    }

    #endregion // Text Editing

    #region " content size "

    /// <summary>
    /// Gets text annotation content size based on the text and font.
    /// </summary>
    /// <returns>Annotation content position.</returns>
    internal override RectangleF GetContentPosition()
    {
        // Return pre calculated value
        if ( !this.contentSize.IsEmpty )
        {
            return new RectangleF( float.NaN, float.NaN, this.contentSize.Width, this.contentSize.Height );
        }

        // Create temporary bitmap based chart graphics if chart was not 
        // rendered yet and the graphics was not created.
        // NOTE: Fix for issue #3978.
        Graphics graphics = null;
        Image graphicsImage = null;
        ChartGraphics tempChartGraph = null;
        if ( this.GetGraphics() == null && this.Common is not null )
        {
            graphicsImage = new Bitmap( this.Common.ChartPicture.Width, this.Common.ChartPicture.Height );
            graphics = Graphics.FromImage( graphicsImage );
            tempChartGraph = new ChartGraphics( this.Common )
            {
                Graphics = graphics
            };
            tempChartGraph.SetPictureSize( this.Common.ChartPicture.Width, this.Common.ChartPicture.Height );
            this.Common.graph = tempChartGraph;
        }

        // Calculate content size
        RectangleF result = RectangleF.Empty;
        if ( this.GetGraphics() != null && this.Text.Trim().Length > 0 )
        {
            // Measure text using current font and slightly increase it
            this.contentSize = this.GetGraphics().MeasureString(
                 "W" + this.ReplaceKeywords( this.Text.Replace( "\\n", "\n" ) ),
                 this.Font,
                 new SizeF( 2000, 2000 ),
                 StringFormat.GenericTypographic );

            this.contentSize.Height *= 1.04f;

            // Convert to relative coordinates
            this.contentSize = this.GetGraphics().GetRelativeSize( this.contentSize );

            // Add spacing
            RectangleF textSpacing = this.GetTextSpacing( out bool annotationRelative );
            float spacingScaleX = 1f;
            float spacingScaleY = 1f;
            if ( annotationRelative )
            {
                if ( this.contentSize.Width > 25f )
                {
                    spacingScaleX = this.contentSize.Width / 25f;
                    spacingScaleX = Math.Max( 1f, spacingScaleX );
                }
                if ( this.contentSize.Height > 25f )
                {
                    spacingScaleY = this.contentSize.Height / 25f;
                    spacingScaleY = Math.Max( 1f, spacingScaleY );
                }
            }

            this.contentSize.Width += (textSpacing.X + textSpacing.Width) * spacingScaleX;
            this.contentSize.Height += (textSpacing.Y + textSpacing.Height) * spacingScaleY;

            result = new RectangleF( float.NaN, float.NaN, this.contentSize.Width, this.contentSize.Height );
        }

        // Dispose temporary chart graphics
        if ( tempChartGraph is not null )
        {
            tempChartGraph.Dispose();
            graphics.Dispose();
            graphicsImage.Dispose();
            this.Common.graph = null;
        }

        return result;
    }

    /// <summary>
    /// Gets text spacing on four different sides in relative coordinates.
    /// </summary>
    /// <param name="annotationRelative">Indicates that spacing is in annotation relative coordinates.</param>
    /// <returns>Rectangle with text spacing values.</returns>
    internal virtual RectangleF GetTextSpacing( out bool annotationRelative )
    {
        annotationRelative = false;
        RectangleF rect = new( 3f, 3f, 3f, 3f );
        if ( this.GetGraphics() is not null )
        {
            rect = this.GetGraphics().GetRelativeRectangle( rect );
        }
        return rect;
    }

    #endregion

    #region " placement methods "

    /// <summary>
    /// Ends user placement of an annotation.
    /// </summary>
    /// <remarks>
    /// Ends an annotation placement operation previously started by a 
    /// <see cref="Annotation.BeginPlacement"/> method call.
    /// <para>
    /// Calling this method is not required, since placement will automatically
    /// end when an end user enters all required points. However, it is useful when an annotation 
    /// placement operation needs to be aborted for some reason.
    /// </para>
    /// </remarks>
    public override void EndPlacement()
    {
        // Check if text editing is allowed
        // Maybe changed later in the EndPlacement method.
        bool allowTextEditing = this.AllowTextEditing;

        // Call base class
        base.EndPlacement();

        // Begin text editing
        if ( this.Chart is not null )
        {
            this.Chart.Annotations.LastClickedAnnotation = this;
            if ( allowTextEditing )
            {
                this.BeginTextEditing();
            }
        }
    }

    #endregion // Placement Methods

    #endregion	// Methods
}
/// <summary>
/// The <b>AnnotationSmartLabelStyle</b> class is used to store an annotation's smart 
/// labels properties.
/// <seealso cref="Annotation.SmartLabelStyle"/>
/// </summary>
/// <remarks>
/// This class is derived from the <b>SmartLabelStyle</b> class
/// used for <b>Series</b> objects.
/// </remarks>
[
    DefaultProperty( "Enabled" ),
    SRDescription( "DescriptionAttributeAnnotationSmartLabelsStyle_annotationSmartLabelsStyle" ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) ),
]
public class AnnotationSmartLabelStyle : SmartLabelStyle
{
    #region " constructors and initialization "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public AnnotationSmartLabelStyle() => this.chartElement = null;

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="chartElement">
    /// Chart element this style belongs to.
    /// </param>
    public AnnotationSmartLabelStyle( object chartElement ) : base( chartElement )
    {
    }

    #endregion

    #region " non applicable appearance attributes (set as non-browsable) "

    /// <summary>
    /// Callout style of the repositioned smart labels.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design time and runtime.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( LabelCalloutStyle.Underlined ),
    SRDescription( "DescriptionAttributeCalloutStyle3" ),
    ]
    public override LabelCalloutStyle CalloutStyle
    {
        get => base.CalloutStyle;
        set => base.CalloutStyle = value;
    }

    /// <summary>
    /// Label callout line color.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeCalloutLineColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color CalloutLineColor
    {
        get => base.CalloutLineColor;
        set => base.CalloutLineColor = value;
    }

    /// <summary>
    /// Label callout line style.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( ChartDashStyle.Solid ),
    SRDescription( "DescriptionAttributeLineDashStyle" ),
    ]
    public override ChartDashStyle CalloutLineDashStyle
    {
        get => base.CalloutLineDashStyle;
        set => base.CalloutLineDashStyle = value;
    }

    /// <summary>
    /// Label callout back color. Applies to the Box style only.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( typeof( Color ), "Transparent" ),
    SRDescription( "DescriptionAttributeCalloutBackColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public override Color CalloutBackColor
    {
        get => base.CalloutBackColor;
        set => base.CalloutBackColor = value;
    }

    /// <summary>
    /// Label callout line width.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeLineWidth" ),
    ]
    public override int CalloutLineWidth
    {
        get => base.CalloutLineWidth;
        set => base.CalloutLineWidth = value;
    }

    /// <summary>
    /// Label callout line anchor cap.
    /// </summary>
    /// <remarks>
    /// This method is for internal use and is hidden at design and run time.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( LineAnchorCapStyle.Arrow ),
    SRDescription( "DescriptionAttributeCalloutLineAnchorCapStyle" ),
    ]
    public override LineAnchorCapStyle CalloutLineAnchorCapStyle
    {
        get => base.CalloutLineAnchorCapStyle;
        set => base.CalloutLineAnchorCapStyle = value;
    }

    #endregion
}

#pragma warning restore IDE1006 // Naming Styles
