//
//  Purpose:	A utility class which defines chart palette colors.
//              These palettes are used to assign unique colors to 
//              different chart series. For some chart types, like 
//              Pie, different colors are applied on the data point 
//              level.
//              Selected chart series/points palette is exposed 
//              through Chart.Palette property. Series.Palette 
//              property should be used to set different palette 
//              color for each point of the series. 
//

namespace System.Windows.Forms.DataVisualization.Charting;

#region " color palettes enumeration "
/// <summary>
/// Chart color palettes enumeration
/// </summary>
public enum ChartColorPalette
{
    /// <summary>
    /// Palette not set.
    /// </summary>
    None,

    /// <summary>
    /// Bright palette.
    /// </summary>
    Bright,

    /// <summary>
    /// Palette with gray scale colors.
    /// </summary>
    Grayscale,

    /// <summary>
    /// Palette with Excel style colors.
    /// </summary>
    Excel,

    /// <summary>
    /// Palette with LightStyle style colors.
    /// </summary>
    Light,

    /// <summary>
    /// Palette with Pastel style colors.
    /// </summary>
    Pastel,

    /// <summary>
    /// Palette with Earth Tones style colors.
    /// </summary>
    EarthTones,

    /// <summary>
    /// Palette with SemiTransparent style colors.
    /// </summary>
    SemiTransparent,

    /// <summary>
    /// Palette with Berry style colors.
    /// </summary>
    Berry,

    /// <summary>
    /// Palette with Chocolate style colors.
    /// </summary>
    Chocolate,

    /// <summary>
    /// Palette with Fire style colors.
    /// </summary>
    Fire,

    /// <summary>
    /// Palette with SeaGreen style colors.
    /// </summary>
    SeaGreen,

    /// <summary>
    /// Bright pastel palette.
    /// </summary>
    BrightPastel
};

#endregion

