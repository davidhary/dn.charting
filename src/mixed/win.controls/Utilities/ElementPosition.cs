//
//  Purpose:	Class is used to store relative position of the chart
//				elements like Legend, Title and others. It uses
//              relative coordinate system where top left corner is
//              0,0 and bottom right is 100,100.
//              :
//              If Auto property is set to true, all position properties 
//              (X,Y,Width and Height) are ignored and they automatically
//              calculated during chart rendering.
//              :
//              Note that setting any of the position properties will 
//              automatically set Auto property to false.
//


using System.ComponentModel;
using System.Drawing;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// ElementPosition is the base class for many chart visual 
/// elements like Legend, Title and ChartArea. It provides 
/// the position of the chart element in relative coordinates, 
/// from (0,0) to (100,100).
/// </summary>
[
    SRDescription( "DescriptionAttributeElementPosition_ElementPosition" ),
    DefaultProperty( "Data" ),
]
public class ElementPosition : ChartElement
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles

    // Private data members, which store properties values
    private float _x;
    private float _y;
    private float _width;
    private float _height;
    internal bool _auto = true;

    // Indicates the auto position of all areas must be reset
    internal bool resetAreaAutoPosition;

#pragma warning restore IDE1006 // Naming Styles


    #endregion

    #region " constructtion "

    /// <summary>
    /// ElementPosition default constructor
    /// </summary>
    public ElementPosition()
    {
    }

    /// <summary>
    /// ElementPosition default constructor
    /// </summary>
    internal ElementPosition( IChartElement parent )
        : base( parent )
    {
    }

    /// <summary>
    /// ElementPosition constructor.
    /// </summary>
    /// <param name="x">X position.</param>
    /// <param name="y">Y position.</param>
    /// <param name="width">Width.</param>
    /// <param name="height">Height.</param>
    public ElementPosition( float x, float y, float width, float height )
    {
        this._auto = false;
        this._x = x;
        this._y = y;
        this._width = width;
        this._height = height;
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Asks the user at design-time if he wants to change the Auto position
    /// of all areas at the same time.
    /// </summary>
    /// <param name="autoValue">Value to be set for the Auto property.</param>
    private void ResetAllAreasAutoPosition( bool autoValue )
    {
        if ( this.resetAreaAutoPosition )
        {
            // Proceed only if at design time
            if ( this.Chart != null && this.Chart.IsDesignMode() && !this.Chart.serializing && this.Chart.Site is not null )
            {
                // Check if there is more than one area and Auto position set to the same value
                if ( this.Chart.ChartAreas.Count > 1 )
                {
                    bool firstAutoValue = this.Chart.ChartAreas[0].Position.Auto;
                    bool sameAutoValue = true;
                    foreach ( ChartArea area in this.Chart.ChartAreas )
                    {
                        if ( area.Position.Auto != firstAutoValue )
                        {
                            sameAutoValue = false;
                            break;
                        }
                    }

                    // Proceed only all Auto values are the same
                    if ( sameAutoValue )
                    {
                        string message = SR.MessageChangingChartAreaPositionProperty;
                        if ( autoValue )
                        {
                            message += SR.MessageChangingChartAreaPositionConfirmAutomatic;
                        }
                        else
                        {
                            message += SR.MessageChangingChartAreaPositionConfirmCustom;
                        }


                        if ( this.Chart.Site.GetService( typeof( IDesignerMessageBoxDialog ) ) is IDesignerMessageBoxDialog confirm && confirm.ShowQuestion( message ) )
                        {
                            foreach ( ChartArea area in this.Chart.ChartAreas )
                            {
                                if ( autoValue )
                                {
                                    this.SetPositionNoAuto( 0f, 0f, 0f, 0f );
                                }
                                area.Position._auto = autoValue;
                            }

                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Convert element position into RectangleF
    /// </summary>
    /// <returns>RectangleF structure.</returns>
    public RectangleF ToRectangleF()
    {
        return new RectangleF( this._x, this._y, this._width, this._height );
    }

    /// <summary>
    /// Initializes ElementPosition from RectangleF
    /// </summary>
    /// <param name="rect">RectangleF structure.</param>
    public void FromRectangleF( RectangleF rect )
    {
        this._x = rect.X;
        this._y = rect.Y;
        this._width = rect.Width;
        this._height = rect.Height;
        this._auto = false;
    }

    /// <summary>
    /// Gets the size of the ElementPosition object.
    /// </summary>
    /// <returns>The size of the ElementPosition object.</returns>
    [Browsable( false )]
    [Utilities.SerializationVisibility( Utilities.SerializationVisibility.Hidden )]
    public SizeF Size => new( this._width, this._height );

    /// <summary>
    /// Gets the bottom position in relative coordinates.
    /// </summary>
    /// <returns>Bottom position.</returns>
    [Browsable( false )]
    [Utilities.SerializationVisibility( Utilities.SerializationVisibility.Hidden )]
    public float Bottom => this._y + this._height;

    /// <summary>
    /// Gets the right position in relative coordinates.
    /// </summary>
    /// <returns>Right position.</returns>
    [Browsable( false )]
    [Utilities.SerializationVisibility( Utilities.SerializationVisibility.Hidden )]
    public float Right => this._x + this._width;

    /// <summary>
    /// Determines whether the specified Object is equal to the current Object.
    /// </summary>
    /// <param name="obj">The Object to compare with the current Object.</param>
    /// <returns>true if the specified Object is equal to the current Object; otherwise, false.</returns>
    internal override bool EqualsInternal( object obj )
    {
        if ( obj is ElementPosition pos )
        {
            if ( this._auto && this._auto == pos._auto )
            {
                return true;
            }
            else if ( this._x == pos._x && this._y == pos._y &&
                    this._width == pos._width && this._height == pos._height )
            {
                return true;
            }

        }
        return false;
    }

    /// <summary>
    /// Returns a string that represents the element position data.
    /// </summary>
    /// <returns>Element position data as a string.</returns>
    internal override string ToStringInternal()
    {
        string posString = Constants.AutoValue;
        if ( !this._auto )
        {
            posString =
                this._x.ToString( System.Globalization.CultureInfo.CurrentCulture ) + ", " +
                this._y.ToString( System.Globalization.CultureInfo.CurrentCulture ) + ", " +
                this._width.ToString( System.Globalization.CultureInfo.CurrentCulture ) + ", " +
                this._height.ToString( System.Globalization.CultureInfo.CurrentCulture );
        }
        return posString;
    }

    /// <summary>
    /// Set the element position without modifying the "Auto" property
    /// </summary>
    /// <param name="x">X position.</param>
    /// <param name="y">Y position.</param>
    /// <param name="width">Width.</param>
    /// <param name="height">Height.</param>
    internal void SetPositionNoAuto( float x, float y, float width, float height )
    {
        bool oldValue = this._auto;
        this._x = x;
        this._y = y;
        this._width = width;
        this._height = height;
        this._auto = oldValue;
    }

    #endregion

    #region " element position properties "

    /// <summary>
    /// X position of element.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    DefaultValue( 0.0F ),
    SRDescription( "DescriptionAttributeElementPosition_X" ),
    NotifyParentProperty( true ),
    RefreshProperties( RefreshProperties.All )
    ]
    public float X
    {
        get => this._x;
        set
        {
            if ( value is < ( float ) 0.0 or > ( float ) 100.0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionElementPositionArgumentOutOfRange );
            }
            this._x = value;
            this.Auto = false;

            // Adjust width
            if ( (this._x + this.Width) > 100 )
            {
                this.Width = 100 - this._x;
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Y position of element.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    DefaultValue( 0.0F ),
    SRDescription( "DescriptionAttributeElementPosition_Y" ),
    NotifyParentProperty( true ),
    RefreshProperties( RefreshProperties.All )]
    public float Y
    {
        get => this._y;
        set
        {
            if ( value is < ( float ) 0.0 or > ( float ) 100.0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionElementPositionArgumentOutOfRange );
            }
            this._y = value;
            this.Auto = false;

            // Adjust heigth
            if ( (this._y + this.Height) > 100 )
            {
                this.Height = 100 - this._y;
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Width of element.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    DefaultValue( 0.0F ),
    SRDescription( "DescriptionAttributeElementPosition_Width" ),
    NotifyParentProperty( true ),
    RefreshProperties( RefreshProperties.All )
    ]
    public float Width
    {
        get => this._width;
        set
        {
            if ( value is < ( float ) 0.0 or > ( float ) 100.0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionElementPositionArgumentOutOfRange );
            }
            this._width = value;
            this.Auto = false;

            // Adjust x
            if ( (this._x + this.Width) > 100 )
            {
                this._x = 100 - this.Width;
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Height of element.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    DefaultValue( 0.0F ),
    SRDescription( "DescriptionAttributeElementPosition_Height" ),
    NotifyParentProperty( true ),
    RefreshProperties( RefreshProperties.All )
    ]
    public float Height
    {
        get => this._height;
        set
        {
            if ( value is < ( float ) 0.0 or > ( float ) 100.0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionElementPositionArgumentOutOfRange );
            }
            this._height = value;
            this.Auto = false;

            // Adjust y
            if ( (this._y + this.Height) > 100 )
            {
                this._y = 100 - this.Height;

            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a flag which indicates whether positioning is on.
    /// </summary>
    [
SRCategory( "CategoryAttributeMisc" ),
Bindable( true ),
DefaultValue( true ),
SRDescription( "DescriptionAttributeElementPosition_auto" ),
NotifyParentProperty( true ),
RefreshProperties( RefreshProperties.All )
]
    public bool Auto
    {
        get => this._auto;
        set
        {
            if ( value != this._auto )
            {
                this.ResetAllAreasAutoPosition( value );

                if ( value )
                {
                    this._x = 0;
                    this._y = 0;
                    this._width = 0;
                    this._height = 0;
                }
                this._auto = value;

                this.Invalidate();
            }
        }
    }

    #endregion
}
/// <summary>
/// Used for invoking windows forms MesageBox dialog.
/// </summary>
internal interface IDesignerMessageBoxDialog
{
    /// <summary>
    /// Shows Yes/No MessageBox.
    /// </summary>
    /// <param name="message">The message.</param>
    /// <returns>
    /// true if user confirms with Yes
    /// </returns>
    bool ShowQuestion( string message );
}
