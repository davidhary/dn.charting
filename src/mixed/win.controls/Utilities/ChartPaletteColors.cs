using System.Drawing;

namespace System.Windows.Forms.DataVisualization.Charting.Utilities;
/// <summary>
/// ChartPaletteColors is a utility class which provides access 
/// to the predefined chart color palettes. These palettes are 
/// used to assign unique colors to different chart series. 
/// For some chart types, like Pie, different colors are applied 
/// on the data point level.
/// 
/// GetPaletteColors method takes a ChartColorPalette enumeration 
/// as a parameter and returns back an array of Colors. Each 
/// palette contains different number of colors but it is a 
/// good practice to keep this number around 15.
/// </summary>
internal static class ChartPaletteColors
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles

    // Fields which store the palette color values
    private static readonly Color[] _colorsGrayScale = InitializeGrayScaleColors();
    private static readonly Color[] _colorsDefault = [
        Color.Green,
        Color.Blue,
        Color.Purple,
        Color.Lime,
        Color.Fuchsia,
        Color.Teal,
        Color.Yellow,
        Color.Gray,
        Color.Aqua,
        Color.Navy,
        Color.Maroon,
        Color.Red,
        Color.Olive,
        Color.Silver,
        Color.Tomato,
        Color.Moccasin
        ];

    private static readonly Color[] _colorsPastel = [
                                                Color.SkyBlue,
                                                Color.LimeGreen,
                                                Color.MediumOrchid,
                                                Color.LightCoral,
                                                Color.SteelBlue,
                                                Color.YellowGreen,
                                                Color.Turquoise,
                                                Color.HotPink,
                                                Color.Khaki,
                                                Color.Tan,
                                                Color.DarkSeaGreen,
                                                Color.CornflowerBlue,
                                                Color.Plum,
                                                Color.CadetBlue,
                                                Color.PeachPuff,
                                                Color.LightSalmon
                                            ];

    private static readonly Color[] _colorsEarth = [
                                               Color.FromArgb(255, 128, 0),
                                               Color.DarkGoldenrod,
                                               Color.FromArgb(192, 64, 0),
                                               Color.OliveDrab,
                                               Color.Peru,
                                               Color.FromArgb(192, 192, 0),
                                               Color.ForestGreen,
                                               Color.Chocolate,
                                               Color.Olive,
                                               Color.LightSeaGreen,
                                               Color.SandyBrown,
                                               Color.FromArgb(0, 192, 0),
                                               Color.DarkSeaGreen,
                                               Color.Firebrick,
                                               Color.SaddleBrown,
                                               Color.FromArgb(192, 0, 0)
                                           ];

    private static readonly Color[] _colorsSemiTransparent = [
                                                Color.FromArgb(150, 255, 0, 0),
                                                Color.FromArgb(150, 0, 255, 0),
                                                Color.FromArgb(150, 0, 0, 255),
                                                Color.FromArgb(150, 255, 255, 0),
                                                Color.FromArgb(150, 0, 255, 255),
                                                Color.FromArgb(150, 255, 0, 255),
                                                Color.FromArgb(150, 170, 120, 20),
                                                Color.FromArgb(80, 255, 0, 0),
                                                Color.FromArgb(80, 0, 255, 0),
                                                Color.FromArgb(80, 0, 0, 255),
                                                Color.FromArgb(80, 255, 255, 0),
                                                Color.FromArgb(80, 0, 255, 255),
                                                Color.FromArgb(80, 255, 0, 255),
                                                Color.FromArgb(80, 170, 120, 20),
                                                Color.FromArgb(150, 100, 120, 50),
                                                Color.FromArgb(150, 40, 90, 150)
                                          ];

    private static readonly Color[] _colorsLight = [
                                               Color.Lavender,
                                               Color.LavenderBlush,
                                               Color.PeachPuff,
                                               Color.LemonChiffon,
                                               Color.MistyRose,
                                               Color.Honeydew,
                                               Color.AliceBlue,
                                               Color.WhiteSmoke,
                                               Color.AntiqueWhite,
                                               Color.LightCyan
                                           ];

    private static readonly Color[] _colorsExcel = [
        Color.FromArgb(153,153,255),
        Color.FromArgb(153,51,102),
        Color.FromArgb(255,255,204),
        Color.FromArgb(204,255,255),
        Color.FromArgb(102,0,102),
        Color.FromArgb(255,128,128),
        Color.FromArgb(0,102,204),
        Color.FromArgb(204,204,255),
        Color.FromArgb(0,0,128),
        Color.FromArgb(255,0,255),
        Color.FromArgb(255,255,0),
        Color.FromArgb(0,255,255),
        Color.FromArgb(128,0,128),
        Color.FromArgb(128,0,0),
        Color.FromArgb(0,128,128),
        Color.FromArgb(0,0,255)];

    private static readonly Color[] _colorsBerry = [
                                              Color.BlueViolet,
                                              Color.MediumOrchid,
                                              Color.RoyalBlue,
                                              Color.MediumVioletRed,
                                              Color.Blue,
                                              Color.BlueViolet,
                                              Color.Orchid,
                                              Color.MediumSlateBlue,
                                              Color.FromArgb(192, 0, 192),
                                              Color.MediumBlue,
                                              Color.Purple
                                          ];

    private static readonly Color[] _colorsChocolate = [
                                              Color.Sienna,
                                              Color.Chocolate,
                                              Color.DarkRed,
                                              Color.Peru,
                                              Color.Brown,
                                              Color.SandyBrown,
                                              Color.SaddleBrown,
                                              Color.FromArgb(192, 64, 0),
                                              Color.Firebrick,
                                              Color.FromArgb(182, 92, 58)
                                          ];

    private static readonly Color[] _colorsFire = [
                                                  Color.Gold,
                                                  Color.Red,
                                                  Color.DeepPink,
                                                  Color.Crimson,
                                                  Color.DarkOrange,
                                                  Color.Magenta,
                                                  Color.Yellow,
                                                  Color.OrangeRed,
                                                  Color.MediumVioletRed,
                                                  Color.FromArgb(221, 226, 33)
                                              ];

    private static readonly Color[] _colorsSeaGreen = [
                                             Color.SeaGreen,
                                             Color.MediumAquamarine,
                                             Color.SteelBlue,
                                             Color.DarkCyan,
                                             Color.CadetBlue,
                                             Color.MediumSeaGreen,
                                             Color.MediumTurquoise,
                                             Color.LightSteelBlue,
                                             Color.DarkSeaGreen,
                                             Color.SkyBlue
                                         ];

    private static readonly Color[] _colorsBrightPastel = [
                                               Color.FromArgb(65, 140, 240),
                                               Color.FromArgb(252, 180, 65),
                                               Color.FromArgb(224, 64, 10),
                                               Color.FromArgb(5, 100, 146),
                                               Color.FromArgb(191, 191, 191),
                                               Color.FromArgb(26, 59, 105),
                                               Color.FromArgb(255, 227, 130),
                                               Color.FromArgb(18, 156, 221),
                                               Color.FromArgb(202, 107, 75),
                                               Color.FromArgb(0, 92, 219),
                                               Color.FromArgb(243, 210, 136),
                                               Color.FromArgb(80, 99, 129),
                                               Color.FromArgb(241, 185, 168),
                                               Color.FromArgb(224, 131, 10),
                                               Color.FromArgb(120, 147, 190)
                                           ];

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructor "

    /// <summary>
    /// Initializes the GrayScale color array
    /// </summary>
    private static Color[] InitializeGrayScaleColors()
    {
        // Define gray scale colors
        Color[] grayScale = new Color[16];
        for ( int i = 0; i < grayScale.Length; i++ )
        {
            int colorValue = 200 - (i * (180 / 16));
            grayScale[i] = Color.FromArgb( colorValue, colorValue, colorValue );
        }

        return grayScale;
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Return array of colors for the specified palette. Number of
    /// colors returned varies depending on the palette selected.
    /// </summary>
    /// <param name="palette">Palette to get the colors for.</param>
    /// <returns>Array of colors.</returns>
    public static Color[] GetPaletteColors( ChartColorPalette palette )
    {
        switch ( palette )
        {
            case ChartColorPalette.None:
                {
                    throw new ArgumentException( SR.ExceptionPaletteIsEmpty );
                }
            case ChartColorPalette.Bright:
                return _colorsDefault;
            case ChartColorPalette.Grayscale:
                return _colorsGrayScale;
            case ChartColorPalette.Excel:
                return _colorsExcel;
            case ChartColorPalette.Pastel:
                return _colorsPastel;
            case ChartColorPalette.Light:
                return _colorsLight;
            case ChartColorPalette.EarthTones:
                return _colorsEarth;
            case ChartColorPalette.SemiTransparent:
                return _colorsSemiTransparent;
            case ChartColorPalette.Berry:
                return _colorsBerry;
            case ChartColorPalette.Chocolate:
                return _colorsChocolate;
            case ChartColorPalette.Fire:
                return _colorsFire;
            case ChartColorPalette.SeaGreen:
                return _colorsSeaGreen;
            case ChartColorPalette.BrightPastel:
                return _colorsBrightPastel;
            default:
                break;
        }
        return null;
    }

    #endregion
}
