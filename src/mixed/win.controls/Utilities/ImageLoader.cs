//
//  Purpose:	ImageLoader utility class loads specified image and 
//              caches it in the memory for the future use.
//


using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Security;
using System.Threading.Tasks;

namespace System.Windows.Forms.DataVisualization.Charting.Utilities;
/// <summary>
/// ImageLoader utility class loads and returns specified image 
/// form the File, URI, Web Request or Chart Resources. 
/// Loaded images are stored in the internal hashtable which 
/// allows to improve performance if image need to be used 
/// several times.
/// </summary>
internal class ImageLoader : IDisposable, IServiceProvider
{
    #region " fields "

    // Image storage
#pragma warning disable IDE1006 // Naming Styles
    private Hashtable _imageData;

    // Reference to the service container
    private readonly IServiceContainer _serviceContainer;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructors and initialization "

    /// <summary>
    /// Default constructor is not accessible.
    /// </summary>
    private ImageLoader()
    {
    }

    /// <summary>
    /// Default public constructor.
    /// </summary>
    /// <param name="container">Service container.</param>
    public ImageLoader( IServiceContainer container ) => this._serviceContainer = container ?? throw new ArgumentNullException( SR.ExceptionImageLoaderInvalidServiceContainer );

    /// <summary>
    /// Returns Image Loader service object
    /// </summary>
    /// <param name="serviceType">Requested service type.</param>
    /// <returns>Image Loader service object.</returns>
    [EditorBrowsable( EditorBrowsableState.Never )]
    object IServiceProvider.GetService( Type serviceType )
    {
        return serviceType == typeof( ImageLoader )
            ? this
            : throw (new ArgumentException( SR.ExceptionImageLoaderUnsupportedType( serviceType.ToString() ) ));
    }

    /// <summary>
    /// Dispose images in the hash table
    /// </summary>
    public void Dispose()
    {
        if ( this._imageData is not null )
        {
            foreach ( DictionaryEntry entry in this._imageData )
            {
                if ( entry.Value is IDisposable disposable )
                {
                    disposable.Dispose();
                }
            }
            this._imageData = null;
            GC.SuppressFinalize( this );
        }
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Loads image from URL. Checks if image already loaded (cached).
    /// </summary>
    /// <param name="imageURL">Image name (FileName, URL, Resource).</param>
    /// <returns>Image object.</returns>
    public Image LoadImage( string imageURL )
    {
        return this.LoadImage( imageURL, true );
    }

    /// <summary>
    /// Loads image from URL. Checks if image already loaded (cached).
    /// </summary>
    /// <param name="imageURL">Image name (FileName, URL, Resource).</param>
    /// <param name="saveImage">True if loaded image should be saved in cache.</param>
    /// <returns>Image object</returns>
    public Image LoadImage( string imageURL, bool saveImage )
    {
        Image image = null;

        // Check if image is defined in the chart image collection
        if ( this._serviceContainer is not null )
        {
            Chart chart = ( Chart ) this._serviceContainer.GetService( typeof( Chart ) );
            if ( chart is not null )
            {
                foreach ( NamedImage namedImage in chart.Images )
                {
                    if ( namedImage.Name == imageURL )
                    {
                        return namedImage.Image;
                    }
                }
            }
        }

        // Create new hash table
        this._imageData ??= new Hashtable( StringComparer.OrdinalIgnoreCase );

        // First check if image with this name already loaded
        if ( this._imageData.Contains( imageURL ) )
        {
            image = ( Image ) this._imageData[imageURL];
        }

        // Try to load image from resource
        if ( image == null )
        {
            try
            {
                // Check if resource class type was specified
                int columnIndex = imageURL.IndexOf( "::", StringComparison.Ordinal );
                if ( columnIndex > 0 )
                {
                    string resourceRootName = imageURL[..columnIndex];
                    string resourceName = imageURL[(columnIndex + 2)..];
                    ResourceManager resourceManager = new( resourceRootName, Assembly.GetExecutingAssembly() );
                    image = ( Image ) resourceManager.GetObject( resourceName );
                }
                else if ( Assembly.GetEntryAssembly() is not null )
                {
                    // Check if resource class type was specified
                    columnIndex = imageURL.IndexOf( ':' );
                    if ( columnIndex > 0 )
                    {
                        string resourceRootName = imageURL[..columnIndex];
                        string resourceName = imageURL[(columnIndex + 1)..];
                        ResourceManager resourceManager = new( resourceRootName, Assembly.GetEntryAssembly() );
                        image = ( Image ) resourceManager.GetObject( resourceName );
                    }
                    else
                    {
                        // Try to load resource from every type defined in entry assembly
                        Assembly entryAssembly = Assembly.GetEntryAssembly();
                        if ( entryAssembly is not null )
                        {
                            foreach ( Type type in entryAssembly.GetTypes() )
                            {
                                ResourceManager resourceManager = new( type );
                                try
                                {
                                    image = ( Image ) resourceManager.GetObject( imageURL );
                                }
                                catch ( ArgumentNullException )
                                {
                                }
                                catch ( MissingManifestResourceException )
                                {
                                }

                                // Check if image was loaded
                                if ( image is not null )
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch ( MissingManifestResourceException )
            {
            }
        }


        // Try to load image using the Web Request
        if ( image == null )
        {
            Uri imageUri = null;
            try
            {
                // Try to create URI directly from image URL (will work in case of absolute URL)
                imageUri = new Uri( imageURL );
            }
            catch ( UriFormatException )
            { }


            // Load image from file or web resource
            if ( imageUri is not null )
            {
                try
                {
                    image = this.GetImageInternal( imageUri ).Result;
                    // WebRequest request = WebRequest.Create( imageUri );
                    // image = System.Drawing.Image.FromStream( request.GetResponse().GetResponseStream() );
                }
                catch ( ArgumentException )
                {
                }
                catch ( NotSupportedException )
                {
                }
                catch ( SecurityException )
                {
                }
            }
        }

        // absolute uri(without Server.MapPath)in web is not allowed. Loading from replative uri Server[Page].MapPath is done above.
        // Try to load as file
        image ??= this.LoadFromFile( imageURL );

        // Error loading image
        if ( image == null )
        {
            throw new ArgumentException( SR.ExceptionImageLoaderIncorrectImageLocation( imageURL ) );
        }

        // Save new image in cache
        if ( saveImage )
        {
            this._imageData[imageURL] = image;
        }

        return image;
    }

    /// <summary>   Gets image internal. </summary>
    /// <remarks>   David, 2022-02-02. </remarks>
    /// <param name="imageUri"> URI of the image. </param>
    /// <returns>   The image internal. </returns>
    private async Task<Image> GetImageInternal( Uri imageUri )
    {
        Net.Http.HttpClient client = new();
        Stream response = await client.GetStreamAsync( imageUri );
        Image image = System.Drawing.Image.FromStream( response );
        return image;
    }
    /// <summary>
    /// Helper function which loads image from file.
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <returns>Loaded image or null.</returns>
    private Image LoadFromFile( string fileName )
    {
        // Try to load image from file
        try
        {
            return System.Drawing.Image.FromFile( fileName );
        }
        catch ( FileNotFoundException )
        {
            return null;
        }
    }

    /// <summary>
    /// Returns the image size taking the image DPI into consideration.
    /// </summary>
    /// <param name="name">Image name (FileName, URL, Resource).</param>
    /// <param name="graphics">Graphics used to calculate the image size.</param>
    /// <param name="size">Calculated size.</param>
    /// <returns>false if it fails to calculate the size, otherwise true.</returns>
    internal bool GetAdjustedImageSize( string name, Graphics graphics, ref SizeF size )
    {
        Image image = this.LoadImage( name );

        if ( image == null )
            return false;

        GetAdjustedImageSize( image, graphics, ref size );

        return true;
    }

    /// <summary>
    /// Returns the image size taking the image DPI into consideration.
    /// </summary>
    /// <param name="image">Image for which to calculate the size.</param>
    /// <param name="graphics">Graphics used to calculate the image size.</param>
    /// <param name="size">Calculated size.</param>
    internal static void GetAdjustedImageSize( Image image, Graphics graphics, ref SizeF size )
    {
        if ( graphics is not null )
        {
            //this will work in case the image DPI is specified, otherwise the image DPI will be assumed to be same as the screen DPI
            size.Width = image.Width * graphics.DpiX / image.HorizontalResolution;
            size.Height = image.Height * graphics.DpiY / image.VerticalResolution;
        }
        else
        {
            size.Width = image.Width;
            size.Height = image.Height;
        }
    }

    /// <summary>
    /// Checks if the image has the same DPI as the graphics object.
    /// </summary>
    /// <param name="image">Image to be checked.</param>
    /// <param name="graphics">Graphics object to be used.</param>
    /// <returns>true if they match, otherwise false.</returns>
    internal static bool DoDpisMatch( Image image, Graphics graphics )
    {
        return graphics.DpiX == image.HorizontalResolution && graphics.DpiY == image.VerticalResolution;
    }

    internal static Image GetScaledImage( Image image, Graphics graphics )
    {
        Bitmap scaledImage = new( image, new Size( ( int ) (image.Width * graphics.DpiX / image.HorizontalResolution),
            ( int ) (image.Height * graphics.DpiY / image.VerticalResolution) ) );

        return scaledImage;
    }

    #endregion
}
