//
//  Purpose:	Utility class that conatins properties and methods
//				for chart printing.
//


using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// Chart printing class.
/// </summary>
public class PrintingManager : IDisposable
{
    #region " private fields "

#pragma warning disable IDE1006 // Naming Styles
    // Reference to the service container
    private readonly IServiceContainer _serviceContainer;

    // Reference to the chart image object
    private ChartImage _chartImage;

    // Chart printing document
    private PrintDocument _printDocument;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructors and service provider methods "

    /// <summary>
    /// Public constructor is unavailable
    /// </summary>
    private PrintingManager()
    {
    }

    /// <summary>
    /// Public constructor
    /// </summary>
    /// <param name="container">Service container reference.</param>
    public PrintingManager( IServiceContainer container ) => this._serviceContainer = container ?? throw new ArgumentNullException( SR.ExceptionInvalidServiceContainer );

    /// <summary>
    /// Returns Printing Manager service object
    /// </summary>
    /// <param name="serviceType">Requested service type.</param>
    /// <returns>Printing Manager sevice object.</returns>
    internal object GetService( Type serviceType )
    {
        return serviceType == typeof( PrintingManager )
            ? this
            : throw (new ArgumentException( SR.ExceptionChartSerializerUnsupportedType( serviceType.ToString() ) ));
    }
    #endregion

    #region " printing properties "
    /// <summary>
    /// Chart printing document.
    /// </summary>
    [
    Bindable( false ),
    SRDescription( "DescriptionAttributePrintingManager_PrintDocument" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    Utilities.SerializationVisibility( Utilities.SerializationVisibility.Hidden )
    ]
    public PrintDocument PrintDocument
    {
        set => this._printDocument = value;
        get
        {
            if ( this._printDocument == null )
            {
                // Create new object
                this._printDocument = new PrintDocument();

                // Hook up to the PrintPage event of the document
                this.PrintDocument.PrintPage += new PrintPageEventHandler( this.Pd_PrintPage );
            }
            return this._printDocument;
        }
    }

    #endregion

    #region " printing methods "

    /// <summary>
    /// Draws chart on the printer graphics.
    /// </summary>
    /// <param name="graphics">Printer graphics.</param>
    /// <param name="position">Position to draw in the graphics.</param>
    public void PrintPaint( Graphics graphics, Rectangle position )
    {
        // Get a reference to the chart image object
        if ( this._chartImage == null && this._serviceContainer is not null )
        {
            this._chartImage = ( ChartImage ) this._serviceContainer.GetService( typeof( ChartImage ) );
        }

        // Draw chart
        if ( this._chartImage is not null )
        {
            // Change chart size to fit the new position
            int oldWidth = this._chartImage.Width;
            int oldHeight = this._chartImage.Height;
            this._chartImage.Width = position.Width;
            this._chartImage.Height = position.Height;

            // Save graphics state.
            GraphicsState transState = graphics.Save();

            // Set required transformation
            graphics.TranslateTransform( position.X, position.Y );

            // Set printing indicator
            this._chartImage.isPrinting = true;

            // Draw chart
            this._chartImage.Paint( graphics, false );

            // Clear printing indicator
            this._chartImage.isPrinting = false;

            // Restore graphics state.
            graphics.Restore( transState );

            // Restore old chart position
            this._chartImage.Width = oldWidth;
            this._chartImage.Height = oldHeight;
        }
    }

    /// <summary>
    /// Shows Page Setup dialog.
    /// </summary>
    public void PageSetup()
    {
        // Create print preview dialog
        PageSetupDialog pageSetupDialog = new()
        {
            // Initialize printing document
            Document = this.PrintDocument
        };

        // Show page setup dialog
        _ = pageSetupDialog.ShowDialog();
    }

    /// <summary>
    /// Print preview the chart.
    /// </summary>
    public void PrintPreview()
    {
        // Create print preview dialog
        PrintPreviewDialog printPreviewDialog = new()
        {
            // Initialize printing document
            Document = this.PrintDocument
        };

        // Show print preview
        _ = printPreviewDialog.ShowDialog();
    }

    /// <summary>
    /// Prints chart.
    /// </summary>
    /// <param name="showPrintDialog">Indicates if printing dialog should be shown.</param>
    public void Print( bool showPrintDialog )
    {
        // Show Print dialog
        if ( showPrintDialog )
        {
            // Create and show Print dialog
            PrintDialog printDialog = new()
            {
                UseEXDialog = true,
                Document = this.PrintDocument
            };
            DialogResult dialogResult = printDialog.ShowDialog();

            // Do not proceed with printing if OK button was not pressed
            if ( dialogResult is not DialogResult.OK and not DialogResult.Yes )
            {
                return;
            }
        }

        // Print chart
        this.PrintDocument.Print();
    }

    /// <summary>
    /// Handles PrintPage event of the document.
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="ev">Event parameters.</param>
    private void Pd_PrintPage( object sender, PrintPageEventArgs ev )
    {
        // Get a reference to the chart image object
        if ( this._chartImage == null && this._serviceContainer is not null )
        {
            this._chartImage = ( ChartImage ) this._serviceContainer.GetService( typeof( ChartImage ) );
        }

        if ( this._chartImage is not null )
        {
            // Save graphics state.
            GraphicsState transState = ev.Graphics.Save();
            try
            {
                Rectangle marginPixel = ev.MarginBounds;
                // Display units mean different thing depending if chart is rendered on the display or printed.
                // Typically pixels for video displays, and 1/100 inch for printers.
                if ( ev.Graphics.PageUnit != GraphicsUnit.Pixel )
                {
                    ev.Graphics.PageUnit = GraphicsUnit.Pixel;
                    marginPixel.X = ( int ) (marginPixel.X * (ev.Graphics.DpiX / 100.0f));
                    marginPixel.Y = ( int ) (marginPixel.Y * (ev.Graphics.DpiY / 100.0f));
                    marginPixel.Width = ( int ) (marginPixel.Width * (ev.Graphics.DpiX / 100.0f));
                    marginPixel.Height = ( int ) (marginPixel.Height * (ev.Graphics.DpiY / 100.0f));
                }
                // Calculate chart position rectangle
                Rectangle chartPosition = new( marginPixel.X, marginPixel.Y, this._chartImage.Width, this._chartImage.Height );

                // Make sure chart corretly fits the margin area
                float chartWidthScale = marginPixel.Width / (( float ) chartPosition.Width);
                float chartHeightScale = marginPixel.Height / (( float ) chartPosition.Height);
                chartPosition.Width = ( int ) (chartPosition.Width * Math.Min( chartWidthScale, chartHeightScale ));
                chartPosition.Height = ( int ) (chartPosition.Height * Math.Min( chartWidthScale, chartHeightScale ));

                // Calculate top left position so that chart is aligned in the center   
                chartPosition.X += (marginPixel.Width - chartPosition.Width) / 2;
                chartPosition.Y += (marginPixel.Height - chartPosition.Height) / 2;

                // Draw chart on the printer graphisc
                this.PrintPaint( ev.Graphics, chartPosition );
            }
            finally
            {
                // Restore graphics state.
                ev.Graphics.Restore( transState );
            }
        }
    }

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose( bool disposing )
    {
        if ( disposing )
        {
            //Free managed resources
            if ( this._printDocument is not null )
            {
                this._printDocument.Dispose();
                this._printDocument = null;
            }
        }
    }

    /// <summary>
    /// Performs freeing, releasing, or resetting managed resources.
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    #endregion
}
