//
//  Purpose:	Keep track of all registered formula module types.
//


using System.Collections;
using System.ComponentModel;

namespace System.Windows.Forms.DataVisualization.Charting.Formulas;
/// <summary>
/// Keep track of all registered formula modules types.
/// </summary>
internal class FormulaRegistry : IServiceProvider
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Storage for all registered formula modules
    internal Hashtable registeredModules = new( StringComparer.OrdinalIgnoreCase );
    private readonly Hashtable _createdModules = new( StringComparer.OrdinalIgnoreCase );
    private readonly ArrayList _modulesNames = [];
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " methods "

    /// <summary>
    /// Formula Registry public constructor
    /// </summary>
    public FormulaRegistry()
    {
    }

    /// <summary>
    /// Adds modules into the registry.
    /// </summary>
    /// <param name="name">Module name.</param>
    /// <param name="moduleType">Module class type.</param>
    public void Register( string name, Type moduleType )
    {
        // First check if module with specified name already registered
        if ( this.registeredModules.Contains( name ) )
        {
            // If same type provided - ignore
            if ( this.registeredModules[name].GetType() == moduleType )
            {
                return;
            }

            // Error - throw exception
            throw new ArgumentException( SR.ExceptionFormulaModuleNameIsNotUnique( name ) );
        }

        // Add Module Name
        _ = this._modulesNames.Add( name );

        // Make sure that specified class support IFormula interface
        bool found = false;
        Type[] interfaces = moduleType.GetInterfaces();
        foreach ( Type type in interfaces )
        {
            if ( type == typeof( IFormula ) )
            {
                found = true;
                break;
            }
        }
        if ( !found )
        {
            throw new ArgumentException( SR.ExceptionFormulaModuleHasNoInterface );
        }

        // Add formula module to the hash table
        this.registeredModules[name] = moduleType;
    }

    /// <summary>
    /// Returns formula module registry service object.
    /// </summary>
    /// <param name="serviceType">Service AxisName.</param>
    /// <returns>Service object.</returns>
    [EditorBrowsable( EditorBrowsableState.Never )]
    object IServiceProvider.GetService( Type serviceType )
    {
        return serviceType == typeof( FormulaRegistry )
            ? this
            : throw (new ArgumentException( SR.ExceptionFormulaModuleRegistryUnsupportedType( serviceType.ToString() ) ));
    }

    /// <summary>
    /// Returns formula module object by name.
    /// </summary>
    /// <param name="name">Formula Module name.</param>
    /// <returns>Formula module object derived from IFormula.</returns>
    public IFormula GetFormulaModule( string name )
    {
        // First check if formula module with specified name registered
        if ( !this.registeredModules.Contains( name ) )
        {
            throw new ArgumentException( SR.ExceptionFormulaModuleNameUnknown( name ) );
        }

        // Check if the formula module object is already created
        if ( !this._createdModules.Contains( name ) )
        {
            // Create formula module object
            this._createdModules[name] =
                (( Type ) this.registeredModules[name]).Assembly.
                CreateInstance( (( Type ) this.registeredModules[name]).ToString() );
        }

        return ( IFormula ) this._createdModules[name];
    }

    /// <summary>
    /// Returns the name of the module.
    /// </summary>
    /// <param name="index">Module index.</param>
    /// <returns>Module Name.</returns>
    public string GetModuleName( int index )
    {
        return ( string ) this._modulesNames[index];
    }

    #endregion

    #region " properties "

    /// <summary>
    /// Return the number of registered modules.
    /// </summary>
    public int Count => this._modulesNames.Count;

    #endregion
}
/// <summary>
/// Interface which defines the set of standard methods and
/// properties for each formula module
/// </summary>
internal interface IFormula
{
    #region " iformula properties and methods "

    /// <summary>
    /// Formula Module name
    /// </summary>
    string Name { get; }

    /// <summary>
    /// The first method in the module, which converts a formula 
    /// name to the corresponding private method.
    /// </summary>
    /// <param name="formulaName">String which represent a formula name</param>
    /// <param name="inputValues">Arrays of doubles - Input values</param>
    /// <param name="outputValues">Arrays of doubles - Output values</param>
    /// <param name="parameterList">Array of strings - Formula parameters</param>
    /// <param name="extraParameterList">Array of strings - Extra Formula parameters from DataManipulator object</param>
    /// <param name="outLabels">Array of strings - Used for Labels. Description for output results.</param>
    void Formula( string formulaName, double[][] inputValues, out double[][] outputValues, string[] parameterList, string[] extraParameterList, out string[][] outLabels );

    #endregion
}

