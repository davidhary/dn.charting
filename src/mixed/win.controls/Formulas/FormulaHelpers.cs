
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace System.Windows.Forms.DataVisualization.Charting.Formulas;

#region " class formulahelper "
/// <summary>
/// Formula helper is a static utility class implementing common formula related routines.
/// </summary>
internal static class FormulaHelper
{
    #region " static "
    /// <summary>
    /// Gets the formula info instance.
    /// </summary>
    /// <param name="formula">The formula.</param>
    /// <returns>FormulaInfo instance</returns>
    internal static FormulaInfo GetFormulaInfo( FinancialFormula formula )
    {
        switch ( formula )
        {
            //Price indicators
            case FinancialFormula.MovingAverage:
                return new MovingAverageFormulaInfo();
            case FinancialFormula.ExponentialMovingAverage:
                return new ExponentialMovingAverageFormulaInfo();
            case FinancialFormula.WeightedMovingAverage:
                return new WeightedMovingAverageFormulaInfo();
            case FinancialFormula.TriangularMovingAverage:
                return new TriangularMovingAverageFormulaInfo();
            case FinancialFormula.TripleExponentialMovingAverage:
                return new TripleExponentialMovingAverageFormulaInfo();
            case FinancialFormula.BollingerBands:
                return new BollingerBandsFormulaInfo();
            case FinancialFormula.TypicalPrice:
                return new TypicalPriceFormulaInfo();
            case FinancialFormula.WeightedClose:
                return new WeightedCloseFormulaInfo();
            case FinancialFormula.MedianPrice:
                return new MedianPriceFormulaInfo();
            case FinancialFormula.Envelopes:
                return new EnvelopesFormulaInfo();
            case FinancialFormula.StandardDeviation:
                return new StandardDeviationFormulaInfo();

            // Oscilators
            case FinancialFormula.ChaikinOscillator:
                return new ChaikinOscillatorFormulaInfo();
            case FinancialFormula.DetrendedPriceOscillator:
                return new DetrendedPriceOscillatorFormulaInfo();
            case FinancialFormula.VolatilityChaikins:
                return new VolatilityChaikinsFormulaInfo();
            case FinancialFormula.VolumeOscillator:
                return new VolumeOscillatorFormulaInfo();
            case FinancialFormula.StochasticIndicator:
                return new StochasticIndicatorFormulaInfo();
            case FinancialFormula.WilliamsR:
                return new WilliamsRFormulaInfo();

            // General technical indicators
            case FinancialFormula.AverageTrueRange:
                return new AverageTrueRangeFormulaInfo();
            case FinancialFormula.EaseOfMovement:
                return new EaseOfMovementFormulaInfo();
            case FinancialFormula.MassIndex:
                return new MassIndexFormulaInfo();
            case FinancialFormula.Performance:
                return new PerformanceFormulaInfo();
            case FinancialFormula.RateOfChange:
                return new RateOfChangeFormulaInfo();
            case FinancialFormula.RelativeStrengthIndex:
                return new RelativeStrengthIndexFormulaInfo();
            case FinancialFormula.MovingAverageConvergenceDivergence:
                return new MovingAverageConvergenceDivergenceFormulaInfo();
            case FinancialFormula.CommodityChannelIndex:
                return new CommodityChannelIndexFormulaInfo();

            // Forecasting
            case FinancialFormula.Forecasting:
                return new ForecastingFormulaInfo();

            // Volume Indicators
            case FinancialFormula.MoneyFlow:
                return new MoneyFlowFormulaInfo();
            case FinancialFormula.PriceVolumeTrend:
                return new PriceVolumeTrendFormulaInfo();
            case FinancialFormula.OnBalanceVolume:
                return new OnBalanceVolumeFormulaInfo();
            case FinancialFormula.NegativeVolumeIndex:
                return new NegativeVolumeIndexFormulaInfo();
            case FinancialFormula.PositiveVolumeIndex:
                return new PositiveVolumeIndexFormulaInfo();
            case FinancialFormula.AccumulationDistribution:
                return new AccumulationDistributionFormulaInfo();

            default:
                Debug.Fail( string.Format( CultureInfo.InvariantCulture, "{0} case is not defined", formula ) );
                return null;
        }
    }

    /// <summary>
    /// Gets the data fields of the specified chart type.
    /// </summary>
    /// <param name="chartType">Type of the chart.</param>
    /// <returns>Data fields</returns>
    internal static IList<DataField> GetDataFields( SeriesChartType chartType )
    {
        switch ( chartType )
        {
            case SeriesChartType.BoxPlot:
                return [
                    DataField.LowerWisker, DataField.UpperWisker,
                    DataField.LowerBox, DataField.UpperBox,
                    DataField.Average, DataField.Median ];
            case SeriesChartType.Bubble:
                return [
                    DataField.Bubble, DataField.BubbleSize ];
            case SeriesChartType.Candlestick:
            case SeriesChartType.Stock:
                return [
                    DataField.High, DataField.Low,
                    DataField.Open, DataField.Close ];
            case SeriesChartType.ErrorBar:
                return [
                    DataField.Center,
                    DataField.LowerError, DataField.UpperError];
            case SeriesChartType.RangeBar:
            case SeriesChartType.Range:
            case SeriesChartType.RangeColumn:
            case SeriesChartType.SplineRange:
                return [
                    DataField.Top, DataField.Bottom ];
            default:
                return [DataField.Y];
        }
    }

    /// <summary>
    /// Gets the default type of the chart associated with this field name.
    /// </summary>
    /// <param name="field">The field.</param>
    /// <returns></returns>
    internal static SeriesChartType GetDefaultChartType( DataField field )
    {
        switch ( field )
        {
            default:
            case DataField.Y:
                return SeriesChartType.Line;
            case DataField.LowerWisker:
            case DataField.UpperWisker:
            case DataField.LowerBox:
            case DataField.UpperBox:
            case DataField.Average:
            case DataField.Median:
                return SeriesChartType.BoxPlot;
            case DataField.Bubble:
            case DataField.BubbleSize:
                return SeriesChartType.Bubble;
            case DataField.High:
            case DataField.Low:
            case DataField.Open:
            case DataField.Close:
                return SeriesChartType.Stock;
            case DataField.Center:
            case DataField.LowerError:
            case DataField.UpperError:
                return SeriesChartType.ErrorBar;
            case DataField.Top:
            case DataField.Bottom:
                return SeriesChartType.Range;
        }
    }

    /// <summary>
    /// Maps formula data field to a chart type specific data field. 
    /// </summary>
    /// <param name="chartType">Type of the chart.</param>
    /// <param name="formulaField">The formula field to be mapped.</param>
    /// <returns>The series field</returns>
    [Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    [Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0066:Convert switch statement to expression", Justification = "<Pending>" )]
    internal static DataField? MapFormulaDataField( SeriesChartType chartType, DataField formulaField )
    {
        switch ( formulaField )
        {
            case DataField.Top:
            case DataField.High:
                switch ( chartType )
                {
                    default: return null;
                    case SeriesChartType.BoxPlot: return DataField.UpperBox;
                    case SeriesChartType.Candlestick:
                    case SeriesChartType.Stock: return DataField.High;
                    case SeriesChartType.ErrorBar: return DataField.UpperError;
                    case SeriesChartType.RangeBar:
                    case SeriesChartType.Range:
                    case SeriesChartType.RangeColumn:
                    case SeriesChartType.SplineRange: return DataField.Top;
                }

            case DataField.Bottom:
            case DataField.Low:
                switch ( chartType )
                {
                    default: return null;
                    case SeriesChartType.BoxPlot: return DataField.LowerBox;
                    case SeriesChartType.Candlestick:
                    case SeriesChartType.Stock: return DataField.Low;
                    case SeriesChartType.ErrorBar: return DataField.LowerError;
                    case SeriesChartType.RangeBar:
                    case SeriesChartType.Range:
                    case SeriesChartType.RangeColumn:
                    case SeriesChartType.SplineRange: return DataField.Bottom;
                }

            case DataField.Open:
                switch ( chartType )
                {
                    default: return null;
                    case SeriesChartType.BoxPlot: return DataField.Average;
                    case SeriesChartType.Candlestick:
                    case SeriesChartType.Stock: return DataField.Open;
                    case SeriesChartType.ErrorBar: return DataField.Center;
                    case SeriesChartType.RangeBar:
                    case SeriesChartType.Range:
                    case SeriesChartType.RangeColumn:
                    case SeriesChartType.SplineRange: return DataField.Bottom;
                }

            case DataField.Close:
            case DataField.Y:
                switch ( chartType )
                {
                    default: return DataField.Y;
                    case SeriesChartType.BoxPlot: return DataField.Average;
                    case SeriesChartType.Bubble: return DataField.Bubble;
                    case SeriesChartType.Candlestick:
                    case SeriesChartType.Stock: return DataField.Close;
                    case SeriesChartType.ErrorBar: return DataField.Center;
                    case SeriesChartType.RangeBar:
                    case SeriesChartType.Range:
                    case SeriesChartType.RangeColumn:
                    case SeriesChartType.SplineRange: return DataField.Top;
                }
            default:
                return null;
        }
    }

    #endregion
}
#endregion

#region " class formulainfo and inherited formulaspecific classes "
/// <summary>
/// This a base class of the formula metainfo classes.
/// </summary>
internal abstract class FormulaInfo
{
    #region " fields "
    #endregion

    #region " properties "
    /// <summary>
    /// Gets the input data fields of the formula.
    /// </summary>
    /// <value>The input fields.</value>
    public DataField[] InputFields { get; private set; }

    /// <summary>
    /// Gets the output data fields of the formula.
    /// </summary>
    /// <value>The output fields.</value>
    public DataField[] OutputFields { get; private set; }

    /// <summary>
    /// Gets the parameters of the formula.
    /// </summary>
    /// <value>The parameters.</value>
    public object[] Parameters { get; private set; }
    #endregion

    #region " constructtion "
    /// <summary>
    /// Initializes a new instance of the <see cref="FormulaInfo"/> class.
    /// </summary>
    /// <param name="inputFields">The input data fields.</param>
    /// <param name="outputFields">The output data fields.</param>
    /// <param name="defaultParams">The default formula params.</param>
    public FormulaInfo( DataField[] inputFields, DataField[] outputFields, params object[] defaultParams )
    {
        this.InputFields = inputFields;
        this.OutputFields = outputFields;
        this.Parameters = defaultParams;
    }
    #endregion

    #region " methods "
    /// <summary>
    /// Saves the formula parameters to a string.
    /// </summary>
    /// <returns>Csv string with parameters</returns>
    internal virtual string SaveParametersToString()
    {
        StringBuilder sb = new();
        for ( int i = 0; i < this.Parameters.Length; i++ )
        {
            if ( i > 0 ) _ = sb.Append( ',' );
            _ = sb.AppendFormat( CultureInfo.InvariantCulture, "{0}", this.Parameters[i] );
        }
        return sb.ToString();
    }

    /// <summary>
    /// Loads the formula parameters from string.
    /// </summary>
    /// <param name="parameters">Csv string with parameters.</param>
    internal virtual void LoadParametersFromString( string parameters )
    {
        if ( string.IsNullOrEmpty( parameters ) )
            return;

        string[] paramStringList = parameters.Split( ',' );
        int paramStringIndex = 0;
        for ( int i = 0; i < this.Parameters.Length && paramStringIndex < paramStringList.Length; i++ )
        {
            string newParamValue = paramStringList[paramStringIndex++];
            if ( !string.IsNullOrEmpty( newParamValue ) )
            {
                this.Parameters[i] = this.ParseParameter( i, newParamValue );
            }
        }
    }

    /// <summary>
    /// Parses the formula parameter.
    /// </summary>
    /// <param name="index">The param index.</param>
    /// <param name="newParamValue">The parameter value string.</param>
    /// <returns>Parameter value.</returns>
    internal virtual object ParseParameter( int index, string newParamValue )
    {
        object param = this.Parameters[index];
        if ( param is int )
        {
            return Convert.ToInt32( newParamValue, CultureInfo.InvariantCulture );
        }
        else if ( param is bool )
        {
            return Convert.ToBoolean( newParamValue, CultureInfo.InvariantCulture );
        }
        else if ( param is double )
        {
            return Convert.ToDouble( newParamValue, CultureInfo.InvariantCulture );
        }
        return null;
    }

    /// <summary>
    /// Checks the formula parameter string.
    /// </summary>
    /// <param name="parameters">The parameters.</param>
    internal virtual void CheckParameterString( string parameters )
    {
        if ( string.IsNullOrEmpty( parameters ) )
            return;

        string[] paramStringList = parameters.Split( ',' );
        int paramStringIndex = 0;
        for ( int i = 0; i < this.Parameters.Length && paramStringIndex < paramStringList.Length; i++ )
        {
            string newParamValue = paramStringList[paramStringIndex++];
            if ( !string.IsNullOrEmpty( newParamValue ) )
            {
                try
                {
                    _ = this.ParseParameter( i, newParamValue );
                }
                catch ( FormatException )
                {
                    throw new ArgumentException( SR.ExceptionFormulaDataFormatInvalid( parameters ) );
                }
            }
        }
    }
    #endregion
}
/// <summary>
/// MovingAverage FormulaInfo
/// </summary>
internal class MovingAverageFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="MovingAverageFormulaInfo"/> class.
    /// </summary>
    public MovingAverageFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="MovingAverageFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    /// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
    public MovingAverageFormulaInfo( int period, bool startFromFirst )
        : base(
            [DataField.Y], //Input fields
            [DataField.Y], //Output fields
            period, startFromFirst )
    {
    }
}
/// <summary>
/// ExponentialMoving AverageFormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ExponentialMovingAverageFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
internal class ExponentialMovingAverageFormulaInfo( int period, bool startFromFirst ) : FormulaInfo(
        [DataField.Y], //Input fields
        [DataField.Y], //Output fields
        period, startFromFirst )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="ExponentialMovingAverageFormulaInfo"/> class.
    /// </summary>
    public ExponentialMovingAverageFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
}
/// <summary>
/// WeightedMovingAverageFormulaInfo
/// </summary>
internal class WeightedMovingAverageFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="WeightedMovingAverageFormulaInfo"/> class.
    /// </summary>
    public WeightedMovingAverageFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="WeightedMovingAverageFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    /// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
    public WeightedMovingAverageFormulaInfo( int period, bool startFromFirst )
       : base(
            [DataField.Y], //Input fields
            [DataField.Y], //Output fields
            period, startFromFirst )
    {
    }
}
/// <summary>
/// TriangularMovingAverage FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="TriangularMovingAverageFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
internal class TriangularMovingAverageFormulaInfo( int period, bool startFromFirst ) : FormulaInfo(
        [DataField.Y], //Input fields
        [DataField.Y], //Output fields
        period, startFromFirst )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="TriangularMovingAverageFormulaInfo"/> class.
    /// </summary>
    public TriangularMovingAverageFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
}
/// <summary>
/// TripleExponentialMovingAverage FormulaInfo
/// </summary>
internal class TripleExponentialMovingAverageFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="TripleExponentialMovingAverageFormulaInfo"/> class.
    /// </summary>
    public TripleExponentialMovingAverageFormulaInfo()
        : this( 12 )                           //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="TripleExponentialMovingAverageFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    public TripleExponentialMovingAverageFormulaInfo( int period )
        : base(
            [DataField.Y], //Input fields
            [DataField.Y], //Output fields
            period )
    {
    }
}
/// <summary>
/// BollingerBands FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="BollingerBandsFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="deviation">The deviation.</param>
/// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
internal class BollingerBandsFormulaInfo( int period, double deviation, bool startFromFirst ) : FormulaInfo(
        [DataField.Y],                        //Input fields
        [DataField.Top, DataField.Bottom],    //Output fields
        period, deviation, startFromFirst )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="BollingerBandsFormulaInfo"/> class.
    /// </summary>
    public BollingerBandsFormulaInfo()
        : this( 3, 2, true )                                          //Defaults
    {
    }
}
/// <summary>
/// TypicalPrice FormulaInfo
/// </summary>
internal class TypicalPriceFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="TypicalPriceFormulaInfo"/> class.
    /// </summary>
    public TypicalPriceFormulaInfo()
        : base(
            [DataField.Close, DataField.High, DataField.Low], //Input fields
            [DataField.Y] )                                    //Output fields
    {
    }
}
/// <summary>
/// WeightedClose FormulaInfo
/// </summary>
internal class WeightedCloseFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="WeightedCloseFormulaInfo"/> class.
    /// </summary>
    public WeightedCloseFormulaInfo()
        : base(
            [DataField.Close, DataField.High, DataField.Low], //Input fields
            [DataField.Y] )                                    //Output fields
    {
    }
}
/// <summary>
/// MedianPrice FormulaInfo
/// </summary>
internal class MedianPriceFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="MedianPriceFormulaInfo"/> class.
    /// </summary>
    public MedianPriceFormulaInfo()
        : base(
            [DataField.High, DataField.Low], //Input fields
            [DataField.Y] )                    //Output fields
    {
    }
}
/// <summary>
/// Envelopes FormulaInfo
/// </summary>
internal class EnvelopesFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="EnvelopesFormulaInfo"/> class.
    /// </summary>
    public EnvelopesFormulaInfo()
        : this( 2, 10, true )                                          //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="EnvelopesFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    /// <param name="shiftPercentage">The shift percentage.</param>
    /// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
    public EnvelopesFormulaInfo( int period, double shiftPercentage, bool startFromFirst )
        : base(
            [DataField.Y],                        //Input fields
            [DataField.Top, DataField.Bottom],    //Output fields
            period, shiftPercentage, startFromFirst )
    {
    }
}
/// <summary>
/// StandardDeviation FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="StandardDeviationFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
internal class StandardDeviationFormulaInfo( int period, bool startFromFirst ) : FormulaInfo(
        [DataField.Y], //Input fields
        [DataField.Y], //Output fields
        period, startFromFirst )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="StandardDeviationFormulaInfo"/> class.
    /// </summary>
    public StandardDeviationFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
}
/// <summary>
/// ChaikinOscillatorFormulaInfo
/// </summary>
internal class ChaikinOscillatorFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="ChaikinOscillatorFormulaInfo"/> class.
    /// </summary>
    public ChaikinOscillatorFormulaInfo()
        : this( 3, 10, false )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="ChaikinOscillatorFormulaInfo"/> class.
    /// </summary>
    /// <param name="shortPeriod">The short period.</param>
    /// <param name="longPeriod">The long period.</param>
    /// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
    public ChaikinOscillatorFormulaInfo( int shortPeriod, int longPeriod, bool startFromFirst )
        : base(
            [DataField.High, DataField.Low, DataField.Close, DataField.Y], //Input fields
            [DataField.Y], //Output fields
            shortPeriod, longPeriod, startFromFirst )
    {
    }
}
/// <summary>
/// DetrendedPriceOscillator FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="DetrendedPriceOscillatorFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="startFromFirst">if set to <c>true</c> [start from first].</param>
internal class DetrendedPriceOscillatorFormulaInfo( int period, bool startFromFirst ) : FormulaInfo(
        [DataField.Y], //Input fields
        [DataField.Y], //Output fields
        period, startFromFirst )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="DetrendedPriceOscillatorFormulaInfo"/> class.
    /// </summary>
    public DetrendedPriceOscillatorFormulaInfo()
        : this( 2, false )                      //Defaults
    {
    }
}
/// <summary>
/// VolatilityChaikins FormulaInfo
/// </summary>
internal class VolatilityChaikinsFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="VolatilityChaikinsFormulaInfo"/> class.
    /// </summary>
    public VolatilityChaikinsFormulaInfo()
        : this( 10, 10 )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="VolatilityChaikinsFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    /// <param name="signalPeriod">The signal period.</param>
    public VolatilityChaikinsFormulaInfo( int period, int signalPeriod )
        : base(
            [DataField.High, DataField.Low], //Input fields
            [DataField.Y], //Output fields
            period, signalPeriod )
    {
    }
}
/// <summary>
/// VolumeOscillator FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="VolumeOscillatorFormulaInfo"/> class.
/// </remarks>
/// <param name="shortPeriod">The short period.</param>
/// <param name="longPeriod">The long period.</param>
/// <param name="percentage">if set to <c>true</c> [percentage].</param>
internal class VolumeOscillatorFormulaInfo( int shortPeriod, int longPeriod, bool percentage ) : FormulaInfo(
        [DataField.Y], //Input fields
        [DataField.Y], //Output fields
        shortPeriod, longPeriod, percentage )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="VolumeOscillatorFormulaInfo"/> class.
    /// </summary>
    public VolumeOscillatorFormulaInfo()
        : this( 5, 10, true )                      //Defaults
    {
    }
}
/// <summary>
/// StochasticIndicatorFormulaInfo
/// </summary>
internal class StochasticIndicatorFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="StochasticIndicatorFormulaInfo"/> class.
    /// </summary>
    public StochasticIndicatorFormulaInfo()
        : this( 10, 10 )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="StochasticIndicatorFormulaInfo"/> class.
    /// </summary>
    /// <param name="periodD">The period D.</param>
    /// <param name="periodK">The period K.</param>
    public StochasticIndicatorFormulaInfo( int periodD, int periodK )
        : base(
            [DataField.High, DataField.Low, DataField.Close], //Input fields
            [DataField.Y, DataField.Y], //Output fields
            periodD, periodK )
    {
    }
}
/// <summary>
/// WilliamsRFormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="WilliamsRFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
internal class WilliamsRFormulaInfo( int period ) : FormulaInfo(
        [DataField.High, DataField.Low, DataField.Close], //Input fields
        [DataField.Y], //Output fields
        period )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="WilliamsRFormulaInfo"/> class.
    /// </summary>
    public WilliamsRFormulaInfo()
        : this( 14 )                      //Defaults
    {
    }
}
/// <summary>
/// AverageTrueRange FormulaInfo
/// </summary>
internal class AverageTrueRangeFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="AverageTrueRangeFormulaInfo"/> class.
    /// </summary>
    public AverageTrueRangeFormulaInfo()
        : this( 14 )                      //Defaults
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="AverageTrueRangeFormulaInfo"/> class.
    /// </summary>
    /// <param name="period">The period.</param>
    public AverageTrueRangeFormulaInfo( int period )
        : base(
            [DataField.High, DataField.Low, DataField.Close], //Input fields
            [DataField.Y], //Output fields
            period )
    {
    }
}
/// <summary>
/// EaseOfMovement FormulaInfo
/// </summary>
internal class EaseOfMovementFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="EaseOfMovementFormulaInfo"/> class.
    /// </summary>
    public EaseOfMovementFormulaInfo()
        : base(
            [DataField.High, DataField.Low, DataField.Close], //Input fields
            [DataField.Y] ) //Output fields
    {
    }
}
/// <summary>
/// MassIndex FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="MassIndexFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
/// <param name="averagePeriod">The average period.</param>
internal class MassIndexFormulaInfo( int period, int averagePeriod ) : FormulaInfo(
        [DataField.High, DataField.Low], //Input fields
        [DataField.Y], //Output fields
        period, averagePeriod )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="MassIndexFormulaInfo"/> class.
    /// </summary>
    public MassIndexFormulaInfo()
        : this( 25, 9 )                      //Defaults
    {
    }
}
/// <summary>
/// Performance FormulaInfo
/// </summary>
internal class PerformanceFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="PerformanceFormulaInfo"/> class.
    /// </summary>
    public PerformanceFormulaInfo()
        : base(
            [DataField.Close], //Input fields
            [DataField.Y] ) //Output fields
    {
    }
}
/// <summary>
/// RateOfChange FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="RateOfChangeFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
internal class RateOfChangeFormulaInfo( int period ) : FormulaInfo(
        [DataField.Close], //Input fields
        [DataField.Y], //Output fields
        period )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="RateOfChangeFormulaInfo"/> class.
    /// </summary>
    public RateOfChangeFormulaInfo()
        : this( 10 )                      //Defaults
    {
    }
}
/// <summary>
/// RelativeStrengthIndex FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="RelativeStrengthIndexFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
internal class RelativeStrengthIndexFormulaInfo( int period ) : FormulaInfo(
        [DataField.Close], //Input fields
        [DataField.Y], //Output fields
        period )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="RelativeStrengthIndexFormulaInfo"/> class.
    /// </summary>
    public RelativeStrengthIndexFormulaInfo()
        : this( 10 )                      //Defaults
    {
    }
}
/// <summary>
/// MovingAverageConvergenceDivergence FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="MovingAverageConvergenceDivergenceFormulaInfo"/> class.
/// </remarks>
/// <param name="shortPeriod">The short period.</param>
/// <param name="longPeriod">The long period.</param>
internal class MovingAverageConvergenceDivergenceFormulaInfo( int shortPeriod, int longPeriod ) : FormulaInfo(
        [DataField.Close], //Input fields
        [DataField.Y], //Output fields
        shortPeriod, longPeriod )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="MovingAverageConvergenceDivergenceFormulaInfo"/> class.
    /// </summary>
    public MovingAverageConvergenceDivergenceFormulaInfo()
        : this( 12, 26 )                      //Defaults
    {
    }
}
/// <summary>
/// CommodityChannelIndex FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="CommodityChannelIndexFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
internal class CommodityChannelIndexFormulaInfo( int period ) : FormulaInfo(
        [DataField.High, DataField.Low, DataField.Close], //Input fields
        [DataField.Y], //Output fields
        period )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="CommodityChannelIndexFormulaInfo"/> class.
    /// </summary>
    public CommodityChannelIndexFormulaInfo()
        : this( 10 )                      //Defaults
    {
    }
}
/// <summary>
/// Forecasting FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ForecastingFormulaInfo"/> class.
/// </remarks>
/// <param name="regressionType">Type of the regression.</param>
/// <param name="polynomialDegree">The polynomial degree.</param>
/// <param name="forecastingPeriod">The forecasting period.</param>
/// <param name="returnApproximationError">if set to <c>true</c> [return approximation error].</param>
/// <param name="returnForecastingError">if set to <c>true</c> [return forecasting error].</param>
internal class ForecastingFormulaInfo( TimeSeriesAndForecasting.RegressionType regressionType, int polynomialDegree, int forecastingPeriod, bool returnApproximationError, bool returnForecastingError ) : FormulaInfo(
        [DataField.Close], //Input fields
        [DataField.Close, DataField.High, DataField.Low], //Output fields
        regressionType, polynomialDegree, forecastingPeriod, returnApproximationError, returnForecastingError )
{
    //Fields

#pragma warning disable IDE1006 // Naming Styles
    private string _parameters;
#pragma warning restore IDE1006 // Naming Styles

    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="ForecastingFormulaInfo"/> class.
    /// </summary>
    public ForecastingFormulaInfo()
        : this( TimeSeriesAndForecasting.RegressionType.Polynomial, 2, 0, true, true )                      //Defaults
    {
    }

    //Methods
    /// <summary>
    /// Loads the formula parameters from string.
    /// </summary>
    /// <param name="parameters">Csv string with parameters.</param>
    internal override void LoadParametersFromString( string parameters )
    {
        this._parameters = parameters;
    }

    /// <summary>
    /// Checks the formula parameter string.
    /// </summary>
    /// <param name="parameters">The parameters.</param>
    internal override void CheckParameterString( string parameters )
    {
        if ( string.IsNullOrEmpty( parameters ) )
            return;

        string[] paramStringList = parameters.Split( ',' );
        int paramStringIndex = 1;
        //Don't check the first param
        for ( int i = 2; i < this.Parameters.Length && paramStringIndex < paramStringList.Length; i++ )
        {
            string newParamValue = paramStringList[paramStringIndex++];
            if ( !string.IsNullOrEmpty( newParamValue ) )
            {
                try
                {
                    _ = this.ParseParameter( i, newParamValue );
                }
                catch ( FormatException )
                {
                    throw new ArgumentException( SR.ExceptionFormulaDataFormatInvalid( parameters ) );
                }
            }
        }
    }

    /// <summary>
    /// Saves the formula parameters to a string.
    /// </summary>
    /// <returns>Csv string with parameters</returns>
    internal override string SaveParametersToString()
    {
        return string.IsNullOrEmpty( this._parameters ) ? this._parameters : "2,0,true,true";
    }
}
/// <summary>
/// MoneyFlow FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="MoneyFlowFormulaInfo"/> class.
/// </remarks>
/// <param name="period">The period.</param>
internal class MoneyFlowFormulaInfo( int period ) : FormulaInfo(
        [DataField.High, DataField.Low, DataField.Close, DataField.Y], //Input fields: High,Low,Close,Volume
        [DataField.Y], //Output fields
        period )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="MoneyFlowFormulaInfo"/> class.
    /// </summary>
    public MoneyFlowFormulaInfo()
        : this( 2 )                      //Defaults
    {
    }
}
/// <summary>
/// PriceVolumeTrend FormulaInfo
/// </summary>
internal class PriceVolumeTrendFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="PriceVolumeTrendFormulaInfo"/> class.
    /// </summary>
    public PriceVolumeTrendFormulaInfo()
        : base(
            [DataField.Close, DataField.Y], //Input=Close,Volume
            [DataField.Y] ) //Output fields
    {
    }
}
/// <summary>
/// OnBalanceVolume FormulaInfo
/// </summary>
internal class OnBalanceVolumeFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="OnBalanceVolumeFormulaInfo"/> class.
    /// </summary>
    public OnBalanceVolumeFormulaInfo()
        : base(
            [DataField.Close, DataField.Y], //Input=Close,Volume
            [DataField.Y] ) //Output fields
    {
    }
}
/// <summary>
/// NegativeVolumeIndex FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="NegativeVolumeIndexFormulaInfo"/> class.
/// </remarks>
/// <param name="startValue">The start value.</param>
internal class NegativeVolumeIndexFormulaInfo( double startValue ) : FormulaInfo(
        [DataField.Close, DataField.Y], //Input=Close,Volume
        [DataField.Y],
        startValue )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="NegativeVolumeIndexFormulaInfo"/> class.
    /// </summary>
    public NegativeVolumeIndexFormulaInfo() //Note about parameters: Start value is mandatory so we don't provide the default
        : this( double.NaN )
    {
    }
}
/// <summary>
/// PositiveVolumeIndex FormulaInfo
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="PositiveVolumeIndexFormulaInfo"/> class.
/// </remarks>
/// <param name="startValue">The start value.</param>
internal class PositiveVolumeIndexFormulaInfo( double startValue ) : FormulaInfo(
        [DataField.Close, DataField.Y], //Input=Close,Volume
        [DataField.Y],
        startValue )
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="PositiveVolumeIndexFormulaInfo"/> class.
    /// </summary>
    public PositiveVolumeIndexFormulaInfo() //Note about parameters: Start value is mandatory so we don't provide the default
        : this( double.NaN )
    {
    }
}
/// <summary>
/// AccumulationDistribution FormulaInfo
/// </summary>
internal class AccumulationDistributionFormulaInfo : FormulaInfo
{
    //Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="AccumulationDistributionFormulaInfo"/> class.
    /// </summary>
    public AccumulationDistributionFormulaInfo() //Note about parameters: Start value is mandatory so we don't provide the default
        : base(
            [DataField.High, DataField.Low, DataField.Close, DataField.Y], //Input=High, Low, Close, Volume
            [DataField.Y] ) //Output fields
    {
    }
}

#endregion

#region " enum datafield "
/// <summary>
/// Chart data fields
/// </summary>
internal enum DataField
{
    X,
    Y,
    LowerWisker,
    UpperWisker,
    LowerBox,
    UpperBox,
    Average,
    Median,
    Bubble,
    BubbleSize,
    High,
    Low,
    Open,
    Close,
    Center,
    LowerError,
    UpperError,
    Top,
    Bottom
}
#endregion

#region " class seriesfieldinfo "
/// <summary>
/// SeriesFieldInfo class is a OO representation formula input/output data params ("Series1:Y2")
/// </summary>
internal class SeriesFieldInfo
{
    #region " fields "
#pragma warning disable IDE1006 // Naming Styles
    private readonly string _seriesName;
#pragma warning restore IDE1006 // Naming Styles
    #endregion

    #region " properties "
    /// <summary>
    /// Gets the series.
    /// </summary>
    /// <value>The series.</value>
    public Series Series { get; private set; }
    /// <summary>
    /// Gets the name of the series.
    /// </summary>
    /// <value>The name of the series.</value>
    public string SeriesName => this.Series != null ? this.Series.Name : this._seriesName;

    /// <summary>
    /// Gets the data field.
    /// </summary>
    /// <value>The data field.</value>
    public DataField DataField { get; private set; }
    #endregion

    #region " constructtion "
    /// <summary>
    /// Initializes a new instance of the <see cref="SeriesFieldInfo"/> class.
    /// </summary>
    /// <param name="series">The series.</param>
    /// <param name="dataField">The data field.</param>
    public SeriesFieldInfo( Series series, DataField dataField )
    {
        this.Series = series;
        this.DataField = dataField;
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="SeriesFieldInfo"/> class.
    /// </summary>
    /// <param name="seriesName">Name of the series.</param>
    /// <param name="dataField">The data field.</param>
    public SeriesFieldInfo( string seriesName, DataField dataField )
    {
        this._seriesName = seriesName;
        this.DataField = dataField;
    }
    #endregion
}
#endregion

#region " class seriesfieldlist "
/// <summary>
/// SeriesFieldInfo class is a OO representation formula input/output data params ("Series1:Y2,Series2.Y4")
/// </summary>
internal class SeriesFieldList : List<SeriesFieldInfo>
{
    /// <summary>
    /// Returns a <see cref="string"/> that represents the current <see cref="object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="string"/> that represents the current <see cref="object"/>.
    /// </returns>
    public override string ToString()
    {
        StringBuilder sb = new();
        for ( int i = 0; i < this.Count; i++ )
        {
            SeriesFieldInfo info = this[i];

            if ( i > 0 )
                _ = sb.Append( ',' );

            SeriesChartType seriesChartType = info.Series != null ?
                                                    info.Series.ChartType :
                                                    FormulaHelper.GetDefaultChartType( info.DataField );

            IList<DataField> dataFields = FormulaHelper.GetDataFields( seriesChartType );

            int dataFieldIndex = dataFields.IndexOf( info.DataField );
            if ( dataFieldIndex == 0 )
                _ = sb.AppendFormat( CultureInfo.InvariantCulture, "{0}:Y", info.SeriesName ); //The string field descriptor is 1 based ;-(
            else
                _ = sb.AppendFormat( CultureInfo.InvariantCulture, "{0}:Y{1}", info.SeriesName, dataFieldIndex + 1 ); //The string field descriptor is 1 based ;-(
        }
        return sb.ToString();
    }

    //Static
    /// <summary>
    /// Parse the string defining the formula's input/output series and fields.
    /// </summary>
    /// <param name="chart">The chart.</param>
    /// <param name="seriesFields">The series fields list. The series name can be followed by the field names. For example: "Series1:Y,Series1:Y3,Series2:Close"</param>
    /// <param name="formulaFields">The formula fields list.</param>
    /// <returns></returns>
    public static SeriesFieldList FromString( Chart chart, string seriesFields, IList<DataField> formulaFields )
    {
        SeriesFieldList result = [];
        if ( string.IsNullOrEmpty( seriesFields ) )
        {
            return result;
        }

        List<DataField> unmappedFormulaFields = new( formulaFields );

        //Loop through the series/field pairs
        foreach ( string seriesField in seriesFields.Split( ',' ) )
        {
            //Stop processing if all the formula fields are mapped
            if ( unmappedFormulaFields.Count == 0 )
                break;

            //Split a pair into a series + field
            string[] seriesFieldParts = seriesField.Split( ':' );
            if ( seriesFieldParts.Length > 2 )
            {
                throw new ArgumentException( SR.ExceptionFormulaDataFormatInvalid( seriesField ) );
            }

            //Get the series and series fields
            string seriesName = seriesFieldParts[0].Trim();
            Series series = chart.Series.FindByName( seriesName );
            if ( series is not null )
            {
                switch ( seriesFieldParts.Length )
                {
                    case 1: //Only series name is specified: "Series1"
                        AddSeriesFieldInfo( result, series, unmappedFormulaFields );
                        break;
                    case 2: //Series and field names are provided: "Series1:Y3"
                        AddSeriesFieldInfo( result, series, unmappedFormulaFields, seriesFieldParts[1] );
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch ( seriesFieldParts.Length )
                {
                    case 1: //Only series name is specified: "Series1"
                        AddSeriesFieldInfo( result, seriesName, unmappedFormulaFields );
                        break;
                    case 2: //Series and field names are provided: "Series1:Y3"
                        AddSeriesFieldInfo( result, seriesName, unmappedFormulaFields, seriesFieldParts[1] );
                        break;
                    default:
                        break;
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Adds the series field info.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="series">The series.</param>
    /// <param name="unmappedFormulaFields">The unmapped formula fields.</param>
    private static void AddSeriesFieldInfo( SeriesFieldList result, Series series, IList<DataField> unmappedFormulaFields )
    {
        List<DataField> seriesFields = new( FormulaHelper.GetDataFields( series.ChartType ) );

        for ( int i = 0; i < unmappedFormulaFields.Count && seriesFields.Count > 0; )
        {
            DataField formulaField = unmappedFormulaFields[i];
            DataField? seriesField = null;

            // Case 1. Check if the formulaField is valid for this chart type
            if ( seriesFields.Contains( formulaField ) )
            {
                seriesField = formulaField;
            }
            // Case 2. Try to map the formula field to the series field
            seriesField ??= FormulaHelper.MapFormulaDataField( series.ChartType, formulaField );

            // If the seriesField is found - add it to the results
            if ( seriesField is not null )
            {
                result.Add( new SeriesFieldInfo( series, ( DataField ) seriesField ) );
                _ = seriesFields.Remove( formulaField );
                _ = unmappedFormulaFields.Remove( formulaField );
            }
            else
            {
                i++;
            }
        }
    }
    /// <summary>
    /// Adds the series field info.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="series">The series.</param>
    /// <param name="unmappedFormulaFields">The unmapped formula fields.</param>
    /// <param name="seriesFieldId">The series field id.</param>
    private static void AddSeriesFieldInfo( SeriesFieldList result, Series series, IList<DataField> unmappedFormulaFields, string seriesFieldId )
    {
        IList<DataField> seriesFields = FormulaHelper.GetDataFields( series.ChartType );

        DataField? seriesField = null;

        seriesFieldId = seriesFieldId.ToUpperInvariant().Trim();
        if ( seriesFieldId == "Y" )
        {
            seriesField = seriesFields[0];
        }
        else if ( seriesFieldId.StartsWith( "Y", StringComparison.OrdinalIgnoreCase ) )
        {
            if ( int.TryParse( seriesFieldId[1..], out int id ) )
                seriesField = id - 1 < seriesFields.Count
                    ? ( DataField? ) seriesFields[id - 1]
                    : throw (new ArgumentException( SR.ExceptionFormulaYIndexInvalid, seriesFieldId ));
        }
        else
        {
#pragma warning disable CA2263
            seriesField = ( DataField ) Enum.Parse( typeof( DataField ), seriesFieldId, true );
#pragma warning restore CA2263
        }

        // Add the seriesField to the results
        if ( seriesField is not null )
        {
            result.Add( new SeriesFieldInfo( series, ( DataField ) seriesField ) );
            if ( unmappedFormulaFields.Contains( ( DataField ) seriesField ) )
                _ = unmappedFormulaFields.Remove( ( DataField ) seriesField );
            else
                unmappedFormulaFields.RemoveAt( 0 );
        }
        else
        {
            throw new ArgumentException( SR.ExceptionDataPointValueNameInvalid, seriesFieldId );
        }

    }

    /// <summary>
    /// Adds the series field info.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="seriesName">Name of the series.</param>
    /// <param name="unmappedFormulaFields">The unmapped formula fields.</param>
    private static void AddSeriesFieldInfo( SeriesFieldList result, string seriesName, IList<DataField> unmappedFormulaFields )
    {
        SeriesChartType chartType = FormulaHelper.GetDefaultChartType( unmappedFormulaFields[0] );
        List<DataField> seriesFields = new( FormulaHelper.GetDataFields( chartType ) );

        for ( int i = 0; i < unmappedFormulaFields.Count && seriesFields.Count > 0; )
        {
            DataField formulaField = unmappedFormulaFields[i];
            DataField? seriesField = null;

            // Check if the formulaField is valid for this chart type
            if ( seriesFields.Contains( formulaField ) )
            {
                seriesField = formulaField;
            }

            // If the seriesField is found - add it to the results
            if ( seriesField is not null )
            {
                result.Add( new SeriesFieldInfo( seriesName, ( DataField ) seriesField ) );
                _ = seriesFields.Remove( formulaField );
                _ = unmappedFormulaFields.Remove( formulaField );
            }
            else
            {
                i++;
            }
        }
    }
    /// <summary>
    /// Adds the series field info.
    /// </summary>
    /// <param name="result">The result.</param>
    /// <param name="seriesName">Name of the series.</param>
    /// <param name="unmappedFormulaFields">The unmapped formula fields.</param>
    /// <param name="seriesFieldId">The series field id.</param>
    private static void AddSeriesFieldInfo( SeriesFieldList result, string seriesName, IList<DataField> unmappedFormulaFields, string seriesFieldId )
    {
        SeriesChartType chartType = FormulaHelper.GetDefaultChartType( unmappedFormulaFields[0] );
        IList<DataField> seriesFields = FormulaHelper.GetDataFields( chartType );

        //Find the field
        DataField? seriesField = null;

        seriesFieldId = seriesFieldId.ToUpperInvariant().Trim();

        if ( seriesFieldId == "Y" )
        {
            seriesField = seriesFields[0];
        }
        else if ( seriesFieldId.StartsWith( "Y", StringComparison.OrdinalIgnoreCase ) )
        {
            if ( int.TryParse( seriesFieldId[1..], out int seriesFieldIndex ) )
                seriesField = seriesFieldIndex < seriesFields.Count
                    ? ( DataField? ) seriesFields[seriesFieldIndex - 1]
                    : throw (new ArgumentException( SR.ExceptionFormulaYIndexInvalid, seriesFieldId ));
        }
        else
        {
            //Try parse the field name
            try
            {
#pragma warning disable CA2263
                seriesField = ( DataField ) Enum.Parse( typeof( DataField ), seriesFieldId, true );
#pragma warning restore CA2263
            }
            catch ( ArgumentException )
            { }
        }

        if ( seriesField is not null )
        {
            result.Add( new SeriesFieldInfo( seriesName, ( DataField ) seriesField ) );
            _ = unmappedFormulaFields.Remove( ( DataField ) seriesField );
        }
        else
        {
            throw new ArgumentException( SR.ExceptionDataPointValueNameInvalid, seriesFieldId );
        }
    }
}
#endregion

