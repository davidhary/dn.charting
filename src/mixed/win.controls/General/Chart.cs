//
//  Purpose:	This file contains classes, which are used for Image 
//				creation and chart painting. This file has also a 
//				class, which is used for Paint events arguments.
//


using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms.DataVisualization.Charting.Borders3D;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// ChartImage class adds image type and data binding functionality to 
/// the base ChartPicture class.
/// </summary>
internal class ChartImage : ChartPicture
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Private data members, which store properties values
    private int _compression;

    // Chart data source object
    private object _dataSource;

    // Indicates that control was bound to the data source
    internal bool boundToDataSource;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructor "

    /// <summary>
    /// Chart internal constructor.
    /// </summary>
    /// <param name="container">Service container</param>
    internal ChartImage( IServiceContainer container )
        : base( container )
    {
    }

    #endregion // Constructor

    #region " properties "

    /// <summary>
    /// Gets or sets the data source for the Chart object.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeDataSource" ),
    DefaultValue( null ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public object DataSource
    {
        get => this._dataSource;
        set
        {
            if ( this._dataSource != value )
            {
                this._dataSource = value;
                this.boundToDataSource = false;
            }
        }
    }

    /// <summary>
    /// Image compression value
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( 0 ),
    SRDescription( "DescriptionAttributeChartImage_Compression" ),
    ]
    public int Compression
    {
        get => this._compression;
        set
        {
            if ( value is < 0 or > 100 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionChartCompressionInvalid );
            }
            this._compression = value;
        }
    }

    #endregion

    #region " methods "

    #region " image manipulation "

    /// <summary>
    /// Saves image into the metafile stream. 
    /// </summary>
    /// <param name="imageStream">Image stream.</param>
    /// <param name="emfType">Image stream.</param>
    [SecuritySafeCritical]
    public void SaveIntoMetafile( Stream imageStream, EmfType emfType )
    {
        // Check arguments
        if ( imageStream == null ) throw new ArgumentNullException( nameof( imageStream ) );

        // Create temporary Graphics object for metafile
        using Bitmap bitmap = new( this.Width, this.Height );
        using Graphics newGraphics = Graphics.FromImage( bitmap );
        IntPtr hdc = IntPtr.Zero;
        try
        {
            // System.Security.Permissions.SecurityPermission securityPermission = new(System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode);
            // securityPermission.Demand();

            hdc = newGraphics.GetHdc();


            // Create metafile object to record.
            using Metafile metaFile = new(
                imageStream,
                hdc,
                new Rectangle( 0, 0, this.Width, this.Height ),
                MetafileFrameUnit.Pixel,
                emfType );

            // Create graphics object to record metaFile.
            using Graphics metaGraphics = Graphics.FromImage( metaFile );

            // Note: Fix for issue #3674. Some 3D borders shadows may be drawn outside 
            // of image boundaries. This causes issues when generated EMF file 
            // is placed in IE. Image looks shifted down and hot areas do not align.
            if ( this.BorderSkin.SkinStyle != BorderSkinStyle.None )
            {
                metaGraphics.Clip = new Region( new Rectangle( 0, 0, this.Width, this.Height ) );
            }

            // Draw chart in the metafile
            this.ChartGraph.IsMetafile = true;
            this.Paint( metaGraphics, false );
            this.ChartGraph.IsMetafile = false;
        }
        finally
        {
            if ( hdc != IntPtr.Zero )
            {
                newGraphics.ReleaseHdc( hdc );
            }
        }
    }

    public Bitmap GetImage()
    {
        return this.GetImage( 96 );
    }
    /// <summary>
    /// Create Image and draw chart picture
    /// </summary>
    public Bitmap GetImage( float resolution )
    {
        // Create a new bitmap

        Bitmap image = null;

        while ( image == null )
        {
            bool failed = true;
            try
            {
                image = new Bitmap( Math.Max( 1, this.Width ), Math.Max( 1, this.Height ) );
                image.SetResolution( resolution, resolution );
                failed = false;
            }
            catch ( ArgumentException )
            {
                failed = true;
            }
            catch ( OverflowException )
            {
                failed = true;
            }
            catch ( InvalidOperationException )
            {
                failed = true;
            }
            catch ( ExternalException )
            {
                failed = true;
            }

            if ( failed )
            {
                // if failed to create the image, decrease the size and the resolution of the chart
                image = null;
                float newResolution = Math.Max( resolution / 2, 96 );
                this.Width = ( int ) Math.Ceiling( this.Width * newResolution / resolution );
                this.Height = ( int ) Math.Ceiling( this.Height * newResolution / resolution );
                resolution = newResolution;
            }
        }

        // Creates a new Graphics object from the 
        // specified Image object.
        Graphics offScreen = Graphics.FromImage( image );



        Color backGroundColor = this.BackColor != Color.Empty ? this.BackColor : Color.White;

        // Get the page color if border skin is visible.
        if ( this.GetBorderSkinVisibility() &&
            this.BorderSkin.PageColor != Color.Empty )
        {
            backGroundColor = this.BorderSkin.PageColor;
        }

        // draw a rctangle first with the size of the control, this prevent strange behavior when printing in the reporting services,
        // without this rectangle, the printed picture is blurry
        Pen pen = new( backGroundColor );
        offScreen.DrawRectangle( pen, 0, 0, this.Width, this.Height );
        pen.Dispose();

        // Paint the chart
        this.Paint( offScreen, false );

        // Dispose Graphic object
        offScreen.Dispose();

        // Return reference to the image
        return image;
    }

    #endregion // Image Manipulation

    #region " data binding "

    /// <summary>
    /// Checks if the type of the data source is valid.
    /// </summary>
    /// <param name="dataSource">Data source object to test.</param>
    /// <returns>True if valid data source object.</returns>
    internal static bool IsValidDataSource( object dataSource )
    {
        return null != dataSource &&
            (
            dataSource is IEnumerable ||
            dataSource is DataSet ||
            dataSource is DataView ||
            dataSource is DataTable ||
            dataSource is System.Data.SqlClient.SqlCommand ||
            dataSource is System.Data.SqlClient.SqlDataAdapter ||
            // ADDED: for VS2005 compatibility, DT Nov 25, 2005
            dataSource.GetType().GetInterface( "IDataSource" ) != null
            // END ADDED
            );
    }



    /// <summary>
    /// Gets an list of the data source member names.
    /// </summary>
    /// <param name="dataSource">Data source object to get the members for.</param>
    /// <param name="usedForYValue">Indicates that member will be used for Y values.</param>
    /// <returns>List of member names.</returns>
    internal static ArrayList GetDataSourceMemberNames( object dataSource, bool usedForYValue )
    {
        ArrayList names = [];
        if ( dataSource is not null )
        {
            // ADDED: for VS2005 compatibility, DT Nov 25, 2004
            if ( dataSource.GetType().GetInterface( "IDataSource" ) is not null )
            {
                try
                {
                    MethodInfo m = dataSource.GetType().GetMethod( "Select" );
                    if ( m is not null )
                    {
                        if ( m.GetParameters().Length == 1 )
                        {
                            // SQL derived datasource
                            Type selectArgsType = dataSource.GetType().Assembly.GetType( "System.Web.UI.DataSourceSelectArguments", true );
                            ConstructorInfo ci = selectArgsType.GetConstructor( [] );
                            dataSource = m.Invoke( dataSource, [ci.Invoke( [] )] );
                        }
                        else
                        {
                            // object data source
                            dataSource = m.Invoke( dataSource, [] );
                        }
                    }
                }
                catch ( TargetException )
                {
                }
                catch ( TargetInvocationException )
                {
                }
            }
            // END ADDED

            // Check all DataTable based data souces
            DataTable dataTable = null;

            if ( dataSource is DataTable )
            {
                dataTable = ( DataTable ) dataSource;
            }
            else if ( dataSource is DataView )
            {
                dataTable = (( DataView ) dataSource).Table;
            }
            else if ( dataSource is DataSet && (( DataSet ) dataSource).Tables.Count > 0 )
            {
                dataTable = (( DataSet ) dataSource).Tables[0];
            }
            else if ( dataSource is System.Data.SqlClient.SqlDataAdapter )
            {
                dataTable = new DataTable
                {
                    Locale = CultureInfo.CurrentCulture
                };
                dataTable = (( System.Data.SqlClient.SqlDataAdapter ) dataSource).FillSchema( dataTable, SchemaType.Mapped );
            }

            else if ( dataSource is System.Data.SqlClient.SqlDataReader )
            {
                // Add table columns names
                for ( int fieldIndex = 0; fieldIndex < (( System.Data.SqlClient.SqlDataReader ) dataSource).FieldCount; fieldIndex++ )
                {
                    if ( !usedForYValue || (( System.Data.SqlClient.SqlDataReader ) dataSource).GetFieldType( fieldIndex ) != typeof( string ) )
                    {
                        _ = names.Add( (( System.Data.SqlClient.SqlDataReader ) dataSource).GetName( fieldIndex ) );
                    }
                }
            }

            else if ( dataSource is System.Data.SqlClient.SqlCommand )
            {
                System.Data.SqlClient.SqlCommand command = ( System.Data.SqlClient.SqlCommand ) dataSource;
                if ( command.Connection is not null )
                {
                    command.Connection.Open();
                    System.Data.SqlClient.SqlDataReader dataReader = command.ExecuteReader();
                    if ( dataReader.Read() )
                    {
                        for ( int fieldIndex = 0; fieldIndex < dataReader.FieldCount; fieldIndex++ )
                        {
                            if ( !usedForYValue || dataReader.GetFieldType( fieldIndex ) != typeof( string ) )
                            {
                                _ = names.Add( dataReader.GetName( fieldIndex ) );
                            }
                        }
                    }

                    dataReader.Close();
                    command.Connection.Close();
                }
            }

            // Check if DataTable was set
            if ( dataTable is not null )
            {
                // Add table columns names
                foreach ( DataColumn column in dataTable.Columns )
                {
                    if ( !usedForYValue || column.DataType != typeof( string ) )
                    {
                        _ = names.Add( column.ColumnName );
                    }
                }
            }

            else if ( names.Count == 0 && dataSource is ITypedList )
            {
                foreach ( PropertyDescriptor pd in (( ITypedList ) dataSource).GetItemProperties( null ) )
                {
                    if ( !usedForYValue || pd.PropertyType != typeof( string ) )
                    {
                        _ = names.Add( pd.Name );
                    }
                }
            }
            else if ( names.Count == 0 && dataSource is IEnumerable )
            {
                // .Net 2.0 ObjectDataSource processing
                IEnumerator e = (( IEnumerable ) dataSource).GetEnumerator();
                e.Reset();
                _ = e.MoveNext();
                foreach ( PropertyDescriptor pd in TypeDescriptor.GetProperties( e.Current ) )
                {
                    if ( !usedForYValue || pd.PropertyType != typeof( string ) )
                    {
                        _ = names.Add( pd.Name );
                    }

                }
            }



            // Check if list still empty
            if ( names.Count == 0 )
            {
                // Add first column or any data member name
                _ = names.Add( "0" );
            }

        }

        return names;
    }

    /// <summary>
    /// Data binds control to the data source
    /// </summary>
    internal void DataBind()
    {
        // Set bound flag
        this.boundToDataSource = true;

        object dataSource = this.DataSource;
        if ( dataSource is not null )
        {
            // Convert data adapters to command object
            if ( dataSource is System.Data.SqlClient.SqlDataAdapter )
            {
                dataSource = (( System.Data.SqlClient.SqlDataAdapter ) dataSource).SelectCommand;
            }

            // Convert data source to recognizable source for the series
            if ( dataSource is DataSet && (( DataSet ) dataSource).Tables.Count > 0 )
            {
                dataSource = (( DataSet ) dataSource).DefaultViewManager.CreateDataView( (( DataSet ) dataSource).Tables[0] );

            }
            else if ( dataSource is DataTable )
            {
                dataSource = new DataView( ( DataTable ) dataSource );
            }
            else if ( dataSource is System.Data.SqlClient.SqlCommand )
            {
                System.Data.SqlClient.SqlCommand command = ( System.Data.SqlClient.SqlCommand ) dataSource;
                command.Connection.Open();
                System.Data.SqlClient.SqlDataReader dataReader = command.ExecuteReader();

                this.DataBind( dataReader, null );

                dataReader.Close();
                command.Connection.Close();
                return;
            }
            else
            {
                dataSource = dataSource is IList
                    ? dataSource as IList
                    : dataSource is IListSource
                                        ? (( IListSource ) dataSource).ContainsListCollection && (( IListSource ) dataSource).GetList().Count > 0
                                                            ? (( IListSource ) dataSource).GetList()[0] as IEnumerable
                                                            : (( IListSource ) dataSource).GetList()
                                        : dataSource as IEnumerable;
            }

            // Data bind
            this.DataBind( dataSource as IEnumerable, null );
        }
    }

    /// <summary>
    /// Data binds control to the data source
    /// </summary>
    /// <param name="dataSource">Data source to bind to.</param>
    /// <param name="seriesList">List of series to bind.</param>
    internal void DataBind( IEnumerable dataSource, ArrayList seriesList )
    {
        // Data bind series
        if ( dataSource != null && this.Common is not null )
        {
            // ============================
            // If list of series is not provided - bind all of them.
            // ============================
            if ( seriesList == null )
            {
                seriesList = [];
                foreach ( Series series in this.Common.Chart.Series )
                {
                    // note: added for design time data binding
                    if ( this.Common.Chart.IsDesignMode() )
                    {
                        if ( series.YValueMembers.Length > 0 )
                        {
                            _ = seriesList.Add( series );
                        }
                    }
                    else
                    {
                        _ = seriesList.Add( series );
                    }
                }
            }

            // ============================
            // Clear all data points in data bound series
            // ============================
            foreach ( Series series in seriesList )
            {
                if ( series.XValueMember.Length > 0 || series.YValueMembers.Length > 0 )
                {
                    series.Points.Clear();
                }
            }

            // ============================
            // Get and reset data enumerator.
            // ============================
            IEnumerator enumerator = dataSource.GetEnumerator();
            if ( enumerator.GetType() != typeof( System.Data.Common.DbEnumerator ) )
            {
                try
                {
                    enumerator.Reset();
                }
                // Some enumerators may not support Resetting 
                catch ( InvalidOperationException )
                {
                }
                catch ( NotImplementedException )
                {
                }
                catch ( NotSupportedException )
                {
                }
            }


            // ============================
            // Loop through the enumerator.
            // ============================
            bool valueExsists = true;
            bool autoDetectType = true;
            do
            {
                // Move to the next item
                valueExsists = enumerator.MoveNext();

                // Loop through all series 
                foreach ( Series series in seriesList )
                {
                    if ( series.XValueMember.Length > 0 || series.YValueMembers.Length > 0 )
                    {
                        // ============================
                        // Check and convert fields names.
                        // ============================

                        // Convert comma separated field names string to array of names
                        string[] yFieldNames = null;
                        if ( series.YValueMembers.Length > 0 )
                        {
                            yFieldNames = series.YValueMembers.Replace( ",,", "\n" ).Split( ',' );
                            for ( int index = 0; index < yFieldNames.Length; index++ )
                            {
                                yFieldNames[index] = yFieldNames[index].Replace( "\n", "," ).Trim();
                            }
                        }

                        // Double check that a string object is not provided for data binding
                        if ( dataSource is string )
                        {
                            throw new ArgumentException( SR.ExceptionDataBindYValuesToString, nameof( dataSource ) );
                        }

                        // Check number of fields
                        if ( yFieldNames == null || yFieldNames.GetLength( 0 ) > series.YValuesPerPoint )
                        {
                            throw new ArgumentOutOfRangeException( nameof( dataSource ), SR.ExceptionDataPointYValuesCountMismatch( series.YValuesPerPoint.ToString( System.Globalization.CultureInfo.InvariantCulture ) ) );
                        }

                        // ============================
                        // Create new data point.
                        // ============================
                        if ( valueExsists )
                        {
                            // Auto detect values type
                            if ( autoDetectType )
                            {
                                autoDetectType = false;

                                // Make sure Y field is not empty
                                string yField = yFieldNames[0];
                                int fieldIndex = 1;
                                while ( yField.Length == 0 && fieldIndex < yFieldNames.Length )
                                {
                                    yField = yFieldNames[fieldIndex++];
                                }

                                DataPointCollection.AutoDetectValuesType( series, enumerator, series.XValueMember.Trim(), enumerator, yField );
                            }


                            // Create new point
                            DataPoint newDataPoint = new( series );
                            bool emptyValues = false;
                            bool xValueIsNull = false;

                            // ============================
                            // Get new point X and Y values.
                            // ============================
                            object[] yValuesObj = new object[yFieldNames.Length];
                            object xValueObj = null;

                            // Set X to the value provided or use sequence numbers starting with 1
                            if ( series.XValueMember.Length > 0 )
                            {
                                xValueObj = DataPointCollection.ConvertEnumerationItem( enumerator.Current, series.XValueMember.Trim() );
                                if ( xValueObj is DBNull or null )
                                {
                                    xValueIsNull = true;
                                    emptyValues = true;
                                    xValueObj = 0.0;
                                }
                            }

                            if ( yFieldNames.Length == 0 )
                            {
                                yValuesObj[0] = DataPointCollection.ConvertEnumerationItem( enumerator.Current, null );
                                if ( yValuesObj[0] is DBNull or null )
                                {
                                    emptyValues = true;
                                    yValuesObj[0] = 0.0;
                                }
                            }
                            else
                            {
                                for ( int i = 0; i < yFieldNames.Length; i++ )
                                {
                                    if ( yFieldNames[i].Length > 0 )
                                    {
                                        yValuesObj[i] = DataPointCollection.ConvertEnumerationItem( enumerator.Current, yFieldNames[i] );
                                        if ( yValuesObj[i] is DBNull or null )
                                        {
                                            emptyValues = true;
                                            yValuesObj[i] = 0.0;
                                        }
                                    }
                                    else
                                    {
                                        yValuesObj[i] = (( Series ) seriesList[0]).IsYValueDateTime() ? DateTime.Now.Date.ToOADate() : 0.0;
                                    }
                                }
                            }


                            // Add data point if X value is not Null
                            if ( !xValueIsNull )
                            {
                                if ( emptyValues )
                                {
                                    if ( xValueObj is not null )
                                    {
                                        newDataPoint.SetValueXY( xValueObj, yValuesObj );
                                    }
                                    else
                                    {
                                        newDataPoint.SetValueXY( 0, yValuesObj );
                                    }
                                    series.Points.DataPointInit( ref newDataPoint );
                                    newDataPoint.IsEmpty = true;
                                    series.Points.Add( newDataPoint );
                                }
                                else
                                {
                                    if ( xValueObj is not null )
                                    {
                                        newDataPoint.SetValueXY( xValueObj, yValuesObj );
                                    }
                                    else
                                    {
                                        newDataPoint.SetValueXY( 0, yValuesObj );
                                    }
                                    series.Points.DataPointInit( ref newDataPoint );
                                    series.Points.Add( newDataPoint );
                                }
                            }
                            if ( this.Common.Chart.IsDesignMode() )
                            {
                                series["TempDesignData"] = "true";
                            }
                        }
                    }
                }

            } while ( valueExsists );

        }
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    /// <param name="sortAxisLabels">Indicates if points should be sorted by axis labels.</param>
    /// <param name="sortingOrder">Sorting pointSortOrder.</param>
    internal void AlignDataPointsByAxisLabel( bool sortAxisLabels, PointSortOrder sortingOrder )
    {
        // Find series which are attached to the same X axis in the same chart area
        foreach ( ChartArea chartArea in this.ChartAreas )
        {
            // Check if chart area is visible
            if ( chartArea.Visible )

            {
                // Create series list for primary and secondary X axis
                ArrayList chartAreaSeriesPrimary = [];
                ArrayList chartAreaSeriesSecondary = [];
                foreach ( Series series in this.Common.Chart.Series )
                {
                    // Check if series belongs to the chart area
                    if ( series.ChartArea == chartArea.Name )
                    {
                        if ( series.XSubAxisName.Length == 0 )
                        {
                            _ = series.XAxisType == AxisType.Primary ? chartAreaSeriesPrimary.Add( series ) : chartAreaSeriesSecondary.Add( series );
                        }
                    }
                }

                // Align series
                this.AlignDataPointsByAxisLabel( chartAreaSeriesPrimary, sortAxisLabels, sortingOrder );
                this.AlignDataPointsByAxisLabel( chartAreaSeriesSecondary, sortAxisLabels, sortingOrder );
            }
        }
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    /// <param name="seriesList">List of series to align.</param>
    /// <param name="sortAxisLabels">Indicates if points should be sorted by axis labels.</param>
    /// <param name="sortingOrder">Sorting order.</param>
    internal void AlignDataPointsByAxisLabel(
        ArrayList seriesList,
        bool sortAxisLabels,
        PointSortOrder sortingOrder )
    {
        // List is empty
        if ( seriesList.Count == 0 )
        {
            return;
        }

        // Collect information about all points in all series
        bool indexedX = true;
        bool uniqueAxisLabels = true;
        ArrayList axisLabels = [];
        foreach ( Series series in seriesList )
        {
            ArrayList seriesAxisLabels = [];
            foreach ( DataPoint point in series.Points )
            {
                // Check if series has indexed X values
                if ( !series.IsXValueIndexed && point.XValue != 0.0 )
                {
                    indexedX = false;
                    break;
                }

                // Add axis label to the list and make sure it's non-empty and unique
                if ( point.AxisLabel.Length == 0 )
                {
                    uniqueAxisLabels = false;
                    break;
                }
                else if ( seriesAxisLabels.Contains( point.AxisLabel ) )
                {
                    uniqueAxisLabels = false;
                    break;
                }
                else if ( !axisLabels.Contains( point.AxisLabel ) )
                {
                    _ = axisLabels.Add( point.AxisLabel );
                }

                _ = seriesAxisLabels.Add( point.AxisLabel );
            }
        }

        // Sort axis labels
        if ( sortAxisLabels )
        {
            axisLabels.Sort();
            if ( sortingOrder == PointSortOrder.Descending )
            {
                axisLabels.Reverse();
            }
        }

        // All series must be indexed
        if ( !indexedX )
        {
            throw new InvalidOperationException( SR.ExceptionChartDataPointsAlignmentFaild );
        }

        // AxisLabel can't be empty or duplicated
        if ( !uniqueAxisLabels )
        {
            throw new InvalidOperationException( SR.ExceptionChartDataPointsAlignmentFaildAxisLabelsInvalid );
        }

        // Assign unique X values for data points in all series with same axis LabelStyle
        if ( indexedX && uniqueAxisLabels )
        {
            foreach ( Series series in seriesList )
            {
                foreach ( DataPoint point in series.Points )
                {
                    point.XValue = axisLabels.IndexOf( point.AxisLabel ) + 1;
                }

                // Sort points by X value
                series.Sort( PointSortOrder.Ascending, "X" );
            }

            // Make sure ther are no missing points
            foreach ( Series series in seriesList )
            {
                series.IsXValueIndexed = true;
                for ( int index = 0; index < axisLabels.Count; index++ )
                {
                    if ( index >= series.Points.Count ||
                        series.Points[index].XValue != index + 1 )
                    {
                        DataPoint newPoint = new( series )
                        {
                            AxisLabel = ( string ) axisLabels[index],
                            XValue = index + 1
                        };
                        newPoint.YValues[0] = 0.0;
                        newPoint.IsEmpty = true;
                        series.Points.Insert( index, newPoint );
                    }
                }
            }

        }

    }

    /// <summary>
    /// Data bind chart to the table. Series will be automatically added to the chart depending on
    /// the number of unique values in the seriesGroupByField column of the data source.
    /// Data source can be the Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="seriesGroupByField">Name of the field used to group data into series.</param>
    /// <param name="xField">Name of the field for X values.</param>
    /// <param name="yFields">Comma separated name(s) of the field(s) for Y value(s).</param>
    /// <param name="otherFields">Other point properties binding rule in format: PointProperty=Field[{Format}] [,PointProperty=Field[{Format}]]. For example: "Tooltip=Price{C1},Url=WebSiteName".</param>
    /// <param name="sort">Indicates that series should be sorted by group field.</param>
    /// <param name="sortingOrder">Series sorting order by group field.</param>
    internal void DataBindCrossTab(
    IEnumerable dataSource,
    string seriesGroupByField,
    string xField,
    string yFields,
    string otherFields,
    bool sort,
    PointSortOrder sortingOrder )
    {
        // Check arguments
        if ( dataSource == null ) throw new ArgumentNullException( nameof( dataSource ), SR.ExceptionDataPointInsertionNoDataSource );

        if ( dataSource is string )
            throw new ArgumentException( SR.ExceptionDataBindSeriesToString, nameof( dataSource ) );

        if ( string.IsNullOrEmpty( yFields ) )
            throw new ArgumentException( SR.ExceptionChartDataPointsInsertionFailedYValuesEmpty, nameof( yFields ) );

        if ( string.IsNullOrEmpty( seriesGroupByField ) )
            throw new ArgumentException( SR.ExceptionDataBindSeriesGroupByParameterIsEmpty, nameof( seriesGroupByField ) );


        // List of series and group by field values
        ArrayList seriesList = [];
        ArrayList groupByValueList = [];

        // Convert comma separated Y values field names string to array of names
        string[] yFieldNames = null;
        if ( yFields is not null )
        {
            yFieldNames = yFields.Replace( ",,", "\n" ).Split( ',' );
            for ( int index = 0; index < yFieldNames.Length; index++ )
            {
                yFieldNames[index] = yFieldNames[index].Replace( "\n", "," );
            }
        }

        // Convert other fields/properties names to two arrays of names
        string[] otherAttributeNames = null;
        string[] otherFieldNames = null;
        string[] otherValueFormat = null;
        DataPointCollection.ParsePointFieldsParameter(
            otherFields,
            ref otherAttributeNames,
            ref otherFieldNames,
            ref otherValueFormat );


        // Get and reset enumerator
        IEnumerator enumerator = DataPointCollection.GetDataSourceEnumerator( dataSource );
        if ( enumerator.GetType() != typeof( System.Data.Common.DbEnumerator ) )
        {
            try
            {
                enumerator.Reset();
            }
            // Some enumerators may not support Resetting 
            catch ( NotSupportedException )
            {
            }
            catch ( NotImplementedException )
            {
            }
            catch ( InvalidOperationException )
            {
            }

        }

        // Add data points
        bool valueExsist = true;
        object[] yValuesObj = new object[yFieldNames.Length];
        object xValueObj = null;
        bool autoDetectType = true;

        do
        {
            // Move to the next objects in the enumerations
            if ( valueExsist )
            {
                valueExsist = enumerator.MoveNext();
            }

            // Create and initialize data point
            if ( valueExsist )
            {
                // Get value of the group by field
                object groupObj = DataPointCollection.ConvertEnumerationItem(
                    enumerator.Current,
                    seriesGroupByField );

                // Check series group by field and create new series if required
                Series series = null;
                int seriesIndex = groupByValueList.IndexOf( groupObj );
                if ( seriesIndex >= 0 )
                {
                    // Select existing series from the list
                    series = ( Series ) seriesList[seriesIndex];
                }
                else
                {
                    // Create new series
                    series = new Series
                    {
                        YValuesPerPoint = yFieldNames.GetLength( 0 )
                    };

                    // If not the first series in the list copy some properties
                    if ( seriesList.Count > 0 )
                    {
                        series.XValueType = (( Series ) seriesList[0]).XValueType;
                        series.autoXValueType = (( Series ) seriesList[0]).autoXValueType;
                        series.YValueType = (( Series ) seriesList[0]).YValueType;
                        series.autoYValueType = (( Series ) seriesList[0]).autoYValueType;
                    }

                    // Try to set series name based on grouping vlaue
                    series.Name = groupObj is string groupObjStr ? groupObjStr : seriesGroupByField + " - " + groupObj.ToString();


                    // Add series and group value into the lists
                    _ = groupByValueList.Add( groupObj );
                    _ = seriesList.Add( series );
                }


                // Auto detect valu(s) type
                if ( autoDetectType )
                {
                    autoDetectType = false;
                    DataPointCollection.AutoDetectValuesType( series, enumerator, xField, enumerator, yFieldNames[0] );
                }

                // Create new data point
                DataPoint newDataPoint = new( series );
                bool emptyValues = false;

                // Set X to the value provided
                if ( xField.Length > 0 )
                {
                    xValueObj = DataPointCollection.ConvertEnumerationItem( enumerator.Current, xField );
                    if ( DataPointCollection.IsEmptyValue( xValueObj ) )
                    {
                        emptyValues = true;
                        xValueObj = 0.0;
                    }
                }

                // Set Y values
                if ( yFieldNames.Length == 0 )
                {
                    yValuesObj[0] = DataPointCollection.ConvertEnumerationItem( enumerator.Current, null );
                    if ( DataPointCollection.IsEmptyValue( yValuesObj[0] ) )
                    {
                        emptyValues = true;
                        yValuesObj[0] = 0.0;
                    }
                }
                else
                {
                    for ( int i = 0; i < yFieldNames.Length; i++ )
                    {
                        yValuesObj[i] = DataPointCollection.ConvertEnumerationItem( enumerator.Current, yFieldNames[i] );
                        if ( DataPointCollection.IsEmptyValue( yValuesObj[i] ) )
                        {
                            emptyValues = true;
                            yValuesObj[i] = 0.0;
                        }
                    }
                }

                // Set other values
                if ( otherAttributeNames != null &&
                    otherAttributeNames.Length > 0 )
                {
                    for ( int i = 0; i < otherFieldNames.Length; i++ )
                    {
                        // Get object by field name
                        object obj = DataPointCollection.ConvertEnumerationItem( enumerator.Current, otherFieldNames[i] );
                        if ( !DataPointCollection.IsEmptyValue( obj ) )
                        {
                            newDataPoint.SetPointCustomProperty(
                                obj,
                                otherAttributeNames[i],
                                otherValueFormat[i] );
                        }
                    }
                }

                // IsEmpty value was detected
                if ( emptyValues )
                {
                    if ( xValueObj is not null )
                    {
                        newDataPoint.SetValueXY( xValueObj, yValuesObj );
                    }
                    else
                    {
                        newDataPoint.SetValueXY( 0, yValuesObj );
                    }
                    DataPointCollection.DataPointInit( series, ref newDataPoint );
                    newDataPoint.IsEmpty = true;
                    series.Points.Add( newDataPoint );
                }
                else
                {
                    if ( xValueObj is not null )
                    {
                        newDataPoint.SetValueXY( xValueObj, yValuesObj );
                    }
                    else
                    {
                        newDataPoint.SetValueXY( 0, yValuesObj );
                    }
                    DataPointCollection.DataPointInit( series, ref newDataPoint );
                    series.Points.Add( newDataPoint );
                }
            }

        } while ( valueExsist );

        // Sort series usig values of group by field
        if ( sort )
        {
            // Duplicate current list
            ArrayList oldList = ( ArrayList ) groupByValueList.Clone();

            // Sort list 
            groupByValueList.Sort();
            if ( sortingOrder == PointSortOrder.Descending )
            {
                groupByValueList.Reverse();
            }

            // Change order of series in collection
            ArrayList sortedSeriesList = [];
            foreach ( object obj in groupByValueList )
            {
                _ = sortedSeriesList.Add( seriesList[oldList.IndexOf( obj )] );
            }
            seriesList = sortedSeriesList;
        }

        // Add all series from the list into the series collection
        foreach ( Series series in seriesList )
        {
            this.Common.Chart.Series.Add( series );
        }
    }

    /// <summary>
    /// Automatically creates and binds series to specified data table. 
    /// Each column of the table becomes a Y value in a separate series.
    /// Series X value field may also be provided. 
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="xField">Name of the field for series X values.</param>
    internal void DataBindTable(
        IEnumerable dataSource,
        string xField )
    {
        // Check arguments
        if ( dataSource == null ) throw new ArgumentNullException( nameof( dataSource ) );

        // Get list of member names from the data source
        ArrayList dataSourceFields = GetDataSourceMemberNames( dataSource, true );

        // Remove X value field if it's there
        if ( xField != null && xField.Length > 0 )
        {
            int index = -1;
            for ( int i = 0; i < dataSourceFields.Count; i++ )
            {
                if ( string.Equals( ( string ) dataSourceFields[i], xField, StringComparison.OrdinalIgnoreCase ) )
                {
                    index = i;
                    break;
                }
            }
            if ( index >= 0 )
            {
                dataSourceFields.RemoveAt( index );
            }
            else
            {
                // Check if field name passed as index
                bool parseSucceed = int.TryParse( xField, NumberStyles.Any, CultureInfo.InvariantCulture, out index );
                if ( parseSucceed && index >= 0 && index < dataSourceFields.Count )
                {
                    dataSourceFields.RemoveAt( index );
                }
            }
        }

        // Get number of series
        int seriesNumber = dataSourceFields.Count;
        if ( seriesNumber > 0 )
        {
            // Create as many series as fields in the data source
            ArrayList seriesList = [];
            int index = 0;
            foreach ( string fieldName in dataSourceFields )
            {
                Series series = new( fieldName )
                {
                    // Set binding properties
                    YValueMembers = fieldName,
                    XValueMember = xField
                };

                // Add to list
                _ = seriesList.Add( series );
                ++index;
            }


            // Data bind series
            this.DataBind( dataSource, seriesList );

            // Add all series from the list into the series collection
            foreach ( Series series in seriesList )
            {
                // Clear binding properties
                series.YValueMembers = string.Empty;
                series.XValueMember = string.Empty;

                // Add series into the list
                this.Common.Chart.Series.Add( series );
            }
        }
    }

    #endregion // Data Binding

    #endregion
}
/// <summary>
/// ChartPicture class represents chart content like legends, titles, 
/// chart areas and series. It provides methods for positioning and 
/// drawing all chart elements.
/// </summary>
internal class ChartPicture : ChartElement, IServiceProvider
{
    #region " fields "

    /// <summary>
    /// Indicates that chart exceptions should be suppressed.
    /// </summary>

    // Chart Graphic object
    internal ChartGraphics ChartGraph { get; set; }

#pragma warning disable IDE1006 // Naming Styles

    private int _borderWidth = 1;
    private int _width = 300;
    private int _height = 300;
    internal HotRegionsList hotRegionsList;

    // Annotation smart labels class
    internal AnnotationSmartLabel annotationSmartLabel = new();

    // Chart picture events
    internal event EventHandler<ChartPaintEventArgs> BeforePaint;
    internal event EventHandler<ChartPaintEventArgs> AfterPaint;

    // Chart title position rectangle
    private RectangleF _titlePosition = RectangleF.Empty;

    // Element spacing size
    internal const float elementSpacing = 3F;

    // Maximum size of the font in percentage
    internal const float maxTitleSize = 15F;

    // Printing indicator
    internal bool isPrinting;

    // Indicates chart selection mode
    internal bool isSelectionMode;

    // Position of the chart 3D border
    private RectangleF _chartBorderPosition = RectangleF.Empty;

    // Saving As Image indicator
    internal bool isSavingAsImage;

    // Indicates that chart background is restored from the double buffer
    // prior to drawing top level objects like annotations, cursors and selection.
    internal bool backgroundRestored;

    // Buffered image of non-top level chart elements
    internal Bitmap nonTopLevelChartBuffer;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructtion "

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="container">Service container</param>
    public ChartPicture( IServiceContainer container )
    {
        if ( container == null )
        {
            throw new ArgumentNullException( SR.ExceptionInvalidServiceContainer );
        }

        // Create and set Common Elements
        this.Common = new CommonElements( container );
        this.ChartGraph = new ChartGraphics( this.Common );
        this.hotRegionsList = new HotRegionsList( this.Common );

        // Create border properties class
        this.BorderSkin = new BorderSkin( this );

        // Create a collection of chart areas
        this.ChartAreas = new ChartAreaCollection( this );

        // Create a collection of legends
        this.Legends = new LegendCollection( this );

        // Create a collection of titles
        this.Titles = new TitleCollection( this );

        // Create a collection of annotations
        this.Annotations = new AnnotationCollection( this );

        // Set Common elements for data manipulator
        this.DataManipulator.Common = this.Common;
    }

    /// <summary>
    /// Returns Chart service object
    /// </summary>
    /// <param name="serviceType">Service AxisName</param>
    /// <returns>Chart picture</returns>
    [EditorBrowsable( EditorBrowsableState.Never )]
    object IServiceProvider.GetService( Type serviceType )
    {
        return serviceType == typeof( ChartPicture )
            ? this
            : throw (new ArgumentException( SR.ExceptionChartPictureUnsupportedType( serviceType.ToString() ) ));
    }

    #endregion

    #region " painting and selection methods "

    /// <summary>
    /// Performs empty painting.
    /// </summary>
    internal void PaintOffScreen()
    {
        // Check chart size
        // NOTE: Fixes issue #4733
        if ( this.Width <= 0 || this.Height <= 0 )
        {
            return;
        }

        // Set process Mode to hot regions
        this.Common.HotRegionsList.ProcessChartMode |= ProcessMode.HotRegions;
        this.Common.HotRegionsList.hitTestCalled = true;

        // Enable selection mode
        this.isSelectionMode = true;

        // Hot Region list does not exist. Create the list.
        //this.common.HotRegionsList.List = new ArrayList();
        this.Common.HotRegionsList.Clear();

        // Create a new bitmap
        Bitmap image = new( Math.Max( 1, this.Width ), Math.Max( 1, this.Height ) );

        // Creates a new Graphics object from the 
        // specified Image object.
        Graphics offScreen = Graphics.FromImage( image );

        // Connect Graphics object with Chart Graphics object
        this.ChartGraph.Graphics = offScreen;

        // Remember the previous dirty flag
        bool oldDirtyFlag = this.Common.Chart.dirtyFlag;


        this.Paint( this.ChartGraph.Graphics, false );

        image.Dispose();

        // Restore the previous dirty flag
        this.Common.Chart.dirtyFlag = oldDirtyFlag;

        // Disable selection mode
        this.isSelectionMode = false;

        // Set process Mode to hot regions
        this.Common.HotRegionsList.ProcessChartMode |= ProcessMode.HotRegions;

    }

    /// <summary>
    /// Gets text rendering quality.
    /// </summary>
    /// <returns>Text rendering quality.</returns>
    internal TextRenderingHint GetTextRenderingHint()
    {
        TextRenderingHint result = TextRenderingHint.SingleBitPerPixelGridFit;
        if ( (this.AntiAliasing & AntiAliasingStyles.Text) == AntiAliasingStyles.Text )
        {
            result = TextRenderingHint.ClearTypeGridFit;
            if ( this.TextAntiAliasingQuality == TextAntiAliasingQuality.Normal )
            {
                result = TextRenderingHint.AntiAlias;
            }
            else if ( this.TextAntiAliasingQuality == TextAntiAliasingQuality.SystemDefault )
            {
                result = TextRenderingHint.SystemDefault;
            }
        }
        else
        {
            result = TextRenderingHint.SingleBitPerPixelGridFit;
        }

        return result;
    }

    internal bool GetBorderSkinVisibility()
    {
        return this.BorderSkin.SkinStyle != BorderSkinStyle.None && this.Width > 20 && this.Height > 20;
    }

    /// <summary>
    /// This function paints a chart.
    /// </summary>
    /// <param name="graph">The graph provides drawing object to the display device. A Graphics object is associated with a specific device context.</param>
    /// <param name="paintTopLevelElementOnly">Indicates that only chart top level elements like cursors, selection or annotation objects must be redrawn.</param>
    internal void Paint(
        Graphics graph,
        bool paintTopLevelElementOnly )
    {
        // Reset restored and saved backgound flags
        this.backgroundRestored = false;

        // Reset Annotation Smart Labels 
        this.annotationSmartLabel.Reset();

        // Do not draw the control if size is less than 5 pixel
        if ( this.Width < 5 || this.Height < 5 )
        {
            return;
        }

        bool resetHotRegionList = false;

        if (
            this.Common.HotRegionsList.hitTestCalled
            || this.IsToolTipsEnabled()
            )
        {
            this.Common.HotRegionsList.ProcessChartMode = ProcessMode.HotRegions | ProcessMode.Paint;

            this.Common.HotRegionsList.hitTestCalled = false;

            // Clear list of hot regions 
            if ( paintTopLevelElementOnly )
            {
                // If repainting only top level elements (annotations) - 
                // clear top level objects hot regions only
                for ( int index = 0; index < this.Common.HotRegionsList.List.Count; index++ )
                {
                    HotRegion region = ( HotRegion ) this.Common.HotRegionsList.List[index];
                    if ( region.Type == ChartElementType.Annotation )
                    {
                        this.Common.HotRegionsList.List.RemoveAt( index );
                        --index;
                    }
                }
            }
            else
            {
                // If repainting whole chart - clear all hot regions
                resetHotRegionList = true;
            }
        }
        else
        {
            this.Common.HotRegionsList.ProcessChartMode = ProcessMode.Paint;

            // If repainting whole chart - clear all hot regions
            resetHotRegionList = true;
        }

        // Reset hot region list
        if ( resetHotRegionList )
        {
            this.Common.HotRegionsList.Clear();
        }

        // Check if control was data bound
        if ( this is ChartImage chartImage && !chartImage.boundToDataSource )
        {
            if ( this.Common != null && this.Common.Chart != null && !this.Common.Chart.IsDesignMode() )
            {
                this.Common.Chart.DataBind();
            }
        }

        // Connect Graphics object with Chart Graphics object
        this.ChartGraph.Graphics = graph;

        this.Common.graph = this.ChartGraph;

        // Set anti alias mode
        this.ChartGraph.AntiAliasing = this.AntiAliasing;
        this.ChartGraph.softShadows = this.IsSoftShadows;
        this.ChartGraph.TextRenderingHint = this.GetTextRenderingHint();

        try
        {
            // Check if only chart area cursors and annotations must be redrawn
            if ( !paintTopLevelElementOnly )
            {
                // Fire Before Paint event
                this.OnBeforePaint( new ChartPaintEventArgs( this.Chart, this.ChartGraph, this.Common, new ElementPosition( 0, 0, 100, 100 ) ) );

                // Flag indicates that resize method should be called 
                // after adjusting the intervals in 3D charts
                bool resizeAfterIntervalAdjusting = false;

                // RecalculateAxesScale paint chart areas
                foreach ( ChartArea area in this.ChartAreas )
                {
                    // Check if area is visible
                    if ( area.Visible )

                    {
                        area.Set3DAnglesAndReverseMode();
                        area.SetTempValues();
                        area.ReCalcInternal();

                        // Resize should be called the second time
                        if ( area.Area3DStyle.Enable3D && !area.chartAreaIsCurcular )
                        {
                            resizeAfterIntervalAdjusting = true;
                        }
                    }
                }

                // Call Customize event
                this.Common.Chart.CallOnCustomize();

                // Resize picture
                this.Resize( this.ChartGraph, resizeAfterIntervalAdjusting );


                // This code is introduce because labels has to 
                // be changed when scene is rotated.
                bool intervalReCalculated = false;
                foreach ( ChartArea area in this.ChartAreas )
                {
                    if ( area.Area3DStyle.Enable3D &&
                        !area.chartAreaIsCurcular

                        && area.Visible

                        )

                    {
                        // Make correction for interval in 3D space
                        intervalReCalculated = true;
                        area.Estimate3DInterval( this.ChartGraph );
                        area.ReCalcInternal();
                    }
                }

                // Resize chart areas after updating 3D interval
                if ( resizeAfterIntervalAdjusting )
                {
                    // NOTE: Fixes issue #6808.
                    // In 3D chart area interval will be changed to compenstae for the axis rotation angle.
                    // This will cause all standard labels to be changed. We need to call the customize event 
                    // the second time to give user a chance to modify those labels.
                    if ( intervalReCalculated )
                    {
                        // Call Customize event
                        this.Common.Chart.CallOnCustomize();
                    }

                    // Resize chart elements
                    this.Resize( this.ChartGraph );
                }


                // ============================ 
                // Draw chart 3D border
                // ============================ 
                if ( this.GetBorderSkinVisibility() )
                {
                    // Fill rectangle with page color
                    this.ChartGraph.FillRectangleAbs( new RectangleF( 0, 0, this.Width - 1, this.Height - 1 ),
                        this.BorderSkin.PageColor,
                        ChartHatchStyle.None,
                        "",
                        ChartImageWrapMode.Tile,
                        Color.Empty,
                        ChartImageAlignmentStyle.Center,
                        GradientStyle.None,
                        Color.Empty,
                        this.BorderSkin.PageColor,
                        1,
                        ChartDashStyle.Solid,
                        PenAlignment.Inset );

                    // Draw 3D border
                    this.ChartGraph.Draw3DBorderAbs(
                        this.BorderSkin,
                        this._chartBorderPosition,
                        this.BackColor,
                        this.BackHatchStyle,
                        this.BackImage,
                        this.BackImageWrapMode,
                        this.BackImageTransparentColor,
                        this.BackImageAlignment,
                        this.BackGradientStyle,
                        this.BackSecondaryColor,
                        this.BorderColor,
                        this.BorderWidth,
                        this.BorderDashStyle );
                }

                // Paint Background
                else
                {
                    this.ChartGraph.FillRectangleAbs( new RectangleF( 0, 0, this.Width - 1, this.Height - 1 ),
                        this.BackColor,
                        this.BackHatchStyle,
                        this.BackImage,
                        this.BackImageWrapMode,
                        this.BackImageTransparentColor,
                        this.BackImageAlignment,
                        this.BackGradientStyle,
                        this.BackSecondaryColor,
                        this.BorderColor,
                        this.BorderWidth,
                        this.BorderDashStyle,
                        PenAlignment.Inset );
                }

                // Call BackPaint event
                this.Chart.CallOnPrePaint( new ChartPaintEventArgs( this.Chart, this.ChartGraph, this.Common, new ElementPosition( 0, 0, 100, 100 ) ) );

                // Call paint function for each chart area.
                foreach ( ChartArea area in this.ChartAreas )
                {
                    // Check if area is visible
                    if ( area.Visible )

                    {
                        area.Paint( this.ChartGraph );
                    }
                }

                // This code is introduced because of GetPointsInterval method, 
                // which is very time consuming. There is no reason to calculate 
                // interval after painting.
                foreach ( ChartArea area in this.ChartAreas )
                {
                    // Reset interval data
                    area.intervalData = double.NaN;
                }

                // Draw Legends
                foreach ( Legend legendCurrent in this.Legends )
                {
                    legendCurrent.Paint( this.ChartGraph );
                }

                // Draw chart titles from the collection
                foreach ( Title titleCurrent in this.Titles )
                {
                    titleCurrent.Paint( this.ChartGraph );
                }

                // Call Paint event
                this.Chart.CallOnPostPaint( new ChartPaintEventArgs( this.Chart, this.ChartGraph, this.Common, new ElementPosition( 0, 0, 100, 100 ) ) );
            }

            // Draw annotation objects 
            this.Annotations.Paint( this.ChartGraph, paintTopLevelElementOnly );

            // Draw chart areas cursors in all areas.
            // Only if not in selection 
            if ( !this.isSelectionMode )
            {
                foreach ( ChartArea area in this.ChartAreas )
                {
                    // Check if area is visible
                    if ( area.Visible )

                    {
                        area.PaintCursors( this.ChartGraph, paintTopLevelElementOnly );
                    }
                }
            }

            // Return default values
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if area is visible
                if ( area.Visible )

                {
                    area.Restore3DAnglesAndReverseMode();
                    area.GetTempValues();
                }
            }
        }
        catch ( Exception )
        {
            throw;
        }
        finally
        {
            // Fire After Paint event
            this.OnAfterPaint( new ChartPaintEventArgs( this.Chart, this.ChartGraph, this.Common, new ElementPosition( 0, 0, 100, 100 ) ) );

            // Restore temp values for each chart area
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if area is visible
                if ( area.Visible )

                {
                    area.Restore3DAnglesAndReverseMode();
                    area.GetTempValues();
                }
            }
        }
    }

    /// <summary>
    /// Invoke before paint delegates.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected virtual void OnBeforePaint( ChartPaintEventArgs e )
    {
        //Invokes the delegates.
        BeforePaint?.Invoke( this, e );
    }

    /// <summary>
    /// Invoke after paint delegates.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected virtual void OnAfterPaint( ChartPaintEventArgs e )
    {
        //Invokes the delegates.
        AfterPaint?.Invoke( this, e );
    }

    internal override void Invalidate()
    {
        base.Invalidate();

        this.Chart?.Invalidate();
    }
    #endregion

    #region " resizing methods "

    /// <summary>
    /// Resize the chart picture.
    /// </summary>
    /// <param name="chartGraph">Chart graphics.</param>
    public void Resize( ChartGraphics chartGraph )
    {
        this.Resize( chartGraph, false );
    }

    /// <summary>
    /// Resize the chart picture.
    /// </summary>
    /// <param name="chartGraph">Chart graphics.</param>
    /// <param name="calcAreaPositionOnly">Indicates that only chart area position is calculated.</param>
    public void Resize( ChartGraphics chartGraph, bool calcAreaPositionOnly )
    {
        // Set the chart size for Common elements
        this.Common.Width = this._width;
        this.Common.Height = this._height;

        // Set the chart size for Chart graphics
        chartGraph.SetPictureSize( this._width, this._height );

        // Initialize chart area(s) rectangle
        RectangleF chartAreasRectangle = new( 0, 0, this._width - 1, this._height - 1 );
        chartAreasRectangle = chartGraph.GetRelativeRectangle( chartAreasRectangle );

        // ============================
        // Get the 3D border interface
        // ============================
        this._titlePosition = RectangleF.Empty;
        IBorderType border3D = null;
        bool titleInBorder = false;

        if ( this.BorderSkin.SkinStyle != BorderSkinStyle.None )
        {
            // Set border size
            this._chartBorderPosition = chartGraph.GetAbsoluteRectangle( chartAreasRectangle );

            // Get border interface 
            border3D = this.Common.BorderTypeRegistry.GetBorderType( this.BorderSkin.SkinStyle.ToString() );
            if ( border3D is not null )
            {
                border3D.Resolution = chartGraph.Graphics.DpiX;
                // Check if title should be displayed in the border
                titleInBorder = border3D.GetTitlePositionInBorder() != RectangleF.Empty;
                this._titlePosition = chartGraph.GetRelativeRectangle( border3D.GetTitlePositionInBorder() );
                this._titlePosition.Width = chartAreasRectangle.Width - this._titlePosition.Width;

                // Adjust are position to the border size
                border3D.AdjustAreasPosition( chartGraph, ref chartAreasRectangle );
            }
        }

        // ============================
        // Calculate position of all titles in the collection
        // ============================
        RectangleF frameTitlePosition = RectangleF.Empty;
        if ( titleInBorder )
        {
            frameTitlePosition = new RectangleF( this._titlePosition.Location, this._titlePosition.Size );
        }
        foreach ( Title title in this.Titles )
        {
            if ( title.DockedToChartArea == Constants.NotSetValue &&
                title.Position.Auto &&
                title.Visible )
            {
                title.CalcTitlePosition( chartGraph, ref chartAreasRectangle, ref frameTitlePosition, elementSpacing );
            }
        }

        // ============================
        // Calculate position of all legends in the collection
        // ============================
        this.Legends.CalcLegendPosition( chartGraph, ref chartAreasRectangle, elementSpacing );

        // ============================
        // Calculate position of the chart area(s)
        // ============================
        chartAreasRectangle.Width -= elementSpacing;
        chartAreasRectangle.Height -= elementSpacing;
        RectangleF areaPosition = new();


        // Get number of chart areas that requeres automatic positioning
        int areaNumber = 0;
        foreach ( ChartArea area in this.ChartAreas )
        {
            // Check if area is visible
            if ( area.Visible )

            {
                if ( area.Position.Auto )
                {
                    ++areaNumber;
                }
            }
        }

        // Calculate how many columns & rows of areas we going to have
        int areaColumns = ( int ) Math.Floor( Math.Sqrt( areaNumber ) );
        if ( areaColumns < 1 )
        {
            areaColumns = 1;
        }
        int areaRows = ( int ) Math.Ceiling( areaNumber / (( float ) areaColumns) );

        // Set position for all areas
        int column = 0;
        int row = 0;
        foreach ( ChartArea area in this.ChartAreas )
        {
            // Check if area is visible
            if ( area.Visible )

            {
                if ( area.Position.Auto )
                {
                    // Calculate area position
                    areaPosition.Width = (chartAreasRectangle.Width / areaColumns) - elementSpacing;
                    areaPosition.Height = (chartAreasRectangle.Height / areaRows) - elementSpacing;
                    areaPosition.X = chartAreasRectangle.X + (column * (chartAreasRectangle.Width / areaColumns)) + elementSpacing;
                    areaPosition.Y = chartAreasRectangle.Y + (row * (chartAreasRectangle.Height / areaRows)) + elementSpacing;

                    // Calculate position of all titles in the collection docked outside of the chart area
                    TitleCollection.CalcOutsideTitlePosition( this, chartGraph, area, ref areaPosition, elementSpacing );

                    // Calculate position of the legend if it's docked outside of the chart area
                    this.Legends.CalcOutsideLegendPosition( chartGraph, area, ref areaPosition, elementSpacing );

                    // Set area position without changing the Auto flag
                    area.Position.SetPositionNoAuto( areaPosition.X, areaPosition.Y, areaPosition.Width, areaPosition.Height );

                    // Go to next area
                    ++row;
                    if ( row >= areaRows )
                    {
                        row = 0;
                        ++column;
                    }
                }
                else
                {
                    RectangleF rect = area.Position.ToRectangleF();

                    // Calculate position of all titles in the collection docked outside of the chart area
                    TitleCollection.CalcOutsideTitlePosition( this, chartGraph, area, ref rect, elementSpacing );

                    // Calculate position of the legend if it's docked outside of the chart area
                    this.Legends.CalcOutsideLegendPosition( chartGraph, area, ref rect, elementSpacing );
                }
            }
        }

        // ============================
        // Align chart areas Position if required
        // ============================
        this.AlignChartAreasPosition();

        // ============================**
        // Check if only chart area position must be calculated.
        // ============================**
        if ( !calcAreaPositionOnly )
        {
            // ============================
            // Call Resize function for each chart area.
            // ============================
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if area is visible
                if ( area.Visible )

                {
                    area.Resize( chartGraph );
                }
            }

            // ============================
            // Align chart areas InnerPlotPosition if required
            // ============================
            this.AlignChartAreas( AreaAlignmentStyles.PlotPosition );

            // ============================
            // Calculate position of the legend if it's inside
            // chart plotting area
            // ============================

            // Calculate position of all titles in the collection docked outside of the chart area
            TitleCollection.CalcInsideTitlePosition( this, chartGraph, elementSpacing );

            this.Legends.CalcInsideLegendPosition( chartGraph, elementSpacing );
        }
    }

    /// <summary>
    /// Minimum and maximum do not have to be calculated 
    /// from data series every time. It is very time 
    /// consuming. Minimum and maximum are buffered 
    /// and only when this flags are set Minimum and 
    /// Maximum are refreshed from data.
    /// </summary>
    internal void ResetMinMaxFromData()
    {
        if ( this.ChartAreas is not null )
        {
            // Call ResetMinMaxFromData function for each chart area.
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if area is visible
                if ( area.Visible )
                {
                    area.ResetMinMaxFromData();
                }
            }
        }
    }

    /// <summary>
    /// RecalculateAxesScale the chart picture.
    /// </summary>
    public void Recalculate()
    {
        // Call ReCalc function for each chart area.
        foreach ( ChartArea area in this.ChartAreas )
        {
            // Check if area is visible
            if ( area.Visible )

            {
                area.ReCalcInternal();
            }
        }
    }

    #endregion

    #region " chart picture properties "

    // VSTS 96787-Text Direction (RTL/LTR)	
    /// <summary>
    /// Gets or sets the RightToLeft type.
    /// </summary>
    [
    DefaultValue( RightToLeft.No )
    ]
    public RightToLeft RightToLeft
    {
        get => this.Common.Chart.RightToLeft;
        set => this.Common.Chart.RightToLeft = value;
    }

    /// <summary>
    /// Indicates that non-critical chart exceptions will be suppressed.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeSuppressExceptions" ),
    ]
    internal bool SuppressExceptions { set; get; } = false;

    /// <summary>
    /// Chart border skin style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( BorderSkinStyle.None ),
    SRDescription( "DescriptionAttributeBorderSkin" ),
    ]
    public BorderSkin BorderSkin { get; set; } = null;

    /// <summary>
    /// Reference to chart area collection
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartAreas" ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) )
    ]
    public ChartAreaCollection ChartAreas { get; private set; } = null;

    /// <summary>
    /// Chart legend collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeLegends" ),
    Editor( typeof( LegendCellCollectionEditor ), typeof( UITypeEditor ) )
    ]
    public LegendCollection Legends { get; private set; } = null;

    /// <summary>
    /// Chart title collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeCharttitle" ),
    SRDescription( "DescriptionAttributeTitles" ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) )
    ]
    public TitleCollection Titles { get; private set; } = null;



    /// <summary>
    /// Chart annotation collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeAnnotations3" ),
    Editor( typeof( AnnotationCollectionEditor ), (typeof( UITypeEditor )) )
    ]
    public AnnotationCollection Annotations { get; private set; }

    /// <summary>
    /// Background color for the Chart
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBackColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), (typeof( UITypeEditor )) )
    ]
    public Color BackColor { get; set; } = Color.White;

    /// <summary>
    /// Border color for the Chart
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), (typeof( UITypeEditor )) )
    ]
    public Color BorderColor { get; set; } = Color.White;

    /// <summary>
    /// Chart width
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 300 ),
    SRDescription( "DescriptionAttributeWidth" ),
    ]
    public int Width
    {
        get => this._width;
        set
        {
            this.InspectChartDimensions( value, this.Height );
            this._width = value;
            this.Common.Width = this._width;
        }
    }

    /// <summary>
    /// Series Data Manipulator
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    SRDescription( "DescriptionAttributeDataManipulator" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public DataManipulator DataManipulator { get; } = new DataManipulator();

    /// <summary>
    /// Chart height
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 300 ),
    SRDescription( "DescriptionAttributeHeight3" ),
    ]
    public int Height
    {
        get => this._height;
        set
        {
            this.InspectChartDimensions( this.Width, value );
            this._height = value;
            this.Common.Height = value;
        }
    }

    /// <summary>
    /// Back Hatch style
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartHatchStyle.None ),
    SRDescription( "DescriptionAttributeBackHatchStyle" ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
    ]
    public ChartHatchStyle BackHatchStyle { get; set; } = ChartHatchStyle.None;

    /// <summary>
    /// Chart area background image
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeBackImage" ),
    Editor( typeof( ImageValueEditor ), (typeof( UITypeEditor )) ),
    NotifyParentProperty( true )
    ]
    public string BackImage { get; set; } = "";

    /// <summary>
    /// Chart area background image drawing mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageWrapMode.Tile ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageWrapMode" ),
    ]
    public ChartImageWrapMode BackImageWrapMode { get; set; } = ChartImageWrapMode.Tile;

    /// <summary>
    /// Background image transparent color.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageTransparentColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BackImageTransparentColor { get; set; } = Color.Empty;

    /// <summary>
    /// Background image alignment used by ClampUnscale drawing mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageAlignmentStyle.TopLeft ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackImageAlign" ),
    ]
    public ChartImageAlignmentStyle BackImageAlignment { get; set; } = ChartImageAlignmentStyle.TopLeft;

    /// <summary>
    /// Indicates that smoothing is applied while drawing shadows.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( true ),
    SRDescription( "DescriptionAttributeSoftShadows3" ),
    ]
    public bool IsSoftShadows { get; set; } = true;

    /// <summary>
    /// Specifies whether smoothing (antialiasing) is applied while drawing chart.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( typeof( AntiAliasingStyles ), "All" ),
    SRDescription( "DescriptionAttributeAntiAlias" ),
    ]
    public AntiAliasingStyles AntiAliasing { get; set; } = AntiAliasingStyles.All;

    /// <summary>
    /// Specifies the quality of text antialiasing.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( typeof( TextAntiAliasingQuality ), "High" ),
    SRDescription( "DescriptionAttributeTextAntiAliasingQuality" ),
    ]
    public TextAntiAliasingQuality TextAntiAliasingQuality { get; set; } = TextAntiAliasingQuality.High;

    /// <summary>
    /// A type for the background gradient
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( GradientStyle.None ),
    SRDescription( "DescriptionAttributeBackGradientStyle" ),
    Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
    ]
    public GradientStyle BackGradientStyle { get; set; } = GradientStyle.None;

    /// <summary>
    /// The second color which is used for a gradient
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeBackSecondaryColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BackSecondaryColor { get; set; } = Color.Empty;

    /// <summary>
    /// The width of the border line
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeChart_BorderlineWidth" ),
    ]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionChartBorderIsNegative );
            }
            this._borderWidth = value;
        }
    }

    /// <summary>
    /// The style of the border line
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartDashStyle.NotSet ),
    SRDescription( "DescriptionAttributeBorderDashStyle" ),
    ]
    public ChartDashStyle BorderDashStyle { get; set; } = ChartDashStyle.NotSet;

    /// <summary>
    /// Gets the font cache.
    /// </summary>
    /// <value>The font cache.</value>
    internal FontCache FontCache { get; private set; } = new FontCache();

    #endregion

    #region " chart areas alignment methods "

    /// <summary>
    /// Checks if any of the chart areas are aligned.
    /// Also checks if the chart ares name in AlignWithChartArea property is valid.
    /// </summary>
    /// <returns>True if at least one area requires alignment.</returns>
    private bool IsAreasAlignmentRequired()
    {
        bool alignmentRequired = false;

        // Loop through all chart areas
        foreach ( ChartArea area in this.ChartAreas )
        {
            // Check if chart area is visible
            if ( area.Visible )

            {
                // Check if area is aligned
                if ( area.AlignWithChartArea != Constants.NotSetValue )
                {
                    alignmentRequired = true;

                    // Check the chart area used for alignment
                    if ( this.ChartAreas.IndexOf( area.AlignWithChartArea ) < 0 )
                    {
                        throw new InvalidOperationException( SR.ExceptionChartAreaNameReferenceInvalid( area.Name, area.AlignWithChartArea ) );
                    }
                }
            }
        }

        return alignmentRequired;
    }

    /// <summary>   Creates a list of the aligned chart areas. </summary>
    /// <remarks>   2024-11-04. </remarks>
    /// <param name="mainArea">     The main chart area. </param>
    /// <param name="type">         Alignment type. </param>
    /// <param name="orientation">  Vertical or Horizontal orientation. </param>
    /// <returns>   List of areas that area aligned to the main area. </returns>
    private ArrayList GetAlignedAreasGroup( ChartArea mainArea, AreaAlignmentStyles type, AreaAlignmentOrientations orientation )
    {
        ArrayList areaList = [];

        // Loop throughout the chart areas and get the ones aligned with specified main charting area
        foreach ( ChartArea area in this.ChartAreas )
        {
            // Check if chart area is visible
            if ( area.Visible )

            {
                if ( area.Name != mainArea.Name &&
                    area.AlignWithChartArea == mainArea.Name &&
                    (area.AlignmentStyle & type) == type &&
                    (area.AlignmentOrientation & orientation) == orientation )
                {
                    // Add client area into the list
                    _ = areaList.Add( area );
                }
            }
        }

        // If list is not empty insert "main" area in the beginning
        if ( areaList.Count > 0 )
        {
            areaList.Insert( 0, mainArea );
        }

        return areaList;
    }

    /// <summary>
    /// Performs specified type of alignment for the chart areas.
    /// </summary>
    /// <param name="type">Alignment type required.</param>
    internal void AlignChartAreas( AreaAlignmentStyles type )
    {
        // Check if alignment required
        if ( this.IsAreasAlignmentRequired() )
        {
            // Loop through all chart areas
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if chart area is visible
                if ( area.Visible )

                {
                    // Get vertical areas alignment group using current area as a main
                    ArrayList alignGroup = this.GetAlignedAreasGroup(
                        area,
                        type,
                        AreaAlignmentOrientations.Vertical );

                    // Align each area in the group
                    if ( alignGroup.Count > 0 )
                    {
                        this.AlignChartAreasPlotPosition( alignGroup, AreaAlignmentOrientations.Vertical );
                    }

                    // Get horizontal areas alignment group using current area as a main
                    alignGroup = this.GetAlignedAreasGroup(
                        area,
                        type,
                        AreaAlignmentOrientations.Horizontal );

                    // Align each area in the group
                    if ( alignGroup.Count > 0 )
                    {
                        this.AlignChartAreasPlotPosition( alignGroup, AreaAlignmentOrientations.Horizontal );
                    }
                }
            }
        }
    }

    /// <summary>
    /// Align inner plot position of the chart areas in the group.
    /// </summary>
    /// <param name="areasGroup">List of areas in the group.</param>
    /// <param name="orientation">Group orientation.</param>
    private void AlignChartAreasPlotPosition( ArrayList areasGroup, AreaAlignmentOrientations orientation )
    {
        // ============================****
        // Find the smalles size of the inner plot 
        // ============================****
        RectangleF areaPlotPosition = (( ChartArea ) areasGroup[0]).PlotAreaPosition.ToRectangleF();
        foreach ( ChartArea area in areasGroup )
        {
            if ( area.PlotAreaPosition.X > areaPlotPosition.X )
            {
                areaPlotPosition.X += area.PlotAreaPosition.X - areaPlotPosition.X;
                areaPlotPosition.Width -= area.PlotAreaPosition.X - areaPlotPosition.X;
            }
            if ( area.PlotAreaPosition.Y > areaPlotPosition.Y )
            {
                areaPlotPosition.Y += area.PlotAreaPosition.Y - areaPlotPosition.Y;
                areaPlotPosition.Height -= area.PlotAreaPosition.Y - areaPlotPosition.Y;
            }
            if ( area.PlotAreaPosition.Right < areaPlotPosition.Right )
            {
                areaPlotPosition.Width -= areaPlotPosition.Right - area.PlotAreaPosition.Right;
                if ( areaPlotPosition.Width < 5 )
                {
                    areaPlotPosition.Width = 5;
                }
            }
            if ( area.PlotAreaPosition.Bottom < areaPlotPosition.Bottom )
            {
                areaPlotPosition.Height -= areaPlotPosition.Bottom - area.PlotAreaPosition.Bottom;
                if ( areaPlotPosition.Height < 5 )
                {
                    areaPlotPosition.Height = 5;
                }
            }
        }

        // ============================****
        // Align inner plot position for all areas
        // ============================****
        foreach ( ChartArea area in areasGroup )
        {
            // Get curretn plot position of the area
            RectangleF rect = area.PlotAreaPosition.ToRectangleF();

            // Adjust area position
            if ( (orientation & AreaAlignmentOrientations.Vertical) == AreaAlignmentOrientations.Vertical )
            {
                rect.X = areaPlotPosition.X;
                rect.Width = areaPlotPosition.Width;
            }
            if ( (orientation & AreaAlignmentOrientations.Horizontal) == AreaAlignmentOrientations.Horizontal )
            {
                rect.Y = areaPlotPosition.Y;
                rect.Height = areaPlotPosition.Height;
            }

            // Set new plot position in coordinates relative to chart picture
            area.PlotAreaPosition.SetPositionNoAuto( rect.X, rect.Y, rect.Width, rect.Height );

            // Set new plot position in coordinates relative to chart area position
            rect.X = (rect.X - area.Position.X) / area.Position.Width * 100f;
            rect.Y = (rect.Y - area.Position.Y) / area.Position.Height * 100f;
            rect.Width = rect.Width / area.Position.Width * 100f;
            rect.Height = rect.Height / area.Position.Height * 100f;
            area.InnerPlotPosition.SetPositionNoAuto( rect.X, rect.Y, rect.Width, rect.Height );

            if ( (orientation & AreaAlignmentOrientations.Vertical) == AreaAlignmentOrientations.Vertical )
            {
                area.AxisX2.AdjustLabelFontAtSecondPass( this.ChartGraph, area.InnerPlotPosition.Auto );
                area.AxisX.AdjustLabelFontAtSecondPass( this.ChartGraph, area.InnerPlotPosition.Auto );
            }
            if ( (orientation & AreaAlignmentOrientations.Horizontal) == AreaAlignmentOrientations.Horizontal )
            {
                area.AxisY2.AdjustLabelFontAtSecondPass( this.ChartGraph, area.InnerPlotPosition.Auto );
                area.AxisY.AdjustLabelFontAtSecondPass( this.ChartGraph, area.InnerPlotPosition.Auto );
            }
        }

    }

    /// <summary>
    /// Aligns positions of the chart areas.
    /// </summary>
    private void AlignChartAreasPosition()
    {
        // Check if alignment required
        if ( this.IsAreasAlignmentRequired() )
        {
            // Loop through all chart areas
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if chart area is visible
                if ( area.Visible )

                {
                    // Check if area is aligned by Position to any other area
                    if ( area.AlignWithChartArea != Constants.NotSetValue && (area.AlignmentStyle & AreaAlignmentStyles.Position) == AreaAlignmentStyles.Position )
                    {
                        // Get current area position
                        RectangleF areaPosition = area.Position.ToRectangleF();

                        // Get main chart area
                        ChartArea mainArea = this.ChartAreas[area.AlignWithChartArea];

                        // Vertical alignment
                        if ( (area.AlignmentOrientation & AreaAlignmentOrientations.Vertical) == AreaAlignmentOrientations.Vertical )
                        {
                            // Align area position
                            areaPosition.X = mainArea.Position.X;
                            areaPosition.Width = mainArea.Position.Width;
                        }

                        // Horizontal alignment
                        if ( (area.AlignmentOrientation & AreaAlignmentOrientations.Horizontal) == AreaAlignmentOrientations.Horizontal )
                        {
                            // Align area position
                            areaPosition.Y = mainArea.Position.Y;
                            areaPosition.Height = mainArea.Position.Height;
                        }

                        // Set new position
                        area.Position.SetPositionNoAuto( areaPosition.X, areaPosition.Y, areaPosition.Width, areaPosition.Height );
                    }
                }
            }
        }
    }

    /// <summary>
    /// Align chart areas cursor.
    /// </summary>
    /// <param name="changedArea">Changed chart area.</param>
    /// <param name="orientation">Orientation of the changed cursor.</param>
    /// <param name="selectionChanged">AxisName of change cursor or selection.</param>
    internal void AlignChartAreasCursor( ChartArea changedArea, AreaAlignmentOrientations orientation, bool selectionChanged )
    {
        // Check if alignment required
        if ( this.IsAreasAlignmentRequired() )
        {
            // Loop through all chart areas
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if chart area is visible
                if ( area.Visible )

                {
                    // Get vertical areas alignment group using current area as a main
                    ArrayList alignGroup = this.GetAlignedAreasGroup(
                        area,
                        AreaAlignmentStyles.Cursor,
                        orientation );

                    // Align each area in the group if it contains changed area
                    if ( alignGroup.Contains( changedArea ) )
                    {
                        // Set cursor position for all areas in the group
                        foreach ( ChartArea groupArea in alignGroup )
                        {
                            groupArea.alignmentInProcess = true;

                            if ( orientation == AreaAlignmentOrientations.Vertical )
                            {
                                if ( selectionChanged )
                                {
                                    groupArea.CursorX.SelectionStart = changedArea.CursorX.SelectionStart;
                                    groupArea.CursorX.SelectionEnd = changedArea.CursorX.SelectionEnd;
                                }
                                else
                                {
                                    groupArea.CursorX.Position = changedArea.CursorX.Position;
                                }
                            }
                            if ( orientation == AreaAlignmentOrientations.Horizontal )
                            {
                                if ( selectionChanged )
                                {
                                    groupArea.CursorY.SelectionStart = changedArea.CursorY.SelectionStart;
                                    groupArea.CursorY.SelectionEnd = changedArea.CursorY.SelectionEnd;
                                }
                                else
                                {
                                    groupArea.CursorY.Position = changedArea.CursorY.Position;
                                }
                            }

                            groupArea.alignmentInProcess = false;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// One of the chart areas was zoomed by the user.
    /// </summary>
    /// <param name="changedArea">Changed chart area.</param>
    /// <param name="orientation">Orientation of the changed scaleView.</param>
    /// <param name="disposeBufferBitmap">Area double fuffer image must be disposed.</param>
    internal void AlignChartAreasZoomed( ChartArea changedArea, AreaAlignmentOrientations orientation, bool disposeBufferBitmap )
    {
        // Check if alignment required
        if ( this.IsAreasAlignmentRequired() )
        {
            // Loop through all chart areas
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if chart area is visible
                if ( area.Visible )

                {
                    // Get vertical areas alignment group using current area as a main
                    ArrayList alignGroup = this.GetAlignedAreasGroup(
                        area,
                        AreaAlignmentStyles.AxesView,
                        orientation );

                    // Align each area in the group if it contains changed area
                    if ( alignGroup.Contains( changedArea ) )
                    {
                        // Set cursor position for all areas in the group
                        foreach ( ChartArea groupArea in alignGroup )
                        {
                            // Clear image buffer
                            if ( groupArea.areaBufferBitmap != null && disposeBufferBitmap )
                            {
                                groupArea.areaBufferBitmap.Dispose();
                                groupArea.areaBufferBitmap = null;
                            }

                            if ( orientation == AreaAlignmentOrientations.Vertical )
                            {
                                groupArea.CursorX.SelectionStart = double.NaN;
                                groupArea.CursorX.SelectionEnd = double.NaN;
                            }
                            if ( orientation == AreaAlignmentOrientations.Horizontal )
                            {
                                groupArea.CursorY.SelectionStart = double.NaN;
                                groupArea.CursorY.SelectionEnd = double.NaN;
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Align chart areas axes views.
    /// </summary>
    /// <param name="changedArea">Changed chart area.</param>
    /// <param name="orientation">Orientation of the changed scaleView.</param>
    internal void AlignChartAreasAxesView( ChartArea changedArea, AreaAlignmentOrientations orientation )
    {
        // Check if alignment required
        if ( this.IsAreasAlignmentRequired() )
        {
            // Loop through all chart areas
            foreach ( ChartArea area in this.ChartAreas )
            {
                // Check if chart area is visible
                if ( area.Visible )

                {
                    // Get vertical areas alignment group using current area as a main
                    ArrayList alignGroup = this.GetAlignedAreasGroup(
                        area,
                        AreaAlignmentStyles.AxesView,
                        orientation );

                    // Align each area in the group if it contains changed area
                    if ( alignGroup.Contains( changedArea ) )
                    {
                        // Set cursor position for all areas in the group
                        foreach ( ChartArea groupArea in alignGroup )
                        {
                            groupArea.alignmentInProcess = true;

                            if ( orientation == AreaAlignmentOrientations.Vertical )
                            {
                                groupArea.AxisX.ScaleView.Position = changedArea.AxisX.ScaleView.Position;
                                groupArea.AxisX.ScaleView.Size = changedArea.AxisX.ScaleView.Size;
                                groupArea.AxisX.ScaleView.SizeType = changedArea.AxisX.ScaleView.SizeType;

                                groupArea.AxisX2.ScaleView.Position = changedArea.AxisX2.ScaleView.Position;
                                groupArea.AxisX2.ScaleView.Size = changedArea.AxisX2.ScaleView.Size;
                                groupArea.AxisX2.ScaleView.SizeType = changedArea.AxisX2.ScaleView.SizeType;
                            }
                            if ( orientation == AreaAlignmentOrientations.Horizontal )
                            {
                                groupArea.AxisY.ScaleView.Position = changedArea.AxisY.ScaleView.Position;
                                groupArea.AxisY.ScaleView.Size = changedArea.AxisY.ScaleView.Size;
                                groupArea.AxisY.ScaleView.SizeType = changedArea.AxisY.ScaleView.SizeType;

                                groupArea.AxisY2.ScaleView.Position = changedArea.AxisY2.ScaleView.Position;
                                groupArea.AxisY2.ScaleView.Size = changedArea.AxisY2.ScaleView.Size;
                                groupArea.AxisY2.ScaleView.SizeType = changedArea.AxisY2.ScaleView.SizeType;
                            }

                            groupArea.alignmentInProcess = false;
                        }
                    }
                }
            }
        }
    }

    #endregion

    #region " helper methods "

    /// <summary>
    /// Inspects the chart dimensions.
    /// </summary>
    /// <param name="width">The width.</param>
    /// <param name="height">The height.</param>
    internal void InspectChartDimensions( int width, int height )
    {
        if ( this.Chart.IsDesignMode() && ((width * height) > (100 * 1024 * 1024)) )
        {
            throw new ArgumentException( SR.ExceptionChartOutOfLimits );
        }
        if ( width < 0 )
        {
            throw new ArgumentException( SR.ExceptionValueMustBeGreaterThan( nameof( this.Width ), "0px" ) );
        }
        if ( height < 0 )
        {
            throw new ArgumentException( SR.ExceptionValueMustBeGreaterThan( nameof( this.Height ), "0px" ) );
        }
    }

    /// <summary>
    /// Loads chart appearance template from file.
    /// </summary>
    /// <param name="name">Template file name to load from.</param>
    public void LoadTemplate( string name )
    {
        // Check arguments
        if ( name == null ) throw new ArgumentNullException( nameof( name ) );

        // Load template data into the stream
        Stream stream = new FileStream( name, FileMode.Open, FileAccess.Read );

        // Load template from stream
        this.LoadTemplate( stream );

        // Close template stream
        stream.Close();
    }

    /// <summary>
    /// Loads chart appearance template from stream.
    /// </summary>
    /// <param name="stream">Template stream to load from.</param>
    public void LoadTemplate( Stream stream )
    {
        // Check arguments
        if ( stream == null ) throw new ArgumentNullException( nameof( stream ) );

        ChartSerializer serializer = ( ChartSerializer ) this.Common.container.GetService( typeof( ChartSerializer ) );
        if ( serializer is not null )
        {
            // Save previous serializer properties
            string oldSerializableContent = serializer.SerializableContent;
            string oldNonSerializableContent = serializer.NonSerializableContent;
            SerializationFormat oldFormat = serializer.Format;
            bool oldIgnoreUnknownXmlAttributes = serializer.IsUnknownAttributeIgnored;
            bool oldTemplateMode = serializer.IsTemplateMode;

            // Set serializer properties
            serializer.Content = SerializationContents.Appearance;
            serializer.SerializableContent += ",Chart.Titles,Chart.Annotations," +
                                              "Chart.Legends,Legend.CellColumns,Legend.CustomItems,LegendItem.Cells," +
                                              "Chart.Series,Series.*Style," +
                                              "Chart.ChartAreas,ChartArea.Axis*," +
                                              "Axis.*Grid,Axis.*TickMark, Axis.*Style," +
                                              "Axis.StripLines, Axis.CustomLabels";
            serializer.Format = SerializationFormat.Xml;
            serializer.IsUnknownAttributeIgnored = true;
            serializer.IsTemplateMode = true;

            try
            {
                // Load template
                serializer.Load( stream );
            }
            catch ( Exception ex )
            {
                throw new InvalidOperationException( ex.Message );
            }
            finally
            {
                // Restore previous serializer properties
                serializer.SerializableContent = oldSerializableContent;
                serializer.NonSerializableContent = oldNonSerializableContent;
                serializer.Format = oldFormat;
                serializer.IsUnknownAttributeIgnored = oldIgnoreUnknownXmlAttributes;
                serializer.IsTemplateMode = oldTemplateMode;
            }
        }
    }

    /// <summary>
    /// Returns the default title from Titles collection.
    /// </summary>
    /// <param name="create">Create title if it doesn't exists.</param>
    /// <returns>Default title.</returns>
    internal Title GetDefaultTitle( bool create )
    {
        // Check if default title exists
        Title defaultTitle = null;
        foreach ( Title title in this.Titles )
        {
            if ( title.Name == "Default Title" )
            {
                defaultTitle = title;
            }
        }

        // Create new default title
        if ( defaultTitle == null && create )
        {
            defaultTitle = new Title
            {
                Name = "Default Title"
            };
            this.Titles.Insert( 0, defaultTitle );
        }

        return defaultTitle;
    }

    /// <summary>
    /// Checks if tooltips are enabled
    /// </summary>
    /// <returns>true if tooltips enabled</returns>
    private bool IsToolTipsEnabled()
    {
        // Data series loop
        foreach ( Series series in this.Common.DataManager.Series )
        {
            // Check series tooltips
            if ( series.ToolTip.Length > 0 )
            {
                // ToolTips enabled
                return true;
            }

            // Check series tooltips
            if ( series.LegendToolTip.Length > 0 ||
                series.LabelToolTip.Length > 0 )
            {
                // ToolTips enabled
                return true;
            }

            // Check point tooltips only for "non-Fast" chart types
            if ( !series.IsFastChartType() )
            {
                // Data point loop
                foreach ( DataPoint point in series.Points )
                {
                    // ToolTip empty
                    if ( point.ToolTip.Length > 0 )
                    {
                        // ToolTips enabled
                        return true;
                    }
                    // ToolTip empty
                    if ( point.LegendToolTip.Length > 0 ||
                        point.LabelToolTip.Length > 0 )
                    {
                        // ToolTips enabled
                        return true;
                    }
                }
            }
        }

        // Legend items loop
        foreach ( Legend legend in this.Legends )
        {
            foreach ( LegendItem legendItem in legend.CustomItems )
            {
                // ToolTip empty
                if ( legendItem.ToolTip.Length > 0 )
                {
                    return true;
                }
            }
        }

        // Title items loop
        foreach ( Title title in this.Titles )
        {
            // ToolTip empty
            if ( title.ToolTip.Length > 0 )
            {
                return true;
            }
        }

        return false;
    }

    #endregion

    #region " idisposable members "
    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            // Dispose managed resources
            if ( this.ChartGraph is not null )
            {
                this.ChartGraph.Dispose();
                this.ChartGraph = null;
            }
            if ( this.Legends is not null )
            {
                this.Legends.Dispose();
                this.Legends = null;
            }
            if ( this.Titles is not null )
            {
                this.Titles.Dispose();
                this.Titles = null;
            }
            if ( this.ChartAreas is not null )
            {
                this.ChartAreas.Dispose();
                this.ChartAreas = null;
            }
            if ( this.Annotations is not null )
            {
                this.Annotations.Dispose();
                this.Annotations = null;
            }
            if ( this.hotRegionsList is not null )
            {
                this.hotRegionsList.Dispose();
                this.hotRegionsList = null;
            }
            if ( this.FontCache is not null )
            {
                this.FontCache.Dispose();
                this.FontCache = null;
            }
            if ( this.BorderSkin is not null )
            {
                this.BorderSkin.Dispose();
                this.BorderSkin = null;
            }

            if ( this.nonTopLevelChartBuffer is not null )
            {
                this.nonTopLevelChartBuffer.Dispose();
                this.nonTopLevelChartBuffer = null;
            }
        }

        base.Dispose( disposing );
    }

    #endregion
}
/// <summary>
/// Event arguments of Chart paint event.
/// </summary>
public class ChartPaintEventArgs : EventArgs
{
    #region " fields "

    // Private fields
#pragma warning disable IDE1006 // Naming Styles
    private Chart _chart;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " properties "

    /// <summary>
    /// Gets the chart element of the event.
    /// </summary>
    /// <value>The chart element.</value>
    public object ChartElement { get; private set; }

    /// <summary>
    /// Gets the ChartGraphics object of the event.
    /// </summary>
    public ChartGraphics ChartGraphics { get; private set; }

    /// <summary>
    /// Chart Common elements.
    /// </summary>
    internal CommonElements CommonElements { get; private set; }

    /// <summary>
    /// Chart element position in relative coordinates of the event.
    /// </summary>
    public ElementPosition Position { get; private set; }

    /// <summary>
    /// Chart object of the event.
    /// </summary>
    public Chart Chart
    {
        get
        {
            if ( this._chart == null && this.CommonElements is not null )
            {
                this._chart = this.CommonElements.Chart;
            }

            return this._chart;
        }
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Default constructor is not accessible
    /// </summary>
    private ChartPaintEventArgs()
    {
    }

    /// <summary>
    /// Paint event arguments constructor.
    /// </summary>
    /// <param name="chartElement">Chart element.</param>
    /// <param name="chartGraph">Chart graphics.</param>
    /// <param name="common">Common elements.</param>
    /// <param name="position">Position.</param>
    internal ChartPaintEventArgs( object chartElement, ChartGraphics chartGraph, CommonElements common, ElementPosition position )
    {
        this.ChartElement = chartElement;
        this.ChartGraphics = chartGraph;
        this.CommonElements = common;
        this.Position = position;
    }

    #endregion
}
/// <summary>
/// Event arguments of localized numbers formatting event.
/// </summary>
public class FormatNumberEventArgs : EventArgs
{
    #region " properties "

    /// <summary>
    /// Value to be formatted.
    /// </summary>
    public double Value { get; private set; }

    /// <summary>
    /// Localized text.
    /// </summary>
    public string LocalizedValue { get; set; }

    /// <summary>
    /// Format string.
    /// </summary>
    public string Format { get; private set; }

    /// <summary>
    /// Value type.
    /// </summary>
    public ChartValueType ValueType { get; private set; } = ChartValueType.Auto;

    /// <summary>
    /// The sender object of the event.
    /// </summary>
    public object SenderTag { get; private set; }

    /// <summary>
    /// Chart element type.
    /// </summary>
    public ChartElementType ElementType { get; set; }

    #endregion

    #region " methods "

    /// <summary>
    /// Default constructor is not accessible
    /// </summary>
    private FormatNumberEventArgs()
    {
    }

    /// <summary>
    /// Object constructor.
    /// </summary>
    /// <param name="value">Value to be formatted.</param>
    /// <param name="format">Format string.</param>
    /// <param name="valueType">Value type..</param>
    /// <param name="localizedValue">Localized value.</param>
    /// <param name="senderTag">Chart element object tag.</param>
    /// <param name="elementType">Chart element type.</param>
    internal FormatNumberEventArgs( double value, string format, ChartValueType valueType, string localizedValue, object senderTag, ChartElementType elementType )
    {
        this.Value = value;
        this.Format = format;
        this.ValueType = valueType;
        this.LocalizedValue = localizedValue;
        this.SenderTag = senderTag;
        this.ElementType = elementType;
    }

    #endregion
}

#region " fontcache "
/// <summary>
/// Font cache class helps ChartElements to reuse the Font instances
/// </summary>
internal class FontCache : IDisposable
{
    #region " static "

#pragma warning disable IDE1006 // Naming Styles

    // Default font family name
    private static string _defaultFamilyName;

#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Gets the default font family name.
    /// </summary>
    /// <value>The default font family name.</value>
    public static string DefaultFamilyName
    {
        get
        {
            if ( _defaultFamilyName == null )
            {
                // Find the "Microsoft Sans Serif" font
                foreach ( FontFamily fontFamily in FontFamily.Families )
                {
                    if ( fontFamily.Name == "Microsoft Sans Serif" )
                    {
                        _defaultFamilyName = fontFamily.Name;
                        break;
                    }
                }
                // Not found - use the default Sans Serif font
                _defaultFamilyName ??= FontFamily.GenericSansSerif.Name;
            }
            return _defaultFamilyName;
        }
    }
    #endregion

    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    // Cached fonts dictionary 
    private readonly Dictionary<KeyInfo, Font> _fontCache = new( new KeyInfo.EqualityComparer() );
#pragma warning restore IDE1006 // Naming Styles

    #endregion // Fields

    #region " properties "
    /// <summary>
    /// Gets the default font.
    /// </summary>
    /// <value>The default font.</value>
    public Font DefaultFont => this.GetFont( DefaultFamilyName, 8 );

    /// <summary>
    /// Gets the default font.
    /// </summary>
    /// <value>The default font.</value>
    public Font DefaultBoldFont => this.GetFont( DefaultFamilyName, 8, FontStyle.Bold );
    #endregion

    #region " methods "

    /// <summary>
    /// Gets the font.
    /// </summary>
    /// <param name="familyName">Name of the family.</param>
    /// <param name="size">The size.</param>
    /// <returns>Font instance</returns>
    public Font GetFont( string familyName, int size )
    {
        KeyInfo key = new( familyName, size );
        if ( !this._fontCache.ContainsKey( key ) )
        {
            this._fontCache.Add( key, new Font( familyName, size ) );
        }
        return this._fontCache[key];
    }

    /// <summary>
    /// Gets the font.
    /// </summary>
    /// <param name="familyName">Name of the family.</param>
    /// <param name="size">The size.</param>
    /// <param name="style">The style.</param>
    /// <returns>Font instance</returns>
    public Font GetFont( string familyName, float size, FontStyle style )
    {
        KeyInfo key = new( familyName, size, style );
        if ( !this._fontCache.ContainsKey( key ) )
        {
            this._fontCache.Add( key, new Font( familyName, size, style ) );
        }
        return this._fontCache[key];
    }

    /// <summary>
    /// Gets the font.
    /// </summary>
    /// <param name="family">The family.</param>
    /// <param name="size">The size.</param>
    /// <param name="style">The style.</param>
    /// <returns>Font instance</returns>
    public Font GetFont( FontFamily family, float size, FontStyle style )
    {
        KeyInfo key = new( family, size, style );
        if ( !this._fontCache.ContainsKey( key ) )
        {
            this._fontCache.Add( key, new Font( family, size, style ) );
        }
        return this._fontCache[key];
    }

    /// <summary>
    /// Gets the font.
    /// </summary>
    /// <param name="family">The family.</param>
    /// <param name="size">The size.</param>
    /// <param name="style">The style.</param>
    /// <param name="unit">The unit.</param>
    /// <returns>Font instance</returns>
    public Font GetFont( FontFamily family, float size, FontStyle style, GraphicsUnit unit )
    {
        KeyInfo key = new( family, size, style, unit );
        if ( !this._fontCache.ContainsKey( key ) )
        {
            this._fontCache.Add( key, new Font( family, size, style, unit ) );
        }
        return this._fontCache[key];
    }

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
        foreach ( Font font in this._fontCache.Values )
        {
            font.Dispose();
        }
        this._fontCache.Clear();
        GC.SuppressFinalize( this );
    }

    #endregion

    #region " fontkeyinfo structure "
    /// <summary>
    /// Font key info
    /// </summary>
    private class KeyInfo
    {
#pragma warning disable IDE1006 // Naming Styles
        private readonly string _familyName;
        private readonly float _size = 8;
        private readonly GraphicsUnit _unit = GraphicsUnit.Point;
        private readonly FontStyle _style = FontStyle.Regular;
        private readonly int _gdiCharSet = 1;
#pragma warning restore IDE1006 // Naming Styles

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyInfo"/> class.
        /// </summary>
        /// <param name="familyName">Name of the family.</param>
        /// <param name="size">The size.</param>
        public KeyInfo( string familyName, float size )
        {
            this._familyName = familyName;
            this._size = size;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyInfo"/> class.
        /// </summary>
        /// <param name="familyName">Name of the family.</param>
        /// <param name="size">The size.</param>
        /// <param name="style">The style.</param>
        public KeyInfo( string familyName, float size, FontStyle style )
        {
            this._familyName = familyName;
            this._size = size;
            this._style = style;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyInfo"/> class.
        /// </summary>
        /// <param name="family">The family.</param>
        /// <param name="size">The size.</param>
        /// <param name="style">The style.</param>
        public KeyInfo( FontFamily family, float size, FontStyle style )
        {
            this._familyName = family.ToString();
            this._size = size;
            this._style = style;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyInfo"/> class.
        /// </summary>
        /// <param name="family">The family.</param>
        /// <param name="size">The size.</param>
        /// <param name="style">The style.</param>
        /// <param name="unit">The unit.</param>
        public KeyInfo( FontFamily family, float size, FontStyle style, GraphicsUnit unit )
        {
            this._familyName = family.ToString();
            this._size = size;
            this._style = style;
            this._unit = unit;
        }

        #region " iequatable<fontkeyinfo> members "
        /// <summary>
        /// KeyInfo equality comparer
        /// </summary>
        internal class EqualityComparer : IEqualityComparer<KeyInfo>
        {
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type <paramref name="x"/> to compare.</param>
            /// <param name="y">The second object of type <paramref name="y"/> to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals( KeyInfo x, KeyInfo y )
            {
                return
                    x._size == y._size &&
                    x._familyName == y._familyName &&
                    x._unit == y._unit &&
                    x._style == y._style &&
                    x._gdiCharSet == y._gdiCharSet;
            }

            /// <summary>
            /// Returns a hash code for the specified object.
            /// </summary>
            /// <param name="obj">The <see cref="object"/> for which a hash code is to be returned.</param>
            /// <returns>A hash code for the specified object.</returns>
            /// <exception cref="ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.</exception>
            public int GetHashCode( KeyInfo obj )
            {
                return obj._familyName.GetHashCode() ^ obj._size.GetHashCode();
            }
        }
        #endregion
    }
    #endregion
}
#endregion
