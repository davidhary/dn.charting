//
//  Purpose:	CommonElements class provides references to common 
//              chart classes like DataManager, ChartTypeRegistry, 
//              ImageLoader and others. It is passed to different 
//              chart elements to simplify access to those common 
//              classes.
//


using System.ComponentModel.Design;
using System.Globalization;
using System.Windows.Forms.DataVisualization.Charting.Borders3D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.DataVisualization.Charting.Data;
using System.Windows.Forms.DataVisualization.Charting.Formulas;
using System.Windows.Forms.DataVisualization.Charting.Utilities;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// CommonElements class provides references to common chart classes like 
/// DataManager, ChartTypeRegistry, ImageLoader and others. It is passed 
/// to different chart elements to simplify access to those common classes.
/// </summary>
internal class CommonElements
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles

    private Chart _chart;
    private ChartImage _chartPicture;

    // Reference to Chart Graphics Object
    internal ChartGraphics graph;

    /// <summary>
    /// Service Container
    /// </summary>
    internal IServiceContainer container;

    /// <summary>
    /// Indicates painting mode
    /// </summary>
    internal bool processModePaint = true;

    /// <summary>
    /// Indicates selection mode
    /// </summary>
    internal bool processModeRegions;

#pragma warning restore IDE1006 // Naming Styles
    #endregion

    #region " properties "

    /// <summary>
    /// Reference to the Data Manager
    /// </summary>
    internal DataManager DataManager => ( DataManager ) this.container.GetService( typeof( DataManager ) );

    /// <summary>
    /// True if painting mode is active
    /// </summary>
    public bool ProcessModePaint => this.processModePaint;

    /// <summary>
    /// True if Hot region or image maps mode is active
    /// </summary>
    public bool ProcessModeRegions => this.processModeRegions;

    /// <summary>
    /// Reference to the hot regions object
    /// </summary>
    public HotRegionsList HotRegionsList => this.ChartPicture.hotRegionsList;

    /// <summary>
    /// Reference to the Data Manipulator
    /// </summary>
    public DataManipulator DataManipulator => this.ChartPicture.DataManipulator;

    /// <summary>
    /// Reference to the ImageLoader
    /// </summary>
    internal ImageLoader ImageLoader => ( ImageLoader ) this.container.GetService( typeof( ImageLoader ) );

    /// <summary>
    /// Reference to the Chart
    /// </summary>
    internal Chart Chart
    {
        get
        {
            this._chart ??= ( Chart ) this.container.GetService( typeof( Chart ) );
            return this._chart;
        }
    }

    /// <summary>
    /// Reference to the ChartTypeRegistry
    /// </summary>
    internal ChartTypeRegistry ChartTypeRegistry => ( ChartTypeRegistry ) this.container.GetService( typeof( ChartTypeRegistry ) );

    /// <summary>
    /// Reference to the BorderTypeRegistry
    /// </summary>
    internal BorderTypeRegistry BorderTypeRegistry => ( BorderTypeRegistry ) this.container.GetService( typeof( BorderTypeRegistry ) );

    /// <summary>
    /// Reference to the FormulaRegistry
    /// </summary>
    internal FormulaRegistry FormulaRegistry => ( FormulaRegistry ) this.container.GetService( typeof( FormulaRegistry ) );



    /// <summary>
    /// Reference to the ChartPicture
    /// </summary>
    internal ChartImage ChartPicture
    {
        get
        {
            this._chartPicture ??= ( ChartImage ) this.container.GetService( typeof( ChartImage ) );
            return this._chartPicture;
        }
    }

    /// <summary>
    /// Width of the chart picture
    /// </summary>
    internal int Width { get; set; }

    /// <summary>
    /// Height of the chart picture
    /// </summary>
    internal int Height { get; set; }

    #endregion

    #region " methods "

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="container">Service container.</param>
    internal CommonElements( IServiceContainer container ) => this.container = container;


    #endregion

    #region " string convertion helper methods "

    /// <summary>
    /// Converts string to double.
    /// </summary>
    /// <param name="stringToParse">String to convert.</param>
    /// <returns>Double result.</returns>
    internal static double ParseDouble( string stringToParse )
    {
        return ParseDouble( stringToParse, false );
    }

    /// <summary>
    /// Converts string to double.
    /// </summary>
    /// <param name="stringToParse">String to convert.</param>
    /// <param name="throwException">if set to <c>true</c> the exception thrown.</param>
    /// <returns>Double result.</returns>
    internal static double ParseDouble( string stringToParse, bool throwException )
    {
        double result = 0.0;

        if ( throwException )
        {
            result = double.Parse( stringToParse, NumberStyles.Any, CultureInfo.InvariantCulture );
        }
        else
        {
            bool parseSucceed = double.TryParse( stringToParse, NumberStyles.Any, CultureInfo.InvariantCulture, out result );
            if ( !parseSucceed )
            {
                _ = double.TryParse( stringToParse, NumberStyles.Any, CultureInfo.CurrentCulture, out result );
            }
        }
        return result;
    }

    /// <summary>
    /// Converts string to double.
    /// </summary>
    /// <param name="stringToParse">String to convert.</param>
    /// <returns>Double result.</returns>
    internal static float ParseFloat( string stringToParse )
    {
        bool parseSucceed = float.TryParse( stringToParse, NumberStyles.Any, CultureInfo.InvariantCulture, out float result );

        if ( !parseSucceed )
        {
            _ = float.TryParse( stringToParse, NumberStyles.Any, CultureInfo.CurrentCulture, out result );
        }

        return result;
    }

    #endregion
}
