//
//  Purpose:	CircularChartAreaAxis is a helper class which is used
//              in circular chart areas for charts like Polar and 
//              Radar. 
//


using System.Drawing;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// CircularChartAreaAxis class represents a single axis in the circular 
/// chart area chart like radar or polar. It contains axis angular 
/// position, size and title properties.
/// </summary>
internal class CircularChartAreaAxis
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles

    /// <summary>
    /// Angle where axis is located.
    /// </summary>
    internal float AxisPosition;

    /// <summary>
    /// Axis title.
    /// </summary>
    internal string Title = string.Empty;

    /// <summary>
    /// Axis title color.
    /// </summary>
    internal Color TitleForeColor = Color.Empty;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructtion "

    /// <summary>
    /// Constructor.
    /// </summary>
    public CircularChartAreaAxis()
    {
    }

    /// <summary>
    /// Constructor.
    /// </summary>
    internal CircularChartAreaAxis( float axisPosition ) => this.AxisPosition = axisPosition;

    #endregion
}
