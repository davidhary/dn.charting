
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// Base class for all chart element collections
/// </summary>
public abstract class ChartElementCollection<T> : Collection<T>, IChartElement, IDisposable
    where T : ChartElement
{
    #region " member variables "

#pragma warning disable IDE1006 // Naming Styles
    private IChartElement _parent;
    private CommonElements _common;
    internal int _suspendUpdates;
#pragma warning restore IDE1006 // Naming Styles
    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets the parent.
    /// </summary>
    internal IChartElement Parent
    {
        get => this._parent;
        set
        {
            this._parent = value;
            this.Invalidate();
        }
    }
    /// <summary>
    /// Gets the CommonElements of the chart.
    /// </summary>
    internal CommonElements Common
    {
        get
        {
            if ( this._common == null && this._parent is not null )
            {
                this._common = this._parent.Common;
            }
            return this._common;
        }
    }

    /// <summary>
    /// Gets the chart.
    /// </summary>
    internal Chart Chart => this.Common?.Chart;

    /// <summary>
    /// Gets the items as List{T}. Use this property to perform advanced List specific operations (Sorting, etc)
    /// </summary>
    internal List<T> ItemList => this.Items as List<T>;

    internal bool IsSuspended => this._suspendUpdates > 0;
    #endregion

    #region " constructtion "

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartElementCollection{T}"/> class.
    /// </summary>
    /// <param name="parent">The parent chart element.</param>
    internal ChartElementCollection( IChartElement parent ) => this._parent = parent;

    #endregion

    #region " methods "

    /// <summary>
    /// Forces the invalidation of the parent chart element
    /// </summary>
    public virtual void Invalidate()
    {
        if ( this._parent != null && !this.IsSuspended )
            this._parent.Invalidate();
    }

    /// <summary>
    /// Suspends invalidation
    /// </summary>
    public virtual void SuspendUpdates()
    {
        this._suspendUpdates++;
    }

    /// <summary>
    /// Resumes invalidation.
    /// </summary>
    public virtual void ResumeUpdates()
    {
        if ( this._suspendUpdates > 0 )
            this._suspendUpdates--;

        if ( this._suspendUpdates == 0 )
            this.Invalidate();
    }

    /// <summary>
    /// Removes all elements from the <see cref="Collections.ObjectModel.Collection{T}"/>.
    /// </summary>
    protected override void ClearItems()
    {
        this.SuspendUpdates();
        while ( this.Count > 0 )
        {
            this.RemoveItem( 0 );
        }
        this.ResumeUpdates();
    }

    /// <summary>
    /// Deinitializes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    internal virtual void Deinitialize( T item )
    {
    }

    /// <summary>
    /// Initializes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    internal virtual void Initialize( T item )
    {
    }

    /// <summary>
    /// Removes the element at the specified index of the <see cref="Collections.ObjectModel.Collection{T}"/>.
    /// </summary>
    /// <param name="index">The zero-based index of the element to remove.</param>
    protected override void RemoveItem( int index )
    {
        this.Deinitialize( this[index] );
        this[index].Parent = null;
        base.RemoveItem( index );
        this.Invalidate();
    }

    /// <summary>
    /// Inserts an element into the <see cref="Collections.ObjectModel.Collection{T}"/> at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
    /// <param name="item">The object to insert. The value can be null for reference types.</param>
    protected override void InsertItem( int index, T item )
    {
        this.Initialize( item );
        item.Parent = this;
        base.InsertItem( index, item );
        this.Invalidate();
    }

    /// <summary>
    /// Replaces the element at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index of the element to replace.</param>
    /// <param name="item">The new value for the element at the specified index. The value can be null for reference types.</param>
    protected override void SetItem( int index, T item )
    {
        this.Initialize( item );
        item.Parent = this;
        base.SetItem( index, item );
        this.Invalidate();
    }

    #endregion

    #region " ichartelement members "

    IChartElement IChartElement.Parent
    {
        get => this.Parent;
        set => this.Parent = value;
    }

    void IChartElement.Invalidate()
    {
        this.Invalidate();
    }

    CommonElements IChartElement.Common => this.Common;

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose( bool disposing )
    {
        if ( disposing )
        {
            // Dispose managed resources
            foreach ( T element in this )
            {
                element.Dispose();
            }
        }
    }

    /// <summary>
    /// Performs freeing, releasing, or resetting managed resources.
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }
    #endregion
}
/// <summary>
/// Base class for all collections of named chart elements. Performs the name management and enforces the uniquness of the names
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class ChartNamedElementCollection<T> : ChartElementCollection<T>, INameController
    where T : ChartNamedElement
{
    #region " fields "
#pragma warning disable IDE1006 // Naming Styles
    private List<T> _cachedState;
    private int _disableDeleteCount;
#pragma warning restore IDE1006 // Naming Styles
    #endregion

    #region " properties "

    /// <summary>
    /// Gets the name prefix that is used to create unique chart element names.
    /// </summary>
    /// <value>The default name prefix of the chart elements stored in the collection.</value>
    protected virtual string NamePrefix => typeof( T ).Name;

    /// <summary>
    /// Gets or sets the chart element with the specified name.
    /// </summary>
    /// <value></value>
    public T this[string name]
    {
        get
        {
            int index = this.IndexOf( name );
            return index != -1 ? this[index] : throw new ArgumentException( SR.ExceptionNameNotFound( name, this.GetType().Name ) );
        }
        set
        {
            int nameIndex = this.IndexOf( name );
            int itemIndex = this.IndexOf( value );
            bool nameFound = nameIndex > -1;
            bool itemFound = itemIndex > -1;

            if ( !nameFound && !itemFound )
                this.Add( value );

            else if ( nameFound && !itemFound )
                this[nameIndex] = value;

            else if ( !nameFound && itemFound )
                throw new ArgumentException( SR.ExceptionNameAlreadyExistsInCollection( name, this.GetType().Name ) );

            else if ( nameFound && itemFound && nameIndex != itemIndex )
                throw new ArgumentException( SR.ExceptionNameAlreadyExistsInCollection( name, this.GetType().Name ) );

        }
    }
    #endregion

    #region " constructtion "

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartNamedElementCollection{T}"/> class.
    /// </summary>
    /// <param name="parent">The parent chart element.</param>
    internal ChartNamedElementCollection( IChartElement parent )
        : base( parent )
    {
    }

    #endregion

    #region " events "

    internal event EventHandler<NameReferenceChangedEventArgs> NameReferenceChanged;
    internal event EventHandler<NameReferenceChangedEventArgs> NameReferenceChanging;

    #endregion

    #region " methods "

    /// <summary>
    /// Determines whether the chart element with the specified name already exists in the collection.
    /// </summary>
    /// <param name="name">The new chart element name.</param>
    /// <returns>
    /// 	<c>true</c> if new chart element name is unique; otherwise, <c>false</c>.
    /// </returns>
    public virtual bool IsUniqueName( string name )
    {
        return this.FindByName( name ) == null;
    }

    /// <summary>
    /// Finds the unique name for a new element being added to the collection
    /// </summary>
    /// <returns>Next unique chart element name</returns>
    public virtual string NextUniqueName()
    {
        // Find unique name
        string result = string.Empty;
        string prefix = this.NamePrefix;
        for ( int i = 1; i < int.MaxValue; i++ )
        {
            result = prefix + i.ToString( CultureInfo.InvariantCulture );
            // Check whether the name is unique
            if ( this.IsUniqueName( result ) )
            {
                break;
            }
        }
        return result;
    }

    /// <summary>
    /// Indexes the of chart element with the specified name.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public int IndexOf( string name )
    {
        int i = 0;
        foreach ( T namedObj in this )
        {
            if ( namedObj.Name == name )
                return i;
            i++;
        }
        return -1;
    }

    /// <summary>
    /// Verifies the name reference to a chart named element stored in this collection and throws the argument exception if its not valid.
    /// </summary>
    /// <param name="name">Chart element name.</param>
    internal void VerifyNameReference( string name )
    {
        if ( this.Chart != null && !this.Chart.serializing && !this.IsNameReferenceValid( name ) )
            throw new ArgumentException( SR.ExceptionNameNotFound( name, this.GetType().Name ) );
    }

    /// <summary>
    /// Verifies the name reference to a chart named element stored in this collection.
    /// </summary>
    /// <param name="name">Chart element name.</param>
    internal bool IsNameReferenceValid( string name )
    {
        return string.IsNullOrEmpty( name ) ||
                name == Constants.NotSetValue ||
                this.IndexOf( name ) >= 0;
    }

    /// <summary>
    /// Finds the chart element by the name.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public virtual T FindByName( string name )
    {
        foreach ( T namedObj in this )
        {
            if ( namedObj.Name == name )
                return namedObj;
        }
        return null;
    }

    /// <summary>
    /// Inserts the specified item in the collection at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index where the item is to be inserted.</param>
    /// <param name="item">The object to insert.</param>
    protected override void InsertItem( int index, T item )
    {
        if ( string.IsNullOrEmpty( item.Name ) )
            item.Name = this.NextUniqueName();
        else if ( !this.IsUniqueName( item.Name ) )
            throw new ArgumentException( SR.ExceptionNameAlreadyExistsInCollection( item.Name, this.GetType().Name ) );

        //If the item references other named references we might need to fix the references
        this.FixNameReferences( item );

        base.InsertItem( index, item );

        if ( this.Count == 1 && item is not null )
        {
            // First element is added to the list -> fire the NameReferenceChanged event to update all the dependent elements
            (( INameController ) this).OnNameReferenceChanged( new NameReferenceChangedEventArgs( null, item ) );
        }
    }

    /// <summary>
    /// Replaces the element at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index of the element to replace.</param>
    /// <param name="item">The new value for the element at the specified index.</param>
    protected override void SetItem( int index, T item )
    {
        if ( string.IsNullOrEmpty( item.Name ) )
            item.Name = this.NextUniqueName();
        else if ( !this.IsUniqueName( item.Name ) && this.IndexOf( item.Name ) != index )
            throw new ArgumentException( SR.ExceptionNameAlreadyExistsInCollection( item.Name, this.GetType().Name ) );

        //If the item references other named references we might need to fix the references
        this.FixNameReferences( item );

        // Remember the removedElement
        ChartNamedElement removedElement = index < this.Count ? this[index] : null;

        (( INameController ) this).OnNameReferenceChanging( new NameReferenceChangedEventArgs( removedElement, item ) );
        base.SetItem( index, item );
        // Fire the NameReferenceChanged event to update all the dependent elements
        (( INameController ) this).OnNameReferenceChanged( new NameReferenceChangedEventArgs( removedElement, item ) );
    }

    /// <summary>
    /// Removes the element at the specified index of the collection.
    /// </summary>
    /// <param name="index">The zero-based index of the element to remove.</param>
    protected override void RemoveItem( int index )
    {
        // Remember the removedElement
        ChartNamedElement removedElement = index < this.Count ? this[index] : null;
        if ( this._disableDeleteCount == 0 )
        {
            (( INameController ) this).OnNameReferenceChanged( new NameReferenceChangedEventArgs( removedElement, null ) );
        }
        base.RemoveItem( index );
        if ( this._disableDeleteCount == 0 )
        {
            // All elements referencing the removed element will be redirected to the first element in collection
            // Fire the NameReferenceChanged event to update all the dependent elements
            ChartNamedElement defaultElement = this.Count > 0 ? this[0] : null;
            (( INameController ) this).OnNameReferenceChanged( new NameReferenceChangedEventArgs( removedElement, defaultElement ) );
        }
    }

    /// <summary>
    /// Fixes the name references of the item.
    /// </summary>
    internal virtual void FixNameReferences( T item )
    {
        //Nothing to fix at the base class...
    }

    #endregion

    #region " inamecontroller members "

    /// <summary>
    /// Determines whether is the name us unique.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns>
    /// 	<c>true</c> if is the name us unique; otherwise, <c>false</c>.
    /// </returns>
    bool INameController.IsUniqueName( string name )
    {
        return this.IsUniqueName( name );
    }

    /// <summary>
    /// Gets or sets a value indicating whether this instance is in edit mode by collecrtion editor.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance the colection is editing; otherwise, <c>false</c>.
    /// </value>
    bool INameController.IsColectionEditing
    {
        get => this._disableDeleteCount == 0;
        set => this._disableDeleteCount += value ? 1 : -1;
    }

    /// <summary>
    /// Raises the <see cref="E:NameReferenceChanging"/> event.
    /// </summary>
    /// <param name="e">The <see cref="NameReferenceChangedEventArgs"/> instance containing the event data.</param>
    void INameController.OnNameReferenceChanging( NameReferenceChangedEventArgs e )
    {
        if ( !this.IsSuspended )
        {
            this.NameReferenceChanging?.Invoke( this, e );
        }
    }

    /// <summary>
    /// Raises the <see cref="E:NameReferenceChanged"/> event.
    /// </summary>
    /// <param name="e">The <see cref="NameReferenceChangedEventArgs"/> instance containing the event data.</param>
    void INameController.OnNameReferenceChanged( NameReferenceChangedEventArgs e )
    {
        if ( !this.IsSuspended )
        {
            this.NameReferenceChanged?.Invoke( this, e );
        }
    }

    /// <summary>
    /// Does the snapshot of collection items.
    /// </summary>
    /// <param name="save">if set to <c>true</c> collection items will be saved.</param>
    /// <param name="changingCallback">The changing callback.</param>
    /// <param name="changedCallback">The changed callback.</param>
    void INameController.DoSnapshot( bool save,
        EventHandler<NameReferenceChangedEventArgs> changingCallback,
        EventHandler<NameReferenceChangedEventArgs> changedCallback )
    {
        if ( save )
        {
            this._cachedState = new List<T>( this );
            if ( changingCallback is not null ) this.NameReferenceChanging += changingCallback;
            if ( changedCallback is not null ) this.NameReferenceChanged += changedCallback;
        }
        else
        {
            if ( changingCallback is not null ) this.NameReferenceChanging -= changingCallback;
            if ( changedCallback is not null ) this.NameReferenceChanged -= changedCallback;
            this._cachedState.Clear();
            this._cachedState = null;
        }
    }

    /// <summary>
    /// Gets the snapshot of saved collection items.
    /// </summary>
    /// <value>The snapshot.</value>
    IList INameController.Snapshot => this._cachedState;


    #endregion
}
