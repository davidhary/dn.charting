//
//  Purpose:	Every property in the chart references images by names. 
//              This means that you can set MarkerImage property to a 
//              full image path or URL. In case when the user wants to 
//              dynamically generate an image or load it from other 
//              location (like database) you can use named image 
//              collection which is exposed as Images property of the 
//              chart. Any Image can be added to this collection with 
//              unique name and than this name can be used in all the 
//              chart properties which require image names.
//


using System.ComponentModel;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// The NamedImagesCollection class is a strongly typed collection of NamedImage
/// objects.
/// </summary>
public class NamedImagesCollection : ChartNamedElementCollection<NamedImage>
{
    #region " constructor "

    /// <summary>
    /// Constructor
    /// </summary>
    internal NamedImagesCollection() : base( null )
    {
    }

    #endregion
}
/// <summary>
/// The NamedImage class stores a single Image with its unique name.
/// </summary>
[
    SRDescription( "DescriptionAttributeNamedImage_NamedImage" ),
    DefaultProperty( "Name" ),
]
public class NamedImage : ChartNamedElement
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    private string _name = string.Empty;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructor "

    /// <summary>
    /// NamedImage constructor.
    /// </summary>
    public NamedImage()
    {
    }

    /// <summary>
    /// NamedImage constructor.
    /// </summary>
    /// <param name="name">Image name.</param>
    /// <param name="image">Image object.</param>
    public NamedImage( string name, Drawing.Image image )
    {
        this._name = name;
        this.Image = image;
    }

    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets the image name.
    /// </summary>
    [
    Bindable( false ),
    SRDescription( "DescriptionAttributeNamedImage_Name" ),
    ]
    public override string Name
    {
        get => this._name;
        set => this._name = value;
    }

    /// <summary>
    /// Gets or sets the image object.
    /// </summary>
    [
    Bindable( false ),
    SRDescription( "DescriptionAttributeNamedImage_Image" ),
    ]
    public Drawing.Image Image { get; set; } = null;

    #endregion

    #region " idisposable members "
    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            // Dispose managed resources
            if ( this.Image is not null )
            {
                this.Image.Dispose();
                this.Image = null;
            }
        }
        base.Dispose( disposing );
    }

    #endregion
}
