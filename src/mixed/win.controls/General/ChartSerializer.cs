//
//  Purpose:	Serialization saves the state of the chart and also 
//              provides the ability to load the serialized data back
//              into the chart. All chart properties can be persisted, 
//              including the chart's data.
//


using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Xml;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " serialization enumeration "
/// <summary>
/// An enumeration of the formats of the chart serializer.
/// </summary>
public enum SerializationFormat
{
    /// <summary>
    /// XML serializer format.
    /// </summary>
    Xml,

    /// <summary>
    /// Binary serializer format.
    /// </summary>
    Binary
}
/// <summary>
/// An enumeration of chart serializable content definition flags
/// </summary>
[Flags]
public enum SerializationContents
{
    /// <summary>
    /// Default content.
    /// </summary>
    Default = 1,

    /// <summary>
    /// Serialize only series data.
    /// </summary>
    Data = 2,

    /// <summary>
    /// Serialize chart visual appearance (e.g. Colors, Line Styles).
    /// </summary>
    Appearance = 4,

    /// <summary>
    /// All content is serialized. 
    /// </summary>
    All = Default | Data | Appearance

}

#endregion
/// <summary>
/// ChartSerializer class provides chart serialization.
/// </summary>
[
    SRDescription( "DescriptionAttributeChartSerializer_ChartSerializer" ),
    DefaultProperty( "Format" ),
]
public class ChartSerializer
{
    #region " private fields "

#pragma warning disable IDE1006 // Naming Styles
    // Reference to the service container
    private readonly IServiceContainer _serviceContainer;

    // Reference to the chart object
    private Chart _chart;

    // Reference to the serializer object

    private SerializerBase _serializer = new XmlFormatSerializer();

    // Format of the serializer in use
    private SerializationFormat _format = SerializationFormat.Xml;

    // Serialization content 
    private SerializationContents _content = SerializationContents.Default;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " constructors and service provider methods "

    /// <summary>
    /// Default constructor is unavailable
    /// </summary>
    private ChartSerializer()
    {
    }

    /// <summary>
    /// Internal constructor
    /// </summary>
    /// <param name="container">Service container reference.</param>
    internal ChartSerializer( IServiceContainer container )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( container, SR.ExceptionInvalidServiceContainer );
#else
        if ( container == null ) throw new ArgumentNullException( nameof( container ), SR.ExceptionInvalidServiceContainer );
#endif
        this._serviceContainer = container;
    }

    /// <summary>
    /// Returns ChartSerializer service object
    /// </summary>
    /// <param name="serviceType">Requested service type.</param>
    /// <returns>ChartSerializer service object.</returns>
    internal object GetService( Type serviceType )
    {
        return serviceType == typeof( ChartSerializer )
            ? this
            : throw (new ArgumentException( SR.ExceptionChartSerializerUnsupportedType( serviceType.ToString() ) ));
    }

    #endregion

    #region " public properties "

    /// <summary>
    /// Gets or sets the serializable content.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( typeof( SerializationContents ), "Default" ),
    SRDescription( "DescriptionAttributeChartSerializer_Content" )
    ]
    public SerializationContents Content
    {
        get => this._content;
        set
        {
            // Set content value
            this._content = value;

            // Automatically set SerializableContent and NonSerializableContent properties
            this.SetSerializableContent();
        }
    }

    /// <summary>
    /// Gets or sets the format used to serialize the chart data.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( typeof( SerializationFormat ), "Xml" ),
    SRDescription( "DescriptionAttributeChartSerializer_Format" )
    ]
    public SerializationFormat Format
    {
        get => this._format;
        set
        {
            if ( this._format != value )
            {
                this._format = value;

                // Create new serializer object
                SerializerBase newSerializer = this._format == SerializationFormat.Binary ? new BinaryFormatSerializer() : new XmlFormatSerializer();
                // Copy serializer settings
                newSerializer.IsUnknownAttributeIgnored = this._serializer.IsUnknownAttributeIgnored;
                newSerializer.NonSerializableContent = this._serializer.NonSerializableContent;
                newSerializer.IsResetWhenLoading = this._serializer.IsResetWhenLoading;
                newSerializer.SerializableContent = this._serializer.SerializableContent;
                this._serializer = newSerializer;
            }
        }
    }

    /// <summary>
    /// Gets or sets a flag which indicates whether object properties are reset to default
    /// values before loading.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( true ),
    SRDescription( "DescriptionAttributeChartSerializer_ResetWhenLoading" )
    ]
    public bool IsResetWhenLoading
    {
        get => this._serializer.IsResetWhenLoading;
        set => this._serializer.IsResetWhenLoading = value;
    }



    /// <summary>
    /// Gets or sets a flag which indicates whether unknown XML properties and elements will be 
    /// ignored without throwing an exception.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeChartSerializer_IgnoreUnknownXmlAttributes" )
    ]
    public bool IsUnknownAttributeIgnored
    {
        get => this._serializer.IsUnknownAttributeIgnored;
        set => this._serializer.IsUnknownAttributeIgnored = value;
    }

    /// <summary>
    /// Gets or sets a flag which indicates whether chart 
    /// serializer is working in template creation mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeChartSerializer_TemplateMode" )
    ]
    public bool IsTemplateMode
    {
        get => this._serializer.IsTemplateMode;
        set => this._serializer.IsTemplateMode = value;
    }



    /// <summary>
    /// Gets or sets the chart properties that can be serialized.
    /// Comma separated list of serializable (Save/Load/Reset) properties. 
    /// "ClassName.PropertyName,[ClassName.PropertyName]".
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeChartSerializer_SerializableContent" )
    ]
    public string SerializableContent
    {
        get => this._serializer.SerializableContent;
        set => this._serializer.SerializableContent = value;
    }

    /// <summary>
    /// Gets or sets the chart properties that will not be serialized.
    /// Comma separated list of non-serializable (Save/Load/Reset) properties. 
    /// "ClassName.PropertyName,[ClassName.PropertyName]".
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeChartSerializer_NonSerializableContent" )
    ]
    public string NonSerializableContent
    {
        get => this._serializer.NonSerializableContent;
        set => this._serializer.NonSerializableContent = value;
    }

    #endregion

    #region " public methods "

    /// <summary>
    /// This method resets all properties of the chart to default values. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be reset.
    /// </summary>
    public void Reset()
    {
        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Resetting;
        }

        // Reset properties
        this._serializer.ResetObjectProperties( this.GetChartObject() );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
        }
    }

    /// <summary>
    /// This method saves all properties of the chart into a file. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be saved.
    /// </summary>
    /// <param name="fileName">The file name used to write the data.</param>
    public void Save( string fileName )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( fileName, nameof( fileName ) );
#else
        if ( fileName == null ) throw new ArgumentNullException( nameof( fileName ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Saving;
            //GetChartObject().BeginInit();
        }

        // Reset all auto-detected properties values
        this.GetChartObject().ResetAutoValues();

        // Serialize chart data
        this._serializer.Serialize( this.GetChartObject(), fileName );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
            //GetChartObject().EndInit();
        }
    }

    /// <summary>
    /// This method saves all properties of the chart into a stream.  By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be saved.
    /// </summary>
    /// <param name="stream">The stream where to save the data.</param>
    public void Save( Stream stream )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( stream, nameof( stream ) );
#else
        if ( stream == null ) throw new ArgumentNullException( nameof( stream ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Saving;
            //GetChartObject().BeginInit();
        }

        // Reset all auto-detected properties values
        this.GetChartObject().ResetAutoValues();

        // Serialize chart data
        this._serializer.Serialize( this.GetChartObject(), stream );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
            //GetChartObject().EndInit();
        }
    }

    /// <summary>
    /// This method saves all properties of the chart into an XML writer. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be saved.
    /// </summary>
    /// <param name="writer">XML writer to save the data.</param>
    public void Save( XmlWriter writer )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( writer, nameof( writer ) );
#else
        if ( writer == null ) throw new ArgumentNullException( nameof( writer ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Saving;
            //GetChartObject().BeginInit();
        }

        // Reset all auto-detected properties values
        this.GetChartObject().ResetAutoValues();

        // Serialize chart data
        this._serializer.Serialize( this.GetChartObject(), writer );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
            //GetChartObject().EndInit();
        }
    }

    /// <summary>
    /// This method saves all properties of the chart into a text writer.  By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be saved.
    /// </summary>
    /// <param name="writer">Text writer to save the data.</param>
    public void Save( TextWriter writer )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( writer, nameof( writer ) );
#else
        if ( writer == null ) throw new ArgumentNullException( nameof( writer ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Saving;
            //GetChartObject().BeginInit();
        }

        // Reset all auto-detected properties values
        this.GetChartObject().ResetAutoValues();

        // Serialize chart data
        this._serializer.Serialize( this.GetChartObject(), writer );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
            //GetChartObject().EndInit();
        }
    }

    /// <summary>
    /// This method loads all properties of the chart from a file. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be loaded.
    /// </summary>
    /// <param name="fileName">The file to load the data from.</param>
    public void Load( string fileName )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( fileName, nameof( fileName ) );
#else
        if ( fileName == null ) throw new ArgumentNullException( nameof( fileName ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Loading;
        }

        this._serializer.Deserialize( this.GetChartObject(), fileName );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
        }
    }

    /// <summary>
    /// This method loads all properties of the chart from a stream. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be loaded.
    /// </summary>
    /// <param name="stream">The stream to load the data from.</param>
    public void Load( Stream stream )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( stream, nameof( stream ) );
#else
        if ( stream == null ) throw new ArgumentNullException( nameof( stream ) );
#endif


        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Loading;
        }

        this._serializer.Deserialize( this.GetChartObject(), stream );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
        }
    }

    /// <summary>
    /// This method loads all properties of the chart from an XML reader. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be loaded.
    /// </summary>
    /// <param name="reader">The XML reader to load the data from.</param>
    public void Load( XmlReader reader )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( reader, nameof( reader ) );
#else
        if ( reader == null ) throw new ArgumentNullException( nameof( reader ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Loading;
        }

        this._serializer.Deserialize( this.GetChartObject(), reader );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
        }
    }

    /// <summary>
    /// This method loads all properties of the chart from the text reader. By setting Content or 
    /// SerializableContent/NonSerializableContent properties, specific set of 
    /// properties can be loaded.
    /// </summary>
    /// <param name="reader">The text reader to load the data from.</param>
    public void Load( TextReader reader )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( reader, nameof( reader ) );
#else
        if ( reader == null ) throw new ArgumentNullException( nameof( reader ) );
#endif

        // Set serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = true;
            this.GetChartObject().serializationStatus = SerializationStatus.Loading;
        }

        this._serializer.Deserialize( this.GetChartObject(), reader );

        // Clear serializing flag
        if ( this.GetChartObject() is not null )
        {
            this.GetChartObject().serializing = false;
            this.GetChartObject().serializationStatus = SerializationStatus.None;
        }
    }

    #endregion

    #region " protected helper methods "

    /// <summary>
    /// Sets SerializableContent and NonSerializableContent properties
    /// depending on the flags in the Content property.
    /// </summary>
    internal void SetSerializableContent()
    {
        // Reset content definition strings
        this.SerializableContent = "";
        this.NonSerializableContent = "";

        // Loop through all enumeration flags
#pragma warning disable CA2263
        Array enumValues = Enum.GetValues( typeof( SerializationContents ) );
#pragma warning restore CA2263
        foreach ( object flagObject in enumValues )
        {
            if ( flagObject is SerializationContents )
            {
                // Check if flag currently set
                SerializationContents flag = ( SerializationContents ) flagObject;
                if ( (this.Content & flag) == flag &&
                    flag != SerializationContents.All &&
                    this.Content != SerializationContents.All )
                {
                    // Add comma at the end of existing string
                    if ( this.NonSerializableContent.Length != 0 )
                    {
                        this.NonSerializableContent += ", ";
                    }

                    // Add serializable class/properties names
                    this.NonSerializableContent += this.GetContentString( flag, false );
                    this.NonSerializableContent = this.NonSerializableContent.TrimStart( ',' );

                    // Add comma at the end of existing string
                    if ( this.SerializableContent.Length != 0 )
                    {
                        this.SerializableContent += ", ";
                    }

                    // Add serializable class/properties names
                    this.SerializableContent += this.GetContentString( flag, true );
                    this.SerializableContent = this.SerializableContent.TrimStart( ',' );
                }
            }
        }
    }

    /// <summary>
    /// Return a serializable or non serializable class/properties names
    /// for the specific flag.
    /// </summary>
    /// <param name="content">Serializable content</param>
    /// <param name="serializable">True - get serializable string, False - non serializable.</param>
    /// <returns>Serializable or non serializable string with class/properties names.</returns>
    protected string GetContentString( SerializationContents content, bool serializable )
    {
        switch ( content )
        {
            case SerializationContents.All:
                return "";
            case SerializationContents.Default:
                return "";
            case SerializationContents.Data:
                if ( serializable )
                {
                    return
                        "Chart.BuildNumber, " +
                        "Chart.Series, " +
                        "Series.Points, " +
                        "Series.Name, " +
                        "DataPoint.XValue, " +
                        "DataPoint.YValues," +
                        "DataPoint.LabelStyle," +
                        "DataPoint.AxisLabel," +
                        "DataPoint.LabelFormat," +
                        "DataPoint.IsEmpty, " +
                        "Series.YValuesPerPoint, " +
                        "Series.IsXValueIndexed, " +
                        "Series.XValueType, " +
                        "Series.YValueType";
                }
                return "";
            case SerializationContents.Appearance:
                if ( serializable )
                {
                    return
                        "Chart.BuildNumber, " +
                        "*.Name*, " +
                        "*.Fore*, " +
                        "*.Back*, " +
                        "*.Border*, " +
                        "*.Line*, " +
                        "*.Frame*, " +
                        "*.PageColor*, " +
                        "*.SkinStyle*, " +
                        "*.Palette, " +
                        "*.PaletteCustomColors, " +
                        "*.Font*, " +
                        "*.*Font, " +
                        "*.Color, " +
                        "*.Shadow*, " +
                        "*.MarkerColor, " +
                        "*.MarkerStyle, " +
                        "*.MarkerSize, " +
                        "*.MarkerBorderColor, " +
                        "*.MarkerImage, " +
                        "*.MarkerImageTransparentColor, " +
                        "*.LabelBackColor, " +
                        "*.LabelBorder*, " +
                        "*.Enable3D, " +
                        "*.IsRightAngleAxes, " +
                        "*.IsClustered, " +
                        "*.LightStyle, " +
                        "*.Perspective, " +
                        "*.Inclination, " +
                        "*.Rotation, " +
                        "*.PointDepth, " +
                        "*.PointGapDepth, " +
                        "*.WallWidth";
                }
                return "";

            default:
                throw new InvalidOperationException( SR.ExceptionChartSerializerContentFlagUnsupported );
        }
    }

    /// <summary>
    /// Returns chart object for serialization.
    /// </summary>
    /// <returns>Chart object.</returns>
    internal Chart GetChartObject()
    {
        this._chart ??= ( Chart ) this._serviceContainer.GetService( typeof( Chart ) );
        return this._chart;
    }

    #endregion
}
