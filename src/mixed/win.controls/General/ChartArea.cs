//
//  Purpose:	The ChartArea class represents one chart area within 
//              a chart image, and is used to plot one or more chart 
//              series. The number of chart series that can be plotted 
//              in a chart area is unlimited.
//


using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " chart area aligment enumerations "
/// <summary>
/// An enumeration of the alignment orientations of a ChartArea
/// </summary>
[Flags]
public enum AreaAlignmentOrientations
{
    /// <summary>
    /// Chart areas are not automatically aligned.
    /// </summary>
    None = 0,

    /// <summary>
    /// Chart areas are aligned vertically.
    /// </summary>
    Vertical = 1,

    /// <summary>
    /// Chart areas are aligned horizontally.
    /// </summary>
    Horizontal = 2,

    /// <summary>
    /// Chart areas are aligned using all values (horizontally and vertically).
    /// </summary>
    All = Vertical | Horizontal
}
/// <summary>
/// An enumeration of the alignment styles of a ChartArea
/// </summary>
[Flags]
public enum AreaAlignmentStyles
{
    /// <summary>
    /// Chart areas are not automatically aligned.
    /// </summary>
    None = 0,

    /// <summary>
    /// Chart areas are aligned by positions.
    /// </summary>
    Position = 1,

    /// <summary>
    /// Chart areas are aligned by inner plot positions.
    /// </summary>
    PlotPosition = 2,

    /// <summary>
    /// Chart areas are aligned by axes views.
    /// </summary>
    AxesView = 4,

    /// <summary>
    /// Cursor and Selection alignment.
    /// </summary>
    Cursor = 8,

    /// <summary>
    /// Complete alignment.
    /// </summary>
    All = Position | PlotPosition | Cursor | AxesView
}

#endregion
/// <summary>
/// The ChartArea class is used to create and display a chart 
/// area within a chart image. The chart area is a rectangular 
/// area on a chart image.  It has 4 axes, horizontal and vertical grids. 
/// A chart area can contain more than one different chart type.  
/// The number of chart series that can be plotted in a chart area 
/// is unlimited.
/// 
/// ChartArea class exposes all the properties and methods
/// of its base ChartArea3D class.
/// </summary>
[
DefaultProperty( "Axes" ),
SRDescription( "DescriptionAttributeChartArea_ChartArea" ),
]
public partial class ChartArea : ChartNamedElement
{
    #region " chart area fields "

#pragma warning disable IDE1006 // Naming Styles

    /// <summary>
    /// Plot area position
    /// </summary>
    internal ElementPosition PlotAreaPosition;

    // Private data members, which store properties values
    private Axis[] _axisArray = new Axis[4];
    private Color _backColor = Color.Empty;
    private bool _backColorIsSet;
    private ChartHatchStyle _backHatchStyle = ChartHatchStyle.None;
    private string _backImage = "";
    private ChartImageWrapMode _backImageWrapMode = ChartImageWrapMode.Tile;
    private Color _backImageTransparentColor = Color.Empty;
    private ChartImageAlignmentStyle _backImageAlignment = ChartImageAlignmentStyle.TopLeft;
    private GradientStyle _backGradientStyle = GradientStyle.None;
    private Color _backSecondaryColor = Color.Empty;
    private Color _borderColor = Color.Black;
    private int _borderWidth = 1;
    private ChartDashStyle _borderDashStyle = ChartDashStyle.NotSet;
    private int _shadowOffset;
    private Color _shadowColor = Color.FromArgb( 128, 0, 0, 0 );
    private ElementPosition _areaPosition;
    private ElementPosition _innerPlotPosition;
    internal int IterationCounter;

    private bool _isSameFontSizeForAllAxes;
    internal float axesAutoFontSize = 8f;

    private string _alignWithChartArea = Constants.NotSetValue;
    private AreaAlignmentOrientations _alignmentOrientation = AreaAlignmentOrientations.Vertical;
    private AreaAlignmentStyles _alignmentStyle = AreaAlignmentStyles.All;
    private int _circularSectorNumber = int.MinValue;
    private int _circularUsePolygons = int.MinValue;

    // Flag indicates that chart area is acurrently aligned
    internal bool alignmentInProcess;

    // Chart area position before adjustments
    internal RectangleF originalAreaPosition = RectangleF.Empty;

    // Chart area inner plot position before adjustments
    internal RectangleF originalInnerPlotPosition = RectangleF.Empty;

    // Chart area position before adjustments
    internal RectangleF lastAreaPosition = RectangleF.Empty;


    // Center of the circulat chart area
    internal PointF circularCenter = PointF.Empty;

    private ArrayList _circularAxisList;

    // Buffered plotting area image
    internal Bitmap areaBufferBitmap;

    private Cursor _cursorX = new();
    private Cursor _cursorY = new();

    // Area SmartLabel class
    internal SmartLabel smartLabels = new();

    // Gets or sets a flag that specifies whether the chart area is visible.
    private bool _visible = true;

#pragma warning disable IDE1006 // Naming Styles

    #endregion

    #region " chart area cursor properties "

    /// <summary>
    /// Gets or sets a Cursor object that is used for cursors and selected ranges along the X-axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeCursor" ),
    Bindable( true ),
    DefaultValue( null ),
    SRDescription( "DescriptionAttributeChartArea_CursorX" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) ),
    ]
    public Cursor CursorX
    {
        get => this._cursorX;
        set
        {
            this._cursorX = value;

            // Initialize chart object
            this._cursorX.Initialize( this, AxisName.X );
        }
    }

    /// <summary>
    /// Gets or sets a Cursor object that is used for cursors and selected ranges along the Y-axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeCursor" ),
    Bindable( true ),
    DefaultValue( null ),
    SRDescription( "DescriptionAttributeChartArea_CursorY" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) ),
    ]
    public Cursor CursorY
    {
        get => this._cursorY;
        set
        {
            this._cursorY = value;

            // Initialize chart object
            this._cursorY.Initialize( this, AxisName.Y );
        }
    }

    #endregion

    #region " chart area properties "

    /// <summary>
    /// Gets or sets a flag that specifies whether the chart area is visible.
    /// </summary>
    /// <remarks>
    /// When this flag is set to false, all series, legends, titles and annotation objects 
    /// associated with the chart area will also be hidden.
    /// </remarks>
    /// <value>
    /// <b>True</b> if the chart area is visible; <b>false</b> otherwise.
    /// </value>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DefaultValue( true ),
    SRDescription( "DescriptionAttributeChartArea_Visible" ),
    ParenthesizePropertyName( true ),
    ]
    public virtual bool Visible
    {
        get => this._visible;
        set
        {
            this._visible = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the name of the ChartArea object to which this chart area should be aligned.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAlignment" ),
    Bindable( true ),
    DefaultValue( Constants.NotSetValue ),
    SRDescription( "DescriptionAttributeChartArea_alignWithChartArea" ),
    TypeConverter( typeof( LegendAreaNameConverter ) )
    ]
    public string AlignWithChartArea
    {
        get => this._alignWithChartArea;
        set
        {
            if ( value != this._alignWithChartArea )
            {
                if ( string.IsNullOrEmpty( value ) )
                {
                    this._alignWithChartArea = Constants.NotSetValue;
                }
                else
                {
                    if ( this.Chart != null && this.Chart.ChartAreas is not null )
                    {
                        this.Chart.ChartAreas.VerifyNameReference( value );
                    }
                    this._alignWithChartArea = value;
                }
                this.Invalidate();
            }
        }
    }

    /// <summary>
    /// Gets or sets the alignment orientation of a chart area.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAlignment" ),
    Bindable( true ),
    DefaultValue( AreaAlignmentOrientations.Vertical ),
    SRDescription( "DescriptionAttributeChartArea_alignOrientation" ),
    Editor( typeof( FlagsEnumUITypeEditor ), typeof( UITypeEditor ) )
    ]
    public AreaAlignmentOrientations AlignmentOrientation
    {
        get => this._alignmentOrientation;
        set
        {
            this._alignmentOrientation = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the alignment style of the ChartArea.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAlignment" ),
    Bindable( true ),
    DefaultValue( AreaAlignmentStyles.All ),
    SRDescription( "DescriptionAttributeChartArea_alignType" ),
    Editor( typeof( FlagsEnumUITypeEditor ), typeof( UITypeEditor ) )
    ]
    public AreaAlignmentStyles AlignmentStyle
    {
        get => this._alignmentStyle;
        set
        {
            this._alignmentStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets an array that represents all axes for a chart area.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAxes" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartArea_axes" ),
    TypeConverter( typeof( AxesArrayConverter ) ),
    Editor( typeof( AxesArrayEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public Axis[] Axes
    {
        get => this._axisArray;
        set
        {
            this.AxisX = value[0];
            this.AxisY = value[1];
            this.AxisX2 = value[2];
            this.AxisY2 = value[3];
            this.Invalidate();
        }
    }

    /// <summary>
    /// Avoid serialization of the axes array
    /// </summary>
    [EditorBrowsable( EditorBrowsableState.Never )]
    internal bool ShouldSerializeAxes()
    {
        return false;
    }

    /// <summary>
    /// Gets or sets an Axis object that represents the primary Y-axis. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAxis" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeChartArea_axisY" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public Axis AxisY
    {
        get => this.axisY;
        set
        {
            this.axisY = value;
            this.axisY.Initialize( this, AxisName.Y );
            this._axisArray[1] = this.axisY;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets an Axis object that represents the primary X-axis. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAxis" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeChartArea_axisX" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public Axis AxisX
    {
        get => this.axisX;
        set
        {
            this.axisX = value;
            this.axisX.Initialize( this, AxisName.X );
            this._axisArray[0] = this.axisX;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets an Axis object that represents the secondary X-axis. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAxis" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeChartArea_axisX2" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public Axis AxisX2
    {
        get => this.axisX2;
        set
        {
            this.axisX2 = value;
            this.axisX2.Initialize( this, AxisName.X2 );
            this._axisArray[2] = this.axisX2;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets an Axis object that represents the secondary Y-axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAxis" ),
    Bindable( true ),
    Browsable( false ),
    SRDescription( "DescriptionAttributeChartArea_axisY2" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public Axis AxisY2
    {
        get => this.axisY2;
        set
        {
            this.axisY2 = value;
            this.axisY2.Initialize( this, AxisName.Y2 );
            this._axisArray[3] = this.axisY2;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets an ElementPosition object, which defines the position of a chart area object within the chart image.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartArea_Position" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ElementPositionConverter ) ),
    SerializationVisibility( SerializationVisibility.Element )
    ]
    public ElementPosition Position
    {
        get
        {
            // Serialize only position values if Auto set to false
            if ( this.Chart != null && this.Chart.serializationStatus == SerializationStatus.Saving )
            {
                if ( this._areaPosition.Auto )
                {
                    return new ElementPosition();
                }
                else
                {
                    ElementPosition newPosition = new()
                    {
                        Auto = false
                    };
                    newPosition.SetPositionNoAuto( this._areaPosition.X, this._areaPosition.Y, this._areaPosition.Width, this._areaPosition.Height );
                    return newPosition;
                }
            }
            return this._areaPosition;
        }
        set
        {
            this._areaPosition = value;
            this._areaPosition.Parent = this;
            this._areaPosition.resetAreaAutoPosition = true;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Determoines if this position should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializePosition()
    {
        return !this.Position.Auto;
    }

    /// <summary>
    /// Gets or sets an ElementPosition object, which defines the inner plot position of a chart area object.  
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartArea_InnerPlotPosition" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ElementPositionConverter ) ),
    SerializationVisibility( SerializationVisibility.Element )
    ]
    public ElementPosition InnerPlotPosition
    {
        get
        {
            // Serialize only position values if Auto set to false
            if ( this.Common != null && this.Common.Chart != null && this.Common.Chart.serializationStatus == SerializationStatus.Saving )
            {
                if ( this._innerPlotPosition.Auto )
                {
                    return new ElementPosition();
                }
                else
                {
                    ElementPosition newPosition = new()
                    {
                        Auto = false
                    };
                    newPosition.SetPositionNoAuto( this._innerPlotPosition.X, this._innerPlotPosition.Y, this._innerPlotPosition.Width, this._innerPlotPosition.Height );
                    return newPosition;
                }
            }
            return this._innerPlotPosition;
        }
        set
        {
            this._innerPlotPosition = value;
            this._innerPlotPosition.Parent = this;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Determoines if this position should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeInnerPlotPosition()
    {
        return !this.InnerPlotPosition.Auto;
    }

    /// <summary>
    /// Gets or sets the background color of a ChartArea object. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeBackColor" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BackColor
    {
        get => SystemInformation.HighContrast && this._backColor.IsEmpty && !this._backColorIsSet
                ? Drawing.SystemColors.Control
                : this._backColor;
        set
        {
            this._backColor = value;

            this._backColorIsSet = true;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the hatching style of a ChartArea object.
    /// </summary>
    [


    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartHatchStyle.None ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackHatchStyle" ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) )
    ]
    public ChartHatchStyle BackHatchStyle
    {
        get => this._backHatchStyle;
        set
        {
            this._backHatchStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the background image of a ChartArea object. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeBackImage" ),
    Editor( typeof( ImageValueEditor ), typeof( UITypeEditor ) ),
    NotifyParentProperty( true )
    ]
    public string BackImage
    {
        get => this._backImage;
        set
        {
            this._backImage = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the drawing mode of the background image of a ChartArea object.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageWrapMode.Tile ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageWrapMode" ),
    ]
    public ChartImageWrapMode BackImageWrapMode
    {
        get => this._backImageWrapMode;
        set
        {
            this._backImageWrapMode = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the color of a ChartArea object's background image that will be drawn as transparent.  
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageTransparentColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BackImageTransparentColor
    {
        get => this._backImageTransparentColor;
        set
        {
            this._backImageTransparentColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the alignment of a ChartArea object. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageAlignmentStyle.TopLeft ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackImageAlign" ),
    ]
    public ChartImageAlignmentStyle BackImageAlignment
    {
        get => this._backImageAlignment;
        set
        {
            this._backImageAlignment = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the orientation of a chart element's gradient, 
    /// and also determines whether or not a gradient is used.  
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( GradientStyle.None ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackGradientStyle" ),
    Editor( typeof( GradientEditor ), typeof( UITypeEditor ) )
    ]
    public GradientStyle BackGradientStyle
    {
        get => this._backGradientStyle;
        set
        {
            this._backGradientStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the secondary color of a ChartArea object.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackSecondaryColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BackSecondaryColor
    {
        get => this._backSecondaryColor;
        set
        {
            this._backSecondaryColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the shadow color of a ChartArea object.  
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "128,0,0,0" ),
    SRDescription( "DescriptionAttributeShadowColor" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color ShadowColor
    {
        get => this._shadowColor;
        set
        {
            this._shadowColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the shadow offset (in pixels) of a ChartArea object.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 0 ),
    SRDescription( "DescriptionAttributeShadowOffset" ),
    NotifyParentProperty( true ),
    ]
    public int ShadowOffset
    {
        get => this._shadowOffset;
        set
        {
            this._shadowOffset = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the border color of a ChartArea object.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) )
    ]
    public Color BorderColor
    {
        get => this._borderColor;
        set
        {
            this._borderColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the border width of a ChartArea object.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeBorderWidth" ),
    NotifyParentProperty( true )
    ]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            if ( value < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionBorderWidthIsNegative );
            }
            this._borderWidth = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the style of the border line of a ChartArea object.
    /// </summary>
    [

    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartDashStyle.NotSet ),
    SRDescription( "DescriptionAttributeBorderDashStyle" ),
    NotifyParentProperty( true ),
    ]
    public ChartDashStyle BorderDashStyle
    {
        get => this._borderDashStyle;
        set
        {
            this._borderDashStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the unique name of a ChartArea object.
    /// </summary>
    [

    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartArea_Name" ),
    NotifyParentProperty( true ),
    ]
    public override string Name
    {
        get => base.Name;
        set => base.Name = value;
    }

    /// <summary>
    /// Gets or sets a Boolean that determines if the labels of the axes for all chart area
    /// , which have LabelsAutoFit property set to true, are of equal size.  
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeChartArea_EquallySizedAxesFont" ),
    NotifyParentProperty( true ),
    ]
    public bool IsSameFontSizeForAllAxes
    {
        get => this._isSameFontSizeForAllAxes;
        set
        {
            this._isSameFontSizeForAllAxes = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " constructtion "
    /// <summary>
    /// ChartArea constructor.
    /// </summary>
    public ChartArea() => this.Initialize();

    /// <summary>
    /// ChartArea constructor.
    /// </summary>
    /// <param name="name">The name.</param>
    public ChartArea( string name ) : base( name ) => this.Initialize();
    #endregion

    #region " chart area methods "
    /// <summary>
    /// Restores series order and X axis reversed mode for the 3D charts.
    /// </summary>
    internal void Restore3DAnglesAndReverseMode()
    {
        if ( this.Area3DStyle.Enable3D && !this.chartAreaIsCurcular )
        {
            // Restore axis "IsReversed" property and old Y angle
            this.AxisX.IsReversed = this.oldReverseX;
            this.AxisX2.IsReversed = this.oldReverseX;
            this.AxisY.IsReversed = this.oldReverseY;
            this.AxisY2.IsReversed = this.oldReverseY;
            this.Area3DStyle.Rotation = this.oldYAngle;
        }
    }

    /// <summary>
    /// Sets series order and X axis reversed mode for the 3D charts.
    /// </summary>
    internal void Set3DAnglesAndReverseMode()
    {
        // Clear series reversed flag
        this.ReverseSeriesOrder = false;

        // If 3D charting is enabled
        if ( this.Area3DStyle.Enable3D )
        {
            // Make sure primary & secondary axis has the same IsReversed settings
            // This is a limitation for the 3D chart required for labels drawing.
            this.AxisX2.IsReversed = this.AxisX.IsReversed;
            this.AxisY2.IsReversed = this.AxisY.IsReversed;

            // Remember reversed order of X & Y axis and Angles
            this.oldReverseX = this.AxisX.IsReversed;
            this.oldReverseY = this.AxisY.IsReversed;
            this.oldYAngle = this.Area3DStyle.Rotation;

            // Check if Y angle 
            if ( this.Area3DStyle.Rotation is > 90 or < (-90) )
            {
                // This method depends on the 'switchValueAxes' field which is calculated based on the chart types
                // of the series associated with the chart area. We need to call SetData method to make sure this field
                // is correctly initialized. Because we only need to collect information about the series, we pass 'false'
                // as parameters to limit the amount of work this function does.
                this.SetData( false, false );

                // Reversed series order
                this.ReverseSeriesOrder = true;

                // Reversed primary and secondary X axis
                if ( !this.switchValueAxes )
                {
                    this.AxisX.IsReversed = !this.AxisX.IsReversed;
                    this.AxisX2.IsReversed = !this.AxisX2.IsReversed;
                }

                // Reversed primary and secondary Y axis for chart types like Bar
                else
                {
                    this.AxisY.IsReversed = !this.AxisY.IsReversed;
                    this.AxisY2.IsReversed = !this.AxisY2.IsReversed;
                }

                // Adjust Y angle
                if ( this.Area3DStyle.Rotation > 90 )
                {
                    this.Area3DStyle.Rotation = this.Area3DStyle.Rotation - 90 - 90;
                }
                else if ( this.Area3DStyle.Rotation < -90 )
                {
                    this.Area3DStyle.Rotation = this.Area3DStyle.Rotation + 90 + 90;
                }
            }
        }
    }

    /// <summary>
    /// Save all automatic values like Minimum and Maximum.
    /// </summary>
    internal void SetTempValues()
    {
        // Save non automatic area position
        if ( !this.Position.Auto )
        {
            this.originalAreaPosition = this.Position.ToRectangleF();
        }

        // Save non automatic area inner plot position
        if ( !this.InnerPlotPosition.Auto )
        {
            this.originalInnerPlotPosition = this.InnerPlotPosition.ToRectangleF();
        }

        this._circularSectorNumber = int.MinValue;
        this._circularUsePolygons = int.MinValue;
        this._circularAxisList = null;

        // Save Minimum and maximum values for all axes
        this.axisX.StoreAxisValues();
        this.axisX2.StoreAxisValues();
        this.axisY.StoreAxisValues();
        this.axisY2.StoreAxisValues();
    }

    /// <summary>
    /// Load all automatic values like Minimum and Maximum with original values.
    /// </summary>
    internal void GetTempValues()
    {
        // Take Minimum and maximum values for all axes
        this.axisX.ResetAxisValues();
        this.axisX2.ResetAxisValues();
        this.axisY.ResetAxisValues();
        this.axisY2.ResetAxisValues();

        // Restore non automatic area position
        if ( !this.originalAreaPosition.IsEmpty )
        {
            this.lastAreaPosition = this.Position.ToRectangleF();
            this.Position.SetPositionNoAuto( this.originalAreaPosition.X, this.originalAreaPosition.Y, this.originalAreaPosition.Width, this.originalAreaPosition.Height );
            this.originalAreaPosition = RectangleF.Empty;
        }

        // Save non automatic area inner plot position
        if ( !this.originalInnerPlotPosition.IsEmpty )
        {
            this.InnerPlotPosition.SetPositionNoAuto( this.originalInnerPlotPosition.X, this.originalInnerPlotPosition.Y, this.originalInnerPlotPosition.Width, this.originalInnerPlotPosition.Height );
            this.originalInnerPlotPosition = RectangleF.Empty;
        }
    }

    /// <summary>
    /// Initialize Chart area and axes
    /// </summary>
    internal void Initialize()
    {
        // Initialize 3D style class
        this._area3DStyle = new ChartArea3DStyle( this );

        // Create axes for this chart area.
        this.axisY = new Axis();
        this.axisX = new Axis();
        this.axisX2 = new Axis();
        this.axisY2 = new Axis();

        // Initialize axes;
        this.axisX.Initialize( this, AxisName.X );
        this.axisY.Initialize( this, AxisName.Y );
        this.axisX2.Initialize( this, AxisName.X2 );
        this.axisY2.Initialize( this, AxisName.Y2 );

        // Initialize axes array
        this._axisArray[0] = this.axisX;
        this._axisArray[1] = this.axisY;
        this._axisArray[2] = this.axisX2;
        this._axisArray[3] = this.axisY2;

        // Set flag to reset auto values for all areas
        this._areaPosition = new ElementPosition( this )
        {
            resetAreaAutoPosition = true
        };

        this._innerPlotPosition = new ElementPosition( this );

        // Set the position of the new chart area
        this.PlotAreaPosition ??= new ElementPosition( this );

        // Initialize cursor class
        this._cursorX.Initialize( this, AxisName.X );
        this._cursorY.Initialize( this, AxisName.Y );
    }

    /// <summary>
    /// Minimum and maximum do not have to be calculated 
    /// from data series every time. It is very time 
    /// consuming. Minimum and maximum are buffered 
    /// and only when this flags are set Minimum and 
    /// Maximum are refreshed from data.
    /// </summary>
    internal void ResetMinMaxFromData()
    {
        this._axisArray[0].refreshMinMaxFromData = true;
        this._axisArray[1].refreshMinMaxFromData = true;
        this._axisArray[2].refreshMinMaxFromData = true;
        this._axisArray[3].refreshMinMaxFromData = true;
    }

    /// <summary>
    /// Recalculates the axes scale of a chart area.
    /// </summary>
    public void RecalculateAxesScale()
    {
        // Read axis Max/Min from data
        this.ResetMinMaxFromData();

        this.Set3DAnglesAndReverseMode();
        this.SetTempValues();

        // Initialize area position
        this._axisArray[0].ReCalc( this.PlotAreaPosition );
        this._axisArray[1].ReCalc( this.PlotAreaPosition );
        this._axisArray[2].ReCalc( this.PlotAreaPosition );
        this._axisArray[3].ReCalc( this.PlotAreaPosition );

        // Find all Data and chart types which belong 
        // to this chart area an set default values
        this.SetData();

        this.Restore3DAnglesAndReverseMode();
        this.GetTempValues();
    }

    /// <summary>
    /// RecalculateAxesScale the chart area
    /// </summary>
    internal void ReCalcInternal()
    {
        // Initialize area position
        this._axisArray[0].ReCalc( this.PlotAreaPosition );
        this._axisArray[1].ReCalc( this.PlotAreaPosition );
        this._axisArray[2].ReCalc( this.PlotAreaPosition );
        this._axisArray[3].ReCalc( this.PlotAreaPosition );

        // Find all Data and chart types which belong 
        // to this chart area an set default values
        this.SetData();
    }

    /// <summary>
    /// Reset auto calculated chart area values.
    /// </summary>
    internal void ResetAutoValues()
    {
        this._axisArray[0].ResetAutoValues();
        this._axisArray[1].ResetAutoValues();
        this._axisArray[2].ResetAutoValues();
        this._axisArray[3].ResetAutoValues();
    }

    /// <summary>
    /// Calculates Position for the background.
    /// </summary>
    /// <param name="withScrollBars">Calculate with scroll bars</param>
    /// <returns>Background rectangle</returns>
    internal RectangleF GetBackgroundPosition( bool withScrollBars )
    {
        // For pie and doughnut, which do not have axes, the position 
        // for the background is Chart area position not plotting 
        // area position.
        RectangleF backgroundPosition = this.PlotAreaPosition.ToRectangleF();
        if ( !this.requireAxes )
        {
            backgroundPosition = this.Position.ToRectangleF();
        }

        // Without scroll bars
        if ( !withScrollBars )
        {
            return backgroundPosition;
        }

        // Add scroll bar rectangles to the area background 
        RectangleF backgroundPositionWithScrollBars = new( backgroundPosition.Location, backgroundPosition.Size );

        if ( this.requireAxes )
        {
            // Loop through all axis
            foreach ( Axis axis in this.Axes )
            {
                // Find axis with visible scroll bars
                if ( axis.ScrollBar.IsVisible && axis.ScrollBar.IsPositionedInside )
                {
                    // Change size of the background rectangle depending on the axis position
                    if ( axis.AxisPosition == AxisPosition.Bottom )
                    {
                        backgroundPositionWithScrollBars.Height += ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                    }
                    else if ( axis.AxisPosition == AxisPosition.Top )
                    {
                        backgroundPositionWithScrollBars.Y -= ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                        backgroundPositionWithScrollBars.Height += ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                    }
                    else if ( axis.AxisPosition == AxisPosition.Left )
                    {
                        backgroundPositionWithScrollBars.X -= ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                        backgroundPositionWithScrollBars.Width += ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                    }
                    else if ( axis.AxisPosition == AxisPosition.Left )
                    {
                        backgroundPositionWithScrollBars.Width += ( float ) axis.ScrollBar.GetScrollBarRelativeSize();
                    }
                }
            }
        }

        return backgroundPositionWithScrollBars;
    }

    /// <summary>
    /// Call when the chart area is resized.
    /// </summary>
    /// <param name="chartGraph">Chart graphics object.</param>
    internal void Resize( ChartGraphics chartGraph )
    {
        // Initialize plotting area position
        RectangleF plottingRect = this.Position.ToRectangleF();
        if ( !this.InnerPlotPosition.Auto )
        {
            plottingRect.X += this.Position.Width / 100F * this.InnerPlotPosition.X;
            plottingRect.Y += this.Position.Height / 100F * this.InnerPlotPosition.Y;
            plottingRect.Width = this.Position.Width / 100F * this.InnerPlotPosition.Width;
            plottingRect.Height = this.Position.Height / 100F * this.InnerPlotPosition.Height;
        }

        // ============================
        // Calculate number of vertical and horizontal axis
        // ============================
        int verticalAxes = 0;
        int horizontalAxes = 0;
        foreach ( Axis axis in this.Axes )
        {
            if ( axis.enabled )
            {
                if ( axis.AxisPosition == AxisPosition.Bottom )
                {
                    ++horizontalAxes;
                }
                else if ( axis.AxisPosition == AxisPosition.Top )
                {
                    ++horizontalAxes;
                }
                else if ( axis.AxisPosition == AxisPosition.Left )
                {
                    ++verticalAxes;
                }
                else if ( axis.AxisPosition == AxisPosition.Right )
                {
                    ++verticalAxes;
                }
            }
        }
        if ( horizontalAxes <= 0 )
        {
            horizontalAxes = 1;
        }
        if ( verticalAxes <= 0 )
        {
            verticalAxes = 1;
        }


        // ============================
        // Find same auto-fit font size
        // ============================
        Axis[] axisArray = this.switchValueAxes ?
            [this.AxisX, this.AxisX2, this.AxisY, this.AxisY2] :
            [this.AxisY, this.AxisY2, this.AxisX, this.AxisX2];
        if ( this.IsSameFontSizeForAllAxes )
        {
            this.axesAutoFontSize = 20;
            foreach ( Axis axis in axisArray )
            {
                // Process only enabled axis
                if ( axis.enabled )
                {
                    // Resize axis
                    if ( axis.AxisPosition is AxisPosition.Bottom or AxisPosition.Top )
                    {
                        axis.Resize( chartGraph, this.PlotAreaPosition, plottingRect, horizontalAxes, this.InnerPlotPosition.Auto );
                    }
                    else
                    {
                        axis.Resize( chartGraph, this.PlotAreaPosition, plottingRect, verticalAxes, this.InnerPlotPosition.Auto );
                    }

                    // Calculate smallest font size
                    if ( axis.IsLabelAutoFit && axis.autoLabelFont is not null )
                    {
                        this.axesAutoFontSize = Math.Min( this.axesAutoFontSize, axis.autoLabelFont.Size );
                    }
                }
            }
        }

        // ============================
        // Adjust plotting area position according to the axes 
        // elements (title, labels, tick marks) size.
        // ============================
        RectangleF rectLabelSideSpacing = RectangleF.Empty;
        foreach ( Axis axis in axisArray )
        {
            // Process only enabled axis
            if ( !axis.enabled )
            {
                // ============================
                // Adjust for the 3D Wall Width for disabled axis
                // ============================
                if ( this.InnerPlotPosition.Auto && this.Area3DStyle.Enable3D && !this.chartAreaIsCurcular )
                {
                    SizeF areaWallSize = chartGraph.GetRelativeSize( new SizeF( this.Area3DStyle.WallWidth, this.Area3DStyle.WallWidth ) );
                    if ( axis.AxisPosition == AxisPosition.Bottom )
                    {
                        plottingRect.Height -= areaWallSize.Height;
                    }
                    else if ( axis.AxisPosition == AxisPosition.Top )
                    {
                        plottingRect.Y += areaWallSize.Height;
                        plottingRect.Height -= areaWallSize.Height;
                    }
                    else if ( axis.AxisPosition == AxisPosition.Right )
                    {
                        plottingRect.Width -= areaWallSize.Width;
                    }
                    else if ( axis.AxisPosition == AxisPosition.Left )
                    {
                        plottingRect.X += areaWallSize.Width;
                        plottingRect.Width -= areaWallSize.Width;
                    }
                }

                continue;
            }

            // ============================
            // Calculate axes elements position
            // ============================
            if ( axis.AxisPosition is AxisPosition.Bottom or AxisPosition.Top )
            {
                axis.Resize( chartGraph, this.PlotAreaPosition, plottingRect, horizontalAxes, this.InnerPlotPosition.Auto );
            }
            else
            {
                axis.Resize( chartGraph, this.PlotAreaPosition, plottingRect, verticalAxes, this.InnerPlotPosition.Auto );
            }

            // Shift top/bottom labels so they will not overlap with left/right labels
            this.PreventTopBottomAxesLabelsOverlapping( axis );

            // ============================
            // Check axis position
            // ============================
            float axisPosition = ( float ) axis.GetAxisPosition();
            if ( axis.AxisPosition == AxisPosition.Bottom )
            {
                if ( !axis.GetIsMarksNextToAxis() )
                {
                    axisPosition = plottingRect.Bottom;
                }
                axisPosition = plottingRect.Bottom - axisPosition;
            }
            else if ( axis.AxisPosition == AxisPosition.Top )
            {
                if ( !axis.GetIsMarksNextToAxis() )
                {
                    axisPosition = plottingRect.Y;
                }
                axisPosition -= plottingRect.Top;
            }
            else if ( axis.AxisPosition == AxisPosition.Right )
            {
                if ( !axis.GetIsMarksNextToAxis() )
                {
                    axisPosition = plottingRect.Right;
                }
                axisPosition = plottingRect.Right - axisPosition;
            }
            else if ( axis.AxisPosition == AxisPosition.Left )
            {
                if ( !axis.GetIsMarksNextToAxis() )
                {
                    axisPosition = plottingRect.X;
                }
                axisPosition -= plottingRect.Left;
            }

            // ============================
            // Adjust axis elements size with axis position
            // ============================
            // Calculate total size of axis elements
            float axisSize = axis.markSize + axis.labelSize;

#if SUBAXES
					// Add sub-axis size
					if(!this.chartAreaIsCurcular && !this.Area3DStyle.Enable3D)
					{
						foreach(SubAxis subAxis in axis.SubAxes)
						{
							axisSize += subAxis.markSize + subAxis.labelSize + subAxis.titleSize;
						}
					}
#endif // SUBAXES

            // Adjust depending on the axis position
            axisSize -= axisPosition;
            if ( axisSize < 0 )
            {
                axisSize = 0;
            }


            // Add axis title and scroll bar size (always outside of plotting area)
            axisSize += axis.titleSize + axis.scrollBarSize;


            // Calculate horizontal axes size for circualar area
            if ( this.chartAreaIsCurcular &&
                (axis.AxisPosition == AxisPosition.Top || axis.AxisPosition == AxisPosition.Bottom) )
            {
                axisSize = axis.titleSize + axis.markSize + axis.scrollBarSize;
            }

            // ============================
            // Adjust plotting area
            // ============================
            if ( this.InnerPlotPosition.Auto )
            {
                if ( axis.AxisPosition == AxisPosition.Bottom )
                {
                    plottingRect.Height -= axisSize;
                }
                else if ( axis.AxisPosition == AxisPosition.Top )
                {
                    plottingRect.Y += axisSize;
                    plottingRect.Height -= axisSize;
                }
                else if ( axis.AxisPosition == AxisPosition.Left )
                {
                    plottingRect.X += axisSize;
                    plottingRect.Width -= axisSize;
                }
                else if ( axis.AxisPosition == AxisPosition.Right )
                {
                    plottingRect.Width -= axisSize;
                }

                // Check if labels side offset should be processed
                bool addLabelsSideOffsets = true;

                // Update the plotting area depending on the size required for labels on the sides
                if ( addLabelsSideOffsets )
                {
                    if ( axis.AxisPosition is AxisPosition.Bottom or AxisPosition.Top )
                    {
                        if ( axis.labelNearOffset != 0 && axis.labelNearOffset < this.Position.X )
                        {
                            float offset = this.Position.X - axis.labelNearOffset;
                            if ( Math.Abs( offset ) > plottingRect.Width * 0.3f )
                            {
                                offset = plottingRect.Width * 0.3f;
                            }

                            // NOTE: Code was removed to solve an issue with extra space when labels angle = 45
                            //rectLabelSideSpacing.Width = (float)Math.Max(offset, rectLabelSideSpacing.Width);
                            rectLabelSideSpacing.X = ( float ) Math.Max( offset, rectLabelSideSpacing.X );
                        }

                        if ( axis.labelFarOffset > this.Position.Right )
                        {
                            rectLabelSideSpacing.Width = (axis.labelFarOffset - this.Position.Right) < plottingRect.Width * 0.3f
                                ? ( float ) Math.Max( axis.labelFarOffset - this.Position.Right, rectLabelSideSpacing.Width )
                                : ( float ) Math.Max( plottingRect.Width * 0.3f, rectLabelSideSpacing.Width );
                        }
                    }

                    else
                    {
                        if ( axis.labelNearOffset != 0 && axis.labelNearOffset < this.Position.Y )
                        {
                            float offset = this.Position.Y - axis.labelNearOffset;
                            if ( Math.Abs( offset ) > plottingRect.Height * 0.3f )
                            {
                                offset = plottingRect.Height * 0.3f;
                            }

                            // NOTE: Code was removed to solve an issue with extra space when labels angle = 45
                            //rectLabelSideSpacing.Height = (float)Math.Max(offset, rectLabelSideSpacing.Height);
                            rectLabelSideSpacing.Y = ( float ) Math.Max( offset, rectLabelSideSpacing.Y );
                        }

                        if ( axis.labelFarOffset > this.Position.Bottom )
                        {
                            rectLabelSideSpacing.Height = (axis.labelFarOffset - this.Position.Bottom) < plottingRect.Height * 0.3f
                                ? ( float ) Math.Max( axis.labelFarOffset - this.Position.Bottom, rectLabelSideSpacing.Height )
                                : ( float ) Math.Max( plottingRect.Height * 0.3f, rectLabelSideSpacing.Height );
                        }
                    }
                }
            }
        }

        // ============================
        // Make sure there is enough space 
        // for labels on the chart sides
        // ============================
        if ( !this.chartAreaIsCurcular )
        {
            if ( rectLabelSideSpacing.Y > 0 && rectLabelSideSpacing.Y > plottingRect.Y - this.Position.Y )
            {
                float delta = plottingRect.Y - this.Position.Y - rectLabelSideSpacing.Y;
                plottingRect.Y -= delta;
                plottingRect.Height += delta;
            }
            if ( rectLabelSideSpacing.X > 0 && rectLabelSideSpacing.X > plottingRect.X - this.Position.X )
            {
                float delta = plottingRect.X - this.Position.X - rectLabelSideSpacing.X;
                plottingRect.X -= delta;
                plottingRect.Width += delta;
            }
            if ( rectLabelSideSpacing.Height > 0 && rectLabelSideSpacing.Height > this.Position.Bottom - plottingRect.Bottom )
            {
                plottingRect.Height += this.Position.Bottom - plottingRect.Bottom - rectLabelSideSpacing.Height;
            }
            if ( rectLabelSideSpacing.Width > 0 && rectLabelSideSpacing.Width > this.Position.Right - plottingRect.Right )
            {
                plottingRect.Width += this.Position.Right - plottingRect.Right - rectLabelSideSpacing.Width;
            }
        }

        // ============================
        // Plotting area must be square for the circular 
        // chart area (in pixels).
        // ============================
        if ( this.chartAreaIsCurcular )
        {
            // Adjust area to fit the axis title
            float xTitleSize = ( float ) Math.Max( this.AxisY.titleSize, this.AxisY2.titleSize );
            if ( xTitleSize > 0 )
            {
                plottingRect.X += xTitleSize;
                plottingRect.Width -= 2f * xTitleSize;
            }
            float yTitleSize = ( float ) Math.Max( this.AxisX.titleSize, this.AxisX2.titleSize );
            if ( yTitleSize > 0 )
            {
                plottingRect.Y += yTitleSize;
                plottingRect.Height -= 2f * yTitleSize;
            }

            // Make a square plotting rect
            RectangleF rect = chartGraph.GetAbsoluteRectangle( plottingRect );
            if ( rect.Width > rect.Height )
            {
                rect.X += (rect.Width - rect.Height) / 2f;
                rect.Width = rect.Height;
            }
            else
            {
                rect.Y += (rect.Height - rect.Width) / 2f;
                rect.Height = rect.Width;
            }
            plottingRect = chartGraph.GetRelativeRectangle( rect );

            // Remember circular chart area center
            this.circularCenter = new PointF( plottingRect.X + (plottingRect.Width / 2f), plottingRect.Y + (plottingRect.Height / 2f) );

            // Calculate auto-fit font of the circular axis labels and update area position
            this.FitCircularLabels( chartGraph, this.PlotAreaPosition, ref plottingRect, xTitleSize, yTitleSize );
        }

        // ============================
        // Set plotting area position
        // ============================
        if ( plottingRect.Width < 0f )
        {
            plottingRect.Width = 0f;
        }
        if ( plottingRect.Height < 0f )
        {
            plottingRect.Height = 0f;
        }
        this.PlotAreaPosition.FromRectangleF( plottingRect );
        this.InnerPlotPosition.SetPositionNoAuto(
            ( float ) Math.Round( (plottingRect.X - this.Position.X) / (this.Position.Width / 100F), 5 ),
            ( float ) Math.Round( (plottingRect.Y - this.Position.Y) / (this.Position.Height / 100F), 5 ),
            ( float ) Math.Round( plottingRect.Width / (this.Position.Width / 100F), 5 ),
            ( float ) Math.Round( plottingRect.Height / (this.Position.Height / 100F), 5 ) );


        // ============================
        // Adjust label font size for axis, which were 
        // automatically calculated after the opposite axis 
        // change the size of plotting area.
        // ============================
        this.AxisY2.AdjustLabelFontAtSecondPass( chartGraph, this.InnerPlotPosition.Auto );
        this.AxisY.AdjustLabelFontAtSecondPass( chartGraph, this.InnerPlotPosition.Auto );
        if ( this.InnerPlotPosition.Auto )
        {
            this.AxisX2.AdjustLabelFontAtSecondPass( chartGraph, this.InnerPlotPosition.Auto );
            this.AxisX.AdjustLabelFontAtSecondPass( chartGraph, this.InnerPlotPosition.Auto );
        }

    }

    /// <summary>
    /// Finds axis by it's position. Can be Null.
    /// </summary>
    /// <param name="axisPosition">Axis position to find</param>
    /// <returns>Found axis.</returns>
    private Axis FindAxis( AxisPosition axisPosition )
    {
        foreach ( Axis axis in this.Axes )
        {
            if ( axis.AxisPosition == axisPosition )
            {
                return axis;
            }
        }
        return null;
    }

    /// <summary>
    /// Shift top/bottom labels so they will not overlap with left/right labels.
    /// </summary>
    /// <param name="axis">Axis to shift up/down.</param>
    private void PreventTopBottomAxesLabelsOverlapping( Axis axis )
    {
        // If axis is not on the edge of the chart area do not
        // try to adjust it's position when axis labels overlap
        // labels of the oppositie axis.
        if ( !axis.IsAxisOnAreaEdge )
        {
            return;
        }

        // Shift bottom axis labels down
        if ( axis.AxisPosition == AxisPosition.Bottom )
        {
            // Get labels position
            float labelsPosition = ( float ) axis.GetAxisPosition();
            if ( !axis.GetIsMarksNextToAxis() )
            {
                labelsPosition = axis.PlotAreaPosition.Bottom;
            }

            // Only adjust labels outside plotting area
            if ( Math.Round( labelsPosition, 2 ) < Math.Round( axis.PlotAreaPosition.Bottom, 2 ) )
            {
                return;
            }

            // Check if labels may overlap with Left axis
            Axis leftAxis = this.FindAxis( AxisPosition.Left );
            if ( leftAxis != null &&
                leftAxis.enabled &&
                leftAxis.labelFarOffset != 0 &&
                leftAxis.labelFarOffset > labelsPosition &&
                axis.labelNearOffset != 0 &&
                axis.labelNearOffset < this.PlotAreaPosition.X )
            {
                float overlap = ( float ) (leftAxis.labelFarOffset - labelsPosition) * 0.75f;
                if ( overlap > axis.markSize )
                {
                    axis.markSize += overlap - axis.markSize;
                }
            }

            // Check if labels may overlap with Right axis
            Axis rightAxis = this.FindAxis( AxisPosition.Right );
            if ( rightAxis != null &&
                rightAxis.enabled &&
                rightAxis.labelFarOffset != 0 &&
                rightAxis.labelFarOffset > labelsPosition &&
                axis.labelFarOffset != 0 &&
                axis.labelFarOffset > this.PlotAreaPosition.Right )
            {
                float overlap = ( float ) (rightAxis.labelFarOffset - labelsPosition) * 0.75f;
                if ( overlap > axis.markSize )
                {
                    axis.markSize += overlap - axis.markSize;
                }
            }
        }

        // Shift top axis labels up
        else if ( axis.AxisPosition == AxisPosition.Top )
        {
            // Get labels position
            float labelsPosition = ( float ) axis.GetAxisPosition();
            if ( !axis.GetIsMarksNextToAxis() )
            {
                labelsPosition = axis.PlotAreaPosition.Y;
            }

            // Only adjust labels outside plotting area
            if ( Math.Round( labelsPosition, 2 ) < Math.Round( axis.PlotAreaPosition.Y, 2 ) )
            {
                return;
            }

            // Check if labels may overlap with Left axis
            Axis leftAxis = this.FindAxis( AxisPosition.Left );
            if ( leftAxis != null &&
                leftAxis.enabled &&
                leftAxis.labelNearOffset != 0 &&
                leftAxis.labelNearOffset < labelsPosition &&
                axis.labelNearOffset != 0 &&
                axis.labelNearOffset < this.PlotAreaPosition.X )
            {
                float overlap = ( float ) (labelsPosition - leftAxis.labelNearOffset) * 0.75f;
                if ( overlap > axis.markSize )
                {
                    axis.markSize += overlap - axis.markSize;
                }
            }

            // Check if labels may overlap with Right axis
            Axis rightAxis = this.FindAxis( AxisPosition.Right );
            if ( rightAxis != null &&
                rightAxis.enabled &&
                rightAxis.labelNearOffset != 0 &&
                rightAxis.labelNearOffset < labelsPosition &&
                axis.labelFarOffset != 0 &&
                axis.labelFarOffset > this.PlotAreaPosition.Right )
            {
                float overlap = ( float ) (labelsPosition - rightAxis.labelNearOffset) * 0.75f;
                if ( overlap > axis.markSize )
                {
                    axis.markSize += overlap - axis.markSize;
                }
            }
        }

    }

    #endregion

    #region " painting and selection methods "

    /// <summary>
    /// Draws chart area background and/or border.
    /// </summary>
    /// <param name="graph">Chart graphics.</param>
    /// <param name="position">Background position.</param>
    /// <param name="borderOnly">Draws chart area border only.</param>
    private void PaintAreaBack( ChartGraphics graph, RectangleF position, bool borderOnly )
    {
        if ( !borderOnly )
        {
            // Draw background
            if ( !this.Area3DStyle.Enable3D || !this.requireAxes || this.chartAreaIsCurcular )
            {
                // 3D Pie Chart doesn't need scene
                // Draw 2D background
                graph.FillRectangleRel(
                    position,
                    this.BackColor,
                    this.BackHatchStyle,
                    this.BackImage,
                    this.BackImageWrapMode,
                    this.BackImageTransparentColor,
                    this.BackImageAlignment,
                    this.BackGradientStyle,
                    this.BackSecondaryColor,
                    this.requireAxes ? Color.Empty : this.BorderColor,
                    this.requireAxes ? 0 : this.BorderWidth,
                    this.BorderDashStyle,
                    this.ShadowColor,
                    this.ShadowOffset,
                    PenAlignment.Outset,
                    this.chartAreaIsCurcular,
                    (this.chartAreaIsCurcular && this.CircularUsePolygons) ? this.CircularSectorsNumber : 0,
                    this.Area3DStyle.Enable3D );
            }
            else
            {
                // Draw chart area 3D scene
                this.DrawArea3DScene( graph, position );
            }
        }
        else
        {
            if ( !this.Area3DStyle.Enable3D || !this.requireAxes || this.chartAreaIsCurcular )
            {
                // Draw chart area border
                if ( this.BorderColor != Color.Empty && this.BorderWidth > 0 )
                {
                    graph.FillRectangleRel( position,
                        Color.Transparent,
                        ChartHatchStyle.None,
                        "",
                        ChartImageWrapMode.Tile,
                        Color.Empty,
                        ChartImageAlignmentStyle.Center,
                        GradientStyle.None,
                        Color.Empty,
                        this.BorderColor,
                        this.BorderWidth,
                        this.BorderDashStyle,
                        Color.Empty,
                        0,
                        PenAlignment.Outset,
                        this.chartAreaIsCurcular,
                        (this.chartAreaIsCurcular && this.CircularUsePolygons) ? this.CircularSectorsNumber : 0,
                        this.Area3DStyle.Enable3D );
                }
            }

        }
    }

    /// <summary>
    /// Paint the chart area.
    /// </summary>
    /// <param name="graph">Chart graphics.</param>
    internal void Paint( ChartGraphics graph )
    {
        // Check if plot area position was recalculated.
        // If not and non-auto InnerPlotPosition & Position were
        // specified - do all needed calculations
        if ( this.PlotAreaPosition.Width == 0 &&
            this.PlotAreaPosition.Height == 0 &&
            !this.InnerPlotPosition.Auto
            && !this.Position.Auto )
        {
            // Initialize plotting area position
            RectangleF plottingRect = this.Position.ToRectangleF();
            if ( !this.InnerPlotPosition.Auto )
            {
                plottingRect.X += this.Position.Width / 100F * this.InnerPlotPosition.X;
                plottingRect.Y += this.Position.Height / 100F * this.InnerPlotPosition.Y;
                plottingRect.Width = this.Position.Width / 100F * this.InnerPlotPosition.Width;
                plottingRect.Height = this.Position.Height / 100F * this.InnerPlotPosition.Height;
            }

            this.PlotAreaPosition.FromRectangleF( plottingRect );
        }

        // Get background position rectangle.
        RectangleF backgroundPositionWithScrollBars = this.GetBackgroundPosition( true );
        RectangleF backgroundPosition = this.GetBackgroundPosition( false );

        // Add hot region for plotting area.
        if ( this.Common.ProcessModeRegions )
        {
            this.Common.HotRegionsList.AddHotRegion( backgroundPosition, this, ChartElementType.PlottingArea, true );
        }
        // Draw background
        this.PaintAreaBack( graph, backgroundPositionWithScrollBars, false );

        // Call BackPaint event
        this.Common.Chart.CallOnPrePaint( new ChartPaintEventArgs( this, graph, this.Common, this.PlotAreaPosition ) );

        // Draw chart types without axes - Pie.
        if ( !this.requireAxes && this.ChartTypes.Count != 0 )
        {
            // Find first chart type that do not require axis (like Pie) and draw it.
            // Chart types that do not require axes (circular charts) cannot be combined with
            // any other chart types.
            // NOTE: Fixes issues #4672 and #4692
            for ( int chartTypeIndex = 0; chartTypeIndex < this.ChartTypes.Count; chartTypeIndex++ )
            {
                IChartType chartType = this.Common.ChartTypeRegistry.GetChartType( ( string ) this.ChartTypes[chartTypeIndex] );
                if ( !chartType.RequireAxes )
                {
                    chartType.Paint( graph, this.Common, this, null );
                    break;
                }
            }

            // Call Paint event
            this.Common.Chart.CallOnPostPaint( new ChartPaintEventArgs( this, graph, this.Common, this.PlotAreaPosition ) );
            return;
        }



        // Reset Smart Labels 
        this.smartLabels.Reset();



        // Set values for optimized drawing
        foreach ( Axis currentAxis in this._axisArray )
        {
            currentAxis.optimizedGetPosition = true;
            currentAxis.paintViewMax = currentAxis.ViewMaximum;
            currentAxis.paintViewMin = currentAxis.ViewMinimum;
            currentAxis.paintRange = currentAxis.paintViewMax - currentAxis.paintViewMin;
            currentAxis.paintAreaPosition = this.PlotAreaPosition.ToRectangleF();
            if ( currentAxis.ChartArea != null && currentAxis.ChartArea.chartAreaIsCurcular )
            {
                // Update position for circular chart area
                currentAxis.paintAreaPosition.Width /= 2.0f;
                currentAxis.paintAreaPosition.Height /= 2.0f;
            }
            currentAxis.paintAreaPositionBottom = currentAxis.paintAreaPosition.Y + currentAxis.paintAreaPosition.Height;
            currentAxis.paintAreaPositionRight = currentAxis.paintAreaPosition.X + currentAxis.paintAreaPosition.Width;
            currentAxis.paintChartAreaSize = currentAxis.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
                ? currentAxis.paintAreaPosition.Width
                : currentAxis.paintAreaPosition.Height;

            currentAxis.valueMultiplier = 0.0;
            if ( currentAxis.paintRange != 0 )
            {
                currentAxis.valueMultiplier = currentAxis.paintChartAreaSize / currentAxis.paintRange;
            }
        }

        // Draw Axis Striplines (only when StripWidth > 0)
        bool useScaleSegments = false;
        Axis[] axesArray = [this.axisY, this.axisY2, this.axisX, this.axisX2];
        foreach ( Axis currentAxis in axesArray )
        {
            useScaleSegments = currentAxis.ScaleSegments.Count > 0;

            if ( !useScaleSegments )
            {
                currentAxis.PaintStrips( graph, false );
            }

            else
            {
                foreach ( AxisScaleSegment scaleSegment in currentAxis.ScaleSegments )
                {
                    scaleSegment.SetTempAxisScaleAndInterval();

                    currentAxis.PaintStrips( graph, false );

                    scaleSegment.RestoreAxisScaleAndInterval();
                }
            }
        }

        // Draw Axis Grids
        axesArray = [this.axisY, this.axisX2, this.axisY2, this.axisX];
        foreach ( Axis currentAxis in axesArray )
        {
            useScaleSegments = currentAxis.ScaleSegments.Count > 0;

            if ( !useScaleSegments )
            {
                currentAxis.PaintGrids( graph );
            }

            else
            {
                foreach ( AxisScaleSegment scaleSegment in currentAxis.ScaleSegments )
                {
                    scaleSegment.SetTempAxisScaleAndInterval();

                    currentAxis.PaintGrids( graph );

                    scaleSegment.RestoreAxisScaleAndInterval();
                }
            }

        }

        // Draw Axis Striplines (only when StripWidth == 0)
        foreach ( Axis currentAxis in axesArray )
        {
            useScaleSegments = currentAxis.ScaleSegments.Count > 0;

            if ( !useScaleSegments )
            {
                currentAxis.PaintStrips( graph, true );
            }

            else
            {
                foreach ( AxisScaleSegment scaleSegment in currentAxis.ScaleSegments )
                {
                    scaleSegment.SetTempAxisScaleAndInterval();

                    currentAxis.PaintStrips( graph, true );

                    scaleSegment.RestoreAxisScaleAndInterval();
                }
            }

        }

        // Draw Axis elements on the back of the 3D scene
        if ( this.Area3DStyle.Enable3D && !this.chartAreaIsCurcular )
        {
            foreach ( Axis currentAxis in axesArray )
            {
                useScaleSegments = currentAxis.ScaleSegments.Count > 0;

                if ( !useScaleSegments )
                {
                    currentAxis.PrePaint( graph );
                }

                else
                {
                    foreach ( AxisScaleSegment scaleSegment in currentAxis.ScaleSegments )
                    {
                        scaleSegment.SetTempAxisScaleAndInterval();

                        currentAxis.PrePaint( graph );

                        scaleSegment.RestoreAxisScaleAndInterval();
                    }

                }

            }
        }

        // Draws chart area border
        bool borderDrawn = false;
        if ( this.Area3DStyle.Enable3D || !this.IsBorderOnTopSeries() )
        {
            borderDrawn = true;
            this.PaintAreaBack( graph, backgroundPosition, true );
        }

        // Draw chart types
        if ( !this.Area3DStyle.Enable3D || this.chartAreaIsCurcular )
        {
            // Drawing in 2D space

            // NOTE: Fixes issue #6443 and #5385
            // If two chart series of the same type (for example Line) are separated
            // by other series (for example Area) the order is not correct.
            // Old implementation draws ALL series that belongs to the chart type.
            ArrayList typeAndSeries = this.GetChartTypesAndSeriesToDraw();

            // Draw series by chart type or by series
            foreach ( ChartTypeAndSeriesInfo chartTypeInfo in typeAndSeries )
            {
                this.IterationCounter = 0;
                IChartType type = this.Common.ChartTypeRegistry.GetChartType( chartTypeInfo.ChartType );

                // If 'chartTypeInfo.Series' set to NULL all series of that chart type are drawn at once
                type.Paint( graph, this.Common, this, chartTypeInfo.Series );
            }
        }
        else
        {
            // Drawing in 3D space
            this.PaintChartSeries3D( graph );
        }

        // Draw area border if it wasn't drawn prior to the series
        if ( !borderDrawn )
        {
            this.PaintAreaBack( graph, backgroundPosition, true );
        }

        // Draw Axis
        foreach ( Axis currentAxis in axesArray )
        {
            useScaleSegments = currentAxis.ScaleSegments.Count > 0;

            if ( !useScaleSegments )
            {
                // Paint axis and Reset temp axis offset for side-by-side charts like column
                currentAxis.Paint( graph );
            }

            else
            {
                // Some of the axis elements like grid lines and tickmarks 
                // are drawn for each segment
                foreach ( AxisScaleSegment scaleSegment in currentAxis.ScaleSegments )
                {
                    scaleSegment.SetTempAxisScaleAndInterval();

                    currentAxis.PaintOnSegmentedScalePassOne( graph );

                    scaleSegment.RestoreAxisScaleAndInterval();
                }

                // Other elements like labels, title, axis line are drawn once
                currentAxis.PaintOnSegmentedScalePassTwo( graph );
            }

        }

        // Call Paint event
        this.Common.Chart.CallOnPostPaint( new ChartPaintEventArgs( this, graph, this.Common, this.PlotAreaPosition ) );

        // Draw axis scale break lines
        axesArray = [this.axisY, this.axisY2];
        foreach ( Axis currentAxis in axesArray )
        {
            for ( int segmentIndex = 0; segmentIndex < (currentAxis.ScaleSegments.Count - 1); segmentIndex++ )
            {
                currentAxis.ScaleSegments[segmentIndex].PaintBreakLine( graph, currentAxis.ScaleSegments[segmentIndex + 1] );

            }
        }

        // Reset values for optimized drawing
        foreach ( Axis curentAxis in this._axisArray )
        {
            curentAxis.optimizedGetPosition = false;


            // Reset preffered number of intervals on the axis
            curentAxis.prefferedNumberofIntervals = 5;

            // Reset flag that scale segments are used
            curentAxis.scaleSegmentsUsed = false;


        }
    }

    /// <summary>
    /// Checks if chart area border should be drawn on top of series.
    /// </summary>
    /// <returns>True if border should be darwn on top.</returns>
    private bool IsBorderOnTopSeries()
    {
        // For most of the chart types chart area border is drawn on top.
        bool result = true;
        foreach ( Series series in this.Common.Chart.Series )
        {
            if ( series.ChartArea == this.Name )
            {
                // It is common for the Bubble and Point chart types to draw markers
                // partially outside of the chart area. By drawing the border before
                // series we avoiding the possibility of drawing the border line on 
                // top of the marker.
                if ( series.ChartType is SeriesChartType.Bubble or
                    SeriesChartType.Point )
                {
                    return false;
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Paint the chart area cursors.
    /// </summary>
    /// <param name="graph">Chart graphics.</param>
    /// <param name="cursorOnly">Indicates that only cursors are redrawn.</param>
    internal void PaintCursors( ChartGraphics graph, bool cursorOnly )
    {
        // Cursors and selection are supoorted only in 2D charts
        if ( this.Area3DStyle.Enable3D )
        {
            return;
        }

        // Do not draw cursor/selection for chart types that do not require axis (like Pie)
        if ( !this.requireAxes )
        {
            return;
        }

        // Cursors and selection are not supoorted in circular areas
        if ( this.chartAreaIsCurcular )
        {
            return;
        }

        // Do not draw cursor/selection while printing
        if ( this.Common != null &&
            this.Common.ChartPicture != null &&
            this.Common.ChartPicture.isPrinting )
        {
            return;
        }

        // Do not draw cursor/selection when chart area is not visible
        // because either width or height is set to zero
        if ( this.Position.Width == 0f ||
            this.Position.Height == 0f )
        {
            return;
        }

        Chart chart = this.Common.Chart;
        ChartPicture chartPicture = this.Common.ChartPicture;

        // Check if cursor should be drawn
        if ( !double.IsNaN( this._cursorX.SelectionStart ) ||
            !double.IsNaN( this._cursorX.SelectionEnd ) ||
            !double.IsNaN( this._cursorX.Position ) ||
            !double.IsNaN( this._cursorY.SelectionStart ) ||
            !double.IsNaN( this._cursorY.SelectionEnd ) ||
            !double.IsNaN( this._cursorY.Position ) )
        {
            if ( !chartPicture.backgroundRestored &&
                !chartPicture.isSelectionMode )
            {
                chartPicture.backgroundRestored = true;

                Rectangle chartPosition = new( 0, 0, chartPicture.Width, chartPicture.Height );

                // Get chart area position
                Rectangle absAreaPlotPosition = Rectangle.Round( graph.GetAbsoluteRectangle( this.PlotAreaPosition.ToRectangleF() ) );
                int maxCursorWidth = (this.CursorY.LineWidth > this.CursorX.LineWidth) ? this.CursorY.LineWidth + 1 : this.CursorX.LineWidth + 1;
                absAreaPlotPosition.Inflate( maxCursorWidth, maxCursorWidth );
                absAreaPlotPosition.Intersect( new Rectangle( 0, 0, chart.Width, chart.Height ) );

                // Create area buffer bitmap
                if ( this.areaBufferBitmap == null ||
                    chartPicture.nonTopLevelChartBuffer == null ||
                    !cursorOnly )
                {
                    // Dispose previous bitmap
                    if ( this.areaBufferBitmap is not null )
                    {
                        this.areaBufferBitmap.Dispose();
                        this.areaBufferBitmap = null;
                    }
                    if ( chartPicture.nonTopLevelChartBuffer is not null )
                    {
                        chartPicture.nonTopLevelChartBuffer.Dispose();
                        chartPicture.nonTopLevelChartBuffer = null;
                    }


                    // Copy chart area plotting rectangle from the chart's dubble buffer image into area dubble buffer image
                    if ( chart.paintBufferBitmap is not null )
                    {
                        this.areaBufferBitmap = chart.paintBufferBitmap.Clone( absAreaPlotPosition, chart.paintBufferBitmap.PixelFormat );
                    }

                    // Copy whole chart from the chart's dubble buffer image into area dubble buffer image
                    if ( chart.paintBufferBitmap != null &&
                        chart.paintBufferBitmap.Size.Width >= chartPosition.Size.Width &&
                        chart.paintBufferBitmap.Size.Height >= chartPosition.Size.Height )
                    {
                        chartPicture.nonTopLevelChartBuffer = chart.paintBufferBitmap.Clone(
                            chartPosition, chart.paintBufferBitmap.PixelFormat );
                    }

                }
                else if ( cursorOnly && chartPicture.nonTopLevelChartBuffer is not null )
                {
                    // Restore previous background
                    chart.paintBufferBitmapGraphics.DrawImageUnscaled(
                        chartPicture.nonTopLevelChartBuffer,
                        chartPosition );
                }
            }

            // Draw chart area cursors and range selection

            this._cursorY.Paint( graph );
            this._cursorX.Paint( graph );

        }

    }

    #endregion

    #region " circular chart area methods "

    /// <summary>
    /// Gets a circular chart type interface that belongs to this chart area.
    /// </summary>
    /// <returns>ICircularChartType interface or null.</returns>
    internal ICircularChartType GetCircularChartType()
    {
        // Get number of sectors in circular chart area
        foreach ( Series series in this.Common.DataManager.Series )
        {
            if ( series.IsVisible() && series.ChartArea == this.Name )
            {
                ;
                if ( this.Common.ChartTypeRegistry.GetChartType( series.ChartTypeName ) is ICircularChartType type )
                {
                    return type;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Calculate size of the circular axis labels and sets auto-fit font.
    /// </summary>
    /// <param name="chartGraph">Chart graphics object.</param>
    /// <param name="chartAreaPosition">The Chart area position.</param>
    /// <param name="plotArea">Plotting area size.</param>
    /// <param name="xTitleSize">Size of title on the axis.</param>
    /// <param name="yTitleSize">Size of title on the axis.</param>
    internal void FitCircularLabels(
        ChartGraphics chartGraph,
        ElementPosition chartAreaPosition,
        ref RectangleF plotArea,
        float xTitleSize,
        float yTitleSize )
    {
        // Check if axis labels are enabled
        if ( !this.AxisX.LabelStyle.Enabled )
        {
            return;
        }

        // Get absolute titles size
        SizeF titleSize = chartGraph.GetAbsoluteSize( new SizeF( xTitleSize, yTitleSize ) );

        // Get absolute position of area
        RectangleF plotAreaRectAbs = chartGraph.GetAbsoluteRectangle( plotArea );
        RectangleF areaRectAbs = chartGraph.GetAbsoluteRectangle( chartAreaPosition.ToRectangleF() );

        // Get absolute markers size and spacing
        float spacing = chartGraph.GetAbsolutePoint( new PointF( 0, this.AxisX.markSize + Axis.elementSpacing ) ).Y;

        // Get circular axis list
        ArrayList axisList = this.GetCircularAxisList();

        // Get circular axis labels style
        CircularAxisLabelsStyle labelsStyle = this.GetCircularAxisLabelsStyle();

        // ============================*****
        // Calculate the auto-fit font if required
        // ============================*****
        if ( this.AxisX.LabelStyle.Enabled && this.AxisX.IsLabelAutoFit )
        {
            // Set max auto fit font
            this.AxisX.autoLabelFont = this.Common.ChartPicture.FontCache.GetFont(
                this.AxisX.LabelStyle.Font.FontFamily,
                14,
                this.AxisX.LabelStyle.Font.Style,
                GraphicsUnit.Point );

            // Get estimated labels size
            float labelsSizeEstimate = this.GetCircularLabelsSize( chartGraph, areaRectAbs, plotAreaRectAbs, titleSize );
            labelsSizeEstimate = ( float ) Math.Min( labelsSizeEstimate * 1.1f, plotAreaRectAbs.Width / 5f );
            labelsSizeEstimate += spacing;

            // Calculate auto-fit font
            this.AxisX.GetCircularAxisLabelsAutoFitFont( chartGraph, axisList, labelsStyle, plotAreaRectAbs, areaRectAbs, labelsSizeEstimate );
        }

        // ============================*****
        // Shrink plot area size proportionally
        // ============================*****

        // Get labels size
        float labelsSize = this.GetCircularLabelsSize( chartGraph, areaRectAbs, plotAreaRectAbs, titleSize );

        // Check if change size is smaller than radius
        labelsSize = ( float ) Math.Min( labelsSize, plotAreaRectAbs.Width / 2.5f );
        labelsSize += spacing;

        plotAreaRectAbs.X += labelsSize;
        plotAreaRectAbs.Width -= 2f * labelsSize;
        plotAreaRectAbs.Y += labelsSize;
        plotAreaRectAbs.Height -= 2f * labelsSize;

        // Restrict minimum plot area size
        if ( plotAreaRectAbs.Width < 1.0f )
        {
            plotAreaRectAbs.Width = 1.0f;
        }
        if ( plotAreaRectAbs.Height < 1.0f )
        {
            plotAreaRectAbs.Height = 1.0f;
        }

        plotArea = chartGraph.GetRelativeRectangle( plotAreaRectAbs );


        // ============================*****
        // Set axes labels size
        // ============================*****
        SizeF relativeLabelSize = chartGraph.GetRelativeSize( new SizeF( labelsSize, labelsSize ) );
        this.AxisX.labelSize = relativeLabelSize.Height;
        this.AxisX2.labelSize = relativeLabelSize.Height;
        this.AxisY.labelSize = relativeLabelSize.Width;
        this.AxisY2.labelSize = relativeLabelSize.Width;

    }

    /// <summary>
    /// Calculate size of the circular axis labels.
    /// </summary>
    /// <param name="chartGraph">Chart graphics object.</param>
    /// <param name="areaRectAbs">The Chart area position.</param>
    /// <param name="plotAreaRectAbs">Plotting area size.</param>
    /// <param name="titleSize">Size of title on the axes.</param>
    /// <returns>Circulat labels style.</returns>
    internal float GetCircularLabelsSize(
        ChartGraphics chartGraph,
        RectangleF areaRectAbs,
        RectangleF plotAreaRectAbs,
        SizeF titleSize )
    {
        // Find current horiz. and vert. spacing between plotting and chart areas
        SizeF areaDiff = new( plotAreaRectAbs.X - areaRectAbs.X, plotAreaRectAbs.Y - areaRectAbs.Y );
        areaDiff.Width -= titleSize.Width;
        areaDiff.Height -= titleSize.Height;

        // Get absolute center of the area
        PointF areaCenterAbs = chartGraph.GetAbsolutePoint( this.circularCenter );

        // Get circular axis list
        ArrayList axisList = this.GetCircularAxisList();

        // Get circular axis labels style
        CircularAxisLabelsStyle labelsStyle = this.GetCircularAxisLabelsStyle();

        // Defines on how much (pixels) the circular chart area radius should be reduced
        float labelsSize = 0f;

        // ============================*****
        // Loop through all axis labels
        // ============================*****
        foreach ( CircularChartAreaAxis axis in axisList )
        {
            // ============================*****
            // Measure label text
            // ============================*****
            SizeF textSize = chartGraph.MeasureString(
                axis.Title.Replace( "\\n", "\n" ),
this.AxisX.autoLabelFont ?? this.AxisX.LabelStyle.Font );
            textSize.Width = ( float ) Math.Ceiling( textSize.Width * 1.1f );
            textSize.Height = ( float ) Math.Ceiling( textSize.Height * 1.1f );


            // ============================*****
            // Calculate area size change depending on labels style
            // ============================*****
            if ( labelsStyle == CircularAxisLabelsStyle.Circular )
            {
                labelsSize = ( float ) Math.Max(
                    labelsSize,
                    textSize.Height );
            }
            else if ( labelsStyle == CircularAxisLabelsStyle.Radial )
            {
                float textAngle = axis.AxisPosition + 90;

                // For angled text find it's X and Y components
                float width = ( float ) Math.Cos( textAngle / 180F * Math.PI ) * textSize.Width;
                float height = ( float ) Math.Sin( textAngle / 180F * Math.PI ) * textSize.Width;
                width = ( float ) Math.Abs( Math.Ceiling( width ) );
                height = ( float ) Math.Abs( Math.Ceiling( height ) );

                // Reduce text size by current spacing between plotting area and chart area
                width -= areaDiff.Width;
                height -= areaDiff.Height;
                if ( width < 0 )
                    width = 0;
                if ( height < 0 )
                    height = 0;


                labelsSize = ( float ) Math.Max(
                    labelsSize,
                    Math.Max( width, height ) );
            }
            else if ( labelsStyle == CircularAxisLabelsStyle.Horizontal )
            {
                // Get text angle
                float textAngle = axis.AxisPosition;
                if ( textAngle > 180f )
                {
                    textAngle -= 180f;
                }

                // Get label rotated position
                PointF[] labelPosition = [new PointF( areaCenterAbs.X, plotAreaRectAbs.Y )];
                Matrix newMatrix = new();
                newMatrix.RotateAt( textAngle, areaCenterAbs );
                newMatrix.TransformPoints( labelPosition );

                // Calculate width
                float width = textSize.Width;
                width -= areaRectAbs.Right - labelPosition[0].X;
                if ( width < 0f )
                {
                    width = 0f;
                }

                labelsSize = ( float ) Math.Max(
                    labelsSize,
                    Math.Max( width, textSize.Height ) );
            }
        }

        return labelsSize;
    }

    /// <summary>
    /// True if polygons should be used instead of the circles for the chart area.
    /// </summary>
    internal bool CircularUsePolygons
    {
        get
        {
            // Check if value was precalculated
            if ( this._circularUsePolygons == int.MinValue )
            {
                this._circularUsePolygons = 0;

                // Look for custom properties in series
                foreach ( Series series in this.Common.DataManager.Series )
                {
                    if ( series.ChartArea == this.Name && series.IsVisible() )
                    {
                        // Get custom attribute
                        if ( series.IsCustomPropertySet( CustomPropertyName.AreaDrawingStyle ) )
                        {
                            this._circularUsePolygons = string.Compare( series[CustomPropertyName.AreaDrawingStyle], "Polygon", StringComparison.OrdinalIgnoreCase ) == 0
                                ? 1
                                : string.Compare( series[CustomPropertyName.AreaDrawingStyle], "Circle", StringComparison.OrdinalIgnoreCase ) == 0
                                    ? 0
                                    : throw (new InvalidOperationException( SR.ExceptionCustomAttributeValueInvalid( series[CustomPropertyName.AreaDrawingStyle], "AreaDrawingStyle" ) ));
                            break;
                        }
                    }
                }
            }

            return this._circularUsePolygons == 1;
        }
    }

    /// <summary>
    /// Gets circular area axis labels style.
    /// </summary>
    /// <returns>Axis labels style.</returns>
    internal CircularAxisLabelsStyle GetCircularAxisLabelsStyle()
    {
        CircularAxisLabelsStyle style = CircularAxisLabelsStyle.Auto;

        // Get maximum number of points in all series
        foreach ( Series series in this.Common.DataManager.Series )
        {
            if ( series.IsVisible() && series.ChartArea == this.Name && series.IsCustomPropertySet( CustomPropertyName.CircularLabelsStyle ) )
            {
                string styleName = series[CustomPropertyName.CircularLabelsStyle];
                style = string.Compare( styleName, "Auto", StringComparison.OrdinalIgnoreCase ) == 0
                    ? CircularAxisLabelsStyle.Auto
                    : string.Compare( styleName, "Circular", StringComparison.OrdinalIgnoreCase ) == 0
                        ? CircularAxisLabelsStyle.Circular
                        : string.Compare( styleName, "Radial", StringComparison.OrdinalIgnoreCase ) == 0
                                                ? CircularAxisLabelsStyle.Radial
                                                : string.Compare( styleName, "Horizontal", StringComparison.OrdinalIgnoreCase ) == 0
                                                                        ? CircularAxisLabelsStyle.Horizontal
                                                                        : throw (new InvalidOperationException( SR.ExceptionCustomAttributeValueInvalid( styleName, "CircularLabelsStyle" ) ));

            }
        }

        // Get auto style
        if ( style == CircularAxisLabelsStyle.Auto )
        {
            int sectorNumber = this.CircularSectorsNumber;
            style = CircularAxisLabelsStyle.Horizontal;
            if ( sectorNumber > 30 )
            {
                style = CircularAxisLabelsStyle.Radial;
            }
        }

        return style;
    }

    /// <summary>
    /// Number of sectors in the circular area.
    /// </summary>
    internal int CircularSectorsNumber
    {
        get
        {
            // Check if value was precalculated
            if ( this._circularSectorNumber == int.MinValue )
            {
                this._circularSectorNumber = this.GetCircularSectorNumber();
            }

            return this._circularSectorNumber;
        }
    }

    /// <summary>
    /// Gets number of sectors in the circular chart area.
    /// </summary>
    /// <returns>Number of sectors.</returns>
    private int GetCircularSectorNumber()
    {
        ICircularChartType type = this.GetCircularChartType();
        return type != null ? type.GetNumerOfSectors( this, this.Common.DataManager.Series ) : 0;
    }

    /// <summary>
    /// Fills a list of circular axis.
    /// </summary>
    /// <returns>Axes list.</returns>
    internal ArrayList GetCircularAxisList()
    {
        // Check if list was already created
        if ( this._circularAxisList == null )
        {
            this._circularAxisList = [];

            // Loop through all sectors
            int sectorNumber = this.GetCircularSectorNumber();
            for ( int sectorIndex = 0; sectorIndex < sectorNumber; sectorIndex++ )
            {
                // Create new axis object
                CircularChartAreaAxis axis = new( sectorIndex * 360f / sectorNumber );

                // Check if custom X axis labels will be used
                if ( this.AxisX.CustomLabels.Count > 0 )
                {
                    if ( sectorIndex < this.AxisX.CustomLabels.Count )
                    {
                        axis.Title = this.AxisX.CustomLabels[sectorIndex].Text;
                        axis.TitleForeColor = this.AxisX.CustomLabels[sectorIndex].ForeColor;
                    }
                }
                else
                {
                    // Get axis title from all series
                    foreach ( Series series in this.Common.DataManager.Series )
                    {
                        if ( series.IsVisible() && series.ChartArea == this.Name && sectorIndex < series.Points.Count )
                        {
                            if ( series.Points[sectorIndex].AxisLabel.Length > 0 )
                            {
                                axis.Title = series.Points[sectorIndex].AxisLabel;
                                break;
                            }
                        }
                    }
                }

                // Add axis into the list
                _ = this._circularAxisList.Add( axis );
            }

        }
        return this._circularAxisList;
    }

    /// <summary>
    /// Converts circular position of the X axis to angle in degrees.
    /// </summary>
    /// <param name="position">X axis position.</param>
    /// <returns>Angle in degrees.</returns>
    internal float CircularPositionToAngle( double position )
    {
        // Get X axis scale size
        double scaleRatio = 360.0 / Math.Abs( this.AxisX.Maximum - this.AxisX.Minimum );

        return ( float ) ((position * scaleRatio) + this.AxisX.Crossing);
    }

    #endregion

    #region " 2d series drawing order methods "

    /// <summary>
    /// Helper method that returns a list of 'ChartTypeAndSeriesInfo' objects.
    /// This list is used for chart area series drawing in 2D mode. Each
    /// object may represent an individual series or all series that belong
    /// to one chart type.
    /// 
    /// This method is intended to fix issues #6443 and #5385 when area chart 
    /// type incorrectly overlaps point or line chart type.
    /// </summary>
    /// <returns>List of 'ChartTypeAndSeriesInfo' objects.</returns>
    private ArrayList GetChartTypesAndSeriesToDraw()
    {
        ArrayList resultList = [];

        // Build chart type or series position based lists
        if ( this.ChartTypes.Count > 1 &&
            (this.ChartTypes.Contains( ChartTypeNames.Area )
            || this.ChartTypes.Contains( ChartTypeNames.SplineArea )
            )
            )
        {
            // Array of chart type names that do not require furher processing
            ArrayList processedChartType = [];
            ArrayList splitChartType = [];

            // Draw using the exact order in the series collection
            int seriesIndex = 0;
            foreach ( Series series in this.Common.DataManager.Series )
            {
                // Check if series is visible and belongs to the chart area
                if ( series.ChartArea == this.Name && series.IsVisible() && series.Points.Count > 0 )
                {
                    // Check if this chart type was already processed
                    if ( !processedChartType.Contains( series.ChartTypeName ) )
                    {
                        // Check if curent chart type can be individually processed
                        bool canBeIndividuallyProcessed = false;
                        if ( series.ChartType is SeriesChartType.Point or
                            SeriesChartType.Line or
                            SeriesChartType.Spline or
                            SeriesChartType.StepLine )
                        {
                            canBeIndividuallyProcessed = true;
                        }

                        if ( !canBeIndividuallyProcessed )
                        {
                            // Add a record to process all series of that chart type at once
                            _ = resultList.Add( new ChartTypeAndSeriesInfo( series.ChartTypeName ) );
                            _ = processedChartType.Add( series.ChartTypeName );
                        }
                        else
                        {
                            // Check if curent chart type has more that 1 series and they are split 
                            // by other series
                            bool chartTypeIsSplit = false;

                            if ( splitChartType.Contains( series.ChartTypeName ) )
                            {
                                chartTypeIsSplit = true;
                            }
                            else
                            {
                                bool otherChartTypeFound = false;
                                for ( int curentSeriesIndex = seriesIndex + 1; curentSeriesIndex < this.Common.DataManager.Series.Count; curentSeriesIndex++ )
                                {
                                    if ( series.ChartTypeName == this.Common.DataManager.Series[curentSeriesIndex].ChartTypeName )
                                    {
                                        if ( otherChartTypeFound )
                                        {
                                            chartTypeIsSplit = true;
                                            _ = splitChartType.Add( series.ChartTypeName );
                                        }
                                    }
                                    else
                                    {
                                        if ( this.Common.DataManager.Series[curentSeriesIndex].ChartType is SeriesChartType.Area or
                                            SeriesChartType.SplineArea )
                                        {
                                            otherChartTypeFound = true;
                                        }
                                    }
                                }
                            }

                            if ( chartTypeIsSplit )
                            {
                                // Add a record to process this series individually
                                _ = resultList.Add( new ChartTypeAndSeriesInfo( series ) );
                            }
                            else
                            {
                                // Add a record to process all series of that chart type at once
                                _ = resultList.Add( new ChartTypeAndSeriesInfo( series.ChartTypeName ) );
                                _ = processedChartType.Add( series.ChartTypeName );
                            }
                        }
                    }
                }

                ++seriesIndex;
            }
        }
        else
        {
            foreach ( string chartType in this.ChartTypes )
            {
                _ = resultList.Add( new ChartTypeAndSeriesInfo( chartType ) );
            }
        }

        return resultList;
    }

    /// <summary>
    /// Internal data structure that stores chart type name and optionally series object.
    /// </summary>
    internal class ChartTypeAndSeriesInfo
    {
        /// <summary>
        /// Object constructor.
        /// </summary>
        public ChartTypeAndSeriesInfo()
        {
        }

        /// <summary>
        /// Object constructor.
        /// </summary>
        /// <param name="chartType">Chart type name to initialize with.</param>
        public ChartTypeAndSeriesInfo( string chartType ) => this.ChartType = chartType;

        /// <summary>
        /// Object constructor.
        /// </summary>
        /// <param name="series">Series to initialize with.</param>
        public ChartTypeAndSeriesInfo( Series series )
        {
            this.ChartType = series.ChartTypeName;
            this.Series = series;
        }

        // Chart type name
        internal string ChartType = string.Empty;

        // Series object. Can be set to NULL!
        internal Series Series;

    }

    #endregion // 2D Series drawing order methods

    #region " idisposable members "

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            // Dispose managed resources
            if ( this._axisArray is not null )
            {
                foreach ( Axis axis in this._axisArray )
                {
                    axis.Dispose();
                }
                this._axisArray = null;
            }
            if ( this._areaPosition is not null )
            {
                this._areaPosition.Dispose();
                this._areaPosition = null;
            }
            if ( this._innerPlotPosition is not null )
            {
                this._innerPlotPosition.Dispose();
                this._innerPlotPosition = null;
            }
            if ( this.PlotAreaPosition is not null )
            {
                this.PlotAreaPosition.Dispose();
                this.PlotAreaPosition = null;
            }
            if ( this.areaBufferBitmap is not null )
            {
                this.areaBufferBitmap.Dispose();
                this.areaBufferBitmap = null;
            }
            if ( this._cursorX is not null )
            {
                this._cursorX.Dispose();
                this._cursorX = null;
            }
            if ( this._cursorY is not null )
            {
                this._cursorY.Dispose();
                this._cursorY = null;
            }
        }
        base.Dispose( disposing );
    }

    #endregion
}
