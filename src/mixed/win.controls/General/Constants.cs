
namespace System.Windows.Forms.DataVisualization.Charting;

internal static class Constants
{
    public const string AutoValue = "Auto";
    public const string NotSetValue = "NotSet";
    public const string MinValue = "Min";
    public const string MaxValue = "Max";
}

