//
//  Purpose:	Base class for the Axis class which defines axis 
//				csale related properties and methods.
//


using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " axis enumerations "
/// <summary>
/// An enumeration of the mode of automatically calculating intervals.
/// </summary>
public enum IntervalAutoMode
{
    /// <summary>
    /// Fixed number of intervals always created on the axis.
    /// </summary>
    FixedCount,

    /// <summary>
    /// Number of axis intervals depends on the axis length.
    /// </summary>
    VariableCount
}

/// <summary>
/// An enumeration of axis position.
/// </summary>
internal enum AxisPosition
{
    /// <summary>
    /// Left position
    /// </summary>
    Left,

    /// <summary>
    /// Right position
    /// </summary>
    Right,

    /// <summary>
    /// Top position
    /// </summary>
    Top,

    /// <summary>
    /// Bottom position
    /// </summary>
    Bottom
}
/// <summary>
/// An enumeration of axis arrow styles.
/// </summary>
public enum AxisArrowStyle
{
    /// <summary>
    /// No arrow
    /// </summary>
    None,
    /// <summary>
    /// Triangle type
    /// </summary>
    Triangle,
    /// <summary>
    /// Sharp triangle type
    /// </summary>
    SharpTriangle,
    /// <summary>
    /// Lines type
    /// </summary>
    Lines
}

#endregion
/// <summary>
/// The Axis class keeps information about minimum, maximum 
/// and interval values and it is responsible for setting 
/// these values automatically. It also handles
/// logarithmic and reversed axis.
/// </summary>
public partial class Axis
{
    #region " axis scale fields "

#pragma warning disable IDE1006 // Naming Styles
    // Represents the distance between the data points and its 
    // chart area margin, Measured as a percentage of default 
    // margin size.
    internal double margin = 100.0;
    internal double marginView;
    internal bool offsetTempSet;

    // Used for column chart margin
    internal double marginTemp;
    private readonly ArrayList _stripLineOffsets = [];


    // Data members, which store properties values
    private bool _isLogarithmic;
    internal double logarithmBase = 10.0;
    internal bool isReversed;
    internal bool isStartedFromZero = true;
    internal TickMark minorTickMark;
    internal TickMark majorTickMark;
    internal Grid minorGrid;
    internal Grid majorGrid;
    internal bool enabled;
    internal bool autoEnabled = true;
    internal LabelStyle labelStyle;
    private DateTimeIntervalType _internalIntervalType = DateTimeIntervalType.Auto;
    internal double maximum = double.NaN;
    internal double crossing = double.NaN;
    internal double minimum = double.NaN;

    // Temporary Minimum and maximum values.
    internal double tempMaximum = double.NaN;
    internal double tempMinimum = double.NaN;
    internal double tempCrossing = double.NaN;
    internal CustomLabelsCollection tempLabels;
    internal bool tempAutoMaximum = true;
    internal bool tempAutoMinimum = true;
    internal double tempMajorGridInterval = double.NaN;
    internal double tempMinorGridInterval;
    internal double tempMajorTickMarkInterval = double.NaN;
    internal double tempMinorTickMarkInterval;
    internal double tempLabelInterval = double.NaN;
    internal DateTimeIntervalType tempGridIntervalType = DateTimeIntervalType.NotSet;
    internal DateTimeIntervalType tempTickMarkIntervalType = DateTimeIntervalType.NotSet;
    internal DateTimeIntervalType tempLabelIntervalType = DateTimeIntervalType.NotSet;

    // Paint mode
    internal bool paintMode;

    // Axis type (X, Y, X2, Y2)
    internal AxisName axisType = AxisName.X;

    /// <summary>
    /// Axis position: Left, Right, Top Bottom
    /// </summary>
    private AxisPosition _axisPosition = AxisPosition.Left;

    /// <summary>
    /// Opposite Axis for this Axis. Necessary for Crossing.		
    /// </summary>
    internal Axis oppositeAxis;

    // Axis data scaleView
    private AxisScaleView _scaleView;

    // Axis scroll bar class
    internal AxisScrollBar scrollBar;

    // For scater chart X values could be rounded.
    internal bool roundedXValues;

    // If Axis is logarithmic value shoud be converted to 
    // linear only once.
    internal bool logarithmicConvertedToLinear;

    // IsLogarithmic minimum value
    internal double logarithmicMinimum;

    // IsLogarithmic maximum value
    internal double logarithmicMaximum;

    // Correction of interval because of 
    // 3D Rotation and perspective
    internal double interval3DCorrection = double.NaN;

    // Axis coordinate convertion optimization fields
    internal bool optimizedGetPosition;
    internal double paintViewMax;
    internal double paintViewMin;
    internal double paintRange;
    internal double valueMultiplier;
    internal RectangleF paintAreaPosition = RectangleF.Empty;
    internal double paintAreaPositionBottom;
    internal double paintAreaPositionRight;
    internal double paintChartAreaSize;



    // Determines how number of intervals automatically calculated
    private IntervalAutoMode _intervalAutoMode = IntervalAutoMode.FixedCount;

    // True if scale segments are used
    internal bool scaleSegmentsUsed;



    // Preffered number of intervals on the axis
    internal int prefferedNumberofIntervals = 5;

    private readonly Stack<double> _intervalsStore = new();

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " axis scale properties "

    /// <summary>
    /// Axis position
    /// </summary>
    [
    Bindable( true ),
    DefaultValue( AxisPosition.Left ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeReverse" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    internal virtual AxisPosition AxisPosition
    {
        get => this._axisPosition;
        set
        {
            this._axisPosition = value;
#if SUBAXES
				// Update axis position of the sub axis
				if( !((Axis)this).IsSubAxis )
				{
					foreach(SubAxis subAxis in ((Axis)this).SubAxes)
					{
						subAxis._axisPosition = value;
					}
				}

#endif // SUBAXES
            this.Invalidate();
        }
    }



    /// <summary>
    /// Gets or sets a flag which indicates whether the number of intervals
    /// on the axis is fixed or varies with the axis size.
    /// </summary>
    [
    SRCategory( "CategoryAttributeInterval" ),
    DefaultValue( IntervalAutoMode.FixedCount ),
    SRDescription( "DescriptionAttributeIntervalAutoMode" ),
    ]
    public IntervalAutoMode IntervalAutoMode
    {
        get => this._intervalAutoMode;
        set
        {
            this._intervalAutoMode = value;
            this.Invalidate();
        }
    }



    /// <summary>
    /// Gets or sets a flag which indicates whether the axis is reversed.
    /// If set to reversed, the values on the axis are in reversed sort order
    /// and the direction of values on the axis is flipped.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( false ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeReverse" ),
    ]
    public bool IsReversed
    {
        get => this.isReversed;
        set
        {
            this.isReversed = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a flag which indicates whether the minimum value
    /// of the axis will be automatically set to zero if all data point
    /// values are positive.  If there are negative data point values,
    /// the minimum value of the data points will be used.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( true ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeStartFromZero3" ),
    ]
    public bool IsStartedFromZero
    {
        get => this.isStartedFromZero;
        set
        {
            this.isStartedFromZero = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a flag to add a margin to the axis.
    /// If true, a space is added between the first/last data 
    /// point and the border of chart area.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( true ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeMargin" ),
    ]
    public bool IsMarginVisible
    {
        get
        {
            return this.margin > 0;
        }
        set
        {
            this.margin = value ? 100 : 0;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Date and time interval type.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( DateTimeIntervalType.Auto ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeInternalIntervalType" ),
    RefreshProperties( RefreshProperties.All ),
    ]
    internal DateTimeIntervalType InternalIntervalType
    {
        get => this._internalIntervalType;
        set
        {
            // Set intervals for labels, grids and tick marks. ( Auto interval type )
            if ( this.tempMajorGridInterval <= 0.0 ||
                (double.IsNaN( this.tempMajorGridInterval ) && this.Interval <= 0.0) )
            {
                this.majorGrid.intervalType = value;
            }

            if ( this.tempMajorTickMarkInterval <= 0.0 ||
                (double.IsNaN( this.tempMajorTickMarkInterval ) && this.Interval <= 0.0) )
            {
                this.majorTickMark.intervalType = value;
            }

            if ( this.tempLabelInterval <= 0.0 ||
                (double.IsNaN( this.tempLabelInterval ) && this.Interval <= 0.0) )
            {
                this.labelStyle.intervalType = value;
            }

            this._internalIntervalType = value;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Sets auto interval values to grids, tick marks
    /// and labels
    /// </summary>
    internal double SetInterval
    {
        set
        {
            if ( this.tempMajorGridInterval <= 0.0 ||
                (double.IsNaN( this.tempMajorGridInterval ) && this.Interval <= 0.0) )
            {
                this.majorGrid.interval = value;
            }

            if ( this.tempMajorTickMarkInterval <= 0.0 ||
                (double.IsNaN( this.tempMajorTickMarkInterval ) && this.Interval <= 0.0) )
            {
                this.majorTickMark.interval = value;
            }

            if ( this.tempLabelInterval <= 0.0 ||
                (double.IsNaN( this.tempLabelInterval ) && this.Interval <= 0.0) )
            {
                this.labelStyle.interval = value;
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Sets auto interval values to grids, tick marks
    /// and labels
    /// </summary>
    internal void SetIntervalAndType( double newInterval, DateTimeIntervalType newIntervalType )
    {
        if ( this.tempMajorGridInterval <= 0.0 ||
            (double.IsNaN( this.tempMajorGridInterval ) && this.Interval <= 0.0) )
        {
            this.majorGrid.interval = newInterval;
            this.majorGrid.intervalType = newIntervalType;
        }

        if ( this.tempMajorTickMarkInterval <= 0.0 ||
            (double.IsNaN( this.tempMajorTickMarkInterval ) && this.Interval <= 0.0) )
        {
            this.majorTickMark.interval = newInterval;
            this.majorTickMark.intervalType = newIntervalType;
        }

        if ( this.tempLabelInterval <= 0.0 ||
            (double.IsNaN( this.tempLabelInterval ) && this.Interval <= 0.0) )
        {
            this.labelStyle.interval = newInterval;
            this.labelStyle.intervalType = newIntervalType;
        }

        this.Invalidate();
    }

    /// <summary>
    /// Gets or sets the maximum axis value.
    /// </summary>
    [

    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( double.NaN ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeMaximum" ),
    TypeConverter( typeof( AxisMinMaxAutoValueConverter ) )
    ]
    public double Maximum
    {
        get
        {
            // Get maximum
            return this._isLogarithmic && this.logarithmicConvertedToLinear && !double.IsNaN( this.maximum )
                ? this.logarithmicMaximum
                : this.maximum;
        }
        set
        {
            // Split a value to maximum and auto maximum
            if ( double.IsNaN( value ) )
            {
                this.AutoMaximum = true;
                this.maximum = double.NaN;
            }
            else
            {
                // Set maximum
                this.maximum = value;

                // Set non linearized Maximum for logarithmic scale
                this.logarithmicMaximum = value;

                this.AutoMaximum = false;
            }

            // Reset original property value fields
            this.tempMaximum = this.maximum;

            // This line is added because of Save ScaleView State August 29, 2003
            // in Web Forms. This place could cause problems with Reset Auto Values.
            this.tempAutoMaximum = this.AutoMaximum;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the minimum axis value
    /// </summary>
    [

    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( double.NaN ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeMinimum" ),
    TypeConverter( typeof( AxisMinMaxAutoValueConverter ) )
    ]
    public double Minimum
    {
        get
        {
            // Get minimum
            return this._isLogarithmic && this.logarithmicConvertedToLinear && !double.IsNaN( this.maximum )
                ? this.logarithmicMinimum
                : this.minimum;
        }
        set
        {
            // Split a value to minimum and auto minimum
            if ( double.IsNaN( value ) )
            {
                this.AutoMinimum = true;
                this.minimum = double.NaN;
            }
            else
            {
                // Set maximum
                this.minimum = value;
                this.AutoMinimum = false;

                // Set non linearized Minimum for logarithmic scale
                this.logarithmicMinimum = value;
            }

            // Reset original property value fields
            this.tempMinimum = this.minimum;

            // This line is added because of Save ScaleView State August 29, 2003
            // in Web Forms. This place could cause problems with Reset Auto Values.
            this.tempAutoMinimum = this.AutoMinimum;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the point where axis is crossed by another axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( double.NaN ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeCrossing" ),
    TypeConverter( typeof( AxisCrossingValueConverter ) )
    ]
    public virtual double Crossing
    {
        get
        {
            if ( this.paintMode )
                return this._isLogarithmic ? Math.Pow( this.logarithmBase, this.GetCrossing() ) : this.GetCrossing();
            else
                return this.crossing;
        }
        set
        {
            this.crossing = value;

            // Reset original property value fields
            this.tempCrossing = this.crossing;

            this.Invalidate();
        }
    }

    /// <summary>
    /// Enables or disables the axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    Bindable( true ),
    DefaultValue( typeof( AxisEnabled ), "Auto" ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeEnabled7" ),
    ]
    public AxisEnabled Enabled
    {
        get
        {
            // Take Enabled from two fields: enabled and auto enabled
            if ( this.autoEnabled )
            {
                return AxisEnabled.Auto;
            }
            else
            {
                return this.enabled ? AxisEnabled.True : AxisEnabled.False;
            }
        }
        set
        { // Split Enabled to two fields: enabled and auto enabled
            if ( value == AxisEnabled.Auto )
            {
                this.autoEnabled = true;
            }
            else if ( value == AxisEnabled.True )
            {
                this.enabled = true;
                this.autoEnabled = false;
            }
            else
            {
                this.enabled = false;
                this.autoEnabled = false;
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a flag which indicates whether the axis is logarithmic. 
    /// Zeros or negative data values are not allowed on logarithmic charts. 
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( false ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeLogarithmic" ),
    ]
    public bool IsLogarithmic
    {
        get => this._isLogarithmic;
        set
        {
            this._isLogarithmic = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Base of the logarithm used in logarithmic scale. 
    /// By default, this value is 10.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Bindable( true ),
    DefaultValue( 10.0 ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeLogarithmBase" ),
    ]
    public double LogarithmBase
    {
        get => this.logarithmBase;
        set
        {
            if ( value < 2.0 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), SR.ExceptionAxisScaleLogarithmBaseInvalid );
            }

            this.logarithmBase = value;

            this.Invalidate();
        }
    }

    #endregion

    #region " axis segments and scale breaks properties "

#pragma warning disable IDE1006 // Naming Styles
    // Field that stores Axis automatic scale breaks style.
    internal AxisScaleBreakStyle axisScaleBreakStyle;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Gets or sets the style of scale breaks.
    /// </summary>
    [
SRCategory( "CategoryAttributeScale" ),
SRDescription( "DescriptionAttributeScaleBreakStyle" ),
TypeConverter( typeof( NoNameExpandableObjectConverter ) ),
NotifyParentProperty( true ),
DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
]
    public virtual AxisScaleBreakStyle ScaleBreakStyle
    {
        get => this.axisScaleBreakStyle;
        set
        {
            this.axisScaleBreakStyle = value;
            this.axisScaleBreakStyle.axis = this;
            //this.Invalidate();
        }
    }

#pragma warning disable IDE1006 // Naming Styles
    // Field that stores axis scale segments
    internal AxisScaleSegmentCollection scaleSegments;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Axis scale segment collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeScale" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SRDescription( "DescriptionAttributeAxisScaleSegmentCollection_axisScaleSegmentCollection" ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) )
    ]
    internal AxisScaleSegmentCollection ScaleSegments => this.scaleSegments;

    #endregion // Axis Segments and Scale Breaks Properties

    #region " axis data scaleview properties and methods "

    /// <summary>
    /// Gets or sets the scale view settings of the axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeDataView" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeView" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public AxisScaleView ScaleView
    {
        get => this._scaleView;
        set
        {
            this._scaleView = value;
            this._scaleView.axis = this;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the scroll bar settings of the axis.
    /// </summary>
    [
    SRCategory( "CategoryAttributeDataView" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeScrollBar" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    TypeConverter( typeof( NoNameExpandableObjectConverter ) )
    ]
    public AxisScrollBar ScrollBar
    {
        get => this.scrollBar;
        set
        {
            this.scrollBar = value;
            this.scrollBar.axis = this;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets axis data scaleView minimum position.
    /// </summary>
    /// <returns>Axis data scaleView minimum position.</returns>
    internal double ViewMinimum => this._scaleView.ViewMinimum;

    /// <summary>
    /// Gets axis data scaleView minimum position.
    /// </summary>
    /// <returns>Axis data scaleView minimum position.</returns>
    internal double ViewMaximum => this._scaleView.ViewMaximum;

    /// <summary>
    /// Gets automatic maximum value (from data point values). 
    /// </summary>
    internal bool AutoMaximum { get; private set; } = true;

    /// <summary>
    /// Gets automatic minimum value (from data point values). 
    /// </summary>
    internal bool AutoMinimum { get; private set; } = true;

    #endregion

    #region " axis position converters methos "

    /// <summary>
    /// This function converts axis value to relative position (0-100%). 
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="axisValue">Value from axis.</param>
    /// <returns>Relative position (0-100%).</returns>
    public double GetPosition( double axisValue )
    {
        // Adjust for the IsLogarithmic axis
        if ( this._isLogarithmic && axisValue != 0.0 )
        {
            axisValue = Math.Log( axisValue, this.logarithmBase );
        }

        // Get linear position
        return this.GetLinearPosition( axisValue );
    }

    /// <summary>
    /// This function converts an axis value to relative position (0-100%). 
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="axisValue">Axis value.</param>
    /// <returns>Relative position (0-100%).</returns>
    public double ValueToPosition( double axisValue )
    {
        return this.GetPosition( axisValue );
    }

    /// <summary>
    /// This function converts an axis value to a pixel position. 
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="axisValue">Value from axis.</param>
    /// <returns>Pixel position.</returns>
    public double ValueToPixelPosition( double axisValue )
    {
        // Get relative value
        double val = this.ValueToPosition( axisValue );

        // Convert it to pixels
        if ( this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom )
        {
            val *= (this.Common.ChartPicture.Width - 1) / 100F;
        }
        else
        {
            val *= (this.Common.ChartPicture.Height - 1) / 100F;
        }

        return val;
    }

    /// <summary>
    /// This function converts a relative position to an axis value.
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="position">Relative position (0-100%).</param>
    /// <returns>Axis value.</returns>
    public double PositionToValue( double position )
    {
        return this.PositionToValue( position, true );
    }

    /// <summary>
    /// This function converts a relative position to an axis value.
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="position">Relative position (0-100%).</param>
    /// <param name="validateInput">Indicates if input value range should be checked.</param>
    /// <returns>Axis value.</returns>
    internal double PositionToValue( double position, bool validateInput )
    {
        // Check parameters
        if ( validateInput &&
            (position < 0 || position > 100) )
        {
            throw new ArgumentException( SR.ExceptionAxisScalePositionInvalid, nameof( position ) );
        }

        // Check if plot area position was already calculated
        if ( this.PlotAreaPosition == null )
        {
            throw new InvalidOperationException( SR.ExceptionAxisScalePositionToValueCallFailed );
        }

        // Convert chart picture position to plotting position
        position = this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
            ? position - this.PlotAreaPosition.X
            : this.PlotAreaPosition.Bottom - position;


        // The Chart area size
        double ChartArea = this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
            ? this.PlotAreaPosition.Width
            : this.PlotAreaPosition.Height;


        // The Real range as double
        double viewMax = this.ViewMaximum;
        double viewMin = this.ViewMinimum;
        double range = viewMax - viewMin;

        // Avoid division by zero
        double axisValue = 0;
        if ( range != 0 )
        {
            // Find axis value from position
            axisValue = range / ChartArea * position;
        }

        // Corrected axis value for reversed
        axisValue = this.isReversed ? viewMax - axisValue : viewMin + axisValue;

        return axisValue;
    }

    /// <summary>
    /// This function converts a pixel position to an axis value.
    /// If an axis has a logarithmic scale, the value is converted to a linear scale.
    /// </summary>
    /// <param name="position">Pixel position.</param>
    /// <returns>Axis value.</returns>
    public double PixelPositionToValue( double position )
    {
        // Convert it to pixels
        double val = position;
        if ( this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom )
        {
            val *= 100F / (this.Common.ChartPicture.Width - 1);
        }
        else
        {
            val *= 100F / (this.Common.ChartPicture.Height - 1);
        }

        // Get from relative position
        return this.PositionToValue( val );
    }

    #endregion

    #region " axis scale methods "

    /// <summary>
    /// Sets axis position. Axis position depends 
    /// on crossing and reversed value.
    /// </summary>
    internal void SetAxisPosition()
    {
        // Change position of the axis
        if ( this.GetOppositeAxis().isReversed )
        {
            if ( this.AxisPosition == AxisPosition.Left )
                this.AxisPosition = AxisPosition.Right;
            else if ( this.AxisPosition == AxisPosition.Right )
                this.AxisPosition = AxisPosition.Left;
            else if ( this.AxisPosition == AxisPosition.Top )
                this.AxisPosition = AxisPosition.Bottom;
            else if ( this.AxisPosition == AxisPosition.Bottom )
                this.AxisPosition = AxisPosition.Top;
        }
    }

    /// <summary>
    /// Sets temporary offset value.
    /// </summary>
    internal void SetTempAxisOffset()
    {
        if ( this.ChartArea.Series.Count == 0 )
        {
            return;
        }
        // Conditions when this code changes margin size: Column chart, 
        // margin is turned off, Interval offset is not used for 
        // gridlines, tick marks and labels.
        Series ser = this.ChartArea.GetFirstSeries();
        if ( (ser.ChartType == SeriesChartType.Column ||
            ser.ChartType == SeriesChartType.StackedColumn ||
            ser.ChartType == SeriesChartType.StackedColumn100 ||
            ser.ChartType == SeriesChartType.Bar ||

            ser.ChartType == SeriesChartType.RangeBar ||
            ser.ChartType == SeriesChartType.RangeColumn ||

            ser.ChartType == SeriesChartType.StackedBar ||
            ser.ChartType == SeriesChartType.StackedBar100) &&
            this.margin != 100.0 && !this.offsetTempSet &&
            this.AutoMinimum )
        {
            // Find offset correction for Column chart margin.
            double offset;
            this.marginTemp = this.margin;

            // Find point width
            // Check if series provide custom value for point width
            double pointWidthSize;
            string strWidth = ser[CustomPropertyName.PointWidth];
            pointWidthSize = strWidth != null ? CommonElements.ParseDouble( strWidth ) : 0.8;

            this.margin = pointWidthSize / 2 * 100;
            offset = this.margin / 100;
            double contraOffset = (100 - this.margin) / 100;

            if ( this._intervalsStore.Count == 0 )
            {
                this._intervalsStore.Push( this.labelStyle.intervalOffset );
                this._intervalsStore.Push( this.majorGrid.intervalOffset );
                this._intervalsStore.Push( this.majorTickMark.intervalOffset );
                this._intervalsStore.Push( this.minorGrid.intervalOffset );
                this._intervalsStore.Push( this.minorTickMark.intervalOffset );
            }

            this.labelStyle.intervalOffset = double.IsNaN( this.labelStyle.intervalOffset ) ? offset : this.labelStyle.intervalOffset + offset;
            this.majorGrid.intervalOffset = double.IsNaN( this.majorGrid.intervalOffset ) ? offset : this.majorGrid.intervalOffset + offset;
            this.majorTickMark.intervalOffset = double.IsNaN( this.majorTickMark.intervalOffset ) ? offset : this.majorTickMark.intervalOffset + offset;
            this.minorGrid.intervalOffset = double.IsNaN( this.minorGrid.intervalOffset ) ? offset : this.minorGrid.intervalOffset + offset;
            this.minorTickMark.intervalOffset = double.IsNaN( this.minorTickMark.intervalOffset ) ? offset : this.minorTickMark.intervalOffset + offset;

            foreach ( StripLine strip in this.StripLines )
            {
                _ = this._stripLineOffsets.Add( strip.IntervalOffset );
                strip.IntervalOffset -= contraOffset;
            }
            this.offsetTempSet = true;
        }
    }

    /// <summary>
    /// Resets temporary offset value.
    /// </summary>
    internal void ResetTempAxisOffset()
    {
        if ( this.offsetTempSet )
        {
            System.Diagnostics.Debug.Assert( this._intervalsStore.Count == 5, "Fail in interval store count" );

            this.minorTickMark.intervalOffset = this._intervalsStore.Pop();
            this.minorGrid.intervalOffset = this._intervalsStore.Pop();
            this.majorTickMark.intervalOffset = this._intervalsStore.Pop();
            this.majorGrid.intervalOffset = this._intervalsStore.Pop();
            this.labelStyle.intervalOffset = this._intervalsStore.Pop();
            int index = 0;
            foreach ( StripLine strip in this.StripLines )
            {
                if ( this._stripLineOffsets.Count > index )
                {
                    strip.IntervalOffset = ( double ) this._stripLineOffsets[index];
                }
                index++;
            }
            this._stripLineOffsets.Clear();
            this.offsetTempSet = false;
            this.margin = this.marginTemp;
        }
    }

    /// <summary>
    /// This function will create auto maximum and minimum values 
    /// using the interval. This function will make a gap between 
    /// data points and border of the chart area.
    /// </summary>
    /// <param name="inter">Interval</param>
    /// <param name="shouldStartFromZero">True if minimum scale value should start from zero.</param>
    /// <param name="autoMax">Maximum is auto</param>
    /// <param name="autoMin">Minimum is auto</param>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Interval</returns>
    internal double RoundedValues(
        double inter,
        bool shouldStartFromZero,
        bool autoMax,
        bool autoMin,
        ref double min,
        ref double max )
    {
        // For X Axes
        if ( this.axisType is AxisName.X or AxisName.X2 )
        {
            if ( this.margin == 0.0 && !this.roundedXValues )
            {
                return inter;
            }
        }
        else // For Y Axes
        {
            // Avoid dividing with 0. There is no gap.
            if ( this.margin == 0.0 )
            {
                return inter;
            }
        }

        if ( autoMin )
        { // Set minimum value
            min = min < 0.0 || (!shouldStartFromZero && !this.ChartArea.stacked)
                ? ( double ) ((( decimal ) Math.Ceiling( min / inter ) - 1m) * ( decimal ) inter)
                : 0.0;
        }
        if ( autoMax )
        {// Set maximum value
            max = max <= 0.0 && shouldStartFromZero ? 0.0 : ( double ) ((( decimal ) Math.Floor( max / inter ) + 1m) * ( decimal ) inter);
        }
        return inter;
    }

    /// <summary>
    /// Recalculates an intelligent interval from real interval.
    /// </summary>
    /// <param name="diff">Real interval.</param>
    /// <returns>Inteligent interval.</returns>
    internal double CalcInterval( double diff )
    {
        // If the interval is zero return error
        if ( diff == 0.0 )
        {
            throw new ArgumentOutOfRangeException( nameof( diff ), SR.ExceptionAxisScaleIntervalIsZero );
        }

        // If the real interval is > 1.0
        double step = -1;
        double temp = diff;
        while ( temp > 1.0 )
        {
            step++;
            temp /= 10.0;
            if ( step > 1000 )
            {
                throw new InvalidOperationException( SR.ExceptionAxisScaleMinimumMaximumInvalid );
            }
        }


        // If the real interval is < 1.0
        temp = diff;
        if ( temp < 1.0 )
        {
            step = 0;
        }

        while ( temp < 1.0 )
        {
            step--;
            temp *= 10.0;
            if ( step < -1000 )
            {
                throw new InvalidOperationException( SR.ExceptionAxisScaleMinimumMaximumInvalid );
            }
        }

        double power = this.IsLogarithmic ? this.logarithmBase : 10.0;
        double tempDiff = diff / Math.Pow( power, step );

        tempDiff = tempDiff < 3 ? 2 : tempDiff < 7 ? 5 : 10;

        // Make a correction of the real interval
        return tempDiff * Math.Pow( power, step );
    }

    /// <summary>
    /// Recalculates a intelligent interval from real interval 
    /// obtained from maximum and minimum values
    /// </summary>
    /// <param name="min">Minimum</param>
    /// <param name="max">Maximum</param>
    /// <returns>Auto Interval</returns>
    private double CalcInterval( double min, double max )
    {
        // Approximated interval value
        return this.CalcInterval( (max - min) / 5 );
    }

    /// <summary>
    /// Recalculates a intelligent interval from real interval 
    /// obtained from maximum, minimum and date type if 
    /// the values is date-time value.
    /// </summary>
    /// <param name="min">Minimum value.</param>
    /// <param name="max">Maximum value.</param>
    /// <param name="date">True if date.</param>
    /// <param name="type">Date time interval type.</param>
    /// <param name="valuesType">AxisName of date-time values.</param>
    /// <returns>Auto Interval.</returns>
    internal double CalcInterval(
        double min,
        double max,
        bool date,
        out DateTimeIntervalType type,
        ChartValueType valuesType )
    {
        // AxisName is date time
        if ( date )
        {
            DateTime dateTimeMin = DateTime.FromOADate( min );
            DateTime dateTimeMax = DateTime.FromOADate( max );
            TimeSpan timeSpan = dateTimeMax.Subtract( dateTimeMin );

            // Minutes
            double inter = timeSpan.TotalMinutes;

            // For Range less than 60 seconds interval is 5 sec
            if ( inter <= 1.0 && valuesType != ChartValueType.Date )
            {
                // Milli Seconds
                double mlSeconds = timeSpan.TotalMilliseconds;
                if ( mlSeconds <= 10 )
                {
                    type = DateTimeIntervalType.Milliseconds;
                    return 1;
                }
                if ( mlSeconds <= 50 )
                {
                    type = DateTimeIntervalType.Milliseconds;
                    return 4;
                }
                if ( mlSeconds <= 200 )
                {
                    type = DateTimeIntervalType.Milliseconds;
                    return 20;
                }
                if ( mlSeconds <= 500 )
                {
                    type = DateTimeIntervalType.Milliseconds;
                    return 50;
                }

                // Seconds
                double seconds = timeSpan.TotalSeconds;

                if ( seconds <= 7 )
                {
                    type = DateTimeIntervalType.Seconds;
                    return 1;
                }
                else if ( seconds <= 15 )
                {
                    type = DateTimeIntervalType.Seconds;
                    return 2;
                }
                else if ( seconds <= 30 )
                {
                    type = DateTimeIntervalType.Seconds;
                    return 5;
                }
                else if ( seconds <= 60 )
                {
                    type = DateTimeIntervalType.Seconds;
                    return 10;
                }

            }// For Range less than 120 seconds interval is 10 sec
            else if ( inter <= 2.0 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Seconds;
                return 20;
            }// For Range less than 180 seconds interval is 30 sec
            else if ( inter <= 3.0 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Seconds;
                return 30;
            }

            // For Range less than 10 minutes interval is 1 min
            else if ( inter <= 10 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Minutes;
                return 1;
            }
            // For Range less than 20 minutes interval is 1 min
            else if ( inter <= 20 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Minutes;
                return 2;
            }// For Range less than 60 minutes interval is 5 min
            else if ( inter <= 60 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Minutes;
                return 5;
            }// For Range less than 120 minutes interval is 10 min
            else if ( inter <= 120 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Minutes;
                return 10;
            }// For Range less than 180 minutes interval is 30 min
            else if ( inter <= 180 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Minutes;
                return 30;
            }
            // For Range less than 12 hours interval is 1 hour
            else if ( inter <= 60 * 12 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Hours;
                return 1;
            }
            // For Range less than 24 hours interval is 4 hour
            else if ( inter <= 60 * 24 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Hours;
                return 4;
            }
            // For Range less than 2 days interval is 6 hour
            else if ( inter <= 60 * 24 * 2 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Hours;
                return 6;
            }
            // For Range less than 3 days interval is 12 hour
            else if ( inter <= 60 * 24 * 3 && valuesType != ChartValueType.Date )
            {
                type = DateTimeIntervalType.Hours;
                return 12;
            }

            // For Range less than 10 days interval is 1 day
            else if ( inter <= 60 * 24 * 10 )
            {
                type = DateTimeIntervalType.Days;
                return 1;
            }
            // For Range less than 20 days interval is 2 day
            else if ( inter <= 60 * 24 * 20 )
            {
                type = DateTimeIntervalType.Days;
                return 2;
            }
            // For Range less than 30 days interval is 3 day
            else if ( inter <= 60 * 24 * 30 )
            {
                type = DateTimeIntervalType.Days;
                return 3;
            }
            // For Range less than 2 months interval is 1 week
            else if ( inter <= 60 * 24 * 30.5 * 2 )
            {
                type = DateTimeIntervalType.Weeks;
                return 1;
            }
            // For Range less than 5 months interval is 2weeks
            else if ( inter <= 60 * 24 * 30.5 * 5 )
            {
                type = DateTimeIntervalType.Weeks;
                return 2;
            }
            // For Range less than 12 months interval is 1 month
            else if ( inter <= 60 * 24 * 30.5 * 12 )
            {
                type = DateTimeIntervalType.Months;
                return 1;
            }
            // For Range less than 24 months interval is 3 month
            else if ( inter <= 60 * 24 * 30.5 * 24 )
            {
                type = DateTimeIntervalType.Months;
                return 3;
            }
            // For Range less than 48 months interval is 6 months 
            else if ( inter <= 60 * 24 * 30.5 * 48 )
            {
                type = DateTimeIntervalType.Months;
                return 6;
            }
            // For Range more than 48 months interval is year 
            else if ( inter >= 60 * 24 * 30.5 * 48 )
            {
                type = DateTimeIntervalType.Years;
                return this.CalcYearInterval( inter / 60 / 24 / 365 );
            }
        }

        // Else numbers
        type = DateTimeIntervalType.Number;
        return this.CalcInterval( min, max );

    }

    /// <summary>
    /// Recalculates a intelligent interval for years
    /// </summary>
    /// <param name="years">Number of years</param>
    /// <returns>Interval in years</returns>
    private double CalcYearInterval( double years )
    {
        // If the interval is zero return error
        if ( years <= 1.0 )
        {
            throw new ArgumentOutOfRangeException( nameof( years ), SR.ExceptionAxisScaleIntervalIsLessThen1Year );
        }

        if ( years < 5 )
            return 1;
        else if ( years < 10 )
            return 2;

        // Make a correction of the interval
        return Math.Floor( years / 5 );
    }

    /// <summary>
    /// This method returns the number of units 
    /// between min and max.
    /// </summary>
    /// <param name="min">Minimum.</param>
    /// <param name="max">Maximum.</param>
    /// <param name="type">Date type.</param>
    /// <returns>Number of units.</returns>
    private int GetNumOfUnits( double min, double max, DateTimeIntervalType type )
    {
        double current = ChartHelper.GetIntervalSize( min, 1, type );
        return ( int ) Math.Round( (max - min) / current );
    }

    /// <summary>
    /// This method checks if value type is date-time.
    /// </summary>
    /// <returns>Date-time type or Auto.</returns>
    internal ChartValueType GetDateTimeType()
    {
        List<string> list = null;

        ChartValueType dateType = ChartValueType.Auto;

        // Check if Value type is date from first series in the axis
        if ( this.axisType == AxisName.X )
        {
            // Check X axes type
            list = this.ChartArea.GetXAxesSeries( AxisType.Primary, this.SubAxisName );
            if ( list.Count == 0 )
            {
                return ChartValueType.Auto;
            }

            if ( this.Common.DataManager.Series[list[0]].IsXValueDateTime() )
            {
                dateType = this.Common.DataManager.Series[list[0]].XValueType;
            }
        }
        else if ( this.axisType == AxisName.X2 )
        {
            // Check X2 axes type
            list = this.ChartArea.GetXAxesSeries( AxisType.Secondary, this.SubAxisName );
            if ( list.Count == 0 )
            {
                return ChartValueType.Auto;
            }

            if ( this.Common.DataManager.Series[list[0]].IsXValueDateTime() )
            {
                dateType = this.Common.DataManager.Series[list[0]].XValueType;
            }
        }
        else if ( this.axisType == AxisName.Y )
        {
            // Check Y axes type
            list = this.ChartArea.GetYAxesSeries( AxisType.Primary, this.SubAxisName );
            if ( list.Count == 0 )
            {
                return ChartValueType.Auto;
            }

            if ( this.Common.DataManager.Series[list[0]].IsYValueDateTime() )
            {
                dateType = this.Common.DataManager.Series[list[0]].YValueType;
            }
        }
        else if ( this.axisType == AxisName.Y2 )
        {
            // Check Y2 axes type
            list = this.ChartArea.GetYAxesSeries( AxisType.Secondary, this.SubAxisName );
            if ( list.Count == 0 )
            {
                return ChartValueType.Auto;
            }

            if ( this.Common.DataManager.Series[list[0]].IsYValueDateTime() )
            {
                dateType = this.Common.DataManager.Series[list[0]].YValueType;
            }
        }

        return dateType;
    }

    /// <summary>
    /// This method removes "Auto", "min", "max" from crossing
    /// value and creates a double value.
    /// </summary>
    /// <returns>Crossing value</returns>
    private double GetCrossing()
    {
        if ( double.IsNaN( this.crossing ) )
        {
            if ( this.Common.ChartTypeRegistry.GetChartType( ( string ) this.ChartArea.ChartTypes[0] ).ZeroCrossing )
            {
                if ( this.ViewMinimum > 0.0 )
                {
                    return this.ViewMinimum;
                }
                else
                {
                    return this.ViewMaximum < 0.0 ? this.ViewMaximum : 0.0;
                }
            }
            else
            {
                return this.ViewMinimum;
            }
        }
        else if ( this.crossing == double.MaxValue )
        {
            return this.ViewMaximum;
        }
        else if ( this.crossing == double.MinValue )
        {
            return this.ViewMinimum;
        }

        return this.crossing;
    }

    /// <summary>
    /// Set auto minimum number. The minimum number 
    /// which was sent to this function will be used to 
    /// estimate a rounded minimum.
    /// </summary>
    /// <param name="min"> This value is a recommendation for the minimum value. </param>
    internal void SetAutoMinimum( double min )
    {
        // Set the minimum
        if ( this.AutoMinimum )
        {
            this.minimum = min;
        }
    }

    /// <summary>
    /// Set auto maximum number. The maximum number 
    /// which was sent to this function will be used to 
    /// estimate a rounded maximum.
    /// </summary>
    /// <param name="max">This value is a recommendation for the maximum value.</param>
    internal void SetAutoMaximum( double max )
    {
        // Set the maximum
        if ( this.AutoMaximum )
        {
            this.maximum = max;
        }
    }

    /// <summary>
    /// Find opposite axis of this axis.  What is opposite 
    /// axis depend on first series in chart area and primary 
    /// and secondary X and Y axes for the first series.
    /// </summary>
    /// <returns>Opposite axis</returns>
    internal Axis GetOppositeAxis()
    {
        // Oppoiste axis found
        if ( this.oppositeAxis is not null )
        {
            return this.oppositeAxis;
        }

        List<string> list;

        switch ( this.axisType )
        {
            // X Axis
            case AxisName.X:
                list = this.ChartArea.GetXAxesSeries( AxisType.Primary, this.SubAxisName );
                // There aren't data series
                this.oppositeAxis = list.Count == 0
                    ? this.ChartArea.AxisY
                    : this.Common.DataManager.Series[list[0]].YAxisType == AxisType.Primary
                    ? this.ChartArea.AxisY.GetSubAxis( this.Common.DataManager.Series[list[0]].YSubAxisName )
                    : this.ChartArea.AxisY2.GetSubAxis( this.Common.DataManager.Series[list[0]].YSubAxisName );
                break;
            // X2 Axis
            case AxisName.X2:
                list = this.ChartArea.GetXAxesSeries( AxisType.Secondary, this.SubAxisName );
                // There aren't data series
                this.oppositeAxis = list.Count == 0
                    ? this.ChartArea.AxisY2
                    : this.Common.DataManager.Series[list[0]].YAxisType == AxisType.Primary
                    ? this.ChartArea.AxisY.GetSubAxis( this.Common.DataManager.Series[list[0]].YSubAxisName )
                    : this.ChartArea.AxisY2.GetSubAxis( this.Common.DataManager.Series[list[0]].YSubAxisName );
                break;
            // Y Axis
            case AxisName.Y:
                list = this.ChartArea.GetYAxesSeries( AxisType.Primary, this.SubAxisName );
                // There aren't data series
                this.oppositeAxis = list.Count == 0
                    ? this.ChartArea.AxisX
                    : this.Common.DataManager.Series[list[0]].XAxisType == AxisType.Primary
                    ? this.ChartArea.AxisX.GetSubAxis( this.Common.DataManager.Series[list[0]].XSubAxisName )
                    : this.ChartArea.AxisX2.GetSubAxis( this.Common.DataManager.Series[list[0]].XSubAxisName );
                break;
            // Y2 Axis
            case AxisName.Y2:
                list = this.ChartArea.GetYAxesSeries( AxisType.Secondary, this.SubAxisName );
                // There aren't data series
                this.oppositeAxis = list.Count == 0
                    ? this.ChartArea.AxisX2
                    : this.Common.DataManager.Series[list[0]].XAxisType == AxisType.Primary
                    ? this.ChartArea.AxisX.GetSubAxis( this.Common.DataManager.Series[list[0]].XSubAxisName )
                    : this.ChartArea.AxisX2.GetSubAxis( this.Common.DataManager.Series[list[0]].XSubAxisName );
                break;
            default:
                break;
        }
        return this.oppositeAxis;
    }

    /// <summary>
    /// This function converts Values from Axes to 
    /// linear relative positions.
    /// </summary>
    /// <param name="axisValue">Value from axis.</param>
    /// <returns>Relative position.</returns>
    internal double GetLinearPosition( double axisValue )
    {
        bool circularArea = this.ChartArea != null && this.ChartArea.chartAreaIsCurcular;

        // Check if some value calculation is optimized
        if ( !this.optimizedGetPosition )
        {
            this.paintViewMax = this.ViewMaximum;
            this.paintViewMin = this.ViewMinimum;
            this.paintRange = this.paintViewMax - this.paintViewMin;
            this.paintAreaPosition = this.PlotAreaPosition.ToRectangleF();

            // Update position for circular chart area
            if ( circularArea )
            {
                this.paintAreaPosition.Width /= 2.0f;
                this.paintAreaPosition.Height /= 2.0f;
            }

            this.paintAreaPositionBottom = this.paintAreaPosition.Y + this.paintAreaPosition.Height;
            this.paintAreaPositionRight = this.paintAreaPosition.X + this.paintAreaPosition.Width;

            // The Chart area size
            this.paintChartAreaSize = this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
                ? this.paintAreaPosition.Width
                : this.paintAreaPosition.Height;

            this.valueMultiplier = 0.0;
            if ( this.paintRange != 0 )
            {
                this.valueMultiplier = this.paintChartAreaSize / this.paintRange;
            }
        }

        // The Chart area pixel size
        double position = this.valueMultiplier * (axisValue - this.paintViewMin);



        // Check if axis scale segments are enabled
        if ( this.scaleSegmentsUsed )
        {
            AxisScaleSegment scaleSegment = this.ScaleSegments.FindScaleSegmentForAxisValue( axisValue );
            if ( scaleSegment is not null )
            {
                scaleSegment.GetScalePositionAndSize( this.paintChartAreaSize, out double segmentPosition, out double segmentSize );

                // Make sure value do not exceed max possible
                if ( !this.ScaleSegments.AllowOutOfScaleValues )
                {
                    if ( axisValue > scaleSegment.ScaleMaximum )
                    {
                        axisValue = scaleSegment.ScaleMaximum;
                    }
                    else if ( axisValue < scaleSegment.ScaleMinimum )
                    {
                        axisValue = scaleSegment.ScaleMinimum;
                    }
                }

                double segmentScaleRange = scaleSegment.ScaleMaximum - scaleSegment.ScaleMinimum;

                position = segmentSize / segmentScaleRange * (axisValue - scaleSegment.ScaleMinimum);
                position += segmentPosition;
            }
        }


        // Window position 
        // (Do Not use .Right or .Bottom methods below) - rounding issue!
        position = this.isReversed
            ? this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
                ? this.paintAreaPositionRight - position
                : this.paintAreaPosition.Y + position
            : this.AxisPosition is AxisPosition.Top or AxisPosition.Bottom
                ? this.paintAreaPosition.X + position
                : this.paintAreaPositionBottom - position;

        return position;
    }

    #endregion

    #region " axis estimate axis methods "

    /// <summary>
    /// This function recalculates minimum maximum and interval. 
    /// The function uses current values for minimum and maximum to 
    /// find rounding values. If the value from the data source for the 
    /// maximum value is 376.5 this function will return 380. This function 
    /// also set interval type for date
    /// </summary>
    internal void EstimateAxis()
    {
        double axisInterval;

        // Check if veiw size specified without scaleView position
        if ( !double.IsNaN( this.ScaleView.Size ) )
        {
            // If size set only use axis minimum for scaleView position
            if ( double.IsNaN( this.ScaleView.Position ) )
            {
                this.ScaleView.Position = this.Minimum;
            }
        }

        // Zooming Mode
        if ( !double.IsNaN( this._scaleView.Position ) && !double.IsNaN( this._scaleView.Size ) )
        {
            double viewMaximum = this.ViewMaximum;
            double viewMinimum = this.ViewMinimum;

            // IsLogarithmic axes
            if ( this._isLogarithmic )
            {
                viewMaximum = Math.Pow( this.logarithmBase, viewMaximum );
                viewMinimum = Math.Pow( this.logarithmBase, viewMinimum );
            }
            else
            {
                // Add rounding and gap for maximum and minimum
                this.EstimateAxis( ref this.minimum, ref this.maximum, this.AutoMaximum, this.AutoMinimum );
            }

            // Find Interval for Zoom
            axisInterval = this.EstimateAxis( ref viewMinimum, ref viewMaximum, true, true );
        }
        else // No Zooming mode
        {
            // Estimate axis shoud be always called for non logarithmic axis
            axisInterval = this.EstimateAxis( ref this.minimum, ref this.maximum, this.AutoMaximum, this.AutoMinimum );
        }

        // Set intervals for grids, tick marks and labels
        if ( axisInterval <= 0.0 )
        {
            throw new InvalidOperationException( SR.ExceptionAxisScaleAutoIntervalInvalid );
        }
        else
        {
            // This code checks if all series in the chart area have “integer type” 
            // for specified axes, which means int, uint, long and ulong and rounds interval.
#if SUBAXES
				if( ChartArea.SeriesIntegerType( this.axisType, ((Axis)this).SubAxisName ) )
#else // SUBAXES
            if ( this.ChartArea.SeriesIntegerType( this.axisType, string.Empty ) )
#endif // SUBAXES
            {
                axisInterval = Math.Round( axisInterval );
                if ( axisInterval == 0.0 )
                {
                    axisInterval = 1.0;
                }

                // Round Minimum to floor value if type is integer
                this.minimum = Math.Floor( this.minimum );
            }

            this.SetInterval = axisInterval;
        }
    }

    /// <summary>
    /// This function recalculates minimum maximum and interval. 
    /// The function uses current values for minimum and maximum to 
    /// find rounding values. If the value from the data source for the 
    /// maximum value is 376.5 this function will return 380. This function 
    /// also set interval type for date
    /// </summary>
    /// <param name="minimumValue">Minimum</param>
    /// <param name="maximumValue">Maximum</param>
    /// <param name="autoMaximum">Maximum value is Auto</param>
    /// <param name="autoMinimum">Minimum value is Auto</param>
    /// <returns>Interval</returns>
    internal double EstimateAxis( ref double minimumValue, ref double maximumValue, bool autoMaximum, bool autoMinimum )
    {
        double axisInterval;

        // The axis minimum value is greater than the maximum value.
        if ( maximumValue < minimumValue )
        {
            if ( !this.Common.ChartPicture.SuppressExceptions )
            {
                throw new InvalidOperationException( SR.ExceptionAxisScaleMinimumValueIsGreaterThenMaximumDataPoint );
            }
            else
            {
                // Max axis scale should be always bigger
                (minimumValue, maximumValue) = (maximumValue, minimumValue);
            }
        }

        // Take Value type
        ChartValueType dateType = this.GetDateTimeType();

        // Axis type is logarithmic
        axisInterval = this._isLogarithmic
            ? this.EstimateLogarithmicAxis( ref minimumValue, ref maximumValue, this.crossing, autoMaximum, autoMinimum )
            : dateType != ChartValueType.Auto
                ? this.EstimateDateAxis( ref minimumValue, ref maximumValue, autoMaximum, autoMinimum, dateType )
                : this.EstimateNumberAxis( ref minimumValue, ref maximumValue, this.IsStartedFromZero, this.prefferedNumberofIntervals, autoMaximum, autoMinimum );

        // Set intervals for grids, tick marks and labels
        if ( axisInterval <= 0.0 )
        {
            throw new InvalidOperationException( SR.ExceptionAxisScaleAutoIntervalInvalid );
        }
        else
        {
            // Set interval for Grid lines Tick Marks and labels
            this.SetInterval = axisInterval;
        }

        return axisInterval;

    }

    /// <summary>
    /// This function recalculates minimum maximum and interval for 
    /// logarithmic axis. The function uses current values for minimum and 
    /// maximum to find new rounding values.
    /// </summary>
    /// <param name="minimumValue">Current Minimum value</param>
    /// <param name="maximumValue">Current Maximum value</param>
    /// <param name="crossingValue">Crossing value</param>
    /// <param name="autoMaximum">Maximum value is Auto</param>
    /// <param name="autoMinimum">Minimum value is Auto</param>
    /// <returns>Interval</returns>
    private double EstimateLogarithmicAxis( ref double minimumValue, ref double maximumValue, double crossingValue, bool autoMaximum, bool autoMinimum )
    {
        double axisInterval;

        if ( !this.logarithmicConvertedToLinear )
        {
            // Remember values. Do not use POW function because of rounding.
            this.logarithmicMinimum = this.minimum;
            this.logarithmicMaximum = this.maximum;
        }

        // For log axis margin always turn on.
        this.margin = 100;

        // Supress zero and negative values with logarithmic axis exceptions
        if ( this.Common != null && this.Common.Chart != null && this.Common.Chart.chartPicture.SuppressExceptions )
        {
            if ( minimumValue <= 0.0 )
            {
                minimumValue = 1.0;
            }
            if ( maximumValue <= 0.0 )
            {
                maximumValue = 1.0;
            }
            if ( crossingValue is <= 0.0 and not double.MinValue )
            {
                crossingValue = 1.0;
            }
        }

        // The logarithmic axes can not show negative values.
        if ( minimumValue <= 0.0 || maximumValue <= 0.0 || crossingValue <= 0.0 )
        {
            if ( minimumValue <= 0.0 )
                throw new ArgumentOutOfRangeException( nameof( minimumValue ), SR.ExceptionAxisScaleLogarithmicNegativeValues );
            if ( maximumValue <= 0.0 )
                throw new ArgumentOutOfRangeException( nameof( maximumValue ), SR.ExceptionAxisScaleLogarithmicNegativeValues );
        }

        // Change crossing to linear scale
        crossingValue = Math.Log( crossingValue, this.logarithmBase );

        // Change minimum and maximum to linear scale
        minimumValue = Math.Log( minimumValue, this.logarithmBase );
        maximumValue = Math.Log( maximumValue, this.logarithmBase );

        this.logarithmicConvertedToLinear = true;

        // Find interval - Make approximately 5 grid lines and labels.
        double diff = (maximumValue - minimumValue) / 5;

        // Make good interval for logarithmic scale
        axisInterval = Math.Floor( diff );
        if ( axisInterval == 0 ) axisInterval = 1;

        if ( autoMinimum && autoMaximum )
        {
            // The maximum and minimum rounding with interval
            _ = this.RoundedValues( axisInterval, this.IsStartedFromZero, autoMaximum, autoMinimum, ref minimumValue, ref maximumValue );
        }

        // Do not allow min/max values more than a hundred
        if ( this.ChartArea.hundredPercent )
        {
            if ( autoMinimum )
            {
                if ( minimumValue < 0 )
                    minimumValue = 0;
            }

            if ( autoMaximum )
            {
                if ( maximumValue > 2 )
                    maximumValue = 2;
            }
        }

        // Set interval for Grid lines Tick Marks and labels
        return axisInterval;
    }

    /// <summary>
    /// This function recalculates minimum maximum and interval for 
    /// Date axis. The function uses current values for minimum and 
    /// maximum to find new rounding values.
    /// </summary>
    /// <param name="minimumValue">Current Minimum value</param>
    /// <param name="maximumValue">Current Maximum value</param>
    /// <param name="autoMaximum">Maximum value is Auto</param>
    /// <param name="autoMinimum">Minimum value is Auto</param>
    /// <param name="valuesType">AxisName of date-time values.</param>
    /// <returns>Interval</returns>
    private double EstimateDateAxis(
        ref double minimumValue,
        ref double maximumValue,
        bool autoMaximum,
        bool autoMinimum,
        ChartValueType valuesType )
    {
        double axisInterval;

        double min = minimumValue;
        double max = maximumValue;

        // Find interval for this date type
        axisInterval = this.CalcInterval( min, max, true, out this._internalIntervalType, valuesType );


        // For 3D Charts interval could be changed. After rotation 
        // projection of axis could be very small.
        if ( !double.IsNaN( this.interval3DCorrection ) &&
            this.ChartArea.Area3DStyle.Enable3D &&
            !this.ChartArea.chartAreaIsCurcular )
        {
            axisInterval = Math.Floor( axisInterval / this.interval3DCorrection );

            this.interval3DCorrection = double.NaN;
        }

        // Find number of units between minimum and maximum
        int numberOfUnits = this.GetNumOfUnits( min, max, this._internalIntervalType );

        // Make a gap between max point and axis for Y axes
        if ( this.axisType is AxisName.Y or AxisName.Y2 )
        {
            if ( autoMinimum && minimumValue > ChartHelper.GetIntervalSize( min, axisInterval, this._internalIntervalType ) )
            {
                // Add gap to the minimum value from the series
                // equal half of the interval
                minimumValue += ChartHelper.GetIntervalSize(
                    min,
                    -(axisInterval / 2.0) * this.margin / 100,
                    this._internalIntervalType,
                    null,
                    0.0,
                    DateTimeIntervalType.Number,
                    false,
                    false );

                // Align minimum sacale value on the interval
                minimumValue = ChartHelper.AlignIntervalStart(
                    minimumValue,
                    axisInterval * this.margin / 100,
                    this._internalIntervalType );
            }

            // Increase maximum if not zero. Make a space between chart type 
            // and the end of the chart area.
            if ( autoMaximum && max > 0 && this.margin != 0.0 )
            {
                maximumValue = minimumValue + ChartHelper.GetIntervalSize(
                    minimumValue,
                    ( double ) ((Math.Floor( numberOfUnits / axisInterval / this.margin * 100 ) + 2) * axisInterval * this.margin / 100),
                    this._internalIntervalType );
            }
        }

        this.InternalIntervalType = this._internalIntervalType;

        // Set interval for Grid lines Tick Marks and labels
        return axisInterval;
    }

    /// <summary>
    /// This function recalculates minimum maximum and interval for 
    /// number type axis. The function uses current values for minimum and 
    /// maximum to find new rounding values.
    /// </summary>
    /// <param name="minimumValue">Current Minimum value</param>
    /// <param name="maximumValue">Current Maximum value</param>
    /// <param name="shouldStartFromZero">Should start from zero flag.</param>
    /// <param name="preferredNumberOfIntervals">Preferred number of intervals. Can be set to zero for dynamic mode.</param>
    /// <param name="autoMaximum">Maximum value is Auto</param>
    /// <param name="autoMinimum">Minimum value is Auto</param>
    /// <returns>Interval</returns>
    internal double EstimateNumberAxis(
        ref double minimumValue,
        ref double maximumValue,
        bool shouldStartFromZero,
        int preferredNumberOfIntervals,
        bool autoMaximum,
        bool autoMinimum )
    {
        double axisInterval;
        double min = minimumValue;
        double max = maximumValue;
        double diff;

        if ( !this.roundedXValues && (this.axisType == AxisName.X || this.axisType == AxisName.X2) )
        {
            diff = this.ChartArea.GetPointsInterval( false, 10 );
            if ( diff == 0 || (max - min) / diff > 20 )
            {
                diff = (max - min) / preferredNumberOfIntervals;
            }

        }
        else
        {
            diff = (max - min) / preferredNumberOfIntervals;
        }

        // For 3D Charts interval could be changed. After rotation 
        // projection of axis could be very small.
        if ( !double.IsNaN( this.interval3DCorrection ) &&
            this.ChartArea.Area3DStyle.Enable3D &&
            !this.ChartArea.chartAreaIsCurcular )
        {
            diff /= this.interval3DCorrection;

            // Do not change minimum and maximum with 3D correction.
            if ( max - min < diff )
            {
                diff = max - min;
            }

            this.interval3DCorrection = double.NaN;

            if ( diff != 0.0 )
            {
                diff = this.CalcInterval( diff );
            }
        }


        if ( autoMaximum || autoMinimum )
        {
            if ( diff == 0 )
            {
                // Can not find interval. Minimum and maximum are same

                max = min + 1;
                diff = 0.2;
                axisInterval = 0.2;
            }
            else
            {
                axisInterval = this.CalcInterval( diff );
            }
        }
        else
        {
            axisInterval = diff;
        }

        // Case when minimum or maximum is set and interval is > maximum. 
        // Reasons overflow exception.
        if ( this.interval != 0 && this.interval > axisInterval && minimumValue + this.interval > maximumValue )
        {
            axisInterval = this.interval;
            if ( autoMaximum )
            {
                maximumValue = minimumValue + axisInterval;
            }

            if ( autoMinimum )
            {
                minimumValue = maximumValue - axisInterval;
            }
        }

        // The maximum and minimum rounding for Y Axes
        if ( this.axisType == AxisName.Y || this.axisType == AxisName.Y2 || (this.roundedXValues && (this.axisType == AxisName.X || this.axisType == AxisName.X2)) )
        {
            // Start from zero for the 100% chart types
            bool minIsZero = false;
            bool maxIsZero = false;
            if ( this.ChartArea.hundredPercent )
            {
                minIsZero = minimumValue == 0.0;
                maxIsZero = maximumValue == 0.0;
            }

            // Round min/max values
            _ = this.RoundedValues( axisInterval, shouldStartFromZero, autoMaximum, autoMinimum, ref minimumValue, ref maximumValue );

            // Do not allow min/max values more than a hundred
            if ( this.ChartArea.hundredPercent )
            {
                if ( autoMinimum )
                {
                    if ( minimumValue < -100 )
                        minimumValue = -100;
                    if ( minIsZero )
                        minimumValue = 0;
                }

                if ( autoMaximum )
                {
                    if ( maximumValue > 100 )
                        maximumValue = 100;
                    if ( maxIsZero )
                        maximumValue = 0;
                }
            }
        }

        // Set interval for Grid lines Tick Marks and labels
        return axisInterval;
    }

    #endregion
}

