//
//  Purpose:	Axis tick marks and grid lines a very similar chart 
//              elements and most of the functionality is located 
//              in the Grid class. TickMark class is derived from 
//              the Grid class and provides tick mark specific 
//              functionality.
//


using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " tick marks style enumeration "
/// <summary>
/// An enumeration of tick mark styles.
/// </summary>
public enum TickMarkStyle
{
    /// <summary>
    /// Tickmarks are disabled.
    /// </summary>
    None,
    /// <summary>
    /// Tickmarks are located outside of the chart area.
    /// </summary>
    OutsideArea,
    /// <summary>
    /// Tickmarks are located inside of the chart area.
    /// </summary>
    InsideArea,
    /// <summary>
    /// Tickmarks are set across the axis line.
    /// </summary>
    AcrossAxis
};

#endregion
/// <summary>
/// The TickMark class represents axis tick marks which are drawn next to 
/// the axis line. TickMark shares many common properties with the Grid 
/// class. This class also contains methods for tick marks drawing.
/// </summary>
[
    DefaultProperty( "Enabled" ),
    SRDescription( "DescriptionAttributeTickMark_TickMark" ),
]
public class TickMark : Grid
{
    #region " private fields and constructors "

#pragma warning disable IDE1006 // Naming Styles

    // Tick marks style
    private TickMarkStyle _style = TickMarkStyle.OutsideArea;

    // Tick marks size
    private float _size = 1;

#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Public default constructor
    /// </summary>
    public TickMark() : base( null, true )
    {
    }

    /// <summary>
    /// Public constructor
    /// </summary>
    /// <param name="axis">Axis which owns the grid or tick mark.</param>
    /// <param name="major">Major axis element.</param>
    internal TickMark( Axis axis, bool major ) : base( axis, major )
    {
    }

    #endregion

    #region " tick marks painting method "

    /// <summary>
    /// Draws and hit test for TickMarks.
    /// </summary>
    /// <param name="graph">Reference to the Chart Graphics object.</param>
    /// <param name="backElements">Back elements of the axis should be drawn in 3D scene.</param>
    internal void Paint( ChartGraphics graph, bool backElements )
    {
        PointF first = PointF.Empty; // The First point of a tick mark
        PointF second = PointF.Empty; // The Second point of a tick mark
        float axisPosition; // Axis position. 

        // Tick Marks are disabled
        if ( !this.enabled )
        {
            return;
        }

        // ****************************************************************
        // This code creates auto interval for auto tick marks and 
        // gridlines. If type is not date there are always four tickmarks 
        // or gridlines between major gridlines and tickmarks. For date 
        // type interval is calculated using CalcInterval function.
        // ****************************************************************
        double oldInterval = this.interval;
        DateTimeIntervalType oldIntervalType = this.intervalType;
        double oldIntervalOffset = this.intervalOffset;
        DateTimeIntervalType oldIntervalOffsetType = this.intervalOffsetType;
        if ( !this.majorGridTick && (this.interval == 0 || double.IsNaN( this.interval )) )
        {
            // Number type
            if ( this.Axis.majorGrid.GetIntervalType() == DateTimeIntervalType.Auto )
            {
                this.interval = this.Axis.majorGrid.GetInterval() / Grid.NumberOfIntervals;
            }
            // Date type
            else
            {
                DateTimeIntervalType localIntervalType = this.Axis.majorGrid.GetIntervalType();
                this.interval = this.Axis.CalcInterval(
                    this.Axis.ViewMinimum,
                    this.Axis.ViewMinimum + ((this.Axis.ViewMaximum - this.Axis.ViewMinimum) / Grid.NumberOfDateTimeIntervals),
                    true,
                    out localIntervalType,
                    ChartValueType.DateTime );
                this.intervalType = localIntervalType;
                this.intervalOffsetType = this.Axis.majorGrid.GetIntervalOffsetType();
                this.intervalOffset = this.Axis.majorGrid.GetIntervalOffset();
            }
        }

        if ( this._style == TickMarkStyle.None )
        {
            return;
        }

        // Check if custom tick marks should be drawn from custom labels
        if ( this.Axis.IsCustomTickMarks() )
        {
            this.PaintCustom( graph, backElements );
            return;
        }

        // Get first series attached to this axis
        Series axisSeries = null;
        if ( this.Axis.axisType is AxisName.X or AxisName.X2 )
        {
            List<string> seriesArray = this.Axis.ChartArea.GetXAxesSeries( (this.Axis.axisType == AxisName.X) ? AxisType.Primary : AxisType.Secondary, this.Axis.SubAxisName );
            if ( seriesArray.Count > 0 )
            {
                axisSeries = this.Axis.Common.DataManager.Series[seriesArray[0]];
                if ( axisSeries != null && !axisSeries.IsXValueIndexed )
                {
                    axisSeries = null;
                }
            }
        }

        // Current position for tick mark is minimum
        double current = this.Axis.ViewMinimum;

        // Get offse type
        DateTimeIntervalType offsetType = (this.GetIntervalOffsetType() == DateTimeIntervalType.Auto) ? this.GetIntervalType() : this.GetIntervalOffsetType();


        // ***********************************
        // Check if the AJAX zooming and scrolling mode is enabled.
        // ***********************************

        // Adjust start position depending on the interval type
        if ( !this.Axis.ChartArea.chartAreaIsCurcular ||
            this.Axis.axisType == AxisName.Y ||
            this.Axis.axisType == AxisName.Y2 )
        {
            current = ChartHelper.AlignIntervalStart( current, this.GetInterval(), this.GetIntervalType(), axisSeries, this.majorGridTick );
        }

        // The Current position is start position, not minimum
        if ( this.GetIntervalOffset() != 0 && !double.IsNaN( this.GetIntervalOffset() ) && axisSeries == null )
        {
            current += ChartHelper.GetIntervalSize( current, this.GetIntervalOffset(),
                offsetType, axisSeries, 0, DateTimeIntervalType.Number, true, false );
        }

        // Too many tick marks
        if ( (this.Axis.ViewMaximum - this.Axis.ViewMinimum) / ChartHelper.GetIntervalSize( current, this.GetInterval(), this.GetIntervalType(), axisSeries, 0, DateTimeIntervalType.Number, true ) > ChartHelper.MaxNumOfGridlines )
        {
            return;
        }

        // If Maximum, minimum and interval don’t have 
        // proper value do not draw tick marks.
        if ( this.Axis.ViewMaximum <= this.Axis.ViewMinimum )
        {
            return;
        }

        // Axis scroll bar will increase size of the Outside and Cross style tick marks
        float scrollBarSize = 0;

        if ( this.Axis.ScrollBar.IsVisible &&
            this.Axis.ScrollBar.IsPositionedInside &&
            (this.Axis.IsAxisOnAreaEdge || !this.Axis.IsMarksNextToAxis) )
        {
            scrollBarSize = ( float ) this.Axis.ScrollBar.GetScrollBarRelativeSize();
        }

        // Left tickmarks
        if ( this.Axis.AxisPosition == AxisPosition.Left )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.X;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.X = axisPosition;
                second.X = axisPosition + this._size;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.X = axisPosition - this._size - scrollBarSize;
                second.X = axisPosition;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.X = axisPosition - (this._size / 2) - scrollBarSize;
                second.X = axisPosition + (this._size / 2);
            }
        }

        // Right tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Right )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Right;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.X = axisPosition - this._size;
                second.X = axisPosition;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.X = axisPosition;
                second.X = axisPosition + this._size + scrollBarSize;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.X = axisPosition - (this._size / 2);
                second.X = axisPosition + (this._size / 2) + scrollBarSize;
            }
        }

        // Top tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Top )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Y;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.Y = axisPosition;
                second.Y = axisPosition + this._size;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.Y = axisPosition - this._size - scrollBarSize;
                second.Y = axisPosition;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.Y = axisPosition - (this._size / 2) - scrollBarSize;
                second.Y = axisPosition + (this._size / 2);
            }
        }

        // Bottom tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Bottom )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Bottom;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.Y = axisPosition - this._size;
                second.Y = axisPosition;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.Y = axisPosition;
                second.Y = axisPosition + this._size + scrollBarSize;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.Y = axisPosition - (this._size / 2);
                second.Y = axisPosition + (this._size / 2) + scrollBarSize;
            }
        }


        // Loop for drawing grid tick marks
        int counter = 0;
        int logStep = 1;
        double oldCurrent = current;
        double interval = 0;
        while ( current <= this.Axis.ViewMaximum )
        {
            double logInterval = 0;

            // Take an interval between gridlines. Interval 
            // depends on interval type.
            if ( this.majorGridTick || !this.Axis.IsLogarithmic )
            {
                // Take an interval between tickmarks. Interval 
                // depends on interval type.
                interval = ChartHelper.GetIntervalSize( current, this.GetInterval(), this.GetIntervalType(), axisSeries, this.GetIntervalOffset(), offsetType, true );

            }
            // Code for linear minor gridlines and tickmarks 
            // if scale is logarithmic.
            else
            {
                // This code is used only for logarithmic scale and minor tick marks or 
                // gridlines which have linear minor scale in logarithmic major scale. 
                // This code is used to find minimum value for the interval. For example 
                // if logarithmic base is 2 and interval is between 4 and 8; current value 
                // is 5.6; this method will return linearised value for 4. This code works 
                // like Math.Floor for logarithmic scale.
                double logMinimum = this.GetLogMinimum( current, axisSeries );

                if ( oldCurrent != logMinimum )
                {
                    oldCurrent = logMinimum;
                    logStep = 1;
                }

                // Find interval for logarithmic linearised scale
                logInterval = Math.Log( 1 + (this.interval * logStep), this.Axis.logarithmBase );

                current = oldCurrent;

                interval = logInterval;

                logStep++;

                // Reset current position if major interval is passed.
                if ( this.GetLogMinimum( current + logInterval, axisSeries ) != logMinimum )
                {
                    current += logInterval;
                    continue;
                }
            }

            // For indexed series do not draw the last tickmark
            if ( current == this.Axis.ViewMaximum && axisSeries is not null )
            {
                current += interval;

                continue;
            }

            // Check interval size
            if ( interval == 0 )
            {
                throw new InvalidOperationException( SR.ExceptionTickMarksIntervalIsZero );
            }

            // Check if we do not exceed max number of elements
            if ( counter++ > ChartHelper.MaxNumOfGridlines )
            {
                break;
            }

            // Do not draw the very first tick mark for circular chart area
            if ( this.Axis != null && this.Axis.ChartArea is not null )
            {
                if ( this.Axis.ChartArea.chartAreaIsCurcular &&
                    ((!this.Axis.IsReversed && current == this.Axis.ViewMinimum) ||
                    (this.Axis.IsReversed && current == this.Axis.ViewMaximum)) )
                {
                    current += interval;

                    continue;
                }
            }

            if ( !this.majorGridTick && this.Axis.IsLogarithmic )
            {
                current += logInterval;

                if ( current > this.Axis.ViewMaximum )
                {
                    break;
                }
            }

            if ( ( decimal ) current >= ( decimal ) this.Axis.ViewMinimum )
            {
                // Left tickmarks
                if ( this.Axis.AxisPosition == AxisPosition.Left )
                {
                    first.Y = ( float ) this.Axis.GetLinearPosition( current );
                    second.Y = first.Y;
                }

                // Right tickmarks
                else if ( this.Axis.AxisPosition == AxisPosition.Right )
                {
                    first.Y = ( float ) this.Axis.GetLinearPosition( current );
                    second.Y = first.Y;
                }

                // Top tickmarks
                else if ( this.Axis.AxisPosition == AxisPosition.Top )
                {
                    first.X = ( float ) this.Axis.GetLinearPosition( current );
                    second.X = first.X;
                }

                // Bottom tickmarks
                else if ( this.Axis.AxisPosition == AxisPosition.Bottom )
                {
                    first.X = ( float ) this.Axis.GetLinearPosition( current );
                    second.X = first.X;
                }

                if ( this.Axis.Common.ProcessModeRegions )
                {
                    if ( this.Axis.ChartArea.chartAreaIsCurcular )
                    {
                        RectangleF rect = new( first.X - 0.5f, first.Y - 0.5f, Math.Abs( second.X - first.X ) + 1, Math.Abs( second.Y - first.Y ) + 1 );
                        using GraphicsPath path = new();
                        path.AddRectangle( graph.GetAbsoluteRectangle( rect ) );
                        path.Transform( graph.Transform );
                        this.Axis.Common.HotRegionsList.AddHotRegion(
                            path,
                            false,
                            ChartElementType.TickMarks,
                            this );
                    }
                    else if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                    {
                        RectangleF rect = new( first.X - 0.5f, first.Y - 0.5f, Math.Abs( second.X - first.X ) + 1, Math.Abs( second.Y - first.Y ) + 1 );

                        this.Axis.Common.HotRegionsList.AddHotRegion( rect, this, ChartElementType.TickMarks, true );
                    }
                    else
                    {
                        if ( !this.Axis.Common.ProcessModePaint ) //if ProcessModePaint is true it will be called later
                            this.Draw3DTickLine( graph, first, second, backElements );
                    }

                }

                if ( this.Axis.Common.ProcessModePaint )
                {
                    // Draw grid line
                    if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                    {
                        graph.DrawLineRel( this.borderColor, this.borderWidth, this.borderDashStyle, first, second );
                    }
                    else
                    {
                        this.Draw3DTickLine( graph, first, second, backElements );
                    }
                }
            }

            // Move position
            if ( this.majorGridTick || !this.Axis.IsLogarithmic )
            {
                current += interval;
            }
        }

        // Used for auto interval for auto tick marks and 
        // gridlines
        if ( !this.majorGridTick )
        {
            this.interval = oldInterval;
            this.intervalType = oldIntervalType;
            this.intervalOffset = oldIntervalOffset;
            this.intervalOffsetType = oldIntervalOffsetType;
        }
    }

    /// <summary>
    /// This method returns linearized logarithmic value 
    /// which is minimum for range with interval 1.
    /// </summary>
    /// <param name="current">Current value</param>
    /// <param name="axisSeries">First series attached to axis.</param>
    /// <returns>Returns Minimum for the range which contains current value</returns>
    private double GetLogMinimum( double current, Series axisSeries )
    {
        double viewMinimum = this.Axis.ViewMinimum;
        DateTimeIntervalType offsetType = (this.GetIntervalOffsetType() == DateTimeIntervalType.Auto) ? this.GetIntervalType() : this.GetIntervalOffsetType();
        if ( this.GetIntervalOffset() != 0 && axisSeries == null )
        {
            viewMinimum += ChartHelper.GetIntervalSize( viewMinimum, this.GetIntervalOffset(),
                offsetType, axisSeries, 0, DateTimeIntervalType.Number, true, false );
        }

        return viewMinimum + Math.Floor( current - viewMinimum );
    }

    /// <summary>
    /// Draws and hit test for custom TickMarks from the custom labels collection.
    /// </summary>
    /// <param name="graph">Reference to the Chart Graphics object.</param>
    /// <param name="backElements">Back elements of the axis should be drawn in 3D scene.</param>
    internal void PaintCustom( ChartGraphics graph, bool backElements )
    {
        PointF first = PointF.Empty;    // The First point of a tick mark
        PointF second = PointF.Empty;   // The Second point of a tick mark
        float axisPosition;             // Axis position. 

        // Axis scroll bar will increase size of the Outside and Cross style tick marks
        float scrollBarSize = 0;

        if ( this.Axis.ScrollBar.IsVisible && this.Axis.ScrollBar.IsPositionedInside && this.Axis.IsAxisOnAreaEdge )
        {
            scrollBarSize = ( float ) this.Axis.ScrollBar.GetScrollBarRelativeSize();
        }

        // Left tickmarks
        if ( this.Axis.AxisPosition == AxisPosition.Left )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.X;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.X = axisPosition;
                second.X = axisPosition + this._size;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.X = axisPosition - this._size - scrollBarSize;
                second.X = axisPosition;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.X = axisPosition - (this._size / 2) - scrollBarSize;
                second.X = axisPosition + (this._size / 2);
            }
        }

        // Right tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Right )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Right;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.X = axisPosition - this._size;
                second.X = axisPosition;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.X = axisPosition;
                second.X = axisPosition + this._size + scrollBarSize;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.X = axisPosition - (this._size / 2);
                second.X = axisPosition + (this._size / 2) + scrollBarSize;
            }
        }

        // Top tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Top )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Y;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.Y = axisPosition;
                second.Y = axisPosition + this._size;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.Y = axisPosition - this._size - scrollBarSize;
                second.Y = axisPosition;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.Y = axisPosition - (this._size / 2) - scrollBarSize;
                second.Y = axisPosition + (this._size / 2);
            }
        }

        // Bottom tickmarks
        else if ( this.Axis.AxisPosition == AxisPosition.Bottom )
        {
            // The tick marks will follow axis or they will 
            // be always on the border of the chart area.
            axisPosition = this.Axis.GetIsMarksNextToAxis() ? ( float ) this.Axis.GetAxisPosition() : this.Axis.PlotAreaPosition.Bottom;

            if ( this._style == TickMarkStyle.InsideArea )
            {
                first.Y = axisPosition - this._size;
                second.Y = axisPosition;
            }
            else if ( this._style == TickMarkStyle.OutsideArea )
            {
                first.Y = axisPosition;
                second.Y = axisPosition + this._size + scrollBarSize;
            }
            else if ( this._style == TickMarkStyle.AcrossAxis )
            {
                first.Y = axisPosition - (this._size / 2);
                second.Y = axisPosition + (this._size / 2) + scrollBarSize;
            }
        }

        // Loop through all custom labels
        foreach ( CustomLabel label in this.Axis.CustomLabels )
        {
            if ( (label.GridTicks & GridTickTypes.TickMark) == GridTickTypes.TickMark )
            {
                double position = (label.ToPosition + label.FromPosition) / 2.0;
                if ( position >= this.Axis.ViewMinimum && position <= this.Axis.ViewMaximum )
                {
                    // Left tickmarks
                    if ( this.Axis.AxisPosition == AxisPosition.Left )
                    {
                        first.Y = ( float ) this.Axis.GetLinearPosition( position );
                        second.Y = first.Y;
                    }

                    // Right tickmarks
                    else if ( this.Axis.AxisPosition == AxisPosition.Right )
                    {
                        first.Y = ( float ) this.Axis.GetLinearPosition( position );
                        second.Y = first.Y;
                    }

                    // Top tickmarks
                    else if ( this.Axis.AxisPosition == AxisPosition.Top )
                    {
                        first.X = ( float ) this.Axis.GetLinearPosition( position );
                        second.X = first.X;
                    }

                    // Bottom tickmarks
                    else if ( this.Axis.AxisPosition == AxisPosition.Bottom )
                    {
                        first.X = ( float ) this.Axis.GetLinearPosition( position );
                        second.X = first.X;
                    }

                    if ( this.Axis.Common.ProcessModeRegions )
                    {
                        if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                        {
                            RectangleF rect = new( first.X - 0.5f, first.Y - 0.5f, Math.Abs( second.X - first.X ) + 1, Math.Abs( second.Y - first.Y ) + 1 );

                            this.Axis.Common.HotRegionsList.AddHotRegion( rect, this, ChartElementType.TickMarks, true );
                        }
                        else
                        {
                            this.Draw3DTickLine( graph, first, second, backElements );
                        }
                    }

                    if ( this.Axis.Common.ProcessModePaint )
                    {
                        // Draw grid line
                        if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                        {
                            graph.DrawLineRel( this.borderColor, this.borderWidth, this.borderDashStyle, first, second );
                        }
                        else
                        {
                            this.Draw3DTickLine( graph, first, second, backElements );
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Draws tick mark line in 3D space.
    /// </summary>
    /// <param name="graph">Reference to the Chart Graphics object.</param>
    /// <param name="point1">First line point.</param>
    /// <param name="point2">Second line point.</param>
    /// <param name="backElements">Back elements of the axis should be drawn in 3D scene.</param>
    internal void Draw3DTickLine(
        ChartGraphics graph,
        PointF point1,
        PointF point2,
        bool backElements
        )
    {
        ChartArea area = this.Axis.ChartArea;

        // ============================*****
        // Set the tick marks line depth
        // ============================*****
        float wallZPosition = this.Axis.GetMarksZPosition( out bool axisOnEdge );

        // ============================*****
        // Check if tick mark should be drawn as back or front element
        // ============================*****

        // Check if axis tick marks are drawn inside plotting area
        bool tickMarksOnEdge = axisOnEdge;
        if ( (tickMarksOnEdge &&
            this.Axis.MajorTickMark.TickMarkStyle == TickMarkStyle.AcrossAxis) ||
            this.Axis.MajorTickMark.TickMarkStyle == TickMarkStyle.InsideArea ||
            this.Axis.MinorTickMark.TickMarkStyle == TickMarkStyle.AcrossAxis ||
            this.Axis.MinorTickMark.TickMarkStyle == TickMarkStyle.InsideArea )
        {
            tickMarksOnEdge = false;
        }

        SurfaceNames surfaceName = (wallZPosition == 0f) ? SurfaceNames.Back : SurfaceNames.Front;
        if ( !area.ShouldDrawOnSurface( surfaceName, backElements, tickMarksOnEdge ) )
        {
            // Skip drawing
            return;
        }

        // ============================*****
        // Add area scene wall width to the length of the tick mark
        // ============================*****
        if ( this.Axis.AxisPosition == AxisPosition.Bottom &&
            (!this.Axis.GetIsMarksNextToAxis() || axisOnEdge) &&
            area.IsBottomSceneWallVisible() )
        {
            point2.Y += area.areaSceneWallWidth.Height;
        }
        else if ( this.Axis.AxisPosition == AxisPosition.Left &&
            (!this.Axis.GetIsMarksNextToAxis() || axisOnEdge) &&
            area.IsSideSceneWallOnLeft() )
        {
            point1.X -= area.areaSceneWallWidth.Width;
        }
        else if ( this.Axis.AxisPosition == AxisPosition.Right &&
            (!this.Axis.GetIsMarksNextToAxis() || axisOnEdge) &&
            !area.IsSideSceneWallOnLeft() )
        {
            point2.X += area.areaSceneWallWidth.Width;
        }
        else if ( this.Axis.AxisPosition == AxisPosition.Top &&
            (!this.Axis.GetIsMarksNextToAxis() || axisOnEdge) )
        {
            point1.Y -= area.areaSceneWallWidth.Height;
        }

        // ============================*****
        // Adjust grid line direction for the Top axis
        // ============================*****
        Point3D point3 = null, point4 = null;
        if ( axisOnEdge && area.areaSceneWallWidth.Width != 0f )
        {
            if ( this.Axis.AxisPosition == AxisPosition.Top )
            {
                // Always use plot area position to draw tick mark
                float axisPosition = this.Axis.PlotAreaPosition.Y;

                if ( this._style == TickMarkStyle.InsideArea )
                {
                    point1.Y = axisPosition;
                    point2.Y = axisPosition + this._size;

                    point3 = new Point3D( point1.X, point1.Y, -area.areaSceneWallWidth.Width );
                    point4 = new Point3D( point1.X, point1.Y, 0f );
                }
                else if ( this._style == TickMarkStyle.OutsideArea )
                {
                    point1.Y = axisPosition;
                    point2.Y = axisPosition;

                    point3 = new Point3D( point1.X, axisPosition, wallZPosition );
                    point4 = new Point3D( point1.X, point1.Y, -this._size - area.areaSceneWallWidth.Width );
                }
                else if ( this._style == TickMarkStyle.AcrossAxis )
                {
                    point1.Y = axisPosition;
                    point2.Y = axisPosition + (this._size / 2);

                    point3 = new Point3D( point1.X, axisPosition, wallZPosition );
                    point4 = new Point3D( point1.X, point1.Y, (-this._size / 2) - area.areaSceneWallWidth.Width );
                }

                // Do not show "bent" tick marks on the top surface
                if ( area.ShouldDrawOnSurface( SurfaceNames.Top, backElements, false ) )
                {
                    point3 = null;
                    point4 = null;
                }
            }

            // ============================*****
            // Adjust grid line direction for the Left axis
            // ============================*****
            if ( this.Axis.AxisPosition == AxisPosition.Left && !area.IsSideSceneWallOnLeft() )
            {
                // Always use plot area position to draw tick mark
                float axisPosition = this.Axis.PlotAreaPosition.X;

                if ( this._style == TickMarkStyle.InsideArea )
                {
                    point1.X = axisPosition;
                    point2.X = axisPosition + this._size;

                    point3 = new Point3D( point1.X, point1.Y, -area.areaSceneWallWidth.Width );
                    point4 = new Point3D( point1.X, point1.Y, 0f );
                }
                else if ( this._style == TickMarkStyle.OutsideArea )
                {
                    point1.X = axisPosition;
                    point2.X = axisPosition;

                    point3 = new Point3D( axisPosition, point1.Y, wallZPosition );
                    point4 = new Point3D( axisPosition, point1.Y, -this._size - area.areaSceneWallWidth.Width );
                }
                else if ( this._style == TickMarkStyle.AcrossAxis )
                {
                    point1.X = axisPosition;
                    point2.X = axisPosition + (this._size / 2);

                    point3 = new Point3D( axisPosition, point1.Y, wallZPosition );
                    point4 = new Point3D( axisPosition, point1.Y, (-this._size / 2) - area.areaSceneWallWidth.Width );
                }

                // Do not show "bent" tick marks on the left surface
                if ( area.ShouldDrawOnSurface( SurfaceNames.Left, backElements, false ) )
                {
                    point3 = null;
                    point4 = null;
                }
            }

            // ============================*****
            // Adjust grid line direction for the Right axis
            // ============================*****
            else if ( this.Axis.AxisPosition == AxisPosition.Right && area.IsSideSceneWallOnLeft() )
            {
                // Always use plot area position to draw tick mark
                float axisPosition = this.Axis.PlotAreaPosition.Right;

                if ( this._style == TickMarkStyle.InsideArea )
                {
                    point1.X = axisPosition - this._size;
                    point2.X = axisPosition;

                    point3 = new Point3D( point2.X, point2.Y, -area.areaSceneWallWidth.Width );
                    point4 = new Point3D( point2.X, point2.Y, 0f );
                }
                else if ( this._style == TickMarkStyle.OutsideArea )
                {
                    point1.X = axisPosition;
                    point2.X = axisPosition;

                    point3 = new Point3D( axisPosition, point1.Y, wallZPosition );
                    point4 = new Point3D( axisPosition, point1.Y, -this._size - area.areaSceneWallWidth.Width );

                }
                else if ( this._style == TickMarkStyle.AcrossAxis )
                {
                    point1.X = axisPosition - (this._size / 2);
                    point2.X = axisPosition;

                    point3 = new Point3D( axisPosition, point1.Y, wallZPosition );
                    point4 = new Point3D( axisPosition, point1.Y, (-this._size / 2) - area.areaSceneWallWidth.Width );
                }

                // Do not show "bent" tick marks on the right surface
                if ( area.ShouldDrawOnSurface( SurfaceNames.Right, backElements, false ) )
                {
                    point3 = null;
                    point4 = null;
                }
            }
        }

        // ============================*****
        // Draw tick mark (first line)
        // ============================*****
        graph.Draw3DLine(
            area.matrix3D,
            this.borderColor, this.borderWidth, this.borderDashStyle,
            new Point3D( point1.X, point1.Y, wallZPosition ),
            new Point3D( point2.X, point2.Y, wallZPosition ),
            this.Axis.Common,
            this,
            ChartElementType.TickMarks
            );


        // ============================*****
        // Draw tick mark (second line)
        // ============================*****
        if ( point3 != null && point4 is not null )
        {
            graph.Draw3DLine(
                area.matrix3D,
                this.borderColor, this.borderWidth, this.borderDashStyle,
                point3,
                point4,
                this.Axis.Common,
                this,
                ChartElementType.TickMarks
                );
        }
    }

    #endregion

    #region " tickmark properties "

    /// <summary>
    /// Tick mark style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( TickMarkStyle.OutsideArea ),
    SRDescription( "DescriptionAttributeTickMark_Style" )
    ]
    public TickMarkStyle TickMarkStyle
    {
        get => this._style;
        set
        {
            this._style = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Tick mark size.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 1.0F ),
    SRDescription( "DescriptionAttributeTickMark_Size" )
    ]
    public float Size
    {
        get => this._size;
        set
        {
            this._size = value;
            this.Invalidate();
        }
    }

    #endregion
}
/// <summary>
/// The Grid class represents axis grid lines which are drawn in the 
/// plotting area. It contains grid interval and visual appearance 
/// properties. This class also contains methods for grid lines drawing. 
/// </summary>
[
    DefaultProperty( "Enabled" ),
    SRDescription( "DescriptionAttributeGrid_Grid" )
]
public class Grid
{
    #region " grid fields and constructors "

    // Reference to the Axis object

#pragma warning disable IDE1006 // Naming Styles

    // Flags indicate that interval properties where changed
    internal bool intervalOffsetChanged;
    internal bool intervalChanged;
    internal bool intervalTypeChanged;
    internal bool intervalOffsetTypeChanged;

    internal bool enabledChanged;

    // Data members, which store properties values
    internal double intervalOffset;
    internal double interval;
    internal DateTimeIntervalType intervalType = DateTimeIntervalType.Auto;
    internal DateTimeIntervalType intervalOffsetType = DateTimeIntervalType.Auto;
    internal Color borderColor = Color.Black;
    internal int borderWidth = 1;
    internal ChartDashStyle borderDashStyle = ChartDashStyle.Solid;
    internal bool enabled = true;

    // Indicates that object describes Major Tick Mark or Grid Line
    internal bool majorGridTick;

    // Common number of intervals on the numeric and date-time axis
    internal const double NumberOfIntervals = 5.0;
    internal const double NumberOfDateTimeIntervals = 4.0;

#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Public default constructor.
    /// </summary>
    public Grid()
    {
    }

    /// <summary>
    /// Public constructor.
    /// </summary>
    /// <param name="axis">Axis which owns the grid.</param>
    /// <param name="major">Major axis element.</param>
    internal Grid( Axis axis, bool major ) => this.Initialize( axis, major );

    /// <summary>
    /// Initializes the object.
    /// </summary>
    /// <param name="axis">Axis which owns the grid.</param>
    /// <param name="major">Major axis element.</param>
    internal void Initialize( Axis axis, bool major )
    {
        // Minor elements are disabled by default
        if ( !this.enabledChanged &&
            this.Axis == null &&
            !major )
        {
            this.enabled = false;
        }

        // If object was first created and populated with data and then added into the axis 
        // we need to remember changed values.
        // NOTE: Fixes issue #6237
        if ( this.Axis == null )
        {
            TickMark tickMark = this as TickMark;

            if ( this.interval != 0 )
            {
                if ( tickMark is not null )
                {
                    if ( major )
                    {
                        axis.tempMajorTickMarkInterval = this.interval;
                    }
                    else
                    {
                        axis.tempMinorTickMarkInterval = this.interval;
                    }
                }
                else
                {
                    if ( major )
                    {
                        axis.tempMajorGridInterval = this.interval;
                    }
                    else
                    {
                        axis.tempMinorGridInterval = this.interval;
                    }
                }
            }
            if ( this.intervalType != DateTimeIntervalType.Auto )
            {
                if ( tickMark is not null )
                {
                    if ( major )
                    {
                        axis.tempTickMarkIntervalType = this.intervalType;
                    }
                }
                else
                {
                    if ( major )
                    {
                        axis.tempGridIntervalType = this.intervalType;
                    }
                }
            }
        }

        // Set axis object reference
        this.Axis = axis;

        // Set a flag if this object represent minor or major tick
        this.majorGridTick = major;

        //		internal double							interval = 0;
        //		internal DateTimeIntervalType			intervalType = DateTimeIntervalType.Auto;

    }

    #endregion

    #region " grid helper functions "
    /// <summary>
    /// Gets axes to which this object attached to
    /// </summary>
    /// <returns>Axis object.</returns>
    internal Axis GetAxis()
    {
        return this.Axis;
    }
    /// <summary>
    /// Invalidate chart area the axis belong to.
    /// </summary>
    internal void Invalidate()
    {
        this.Axis?.Invalidate();
    }

    #endregion

    #region " grid lines drawing functions "

    /// <summary>
    /// Draws grid lines.
    /// </summary>
    /// <param name="graph">Reference to the Chart Graphics object.</param>
    internal void Paint( ChartGraphics graph )
    {
        // Grids are disabled
        if ( !this.enabled )
        {
            return;
        }

        // Check if custom grid lines should be drawn from custom labels
        if ( this.Axis.IsCustomGridLines() )
        {
            this.PaintCustom( graph );
            return;
        }

        double gridInterval; // Grid interval
        double current; // Current position

        // Get first series attached to this axis
        Series axisSeries = null;
        if ( this.Axis.axisType is AxisName.X or AxisName.X2 )
        {
            List<string> seriesArray = this.Axis.ChartArea.GetXAxesSeries( (this.Axis.axisType == AxisName.X) ? AxisType.Primary : AxisType.Secondary, this.Axis.SubAxisName );
            if ( seriesArray.Count > 0 )
            {
                axisSeries = this.Axis.Common.DataManager.Series[seriesArray[0]];
                if ( axisSeries != null && !axisSeries.IsXValueIndexed )
                {
                    axisSeries = null;
                }
            }
        }

        // ****************************************************************
        // This code creates auto interval for auto tick marks and 
        // gridlines. If type is not date there are always four tickmarks 
        // or gridlines between major gridlines and tickmarks. For date 
        // type interval is calculated using CalcInterval function.
        // ****************************************************************
        double oldInterval = this.interval;
        DateTimeIntervalType oldIntervalType = this.intervalType;
        double oldIntervalOffset = this.intervalOffset;
        DateTimeIntervalType oldIntervalOffsetType = this.intervalOffsetType;
        if ( !this.majorGridTick && (this.interval == 0 || double.IsNaN( this.interval )) )
        {
            // Number type
            if ( this.Axis.majorGrid.GetIntervalType() == DateTimeIntervalType.Auto )
            {
                this.interval = this.Axis.majorGrid.GetInterval() / Grid.NumberOfIntervals;
            }
            // Date type
            else
            {
                DateTimeIntervalType localIntervalType = this.Axis.majorGrid.GetIntervalType();
                this.interval = this.Axis.CalcInterval(
                    this.Axis.minimum,
                    this.Axis.minimum + ((this.Axis.maximum - this.Axis.minimum) / Grid.NumberOfDateTimeIntervals),
                    true,
                    out localIntervalType,
                    ChartValueType.DateTime );
                this.intervalType = localIntervalType;
                this.intervalOffsetType = this.Axis.majorGrid.GetIntervalOffsetType();
                this.intervalOffset = this.Axis.majorGrid.GetIntervalOffset();
            }
        }

        // Current position for grid lines is minimum
        current = this.Axis.ViewMinimum;


        // ***********************************
        // Check if the AJAX zooming and scrolling mode is enabled.
        // ***********************************

        // Adjust start position depending on the interval type
        if ( !this.Axis.ChartArea.chartAreaIsCurcular ||
            this.Axis.axisType == AxisName.Y ||
            this.Axis.axisType == AxisName.Y2 )
        {
            current = ChartHelper.AlignIntervalStart( current, this.GetInterval(), this.GetIntervalType(), axisSeries, this.majorGridTick );
        }

        // The Current position is start position, not minimum
        DateTimeIntervalType offsetType = (this.GetIntervalOffsetType() == DateTimeIntervalType.Auto) ? this.GetIntervalType() : this.GetIntervalOffsetType();
        if ( this.GetIntervalOffset() != 0 && !double.IsNaN( this.GetIntervalOffset() ) && axisSeries == null )
        {
            current += ChartHelper.GetIntervalSize( current, this.GetIntervalOffset(), offsetType, axisSeries, 0, DateTimeIntervalType.Number, true, false );
        }

        // Too many gridlines
        if ( (this.Axis.ViewMaximum - this.Axis.ViewMinimum) / ChartHelper.GetIntervalSize( current, this.GetInterval(), this.GetIntervalType(), axisSeries, 0, DateTimeIntervalType.Number, true ) > ChartHelper.MaxNumOfGridlines )
            return;

        // If Maximum, minimum and interval don’t have 
        // proper value do not draw grid lines.
        if ( this.Axis.ViewMaximum <= this.Axis.ViewMinimum )
            return;

        if ( this.GetInterval() <= 0 )
            return;

        // Loop for drawing grid lines
        int counter = 0;
        int logStep = 1;
        double oldCurrent = current;
        decimal viewMaximum = ( decimal ) this.Axis.ViewMaximum;
        while ( ( decimal ) current <= viewMaximum )
        {
            // Take an interval between gridlines. Interval 
            // depends on interval type.
            if ( this.majorGridTick || !this.Axis.IsLogarithmic )
            {
                double autoInterval = this.GetInterval();

                gridInterval = ChartHelper.GetIntervalSize( current, autoInterval, this.GetIntervalType(), axisSeries, this.GetIntervalOffset(), offsetType, true );

                // Check interval size
                if ( gridInterval == 0 )
                {
                    throw new InvalidOperationException( SR.ExceptionTickMarksIntervalIsZero );
                }

                // Draw between min & max values only
                if ( ( decimal ) current >= ( decimal ) this.Axis.ViewMinimum )
                {
                    this.DrawGrid( graph, current );
                }

                // Move position
                current += gridInterval;
            }
            // Code for linear minor gridlines and tickmarks 
            // if scale is logarithmic.
            else
            {
                // This code is used only for logarithmic scale and minor tick marks or 
                // gridlines which have linear minor scale in logarithmic major scale. 
                // This code is used to find minimum value for the interval. For example 
                // if logarithmic base is 2 and interval is between 4 and 8; current value 
                // is 5.6; this method will return linearised value for 4. This code works 
                // like Math.Floor for logarithmic scale.
                double logMinimum = this.GetLogMinimum( current, axisSeries );

                if ( oldCurrent != logMinimum )
                {
                    oldCurrent = logMinimum;
                    logStep = 1;
                }

                // Find interval for logarithmic linearised scale
                double logInterval = Math.Log( 1 + (this.interval * logStep), this.Axis.logarithmBase );

                current = oldCurrent;

                // Move position
                current += logInterval;

                logStep++;

                // Reset current position if major interval is passed.
                if ( this.GetLogMinimum( current, axisSeries ) != logMinimum )
                {
                    continue;
                }

                // Check interval size
                if ( logInterval == 0 )
                {
                    throw new InvalidOperationException( SR.ExceptionTickMarksIntervalIsZero );
                }

                // Draw between min & max values only
                if ( ( decimal ) current >= ( decimal ) this.Axis.ViewMinimum && ( decimal ) current <= ( decimal ) this.Axis.ViewMaximum )
                {
                    this.DrawGrid( graph, current );
                }
            }

            // Check if we do not exceed max number of elements
            if ( counter++ > ChartHelper.MaxNumOfGridlines )
            {
                break;
            }
        }

        // Used for auto interval for auto tick marks and 
        // gridlines
        if ( !this.majorGridTick )
        {
            this.interval = oldInterval;
            this.intervalType = oldIntervalType;
            this.intervalOffset = oldIntervalOffset;
            this.intervalOffsetType = oldIntervalOffsetType;
        }
    }

    /// <summary>
    /// This method returns linearized logarithmic value 
    /// which is minimum for range with interval 1.
    /// </summary>
    /// <param name="current">Current value</param>
    /// <param name="axisSeries">First series attached to axis.</param>
    /// <returns>Returns Minimum for the range which contains current value</returns>
    private double GetLogMinimum( double current, Series axisSeries )
    {
        double viewMinimum = this.Axis.ViewMinimum;
        DateTimeIntervalType offsetType = (this.GetIntervalOffsetType() == DateTimeIntervalType.Auto) ? this.GetIntervalType() : this.GetIntervalOffsetType();
        if ( this.GetIntervalOffset() != 0 && axisSeries == null )
        {
            viewMinimum += ChartHelper.GetIntervalSize( viewMinimum, this.GetIntervalOffset(),
                offsetType, axisSeries, 0, DateTimeIntervalType.Number, true, false );
        }

        return viewMinimum + Math.Floor( current - viewMinimum );
    }

    /// <summary>
    /// Draw the grid line 
    /// </summary>
    /// <param name="graph">Chart Graphics object</param>
    /// <param name="current">Current position of the gridline</param>
    private void DrawGrid( ChartGraphics graph, double current )
    {
        // Common elements
        CommonElements common = this.Axis.Common;

        PointF first = PointF.Empty; // The First point of a grid line
        PointF second = PointF.Empty; // The Second point of a grid line
        RectangleF plotArea; // Plot area position

        plotArea = this.Axis.PlotAreaPosition.ToRectangleF();

        // Horizontal gridlines
        if ( this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right )
        {
            first.X = plotArea.X;
            second.X = plotArea.Right;
            first.Y = ( float ) this.Axis.GetLinearPosition( current );
            second.Y = first.Y;
        }

        // Vertical gridlines
        if ( this.Axis.AxisPosition is AxisPosition.Top or AxisPosition.Bottom )
        {
            first.Y = plotArea.Y;
            second.Y = plotArea.Bottom;
            first.X = ( float ) this.Axis.GetLinearPosition( current );
            second.X = first.X;
        }

        if ( common.ProcessModeRegions )
        {
            if ( this.Axis.ChartArea.Area3DStyle.Enable3D && !this.Axis.ChartArea.chartAreaIsCurcular )
            {
                if ( !common.ProcessModePaint ) //if ProcessModePaint is true it will be called later
                    graph.Draw3DGridLine( this.Axis.ChartArea, this.borderColor, this.borderWidth, this.borderDashStyle, first, second, this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right, common, this );
            }
            else if ( !this.Axis.ChartArea.chartAreaIsCurcular )
            {
                using GraphicsPath path = new();
                if ( Math.Abs( first.X - second.X ) > Math.Abs( first.Y - second.Y ) )
                {
                    path.AddLine( first.X, first.Y - 1, second.X, second.Y - 1 );
                    path.AddLine( second.X, second.Y + 1, first.X, first.Y + 1 );
                    path.CloseAllFigures();
                }
                else
                {
                    path.AddLine( first.X - 1, first.Y, second.X - 1, second.Y );
                    path.AddLine( second.X + 1, second.Y, first.X + 1, first.Y );
                    path.CloseAllFigures();

                }
                common.HotRegionsList.AddHotRegion( path, true, ChartElementType.Gridlines, this );
            }
        }

        if ( common.ProcessModePaint )
        {
            // Check if grid lines should be drawn for circular chart area
            if ( this.Axis.ChartArea.chartAreaIsCurcular )
            {
                if ( this.Axis.axisType == AxisName.Y )
                {
                    this.Axis.DrawCircularLine( this, graph, this.borderColor, this.borderWidth, this.borderDashStyle, first.Y );
                }
                if ( this.Axis.axisType == AxisName.X )
                {
                    ICircularChartType chartType = this.Axis.ChartArea.GetCircularChartType();
                    if ( chartType != null && chartType.RadialGridLinesSupported() )
                    {
                        this.Axis.DrawRadialLine( this, graph, this.borderColor, this.borderWidth, this.borderDashStyle, current );
                    }
                }
            }
            else if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
            {
                graph.DrawLineRel( this.borderColor, this.borderWidth, this.borderDashStyle, first, second );
            }
            else
            {
                graph.Draw3DGridLine( this.Axis.ChartArea, this.borderColor, this.borderWidth, this.borderDashStyle, first, second, this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right, this.Axis.Common, this );
            }
        }
    }

    /// <summary>
    /// Draws custom grid lines from custom labels.
    /// </summary>
    /// <param name="graph">Reference to the Chart Graphics object.</param>
    internal void PaintCustom( ChartGraphics graph )
    {
        // Common Elements
        CommonElements common = this.Axis.Common;

        PointF first = PointF.Empty; // The First point of a grid line
        PointF second = PointF.Empty; // The Second point of a grid line
        RectangleF plotArea = this.Axis.PlotAreaPosition.ToRectangleF(); // Plot area position


        // Loop through all custom labels
        foreach ( CustomLabel label in this.Axis.CustomLabels )
        {
            if ( (label.GridTicks & GridTickTypes.Gridline) == GridTickTypes.Gridline )
            {
                double position = (label.ToPosition + label.FromPosition) / 2.0;
                if ( position >= this.Axis.ViewMinimum && position <= this.Axis.ViewMaximum )
                {
                    // Horizontal gridlines
                    if ( this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right )
                    {
                        first.X = plotArea.X;
                        second.X = plotArea.Right;
                        first.Y = ( float ) this.Axis.GetLinearPosition( position );
                        second.Y = first.Y;

                    }

                    // Vertical gridlines
                    if ( this.Axis.AxisPosition is AxisPosition.Top or AxisPosition.Bottom )
                    {
                        first.Y = plotArea.Y;
                        second.Y = plotArea.Bottom;
                        first.X = ( float ) this.Axis.GetLinearPosition( position );
                        second.X = first.X;
                    }

                    if ( common.ProcessModeRegions )
                    {
                        if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                        {
                            using GraphicsPath path = new();

                            if ( Math.Abs( first.X - second.X ) > Math.Abs( first.Y - second.Y ) )
                            {
                                path.AddLine( first.X, first.Y - 1, second.X, second.Y - 1 );
                                path.AddLine( second.X, second.Y + 1, first.X, first.Y + 1 );
                                path.CloseAllFigures();
                            }
                            else
                            {
                                path.AddLine( first.X - 1, first.Y, second.X - 1, second.Y );
                                path.AddLine( second.X + 1, second.Y, first.X + 1, first.Y );
                                path.CloseAllFigures();

                            }
                            common.HotRegionsList.AddHotRegion( path, true, ChartElementType.Gridlines, this );
                        }
                        else
                        {
                            graph.Draw3DGridLine( this.Axis.ChartArea, this.borderColor, this.borderWidth, this.borderDashStyle, first, second, this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right, common, this );
                        }
                    }

                    if ( common.ProcessModePaint )
                    {
                        if ( !this.Axis.ChartArea.Area3DStyle.Enable3D || this.Axis.ChartArea.chartAreaIsCurcular )
                        {
                            graph.DrawLineRel( this.borderColor, this.borderWidth, this.borderDashStyle, first, second );
                        }
                        else
                        {
                            graph.Draw3DGridLine( this.Axis.ChartArea, this.borderColor, this.borderWidth, this.borderDashStyle, first, second, this.Axis.AxisPosition is AxisPosition.Left or AxisPosition.Right, this.Axis.Common, this );
                        }
                    }
                }
            }
        }
    }

    #endregion

    #region " grid properties "

    /// <summary>
    /// Gets or sets grid or tick mark interval offset.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeIntervalOffset3" ),
    TypeConverter( typeof( AxisElementIntervalValueConverter ) )
    ]
    public double IntervalOffset
    {
        get => this.intervalOffset;
        set
        {
            this.intervalOffset = value;
            this.intervalOffsetChanged = true;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Determines if Enabled property should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeIntervalOffset()
    {
        return this.majorGridTick ? !double.IsNaN( this.intervalOffset ) : this.intervalOffset != 0d;
    }

    /// <summary>
    /// Gets the interval offset.
    /// </summary>
    /// <returns></returns>
    internal double GetIntervalOffset()
    {
        return this.majorGridTick && double.IsNaN( this.intervalOffset ) && this.Axis != null
            ? this.Axis.IntervalOffset
            : this.intervalOffset;
    }

    /// <summary>
    /// Gets or sets the unit of measurement of grid or tick mark offset.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeIntervalOffsetType6" ),
    RefreshProperties( RefreshProperties.All )
    ]
    public DateTimeIntervalType IntervalOffsetType
    {
        get => this.intervalOffsetType;
        set
        {
            this.intervalOffsetType = value;
            this.intervalOffsetTypeChanged = true;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Determines if IntervalOffsetType property should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeIntervalOffsetType()
    {
        return this.majorGridTick
            ? this.intervalOffsetType != DateTimeIntervalType.NotSet
            : this.intervalOffsetType != DateTimeIntervalType.Auto;
    }

    /// <summary>
    /// Gets the type of the interval offset.
    /// </summary>
    /// <returns></returns>
    internal DateTimeIntervalType GetIntervalOffsetType()
    {
        return this.majorGridTick && this.intervalOffsetType == DateTimeIntervalType.NotSet && this.Axis != null
            ? this.Axis.IntervalOffsetType
            : this.intervalOffsetType;
    }

    /// <summary>
    /// Gets or sets grid or tick mark interval size.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeInterval6" ),
    TypeConverter( typeof( AxisElementIntervalValueConverter ) ),
    RefreshProperties( RefreshProperties.All ),
    ]
    public double Interval
    {
        get => this.interval;
        set
        {
            // Validation
            if ( value < 0.0 )
                throw new ArgumentException( SR.ExceptionTickMarksIntervalIsNegative, nameof( value ) );

            this.interval = value;
            this.intervalChanged = true;

            // Enable minor elements
            if ( !this.majorGridTick && value != 0.0 && !double.IsNaN( value ) )
            {
                // Prevent grids enabling during the serialization
                if ( this.Axis is not null )
                {
                    if ( this.Axis.Chart != null && this.Axis.Chart.serializing )
                    {
                        this.Enabled = true;
                    }
                }
            }

            // Reset original property value fields
            if ( this.Axis is not null )
            {
                if ( this is TickMark )
                {
                    if ( this.majorGridTick )
                    {
                        this.Axis.tempMajorTickMarkInterval = this.interval;
                    }
                    else
                    {
                        this.Axis.tempMinorTickMarkInterval = this.interval;
                    }
                }
                else
                {
                    if ( this.majorGridTick )
                    {
                        this.Axis.tempMajorGridInterval = this.interval;
                    }
                    else
                    {
                        this.Axis.tempMinorGridInterval = this.interval;
                    }
                }
            }

            this.Invalidate();
        }

    }

    /// <summary>
    /// Determines if IntervalOffsetType property should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeInterval()
    {
        return this.majorGridTick ? !double.IsNaN( this.interval ) : this.interval != 0d;
    }

    /// <summary>
    /// Gets the interval.
    /// </summary>
    /// <returns></returns>
    internal double GetInterval()
    {
        return this.majorGridTick && double.IsNaN( this.interval ) && this.Axis != null ? this.Axis.Interval : this.interval;
    }

    /// <summary>
    /// Gets or sets the unit of measurement of grid or tick mark interval.
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeIntervalType3" ),
    RefreshProperties( RefreshProperties.All )
    ]
    public DateTimeIntervalType IntervalType
    {
        get => this.intervalType;
        set
        {
            this.intervalType = value;
            this.intervalTypeChanged = true;

            // Reset original property value fields
            if ( this.Axis is not null )
            {
                if ( this is TickMark )
                {
                    this.Axis.tempTickMarkIntervalType = this.intervalType;
                }
                else
                {
                    this.Axis.tempGridIntervalType = this.intervalType;
                }
            }

            this.Invalidate();
        }
    }

    /// <summary>
    /// Determines if IntervalOffsetType property should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeIntervalType()
    {
        return this.majorGridTick ? this.intervalType != DateTimeIntervalType.NotSet : this.intervalType != DateTimeIntervalType.Auto;
    }

    /// <summary>
    /// Gets the type of the interval.
    /// </summary>
    /// <returns></returns>
    internal DateTimeIntervalType GetIntervalType()
    {
        if ( this.majorGridTick && this.intervalType == DateTimeIntervalType.NotSet && this.Axis is not null )
        {
            // Return default value during serialization
            return this.Axis.IntervalType;
        }
        return this.intervalType;
    }

    /// <summary>
    /// Gets or sets grid or tick mark line color.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "Black" ),
    SRDescription( "DescriptionAttributeLineColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color LineColor
    {
        get => this.borderColor;
        set
        {
            this.borderColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets grid or tick mark line style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartDashStyle.Solid ),
    SRDescription( "DescriptionAttributeLineDashStyle" )
    ]
    public ChartDashStyle LineDashStyle
    {
        get => this.borderDashStyle;
        set
        {
            this.borderDashStyle = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets grid or tick mark line width.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeLineWidth" )
    ]
    public int LineWidth
    {
        get => this.borderWidth;
        set
        {
            this.borderWidth = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets a flag which indicates if the grid or tick mark is enabled.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeEnabled5" )
    ]
    public bool Enabled
    {
        get =>
            //// Never serialize this property for minor elements
            //// "Disabled" property should be serialized instead.
            //if(this.axis != null && this.axis.IsSerializing())
            //{
            //    if(!this.majorGridTick)
            //    {
            //        // Return default value to prevent serialization
            //        return true;
            //    }
            //}

            this.enabled;
        set
        {
            this.enabled = value;
            this.enabledChanged = true;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Determines if Enabled property should be serialized.
    /// </summary>
    /// <returns></returns>
    internal bool ShouldSerializeEnabled()
    {
        return this.majorGridTick ? !this.Enabled : this.Enabled;
    }

    /// <summary>
    /// Gets or sets the reference to the Axis object
    /// </summary>
    internal Axis Axis { get; set; }

    #endregion
}
