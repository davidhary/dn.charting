//
//  Purpose:	ChartAreaCollection class represents a strongly 
//              typed collection of ChartArea objects.
//


namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// The ChartAreaCollection class represents a strongly typed collection of 
/// ChartArea objects. Each chart area has a unique name in the collection
/// and can be retrieved by name or by index.
/// </summary>
public class ChartAreaCollection : ChartNamedElementCollection<ChartArea>
{
    #region " constructtion "

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartAreaCollection"/> class.
    /// </summary>
    /// <param name="chartPicture">Parent chart picture.</param>
    internal ChartAreaCollection( ChartPicture chartPicture ) : base( chartPicture )
    {
    }

    #endregion

    #region " properties "
    /// <summary>
    /// Gets the default chart area name.
    /// </summary>
    internal string DefaultNameReference => this.Count > 0 ? this[0].Name : string.Empty;
    #endregion

    #region " methods "

    /// <summary>
    /// Creates a new ChartArea with the specified name and adds it to the collection.
    /// </summary>
    /// <param name="name">The new chart area name.</param>
    /// <returns></returns>
    public ChartArea Add( string name )
    {
        ChartArea area = new( name );
        this.Add( area );
        return area;
    }

    #endregion

    #region " event handlers "
    /// <summary>
    /// Updates the ChartArea alignment references to another chart areas.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="NameReferenceChangedEventArgs"/> instance containing the event data.</param>
    internal void ChartAreaNameReferenceChanged( object sender, NameReferenceChangedEventArgs e )
    {
        foreach ( ChartArea chartArea in this )
            if ( chartArea.AlignWithChartArea == e.OldName )
                chartArea.AlignWithChartArea = e.NewName;
    }

    #endregion
}
