
using System.ComponentModel;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// ChartElement is the most basic element of the chart element hierarchy. 
/// </summary>
public abstract class ChartElement : IChartElement, IDisposable
{
    #region " member variables "

#pragma warning disable IDE1006 // Naming Styles
    private IChartElement _parent;
    private CommonElements _common;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets an object associated with this chart element.
    /// </summary>
    /// <value>
    /// An <see cref="object"/> associated with this chart element.
    /// </value>
    /// <remarks>
    /// This property may be used to store additional data with this chart element.
    /// </remarks>
    [
    Browsable( false ),
    DefaultValue( null ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    Utilities.SerializationVisibility( Utilities.SerializationVisibility.Hidden )
    ]
    public object Tag { get; set; } = null;

    /// <summary>
    /// Gets or sets the parent chart element or collection.
    /// </summary>
    /// <value>The parent chart element or collection.</value>
    internal virtual IChartElement Parent
    {
        get => this._parent;
        set => this._parent = value;
    }

    /// <summary>
    /// Gets a shortcut to Common intance providing access to the various chart related services.
    /// </summary>
    /// <value>The Common instance.</value>
    internal CommonElements Common
    {
        get
        {
            if ( this._common == null && this._parent is not null )
            {
                this._common = this._parent.Common;
            }
            return this._common;
        }
        set => this._common = value;
    }

    /// <summary>
    /// Gets the chart.
    /// </summary>
    /// <value>The chart.</value>
    internal Chart Chart => this.Common?.Chart;

    #endregion

    #region " constructtion "

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartElement"/> class.
    /// </summary>
    protected ChartElement()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartElement"/> class.
    /// </summary>
    /// <param name="parent">The parent chart element or collection.</param>
    internal ChartElement( IChartElement parent ) => this._parent = parent;

    #endregion

    #region " methods "

    /// <summary>
    /// Invalidates this chart element.
    /// </summary>
    internal virtual void Invalidate()
    {
        this._parent?.Invalidate();
    }

    #endregion

    #region " ichartelement members "


    IChartElement IChartElement.Parent
    {
        get => this._parent;
        set => this.Parent = value;
    }

    void IChartElement.Invalidate()
    {
        this.Invalidate();
    }

    CommonElements IChartElement.Common => this.Common;

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose( bool disposing )
    {
    }

    /// <summary>
    /// Performs freeing, releasing, or resetting managed resources.
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Returns a <see cref="string"/> that represents the current <see cref="object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="string"/> that represents the current <see cref="object"/>.
    /// </returns>
    /// <remarks>For internal use.</remarks>
    internal virtual string ToStringInternal()
    {
        return this.GetType().Name;
    }

    /// <summary>
    /// Returns a <see cref="string"/> that represents the current <see cref="object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="string"/> that represents the current <see cref="object"/>.
    /// </returns>
    public override string ToString()
    {
        return this.ToStringInternal();
    }

    /// <summary>
    /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="object"/>.
    /// </summary>
    /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="object"/>.</param>
    /// <returns>
    /// true if the specified <see cref="object"/> is equal to the current <see cref="object"/>; otherwise, false.
    /// </returns>
    /// <exception cref="NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
    /// <remarks>For internal use.</remarks>
    internal virtual bool EqualsInternal( object obj )
    {
        return base.Equals( obj );
    }

    /// <summary>
    /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="object"/>.
    /// </summary>
    /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="object"/>.</param>
    /// <returns>
    /// true if the specified <see cref="object"/> is equal to the current <see cref="object"/>; otherwise, false.
    /// </returns>
    /// <exception cref="NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
    public override bool Equals( object obj )
    {
        return this.EqualsInternal( obj );
    }

    /// <summary>
    /// Serves as a hash function for a particular type.
    /// </summary>
    /// <returns>
    /// A hash code for the current <see cref="object"/>.
    /// </returns>
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    #endregion
}
/// <summary>
/// ChartNamedElement is a base class for most chart elements. Series, ChartAreas, Legends and other chart elements have a Name and reuse the unique name generation and validation logic provided by the ChartNamedElementCollection.
/// </summary>
public abstract class ChartNamedElement : ChartElement
{
    #region " member variables "

#pragma warning disable IDE1006 // Naming Styles
    private string _name = string.Empty;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " properties "

    /// <summary>
    /// Gets or sets the name of the chart element.
    /// </summary>
    /// <value>The name.</value>
    [DefaultValue( "" )]
    public virtual string Name
    {
        get => this._name;
        set
        {
            if ( this._name != value )
            {
                if ( this.Parent is INameController )
                {
                    INameController nameController = this.Parent as INameController;

                    if ( !nameController.IsUniqueName( value ) )
                        throw new ArgumentException( SR.ExceptionNameAlreadyExistsInCollection( value, nameController.GetType().Name ) );

                    // Fire the name change events in case when the old name is not empty
                    NameReferenceChangedEventArgs args = new( this, this._name, value );
                    nameController.OnNameReferenceChanging( args );
                    this._name = value;
                    nameController.OnNameReferenceChanged( args );
                }
                else
                {
                    this._name = value;
                }
                this.Invalidate();
            }
        }
    }

    #endregion

    #region " constructtion "

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartNamedElement"/> class.
    /// </summary>
    protected ChartNamedElement()
        : base()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartNamedElement"/> class.
    /// </summary>
    /// <param name="name">The name of the new chart element.</param>
    protected ChartNamedElement( string name )
        : base() => this._name = name;

    /// <summary>
    /// Initializes a new instance of the <see cref="ChartNamedElement"/> class.
    /// </summary>
    /// <param name="parent">The parent chart element.</param>
    /// <param name="name">The name of the new chart element.</param>
    internal ChartNamedElement( IChartElement parent, string name ) : base( parent ) => this._name = name;

    #endregion

    #region " methods "

    /// <summary>
    /// Returns a <see cref="string"/> that represents the current <see cref="object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="string"/> that represents the current <see cref="object"/>.
    /// </returns>
    internal override string ToStringInternal()
    {
        string typeName = this.GetType().Name;
        return string.IsNullOrEmpty( this._name ) ? typeName : typeName + '-' + this._name;
    }

    #endregion
}
/// <summary>
/// NameReferenceChanged events help chart maintain referencial integrity.
/// </summary>
internal class NameReferenceChangedEventArgs : EventArgs
{
    #region " membervaliables "


    #endregion

    #region " properties "
    public ChartNamedElement OldElement { get; private set; }
    public string OldName { get; private set; }
    public string NewName { get; private set; }
    #endregion

    #region " constructor "
    public NameReferenceChangedEventArgs( ChartNamedElement oldElement, ChartNamedElement newElement )
    {
        this.OldElement = oldElement;
        this.OldName = oldElement != null ? oldElement.Name : string.Empty;
        this.NewName = newElement != null ? newElement.Name : string.Empty;
    }
    public NameReferenceChangedEventArgs( ChartNamedElement oldElement, string oldName, string newName )
    {
        this.OldElement = oldElement;
        this.OldName = oldName;
        this.NewName = newName;
    }
    #endregion
}
