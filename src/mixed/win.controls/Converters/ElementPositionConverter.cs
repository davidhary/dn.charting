//
//  Purpose:	Converter of the element position.
//


using System.ComponentModel;
using System.Globalization;

namespace System.Windows.Forms.DataVisualization.Charting;
/// <summary>
/// Element position converter.
/// </summary>
internal class ElementPositionConverter : ExpandableObjectConverter
{
    #region " converter methods "

    /// <summary>
    /// Overrides the CanConvertFrom method of TypeConverter.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="sourceType">Convertion source type.</param>
    /// <returns>Indicates if convertion is possible.</returns>
    public override bool CanConvertFrom( ITypeDescriptorContext context, Type sourceType )
    {
        return sourceType == typeof( string ) || base.CanConvertFrom( context, sourceType );
    }

    /// <summary>
    /// Overrides the CanConvertTo method of TypeConverter.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="destinationType">Destination type.</param>
    /// <returns>Indicates if convertion is possible.</returns>
    public override bool CanConvertTo( ITypeDescriptorContext context, Type destinationType )
    {
        return destinationType == typeof( string ) || base.CanConvertTo( context, destinationType );
    }

    /// <summary>
    /// Overrides the ConvertTo method of TypeConverter.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="culture">Culture information.</param>
    /// <param name="value">Value to convert.</param>
    /// <param name="destinationType">Convertion destination type.</param>
    /// <returns>Converted object.</returns>
    public override object ConvertTo( ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType )
    {
        return destinationType == typeof( string )
            ? (( ElementPosition ) value).ToString()
            : base.ConvertTo( context, culture, value, destinationType );
    }

    /// <summary>
    /// Overrides the ConvertFrom method of TypeConverter.
    /// Converts from string with comma separated values.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="culture">Culture information.</param>
    /// <param name="value">Value to convert from.</param>
    /// <returns>Indicates if convertion is possible.</returns>
    public override object ConvertFrom( ITypeDescriptorContext context, CultureInfo culture, object value )
    {
        if ( value is string posValue )
        {
            if ( string.Compare( posValue, Constants.AutoValue, StringComparison.OrdinalIgnoreCase ) == 0 )
            {
                return new ElementPosition();
            }
            else
            {
                string[] array = posValue.Split( ',' );
                return array.Length == 4
                    ? new ElementPosition(
                        float.Parse( array[0], System.Globalization.CultureInfo.CurrentCulture ),
                        float.Parse( array[1], System.Globalization.CultureInfo.CurrentCulture ),
                        float.Parse( array[2], System.Globalization.CultureInfo.CurrentCulture ),
                        float.Parse( array[3], System.Globalization.CultureInfo.CurrentCulture ) )
                    : throw (new ArgumentException( SR.ExceptionElementPositionConverter ));
            }
        }
        return base.ConvertFrom( context, culture, value );
    }

    #endregion
}
