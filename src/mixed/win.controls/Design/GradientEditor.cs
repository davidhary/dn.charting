//
//  Purpose:	Design-time gradient editor class. 
//


using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Reflection;
using System.Windows.Forms.DataVisualization.Charting;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// AxisName editor for the gradient type.
/// Paints a rectangle with gradient sample.
/// </summary>
internal class GradientEditor : UITypeEditor, IDisposable
{
    #region " editor method and properties "

#pragma warning disable IDE1006 // Naming Styles
    private ChartGraphics _chartGraph;
    private bool _disposed;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <returns>Can paint values.</returns>
    public override bool GetPaintValueSupported( ITypeDescriptorContext context )
    {
        return true;
    }

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="e">Paint value event arguments.</param>
    public override void PaintValue( PaintValueEventArgs e )
    {
        if ( e.Value is GradientStyle style )
        {
            // Create chart graphics object
            this._chartGraph ??= new ChartGraphics( null );
            this._chartGraph.Graphics = e.Graphics;

            // Try to get original color from the object
            Color color1 = Color.Black;
            Color color2 = Color.White;
            if ( e.Context != null && e.Context.Instance is not null )
            {
                // Get color properties using reflection
                PropertyInfo propertyInfo = e.Context.Instance.GetType().GetProperty( "BackColor" );
                if ( propertyInfo is not null )
                {
                    color1 = ( Color ) propertyInfo.GetValue( e.Context.Instance, null );
                }
                else
                {
                    propertyInfo = e.Context.Instance.GetType().GetProperty( "BackColor" );
                    if ( propertyInfo is not null )
                    {
                        color1 = ( Color ) propertyInfo.GetValue( e.Context.Instance, null );
                    }
                    else
                    {
                        // If object do not have "BackColor" property try using "Color" property 
                        propertyInfo = e.Context.Instance.GetType().GetProperty( "Color" );
                        if ( propertyInfo is not null )
                        {
                            color1 = ( Color ) propertyInfo.GetValue( e.Context.Instance, null );
                        }
                    }
                }

                propertyInfo = e.Context.Instance.GetType().GetProperty( "BackSecondaryColor" );
                if ( propertyInfo is not null )
                {
                    color2 = ( Color ) propertyInfo.GetValue( e.Context.Instance, null );
                }
                else
                {
                    propertyInfo = e.Context.Instance.GetType().GetProperty( "BackSecondaryColor" );
                    if ( propertyInfo is not null )
                    {
                        color2 = ( Color ) propertyInfo.GetValue( e.Context.Instance, null );
                    }
                }

            }

            // Check if colors are valid
            if ( color1 == Color.Empty )
            {
                color1 = Color.Black;
            }
            if ( color2 == Color.Empty )
            {
                color2 = Color.White;
            }
            if ( color1 == color2 )
            {
                color2 = Color.FromArgb( color1.B, color1.R, color1.G );
            }


            // Draw gradient sample
            if ( style != GradientStyle.None )
            {
                Brush brush = this._chartGraph.GetGradientBrush( e.Bounds, color1, color2, style );
                e.Graphics.FillRectangle( brush, e.Bounds );

                brush.Dispose();
            }
        }
    }

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Finalizer for the GradientEditor, disposes any remaining
    /// resources if it has not already been disposed.
    /// </summary>
    ~GradientEditor()
    {
        this.Dispose( false );
    }

    /// <summary>
    /// Disposes resources used by this object.
    /// </summary>
    /// <param name="disposing">Whether this method was called form Dispose() or the finalizer.</param>
    protected virtual void Dispose( bool disposing )
    {
        if ( !this._disposed )
        {
            if ( disposing )
            {
                this._chartGraph.Dispose();
            }

            this._disposed = true;
        }
    }

    /// <summary>
    /// Disposes all resources used by this object.
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    #endregion
}
