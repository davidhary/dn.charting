//
//  Purpose:	Design-time editors and converters for the Axes array.
//


using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms.DataVisualization.Charting;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// Designer editor for the chart areas collection.
/// </summary>
internal class AxesArrayEditor : ArrayEditor
{
    #region " fields and constructor "

#pragma warning disable IDE1006 // Naming Styles

    // Collection form
    private CollectionForm _form;

    // Help topic string
    private string _helpTopic = "";

#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Object constructor.
    /// </summary>
    public AxesArrayEditor() : base( typeof( Axis[] ) )
    {
    }

    #endregion

    #region " methods "

    /// <summary>
    /// Items can not be removed.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <returns>False if can't remove.</returns>
    protected override bool CanRemoveInstance( object value )
    {
        return false;
    }

    /// <summary>
    /// Override the HelpTopic property to provide different topics,
    /// depending on selected property.
    /// </summary>
    protected override string HelpTopic => (this._helpTopic.Length == 0) ? base.HelpTopic : this._helpTopic;

    /// <summary>
    /// Returns the collection form property grid. Added for VS2005 compatibility.
    /// </summary>
    /// <param name="controls"></param>
    /// <returns></returns>
    private PropertyGrid GetPropertyGrid( Control.ControlCollection controls )
    {
        foreach ( Control control in controls )
        {
            if ( control is PropertyGrid grid )
            {
                return grid;
            }
            if ( control.Controls.Count > 0 )
            {
                grid = this.GetPropertyGrid( control.Controls );
                if ( grid is not null )
                {
                    return grid;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Collect the collection editor form buttons into array. Added for VS2005 compatibility.
    /// </summary>
    /// <param name="buttons"></param>
    /// <param name="controls"></param>
    private void CollectButtons( ArrayList buttons, Control.ControlCollection controls )
    {
        foreach ( Control control in controls )
        {
            if ( control is Button )
            {
                _ = buttons.Add( control );
            }
            if ( control.Controls.Count > 0 )
            {
                this.CollectButtons( buttons, control.Controls );
            }
        }
    }

    /// <summary>
    /// Displaying help for the currently selected item in the property grid
    /// </summary>
    protected override void ShowHelp()
    {
        // Init topic name
        this._helpTopic = "";
        PropertyGrid grid = this.GetPropertyGrid( this._form.Controls ); ;

        // Check currently selected grid item
        if ( grid is not null )
        {
            GridItem item = grid.SelectedGridItem;
            if ( item != null && (item.GridItemType == GridItemType.Property || item.GridItemType == GridItemType.ArrayValue) )
            {
                this._helpTopic = item.PropertyDescriptor.ComponentType.ToString() + "." + item.PropertyDescriptor.Name;
            }
        }

        // Call base class
        base.ShowHelp();

        // Re-Init topic name
        this._helpTopic = "";
    }

    /// <summary>
    /// Creates editor's form.
    /// </summary>
    /// <returns>Collection form.</returns>
    protected override CollectionForm CreateCollectionForm()
    {
        // Create collection form using the base class
        this._form = base.CreateCollectionForm();
        // Changed Apr 29, DT,  for VS2005 compatibility
        PropertyGrid grid = this.GetPropertyGrid( this._form.Controls );
        if ( grid is not null )
        {
            // Show properties help
            grid.HelpVisible = true;
            grid.CommandsVisibleIfAvailable = true;
        }

        // Changed Apr 29, DT, for VS2005 compatibility
        ArrayList buttons = [];
        this.CollectButtons( buttons, this._form.Controls );
        foreach ( Button button in buttons )
        {
            if ( button.Name.StartsWith( "add", StringComparison.OrdinalIgnoreCase ) ||
                button.Name.StartsWith( "remove", StringComparison.OrdinalIgnoreCase ) ||
                button.Text.Length == 0 )
            {
                button.Enabled = false;
                button.EnabledChanged += new EventHandler( this.Button_EnabledChanged );
            }
        }
        return this._form;
    }

#pragma warning disable IDE1006 // Naming Styles
    /// <summary>
    /// Flag to prevent stack overflow.
    /// </summary>
    private bool _button_EnabledChanging;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Handles the EnabledChanged event of the Button control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    private void Button_EnabledChanged( object sender, EventArgs e )
    {
        if ( !this._button_EnabledChanging && sender is Button s )
        {
            this._button_EnabledChanging = true;
            try
            {
                s.Enabled = false;
            }
            finally
            {
                this._button_EnabledChanging = false;
            }
        }
    }

    #endregion
}

internal class DataPointCustomPropertiesConverter : TypeConverter
{
    /// <summary>
    /// Returns whether this object supports properties, using the specified context.
    /// </summary>
    /// <param name="context">An <see cref="ITypeDescriptorContext"/> that provides a format context.</param>
    /// <returns>
    /// true if <see cref="System.ComponentModel.TypeConverter.GetProperties(System.Object)"/> should be called to find the properties of this object; otherwise, false.
    /// </returns>
    public override bool GetPropertiesSupported( ITypeDescriptorContext context )
    {
        return true;
    }

    /// <summary>
    /// Returns a collection of properties for the type of array specified by the value parameter, using the specified context and attributes.
    /// </summary>
    /// <param name="context">An <see cref="ITypeDescriptorContext"/> that provides a format context.</param>
    /// <param name="value">An <see cref="object"/> that specifies the type of array for which to get properties.</param>
    /// <param name="attributes">An array of type <see cref="Attribute"/> that is used as a filter.</param>
    /// <returns>
    /// A <see cref="PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.
    /// </returns>
    public override PropertyDescriptorCollection GetProperties( ITypeDescriptorContext context, object value, Attribute[] attributes )
    {
        // Fill collection with properties descriptors
        PropertyDescriptorCollection propDescriptors = TypeDescriptor.GetProperties( value, attributes, false );

        // Return original collection if not in design mode
        if ( context != null && context.Instance is ChartElement &&
            (context.Instance as ChartElement).Chart != null &&
            (context.Instance as ChartElement).Chart.IsDesignMode() )
        {
            // Create new descriptors collection
            PropertyDescriptorCollection newPropDescriptors = new( null );

            // Loop through all original property descriptors
            foreach ( PropertyDescriptor propertyDescriptor in propDescriptors )
            {
                // Change name of "CustomAttributesEx" property to "CustomProperties"
                if ( propertyDescriptor.Name == "CustomAttributesEx" )
                {
                    DynamicPropertyDescriptor dynPropDesc = new(
                        propertyDescriptor,
                        "CustomProperties" );
                    _ = newPropDescriptors.Add( dynPropDesc );
                }
                else
                {
                    _ = newPropDescriptors.Add( propertyDescriptor );
                }
            }
            return newPropDescriptors;
        }

        // Return original collection if not in design mode
        return propDescriptors;

    }

    /// <summary>
    /// Converts the given value object to the specified type, using the specified context and culture information.
    /// </summary>
    /// <param name="context">An <see cref="ITypeDescriptorContext"/> that provides a format context.</param>
    /// <param name="culture">A <see cref="Globalization.CultureInfo"/>. If null is passed, the current culture is assumed.</param>
    /// <param name="value">The <see cref="object"/> to convert.</param>
    /// <param name="destinationType">The <see cref="Type"/> to convert the <paramref name="value"/> parameter to.</param>
    /// <returns>
    /// An <see cref="object"/> that represents the converted value.
    /// </returns>
    /// <exception cref="ArgumentNullException">The <paramref name="destinationType"/> parameter is null. </exception>
    /// <exception cref="NotSupportedException">The conversion cannot be performed. </exception>
    public override object ConvertTo( ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType )
    {
        if ( context is not null )
        {
            if ( destinationType == typeof( string ) )
            {
                return "";
            }
        }                // Always call base, even if you can't convert.
        return base.ConvertTo( context, culture, value, destinationType );
    }
}
/// <summary>
/// DataPoint Converter - helps windows form serializer to create inline datapoints.
/// </summary>
internal class DataPointConverter : DataPointCustomPropertiesConverter
{
    /// <summary>
    /// This method overrides CanConvertTo from TypeConverter. This is called when someone
    /// wants to convert an instance of object to another type.  Here,
    /// only conversion to an InstanceDescriptor is supported.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="destinationType">Destination type.</param>
    /// <returns>True if object can be converted.</returns>
    public override bool CanConvertTo( ITypeDescriptorContext context, Type destinationType )
    {
        if ( destinationType == typeof( InstanceDescriptor ) )
        {
            return true;
        }

        // Always call the base to see if it can perform the conversion.
        return base.CanConvertTo( context, destinationType );
    }

    /// <summary>
    /// This methods performs the actual conversion from an object to an InstanceDescriptor.
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <param name="culture">Culture information.</param>
    /// <param name="value">Object value.</param>
    /// <param name="destinationType">Destination type.</param>
    /// <returns>Converted object.</returns>
    public override object ConvertTo( ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType )
    {
        if ( destinationType == typeof( InstanceDescriptor ) && value is DataPoint dataPoint )
        {
            if ( dataPoint.YValues.Length > 1 )
            {
                ConstructorInfo ci = typeof( DataPoint ).GetConstructor( [typeof( double ), typeof( string )] );
                string yValues = "";
                foreach ( double y in dataPoint.YValues )
                {
                    yValues += y.ToString( System.Globalization.CultureInfo.InvariantCulture ) + ",";
                }

                return new InstanceDescriptor( ci, new object[] { dataPoint.XValue, yValues.TrimEnd( ',' ) }, false );
            }
            else
            {
                ConstructorInfo ci = typeof( DataPoint ).GetConstructor( [typeof( double ), typeof( double )] );
                return new InstanceDescriptor( ci, new object[] { dataPoint.XValue, dataPoint.YValues[0] }, false );
            }
        }
        // Always call base, even if you can't convert.
        return base.ConvertTo( context, culture, value, destinationType );
    }

}
