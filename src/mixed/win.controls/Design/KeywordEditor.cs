//
//  Purpose:	A keyword editor form. Allows the end user to insert
//				new and edit exsisting keywords in the string.
//


using System.Collections;
using System.Globalization;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.DataVisualization.Charting.Utilities;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// Summary description for KeywordEditor.
/// </summary>
internal class KeywordEditor : Form
{
    #region " fields "

#pragma warning disable IDE1006 // Naming Styles
    /// <summary>
    /// List of keywords that are applicable to the edited property
    /// </summary>
    private readonly ArrayList _applicableKeywords;

    /// <summary>
    /// Keyword begin edited or empty if inserting a new one.
    /// </summary>
    internal string Keyword = string.Empty;

    /// <summary>
    /// Maximum number of supported Y values.
    /// </summary>
    private readonly int _maxYValueIndex = 9;

    // Form fields
    private GroupBox _groupBoxKeywords;
    private ListBox _listBoxKeywords;
    private GroupBox _groupBoxDescription;
    private Label _labelDescription;
    private Button _buttonCancel;
    private Button _buttonOk;
    private GroupBox _groupBoxFormat;
    private Label _labelFormat;
    private NumericUpDown _numericUpDownYValue;
    private Label _labelYValue;
    private ComboBox _comboBoxFormat;
    private Label _labelPrecision;
    private TextBox _textBoxCustomFormat;
    private Label _labelCustomFormat;
    private Label _labelSample;
    private TextBox _textBoxSample;
    private TextBox _textBoxPrecision;
    private ToolTip _toolTip;
    private System.ComponentModel.Container _components;

    // resolved VSTS by extending the dialog by 36x28 pixels.
    // 5767	FRA: ChartAPI: String "Format Sample:" is  truncated on the "Keywords Editor'	
    // 4383	DEU: VC/VB/VCS/VWD: ChartAPI: The string "If a chart type supports..." is truncated on the 'Keyword Editor' dialog.
    // 3524	DEU: VC/VB/VCS/VWD: ChartAPI: The string "If a chart type supports..." is truncated on the 'Keyword Editor' dialog.

    private static readonly int widthDialogExtend = 80;
    private static readonly int heightDialogExtend = 38;
#pragma warning restore IDE1006 // Naming Styles

    #endregion // Fields

    #region " constructtion "

    /// <summary>
    /// Default public constructor.
    /// </summary>
    public KeywordEditor()
    {
        //
        // Required for Windows Form Designer support
        //
        this.InitializeComponent();
        this.PrepareControlsLayout();
    }

    /// <summary>
    /// Form constructor.
    /// </summary>
    /// <param name="applicableKeywords">List of keywords that can be inserted.</param>
    /// <param name="keyword">Keyword that should be edited.</param>
    /// <param name="maxYValueIndex">Maximum number of Y Values supported.</param>
    public KeywordEditor( ArrayList applicableKeywords, string keyword, int maxYValueIndex ) : this()
    {
        // Save input data
        this._applicableKeywords = applicableKeywords;
        this.Keyword = keyword;
        this._maxYValueIndex = maxYValueIndex;
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this._components?.Dispose();
        }
        base.Dispose( disposing );
    }

    #endregion // Constructors

    #region " windows form designer generated code "
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._components = new System.ComponentModel.Container();
        this._groupBoxKeywords = new GroupBox();
        this._listBoxKeywords = new ListBox();
        this._groupBoxDescription = new GroupBox();
        this._labelDescription = new Label();
        this._buttonCancel = new Button();
        this._buttonOk = new Button();
        this._groupBoxFormat = new GroupBox();
        this._textBoxPrecision = new TextBox();
        this._labelSample = new Label();
        this._textBoxSample = new TextBox();
        this._numericUpDownYValue = new NumericUpDown();
        this._labelYValue = new Label();
        this._comboBoxFormat = new ComboBox();
        this._labelPrecision = new Label();
        this._labelFormat = new Label();
        this._labelCustomFormat = new Label();
        this._textBoxCustomFormat = new TextBox();
        this._toolTip = new ToolTip( this._components );
        this._groupBoxKeywords.SuspendLayout();
        this._groupBoxDescription.SuspendLayout();
        this._groupBoxFormat.SuspendLayout();
        (( System.ComponentModel.ISupportInitialize ) this._numericUpDownYValue).BeginInit();
        this.SuspendLayout();
        // 
        // groupBoxKeywords
        // 
        this._groupBoxKeywords.Controls.AddRange( [
                                                                                       this._listBoxKeywords] );
        this._groupBoxKeywords.Location = new Drawing.Point( 8, 16 );
        this._groupBoxKeywords.Name = "groupBoxKeywords";
        this._groupBoxKeywords.Size = new Drawing.Size( 216, 232 );
        this._groupBoxKeywords.TabIndex = 0;
        this._groupBoxKeywords.TabStop = false;
        this._groupBoxKeywords.Text = SR.LabelKeyKeywords;
        // 
        // listBoxKeywords
        // 
        this._listBoxKeywords.Location = new Drawing.Point( 8, 24 );
        this._listBoxKeywords.Name = "listBoxKeywords";
        this._listBoxKeywords.Size = new Drawing.Size( 200, 199 );
        this._listBoxKeywords.TabIndex = 0;
        this._listBoxKeywords.DoubleClick += new EventHandler( this.ListBoxKeywords_DoubleClick );
        this._listBoxKeywords.SelectedIndexChanged += new EventHandler( this.ListBoxKeywords_SelectedIndexChanged );
        // 
        // groupBoxDescription
        // 
        this._groupBoxDescription.Controls.AddRange( [
                                                                                          this._labelDescription] );
        this._groupBoxDescription.Location = new Drawing.Point( 240, 16 );
        this._groupBoxDescription.Name = "groupBoxDescription";
        this._groupBoxDescription.Size = new Drawing.Size( 328, 88 );
        this._groupBoxDescription.TabIndex = 1;
        this._groupBoxDescription.TabStop = false;
        this._groupBoxDescription.Text = SR.LabelDescription;
        // 
        // labelDescription
        // 
        this._labelDescription.Location = new Drawing.Point( 16, 24 );
        this._labelDescription.Name = "labelDescription";
        this._labelDescription.Size = new Drawing.Size( 304, 56 );
        this._labelDescription.TabIndex = 0;
        this._labelDescription.Text = "<replaced at runtime>";
        // 
        // buttonCancel
        // 
        this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this._buttonCancel.Location = new Drawing.Point( 479, 256 );
        this._buttonCancel.Name = "buttonCancel";
        this._buttonCancel.Size = new Drawing.Size( 90, 27 );
        this._buttonCancel.TabIndex = 4;
        this._buttonCancel.Text = SR.LabelButtonCancel;
        // 
        // buttonOk
        // 
        this._buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
        this._buttonOk.Location = new Drawing.Point( 367, 256 );
        this._buttonOk.Name = "buttonOk";
        this._buttonOk.Size = new Drawing.Size( 90, 27 );
        this._buttonOk.TabIndex = 3;
        this._buttonOk.Text = SR.LabelButtonOk;
        this._buttonOk.Click += new EventHandler( this.ButtonOk_Click );
        // 
        // groupBoxFormat
        // 
        this._groupBoxFormat.Controls.AddRange( [
                                                                                     this._textBoxPrecision,
                                                                                     this._labelSample,
                                                                                     this._textBoxSample,
                                                                                     this._numericUpDownYValue,
                                                                                     this._labelYValue,
                                                                                     this._comboBoxFormat,
                                                                                     this._labelPrecision,
                                                                                     this._labelFormat,
                                                                                     this._labelCustomFormat,
                                                                                     this._textBoxCustomFormat] );
        this._groupBoxFormat.Location = new Drawing.Point( 240, 112 );
        this._groupBoxFormat.Name = "groupBoxFormat";
        this._groupBoxFormat.Size = new Drawing.Size( 328, 136 );
        this._groupBoxFormat.TabIndex = 2;
        this._groupBoxFormat.TabStop = false;
        this._groupBoxFormat.Text = SR.LabelValueFormatting;
        // 
        // textBoxPrecision
        // 
        this._textBoxPrecision.Location = new Drawing.Point( 112, 48 );
        this._textBoxPrecision.Name = "textBoxPrecision";
        this._textBoxPrecision.Size = new Drawing.Size( 64, 20 );
        this._textBoxPrecision.TabIndex = 3;
        this._textBoxPrecision.Text = "";
        this._textBoxPrecision.TextChanged += new EventHandler( this.TextBoxPrecision_TextChanged );
        // 
        // labelSample
        // 
        this._labelSample.Location = new Drawing.Point( 8, 72 );
        this._labelSample.Name = "labelSample";
        this._labelSample.Size = new Drawing.Size( 96, 23 );
        this._labelSample.TabIndex = 7;
        this._labelSample.Text = SR.LabelFormatKeySample;
        this._labelSample.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // textBoxSample
        // 
        this._textBoxSample.Location = new Drawing.Point( 112, 72 );
        this._textBoxSample.Name = "textBoxSample";
        this._textBoxSample.ReadOnly = true;
        this._textBoxSample.Size = new Drawing.Size( 192, 20 );
        this._textBoxSample.TabIndex = 8;
        this._textBoxSample.Text = "";
        // 
        // numericUpDownYValue
        // 
        this._numericUpDownYValue.CausesValidation = false;
        this._numericUpDownYValue.Location = new Drawing.Point( 112, 104 );
        this._numericUpDownYValue.Maximum = new decimal( [
                                                                            9,
                                                                            0,
                                                                            0,
                                                                            0] );
        this._numericUpDownYValue.Name = "numericUpDownYValue";
        this._numericUpDownYValue.Size = new Drawing.Size( 64, 20 );
        this._numericUpDownYValue.TabIndex = 10;
        this._numericUpDownYValue.ValueChanged += new EventHandler( this.NumericUpDownYValue_ValueChanged );
        // 
        // labelYValue
        // 
        this._labelYValue.Location = new Drawing.Point( 8, 104 );
        this._labelYValue.Name = "labelYValue";
        this._labelYValue.Size = new Drawing.Size( 96, 23 );
        this._labelYValue.TabIndex = 9;
        this._labelYValue.Text = SR.LabelKeyYValueIndex;
        this._labelYValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // comboBoxFormat
        // 
        this._comboBoxFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this._comboBoxFormat.Items.AddRange( [
                                                            SR.DescriptionTypeNone,
                                                            SR.DescriptionNumberFormatTypeCurrency,
                                                            SR.DescriptionNumberFormatTypeDecimal,
                                                            SR.DescriptionNumberFormatTypeScientific,
                                                            SR.DescriptionNumberFormatTypeFixedPoint,
                                                            SR.DescriptionNumberFormatTypeGeneral,
                                                            SR.DescriptionNumberFormatTypeNumber,
                                                            SR.DescriptionNumberFormatTypePercent,
                                                            SR.DescriptionTypeCustom] );

        this._comboBoxFormat.Location = new Drawing.Point( 112, 24 );
        this._comboBoxFormat.MaxDropDownItems = 10;
        this._comboBoxFormat.Name = "comboBoxFormat";
        this._comboBoxFormat.Size = new Drawing.Size( 192, 21 );
        this._comboBoxFormat.TabIndex = 1;
        this._comboBoxFormat.SelectedIndexChanged += new EventHandler( this.ComboBoxFormat_SelectedIndexChanged );
        // 
        // labelPrecision
        // 
        this._labelPrecision.Location = new Drawing.Point( 8, 48 );
        this._labelPrecision.Name = "labelPrecision";
        this._labelPrecision.Size = new Drawing.Size( 96, 23 );
        this._labelPrecision.TabIndex = 2;
        this._labelPrecision.Text = SR.LabelKeyPrecision;
        this._labelPrecision.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // labelFormat
        // 
        this._labelFormat.Location = new Drawing.Point( 8, 24 );
        this._labelFormat.Name = "labelFormat";
        this._labelFormat.Size = new Drawing.Size( 96, 23 );
        this._labelFormat.TabIndex = 0;
        this._labelFormat.Text = SR.LabelKeyFormat;
        this._labelFormat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // labelCustomFormat
        // 
        this._labelCustomFormat.Location = new Drawing.Point( 8, 48 );
        this._labelCustomFormat.Name = "labelCustomFormat";
        this._labelCustomFormat.Size = new Drawing.Size( 96, 23 );
        this._labelCustomFormat.TabIndex = 4;
        this._labelCustomFormat.Text = SR.LabelKeyCustomFormat;
        this._labelCustomFormat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        this._labelCustomFormat.Visible = false;
        // 
        // textBoxCustomFormat
        // 
        this._textBoxCustomFormat.Location = new Drawing.Point( 112, 48 );
        this._textBoxCustomFormat.Name = "textBoxCustomFormat";
        this._textBoxCustomFormat.Size = new Drawing.Size( 192, 20 );
        this._textBoxCustomFormat.TabIndex = 5;
        this._textBoxCustomFormat.Text = "";
        this._textBoxCustomFormat.Visible = false;
        this._textBoxCustomFormat.TextChanged += new EventHandler( this.TextBoxCustomFormat_TextChanged );
        // 
        // KeywordEditor
        // 
        this.AcceptButton = this._buttonOk;
        this.AutoScaleBaseSize = new Drawing.Size( 5, 13 );
        this.CancelButton = this._buttonCancel;
        this.ClientSize = new Drawing.Size( 578, 295 );
        this.Controls.AddRange( [
                                                                      this._groupBoxFormat,
                                                                      this._buttonCancel,
                                                                      this._buttonOk,
                                                                      this._groupBoxDescription,
                                                                      this._groupBoxKeywords] );
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        this.MaximizeBox = false;
        this.MinimizeBox = false;
        this.Name = "KeywordEditor";
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = SR.LabelKeywordEditor;
        this.Load += new EventHandler( this.KeywordEditor_Load );
        this._groupBoxKeywords.ResumeLayout( false );
        this._groupBoxDescription.ResumeLayout( false );
        this._groupBoxFormat.ResumeLayout( false );
        (( System.ComponentModel.ISupportInitialize ) this._numericUpDownYValue).EndInit();
        this.ResumeLayout( false );

    }
    #endregion

    #region " event handlers "

    /// <summary>
    /// Form loaded event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void KeywordEditor_Load( object sender, EventArgs e )
    {
        // Set restriction on the Y Value index editor
        if ( this._maxYValueIndex is >= 0 and < 10 )
        {
            this._numericUpDownYValue.Maximum = this._maxYValueIndex;
        }
        this._numericUpDownYValue.Enabled = this._maxYValueIndex > 0;
        this._labelYValue.Enabled = this._maxYValueIndex > 0;

        // Set tooltip for custom format
        this._toolTip.SetToolTip( this._textBoxCustomFormat, SR.DescriptionToolTipCustomFormatCharacters );

        // Select format None
        this._comboBoxFormat.SelectedIndex = 0;

        // Fill list of applicable keywords
        if ( this._applicableKeywords is not null )
        {
            foreach ( KeywordInfo keywordInfo in this._applicableKeywords )
            {
                _ = this._listBoxKeywords.Items.Add( keywordInfo );
            }
        }

        // Check if keyword for editing was specified
        if ( this.Keyword.Length == 0 )
        {
            this._listBoxKeywords.SelectedIndex = 0;
            this._comboBoxFormat.SelectedIndex = 0;
        }
        else
        {
            // Iterate through all keywords and find a match
            bool itemFound = false;
            foreach ( KeywordInfo keywordInfo in this._applicableKeywords )
            {
                // Iterate through all possible keyword names
                string[] keywordNames = keywordInfo.GetKeywords();
                foreach ( string keywordName in keywordNames )
                {
                    if ( this.Keyword.StartsWith( keywordName, StringComparison.Ordinal ) )
                    {
                        // Select keyword in the list
                        this._listBoxKeywords.SelectedItem = keywordInfo;
                        int keywordLength = keywordName.Length;

                        // Check if keyword support multiple Y values
                        if ( keywordInfo.SupportsValueIndex )
                        {
                            if ( this.Keyword.Length > keywordLength &&
                                this.Keyword[keywordLength] == 'Y' )
                            {
                                ++keywordLength;
                                if ( this.Keyword.Length > keywordLength &&
                                    char.IsDigit( this.Keyword[keywordLength] ) )
                                {
                                    int yValueIndex = int.Parse( this.Keyword.Substring( keywordLength, 1 ), CultureInfo.InvariantCulture );
                                    if ( yValueIndex < 0 || yValueIndex > this._maxYValueIndex )
                                    {
                                        yValueIndex = 0;
                                    }
                                    this._numericUpDownYValue.Value = yValueIndex;
                                    ++keywordLength;
                                }
                            }
                        }

                        // Check if keyword support format string
                        if ( keywordInfo.SupportsFormatting )
                        {
                            if ( this.Keyword.Length > keywordLength &&
                                this.Keyword[keywordLength] == '{' &&
                                this.Keyword.EndsWith( "}", StringComparison.OrdinalIgnoreCase ) )
                            {
                                // Get format string
                                string format = this.Keyword.Substring( keywordLength + 1, this.Keyword.Length - keywordLength - 2 );

                                if ( format.Length == 0 )
                                {
                                    // Select format None
                                    this._comboBoxFormat.SelectedIndex = 0;
                                }
                                else
                                {
                                    // Check if format string is custom
                                    if ( format.Length == 1 ||
                                        (format.Length == 2 && char.IsDigit( format[1] )) ||
                                        (format.Length == 3 && char.IsDigit( format[2] )) )
                                    {
                                        if ( format[0] == 'C' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 1;
                                        }
                                        else if ( format[0] == 'D' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 2;
                                        }
                                        else if ( format[0] == 'E' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 3;
                                        }
                                        else if ( format[0] == 'F' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 4;
                                        }
                                        else if ( format[0] == 'G' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 5;
                                        }
                                        else if ( format[0] == 'N' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 6;
                                        }
                                        else if ( format[0] == 'P' )
                                        {
                                            this._comboBoxFormat.SelectedIndex = 7;
                                        }
                                        else
                                        {
                                            // Custom format
                                            this._comboBoxFormat.SelectedIndex = 8;
                                            this._textBoxCustomFormat.Text = format;
                                        }

                                        // Get precision
                                        if ( this._comboBoxFormat.SelectedIndex != 8 && format.Length > 0 )
                                        {
                                            this._textBoxPrecision.Text = format[1..];
                                        }
                                    }
                                    else
                                    {
                                        // Custom format
                                        this._comboBoxFormat.SelectedIndex = 8;
                                        this._textBoxCustomFormat.Text = format;
                                    }
                                }
                            }
                        }

                        // Stop iteration
                        itemFound = true;
                        break;
                    }
                }

                // Break from the keywords loop
                if ( itemFound )
                {
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Selected format changed event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void ComboBoxFormat_SelectedIndexChanged( object sender, EventArgs e )
    {
        // Format disabled
        this._labelCustomFormat.Enabled = this._comboBoxFormat.SelectedIndex > 0;
        this._textBoxCustomFormat.Enabled = this._comboBoxFormat.SelectedIndex > 0;
        this._labelPrecision.Enabled = this._comboBoxFormat.SelectedIndex > 0;
        this._textBoxPrecision.Enabled = this._comboBoxFormat.SelectedIndex > 0;
        this._labelSample.Enabled = this._comboBoxFormat.SelectedIndex > 0;
        this._textBoxSample.Enabled = this._comboBoxFormat.SelectedIndex > 0;

        // Hide show form control depending on the format selection
        bool customFormat = ( string ) this._comboBoxFormat.SelectedItem == "Custom";
        this._labelCustomFormat.Visible = customFormat;
        this._textBoxCustomFormat.Visible = customFormat;
        this._labelPrecision.Visible = !customFormat;
        this._textBoxPrecision.Visible = !customFormat;

        // Update format sample
        this.UpdateNumericSample();
    }

    /// <summary>
    /// Selected keyword changed event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void ListBoxKeywords_SelectedIndexChanged( object sender, EventArgs e )
    {
        // Get selected keyword
        if ( this._listBoxKeywords.SelectedItem is KeywordInfo keywordInfo )
        {
            // Show description of the selected keyword
            this._labelDescription.Text = keywordInfo.Description.Replace( "\\n", "\n" );

            // Check if keyword support value formatting
            this._groupBoxFormat.Enabled = keywordInfo.SupportsFormatting;

            // Check if keyword support Y value index
            this._labelYValue.Enabled = keywordInfo.SupportsValueIndex;
            this._numericUpDownYValue.Enabled = keywordInfo.SupportsValueIndex && this._maxYValueIndex > 0;
            this._labelYValue.Enabled = keywordInfo.SupportsValueIndex && this._maxYValueIndex > 0;
        }

    }

    /// <summary>
    /// Keyword double click event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void ListBoxKeywords_DoubleClick( object sender, EventArgs e )
    {
        // Simulate accept button click when user double clicks in the list
        this.AcceptButton.PerformClick();
    }

    /// <summary>
    /// Precision text changed event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void TextBoxPrecision_TextChanged( object sender, EventArgs e )
    {
        MessageBoxOptions messageBoxOptions = 0;
        if ( this.RightToLeft == System.Windows.Forms.RightToLeft.Yes )
        {
            messageBoxOptions = MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading;
        }

        if ( this._textBoxPrecision.Text.Length >= 1 && !char.IsDigit( this._textBoxPrecision.Text[0] ) )
        {
            _ = MessageBox.Show( this, SR.MessagePrecisionInvalid, SR.MessageChartTitle, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, messageBoxOptions );
            this._textBoxPrecision.Text = "";
        }
        else if ( this._textBoxPrecision.Text.Length >= 2 && (!char.IsDigit( this._textBoxPrecision.Text[0] ) || !char.IsDigit( this._textBoxPrecision.Text[1] )) )
        {
            _ = MessageBox.Show( this, SR.MessagePrecisionInvalid, SR.MessageChartTitle, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, messageBoxOptions );
            this._textBoxPrecision.Text = "";
        }

        this.UpdateNumericSample();
    }

    /// <summary>
    /// Custom format text changed event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void TextBoxCustomFormat_TextChanged( object sender, EventArgs e )
    {
        this.UpdateNumericSample();
    }

    /// <summary>
    /// Ok button click event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void ButtonOk_Click( object sender, EventArgs e )
    {
        // Generate new keyword
        this.Keyword = string.Empty;

        // Get selected keyword
        if ( this._listBoxKeywords.SelectedItem is KeywordInfo keywordInfo )
        {
            this.Keyword = keywordInfo.Keyword;

            if ( keywordInfo.SupportsValueIndex &&
                ( int ) this._numericUpDownYValue.Value > 0 )
            {
                this.Keyword += "Y" + (( int ) this._numericUpDownYValue.Value).ToString( CultureInfo.InvariantCulture );
            }
            if ( keywordInfo.SupportsFormatting &&
                this._comboBoxFormat.SelectedIndex > 0 &&
                this.GetFormatString().Length > 0 )
            {
                this.Keyword += "{" + this.GetFormatString() + "}";
            }
        }
    }

    /// <summary>
    /// Y Value index changed event handler.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event arguments.</param>
    private void NumericUpDownYValue_ValueChanged( object sender, EventArgs e )
    {
        if ( this._numericUpDownYValue.Value > this._maxYValueIndex && this._numericUpDownYValue.Value < 0 )
        {
            MessageBoxOptions messageBoxOptions = 0;
            if ( this.RightToLeft == System.Windows.Forms.RightToLeft.Yes )
            {
                messageBoxOptions = MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading;
            }

            _ = MessageBox.Show( this, SR.MessageYValueIndexInvalid( this._maxYValueIndex.ToString( CultureInfo.CurrentCulture ) ), SR.MessageChartTitle,
                MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, messageBoxOptions );

            this._numericUpDownYValue.Value = 0;
        }
    }

    #endregion // Event Handlers

    #region " helper methods "

    /// <summary>
    /// Gets current format string
    /// </summary>
    /// <returns></returns>
    private string GetFormatString()
    {
        string formatString = string.Empty;
        if ( this._comboBoxFormat.Enabled &&
            this._comboBoxFormat.SelectedIndex == 1 )
        {
            formatString = "C" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 2 )
        {
            formatString = "D" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 3 )
        {
            formatString = "E" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 4 )
        {
            formatString = "G" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 5 )
        {
            formatString = "G" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 6 )
        {
            formatString = "N" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 7 )
        {
            formatString = "P" + this._textBoxPrecision.Text;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 8 )
        {
            formatString = this._textBoxCustomFormat.Text;
        }

        return formatString;
    }

    /// <summary>
    /// Updates numeric sample on the form.
    /// </summary>
    private void UpdateNumericSample()
    {
        string formatString = this.GetFormatString();
        if ( this._comboBoxFormat.SelectedIndex == 0 )
        {
            // No format
            this._textBoxSample.Text = string.Empty;
        }
        else if ( this._comboBoxFormat.SelectedIndex == 1 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.6789 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 2 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 3 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.6789 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 4 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.6789 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 5 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.6789 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 6 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.6789 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 7 )
        {
            this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 0.126 );
        }
        else if ( this._comboBoxFormat.SelectedIndex == 8 )
        {
            // Custom format
            bool success = false;
            try
            {
                this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345.67890 );
                success = true;
            }
            catch ( FormatException )
            {
            }

            if ( !success )
            {
                try
                {
                    this._textBoxSample.Text = string.Format( CultureInfo.CurrentCulture, "{0:" + formatString + "}", 12345 );
                    success = true;
                }
                catch ( FormatException )
                {
                }
            }

            if ( !success )
            {
                this._textBoxSample.Text = SR.DesciptionCustomLabelFormatInvalid;
            }
        }
    }
    /// <summary>
    /// VSTS: 787936, 787937 - Expand the dialog with widthDialogExtend, heightDialogExtend  to make room for localization.
    /// </summary>
    private void PrepareControlsLayout()
    {
        this.Width += widthDialogExtend;
        this._buttonOk.Left += widthDialogExtend;
        this._buttonCancel.Left += widthDialogExtend;
        this._groupBoxDescription.Width += widthDialogExtend;
        this._groupBoxFormat.Width += widthDialogExtend;
        this._labelDescription.Width += widthDialogExtend;
        foreach ( Control ctrl in this._groupBoxFormat.Controls )
        {
            if ( ctrl is Label )
                ctrl.Width += widthDialogExtend;
            else
                ctrl.Left += widthDialogExtend;
        }

        this.Height += heightDialogExtend;
        this._buttonOk.Top += heightDialogExtend;
        this._buttonCancel.Top += heightDialogExtend;
        this._groupBoxKeywords.Height += heightDialogExtend;
        this._listBoxKeywords.IntegralHeight = false;
        this._listBoxKeywords.Height += heightDialogExtend;
        this._groupBoxDescription.Height += heightDialogExtend;
        this._labelDescription.Height += heightDialogExtend;
        this._groupBoxFormat.Top += heightDialogExtend;
    }

    #endregion // Helper Methods
}


