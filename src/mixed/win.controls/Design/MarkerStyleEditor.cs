//
//  Purpose:	Design-time marker style editor class. 
//


using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms.DataVisualization.Charting;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// AxisName editor for the marker style.
/// Paints a rectangle with marker sample.
/// </summary>
internal class MarkerStyleEditor : UITypeEditor, IDisposable
{
    #region " editor method and properties "

#pragma warning disable IDE1006 // Naming Styles
    private ChartGraphics _chartGraph;
    private bool _disposed;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <returns>Can paint values.</returns>
    public override bool GetPaintValueSupported( ITypeDescriptorContext context )
    {
        return true;
    }

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="e">Paint value event arguments.</param>
    public override void PaintValue( PaintValueEventArgs e )
    {
        if ( e.Value is MarkerStyle )
        {
            // Create chart graphics object
            this._chartGraph ??= new ChartGraphics( null );
            this._chartGraph.Graphics = e.Graphics;

            // Get marker properties
            DataPointCustomProperties attributes = null;
            if ( e.Context != null && e.Context.Instance is not null )
            {
                // Check if several object selected
                object attrObject = e.Context.Instance;
                if ( e.Context.Instance is Array )
                {
                    Array array = ( Array ) e.Context.Instance;
                    if ( array.Length > 0 )
                    {
                        attrObject = array.GetValue( 0 );
                    }
                }

                // Check what kind of object is selected
                if ( attrObject is Series )
                {
                    attributes = ( DataPointCustomProperties ) attrObject;
                }
                else if ( attrObject is DataPoint )
                {
                    attributes = ( DataPointCustomProperties ) attrObject;
                }
                else if ( attrObject is DataPointCustomProperties )
                {
                    attributes = ( DataPointCustomProperties ) attrObject;
                }
                else if ( attrObject is LegendItem )
                {
                    attributes = new DataPointCustomProperties
                    {
                        MarkerColor = (( LegendItem ) attrObject).markerColor,
                        MarkerBorderColor = (( LegendItem ) attrObject).markerBorderColor,
                        MarkerSize = (( LegendItem ) attrObject).markerSize
                    };
                }
            }

            // Draw marker sample
            if ( attributes != null && ( MarkerStyle ) e.Value != MarkerStyle.None )
            {
                PointF point = new( e.Bounds.X + (e.Bounds.Width / 2F) - 0.5F, e.Bounds.Y + (e.Bounds.Height / 2F) - 0.5F );
                Color color = (attributes.MarkerColor == Color.Empty) ? Color.Black : attributes.MarkerColor;
                int size = attributes.MarkerSize;
                if ( size > (e.Bounds.Height - 4) )
                {
                    size = e.Bounds.Height - 4;
                }
                this._chartGraph.DrawMarkerAbs(
                    point,
                    ( MarkerStyle ) e.Value,
                    size,
                    color,
                    attributes.MarkerBorderColor,
                    attributes.MarkerBorderWidth,
                    "",
                    Color.Empty,
                    0,
                    Color.Empty,
                    RectangleF.Empty,
                    true );
            }
        }
    }

    #endregion

    #region " idisposable members "

    /// <summary>
    /// Finalizer for the MarkerStyleEditor, disposes any remaining
    /// resources if it has not already been disposed.
    /// </summary>
    ~MarkerStyleEditor()
    {
        this.Dispose( false );
    }

    /// <summary>
    /// Disposes resources used by this object.
    /// </summary>
    /// <param name="disposing">Whether this method was called form Dispose() or the finalizer.</param>
    protected virtual void Dispose( bool disposing )
    {
        if ( !this._disposed )
        {
            if ( disposing )
            {
                this._chartGraph.Dispose();
            }

            this._disposed = true;
        }
    }

    /// <summary>
    /// Disposes all resources used by this object
    /// </summary>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    #endregion
}
