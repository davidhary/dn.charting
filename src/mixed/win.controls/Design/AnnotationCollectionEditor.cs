//
//  Purpose:	Design-time UI editor for Annotations.
//


using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Windows.Forms.DataVisualization.Charting;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// Designer editor for the Annotation Collection.
/// </summary>
internal class AnnotationCollectionEditor : ChartCollectionEditor
{
    #region " methods "

    /// <summary>
    /// Object constructor.
    /// </summary>
    public AnnotationCollectionEditor()
        : base( typeof( AnnotationCollection ) )
    {
    }

    /// <summary>
    /// Gets the data types that this collection editor can contain.
    /// </summary>
    /// <returns>An array of data types that this collection can contain.</returns>
    protected override Type[] CreateNewItemTypes()
    {
        return [
            typeof(LineAnnotation),
            typeof(VerticalLineAnnotation),
            typeof(HorizontalLineAnnotation),
            typeof(TextAnnotation),
            typeof(RectangleAnnotation),
            typeof(EllipseAnnotation),
            typeof(ArrowAnnotation),
            typeof(Border3DAnnotation),
            typeof(CalloutAnnotation),
            typeof(PolylineAnnotation),
            typeof(PolygonAnnotation),
            typeof(ImageAnnotation),
            typeof(AnnotationGroup)
        ];
    }

    /// <summary>
    /// Create annotation instance in the editor 
    /// </summary>
    /// <param name="itemType">Item type.</param>
    /// <returns>Newly created item.</returns>
    protected override object CreateInstance( Type itemType )
    {
        Chart control = ( Chart ) GetChartReference( this.Context.Instance );

        // Call base class
        Annotation annotation = base.CreateInstance( itemType ) as Annotation;

        // Generate unique name 
        if ( control is not null )
        {
            annotation.Name = NextUniqueName( control, itemType );
        }

        return annotation;
    }



    /// <summary>
    /// Finds the unique name for a new annotation being added to the collection
    /// </summary>
    /// <param name="control">Chart control reference.</param>
    /// <param name="type">Type of the annotation added.</param>
    /// <returns>Next unique chart annotation name</returns>
    private static string NextUniqueName( Chart control, Type type )
    {
        // Find unique name
        string result = string.Empty;
        string prefix = type.Name;
        for ( int i = 1; i < int.MaxValue; i++ )
        {
            result = prefix + i.ToString( CultureInfo.InvariantCulture );

            // Check whether the name is unique
            if ( control.Annotations.IsUniqueName( result ) )
            {
                break;
            }
        }
        return result;
    }

    #endregion // Methods
}
/// <summary>
/// UI type editor for the annotation anchor point
/// </summary>
internal class AnchorPointUITypeEditor : UITypeEditor
{
    #region " editor methods and properties "

    /// <summary>
    /// Display a drop down list with check boxes.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <param name="provider">Provider.</param>
    /// <param name="value">Value to edit.</param>
    /// <returns>Result</returns>
    public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
    {
        if ( context != null && context.Instance != null && provider is not null )
        {
            IWindowsFormsEditorService edSvc = ( IWindowsFormsEditorService ) provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc != null &&
                context.Instance is Annotation )
            {
                // Create control for editing
                AnchorPointNameTreeView control = new(
                    edSvc,
                    ( Annotation ) context.Instance,
                    value as DataPoint );

                // Show drop down control
                edSvc.DropDownControl( control );

                // Get new enumeration value
                value = control.GetNewValue();

                // Dispose control
                control.Dispose();
            }
        }

        return value;
    }

    /// <summary>
    /// Gets editing style.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <returns>Editor style.</returns>
    public override UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
    {
        return context != null && context.Instance != null ? UITypeEditorEditStyle.DropDown : base.GetEditStyle( context );
    }

    #endregion
}
/// <summary>
/// Anchor data points name tree view, which is used for the UI type editing.
/// </summary>
internal class AnchorPointNameTreeView : TreeView
{
    #region " control fields "

#pragma warning disable IDE1006 // Naming Styles

    // Annotation object to edit
    private readonly Annotation _annotation;
    private readonly DataPoint _dataPoint;
    private readonly IWindowsFormsEditorService _edSvc;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " control constructor "

    /// <summary>
    /// Public constructor.
    /// </summary>
    /// <param name="edSvc">Editor service.</param>
    /// <param name="annotation">Annotation to edit.</param>
    /// <param name="dataPoint">Annotation anchor data point to edit.</param>
    public AnchorPointNameTreeView(
        IWindowsFormsEditorService edSvc,
        Annotation annotation,
        DataPoint dataPoint )
    {
        // Set editable value
        this._annotation = annotation;
        this._dataPoint = dataPoint;
        this._edSvc = edSvc;

        // Set control border style
        this.BorderStyle = BorderStyle.None;

        // Fill tree with data point names
        this.FillTree();
    }

    #endregion

    #region " control methods "

    /// <summary>
    /// Fills data points name tree.
    /// </summary>
    private void FillTree()
    {
        bool nodeSelected = false;
        this.BeginUpdate();

        // Add "None" option
        TreeNode noPoint = this.Nodes.Add( "NotSet" );

        // Get chart object
        if ( this._annotation != null &&
            this._annotation.AnnotationGroup == null &&
            this._annotation.Chart is not null )
        {
            Chart chart = this._annotation.Chart;

            // Loop through all series
            foreach ( Series series in chart.Series )
            {
                TreeNode seriesNode = this.Nodes.Add( series.Name );
                seriesNode.Tag = series;

                // Indicate that there are no points in series
                if ( series.Points.Count == 0 )
                {
                    _ = seriesNode.Nodes.Add( "(empty)" );
                }

                // Loop through all points
                int index = 1;
                foreach ( DataPoint point in series.Points )
                {
                    TreeNode dataPointNode = seriesNode.Nodes.Add( "DataPoint" + index.ToString( System.Globalization.CultureInfo.InvariantCulture ) );
                    dataPointNode.Tag = point;
                    ++index;

                    // Check if this node should be selected
                    if ( point == this._dataPoint )
                    {
                        seriesNode.Expand();
                        this.SelectedNode = dataPointNode;
                        nodeSelected = true;
                    }
                }
            }
        }

        // Select default node
        if ( !nodeSelected )
        {
            this.SelectedNode = noPoint;
        }

        this.EndUpdate();
    }

    /// <summary>
    /// Gets new data point.
    /// </summary>
    /// <returns>New enum value.</returns>
    public DataPoint GetNewValue()
    {
        return this.SelectedNode != null &&
            this.SelectedNode.Tag != null &&
            this.SelectedNode.Tag is DataPoint
            ? ( DataPoint ) this.SelectedNode.Tag
            : null;
    }

    /// <summary>
    /// Mouse double clicked.
    /// </summary>
    protected override void OnDoubleClick( EventArgs e )
    {
        base.OnDoubleClick( e );
        if ( this._edSvc is not null )
        {
            if ( this.GetNewValue() is not null )
            {
                this._edSvc.CloseDropDown();
            }
            else if ( this.SelectedNode != null &&
                this.SelectedNode.Text == "NotSet" )
            {
                this._edSvc.CloseDropDown();
            }
        }
    }
    #endregion
}
/// <summary>
/// UI type editor for the annotation axes.
/// </summary>
internal class AnnotationAxisUITypeEditor : UITypeEditor
{
    #region " editor methods and properties "

    /// <summary>
    /// Display a drop down list with check boxes.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <param name="provider">Provider.</param>
    /// <param name="value">Value to edit.</param>
    /// <returns>Result</returns>
    public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
    {
        if ( context != null && context.Instance != null && provider is not null )
        {
            IWindowsFormsEditorService edSvc = ( IWindowsFormsEditorService ) provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc != null &&
                context.Instance is Annotation )
            {
                // Check if we dealing with X or Y axis
                bool showXAxes = true;
                if ( context.PropertyDescriptor != null &&
                    context.PropertyDescriptor.Name == "AxisY" )
                {
                    showXAxes = false;
                }

                // Create control for editing
                AnnotationAxisNameTreeView control = new(
                    edSvc,
                    ( Annotation ) context.Instance,
                    value as Axis,
                    showXAxes );

                // Show drop down control
                edSvc.DropDownControl( control );

                // Get new enumeration value
                value = control.GetNewValue();

                // Dispose control
                control.Dispose();
            }
        }

        return value;
    }

    /// <summary>
    /// Gets editing style.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <returns>Editor style.</returns>
    public override UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
    {
        return context != null && context.Instance != null ? UITypeEditorEditStyle.DropDown : base.GetEditStyle( context );
    }

    #endregion
}
/// <summary>
/// Annotation axes names tree view, which is used for the UI type editing.
/// </summary>
internal class AnnotationAxisNameTreeView : TreeView
{
    #region " control fields "

#pragma warning disable IDE1006 // Naming Styles

    // Annotation object to edit
    private readonly Annotation _annotation;
    private readonly Axis _axis;
    private readonly IWindowsFormsEditorService _edSvc;
    private readonly bool _showXAxes = true;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " control constructor "

    /// <summary>
    /// Public constructor.
    /// </summary>
    /// <param name="edSvc">Editor service.</param>
    /// <param name="annotation">Annotation to edit.</param>
    /// <param name="axis">Axis object.</param>
    /// <param name="showXAxes">Indicates if X or Y axis should be shown.</param>
    public AnnotationAxisNameTreeView(
        IWindowsFormsEditorService edSvc,
        Annotation annotation,
        Axis axis,
        bool showXAxes )
    {
        // Set editable value
        this._annotation = annotation;
        this._axis = axis;
        this._edSvc = edSvc;
        this._showXAxes = showXAxes;

        // Set control border style
        this.BorderStyle = BorderStyle.None;

        // Fill tree with data point names
        this.FillTree();
    }

    #endregion

    #region " control methods "

    /// <summary>
    /// Fills data points name tree.
    /// </summary>
    private void FillTree()
    {
        bool nodeSelected = false;
        this.BeginUpdate();

        // Add "None" option
        TreeNode noPoint = this.Nodes.Add( "NotSet" );

        // Get chart object
        if ( this._annotation != null &&
            this._annotation.AnnotationGroup == null &&
            this._annotation.Chart is not null )
        {
            Chart chart = this._annotation.Chart;

            // Loop through all chart areas
            foreach ( ChartArea chartArea in chart.ChartAreas )
            {
                TreeNode areaNode = this.Nodes.Add( chartArea.Name );
                areaNode.Tag = chartArea;

                // Loop through all axes
                foreach ( Axis curAxis in chartArea.Axes )
                {
                    // Hide X or Y axes
                    if ( curAxis.AxisName is AxisName.Y or AxisName.Y2 )
                    {
                        if ( this._showXAxes )
                        {
                            continue;
                        }
                    }
                    if ( curAxis.AxisName is AxisName.X or AxisName.X2 )
                    {
                        if ( !this._showXAxes )
                        {
                            continue;
                        }
                    }

                    // Create child node
                    TreeNode axisNode = areaNode.Nodes.Add( curAxis.Name );
                    axisNode.Tag = curAxis;

                    // Check if this node should be selected
                    if ( this._axis == curAxis )
                    {
                        areaNode.Expand();
                        this.SelectedNode = axisNode;
                        nodeSelected = true;
                    }
                }
            }
        }

        // Select default node
        if ( !nodeSelected )
        {
            this.SelectedNode = noPoint;
        }

        this.EndUpdate();
    }

    /// <summary>
    /// Gets new data point.
    /// </summary>
    /// <returns>New enum value.</returns>
    public Axis GetNewValue()
    {
        return this.SelectedNode != null &&
            this.SelectedNode.Tag != null &&
            this.SelectedNode.Tag is Axis
            ? ( Axis ) this.SelectedNode.Tag
            : null;
    }

    /// <summary>
    /// Mouse double clicked.
    /// </summary>
    protected override void OnDoubleClick( EventArgs e )
    {
        base.OnDoubleClick( e );
        if ( this._edSvc is not null )
        {
            if ( this.GetNewValue() is not null )
            {
                this._edSvc.CloseDropDown();
            }
            else if ( this.SelectedNode != null &&
                this.SelectedNode.Text == "NotSet" )
            {
                this._edSvc.CloseDropDown();
            }
        }
    }
    #endregion
}
