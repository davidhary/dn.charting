//
//  Purpose:	Design-time editor for the enumerations with Flags attribute.
//				Editor displays the drop down list with check marks.
//


using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.DataVisualization.Charting;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// UI type editor for the enumerations with Flags attribute
/// </summary>
internal class FlagsEnumUITypeEditor : UITypeEditor
{
    #region " constructor "

#pragma warning disable IDE1006 // Naming Styles
    /// <summary>
    /// Enumeration type.
    /// </summary>
    private Type _enumType;
#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " editor methods and properties "

#pragma warning disable IDE1006 // Naming Styles
    private IWindowsFormsEditorService _edSvc;
#pragma warning restore IDE1006 // Naming Styles

    /// <summary>
    /// Display a drop down list with check boxes.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <param name="provider">Provider.</param>
    /// <param name="value">Value to edit.</param>
    /// <returns>Result</returns>
    public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
    {
        if ( context != null && context.Instance != null && provider is not null )
        {
            this._edSvc = ( IWindowsFormsEditorService ) provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( this._edSvc is not null )
            {
                // Get enum type
                if ( value is not null )
                {
                    this._enumType = value.GetType();
                }
                else if ( context != null && context.PropertyDescriptor is not null )
                {
                    this._enumType = context.PropertyDescriptor.PropertyType;
                }

                if ( this._enumType is not null )
                {
                    // Create control for editing
                    FlagsEnumCheckedListBox control = new( value, this._enumType );

                    // Show drop down control
                    this._edSvc.DropDownControl( control );

                    // Get new enumeration value
                    value = control.GetNewValue();
                }
            }
        }

        return value;
    }

    /// <summary>
    /// Gets editing style.
    /// </summary>
    /// <param name="context">Editing context.</param>
    /// <returns>Editor style.</returns>
    public override UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
    {
        return context != null && context.Instance != null ? UITypeEditorEditStyle.DropDown : base.GetEditStyle( context );
    }

    #endregion
}
/// <summary>
/// Checked list box, which is used for the UI type editing.
/// </summary>
internal class FlagsEnumCheckedListBox : CheckedListBox
{
    #region " control fields "

#pragma warning disable IDE1006 // Naming Styles

    // Enumeration object to edit
    private readonly object _editValue;

    // Enumeration type to edit
    private readonly Type _editType;

#pragma warning restore IDE1006 // Naming Styles

    #endregion

    #region " control constructor "

    /// <summary>
    /// Public constructor.
    /// </summary>
    /// <param name="editValue">Value to edit.</param>
    /// <param name="editType">Typpe to edit.</param>
    public FlagsEnumCheckedListBox( object editValue, Type editType )
    {
        // Set editable value
        this._editValue = editValue;
        this._editType = editType;

        // Set control border style
        this.BorderStyle = System.Windows.Forms.BorderStyle.None;

        // Fill enum items list
        this.FillList();
    }

    #endregion

    #region " control methods "

    /// <summary>
    /// Fills checked list items
    /// </summary>
    private void FillList()
    {
        // Check if editable object is of type Enum
        if ( !this._editType.IsEnum )
        {
            throw new ArgumentException( SR.ExceptionEditorUITypeEditorInapplicable );
        }

        // Check underlying type
        if ( Enum.GetUnderlyingType( this._editType ) != typeof( int ) )
        {
            throw new ArgumentException( SR.ExceptionEditorUITypeEditorInt32ApplicableOnly );
        }

        // Convert enumeration value to Int32
        int editValueInt32 = 0;
        if ( this._editValue is not null )
        {
            editValueInt32 = ( int ) this._editValue;
        }

        // Fill list with all possible values in the enumeration
        foreach ( object enumValue in Enum.GetValues( this._editType ) )
        {
            // Add items into list except the enum value zero
            int currentValueInt32 = ( int ) enumValue;
            if ( currentValueInt32 != 0 )
            {
                bool isChecked = (editValueInt32 & currentValueInt32) == currentValueInt32;
                _ = this.Items.Add( Enum.GetName( this._editType, enumValue ), isChecked );
            }
        }
    }

    /// <summary>
    /// Gets new enumeration value.
    /// </summary>
    /// <returns>New enum value.</returns>
    public object GetNewValue()
    {
        // Update enumeration flags
        int editValueInt32 = 0;
        foreach ( object checkedItem in this.CheckedItems )
        {
            int currentValueInt32 = ( int ) Enum.Parse( this._editType, ( string ) checkedItem );
            editValueInt32 |= currentValueInt32;
        }

        // Return enumeration value
        return Enum.ToObject( this._editType, editValueInt32 );
    }

    #endregion
}

