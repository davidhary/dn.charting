//
//  Purpose:	Design-time classes for the image maps.
//


using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.DataVisualization.Charting.Utilities;

namespace System.Windows.Forms.Design.DataVisualization.Charting;
/// <summary>
/// Image string editor class.
/// </summary>
internal class ImageValueEditor : FileNameEditor
{
    #region " editor method and properties "

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="context">Descriptor context.</param>
    /// <returns>Can paint values.</returns>
    public override bool GetPaintValueSupported( ITypeDescriptorContext context )
    {
        return true;
    }

    /// <summary>
    /// Override this function to support palette colors drawing
    /// </summary>
    /// <param name="e">Paint value event arguments.</param>
    public override void PaintValue( PaintValueEventArgs e )
    {
        try
        {
            if ( e.Value is string @string )
            {
                // Get image loader 
                ImageLoader imageLoader = null;
                if ( e.Context != null && e.Context.Instance is not null )
                {
                    if ( e.Context.Instance is Chart chart )
                    {
                        imageLoader = ( ImageLoader ) chart.GetService( typeof( ImageLoader ) );
                    }
                    else if ( e.Context.Instance is IChartElement chartElement )
                    {
                        imageLoader = chartElement.Common.ImageLoader;
                    }
                }

                if ( imageLoader != null && !string.IsNullOrEmpty( @string ) )
                {
                    // Load a image
                    Drawing.Image image = imageLoader.LoadImage( @string );

                    // Draw Image
                    e.Graphics.DrawImage( image, e.Bounds );
                }
            }
        }
        catch ( ArgumentException )
        { }
    }
    #endregion
}
