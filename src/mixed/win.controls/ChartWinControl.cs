//
//  Purpose:	Main windows forms chart control class.
//

#pragma warning disable IDE0044 // Add readonly modifier
#pragma warning disable IDE1006 // Naming Styles
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Forms.DataVisualization.Charting.Borders3D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.DataVisualization.Charting.Data;
using System.Windows.Forms.DataVisualization.Charting.Formulas;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using System.Windows.Forms.Design.DataVisualization.Charting;

namespace System.Windows.Forms.DataVisualization.Charting;

#region " enumerations "
/// <summary>
/// Specifies the format of the image
/// </summary>
public enum ChartImageFormat
{
    /// <summary>
    /// Gets the Joint Photographic Experts Group (JPEG) image format.
    /// </summary>
    Jpeg,

    /// <summary>
    /// Gets the W3C Portable Network Graphics (PNG) image format.
    /// </summary>
    Png,

    /// <summary>
    /// Gets the bitmap image format (BMP).
    /// </summary>
    Bmp,

    /// <summary>
    /// Gets the Tag Image File Format (TIFF) image format.
    /// </summary>
    Tiff,

    /// <summary>
    /// Gets the Graphics Interchange Format (GIF) image format.
    /// </summary>
    Gif,

    /// <summary>
    /// Gets the Enhanced Meta File (Emf) image format.
    /// </summary>
    Emf,

    /// <summary>
    /// Enhanced Meta File (EmfDual) image format.
    /// </summary>
    EmfDual,

    /// <summary>
    /// Enhanced Meta File (Emf+) image format.
    /// </summary>
    EmfPlus,
}

#endregion
/// <summary>
/// Chart windows forms control
/// </summary>
[ToolboxBitmap( typeof( Chart ), "ChartControl.ico" )]
[SRDescription( "DescriptionAttributeChart_Chart" )]
[Designer( typeof( ChartWinDesigner ) )]
[DesignerSerializer( typeof( ChartWinDesignerSerializer ), typeof( CodeDomSerializer ) )]
[DisplayName( "Chart" )]
public class Chart : Control, ISupportInitialize
{
    #region " control fields "

    /// <summary>
    /// Determines whether or not to show debug markings in debug mode. For internal use.
    /// </summary>
    internal bool ShowDebugMarkings;

    // Chart services components
    private ChartTypeRegistry _chartTypeRegistry;
    private BorderTypeRegistry _borderTypeRegistry;
    private CustomPropertyRegistry _customAttributeRegistry;
    private DataManager _dataManager;
    internal ChartImage chartPicture;
    private ImageLoader _imageLoader;
    internal ServiceContainer serviceContainer;

    // Selection class
    internal Selection selection;


    // Formula registry service component
    private FormulaRegistry _formulaRegistry;


    // Indicates that control invalidation is temporary disabled
    internal bool disableInvalidates;

    // Indicates that chart is serializing the data
    internal bool serializing;

    // Detailed serialization status which allows not only to determine if serialization
    // is currently in process but also check if we are saving, loading or resetting the chart.
    internal SerializationStatus serializationStatus = SerializationStatus.None;

    // Bitmap used for double buffering chart painting
    internal Bitmap paintBufferBitmap;

    // Graphics of the double buffered bitmap
    internal Graphics paintBufferBitmapGraphics;

    // Indicates that only chart area cursor/selection must be drawn during the next paint event
    internal bool paintTopLevelElementOnly;

    // Indicates that some chart properties where changed (used for painting)
    internal bool dirtyFlag = true;


    // Chart default cursor
    internal Forms.Cursor defaultCursor = Cursors.Default;

    // Keywords registry
    private KeywordsRegistry _keywordsRegistry;

    // Horizontal rendering resolution.
    internal static double renderingDpiX = 96.0;


    #endregion

    #region " component designer generated code "
    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    [Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private void InitializeComponent()
    {
    }
    #endregion

    #region " control constructors "

    /// <summary>
    /// Chart control constructor.
    /// </summary>
    public Chart()
    {
        // ============================*
        // Check control license
        // ============================*

        // ============================***
        // Set control styles
        // ============================***
        this.SetStyle( ControlStyles.ResizeRedraw, true );
        //this.SetStyle(ControlStyles.Opaque, true);
        this.SetStyle( ControlStyles.UserPaint, true );
        this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
        this.SetStyle( ControlStyles.SupportsTransparentBackColor, true );
        this.SetStyle( ControlStyles.Selectable, true );

        // NOTE: Fixes issue #4475
        this.SetStyle( ControlStyles.DoubleBuffer, true );

        // This is necessary to raise focus event on chart mouse click.
        this.SetStyle( ControlStyles.UserMouse, true );

        // ============================***
        // Create services
        // ============================***
        this.serviceContainer = new ServiceContainer();
        this._chartTypeRegistry = new ChartTypeRegistry();
        this._borderTypeRegistry = new BorderTypeRegistry();
        this._customAttributeRegistry = new CustomPropertyRegistry();

        this._keywordsRegistry = new KeywordsRegistry();

        this._dataManager = new DataManager( this.serviceContainer );
        this._imageLoader = new ImageLoader( this.serviceContainer );

        this.chartPicture = new ChartImage( this.serviceContainer );
        this.Serializer = new ChartSerializer( this.serviceContainer );
        this.Printing = new PrintingManager( this.serviceContainer );
        this._formulaRegistry = new FormulaRegistry();

        // Add services to the service container
        this.serviceContainer.AddService( typeof( Chart ), this );                           // Chart Control
        this.serviceContainer.AddService( this._chartTypeRegistry.GetType(), this._chartTypeRegistry );// Chart types registry
        this.serviceContainer.AddService( this._borderTypeRegistry.GetType(), this._borderTypeRegistry );// Border types registry
        this.serviceContainer.AddService( this._customAttributeRegistry.GetType(), this._customAttributeRegistry );// Custom attribute registry
        this.serviceContainer.AddService( this._dataManager.GetType(), this._dataManager );            // Data Manager service
        this.serviceContainer.AddService( this._imageLoader.GetType(), this._imageLoader );            // Image Loader service
        this.serviceContainer.AddService( this.chartPicture.GetType(), this.chartPicture );            // Chart image service
        this.serviceContainer.AddService( this.Serializer.GetType(), this.Serializer );    // Chart serializer service
        this.serviceContainer.AddService( this.Printing.GetType(), this.Printing );    // Printing manager service
        this.serviceContainer.AddService( this._formulaRegistry.GetType(), this._formulaRegistry );    // Formula modules service
        this.serviceContainer.AddService( this._keywordsRegistry.GetType(), this._keywordsRegistry );  // Keywords registry

        // Initialize objects
        this._dataManager.Initialize();


        // Register known chart types
        this._chartTypeRegistry.Register( ChartTypeNames.Bar, typeof( BarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Column, typeof( ColumnChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Point, typeof( PointChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Bubble, typeof( BubbleChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Line, typeof( LineChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Spline, typeof( SplineChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.StepLine, typeof( StepLineChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Area, typeof( AreaChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.SplineArea, typeof( SplineAreaChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.StackedArea, typeof( StackedAreaChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Pie, typeof( PieChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Stock, typeof( StockChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Candlestick, typeof( CandleStickChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Doughnut, typeof( DoughnutChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.StackedBar, typeof( StackedBarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.StackedColumn, typeof( StackedColumnChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.OneHundredPercentStackedColumn, typeof( HundredPercentStackedColumnChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.OneHundredPercentStackedBar, typeof( HundredPercentStackedBarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.OneHundredPercentStackedArea, typeof( HundredPercentStackedAreaChart ) );



        this._chartTypeRegistry.Register( ChartTypeNames.Range, typeof( RangeChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.SplineRange, typeof( SplineRangeChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.RangeBar, typeof( RangeBarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Radar, typeof( RadarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.RangeColumn, typeof( RangeColumnChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.ErrorBar, typeof( ErrorBarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.BoxPlot, typeof( BoxPlotChart ) );



        this._chartTypeRegistry.Register( ChartTypeNames.Renko, typeof( RenkoChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.ThreeLineBreak, typeof( ThreeLineBreakChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Kagi, typeof( KagiChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.PointAndFigure, typeof( PointAndFigureChart ) );





        this._chartTypeRegistry.Register( ChartTypeNames.Polar, typeof( PolarChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.FastLine, typeof( FastLineChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Funnel, typeof( FunnelChart ) );
        this._chartTypeRegistry.Register( ChartTypeNames.Pyramid, typeof( PyramidChart ) );





        this._chartTypeRegistry.Register( ChartTypeNames.FastPoint, typeof( FastPointChart ) );



        // Register known formula modules
        this._formulaRegistry.Register( SR.FormulaNamePriceIndicators, typeof( PriceIndicators ) );
        this._formulaRegistry.Register( SR.FormulaNameGeneralTechnicalIndicators, typeof( GeneralTechnicalIndicators ) );
        this._formulaRegistry.Register( SR.FormulaNameTechnicalVolumeIndicators, typeof( VolumeIndicators ) );
        this._formulaRegistry.Register( SR.FormulaNameOscillator, typeof( Oscillators ) );
        this._formulaRegistry.Register( SR.FormulaNameGeneralFormulas, typeof( GeneralFormulas ) );
        this._formulaRegistry.Register( SR.FormulaNameTimeSeriesAndForecasting, typeof( TimeSeriesAndForecasting ) );
        this._formulaRegistry.Register( SR.FormulaNameStatisticalAnalysis, typeof( StatisticalAnalysis ) );



        // Register known 3D border types
        this._borderTypeRegistry.Register( "Emboss", typeof( EmbossBorder ) );
        this._borderTypeRegistry.Register( "Raised", typeof( RaisedBorder ) );
        this._borderTypeRegistry.Register( "Sunken", typeof( SunkenBorder ) );
        this._borderTypeRegistry.Register( "FrameThin1", typeof( FrameThin1Border ) );
        this._borderTypeRegistry.Register( "FrameThin2", typeof( FrameThin2Border ) );
        this._borderTypeRegistry.Register( "FrameThin3", typeof( FrameThin3Border ) );
        this._borderTypeRegistry.Register( "FrameThin4", typeof( FrameThin4Border ) );
        this._borderTypeRegistry.Register( "FrameThin5", typeof( FrameThin5Border ) );
        this._borderTypeRegistry.Register( "FrameThin6", typeof( FrameThin6Border ) );
        this._borderTypeRegistry.Register( "FrameTitle1", typeof( FrameTitle1Border ) );
        this._borderTypeRegistry.Register( "FrameTitle2", typeof( FrameTitle2Border ) );
        this._borderTypeRegistry.Register( "FrameTitle3", typeof( FrameTitle3Border ) );
        this._borderTypeRegistry.Register( "FrameTitle4", typeof( FrameTitle4Border ) );
        this._borderTypeRegistry.Register( "FrameTitle5", typeof( FrameTitle5Border ) );
        this._borderTypeRegistry.Register( "FrameTitle6", typeof( FrameTitle6Border ) );
        this._borderTypeRegistry.Register( "FrameTitle7", typeof( FrameTitle7Border ) );
        this._borderTypeRegistry.Register( "FrameTitle8", typeof( FrameTitle8Border ) );

        // Enable chart invalidating
        this.disableInvalidates = false;

        // Create selection object
        this.selection = new Selection( this.serviceContainer );

        // Create named images collection
        this.Images = [];

        // Hook up event handlers
        this.ChartAreas.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.Series.ChartAreaNameReferenceChanged );
        this.ChartAreas.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.Legends.ChartAreaNameReferenceChanged );
        this.ChartAreas.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.Titles.ChartAreaNameReferenceChanged );
        this.ChartAreas.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.Annotations.ChartAreaNameReferenceChanged );
        this.ChartAreas.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.ChartAreas.ChartAreaNameReferenceChanged );
        this.Legends.NameReferenceChanged += new EventHandler<NameReferenceChangedEventArgs>( this.Series.LegendNameReferenceChanged );
    }

    #endregion

    #region " control painting methods "

    /// <summary>
    /// Paint chart control.
    /// </summary>
    /// <param name="e">Paint event arguments.</param>
    protected override void OnPaint( PaintEventArgs e )
    {
        // ============================*
        // Check control license

        // Disable invalidates
        this.disableInvalidates = true;

        // ============================*
        // If chart background is transparent - draw without
        // double buffering.
        // ============================*
        if ( this.IsBorderTransparent() ||
            (!this.BackColor.IsEmpty &&
            (this.BackColor == Color.Transparent || this.BackColor.A != 255)) )
        {
            // Draw chart directly on the graphics
            try
            {
                if ( this.paintTopLevelElementOnly )
                {
                    this.chartPicture.Paint( e.Graphics, false );
                }
                this.chartPicture.Paint( e.Graphics, this.paintTopLevelElementOnly );
            }
            catch ( Exception )
            {
                // Draw exception method
                this.DrawException( e.Graphics );


                // Rethrow exception if not in design-time mode
                if ( !this.DesignMode )
                {
                    throw;
                }

            }

        }
        else
        {
            // ============================*
            // If nothing was changed in the chart and last image is stored in the buffer
            // there is no need to repaint the chart.
            // ============================*
            if ( this.dirtyFlag || this.paintBufferBitmap == null )
            {
                // Get scaling component from the drawing graphics
                float scaleX = e.Graphics.Transform.Elements[0];
                float scaleY = e.Graphics.Transform.Elements[3];

                // Create offscreen buffer bitmap
                if ( this.paintBufferBitmap == null ||
                    this.paintBufferBitmap.Width < scaleX * this.ClientRectangle.Width ||
                    this.paintBufferBitmap.Height < scaleY * this.ClientRectangle.Height )
                {
                    if ( this.paintBufferBitmap is not null )
                    {
                        this.paintBufferBitmap.Dispose();
                        this.paintBufferBitmapGraphics.Dispose();
                    }

                    // Create offscreen bitmap taking in consideration graphics scaling
                    this.paintBufferBitmap = new Bitmap( ( int ) (this.ClientRectangle.Width * scaleX), ( int ) (this.ClientRectangle.Height * scaleY), e.Graphics );
                    this.paintBufferBitmapGraphics = Graphics.FromImage( this.paintBufferBitmap );
                    this.paintBufferBitmapGraphics.ScaleTransform( scaleX, scaleY );
                }

                // ============================*
                // Draw chart in bitmap buffer
                // ============================*
                try
                {
                    this.chartPicture.Paint( this.paintBufferBitmapGraphics, this.paintTopLevelElementOnly );
                }
                catch ( Exception )
                {
                    // Draw exception method
                    this.DrawException( this.paintBufferBitmapGraphics );

                    // Rethrow exception if not in design-time mode
                    if ( !this.DesignMode )
                    {
                        throw;
                    }
                }
            }

            // ============================*
            // Push bitmap buffer forward into the screen
            // ============================*
            // Set drawing scale 1:1. Only persist the transformation from current matrix
            Drawing.Drawing2D.Matrix drawingMatrix = new();
            Drawing.Drawing2D.Matrix oldMatrix = e.Graphics.Transform;
            drawingMatrix.Translate( oldMatrix.OffsetX, oldMatrix.OffsetY );
            e.Graphics.Transform = drawingMatrix;

            // Draw image
            e.Graphics.DrawImage( this.paintBufferBitmap, 0, 0 );
            e.Graphics.Transform = oldMatrix;
        }

        // Clears control dirty flag
        this.dirtyFlag = false;
        this.disableInvalidates = false;

        // Call base class
        base.OnPaint( e );

        // ============================*
        // Check if smart client data must be loaded
        // ============================*
    }

    /// <summary>   Paints control background. </summary>
    /// <remarks>   2023-05-27. </remarks>
    /// <param name="pevent">    Paint event arguments. </param>
    protected override void OnPaintBackground( PaintEventArgs pevent )
    {
        this.disableInvalidates = true;

        // ============================***
        // Check if chart back ground has a transparent color
        // ============================***
        bool transparentBack = false;
        if ( this.chartPicture.BackColor.A != 255 && this.chartPicture.BackColor != Color.Empty )
        {
            transparentBack = true;
        }
        else if ( this.chartPicture.BackImageTransparentColor.A != 255 &&
            this.chartPicture.BackImageTransparentColor != Color.Empty &&
            !string.IsNullOrEmpty( this.chartPicture.BackImage ) )
        {
            transparentBack = true;
        }

        // ============================***
        // If chart or chart border page color has transparent color
        // ============================***
        bool transparentBorder = this.IsBorderTransparent();
        if ( transparentBorder || transparentBack )
        {
            Color oldBackColor = this.chartPicture.BackColor;
            if ( transparentBorder )

            {
                this.chartPicture.BackColor = Color.Transparent;
            }

            // Call base class 
            base.OnPaintBackground( pevent );

            this.chartPicture.BackColor = oldBackColor;
        }

        this.disableInvalidates = false;
    }

    /// <summary>
    /// When user changes system color, the Chart redraws itself.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnSystemColorsChanged( EventArgs e )
    {
        base.OnSystemColorsChanged( e );

        this.Invalidate();
    }

    /// <summary>
    /// Checks if border skins is enabled in the chart and it uses transparency in the page color
    /// </summary>
    /// <returns>True if transparency is used in the border.</returns>
    private bool IsBorderTransparent()
    {
        bool transparentBorder = false;
        if ( this.chartPicture.BorderSkin.SkinStyle != BorderSkinStyle.None )
        {
            if ( this.chartPicture.BorderSkin.PageColor.A != 255 &&
                this.chartPicture.BorderSkin.PageColor != Color.Empty )
            {
                transparentBorder = true;
            }
            if ( this.chartPicture.BorderSkin.BackColor.A != 255 && this.chartPicture.BorderSkin.BackColor != Color.Empty )
            {
                transparentBorder = true;
            }
            else if ( this.chartPicture.BorderSkin.BackImageTransparentColor.A != 255 &&
                this.chartPicture.BorderSkin.BackImageTransparentColor != Color.Empty &&
                !string.IsNullOrEmpty( this.chartPicture.BorderSkin.BackImage ) )
            {
                transparentBorder = true;
            }
        }

        return transparentBorder;
    }

    /// <summary>
    /// Draws exception information at design-time.
    /// </summary>
    /// <param name="graphics">Chart graphics to use.</param>
    private void DrawException( Graphics graphics )
    {
        // Fill background
        graphics.FillRectangle( Brushes.White, 0, 0, this.Width, this.Height );

        string addMessage = SR.ExceptionChartPreviewNotAvailable;
        // Get text rectangle
        RectangleF rect = new( 3, 3, this.Width - 6, this.Height - 6 );

        // Draw exception text
        using StringFormat format = new();
        format.Alignment = StringAlignment.Center;
        format.LineAlignment = StringAlignment.Center;
        using Font font = new( FontCache.DefaultFamilyName, 8 );
        graphics.DrawString( addMessage, font, Brushes.Black, rect, format );
    }

    /// <summary>
    /// Forces the control to invalidate its client area and immediately redraw itself and any child controls.
    /// </summary>
    [
    EditorBrowsable( EditorBrowsableState.Never )
    ]
    public override void Refresh()
    {
        // Clear bitmap used to improve the performance of elements
        // like cursors and annotations
        // NOTE: Fixes issue #4157
        if ( this.chartPicture.nonTopLevelChartBuffer is not null )
        {
            this.chartPicture.nonTopLevelChartBuffer.Dispose();
            this.chartPicture.nonTopLevelChartBuffer = null;
        }

        this.dirtyFlag = true;
        this.ResetAccessibilityObject();
        base.Refresh();
    }

    /// <summary>
    /// Invalidates a specific region of the control and causes a paint message to be sent to the control.
    /// </summary>
    public new void Invalidate()
    {
        this.dirtyFlag = true;
        this.ResetAccessibilityObject();
        if ( !this.disableInvalidates )
        {
            base.Invalidate( true );

        }

        // NOTE: Code below required for the Diagram integration. -AG
        if ( !this.chartPicture.isSavingAsImage )
        {
            InvalidateEventArgs e = new( Rectangle.Empty );
            this.OnInvalidated( e );
        }

    }

    /// <summary>
    /// Invalidates a specific region of the control and causes a paint message to be sent to the control.
    /// </summary>
    public new void Invalidate( Rectangle rectangle )
    {
        this.dirtyFlag = true;
        this.ResetAccessibilityObject();
        if ( !this.disableInvalidates )
        {
            base.Invalidate( rectangle );

        }

        // NOTE: Code below required for the Diagram integration. -AG
        if ( !this.chartPicture.isSavingAsImage )
        {
            InvalidateEventArgs e = new( Rectangle.Empty );
            this.OnInvalidated( e );
        }
    }




    /// <summary>
    /// Updates chart cursor and range selection only.
    /// </summary>
    public void UpdateCursor()
    {
        // Set flag to redraw cursor/selection only
        this.paintTopLevelElementOnly = true;

        // Update chart cursor and range selection
        base.Update();

        // Clear flag to redraw cursor/selection only
        this.paintTopLevelElementOnly = false;
    }



    /// <summary>
    /// Updates chart annotations only.
    /// </summary>
    public void UpdateAnnotations()
    {
        // Set flag to redraw cursor/selection only
        this.paintTopLevelElementOnly = true;

        // Update chart cursor and range selection
        base.Update();

        // Clear flag to redraw cursor/selection only
        this.paintTopLevelElementOnly = false;
    }

    #endregion

    #region " control size and location properties/methods "

    /// <summary>
    /// Returns default control size.
    /// </summary>
    protected override Size DefaultSize => new( 300, 300 );

    /// <summary>
    /// Control location changed.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnLocationChanged( EventArgs e )
    {
        // If chart or chart border page color has transparent color
        if ( (this.chartPicture.BackColor.A != 255 && this.chartPicture.BackColor != Color.Empty) ||
            (this.chartPicture.BorderSkin.SkinStyle != BorderSkinStyle.None &&
            this.chartPicture.BorderSkin.PageColor.A != 255 &&
            this.chartPicture.BorderSkin.PageColor != Color.Empty) )
        {
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }
        }
        base.OnLocationChanged( e );
    }

    /// <summary>
    /// Control resized.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnResize( EventArgs e )
    {
        this.chartPicture.Width = this.Size.Width;
        this.chartPicture.Height = this.Size.Height;
        this.dirtyFlag = true;
        this.ResetAccessibilityObject();
        base.OnResize( e );
    }
    /// <summary>
    /// Fires RightToLeftChanged event.
    /// </summary>
    /// <param name="e">Event Arguments</param>
    protected override void OnRightToLeftChanged( EventArgs e )
    {
        base.OnRightToLeftChanged( e );
        this.Invalidate();
    }

    #endregion

    #region " chart image saving methods "

    /// <summary>
    /// Saves chart image into the file.
    /// </summary>
    /// <param name="imageFileName">Image file name</param>
    /// <param name="format">Image format.</param>
    public void SaveImage( string imageFileName, ChartImageFormat format )
    {
        // Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( imageFileName, nameof( imageFileName ) );
#else
        if ( imageFileName == null ) throw new ArgumentNullException( nameof( imageFileName ) );
#endif

        // Create file stream for the specified file name
        FileStream fileStream = new( imageFileName, FileMode.Create );

        // Save into stream
        try
        {
            this.SaveImage( fileStream, format );
        }
        finally
        {
            // Close file stream
            fileStream.Close();
        }
    }

    /// <summary>
    /// Saves chart image into the file.
    /// </summary>
    /// <param name="imageFileName">Image file name</param>
    /// <param name="format">Image format.</param>
    public void SaveImage( string imageFileName, ImageFormat format )
    {
        // Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( imageFileName, nameof( imageFileName ) );
        ArgumentNullException.ThrowIfNull( format, nameof( format ) );
#else
        if ( imageFileName == null ) throw new ArgumentNullException( nameof( imageFileName ) );
        if ( format == null ) throw new ArgumentNullException( nameof( format ) );
#endif

        // Create file stream for the specified file name
        FileStream fileStream = new( imageFileName, FileMode.Create );

        // Save into stream
        try
        {
            this.SaveImage( fileStream, format );
        }
        finally
        {
            // Close file stream
            fileStream.Close();
        }
    }

    /// <summary>
    /// Saves chart image into the stream.
    /// </summary>
    /// <param name="imageStream">Image stream.</param>
    /// <param name="format">Image format.</param>
    public void SaveImage( Stream imageStream, ImageFormat format )
    {
        // Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( imageStream, nameof( imageStream ) );
        ArgumentNullException.ThrowIfNull( format, nameof( format ) );
#else
        if ( imageStream == null ) throw new ArgumentNullException( nameof( imageStream ) );
        if ( format == null ) throw new ArgumentNullException( nameof( format ) );
#endif

        // Indicate that chart is saved into the image
        this.chartPicture.isSavingAsImage = true;

        if ( format == ImageFormat.Emf || format == ImageFormat.Wmf )
        {
            this.chartPicture.SaveIntoMetafile( imageStream, EmfType.EmfOnly );
        }
        else
        {
            // Get chart image
            Image chartImage = this.chartPicture.GetImage();

            // Save image into the file
            chartImage.Save( imageStream, format );

            // Dispose image
            chartImage.Dispose();
        }

        // Reset flag
        this.chartPicture.isSavingAsImage = false;
    }

    /// <summary>
    /// Saves chart image into the stream.
    /// </summary>
    /// <param name="imageStream">Image stream.</param>
    /// <param name="format">Image format.</param>
    public void SaveImage( Stream imageStream, ChartImageFormat format )
    {
        // Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( imageStream, nameof( imageStream ) );
#else
        if ( imageStream == null ) throw new ArgumentNullException( nameof( imageStream ) );
#endif

        // Indicate that chart is saved into the image
        this.chartPicture.isSavingAsImage = true;

        if ( format is ChartImageFormat.Emf or
            ChartImageFormat.EmfPlus or
            ChartImageFormat.EmfDual )
        {
            EmfType emfType = EmfType.EmfOnly;
            if ( format == ChartImageFormat.EmfDual )
            {
                emfType = EmfType.EmfPlusDual;
            }
            else if ( format == ChartImageFormat.EmfPlus )
            {
                emfType = EmfType.EmfPlusOnly;
            }

            this.chartPicture.SaveIntoMetafile( imageStream, emfType );
        }
        else
        {
            // Get chart image
            Image chartImage = this.chartPicture.GetImage();

            ImageFormat standardImageFormat = ImageFormat.Png;

            switch ( format )
            {
                case ChartImageFormat.Bmp:
                    standardImageFormat = ImageFormat.Bmp;
                    break;
                case ChartImageFormat.Gif:
                    standardImageFormat = ImageFormat.Gif;
                    break;
                case ChartImageFormat.Jpeg:
                    standardImageFormat = ImageFormat.Jpeg;
                    break;
                case ChartImageFormat.Png:
                    standardImageFormat = ImageFormat.Png;
                    break;
                case ChartImageFormat.Tiff:
                    standardImageFormat = ImageFormat.Tiff;
                    break;
                default:
                    break;
            }

            // Save image into the file
            chartImage.Save( imageStream, standardImageFormat );

            // Dispose image
            chartImage.Dispose();
        }

        // Reset flag
        this.chartPicture.isSavingAsImage = false;
    }

    #endregion

    #region " control public properties "

    /// <summary>
    /// Array of custom palette colors.
    /// </summary>
    /// <remarks>
    /// When this custom colors array is non-empty the <b>Palette</b> property is ignored.
    /// </remarks>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    SerializationVisibility( SerializationVisibility.Attribute ),
    SRDescription( "DescriptionAttributeChart_PaletteCustomColors" ),
    TypeConverter( typeof( ColorArrayConverter ) )
    ]
    public Color[] PaletteCustomColors
    {
        set
        {
            this._dataManager.PaletteCustomColors = value;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }
        }
        get => this._dataManager.PaletteCustomColors;
    }

    /// <summary>
    /// Method resets custom colors array. Internal use only.
    /// </summary>
    [EditorBrowsable( EditorBrowsableState.Never )]
    internal void ResetPaletteCustomColors()
    {
        this.PaletteCustomColors = [];
    }

    /// <summary>
    /// Method resets custom colors array. Internal use only.
    /// </summary>
    [EditorBrowsable( EditorBrowsableState.Never )]
    internal bool ShouldSerializePaletteCustomColors()
    {
        return this.PaletteCustomColors != null &&
            this.PaletteCustomColors.Length != 0;
    }

    /// <summary>
    /// Indicates that non-critical chart exceptions will be suppressed.
    /// </summary>
    [
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( false ),
    SRDescription( "DescriptionAttributeSuppressExceptions" ),
    ]
    public bool SuppressExceptions
    {
        set => this.chartPicture.SuppressExceptions = value;
        get => this.chartPicture.SuppressExceptions;
    }

    /// <summary>
    /// "The data source used to populate series data. Series ValueMember properties must be also set."
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeDataSource" ),
    DefaultValue( null ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    AttributeProvider( typeof( IListSource ) )
    ]
    public object DataSource
    {
        get => this.chartPicture.DataSource;
        set => this.chartPicture.DataSource = value;
    }

    /// <summary>
    /// Chart named images collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    Bindable( false ),
    SRDescription( "DescriptionAttributeChart_Images" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public NamedImagesCollection Images { get; private set; } = null;

    /// <summary>
    /// Chart printing object.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    Bindable( false ),
    SRDescription( "DescriptionAttributeChart_Printing" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public PrintingManager Printing { get; private set; } = null;

    /// <summary>
    /// Chart series collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeChart_Series" ),
    Editor( typeof( SeriesCollectionEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public SeriesCollection Series => this._dataManager.Series;

    /// <summary>
    /// Chart legend collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeLegends" ),
    Editor( typeof( LegendCollectionEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public LegendCollection Legends => this.chartPicture.Legends;

    /// <summary>
    /// Chart title collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeTitles" ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public TitleCollection Titles => this.chartPicture.Titles;

    /// <summary>
    /// Chart annotation collection.
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    SRDescription( "DescriptionAttributeAnnotations3" ),
    Editor( typeof( AnnotationCollectionEditor ), typeof( UITypeEditor ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    ]
    public AnnotationCollection Annotations => this.chartPicture.Annotations;

    /// <summary>
    /// BackImage is not used. Use BackImage property instead.
    /// </summary>
    [
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public override Image BackgroundImage
    {
        get => base.BackgroundImage;
        set => base.BackgroundImage = value;
    }

    /// <summary>
    /// Color palette to use
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributePalette" ),
    DefaultValue( ChartColorPalette.BrightPastel ),
    Editor( typeof( ColorPaletteEditor ), typeof( UITypeEditor ) ),
    ]
    public ChartColorPalette Palette
    {
        get => this._dataManager.Palette;
        set
        {
            this._dataManager.Palette = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Specifies whether smoothing (antialiasing) is applied while drawing chart.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( typeof( AntiAliasingStyles ), "All" ),
    SRDescription( "DescriptionAttributeAntiAlias" ),
    Editor( typeof( FlagsEnumUITypeEditor ), typeof( UITypeEditor ) ),
    ]
    public AntiAliasingStyles AntiAliasing
    {
        get => this.chartPicture.AntiAliasing;
        set
        {
            if ( this.chartPicture.AntiAliasing != value )
            {
                this.chartPicture.AntiAliasing = value;

                this.dirtyFlag = true;
                if ( !this.disableInvalidates )
                {
                    this.Invalidate();
                }
            }
        }
    }

    /// <summary>
    /// Specifies the quality of text antialiasing.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( typeof( TextAntiAliasingQuality ), "High" ),
    SRDescription( "DescriptionAttributeTextAntiAliasingQuality" )
    ]
    public TextAntiAliasingQuality TextAntiAliasingQuality
    {
        get => this.chartPicture.TextAntiAliasingQuality;
        set
        {
            this.chartPicture.TextAntiAliasingQuality = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }
        }
    }

    /// <summary>
    /// Specifies whether smoothing is applied while drawing shadows.
    /// </summary>
    [
    SRCategory( "CategoryAttributeImage" ),
    Bindable( true ),
    DefaultValue( true ),
    SRDescription( "DescriptionAttributeChart_SoftShadows" ),
    ]
    public bool IsSoftShadows
    {
        get => this.chartPicture.IsSoftShadows;
        set
        {
            this.chartPicture.IsSoftShadows = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Reference to chart area collection
    /// </summary>
    [
    SRCategory( "CategoryAttributeChart" ),
    Bindable( true ),
    SRDescription( "DescriptionAttributeChartAreas" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
    Editor( typeof( ChartCollectionEditor ), typeof( UITypeEditor ) ),
    ]
    public ChartAreaCollection ChartAreas => this.chartPicture.ChartAreas;

    /// <summary>
    /// Back ground color for the Chart
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBackColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public override Color BackColor
    {
        get => this.chartPicture.BackColor;
        set
        {
            if ( this.chartPicture.BackColor != value )
            {
                this.chartPicture.BackColor = value;
                this.dirtyFlag = true;
                if ( !this.disableInvalidates )
                {
                    this.Invalidate();
                }

                // Call notification event
                this.OnBackColorChanged( EventArgs.Empty );
            }
        }
    }

    /// <summary>
    /// Fore color property (not used)
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( false ),
    Browsable( false ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeForeColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public override Color ForeColor
    {
        get => Color.Empty;
        set
        {
        }
    }

    /// <summary>
    /// Fore color property (not used)
    /// </summary>
    [
    SRCategory( "CategoryAttributeLayout" ),
    Bindable( true ),
    DefaultValue( typeof( Size ), "300, 300" ),
    SRDescription( "DescriptionAttributeChart_Size" ),
    ]
    public new Size Size
    {
        get => base.Size;
        set
        {
            this.chartPicture.InspectChartDimensions( value.Width, value.Height );
            base.Size = value;
        }
    }

    /// <summary>
    /// Series data manipulator
    /// </summary>
    [
    SRCategory( "CategoryAttributeData" ),
    SRDescription( "DescriptionAttributeDataManipulator" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public DataManipulator DataManipulator => this.chartPicture.DataManipulator;

    /// <summary>
    /// Chart serializer object.
    /// </summary>
    [
    SRCategory( "CategoryAttributeSerializer" ),
    SRDescription( "DescriptionAttributeChart_Serializer" ),
    Browsable( false ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public ChartSerializer Serializer { get; private set; } = null;

    /// <summary>
    /// Title font
    /// </summary>
    [
    SRCategory( "CategoryAttributeChartTitle" ),
    Bindable( false ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8pt" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public new Font Font
    {
        get => base.Font;
        set => base.Font = value;
    }

    /// <summary>
    /// Back Hatch style
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartHatchStyle.None ),
    SRDescription( "DescriptionAttributeBackHatchStyle" ),
    Editor( typeof( HatchStyleEditor ), typeof( UITypeEditor ) ),
    ]
    public ChartHatchStyle BackHatchStyle
    {
        get => this.chartPicture.BackHatchStyle;
        set
        {
            this.chartPicture.BackHatchStyle = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }



    /// <summary>
    /// Chart area background image
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( "" ),
    SRDescription( "DescriptionAttributeBackImage" ),
    NotifyParentProperty( true ),
    Editor( typeof( ImageValueEditor ), typeof( UITypeEditor ) ),
    ]
    public string BackImage
    {
        get => this.chartPicture.BackImage;
        set
        {
            this.chartPicture.BackImage = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Chart area background image drawing mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageWrapMode.Tile ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageWrapMode" ),
    ]
    public ChartImageWrapMode BackImageWrapMode
    {
        get => this.chartPicture.BackImageWrapMode;
        set
        {
            this.chartPicture.BackImageWrapMode = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Background image transparent color.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeImageTransparentColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BackImageTransparentColor
    {
        get => this.chartPicture.BackImageTransparentColor;
        set
        {
            this.chartPicture.BackImageTransparentColor = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Background image alignment used by Clamp Un-Scale drawing mode.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartImageAlignmentStyle.TopLeft ),
    NotifyParentProperty( true ),
    SRDescription( "DescriptionAttributeBackImageAlign" ),
    ]
    public ChartImageAlignmentStyle BackImageAlignment
    {
        get => this.chartPicture.BackImageAlignment;
        set
        {
            this.chartPicture.BackImageAlignment = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// A type for the background gradient
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( GradientStyle.None ),
    SRDescription( "DescriptionAttributeBackGradientStyle" ),
    Editor( typeof( GradientEditor ), typeof( UITypeEditor ) ),
    ]
    public GradientStyle BackGradientStyle
    {
        get => this.chartPicture.BackGradientStyle;
        set
        {
            this.chartPicture.BackGradientStyle = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// The second color which is used for a gradient
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "" ),
    SRDescription( "DescriptionAttributeBackSecondaryColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BackSecondaryColor
    {
        get => this.chartPicture.BackSecondaryColor;
        set
        {
            this.chartPicture.BackSecondaryColor = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Border color for the Chart
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( false ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BorderColor
    {
        get => this.chartPicture.BorderColor;
        set
        {
            this.chartPicture.BorderColor = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// The width of the border line
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( false ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeChart_BorderlineWidth" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public int BorderWidth
    {
        get => this.chartPicture.BorderWidth;
        set
        {
            this.chartPicture.BorderWidth = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// The style of the border line
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( false ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DefaultValue( ChartDashStyle.NotSet ),
    SRDescription( "DescriptionAttributeBorderDashStyle" ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public ChartDashStyle BorderDashStyle
    {
        get => this.chartPicture.BorderDashStyle;
        set
        {
            this.chartPicture.BorderDashStyle = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Border color for the Chart
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( typeof( Color ), "White" ),
    SRDescription( "DescriptionAttributeBorderColor" ),
    TypeConverter( typeof( ColorConverter ) ),
    Editor( typeof( ChartColorEditor ), typeof( UITypeEditor ) ),
    ]
    public Color BorderlineColor
    {
        get => this.chartPicture.BorderColor;
        set
        {
            this.chartPicture.BorderColor = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// The width of the border line
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( 1 ),
    SRDescription( "DescriptionAttributeChart_BorderlineWidth" ),
    ]
    public int BorderlineWidth
    {
        get => this.chartPicture.BorderWidth;
        set
        {
            this.chartPicture.BorderWidth = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// The style of the border line
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( ChartDashStyle.NotSet ),
    SRDescription( "DescriptionAttributeBorderDashStyle" ),
    ]
    public ChartDashStyle BorderlineDashStyle
    {
        get => this.chartPicture.BorderDashStyle;
        set
        {
            this.chartPicture.BorderDashStyle = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Chart border skin style.
    /// </summary>
    [
    SRCategory( "CategoryAttributeAppearance" ),
    Bindable( true ),
    DefaultValue( BorderSkinStyle.None ),
    SRDescription( "DescriptionAttributeBorderSkin" ),
    NotifyParentProperty( true ),
    TypeConverter( typeof( LegendConverter ) ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Content )
    ]
    public BorderSkin BorderSkin
    {
        get => this.chartPicture.BorderSkin;
        set
        {
            this.chartPicture.BorderSkin = value;
            this.dirtyFlag = true;
            if ( !this.disableInvalidates )
            {
                this.Invalidate();
            }

        }
    }

    /// <summary>
    /// Build number of the control
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChart_BuildNumber" ),
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    DefaultValue( "" )
    ]
    public string BuildNumber
    {
        get
        {
            // Get build number from the assembly
            string buildNumber = string.Empty;
            Assembly assembly = Assembly.GetExecutingAssembly();
            if ( assembly is not null )
            {
                buildNumber = assembly.FullName.ToUpper( CultureInfo.InvariantCulture );
                int versionIndex = buildNumber.IndexOf( "VERSION=", StringComparison.Ordinal );
                if ( versionIndex >= 0 )
                {
                    buildNumber = buildNumber[(versionIndex + 8)..];
                }
                versionIndex = buildNumber.IndexOf( ",", StringComparison.OrdinalIgnoreCase );
                if ( versionIndex >= 0 )
                {
                    buildNumber = buildNumber[..versionIndex];
                }
            }
            return buildNumber;
        }
    }

    // Vertical rendering resolution.
    internal static double renderingDpiY = 96.0;

    /// <summary>
    /// Vertical resolution of the chart renderer.
    /// </summary>
    /// <remarks>
    /// This property is for the internal use only.
    /// </remarks>
    [
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( 96.0 ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public double RenderingDpiY
    {
        set => Chart.renderingDpiY = value;
        get => Chart.renderingDpiY;
    }

    /// <summary>
    /// Horizontal resolution of the chart renderer.
    /// </summary>
    /// <remarks>
    /// This property is for the internal use only.
    /// </remarks>
    [
    Browsable( false ),
    EditorBrowsable( EditorBrowsableState.Never ),
    SRCategory( "CategoryAttributeMisc" ),
    DefaultValue( 96.0 ),
    DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
    SerializationVisibility( SerializationVisibility.Hidden )
    ]
    public double RenderingDpiX
    {
        set => Chart.renderingDpiX = value;
        get => Chart.renderingDpiX;
    }

    #endregion

    #region " control public methods "

    /// <summary>
    /// Loads chart appearance template from file.
    /// </summary>
    /// <param name="name">Template file name to load from.</param>
    public void LoadTemplate( string name )
    {
        this.chartPicture.LoadTemplate( name );
    }

    /// <summary>
    /// Loads chart appearance template from stream.
    /// </summary>
    /// <param name="stream">Template stream to load from.</param>
    public void LoadTemplate( Stream stream )
    {
        this.chartPicture.LoadTemplate( stream );
    }

    /// <summary>
    /// Applies palette colors to series or data points.
    /// </summary>
    public void ApplyPaletteColors()
    {
        // Apply palette colors to series
        this._dataManager.ApplyPaletteColors();

        // Apply palette colors to data Points in series
        foreach ( Series series in this.Series )
        {
            // Check if palette colors should be applied to the points
            bool applyToPoints = false;
            if ( series.Palette != ChartColorPalette.None )
            {
                applyToPoints = true;
            }
            else
            {
                IChartType chartType = this._chartTypeRegistry.GetChartType( series.ChartType );
                applyToPoints = chartType.ApplyPaletteColorsToPoints;
            }

            // Apply palette colors to the points
            if ( applyToPoints )
            {
                series.ApplyPaletteColors();
            }
        }
    }

    /// <summary>
    /// Checks if control is in design mode.
    /// </summary>
    /// <returns>True if control is in design mode.</returns>
    internal bool IsDesignMode()
    {
        return this.DesignMode;
    }

    /// <summary>
    /// Reset auto calculated chart properties values to "Auto".
    /// </summary>
    public void ResetAutoValues()
    {
        // Reset auto calculated series properties values 
        foreach ( Series series in this.Series )
        {
            series.ResetAutoValues();
        }

        // Reset auto calculated axis properties values 
        foreach ( ChartArea chartArea in this.ChartAreas )
        {
            chartArea.ResetAutoValues();
        }

    }

    /// <summary>
    /// This method performs the hit test and returns a HitTestResult objects.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <returns>Hit test result object</returns>
    public HitTestResult HitTest( int x, int y )
    {
        return this.selection.HitTest( x, y );
    }

    /// <summary>
    /// This method performs the hit test and returns a HitTestResult object.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="ignoreTransparent">Indicates that transparent elements should be ignored.</param>
    /// <returns>Hit test result object</returns>
    public HitTestResult HitTest( int x, int y, bool ignoreTransparent )
    {
        return this.selection.HitTest( x, y, ignoreTransparent );
    }

    /// <summary>
    /// This method performs the hit test and returns a HitTestResult object.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="requestedElement">Only this chart element will be hit tested.</param>
    /// <returns>Hit test result object</returns>
    public HitTestResult HitTest( int x, int y, ChartElementType requestedElement )
    {
        return this.selection.HitTest( x, y, requestedElement );
    }

    /// <summary>
    /// Call this method to determine the  chart element,
    /// if any, that is located at a point defined by the given X and Y 
    /// coordinates.
    /// <seealso cref="HitTestResult"/></summary>
    /// <param name="x">The X coordinate for the point in question.
    /// Often obtained from a parameter in an event
    /// (e.g. the X parameter value in the MouseDown event).</param>
    /// <param name="y">The Y coordinate for the point in question.
    /// Often obtained from a parameter in an event
    /// (e.g. the Y parameter value in the MouseDown event).</param>
    /// <param name="ignoreTransparent">Indicates that transparent 
    /// elements should be ignored.</param>
    /// <param name="requestedElement">
    /// An array of type which specify the types                  
    /// to test for, on order to filter the result. If omitted checking for                 
    /// elementTypes will be ignored and all kind of elementTypes will be 
    /// valid.
    ///  </param>
    /// <returns>
    /// A array of <see cref="HitTestResult"/> objects,
    /// which provides information concerning the  chart element
    /// (if any) that is at the specified location. Result contains at least
    /// one element, which could be ChartElementType.Nothing. 
    /// The objects in the result are sorted in from top to bottom of 
    /// different layers of control. </returns>
    /// <remarks>Call this method to determine the  gauge element
    /// (if any) that is located at a specified point. Often this method is used in
    /// some mouse-related event (e.g. MouseDown)
    /// to determine what  gauge element the end-user clicked on.
    /// The X and Y mouse coordinates obtained from the
    /// event parameters are then used for the X and Y parameter              
    /// values of this method call.   The returned 
    /// <see cref="HitTestResult"/> object's properties
    /// can then be used to determine what  chart element was clicked on,
    /// and also provides a reference to the actual object selected (if 
    /// any).</remarks>
    public HitTestResult[] HitTest( int x, int y, bool ignoreTransparent, params ChartElementType[] requestedElement )
    {
        return this.selection.HitTest( x, y, ignoreTransparent, requestedElement );
    }

    /// <summary>
    /// Gets the chart element outline.
    /// </summary>
    /// <param name="element">The chart object.</param>
    /// <param name="elementType">Type of the element.</param>
    /// <returns> A <see cref="ChartElementOutline"/> object which contains
    /// 1) An array of points in absolute coordinates which can be used as outline markers around this chart element.
    /// 2) A GraphicsPath for drawing outline around this chart element.
    /// </returns>
    /// <remarks>
    /// If the <paramref name="element"/> is not part of the chart or <paramref name="elementType"/> cannot be combined 
    /// with <paramref name="element"/> then the result will contain empty array of marker points. 
    /// The marker points are sorted clockwise.
    /// </remarks>
    public ChartElementOutline GetChartElementOutline( object element, ChartElementType elementType )
    {
        return this.selection.GetChartElementOutline( element, elementType );
    }

    #endregion

    #region " control protected methods "

    /// <summary>   Raises the <see cref="System.Windows.Forms.Control.GotFocus" /> event. </summary>
    /// <remarks>   David, 2021-06-09. </remarks>
    /// <param name="e">    An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnGotFocus( EventArgs e )
    {
        base.OnGotFocus( e );

        using Graphics g = Graphics.FromHwndInternal( this.Handle );
        ControlPaint.DrawFocusRectangle( g, new Rectangle( 1, 1, this.Size.Width - 2, this.Size.Height - 2 ) );
    }

    /// <summary>   Raises the <see cref="System.Windows.Forms.Control.LostFocus" /> event. </summary>
    /// <remarks>   David, 2021-06-09. </remarks>
    /// <param name="e">    An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLostFocus( EventArgs e )
    {
        base.OnLostFocus( e );

        using Graphics g = Graphics.FromHwndInternal( this.Handle );
        using Brush b = new SolidBrush( this.BackColor );
        Rectangle topBorder = new( 1, 1, this.Size.Width - 2, 1 );
        g.FillRectangle( b, topBorder );

        Rectangle rightBorder = new( this.Size.Width - 2, 1, 1, this.Size.Height - 2 );
        g.FillRectangle( b, rightBorder );

        Rectangle bottomBorder = new( 1, this.Size.Height - 2, this.Size.Width - 2, 1 );
        g.FillRectangle( b, bottomBorder );

        Rectangle leftBorder = new( 1, 1, 1, this.Size.Height - 2 );
        g.FillRectangle( b, leftBorder );
    }

    #endregion

    #region " isupportinitialize implementation "

    /// <summary>
    /// Signals the object that initialization is starting.
    /// </summary>
    public void BeginInit()
    {
        // Disable control invalidation
        this.disableInvalidates = true;
    }

    /// <summary>
    /// Signals the object that initialization is complete.
    /// </summary>
    public void EndInit()
    {
        // Enable control invalidation
        this.disableInvalidates = false;

        // If control is dirty - invalidate it
        if ( this.dirtyFlag )
        {
            base.Invalidate();
        }

    }

    #endregion

    #region " control mouse events "

    /// <summary>
    /// Raises the <see cref="System.Windows.Forms.Control.CursorChanged"/> event.
    /// </summary>
    /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
    protected override void OnCursorChanged( EventArgs e )
    {
        this.defaultCursor = this.Cursor;
        base.OnCursorChanged( e );
    }

    /// <summary>
    /// Mouse button pressed in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnMouseDown( MouseEventArgs e )
    {
        this.OnChartMouseDown( e );
    }

    /// <summary>
    /// Mouse button pressed in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void OnChartMouseDown( MouseEventArgs e )
    {
        bool handled = false;

        if ( !handled )
        {
            // Notify annotation object collection about the mouse down event
            this.Annotations.OnMouseDown( e, ref handled );
        }

        // Loop through all areas and notify required object about the event
        if ( !handled )
        {
            foreach ( ChartArea area in this.ChartAreas )
            {
                // No cursor or scroll bar support in 3D
                if ( !area.Area3DStyle.Enable3D &&
                    !area.chartAreaIsCurcular
                    && area.Visible )
                {
                    foreach ( Axis axis in area.Axes )
                    {
                        // Notify axis scroll bar
                        axis.ScrollBar.ScrollBar_MouseDown( this, e );
                    }

                    // Notify area X and Y cursors
                    area.CursorX.Cursor_MouseDown( this, e );
                    area.CursorY.Cursor_MouseDown( this, e );
                }
            }
        }

        // Call the base class
        base.OnMouseDown( e );
    }

    /// <summary>
    /// Mouse button up in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnMouseUp( MouseEventArgs e )
    {
        this.OnChartMouseUp( e );
    }

    /// <summary>
    /// Mouse button up in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void OnChartMouseUp( MouseEventArgs e )
    {
        // Loop through all areas and notify required object about the event
        foreach ( ChartArea area in this.ChartAreas )
        {
            // No cursor or scroll bar support in 3D
            if ( !area.Area3DStyle.Enable3D &&
                !area.chartAreaIsCurcular
                && area.Visible )
            {
                foreach ( Axis axis in area.Axes )
                {
                    // Notify axis scroll bar
                    axis.ScrollBar.ScrollBar_MouseUp( this, e );
                }

                // Notify area X and Y cursors
                area.CursorX.Cursor_MouseUp( this, e );
                area.CursorY.Cursor_MouseUp( this, e );
            }
        }

        // Notify annotation object collection about the mouse down event
        this.Annotations.OnMouseUp( e );

        // Call the base class
        base.OnMouseUp( e );
    }

    /// <summary>
    /// Mouse moved in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnMouseMove( MouseEventArgs e )
    {
        this.OnChartMouseMove( e );
    }

    /// <summary>
    /// Mouse moved in the control.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void OnChartMouseMove( MouseEventArgs e )
    {
        // Flag which indicates if event was already isHandled
        bool handled = false;

        // Loop through all areas and notify required object about the event
        foreach ( ChartArea area in this.ChartAreas )
        {
            // No cursor or scroll bar support in 3D
            if ( !area.Area3DStyle.Enable3D &&
                !area.chartAreaIsCurcular
                && area.Visible )
            {
                foreach ( Axis axis in area.Axes )
                {
                    // Notify axis scroll bar
                    axis.ScrollBar.ScrollBar_MouseMove( e, ref handled );
                }

                // Notify area X and Y cursors
                area.CursorX.Cursor_MouseMove( e, ref handled );
                area.CursorY.Cursor_MouseMove( e, ref handled );
            }
        }

        // Notify Selection object for tool tips processing
        if ( !handled )
        {
            this.selection.Selection_MouseMove( this, e );
        }

        // Notify annotation object collection about the mouse down event
        if ( !handled )
        {
            this.Annotations.OnMouseMove( e );
        }

        // Call the base class
        base.OnMouseMove( e );
    }

    /// <summary>
    /// Mouse was double clicked on the control.
    /// </summary>
    /// <param name="e">Event arguments</param>
    protected override void OnDoubleClick( EventArgs e )
    {
        // Notify annotation object collection about the mouse down event
        this.Annotations.OnDoubleClick();

        // Call the base class
        base.OnDoubleClick( e );
    }

    #endregion

    #region " chart get tool tip text events "

    /// <summary>
    /// Called before showing the tooltip to get the tooltip text.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_GetToolTipText" ),
    SRCategory( "CategoryAttributeToolTips" )]
    public event EventHandler<ToolTipEventArgs> GetToolTipText;

    /// <summary>
    /// Checks if GetToolTipEvent is used
    /// </summary>
    /// <returns>True if event is used</returns>
    internal bool IsToolTipEventUsed()
    {
        return GetToolTipText != null;
    }

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Cursor event arguments.</param>
    internal void OnGetToolTipText( ToolTipEventArgs arguments )
    {
        GetToolTipText?.Invoke( this, arguments );
    }

    #endregion

    #region " chart area cursor and selection events "

    /// <summary>
    /// Called when cursor position is about to change.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_CursorPositionChanging" ),
    SRCategory( "CategoryAttributeCursor" )]
    public event EventHandler<CursorEventArgs> CursorPositionChanging;

    /// <summary>
    /// Called when cursor position is changed.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_CursorPositionChanged" ),
    SRCategory( "CategoryAttributeCursor" )]
    public event EventHandler<CursorEventArgs> CursorPositionChanged;

    /// <summary>
    /// Called when selection start/end position is about to change.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_SelectionRangeChanging" ),
    SRCategory( "CategoryAttributeCursor" )]
    public event EventHandler<CursorEventArgs> SelectionRangeChanging;

    /// <summary>
    /// Called when selection start/end position is changed.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_SelectionRangeChanged" ),
    SRCategory( "CategoryAttributeCursor" )]
    public event EventHandler<CursorEventArgs> SelectionRangeChanged;

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Cursor event arguments.</param>
    internal void OnCursorPositionChanging( CursorEventArgs arguments )
    {
        CursorPositionChanging?.Invoke( this, arguments );
    }

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Cursor event arguments.</param>
    internal void OnCursorPositionChanged( CursorEventArgs arguments )
    {
        CursorPositionChanged?.Invoke( this, arguments );
    }

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Cursor event arguments.</param>
    internal void OnSelectionRangeChanging( CursorEventArgs arguments )
    {
        SelectionRangeChanging?.Invoke( this, arguments );
    }

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Cursor event arguments.</param>
    internal void OnSelectionRangeChanged( CursorEventArgs arguments )
    {
        SelectionRangeChanged?.Invoke( this, arguments );
    }

    #endregion

    #region " axis data scaleview position/size changing events "

    /// <summary>
    /// Called when axis scaleView position/size is about to change.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_axisViewChanging" ),
    SRCategory( "CategoryAttributeAxisView" )]
    public event EventHandler<ViewEventArgs> AxisViewChanging;

    /// <summary>
    /// Called when axis scaleView position/size is changed.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_axisViewChanged" ),
    SRCategory( "CategoryAttributeAxisView" )]
    public event EventHandler<ViewEventArgs> AxisViewChanged;

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Axis scaleView event arguments.</param>
    internal void OnAxisViewChanging( ViewEventArgs arguments )
    {
        AxisViewChanging?.Invoke( this, arguments );
    }

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Axis scaleView event arguments.</param>
    internal void OnAxisViewChanged( ViewEventArgs arguments )
    {
        AxisViewChanged?.Invoke( this, arguments );
    }

    #endregion

    #region " axis scroll bar events "

    /// <summary>
    /// Called when axis scroll bar is used by user.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_axisScrollBarClicked" ),
    SRCategory( "CategoryAttributeAxisView" )]
    public event EventHandler<ScrollBarEventArgs> AxisScrollBarClicked;

    /// <summary>
    /// Calls event delegate.
    /// </summary>
    /// <param name="arguments">Axis scroll bar event arguments.</param>
    internal void OnAxisScrollBarClicked( ScrollBarEventArgs arguments )
    {
        AxisScrollBarClicked?.Invoke( this, arguments );
    }

    #endregion

    #region " painting events "

    /// <summary>
    /// Called when chart element is painted.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_PostPaint" ),
    SRCategory( "CategoryAttributeAppearance" )]
    public event EventHandler<ChartPaintEventArgs> PostPaint;

    /// <summary>
    /// Called when chart element back ground is painted.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_PrePaint" ),
    SRCategory( "CategoryAttributeAppearance" )]
    public event EventHandler<ChartPaintEventArgs> PrePaint;

    /// <summary>
    /// Fires when chart element background must be drawn. 
    /// This event is fired for elements like: ChartPicture, ChartArea and Legend
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected virtual void OnPrePaint( ChartPaintEventArgs e )
    {
        PrePaint?.Invoke( this, e );
    }

    /// <summary>
    /// Fires when chart element background must be drawn. 
    /// This event is fired for elements like: ChartPicture, ChartArea and Legend
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void CallOnPrePaint( ChartPaintEventArgs e )
    {
        this.OnPrePaint( e );
    }

    /// <summary>
    /// Fires when chart element must be drawn. 
    /// This event is fired for elements like: ChartPicture, ChartArea and Legend
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected virtual void OnPostPaint( ChartPaintEventArgs e )
    {
        PostPaint?.Invoke( this, e );
    }

    /// <summary>
    /// Fires when chart element must be drawn. 
    /// This event is fired for elements like: ChartPicture, ChartArea and Legend
    /// </summary>
    /// <param name="e">Event arguments.</param>
    internal void CallOnPostPaint( ChartPaintEventArgs e )
    {
        this.OnPostPaint( e );
    }

    #endregion

    #region " customize event "

    /// <summary>
    /// Fires just before the chart image is drawn. Use this event to customize the chart picture.
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_Customize" )
    ]
    public event EventHandler Customize;

    /// <summary>
    /// Fires when all chart data is prepared to be customized before drawing. 
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChart_OnCustomize" )
    ]
    protected virtual void OnCustomize()
    {
        Customize?.Invoke( this, EventArgs.Empty );
    }

    /// <summary>
    /// Fires when all chart data is prepared to be customized before drawing. 
    /// </summary>
    internal void CallOnCustomize()
    {
        this.OnCustomize();
    }

    /// <summary>
    /// Use this event to customize chart legend.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_CustomizeLegend" )]
    public event EventHandler<CustomizeLegendEventArgs> CustomizeLegend;

    /// <summary>
    /// Fires when all chart data is prepared to be customized before drawing. 
    /// </summary>
    [SRDescription( "DescriptionAttributeChart_OnCustomizeLegend" )]
    protected virtual void OnCustomizeLegend( LegendItemsCollection legendItems, string legendName )
    {
        CustomizeLegend?.Invoke( this, new CustomizeLegendEventArgs( legendItems, legendName ) );
    }

    /// <summary>
    /// Fires when all chart data is prepared to be customized before drawing. 
    /// </summary>
    internal void CallOnCustomizeLegend( LegendItemsCollection legendItems, string legendName )
    {
        this.OnCustomizeLegend( legendItems, legendName );
    }
    #endregion

    #region " annotation events "

    /// <summary>
    /// Fires when annotation text was changed. 
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_annotationTextChanged" ),
    SRCategory( "CategoryAttributeAnnotation" )
    ]
    public event EventHandler AnnotationTextChanged;

    /// <summary>
    /// Fires when annotation text is changed.
    /// </summary>
    /// <param name="annotation">Annotation which text was changed.</param>
    internal void OnAnnotationTextChanged( Annotation annotation )
    {
        AnnotationTextChanged?.Invoke( annotation, EventArgs.Empty );
    }

    /// <summary>
    /// Fires when selected annotation changes. 
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_annotationSelectionChanged" ),
    SRCategory( "CategoryAttributeAnnotation" )
    ]
    public event EventHandler AnnotationSelectionChanged;

    /// <summary>
    /// Fires when annotation position was changed.
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_annotationPositionChanged" ),
    SRCategory( "CategoryAttributeAnnotation" )
    ]
    public event EventHandler AnnotationPositionChanged;

    /// <summary>
    /// Fires when annotation position is changing.
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_annotationPositionChanging" ),
    SRCategory( "CategoryAttributeAnnotation" )
    ]
    public event EventHandler<AnnotationPositionChangingEventArgs> AnnotationPositionChanging;

    /// <summary>
    /// Fires when annotation is placed by the user on the chart. 
    /// </summary>
    [
    SRDescription( "DescriptionAttributeChartEvent_annotationPlaced" ),
    SRCategory( "CategoryAttributeAnnotation" )
    ]
    public event EventHandler AnnotationPlaced;

    /// <summary>
    /// Fires when annotation is placed by the user on the chart.
    /// </summary>
    /// <param name="annotation">Annotation which was placed.</param>
    internal void OnAnnotationPlaced( Annotation annotation )
    {
        AnnotationPlaced?.Invoke( annotation, EventArgs.Empty );
    }

    /// <summary>
    /// Fires when selected annotation changes. 
    /// </summary>
    /// <param name="annotation">Annotation which have it's selection changed.</param>
    internal void OnAnnotationSelectionChanged( Annotation annotation )
    {
        AnnotationSelectionChanged?.Invoke( annotation, EventArgs.Empty );
    }

    /// <summary>
    /// Fires when annotation position was changed.
    /// </summary>
    /// <param name="annotation">Annotation which have it's position changed.</param>
    internal void OnAnnotationPositionChanged( Annotation annotation )
    {
        AnnotationPositionChanged?.Invoke( annotation, EventArgs.Empty );
    }

    /// <summary>
    /// Fires when annotation position is changing.
    /// </summary>
    /// <param name="args">Event arguments.</param>
    /// <returns>True if event was processed.</returns>
    internal bool OnAnnotationPositionChanging( ref AnnotationPositionChangingEventArgs args )
    {
        if ( AnnotationPositionChanging is not null )
        {
            AnnotationPositionChanging( args.Annotation, args );
            return true;
        }
        return false;
    }

    #endregion

    #region " control databind method "

    /// <summary>
    /// Data binds control to the selected data source.
    /// </summary>
    public void DataBind()
    {
        this.chartPicture.DataBind();
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    public void AlignDataPointsByAxisLabel()
    {
        this.chartPicture.AlignDataPointsByAxisLabel( false, PointSortOrder.Ascending );
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    /// <param name="series">Comma separated list of series that should be aligned by axis label.</param>
    public void AlignDataPointsByAxisLabel( string series )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( series, nameof( series ) );
#else
        if ( series == null ) throw new ArgumentNullException( nameof( series ) );
#endif

        // Create list of series
        ArrayList seriesList = [];
        string[] seriesNames = series.Split( ',' );
        foreach ( string name in seriesNames )
        {
            _ = seriesList.Add( this.Series[name.Trim()] );
        }

        // Align series
        this.chartPicture.AlignDataPointsByAxisLabel( seriesList, false, PointSortOrder.Ascending );
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    /// <param name="series">Comma separated list of series that should be aligned by axis label.</param>
    /// <param name="sortingOrder">Points sorting order by axis labels.</param>
    public void AlignDataPointsByAxisLabel( string series, PointSortOrder sortingOrder )
    {
        //Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( series, nameof( series ) );
#else
        if ( series == null ) throw new ArgumentNullException( nameof( series ) );
#endif

        // Create list of series
        ArrayList seriesList = [];
        string[] seriesNames = series.Split( ',' );
        foreach ( string name in seriesNames )
        {
            _ = seriesList.Add( this.Series[name.Trim()] );
        }

        // Align series
        this.chartPicture.AlignDataPointsByAxisLabel( seriesList, true, sortingOrder );
    }

    /// <summary>
    /// Aligns data points using their axis labels.
    /// </summary>
    /// <param name="sortingOrder">Points sorting order by axis labels.</param>
    public void AlignDataPointsByAxisLabel( PointSortOrder sortingOrder )
    {
        this.chartPicture.AlignDataPointsByAxisLabel( true, sortingOrder );
    }

    /// <summary>
    /// Automatically creates and binds series to specified data table. 
    /// Each column of the table becomes a Y value in a separate series.
    /// Series X value field may also be provided. 
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="xField">Name of the field for series X values.</param>
    public void DataBindTable( IEnumerable dataSource, string xField )
    {
        this.chartPicture.DataBindTable(
            dataSource,
            xField );
    }

    /// <summary>
    /// Automatically creates and binds series to specified data table. 
    /// Each column of the table becomes a Y value in a separate series.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    public void DataBindTable( IEnumerable dataSource )
    {
        this.chartPicture.DataBindTable(
            dataSource,
            string.Empty );
    }

    /// <summary>
    /// Data bind chart to the table. Series will be automatically added to the chart depending ont
    /// yhe number of unique values in the seriesGroupByField column of the data source.
    /// Data source can be the Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="seriesGroupByField">Name of the field used to group data into series.</param>
    /// <param name="xField">Name of the field for X values.</param>
    /// <param name="yFields">Comma separated name(s) of the field(s) for Y value(s).</param>
    /// <param name="otherFields">Other point properties binding rule in format: PointProperty=Field[{Format}] [,PointProperty=Field[{Format}]]. For example: "Tooltip=Price{C1},Url=WebSiteName".</param>
    public void DataBindCrossTable(
        IEnumerable dataSource,
        string seriesGroupByField,
        string xField,
        string yFields,
        string otherFields )
    {
        this.chartPicture.DataBindCrossTab(
            dataSource,
            seriesGroupByField,
            xField,
            yFields,
            otherFields,
            false,
            PointSortOrder.Ascending );
    }

    /// <summary>
    /// Data bind chart to the table. Series will be automatically added to the chart depending ont
    /// yhe number of unique values in the seriesGroupByField column of the data source.
    /// Data source can be the Ole(SQL)DataReader, DataView, DataSet, DataTable or DataRow.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <param name="seriesGroupByField">Name of the field used to group data into series.</param>
    /// <param name="xField">Name of the field for X values.</param>
    /// <param name="yFields">Comma separated name(s) of the field(s) for Y value(s).</param>
    /// <param name="otherFields">Other point properties binding rule in format: PointProperty=Field[{Format}] [,PointProperty=Field[{Format}]]. For example: "Tooltip=Price{C1},Url=WebSiteName".</param>
    /// <param name="sortingOrder">Series will be sorted by group field values in specified order.</param>
    public void DataBindCrossTable(
        IEnumerable dataSource,
        string seriesGroupByField,
        string xField,
        string yFields,
        string otherFields,
        PointSortOrder sortingOrder )
    {
        this.chartPicture.DataBindCrossTab(
            dataSource,
            seriesGroupByField,
            xField,
            yFields,
            otherFields,
            true,
            sortingOrder );
    }

    #endregion

    #region " special extension methods and properties "

    /// <summary>
    /// Gets the requested chart service.
    /// </summary>
    /// <param name="serviceType">AxisName of requested service.</param>
    /// <returns>Instance of the service or null if it can't be found.</returns>
    public new object GetService( Type serviceType )
    {
        // Check arguments
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( serviceType, nameof( serviceType ) );
#else
        if ( serviceType == null ) throw new ArgumentNullException( nameof( serviceType ) );
#endif

        object service = null;
        if ( this.serviceContainer is not null )
        {
            service = this.serviceContainer.GetService( serviceType );
        }

        if ( service == null )
        {
            _ = base.GetService( serviceType );
        }

        return service;
    }

    /// <summary>
    /// Called when a numeric value has to be converted to a string.
    /// </summary>
    [SRDescription( "DescriptionAttributeChartEvent_PrePaint" )]
    public event EventHandler<FormatNumberEventArgs> FormatNumber;

    /// <summary>
    /// Utility method for firing the FormatNumber event. Allows it to be
    /// handled via OnFormatNumber as is the usual pattern as well as via
    /// CallOnFormatNumber.
    /// </summary>
    /// <param name="caller">Event caller. Can be ChartPicture, ChartArea or Legend objects.</param>
    /// <param name="e">Event arguments</param>
    private void OnFormatNumber( object caller, FormatNumberEventArgs e )
    {
        FormatNumber?.Invoke( caller, e );
    }

    /// <summary>
    /// Called when a numeric value has to be converted to a string.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected virtual void OnFormatNumber( FormatNumberEventArgs e )
    {
        this.OnFormatNumber( this, e );
    }

    /// <summary>
    /// Called when a numeric value has to be converted to a string.
    /// </summary>
    /// <param name="caller">Event caller. Can be ChartPicture, ChartArea or Legend objects.</param>
    /// <param name="e">Event arguments.</param>
    internal void CallOnFormatNumber( object caller, FormatNumberEventArgs e )
    {
        this.OnFormatNumber( caller, e );
    }

    #endregion

    #region " accessibility "

    // Current chart accessibility object
    private ChartAccessibleObject _chartAccessibleObject;

    /// <summary>
    /// Overridden to return the custom AccessibleObject for the entire chart.
    /// </summary>
    /// <returns>Chart accessibility object.</returns>
    protected override AccessibleObject CreateAccessibilityInstance()
    {
        this._chartAccessibleObject ??= new ChartAccessibleObject( this );
        return this._chartAccessibleObject;
    }

    /// <summary>
    /// Reset accessibility object children.
    /// </summary>
    private void ResetAccessibilityObject()
    {
        this._chartAccessibleObject?.ResetChildren();
    }

    #endregion // Accessibility

    #region " idisposable override "

    /// <summary>
    /// Disposing control resources
    /// </summary>
    protected override void Dispose( bool disposing )
    {
        // call first because font cache
        base.Dispose( disposing );

        if ( disposing )
        {
            // Dispose managed objects here
            this._imageLoader?.Dispose();
            this._imageLoader = null;

            this.Images?.Dispose();
            this.Images = null;

            this._chartTypeRegistry?.Dispose();
            this._chartTypeRegistry = null;

            this.serviceContainer?.RemoveService( typeof( Chart ) );
            this.serviceContainer?.Dispose();
            this.serviceContainer = null;

            // Dispose selection manager
            this.selection?.Dispose();
            this.selection = null;

            // Dispose print manager
            this.Printing?.Dispose();
            this.Printing = null;

            // Dispose buffer
            this.paintBufferBitmap?.Dispose();
            this.paintBufferBitmap = null;

            this.paintBufferBitmapGraphics?.Dispose();
            this.paintBufferBitmapGraphics = null;
        }

        base.Dispose( disposing );

        //The chart picture and data manager will be the last to be disposed
        if ( disposing )
        {
            this._dataManager?.Dispose();
            this._dataManager = null;

            this.chartPicture?.Dispose();
            this.chartPicture = null;
        }
    }
    #endregion
}

#region " customize event delegate "
/// <summary>
/// Chart legend customize events arguments
/// </summary>
public class CustomizeLegendEventArgs : EventArgs
{
    /// <summary>
    /// Default constructor is not accessible
    /// </summary>
    private CustomizeLegendEventArgs()
    {
    }

    /// <summary>
    /// Customize legend event arguments constructor
    /// </summary>
    /// <param name="legendItems">Legend items collection.</param>
    public CustomizeLegendEventArgs( LegendItemsCollection legendItems ) => this.LegendItems = legendItems;

    /// <summary>
    /// Customize legend event arguments constructor
    /// </summary>
    /// <param name="legendItems">Legend items collection.</param>
    /// <param name="legendName">Legend name.</param>
    public CustomizeLegendEventArgs( LegendItemsCollection legendItems, string legendName )
    {
        this.LegendItems = legendItems;
        this.LegendName = legendName;
    }

    /// <summary>
    /// Legend name.
    /// </summary>
    public string LegendName { get; private set; } = "";

    /// <summary>
    /// Legend items collection.
    /// </summary>
    public LegendItemsCollection LegendItems { get; private set; }

}

#endregion

#pragma warning restore IDE1006 // Naming Styles
#pragma warning restore IDE0044 // Add readonly modifier

