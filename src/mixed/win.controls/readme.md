# About

The [cc.isr.Visuals.Charting] packages the System.Windows.Forms.DataVisualization, which is a .Net library for charting. This library was part of the .NET Framework versions 4.x. The source code, which was published by the .NET Foundation, is compiled in this library targeting .NET 4.8 and 7.0.

# License
[cc.isr.Visuals.Charting] uses the System.Windows.Forms.DataVisualization.Charting chart control which is lLicensed to the .NET Foundation under one or more agreements. The .NET Foundation licenses this file to you under the MIT license. See the LICENSE file in the project root for more information. 

# How to Use


## Histogram example
The following code comes from a console application demo program that is 
part of the [Core Framework Repository].

```
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

using cc.isr.Std.Cartesian;
using cc.isr.Std.RandomExtensions;

namespace cc.isr.Visuals.Charting.Histogram.Example;

/// <summary> Form for viewing the chart histogram. </summary>
/// <remarks> David, 2020-10-26. </remarks>
public partial class ChartHistogramForm : Form
{

    /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public ChartHistogramForm()
    {
        this.InitializeComponent();
        var histogram = new HistogramBindingList( -3, 3d, 31 );
        histogram.Initialize();
        var bindingList = new BindingList< cc.isr.Std.Cartesian.CartesianPoint<double>>();
        var random = new Random();
        for ( int i = 1; i <= 10000; i++ )
        {
            var p = new cc.isr.Std.Cartesian.CartesianPoint<double>( i, random.NextNormal() );
            bindingList.Add( p );
            histogram.Update( p.Y );
        }

        this.HistogramChart.InitializeKnownState( false );
        _ = this.HistogramChart.BindLineSeries( "Histogram", SeriesChartType.FastLine, histogram );
        this.HistogramChart.ChartArea.AxisX.Minimum = -3;
        this.HistogramChart.ChartArea.AxisX.Maximum = 3d;
        this.HistogramChart.ChartArea.AxisX.Interval = 1d;
        histogram.ListChanged += this.Histogram_ListChanged;
    }

    /// <summary> Event handler. Called by Histogram for list changed events. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      List changed event information. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
    private void Histogram_ListChanged( object sender, ListChangedEventArgs e )
    {
        if ( this.InvokeRequired )
        {
            _ = this.Invoke( new Action<object, ListChangedEventArgs>( this.Histogram_ListChanged ), new object[] { sender, e } );
        }
        else
        {
            if ( sender is not HistogramBindingList histogram || e is null )
                return;
            string activity = $"handling {nameof( HistogramBindingList )}.{e.ListChangedType} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, ListChangedEventArgs>( this.Histogram_ListChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HistogramChart.DataBind();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception occurred {activity}: {ex.ToString()}" );
            }
        }
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="T:System.Windows.Forms.Form" />.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }
}
```

# Key Features

* [Chart Class];

# Main Types

See [Chart Class]:

# Feedback

[cc.isr.Visuals.Charting] is released as open source
under the MIT license.
Bug reports and contributions are welcome at 
the [Charting Repository].

[cc.isr.Visuals.Charting]: https://bitbucket.org/davidhary/dn.charting
[Chart Class]: https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.datavisualization.charting.chart?view=netframework-4.8
