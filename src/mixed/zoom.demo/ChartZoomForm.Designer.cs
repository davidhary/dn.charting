using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.Visuals.Charting.Zoom.Example;

public partial class ChartZoomForm : Form
{
    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this._zoomChart = new cc.isr.Visuals.Charting.LineChartControl();
        this.PromptLabel = new System.Windows.Forms.Label();
        ((System.ComponentModel.ISupportInitialize)(this._zoomChart)).BeginInit();
        this.SuspendLayout();
        // 
        // _zoomChart
        // 
        this._zoomChart.Dock = System.Windows.Forms.DockStyle.Fill;
        this._zoomChart.Location = new System.Drawing.Point(0, 0);
        this._zoomChart.Name = "_ZoomChart";
        this._zoomChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
        this._zoomChart.PaletteCustomColors = new System.Drawing.Color[] {
    System.Drawing.Color.Blue,
    System.Drawing.Color.DarkRed,
    System.Drawing.Color.OliveDrab,
    System.Drawing.Color.DarkOrange,
    System.Drawing.Color.Purple,
    System.Drawing.Color.Turquoise,
    System.Drawing.Color.Green,
    System.Drawing.Color.Crimson,
    System.Drawing.Color.RoyalBlue,
    System.Drawing.Color.Sienna,
    System.Drawing.Color.Teal,
    System.Drawing.Color.YellowGreen,
    System.Drawing.Color.SandyBrown,
    System.Drawing.Color.DeepSkyBlue,
    System.Drawing.Color.Brown,
    System.Drawing.Color.Cyan};
        this._zoomChart.Size = new System.Drawing.Size(800, 437);
        this._zoomChart.TabIndex = 0;
        this._zoomChart.Text = "Zoom Chart";
        // 
        // PromptLabel
        // 
        this.PromptLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
        this.PromptLabel.Location = new System.Drawing.Point(0, 437);
        this.PromptLabel.Name = "PromptLabel";
        this.PromptLabel.Size = new System.Drawing.Size(800, 13);
        this.PromptLabel.TabIndex = 1;
        this.PromptLabel.Text = "Hold the control key and drag the mouse to select an area to zoom on";
        // 
        // ChartZoomForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(800, 450);
        this.Controls.Add(this._zoomChart);
        this.Controls.Add(this.PromptLabel);
        this.Name = "ChartZoomForm";
        this.Text = "ChartZoomForm";
        ((System.ComponentModel.ISupportInitialize)(this._zoomChart)).EndInit();
        this.ResumeLayout(false);

    }

    private LineChartControl _zoomChart;
    private Label PromptLabel;
}
