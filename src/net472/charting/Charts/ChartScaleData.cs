using System.Drawing;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace cc.isr.Visuals.Charting;

/// <summary> Container class to maintain scaling data for each chart's axes. </summary>
/// <remarks>
/// From https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart.
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-10-20. </remarks>
/// <param name="chart"> The chart. </param>
public class ChartScaleData( Chart chart )
{
    /// <summary> The chart. </summary>
    private readonly Chart _chart = chart;
    private double _xBaseMin, _xBaseMax, _xBaseInt, _xBaseMinorInt;
    private double _yBaseMin, _yBaseMax, _yBaseInt, _yBaseMinorInt;

    /// <summary> Gets the is zoomed. </summary>
    /// <value> The is zoomed. </value>
    public bool IsZoomed { get; set; } = false;

    /// <summary>   Gets or sets a value indicating whether the scales are valid. 
    ///             True if scales are valid. Only true after the chart has been drawn once. </summary>
    /// <value> True if scales are valid, false if not. </value>
    public bool ScalesAreValid { get; set; }

    /// <summary> Updates the axis base data x coordinate. </summary>
    /// <remarks> David, 2020-09-07. </remarks>
    public void UpdateAxisBaseDataX()
    {
        Axis axis = this._chart.ChartAreas.First().AxisX;
        this._xBaseMinorInt = axis.MinorTickMark.Interval;
        this._xBaseInt = axis.Interval;
        this._xBaseMax = axis.Maximum;
        this._xBaseMin = axis.Minimum;
    }

    /// <summary> Updates the axis base data y coordinate. </summary>
    /// <remarks> David, 2020-09-07. </remarks>
    public void UpdateAxisBaseDataY()
    {
        Axis axis = this._chart.ChartAreas.First().AxisY;
        this._yBaseMinorInt = axis.MinorTickMark.Interval;
        this._yBaseInt = axis.Interval;
        this._yBaseMax = axis.Maximum;
        this._yBaseMin = axis.Minimum;
    }

    /// <summary> Updates the axis base data. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void UpdateAxisBaseData()
    {
        this.UpdateAxisBaseDataX();
        this.UpdateAxisBaseDataY();
        this.ScalesAreValid = true;
    }

    /// <summary> Resets the axis scale x coordinate. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void ResetAxisScaleX()
    {
        Axis axis = this._chart.ChartAreas.First().AxisX;
        axis.MinorTickMark.Interval = this._xBaseMinorInt;
        axis.Interval = this._xBaseInt;
        axis.Maximum = this._xBaseMax;
        axis.Minimum = this._xBaseMin;
    }

    /// <summary> Resets the axis scale y coordinate. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void ResetAxisScaleY()
    {
        Axis axis = this._chart.ChartAreas.First().AxisY;
        axis.MinorTickMark.Interval = this._yBaseMinorInt;
        axis.Interval = this._yBaseInt;
        axis.Maximum = this._yBaseMax;
        axis.Minimum = this._yBaseMin;
    }

    /// <summary> Resets the axis scale. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public void ResetAxisScale()
    {
        this.ResetAxisScaleX();
        this.ResetAxisScaleY();
    }

    /// <summary> Gets the chart area rectangle f. </summary>
    /// <value> The chart area rectangle f. </value>
    public RectangleF ChartAreaRectangleF
    {
        get
        {
            Rectangle cr = this._chart.ClientRectangle;
            RectangleF rfp = this._chart.ChartAreas.First().Position.ToRectangleF();
            // RFP is the chart area rectangle as percentages of the entire chart ClientRectangle
            float chAreaX = rfp.Left * cr.Width / 100.0f;
            float chAreaY = rfp.Top * cr.Height / 100.0f;
            float chAreaW = rfp.Width * cr.Width / 100.0f;
            float chAreaH = rfp.Height * cr.Height / 100.0f;
            return new RectangleF( chAreaX, chAreaY, chAreaW, chAreaH );
        }
    }

    /// <summary> Gets the chart area rectangle. </summary>
    /// <value> The chart area rectangle. </value>
    public Rectangle ChartAreaRectangle => Rectangle.Round( this.ChartAreaRectangleF );

    /// <summary> Gets the inner plot rectangle f. </summary>
    /// <value> The inner plot rectangle f. </value>
    public RectangleF InnerPlotRectangleF
    {
        get
        {
            // this is the inner plot area rectangle as percentages of the chart area rectangle 
            RectangleF rfi = this._chart.ChartAreas.First().InnerPlotPosition.ToRectangleF();
            RectangleF chArea = this.ChartAreaRectangleF;
            float ipX = chArea.X + (rfi.Left * chArea.Width / 100.0f);
            float ipY = chArea.Y + (rfi.Top * chArea.Height / 100.0f);
            float ipW = rfi.Width * chArea.Width / 100.0f;
            float ipH = rfi.Height * chArea.Height / 100.0f;
            return new RectangleF( ipX, ipY, ipW, ipH );
        }
    }

    /// <summary> Gets the inner plot rectangle. </summary>
    /// <value> The inner plot rectangle. </value>
    public Rectangle InnerPlotRectangle => Rectangle.Round( this.InnerPlotRectangleF );
}
