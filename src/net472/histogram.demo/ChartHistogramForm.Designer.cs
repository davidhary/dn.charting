
namespace cc.isr.Visuals.Charting.Histogram.Example;

partial class ChartHistogramForm
{
    #region " windows form designer generated code "

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
        System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
        System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
        this.HistogramChart = new cc.isr.Visuals.Charting.LineChartControl();
        ((System.ComponentModel.ISupportInitialize)(this.HistogramChart)).BeginInit();
        this.SuspendLayout();
        // 
        // LineChartControl
        // 
        chartArea1.Name = "ChartArea1";
        this.HistogramChart.ChartAreas.Add(chartArea1);
        this.HistogramChart.Dock = System.Windows.Forms.DockStyle.Fill;
        legend1.Name = "Legend1";
        this.HistogramChart.Legends.Add(legend1);
        this.HistogramChart.Location = new System.Drawing.Point(0, 0);
        this.HistogramChart.Name = "LineChartControl";
        series1.ChartArea = "ChartArea1";
        series1.Legend = "Legend1";
        series1.Name = "Series1";
        this.HistogramChart.Series.Add(series1);
        this.HistogramChart.Size = new System.Drawing.Size(800, 450);
        this.HistogramChart.TabIndex = 0;
        this.HistogramChart.Text = "LineChartControl";
        // 
        // Form1
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(800, 450);
        this.Controls.Add(this.HistogramChart);
        this.Name = "ChartHistogramForm";
        this.Text = "Chart Histogram Form";
        ((System.ComponentModel.ISupportInitialize)(this.HistogramChart)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private LineChartControl HistogramChart;
}

