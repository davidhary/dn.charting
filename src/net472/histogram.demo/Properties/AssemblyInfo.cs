// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: System.Reflection.AssemblyTitle( "cc.isr.Visuals.Charting.Histogram.Example" )]
[assembly: System.Reflection.AssemblyDescription( "Charting histogram example using .NET 4.72 library" )]
[assembly: System.Reflection.AssemblyProduct( "cc.isr.Visuals.Charting.Histogram.Example" )]
