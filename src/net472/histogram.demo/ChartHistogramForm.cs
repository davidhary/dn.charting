using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using cc.isr.Std.Cartesian;
using cc.isr.Std.RandomExtensions;

namespace cc.isr.Visuals.Charting.Histogram.Example;

/// <summary> Form for viewing the chart histogram. </summary>
/// <remarks> David, 2020-10-26. </remarks>
public partial class ChartHistogramForm : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public ChartHistogramForm()
    {
        this.InitializeComponent();
        HistogramBindingList histogram = new( -3, 3d, 31 );
        histogram.Initialize();
        BindingList<CartesianPoint<double>> bindingList = [];
        Random random = new();
        for ( int i = 1; i <= 10000; i++ )
        {
            CartesianPoint<double> p = new( i, random.NextNormal() );
            bindingList.Add( p );
            _ = histogram.Update( p.Y );
        }

        this.HistogramChart.InitializeKnownState( false );
        _ = this.HistogramChart.BindLineSeries( "Histogram", SeriesChartType.FastLine, histogram );
        this.HistogramChart.ChartArea.AxisX.Minimum = -3;
        this.HistogramChart.ChartArea.AxisX.Maximum = 3d;
        this.HistogramChart.ChartArea.AxisX.Interval = 1d;
        histogram.ListChanged += this.Histogram_ListChanged;
    }

    /// <summary> Event handler. Called by Histogram for list changed events. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      List changed event information. </param>
    private void Histogram_ListChanged( object sender, ListChangedEventArgs e )
    {
        if ( this.InvokeRequired )
        {
            _ = this.Invoke( new Action<object, ListChangedEventArgs>( this.Histogram_ListChanged ), [sender, e] );
        }
        else
        {
            if ( sender is not HistogramBindingList histogram || e is null )
                return;
            string activity = $"handling {nameof( HistogramBindingList )}.{e.ListChangedType} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, ListChangedEventArgs>( this.Histogram_ListChanged ), [sender, e] );
                }
                else
                {
                    this.HistogramChart.DataBind();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception occurred {activity}: {ex}" );
            }
        }
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }
}
