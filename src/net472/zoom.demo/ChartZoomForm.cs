using System.Diagnostics;

namespace cc.isr.Visuals.Charting.Zoom.Example;

/// <summary> Form for viewing the chart zoom. </summary>
/// <remarks> David, 2020-10-26. </remarks>
public partial class ChartZoomForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public ChartZoomForm()
    {
        this.InitializeComponent();
        this._zoomChart.InitializeKnownState( false );
        this._zoomChart.SetupChartZoomExample();
        this._zoomChart.ToggleDashedZoomRectangleMouseHandlers( true );
        this._zoomChart.PopulateContextMenu();
        this._zoomChart.ToggleContextMenu( true );
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
                this._zoomChart?.ToggleDashedZoomRectangleMouseHandlers( false );
                this._zoomChart?.ToggleContextMenu( false );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }
}
