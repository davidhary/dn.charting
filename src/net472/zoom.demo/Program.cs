using System;
using System.Windows.Forms;

namespace cc.isr.Visuals.Charting.Zoom.Example;

static class Program
{
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault( false );
        Application.Run( new ChartZoomForm() );
    }
}
